package com.cydcor.framework.census;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import com.cydcor.framework.utils.ServiceLocator;

public class ImportZipData extends ImportData {
	private static final transient Log logger = LogFactory
			.getLog(ImportData.class);

	public ImportZipData() {
	}

	@Override
	public void processCountyData(String filePath, String stateName) {
		// super.processCountyData(filePath, stateName);
		File file = new File(filePath);
		FileInputStream fileInputStream = null;
		BufferedInputStream bufferedInputStream = null;
		DataInputStream dataInputStream = null;
		try {
			fileInputStream = new FileInputStream(file);
			bufferedInputStream = new BufferedInputStream(fileInputStream);
			dataInputStream = new DataInputStream(bufferedInputStream);
			Polygon polygon = null;
			String id = null;
			boolean eor = false;
			boolean isErrorRecord = false;
			StringBuffer coordinates = null;
			String last = null, first = null;
			List<Polygon> countyDataBeans = new ArrayList<Polygon>();
			while (dataInputStream.available() != 0) {

				List<String> raw = getData(dataInputStream.readLine());
				switch (raw.size()) {
				case 1:
					if (!eor && raw.get(0).equalsIgnoreCase("END")) {
						eor = true;
						if (!isErrorRecord) {
							isErrorRecord = false;
							if (!first.equals(last))
								coordinates.append(first);
							polygon.setCoordinates(coordinates.toString()
									.substring(0, coordinates.length() - 2));
							if (keyIds.get(polygon.getCountyTypeId()) != null) {
								polygon.getTerritory().setKey(
										keyIds.get(polygon.getCountyTypeId()));
								countyDataBeans.add(polygon);
							}
							first = null;
						}
					} else {
						isErrorRecord = true;
					}
					break;
				case 2:
					if (!isErrorRecord) {
						String current = new BigDecimal(raw.get(0)) + " "
								+ new BigDecimal(raw.get(1)) + ", ";
						if (first == null) {
							first = current;
						}
						// if (!current.equals(last))
						coordinates.append(current);
						last = current;
					} else {
						eor = false;
					}
					break;
				case 3:
					eor = false;
					isErrorRecord = false;
					polygon = new Polygon();
					polygon.setType("ZIP");
					polygon.setColor("0000FF");
					polygon.setOpacity("0.35");
					polygon.setFill(true);
					polygon.setLineColor("000000");
					polygon.setCreatedDate(now);
					polygon.setCreatedUser("admin");
					polygon.setModifiedDate(now);
					polygon.setModifiedUser("admin");
					coordinates = new StringBuffer("");
					id = raw.get(0).trim();
					polygon.setCountyTypeId(id);
					// first = new BigDecimal(raw.get(1)) + " "
					// + new BigDecimal(raw.get(2)) + ", ";
					// coordinates.append(first);
					break;
				default:
					break;
				}

			}
			dbUtil.insertPolygons(countyDataBeans);
			dataInputStream.close();
			bufferedInputStream.close();
			fileInputStream.close();
		} catch (FileNotFoundException e) {

			logger.debug(e);
		} catch (IOException e) {

			logger.debug(e);
		}

	}

	@Override
	public boolean processCountyDef(String filePath, String stateName)
			throws SQLException {
		File file = new File(filePath);
		FileInputStream fileInputStream = null;
		BufferedInputStream bufferedInputStream = null;
		DataInputStream dataInputStream = null;
		int i = 0;
		int j = 0;

		try {
			fileInputStream = new FileInputStream(file);
			bufferedInputStream = new BufferedInputStream(fileInputStream);
			dataInputStream = new DataInputStream(bufferedInputStream);
			Territory territory = null;
			List<Territory> zips = new ArrayList<Territory>();
			while (dataInputStream.available() != 0) {
				j = i % 6;
				String data = dataInputStream.readLine().trim().replaceAll(
						"\"", "");
				switch (j) {
				case 0:

					territory = new Territory();
					territory.setStateName(stateName);
					territory.setType("ZIP");
					territory.setCreatedDate(now);
					territory.setCreatedUser("admin");
					territory.setModifiedDate(now);
					territory.setModifiedUser("admin");

					if (!data.trim().equals(""))
						territory.setCountyTypeId(data.trim());
					break;
				case 1:
					territory.setStateId(data.trim());
					break;
				case 2:
					territory.setName(data.trim());
					break;
				case 3:
					territory.setDescription("TerritoryType:"
							+ territory.getType());
					break;
				case 4:
					// territory.setLsad(data);
					break;
				default:
					if (StringUtils.isNotBlank(territory.getName()))
						zips.add(territory);
					break;
				}
				i++;
			}
			Map<String, Integer> ids = dbUtil.insertCounties(zips, stateName);
			if (ids != null)
				keyIds.putAll(ids);
			else {
				logger
						.debug("State " + territory.getStateId()
								+ " was Skipped");
				keyIds.clear();
				return false;
			}
			dataInputStream.close();
			bufferedInputStream.close();
			fileInputStream.close();
		} catch (FileNotFoundException e) {
			logger.debug(e);
		} catch (IOException e) {
			logger.debug(e);
		}
		return true;
	}

	/**
	 * Main method to insert Counties Info and Data
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		ImportZipData importData = new ImportZipData();

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		JdbcTemplate jdbcTemplate = (JdbcTemplate) context
				.getBean("jdbcTemplate");
		PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator
				.getService("txManager");
		importData.dbUtil = new DBUtil(jdbcTemplate, transactionManager);
		importData.dbUtil.getCampaignOffices("346336");
		File file = new File(
				"C:\\development\\idhasoft\\cydcor\\svn\\imapsolution\\ZipCounties.txt");
		FileReader fileReader;
		LineNumberReader numberReader;
		String line;
		try {
			fileReader = new FileReader(file);
			numberReader = new LineNumberReader(fileReader);
			while ((line = numberReader.readLine()) != null) {

				boolean executed = importData.processCountyDef(
						"C:\\development\\idhasoft\\cydcor\\data\\zip\\ZipCodes\\"
								+ line + "\\" + line + "-" + "Def.dat", line);
				if (executed) {
					importData.processCountyData(
							"C:\\development\\idhasoft\\cydcor\\data\\zip\\ZipCodes\\"
									+ line + "\\" + line + "-" + "Data.dat",
							line);
					importData.keyIds.clear();
					logger.debug(line + " is Done!!!");
				}
			}
			logger.debug("*** DB Import Finished ***");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			logger.debug(e);
		} catch (SQLException e) {
			logger.debug(e);
		}

	}
}