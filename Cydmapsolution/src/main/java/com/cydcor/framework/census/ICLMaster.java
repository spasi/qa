package com.cydcor.framework.census;

import org.apache.commons.lang.StringUtils;

public class ICLMaster {
	private Integer iclKey;
	private Integer officeSeq;
	private String officeCode;
	private String pointCoordinates;
	private String officeAddress;
	private String officeName;

	public Integer getIclKey() {
		return iclKey;
	}

	public void setIclKey(Integer iclKey) {
		this.iclKey = iclKey;
	}

	public Integer getOfficeSeq() {
		return officeSeq;
	}

	public void setOfficeSeq(Integer officeSeq) {
		this.officeSeq = officeSeq;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getPointCoordinates() {
		return pointCoordinates;
	}

	public void setPointCoordinates(String pointCoordinates) {
		this.pointCoordinates = pointCoordinates;
	}

	public String getPointCoordinatesForDB() {
		return "POINT(" + pointCoordinates.replaceAll(",", " ") + ")";
	}

	public String getPointCoordinatesForKML() {
		try {
			pointCoordinatesForKML = pointCoordinates.replaceAll(
					"POINT |\\(|\\)", "").replaceAll(" ", ",");
		} catch (Exception e) {
			return "";
		}
		if (StringUtils.isBlank(pointCoordinatesForKML)) {
			pointCoordinatesForKML = "";
		}

		return pointCoordinatesForKML;
	}

	public void setPointCoordinatesForKML(String pointCoordinatesForKML) {
		this.pointCoordinatesForKML = pointCoordinatesForKML;
	}

	private String pointCoordinatesForKML;

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		if(StringUtils.isNotBlank(officeAddress))
			this.officeAddress = officeAddress.replaceAll("`", "");
	}

	public String getOfficeName() {
		if(StringUtils.isNotBlank(officeAddress))
			return officeName.replaceAll("`", "");
		return "";
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

}
