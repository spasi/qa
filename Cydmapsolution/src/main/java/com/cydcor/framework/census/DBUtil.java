package com.cydcor.framework.census;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.TempLeadSheet;
import com.cydcor.framework.rowmapper.LeadMasterMapper;
import com.cydcor.framework.rowmapper.LeadMasterStreamMapper;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.RandomColor;

/**
 * DB Operations
 * 
 * @author Aswin
 */

public class DBUtil {
    protected static Log logger = LogFactory.getLog(DBUtil.class);

    public DBUtil(JdbcTemplate template, PlatformTransactionManager transactionManager) {
	this.jdbcTemplate = template;
	this.transactionTemplate = new TransactionTemplate(transactionManager);
    }

    /**
     * Returns Connection Object
     * 
     * @param db_connect_string
     * @param db_userid
     * @param db_password
     * @return
     */

    JdbcTemplate jdbcTemplate;
    TransactionTemplate transactionTemplate;

    public Connection dbConnect(String db_connect_string, String db_userid, String db_password) {

	try {
	    Class.forName("net.sourceforge.jtds.jdbc.Driver");
	    Connection conn = DriverManager.getConnection(db_connect_string, db_userid, db_password);
	    return conn;
	} catch (Exception e) {
	    logger.debug(e);
	}
	return null;
    }

    /**
     * Inserts all Counties definition of a State
     * 
     * @param counties
     * @return
     * @throws SQLException
     */
    public Map<String, Integer> insertCounties(final List<Territory> counties, String stateName) {
	// final String sql =
	// "insert into IMS.IMS_TERRITORY (TERRITORY_NAME,TERRITORY_DESCRIPTION,TERRITORY_TYPE,STATE_ID,STATE_NAME,NUMBER_OF_BUSINESSES,NUMBER_OF_ORDERS,IS_ACTIVE,CREATED_DATE,CREATED_BY,MODIFIED_DATE,MODIFIED_BY,TERRITORY_GROUP) values (?,?,?,?,?,?,?,?,?,?,?,?,?);";

	// Connection connection = dbConnect(URL, USERNAME, PASSWORD);
	final Map<String, Integer> map = new HashMap<String, Integer>();

	try {
	    transactionTemplate.execute(new TransactionCallbackWithoutResult() {
		KeyHolder keyHolder = new GeneratedKeyHolder();

		@Override
		protected void doInTransactionWithoutResult(TransactionStatus status) {
		    for (final Territory territory : counties) {
			jdbcTemplate.update(new PreparedStatementCreator() {
			    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement statement = connection.prepareStatement(QueryUtils.getQuery().get("INSERT_COUNTIES"),
					Statement.RETURN_GENERATED_KEYS);
				statement.setString(1, territory.getName());
				statement.setString(2, territory.getDescription());
				statement.setString(3, territory.getType());
				statement.setString(4, territory.getStateId());
				statement.setString(5, territory.getStateName());
				statement.setInt(6, territory.getNumberOfBusinesses());
				statement.setInt(7, territory.getNumberOfOrders());
				statement.setBoolean(8, territory.isActive());
				statement.setDate(9, new Date(territory.getCreatedDate().getTime()));
				statement.setString(10, territory.getCreatedUser());
				statement.setDate(11, new Date(territory.getModifiedDate().getTime()));
				statement.setString(12, territory.getModifiedUser());
				statement.setString(13, territory.getGroup());

				return statement;
			    }
			}, keyHolder);
			map.put(territory.getCountyTypeId(), keyHolder.getKey().intValue());
		    }
		}
	    });
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	    logger.debug(stateName + "state was skipped ");
	    return null;
	}
	return map;
    }

    /**
     * Inserts all coordinates of County
     * 
     * @param data
     * @throws SQLException
     */
    public void insertPolygons(final List<Polygon> polygons) {
	/*
	 * final String sql =
	 * "insert into IMS.IMS_POLYGON (POLYGON_TYPE,BOUNDARY_COORDINATES,IS_ACTIVE,COLOR,FILL,OPACITY,OUTLINE,LINE_COLOR,LINE_WIDTH,LINE_OPACITY,CREATED_DATE,CREATED_BY,MODIFIED_DATE,MODIFIED_BY,TERRITORY_KEY) values "
	 * +
	 * "(?,geography::STGeomFromWKB(geometry::STPolyFromText(?,4326).MakeValid().Reduce(.00000001).STUnion(geometry::STPolyFromText(?,4326).STStartPoint()).MakeValid().STAsBinary(),4326),?,?,?,?,?,?,?,?,?,?,?,?,?);"
	 * ;
	 */
	KeyHolder generatedKeyHolder = new GeneratedKeyHolder();

	for (final Polygon polygon : polygons) {
	    try {
		jdbcTemplate.update(new PreparedStatementCreator() {

		    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement preparedStatement = con.prepareStatement(QueryUtils.getQuery().get("INSERT_POLYGONS"));
			preparedStatement.setString(1, polygon.getType());
			preparedStatement.setString(2, polygon.getCoordinatesForDB());
			preparedStatement.setString(3, polygon.getCoordinatesForDB());

			preparedStatement.setBoolean(4, polygon.isActive());
			preparedStatement.setString(5, polygon.getColor());
			preparedStatement.setBoolean(6, polygon.getFill());
			preparedStatement.setString(7, polygon.getOpacity());
			preparedStatement.setBoolean(8, polygon.getOutline());
			preparedStatement.setString(9, polygon.getLineColor());
			preparedStatement.setInt(10, polygon.getLineWidth());
			preparedStatement.setString(11, polygon.getLineOpacity());
			preparedStatement.setDate(12, new Date(polygon.getCreatedDate().getTime()));
			preparedStatement.setString(13, polygon.getCreatedUser());
			preparedStatement.setDate(14, new Date(polygon.getModifiedDate().getTime()));
			preparedStatement.setString(15, polygon.getModifiedUser());
			preparedStatement.setInt(16, polygon.getTerritory().getKey());

			return preparedStatement;
		    }
		}, generatedKeyHolder);
		logger.debug(generatedKeyHolder.getKey());
	    } catch (Exception e) {
		logger.error(e + "" + polygon.getKey());
		// DELETE CORRESPONDING TERRITORY KEY
		jdbcTemplate.update(new PreparedStatementCreator() {
		    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement preparedStatement = con.prepareStatement(QueryUtils.getQuery().get("DELETE_TERRITORY_DETAILS_BY_KEY"));
			preparedStatement.setInt(1, polygon.getTerritory().getKey());
			return preparedStatement;
		    };
		});
	    }
	}
	// return generatedKeyHolder.getKey().intValue();

	// return null;
    }

    @SuppressWarnings("unchecked")
    public Polygon getTerritoryDetails(String territoryName) {
	/*
	 * String sql =
	 * "SELECT T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES,T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY,T.ASSIGNED_SALES_REP_TERRITORY_KEY,"
	 * +
	 * "P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),P.IS_ACTIVE,P.COLOR,P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY,P.MODIFIED_DATE,P.MODIFIED_BY FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P where P.TERRITORY_KEY = T.TERRITORY_KEY and T.TERRITORY_NAME='"
	 * + territoryName + "';";
	 */
	try {
	    List<Polygon> list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_TERRITORY_DETAILS"), new Object[] { territoryName }, new RowMapper() {

		public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
		    final Polygon polygon = new Polygon();
		    polygon.getTerritory().setKey(rs.getInt(1));
		    polygon.getTerritory().setName(rs.getString(2));
		    polygon.getTerritory().setDescription(rs.getString(3));
		    polygon.getTerritory().setType(rs.getString(4));
		    polygon.getTerritory().setStateId(rs.getString(5));
		    polygon.getTerritory().setStateName(rs.getString(6));
		    polygon.getTerritory().setNumberOfBusinesses(rs.getInt(7));
		    polygon.getTerritory().setNumberOfOrders(rs.getInt(8));
		    polygon.getTerritory().setActive(rs.getBoolean(9));
		    polygon.getTerritory().setCreatedDate(rs.getDate(10));
		    polygon.getTerritory().setCreatedUser(rs.getString(11));
		    polygon.getTerritory().setModifiedDate(rs.getDate(12));
		    polygon.getTerritory().setModifiedUser(rs.getString(13));
		    polygon.setKey(rs.getInt(14));
		    polygon.setType(rs.getString(15));
		    polygon.setCoordinates(rs.getString(16));
		    polygon.setActive(rs.getBoolean(17));
		    polygon.setColor(rs.getString(18));
		    polygon.setFill(rs.getBoolean(19));
		    polygon.setOpacity(rs.getString(20));
		    polygon.setOutline(rs.getBoolean(21));
		    polygon.setLineColor(rs.getString(22));
		    polygon.setLineWidth(rs.getInt(23));
		    polygon.setLineOpacity(rs.getString(24));
		    polygon.setCreatedDate(rs.getDate(25));
		    polygon.setCreatedUser(rs.getString(26));
		    polygon.setModifiedDate(rs.getDate(27));
		    polygon.setModifiedUser(rs.getString(28));
		    return polygon;
		}
	    });
	    if (list != null && list.size() > 0)
		return list.get(0);
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return null;
    }

    public void updatePolygon(final Polygon polygon) {
	/*
	 * final String territorySql =
	 * "update IMS.IMS_TERRITORY  set TERRITORY_DESCRIPTION=? where TERRITORY_NAME=?;"
	 * ; final String polygonSql =
	 * "update IMS.IMS_POLYGON  set BOUNDARY_COORDINATES= geography::STGeomFromWKB(geometry::STPolyFromText(?,4326).MakeValid().Reduce(.00000001).STUnion(geometry::STPolyFromText(?,4326).STStartPoint()).MakeValid().STAsBinary(),4326), COLOR=?,  OPACITY=?, OUTLINE=?, LINE_COLOR=?, LINE_WIDTH=?, LINE_OPACITY=? where TERRITORY_KEY=(select TERRITORY_KEY from IMS.IMS_TERRITORY  where TERRITORY_NAME=?);"
	 * ;
	 */

	try {

	    jdbcTemplate.update(QueryUtils.getQuery().get("UPDATE_TERRITORY_DETAILS"), new PreparedStatementSetter() {

		public void setValues(PreparedStatement statement) throws SQLException {
		    statement.setString(1, polygon.getTerritory().getDescription());
		    statement.setString(2, polygon.getTerritory().getName());
		    statement.setInt(3, polygon.getTerritory().getKey());
		}
	    });

	    jdbcTemplate.update(QueryUtils.getQuery().get("UPDATE_POLYGON_DETAILS"), new PreparedStatementSetter() {

		public void setValues(PreparedStatement statement) throws SQLException {
		    statement.setString(1, polygon.getCoordinatesForDB());
		    statement.setString(2, polygon.getCoordinatesForDB());
		    statement.setString(3, polygon.getColor());
		    statement.setString(4, polygon.getOpacity());
		    statement.setBoolean(5, polygon.getOutline());
		    statement.setString(6, polygon.getLineColor());
		    statement.setInt(7, polygon.getLineWidth());
		    statement.setString(8, polygon.getLineOpacity());
		    statement.setInt(9, polygon.getTerritory().getKey());

		}
	    });

	} catch (Exception e) {
	    logger.debug(e);
	}
    }

    public void deleteCountyDetails(final String territoryId, final String territoryName, final String territoryMapSeq) {
	/*
	 * final String polygonSql =
	 * "delete from IMS.IMS_Polygon where TERRITORY_KEY=?;"; final String
	 * territorySql =
	 * "delete from IMS.IMS_Territory where TERRITORY_NAME=?;"; final String
	 * iclAssnSql =
	 * "delete from IMS.IMS_ICL_TERRITORY_DEF where TERRITORY_KEY=?;";
	 */
	Long polygonTerritorySeq = null;
	try {
	    polygonTerritorySeq = jdbcTemplate.queryForLong(QueryUtils.getQuery().get("IS_CUSTOM_POLYGON_EXISTS_NAME_DEF_SEQ"), new Object[] { territoryName,
		    territoryId });

	    jdbcTemplate.update(QueryUtils.getQuery().get("DELETE_TERRITORY_DETAILS_BY_TERRITORY_SEQ"), new Object[] { polygonTerritorySeq });

	    jdbcTemplate.update(QueryUtils.getQuery().get("DELETE_POLYGON_DETAILS"), new Object[] { polygonTerritorySeq });

	    jdbcTemplate.update(QueryUtils.getQuery().get("DELETE_TERRITORY_DETAILS"), new Object[] { polygonTerritorySeq });
	} catch (Exception e) {

	    jdbcTemplate.update(QueryUtils.getQuery().get("DELETE_TERRITORY_DETAILS_BY_LEADMAP_SEQ"), new Object[] { new Long(territoryMapSeq) });

	    logger.error(e);
	}
    }

    public Integer insertTerritory(final Territory territory) {
	// final String sql =
	// "insert into IMS.IMS_TERRITORY (TERRITORY_NAME,TERRITORY_DESCRIPTION,TERRITORY_TYPE,COUNTY_TYPE_ID,STATE_ID,STATE_NAME,NUMBER_OF_BUSINESSES,NUMBER_OF_ORDERS,IS_ACTIVE,CREATED_DATE,CREATED_BY,MODIFIED_DATE,MODIFIED_BY) values (?,?,?,?,?,?,?,?,?,?,?,?,?);";

	KeyHolder keyHolder = new GeneratedKeyHolder();
	try {

	    jdbcTemplate.update(new PreparedStatementCreator() {
		public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		    PreparedStatement statement = connection.prepareStatement(QueryUtils.getQuery().get("INSERT_TERRITORY_DEF"),
			    Statement.RETURN_GENERATED_KEYS);
		    statement.setString(1, territory.getName());
		    statement.setString(2, territory.getDescription());
		    statement.setString(3, territory.getType());
		    statement.setString(4, territory.getCountyTypeId());
		    statement.setString(5, territory.getStateId());
		    statement.setString(6, territory.getStateName());
		    statement.setInt(7, territory.getNumberOfBusinesses());
		    statement.setInt(8, territory.getNumberOfOrders());
		    statement.setBoolean(9, territory.isActive());
		    statement.setDate(10, new Date(territory.getCreatedDate().getTime()));
		    statement.setString(11, territory.getCreatedUser());
		    statement.setDate(12, new Date(territory.getModifiedDate().getTime()));
		    statement.setString(13, territory.getModifiedUser());
		    return statement;
		}
	    }, keyHolder);

	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	    return null;
	}
	return keyHolder.getKey().intValue();
    }

    @SuppressWarnings("unchecked")
    public CampaignMarkerDetailObject getCampaignOffices(String campaignSeq) {
	/*
	 * String sql =
	 * "select ASSN.OFFICE_NAME, ASSN.OFFICE_ADDRESS1, icl.GEO_LOCATION.ToString() from IMS.IMS_ICL_MASTER icl,IMS.MERLIN_ICL_DETAILS ASSN WHERE icl.OFFICE_SEQ = ASSN.OFFICE_SEQ and ASSN.CAMPAIGN_SEQ = "
	 * + campaignSeq;
	 */
	CampaignMarkerDetailObject campaign = new CampaignMarkerDetailObject();
	List<ICLMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_CAMPAIGN_OFFICES_DETAILS"), new Object[] { campaignSeq }, new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		    ICLMaster master = new ICLMaster();
		    master.setOfficeName(rs.getString(1));
		    master.setOfficeAddress(rs.getString(2));
		    master.setPointCoordinates(rs.getString(3));

		    return master;
		}
	    });
	    if (list != null && list.size() > 0) {
		for (ICLMaster master : list) {
		    if (!StringUtils.isBlank(master.getPointCoordinates())) {
			campaign.getOffices().add(master);
		    }
		}
		return campaign;
	    }
	} catch (Exception e) {
	    logger.error(e);
	}
	return null;
    }

    public CampaignMarkerDetailObject getOfficeDetails(String officeSeq) {
	/*
	 * String sql =
	 * "select ASSN.OFFICE_NAME, ASSN.OFFICE_ADDRESS1, icl.GEO_LOCATION.ToString() from IMS.IMS_ICL_MASTER icl,IMS.MERLIN_ICL_DETAILS ASSN WHERE icl.OFFICE_SEQ = ASSN.OFFICE_SEQ and ASSN.CAMPAIGN_SEQ = "
	 * + campaignSeq;
	 */
	CampaignMarkerDetailObject campaign = new CampaignMarkerDetailObject();
	List<ICLMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_OFFICE_DETAILS"), new Object[] { officeSeq }, new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		    ICLMaster master = new ICLMaster();
		    master.setOfficeName(rs.getString(1));
		    master.setOfficeAddress(rs.getString(2));
		    master.setPointCoordinates(rs.getString(3));

		    return master;
		}
	    });
	    if (list != null && list.size() > 0) {
		for (ICLMaster master : list) {
		    if (!StringUtils.isBlank(master.getPointCoordinates())) {
			campaign.getOffices().add(master);
		    }
		}
		return campaign;
	    }
	} catch (Exception e) {
	    logger.error(e);
	}
	return null;
    }

    public void insertIclTerritory(final Territory territory) {
	// final String sql =
	// "insert into IMS.IMS_ICL_TERRITORY_DEF(OFFICE_SEQ, TERRITORY_KEY, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY) values (?,?,?,?,?,?,?);";
	KeyHolder keyHolder = new GeneratedKeyHolder();
	try {

	    jdbcTemplate.update(new PreparedStatementCreator() {
		public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		    PreparedStatement statement = connection.prepareStatement(QueryUtils.getQuery().get("INSERT_IMS_ICL_TERRITORY_DEF"),
			    Statement.RETURN_GENERATED_KEYS);
		    statement.setInt(1, territory.getOfficeKey());
		    statement.setInt(2, territory.getKey());
		    statement.setBoolean(3, territory.isActive());
		    statement.setDate(4, new Date(territory.getCreatedDate().getTime()));
		    statement.setString(5, territory.getCreatedUser());
		    statement.setDate(6, new Date(territory.getModifiedDate().getTime()));
		    statement.setString(7, territory.getModifiedUser());
		    return statement;
		}
	    }, keyHolder);

	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
    }

    @SuppressWarnings("unchecked")
    public List<Polygon> selectICLTerritories(String officeKeys) {
	/*
	 * String sql =
	 * "select t.TERRITORY_NAME,t.TERRITORY_DESCRIPTION,p.BOUNDARY_COORDINATES.ToString(),p.COLOR,p.OPACITY,P.POLYGON_TYPE from IMS.IMS_ICL_TERRITORY_DEF itd, IMS.IMS_TERRITORY t, IMS.IMS_POLYGON p where itd.TERRITORY_KEY=t.TERRITORY_KEY and p.TERRITORY_KEY=t.TERRITORY_KEY and itd.OFFICE_SEQ in ("
	 * + officeKeys + ")";
	 */
	List<Polygon> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_ICL_TERRITORIES_DETAILS"), new Object[] { officeKeys }, new RowMapper() {
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		    Polygon polygon = new Polygon();
		    polygon.getTerritory().setName(rs.getString(1));
		    polygon.getTerritory().setDescription(rs.getString(2));
		    polygon.setCoordinates(rs.getString(3));
		    polygon.setColor(rs.getString(4));
		    polygon.setOpacity(rs.getString(5));
		    polygon.setType(rs.getString(6));
		    return polygon;
		}
	    });
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    /**
     * @param campaignSeq
     * @param officeKeys
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Polygon> selectCampaignTerritories(String campaignSeq, String officeKeys) {

	List<Polygon> list = null;
	String queryName = "SELECT_CAMPAIGN_ICL_TERRITORIES_DETAILS";

	try {
	    list = jdbcTemplate
		    .query("select t.TERRITORY_NAME,t.TERRITORY_DESCRIPTION,p.BOUNDARY_COORDINATES.ToString(),p.COLOR,p.OPACITY,P.POLYGON_TYPE from IMS.IMS_ICL_TERRITORY_DEF itd,IMS.IMS_ICL_TERRITORY_MAP MAP, IMS.IMS_TERRITORY t, IMS.IMS_POLYGON p where itd.ICL_TERRITORY_DEF_SEQ = MAP.ICL_TERRITORY_DEF_SEQ AND MAP.TERRITORY_KEY=t.TERRITORY_KEY and p.TERRITORY_KEY=t.TERRITORY_KEY and itd.CAMPAIGN_SEQ = "
			    + campaignSeq + " and itd.OFFICE_SEQ IN (" + officeKeys + ")", new Object[] {}, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			    Polygon polygon = new Polygon();
			    polygon.getTerritory().setName(rs.getString(1));
			    polygon.getTerritory().setDescription(rs.getString(2));
			    polygon.setCoordinates(rs.getString(3));
			    polygon.setColor(rs.getString(4));
			    polygon.setOpacity(rs.getString(5));
			    polygon.setType(rs.getString(6));
			    return polygon;
			}
		    });
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}

	return list;
    }

    @SuppressWarnings("unchecked")
    public List<LeadMaster> selectUnAssingedLeadsUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS");
	try {
	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    @SuppressWarnings("unchecked")
    public List<LeadMaster> selectUnAssingedLeadsByWCUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_WC");
	try {
	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByZipUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_ZIP");
	try {
	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    //code added by sanjay
    public List<LeadMaster> selectUnAssingedLeadsByDAUsingFile(String campaignId, String officeSeq, String selectedFile, String useWirecenters) {

		List<LeadMaster> list = null;
		 boolean breakLoop = false;
		String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_DA");
		try {
			if (StringUtils.isNotBlank(selectedFile)) {
			if(selectedFile.contains("|")){
				String[] combinationFiles = selectedFile.split("\\|");
				String[] spiltSql = sql.split(" ORDER BY ");
				String[] territories = useWirecenters.split("\\|");
				territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
				territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
				sql = spiltSql[0] + " AND ";

				sql = sql + "(" + territories[0]+"='" + combinationFiles[0].replaceAll("'", "")
							+ "' AND " + territories[1]+"='" + combinationFiles[1].replaceAll("'", "") +"') ";

				sql = sql.substring(0, (sql.length()-3))+  "') ORDER BY " + spiltSql[1] ;

			}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
				String[] spiltSql = sql.split(" ORDER BY ");
				sql = spiltSql[0] + " AND CLLI IN (" + selectedFile + ") ORDER BY " + spiltSql[1];
			} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
				String[] spiltSql = sql.split(" ORDER BY ");
				sql = spiltSql[0] + " AND ZIP IN (" + selectedFile + ") ORDER BY " + spiltSql[1];
			} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
				String[] spiltSql = sql.split(" ORDER BY ");
				sql = spiltSql[0] + " AND CITY IN (" + selectedFile + ") ORDER BY " + spiltSql[1];
			} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
				String[] spiltSql = sql.split(" ORDER BY ");
				sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFile + ") ORDER BY " + spiltSql[1];
			} else {
				String[] spiltSql = sql.split(" ORDER BY ");
				sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFile + ") ORDER BY " + spiltSql[1];
			}
			}
			list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());

			for(int i = 0; i < list.size(); i++)
			{
				System.out.println("Show DA" + list.get(i).getDa() + " Wire Center " + list.get(i).getWireCenter());
			}
		} catch (Exception e) {
			logger.debug(e);
			logger.error(e);
		}
		return list;
    }

    
    public List<LeadMaster> selectUnAssingedLeadsByDAUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	 boolean breakLoop = false;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_DA");
    	try {
    	    if (StringUtils.isNotBlank(selectedFiles)) {
    		if(selectedFiles.contains("|")){
    			String[] combinationFiles = selectedFiles.split(",");
    			String[] spiltSql = sql.split(" ORDER BY ");
    			String[] territories = useWirecenters.split("\\|");
    			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
    			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
    			sql = spiltSql[0] + " AND (";
    			for(String fileCombination: combinationFiles)
    			{	
    					sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
    					
    			}
    			sql = sql.substring(0, (sql.length()-3))+  ") ORDER BY " + spiltSql[1] ;
    			
    			
    			
    		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		}
    	    }
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	       	    	
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
        }
    
    
    public List<LeadMaster> selectUnAssingedLeadsByPS01Files(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	 boolean breakLoop = false;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PS01");
    	try {
    	    if (StringUtils.isNotBlank(selectedFiles)) {
    		if(selectedFiles.contains("|")){
    			String[] combinationFiles = selectedFiles.split(",");
    			String[] spiltSql = sql.split(" ORDER BY ");
    			String[] territories = useWirecenters.split("\\|");
    			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
    			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
    			sql = spiltSql[0] + " AND (";
    			for(String fileCombination: combinationFiles)
    			{	
    					sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
    					
    			}
    			sql = sql.substring(0, (sql.length()-3))+  ") ORDER BY " + spiltSql[1] ;
    			
    			
    			
    		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		}
    	    }
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	       	    	
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
        }
    
    
    public List<LeadMaster> selectUnAssingedLeadsByDACRRFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	 boolean breakLoop = false;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_DACRR");
    	try {
    	    if (StringUtils.isNotBlank(selectedFiles)) {
    		if(selectedFiles.contains("|")){
    			String[] combinationFiles = selectedFiles.split(",");
    			String[] spiltSql = sql.split(" ORDER BY ");
    			String[] territories = useWirecenters.split("\\|");
    			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
    			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
    			sql = spiltSql[0] + " AND (";
    			for(String fileCombination: combinationFiles)
    			{	
    					sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
    					
    			}
    			sql = sql.substring(0, (sql.length()-3))+  ") ORDER BY " + spiltSql[1] ;
    			
    			
    			
    		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		}
    	    }
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	       	    	
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
        }
    

    //code added by sanjay 30 Sept 2016 
    public List<LeadMaster> selectUnAssingedLeadsByLLCUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_LLC_Y");
    	try {
    	    if (StringUtils.isNotBlank(selectedFiles)) {
    		if(selectedFiles.contains("|")){
    			String[] combinationFiles = selectedFiles.split(",");
    			String[] spiltSql = sql.split(" ORDER BY ");
    			String[] territories = useWirecenters.split("\\|");
    			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
    			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
    			sql = spiltSql[0] + " AND (";
    			for(String fileCombination: combinationFiles){				
    				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
    			}
    			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
    		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		}
    	    }
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
        }
    //code added by sanjay
    
    @SuppressWarnings("unchecked")
    public List<LeadMaster> selectUnAssingedLeads(String campaignId, String officeSeq, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_FROM_LEADSHEET"), new Object[] { officeSeq, campaignId,
		    leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    @SuppressWarnings("unchecked")
    public List<LeadMaster> selectUnAssingedLeadsByWC(String campaignId, String officeSeq, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_WC_FROM_LEADSHEET"), new Object[] { officeSeq, campaignId,
		    leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByZip(String campaignId, String officeSeq, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_ZIP_FROM_LEADSHEET"), new Object[] { officeSeq, campaignId,
		    leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByDA(String campaignId, String officeSeq, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_DA_FROM_LEADSHEET"), new Object[] { officeSeq, campaignId,
		    leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

	// code added by sanjay
	public List<LeadMaster> selectUnAssingedLeadsByLLC(String campaignId, String officeSeq, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_LLC_FROM_LEADSHEET"), new Object[] { officeSeq, campaignId,
		    leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
  }
	// code added by sanjay
    
    
    
    /**
     * @param campaignSeq
     * @param officeSeq
     * @return
     */
    public List getICLRepsWithDetails(String campaignSeq, String officeSeq) {
	/*
	 * String sql =
	 * "SELECT REP.CAMPAIGN_SEQ,REP.OFFICE_SEQ,REP.FIRST_NAME,REP.LAST_NAME,"
	 * +
	 * "REP.PERSON_ID,TERR.TERRITORY_GROUP,TERR.TERRITORY_TYPE,TERR.TERRITORY_NAME,"
	 * + "TERR.TERRITORY_KEY FROM IMS.MERLIN_ICL_REP_DETAILS REP," +
	 * "IMS.IMS_ICLREP_TERRITORY_DEF MAPPING,IMS.IMS_TERRITORY TERR " +
	 * "WHERE REP.PERSON_ID=MAPPING.PERSON_ID AND MAPPING.TERRITORY_KEY=TERR.TERRITORY_KEY "
	 * + "AND REP.CAMPAIGN_SEQ= " + campaignSeq + " AND REP.OFFICE_SEQ = " +
	 * officeSeq + "";
	 */
	List list = null;
	try {
	    list = jdbcTemplate.queryForList(QueryUtils.getQuery().get("SELECT_ICLREPS_WITH_DETAILS"), new Object[] { campaignSeq, officeSeq });
	} catch (Exception e) {
	    logger.error(e);
	    logger.debug(e);
	}

	return list;

    }

    /**
     * @param sql
     * @return
     */
    public List getResultList(String sql) {
	List list = null;
	try {
	    list = jdbcTemplate.queryForList(sql);
	} catch (Exception e) {
	    logger.error(e);
	    logger.debug(e);
	}

	return list;

    }

    /**
     * @param sql
     * @param columnName
     * @return
     */
    public String getString(String sql, String columnName) {
	List list = null;
	try {
	    list = jdbcTemplate.queryForList(sql);
	} catch (Exception e) {
	    logger.error(e);
	    logger.debug(e);
	}

	if (list != null && !list.isEmpty()) {
	    Map row = (Map) list.get(0);
	    return (String) row.get(columnName);
	}

	return "";

    }

    /**
     * @param sql
     */
    public void update(String sql) {
	try {
	    jdbcTemplate.update(sql);
	} catch (Exception e) {
	    logger.error(e);
	    logger.debug(e);
	}
    }

    /**
     * @param sql
     */
    public void update(String sql, Object[] params) {
	try {
	    jdbcTemplate.update(sql, params);
	} catch (Exception e) {
	    logger.error(e);
	    logger.debug(e);
	}
    }

    @SuppressWarnings("unchecked")
    public List findIclByLeadPoint(String point) {
	/*
	 * String sql =
	 * "select assn.OFFICE_SEQ from IMS.IMS_POLYGON pol, IMS.IMS_TERRITORY ter, IMS.IMS_ICL_TERRITORY_DEF assn where "
	 * +
	 * "assn.TERRITORY_KEY=ter.TERRITORY_KEY and ter.TERRITORY_KEY=pol.TERRITORY_KEY and pol.BOUNDARY_COORDINATES.MakeValid().STContains(geography::STGeomFromText('"
	 * + point + "',4623))=1;";
	 */

	List list = null;
	try {
	    list = jdbcTemplate.queryForList(QueryUtils.getQuery().get("SELECT_ICLBY_LEADPOINT"), new Object[] { point });
	} catch (Exception e) {
	    logger.error(e);
	    logger.debug(e);
	}

	return list;

    }

    @SuppressWarnings("unchecked")
    public List findTerritoryByLeadPoint(String point, String campaignSeq, String officeSeq) {
	List list = null;
	try {
	    list = jdbcTemplate.queryForList(QueryUtils.getQuery().get("SELECT_LEADTERRITORY_LEADPOINT"), new Object[] { campaignSeq, officeSeq, point });
	} catch (Exception e) {
	    logger.error(e);
	    logger.debug(e);
	}
	return list;
    }

    /**
     * @param leadId
     * @param officeSeq
     */
    public void assignLeadToICL(String leadId, Integer officeSeq) {

	/*
	 * String sql =
	 * "update IMS.IMS_LEAD_LIFECYCLE set OFFICE_SEQ=? where LEAD_ID=?";
	 */
	try {
	    jdbcTemplate.update(QueryUtils.getQuery().get("UPDATE_ASSIGNLEAD_TO_ICL"), new Object[] { officeSeq, leadId });
	} catch (Exception e) {
	    logger.error(e);
	    logger.debug(e);
	}
    }

    @SuppressWarnings("unchecked")
    public List<LeadMaster> selectLeadsByCampaign(String campaignSeq) {
	if (campaignSeq.equalsIgnoreCase("-1"))
	    return null;

	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.CAMPAIGN_SEQ ="
	 * + campaignSeq;
	 */

	List<LeadMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_LEAD_BY_CAMPAIGN"), new Object[] { campaignSeq }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    @SuppressWarnings("unchecked")
    public List<LeadMaster> selectLeadsByICL(String officeSeq) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT,MARKER_ICON_IMAGE, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ ="
	 * + officeSeq;
	 */

	List<LeadMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_LEAD_BY_CAMPAIGN"), new Object[] { officeSeq }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    /**
     * @param campaignSeq
     * @param officeSeq
     * @param servletOutputStream
     * @param dispImgs
     * @param showAssignedLeads
     * @return
     */
    @SuppressWarnings("unchecked")
    public void selectLeadsByCampaignAndICL(String campaignSeq, String officeSeq, String selectedFiles, ServletOutputStream servletOutputStream,
	    List<String> dispImgs, boolean showAssignedLeads, String clientKey, String filterByString) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT,MARKER_ICON_IMAGE,  VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ ="
	 * + officeSeq + " AND ilm.CAMPAIGN_SEQ =" + campaignSeq;
	 */
	if (campaignSeq.equalsIgnoreCase("-1") || officeSeq.equalsIgnoreCase("-1"))
	    return;

	String sql = null;
	String countSql = QueryUtils.getQuery().get("ICL_AVAILABLE_LEADS");

	Object[] inputs = null;
	sql = QueryUtils.getQuery().get("SELECT_LEADS_BY_CAMPAIGN_AND_ICL");
	inputs = new Object[] { officeSeq, campaignSeq };
	if(selectedFiles.contains("|")){
		System.out.println("selected files = " + selectedFiles);
		System.out.println("filterByString = " + filterByString);
		String[] combinationFiles = selectedFiles.split(",");
		String[] territories = filterByString.split("\\|");
		territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
		territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
		sql = sql + " AND (";
		countSql = countSql + " AND (";
		StringBuffer query = new StringBuffer("");
		for(String fileCombination: combinationFiles){				
			System.out.println("fileCombination="+fileCombination);
			query.append("(" + territories[0]+"=" + fileCombination.split("\\|")[0] + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1] +") OR ");
		}
		System.out.println("rquery="+query);
		System.out.println("rsql="+sql);
		System.out.println("rcountSql="+countSql);
		sql = sql + query.substring(0, (query.length()-3))+ ") ";
		countSql = countSql + query.substring(0, (query.length()-3))+ ") ";
		System.out.println("rsql="+sql);
		System.out.println("rcountSql="+countSql);
	}else if ("CLLI".equalsIgnoreCase(filterByString)) {
	    if (StringUtils.isNotBlank(selectedFiles)) {
	    	if("67976206".equals(campaignSeq) || "91039497".equals(campaignSeq)){
	    		sql = sql + " AND NODE IN (" + selectedFiles + ")";
	    		countSql = countSql + " AND NODE IN (" + selectedFiles + ")";
	    	}else{
	    		sql = sql + " AND CLLI IN (" + selectedFiles + ")";
	    		countSql = countSql + " AND CLLI IN (" + selectedFiles + ")";
	    	}	
		}
	} else if ("ZIP".equalsIgnoreCase(filterByString)) {
	    if (StringUtils.isNotBlank(selectedFiles)) {
		sql = sql + " AND ZIP IN (" + selectedFiles + ")";
		countSql = countSql + " AND ZIP IN (" + selectedFiles + ")";
	    }
	} else if ("CITY".equalsIgnoreCase(filterByString)) {
	    if (StringUtils.isNotBlank(selectedFiles)) {
		sql = sql + " AND CITY IN (" + selectedFiles + ")";
		countSql = countSql + " AND CITY IN (" + selectedFiles + ")";
	    }
	} else if ("LFA".equalsIgnoreCase(filterByString)) {
	    if (StringUtils.isNotBlank(selectedFiles)) {
		sql = sql + " AND LFA_CODE IN (" + selectedFiles + ")";
		countSql = countSql + " AND LFA_CODE IN (" + selectedFiles + ")";
	    }
	} else {
	    if (StringUtils.isNotBlank(selectedFiles)) {
		sql = sql + " AND RUN_ID IN (" + selectedFiles + ")";
		countSql = countSql + " AND RUN_ID IN (" + selectedFiles + ")";
	    }

	}
	try {
		logger.debug("countSql:"+countSql);
	    int count = jdbcTemplate.queryForInt(countSql, inputs);
	    logger.debug("count:"+count);
	    logger.debug("SELECT_LEADS_BY_CAMPAIGN_AND_ICL sql:"+sql);
	    jdbcTemplate.query(sql, inputs, new LeadMasterStreamMapper(servletOutputStream, count, dispImgs, showAssignedLeads, clientKey));
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
    }

    @SuppressWarnings("unchecked")
    public List<Polygon> getTerritoriesByIds(String territoryIds) {

	String sql = "SELECT T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES,T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY,"
		+ "P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),P.IS_ACTIVE,P.COLOR,P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY,P.MODIFIED_DATE,P.MODIFIED_BY FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P where P.TERRITORY_KEY = T.TERRITORY_KEY and T.TERRITORY_KEY in ("
		+ territoryIds + ");";

	// TODO do not externalize it
	// QueryUtils.getQuery().get("SELECT_TERRITORIES_ID")
	try {
	    List<Polygon> list = jdbcTemplate.query(sql, new RowMapper() {

		public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
		    final Polygon polygon = new Polygon();
		    polygon.getTerritory().setKey(rs.getInt(1));
		    polygon.getTerritory().setName(rs.getString(2));
		    polygon.getTerritory().setDescription(rs.getString(3));
		    polygon.getTerritory().setType(rs.getString(4));
		    polygon.getTerritory().setStateId(rs.getString(5));
		    polygon.getTerritory().setStateName(rs.getString(6));
		    polygon.getTerritory().setNumberOfBusinesses(rs.getInt(7));
		    polygon.getTerritory().setNumberOfOrders(rs.getInt(8));
		    polygon.getTerritory().setActive(rs.getBoolean(9));
		    polygon.getTerritory().setCreatedDate(rs.getDate(10));
		    polygon.getTerritory().setCreatedUser(rs.getString(11));
		    polygon.getTerritory().setModifiedDate(rs.getDate(12));
		    polygon.getTerritory().setModifiedUser(rs.getString(13));

		    polygon.setKey(rs.getInt(14));
		    polygon.setType(rs.getString(15));
		    polygon.setCoordinates(rs.getString(16));
		    polygon.setActive(rs.getBoolean(17));
		    polygon.setColor(rs.getString(18));
		    polygon.setFill(rs.getBoolean(19));
		    polygon.setOpacity(rs.getString(20));
		    polygon.setOutline(rs.getBoolean(21));
		    polygon.setLineColor(rs.getString(22));
		    polygon.setLineWidth(rs.getInt(23));
		    polygon.setLineOpacity(rs.getString(24));
		    polygon.setCreatedDate(rs.getDate(25));
		    polygon.setCreatedUser(rs.getString(26));
		    polygon.setModifiedDate(rs.getDate(27));
		    polygon.setModifiedUser(rs.getString(28));
		    return polygon;
		}
	    });
	    if (list != null && list.size() > 0)
		return list;
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return null;
    }

    @SuppressWarnings("unchecked")
    public List<Polygon> getTerritoriesByParentIds(String parentTerritoryIds) {

	String sql = "SELECT T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES,T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY,P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),P.IS_ACTIVE,P.COLOR,P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY,P.MODIFIED_DATE,P.MODIFIED_BY FROM IMS.IMS_TERRITORY T,IMS.IMS_POLYGON P where P.TERRITORY_KEY = T.TERRITORY_KEY and T.TERRITORY_KEY in ( SELECT TERRITORY_KEY FROM IMS.IMS_ICLLEAD_TERRITORY_MAP WHERE LEAD_TERRITORY_DEF_SEQ IN ( "
		+ parentTerritoryIds + ") )";

	// TODO do not externalize it
	// QueryUtils.getQuery().get("SELECT_TERRITORIES_ID")
	try {
	    final RandomColor color = new RandomColor();
	    List<Polygon> list = jdbcTemplate.query(sql, new RowMapper() {

		public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
		    final Polygon polygon = new Polygon();
		    polygon.getTerritory().setKey(rs.getInt(1));
		    polygon.getTerritory().setName(rs.getString(2));
		    polygon.getTerritory().setDescription(rs.getString(3));
		    polygon.getTerritory().setType(rs.getString(4));
		    polygon.getTerritory().setStateId(rs.getString(5));
		    polygon.getTerritory().setStateName(rs.getString(6));
		    polygon.getTerritory().setNumberOfBusinesses(rs.getInt(7));
		    polygon.getTerritory().setNumberOfOrders(rs.getInt(8));
		    polygon.getTerritory().setActive(rs.getBoolean(9));
		    polygon.getTerritory().setCreatedDate(rs.getDate(10));
		    polygon.getTerritory().setCreatedUser(rs.getString(11));
		    polygon.getTerritory().setModifiedDate(rs.getDate(12));
		    polygon.getTerritory().setModifiedUser(rs.getString(13));

		    polygon.setKey(rs.getInt(14));
		    polygon.setType(rs.getString(15));
		    polygon.setCoordinates(rs.getString(16));
		    polygon.setActive(rs.getBoolean(17));
		    polygon.setColor(color.randomHex());
		    polygon.setFill(rs.getBoolean(19));
		    polygon.setOpacity(rs.getString(20));
		    polygon.setOutline(rs.getBoolean(21));
		    polygon.setLineColor(rs.getString(22));
		    polygon.setLineWidth(rs.getInt(23));
		    polygon.setLineOpacity(rs.getString(24));
		    polygon.setCreatedDate(rs.getDate(25));
		    polygon.setCreatedUser(rs.getString(26));
		    polygon.setModifiedDate(rs.getDate(27));
		    polygon.setModifiedUser(rs.getString(28));
		    return polygon;
		}
	    });
	    if (list != null && list.size() > 0)
		return list;
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return null;
    }

    /**
     * @param campaignSeq
     * @param officeSeq
     * @return
     */
    public List<Polygon> getBoundariesForWCandDA(String campaignSeq, String officeSeq, String defaultView, String wcDaZipString, final String clientKey) {
	if (campaignSeq.equalsIgnoreCase("-1") || officeSeq.equalsIgnoreCase("-1"))
	    return null;

	List<Polygon> list = null;
	String tNames = "";
	String tTypes = "";
	final Map<String, String> zipCount = new HashMap<String, String>();
	final Map<String, String> wcCount = new HashMap<String, String>();
	final Map<String, String> daCount = new LinkedHashMap<String, String>();

	if (StringUtils.isNotBlank(wcDaZipString)) {
	    String[] wdz = wcDaZipString.replaceAll("\\{|\\}", "").split(":");

	    for (String subStr : wdz[0].split(",")) {
		wcCount.put(subStr.split("\\$")[0].trim(), subStr.split("\\$")[1]);
	    }
	    for (String subStr : wdz[1].split(",")) {
		daCount.put(subStr.split("\\$")[0].trim(), subStr.split("\\$")[1]);
	    }
	    for (String subStr : wdz[2].split(",")) {
		zipCount.put(subStr.split("\\$")[0].trim(), subStr.split("\\$")[1]);
	    }

	    String sql = "";

	    if ("byWirecenter".equalsIgnoreCase(defaultView)) {
		tNames = ArrayUtils.toString(wcCount.keySet()).replaceAll("\\[|\\]", "");
		sql = "SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES, "
			+ "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY, P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),"
			+ "P.IS_ACTIVE,P.COLOR, P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY, P.MODIFIED_DATE, P.MODIFIED_BY "
			+ ",T.TERRITORY_GROUP FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P WHERE P.TERRITORY_KEY=T.TERRITORY_KEY AND T.TERRITORY_NAME IN ("
			+ tNames + ") AND T.TERRITORY_TYPE = 'WIRECENTER'";
	    } else if ("byZip".equalsIgnoreCase(defaultView)) {
		tNames = ArrayUtils.toString(zipCount.keySet()).replaceAll("\\[|\\]", "");

		sql = "SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES, "
			+ "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY, P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),"
			+ "P.IS_ACTIVE,P.COLOR, P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY, P.MODIFIED_DATE, P.MODIFIED_BY "
			+ ",T.TERRITORY_GROUP FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P WHERE P.TERRITORY_KEY=T.TERRITORY_KEY AND T.TERRITORY_NAME IN ("
			+ tNames + ") AND T.TERRITORY_TYPE = 'ZIP'";
	    } else if ("byDA".equalsIgnoreCase(defaultView)) {
		tNames = ArrayUtils.toString(daCount.keySet()).replaceAll("\\[|\\]", "");

		sql = "SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES, "
			+ "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY, P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),"
			+ "P.IS_ACTIVE,P.COLOR, P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY, P.MODIFIED_DATE, P.MODIFIED_BY "
			+ ",T.TERRITORY_GROUP FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P WHERE P.TERRITORY_KEY=T.TERRITORY_KEY AND (T.TERRITORY_NAME+'-'+T.TERRITORY_GROUP) IN ("
			+ tNames + ") AND T.TERRITORY_TYPE = 'DA'";

	    } else if ("byWireCenterDA".equalsIgnoreCase(defaultView)) {
		tNames = ArrayUtils.toString(wcCount.keySet()).replaceAll("\\[|\\]", "");
		sql = "SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES, "
			+ "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY, P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),"
			+ "P.IS_ACTIVE,P.COLOR, P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY, P.MODIFIED_DATE, P.MODIFIED_BY "
			+ ",T.TERRITORY_GROUP FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P WHERE P.TERRITORY_KEY=T.TERRITORY_KEY AND T.TERRITORY_NAME IN ("
			+ tNames + ") AND T.TERRITORY_TYPE = 'WIRECENTER'";

		tNames = ArrayUtils.toString(daCount.keySet()).replaceAll("\\[|\\]", "");

		sql = sql
			+ " UNION ALL "
			+ " SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES, "
			+ "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY, P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),"
			+ "P.IS_ACTIVE,P.COLOR, P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY, P.MODIFIED_DATE, P.MODIFIED_BY "
			+ ",T.TERRITORY_GROUP FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P WHERE P.TERRITORY_KEY=T.TERRITORY_KEY AND (T.TERRITORY_NAME+'-'+T.TERRITORY_GROUP) IN ("
			+ tNames + ") AND T.TERRITORY_TYPE = 'DA'";
	    } else if ("byZipDA".equalsIgnoreCase(defaultView)) {
		tNames = ArrayUtils.toString(zipCount.keySet()).replaceAll("\\[|\\]", "");

		sql = "SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES, "
			+ "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY, P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),"
			+ "P.IS_ACTIVE,P.COLOR, P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY, P.MODIFIED_DATE, P.MODIFIED_BY "
			+ ",T.TERRITORY_GROUP FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P WHERE P.TERRITORY_KEY=T.TERRITORY_KEY AND T.TERRITORY_NAME IN ("
			+ tNames + ") AND T.TERRITORY_TYPE = 'ZIP'";

		tNames = ArrayUtils.toString(daCount.keySet()).replaceAll("\\[|\\]", "");

		sql = sql
			+ " UNION ALL "
			+ " SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES, "
			+ "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY, P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),"
			+ "P.IS_ACTIVE,P.COLOR, P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY, P.MODIFIED_DATE, P.MODIFIED_BY "
			+ ",T.TERRITORY_GROUP FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P WHERE P.TERRITORY_KEY=T.TERRITORY_KEY AND (T.TERRITORY_NAME+'-'+T.TERRITORY_GROUP) IN ("
			+ tNames + ") AND T.TERRITORY_TYPE = 'DA'";
	    } else if ("byZipWC".equalsIgnoreCase(defaultView)) {
		tNames = ArrayUtils.toString(zipCount.keySet()).replaceAll("\\[|\\]", "");

		sql = "SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES, "
			+ "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY, P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),"
			+ "P.IS_ACTIVE,P.COLOR, P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY, P.MODIFIED_DATE, P.MODIFIED_BY "
			+ ",T.TERRITORY_GROUP FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P WHERE P.TERRITORY_KEY=T.TERRITORY_KEY AND T.TERRITORY_NAME IN ("
			+ tNames + ") AND T.TERRITORY_TYPE = 'ZIP'";

		tNames = ArrayUtils.toString(wcCount.keySet()).replaceAll("\\[|\\]", "");

		sql = sql
			+ " UNION ALL "
			+ "SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES, "
			+ "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY, P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),"
			+ "P.IS_ACTIVE,P.COLOR, P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY, P.MODIFIED_DATE, P.MODIFIED_BY "
			+ ",T.TERRITORY_GROUP FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P WHERE P.TERRITORY_KEY=T.TERRITORY_KEY AND T.TERRITORY_NAME IN ("
			+ tNames + ") AND T.TERRITORY_TYPE = 'WIRECENTER'";

	    }

	    final RandomColor color = new RandomColor();
	    try {
		list = jdbcTemplate.query(sql, new RowMapper() {

		    public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			Polygon polygon = new Polygon();
			polygon.getTerritory().setKey(rs.getInt(1));
			polygon.getTerritory().setType(rs.getString(4));
			polygon.setType(rs.getString(15));
			String count = ", Leads Count : ";
			if (StringUtils.equalsIgnoreCase(rs.getString(4), "ZIP"))
			    count = count + zipCount.get("'" + rs.getString(2) + "'");
			if (StringUtils.equalsIgnoreCase(rs.getString(4), "WIRECENTER"))
			    count = count + wcCount.get("'" + rs.getString(2) + "'");
			if (StringUtils.equalsIgnoreCase(rs.getString(4), "DA")) {
			    count = count + daCount.get("'" + rs.getString(2) + "-" + rs.getString("TERRITORY_GROUP") + "'");
			    if (clientKey.startsWith("verizon")) {
				polygon.getTerritory().setType("CRR");
				polygon.setType("CRR");
			    }
			}
			polygon.getTerritory().setName(rs.getString(2) + count);
			polygon.getTerritory().setDescription(rs.getString(3));
			if (clientKey.startsWith("verizon")) {
			    polygon.getTerritory().setDescription(rs.getString(3).replace(":DA", ":CRR"));
			}
			polygon.getTerritory().setStateId(rs.getString(5));
			polygon.getTerritory().setStateName(rs.getString(6));
			polygon.getTerritory().setNumberOfBusinesses(rs.getInt(7));
			polygon.getTerritory().setNumberOfOrders(rs.getInt(8));
			polygon.getTerritory().setActive(rs.getBoolean(9));
			polygon.getTerritory().setCreatedDate(rs.getDate(10));
			polygon.getTerritory().setCreatedUser(rs.getString(11));
			polygon.getTerritory().setModifiedDate(rs.getDate(12));
			polygon.getTerritory().setModifiedUser(rs.getString(13));

			polygon.setKey(rs.getInt(14));

			polygon.setCoordinates(rs.getString(16));
			polygon.setActive(rs.getBoolean(17));
			polygon.setColor(color.randomHex());
			polygon.setFill(rs.getBoolean(19));
			// polygon.setOpacity(rs.getString(20));
			polygon.setOutline(rs.getBoolean(21));
			polygon.setLineColor(rs.getString(22));
			polygon.setLineWidth(rs.getInt(23));
			polygon.setLineOpacity(rs.getString(24));
			polygon.setCreatedDate(rs.getDate(25));
			polygon.setCreatedUser(rs.getString(26));
			polygon.setModifiedDate(rs.getDate(27));
			polygon.setModifiedUser(rs.getString(28));
			return polygon;
		    }
		});
	    } catch (Exception e) {
		logger.debug(e);
		logger.error(e);
	    }
	}
	return list;
    }

    /**
     * @param campaignSeq
     * @param officeSeq
     * @return
     */
    public List<Polygon> getBoundariesByCampaignAndOffice(String campaignSeq, String officeSeq) {
	if (campaignSeq.equalsIgnoreCase("-1") || officeSeq.equalsIgnoreCase("-1"))
	    return null;

	List<String> types = new ArrayList<String>();

	List paramList = new ArrayList();
	paramList.add(officeSeq.replaceAll("'", ""));
	paramList.add(campaignSeq.replaceAll("'", ""));
	paramList.toArray(new Object[0]);

	List<Polygon> list = null;
	try {
	    list = jdbcTemplate.query(
		    "SELECT  T.TERRITORY_KEY,T.TERRITORY_NAME,T.TERRITORY_DESCRIPTION,T.TERRITORY_TYPE,T.STATE_ID,T.STATE_NAME,T.NUMBER_OF_BUSINESSES,"
			    + "T.NUMBER_OF_ORDERS,T.IS_ACTIVE,T.CREATED_DATE,T.CREATED_BY,T.MODIFIED_DATE,T.MODIFIED_BY,"
			    + "P.POLYGON_KEY,P.POLYGON_TYPE,P.BOUNDARY_COORDINATES.ToString(),P.IS_ACTIVE,P.COLOR,"
			    + "P.FILL,P.OPACITY,P.OUTLINE,P.LINE_COLOR,P.LINE_WIDTH,P.LINE_OPACITY,P.CREATED_DATE,P.CREATED_BY," + "P.MODIFIED_DATE,"
			    + "P.MODIFIED_BY FROM IMS.IMS_TERRITORY T, IMS.IMS_POLYGON P, "
			    + "IMS.IMS_ICLLEAD_TERRITORY_DEF I,IMS.IMS_ICLLEAD_TERRITORY_MAP MAP where I.LEAD_TERRITORY_DEF_SEQ=MAP.LEAD_TERRITORY_DEF_SEQ "
			    + "AND T.TERRITORY_KEY=MAP.TERRITORY_KEY AND I.OFFICE_SEQ=? AND I.CAMPAIGN_SEQ=? AND P.TERRITORY_KEY = T.TERRITORY_KEY",
		    paramList.toArray(), new RowMapper() {

			public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			    final Polygon polygon = new Polygon();
			    polygon.getTerritory().setKey(rs.getInt(1));
			    polygon.getTerritory().setName(rs.getString(2));
			    polygon.getTerritory().setDescription(rs.getString(3));
			    polygon.getTerritory().setType(rs.getString(4));
			    polygon.getTerritory().setStateId(rs.getString(5));
			    polygon.getTerritory().setStateName(rs.getString(6));
			    polygon.getTerritory().setNumberOfBusinesses(rs.getInt(7));
			    polygon.getTerritory().setNumberOfOrders(rs.getInt(8));
			    polygon.getTerritory().setActive(rs.getBoolean(9));
			    polygon.getTerritory().setCreatedDate(rs.getDate(10));
			    polygon.getTerritory().setCreatedUser(rs.getString(11));
			    polygon.getTerritory().setModifiedDate(rs.getDate(12));
			    polygon.getTerritory().setModifiedUser(rs.getString(13));

			    polygon.setKey(rs.getInt(14));
			    polygon.setType(rs.getString(15));
			    polygon.setCoordinates(rs.getString(16));
			    polygon.setActive(rs.getBoolean(17));
			    polygon.setColor(rs.getString(18));
			    polygon.setFill(rs.getBoolean(19));
			    polygon.setOpacity(rs.getString(20));
			    polygon.setOutline(rs.getBoolean(21));
			    polygon.setLineColor(rs.getString(22));
			    polygon.setLineWidth(rs.getInt(23));
			    polygon.setLineOpacity(rs.getString(24));
			    polygon.setCreatedDate(rs.getDate(25));
			    polygon.setCreatedUser(rs.getString(26));
			    polygon.setModifiedDate(rs.getDate(27));
			    polygon.setModifiedUser(rs.getString(28));
			    return polygon;
			}
		    });
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    @SuppressWarnings("unchecked")
    public List<LeadMaster> selectUnAssingedICLLeads(String campaignId) {
	List<LeadMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_ICL_LEADS"), new Object[] { campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    // to get AccessLinks by RoleName from CRM (FOR HOME PAGE LINKS)
    public static List<ListOrderedMap> getAccessLinksByRoleName() throws Exception {
	String sql = QueryUtils.getQuery().get("SELECT_ACCESS_LINKS_BY_ROLE_NAME");
	try {
	    sql = FreeMarkerEngine.getInstance().evaluateString(sql);
	} catch (Exception e) {
	    logger.error(e);
	    throw e;
	}
	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
	String clientkey=CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("clientKey");
	logger.debug("DBUtil.java:clientkey->"+clientkey);
	JdbcTemplate jdbcTemplate = (JdbcTemplate) context.getBean
			(CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("clientKey")
		+ "JdbcTemplate");

	return jdbcTemplate.queryForList(sql);
    }

    // to get AccessLinks by RoleName from CRM (FOR HOME PAGE LINKS)
    public static List<ListOrderedMap> getAllAccessLinks() {
	String sql = QueryUtils.getQuery().get("SELECT_ALL_ACCESS_LINKS");
	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
	String clientkey=CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("clientKey");
	logger.debug("DBUtil.java:clientkey->"+clientkey);
//	JdbcTemplate jdbcTemplate = (JdbcTemplate) context.getBean
//			(CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("clientKey")
//		+ "JdbcTemplate");

	JdbcTemplate jdbcTemplate = null;
	if (StringUtils.isBlank(clientkey)) {
		jdbcTemplate = (JdbcTemplate) context.getBean
				("JdbcTemplate");			
	}
	else
	{
		jdbcTemplate = (JdbcTemplate) context.getBean
				(CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("clientKey")
			+ "JdbcTemplate");
	}
	

	return jdbcTemplate.queryForList(sql);
    }

    @SuppressWarnings("unchecked")
    public List findOfficeLeadPoint(String point, long campaignSeq) {
	List list = null;
	try {
	    list = jdbcTemplate.queryForList(QueryUtils.getQuery().get("SELECT_OFFICE_LEADPOINT"), new Object[] { point, campaignSeq });
	} catch (Exception e) {
	    logger.error(e);
	    logger.debug(e);
	}
	return list;
    }

   /* public static void main(String[] args) {
	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
	JdbcTemplate jdbcTemplate = (JdbcTemplate) context.getBean("JdbcTemplate");
	PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator.getService("TxManager");
	DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);
	dbUtil.getBoundariesForLeadSheets("29958807", "28481045");

    }*/

    public List<Polygon> getBoundariesForLeadSheets(String campaignSeq, String officeSeq) {
	if (campaignSeq.equalsIgnoreCase("-1") || officeSeq.equalsIgnoreCase("-1"))
	    return null;
	List<Polygon> polygons = new ArrayList<Polygon>();
	List<ListOrderedMap> list = jdbcTemplate
		.queryForList("SELECT LEADSHEET_ID,COUNT(LEAD_ID) AS LEAD_COUNT FROM IMS.IMS_TEMP_LEADSHEET WHERE CAMPAIGN_SEQ=" + campaignSeq
			+ " AND OFFICE_SEQ=" + officeSeq + " GROUP BY LEADSHEET_ID");
	Polygon polygon = null;
	RandomColor color = new RandomColor();
	for (final ListOrderedMap id : list) {
	    try {
		polygon = new Polygon();
		List<SqlParameter> parameters = new ArrayList<SqlParameter>();
		parameters.add(new SqlParameter("@leadSheetId", Types.VARCHAR));
		parameters.add(new SqlOutParameter("@customPolygon", Types.LONGVARCHAR));

		Map map = jdbcTemplate.call(new CallableStatementCreator() {

		    public CallableStatement createCallableStatement(Connection con) throws SQLException {
			CallableStatement statement = con.prepareCall("{call dbo.GetLeadSheetPolygon(?, ?)}");
			statement.setString(1, (String) id.get("LEADSHEET_ID"));
			statement.registerOutParameter(2, Types.LONGVARCHAR);
			return statement;
		    }
		}, parameters);
		Clob clob = (Clob) map.get("@customPolygon");
		if (clob != null) {

		    String coOrd = clob.getSubString(1, new Integer("" + clob.length()));
		    polygon.getTerritory().setName((String) id.get("LEADSHEET_ID") + " :LeadCount :" + id.get("LEAD_COUNT"));
		    polygon.getTerritory().setDescription("TerritoryType:LeadSheet");
		    polygon.setCoordinates(coOrd);
		    polygon.setColor(color.randomHex());
		    polygon.setType("TerritoryType:LeadSheet");
		    polygons.add(polygon);
		}
	    } catch (NumberFormatException e1) {
		e1.printStackTrace();
	    } catch (SQLException e1) {
		e1.printStackTrace();
	    }

	}
	return polygons;
    }

    /**
     * @param campaignSeq
     * @param officeSeq
     * @param leadSheetId
     * @return
     */
    public List<Polygon> getBoundariesForLeadSheets(String campaignSeq, String officeSeq, String leadSheetId) {
	if (campaignSeq.equalsIgnoreCase("-1") || officeSeq.equalsIgnoreCase("-1"))
	    return null;

	List<Polygon> polygons = new ArrayList<Polygon>();
	List<ListOrderedMap> list = jdbcTemplate
		.queryForList("SELECT LEADSHEET_ID,COUNT(LEAD_ID) AS LEAD_COUNT FROM IMS.IMS_TEMP_LEADSHEET WHERE CAMPAIGN_SEQ=" + campaignSeq
			+ " AND OFFICE_SEQ=" + officeSeq + " AND LEADSHEET_ID='" + leadSheetId + "' GROUP BY LEADSHEET_ID");
	Polygon polygon = null;
	RandomColor color = new RandomColor();
	for (final ListOrderedMap id : list) {
	    try {
		polygon = new Polygon();
		List<SqlParameter> parameters = new ArrayList<SqlParameter>();
		parameters.add(new SqlParameter("@leadSheetId", Types.VARCHAR));
		parameters.add(new SqlOutParameter("@customPolygon", Types.LONGVARCHAR));

		Map map = jdbcTemplate.call(new CallableStatementCreator() {

		    public CallableStatement createCallableStatement(Connection con) throws SQLException {
			CallableStatement statement = con.prepareCall("{call dbo.GetLeadSheetPolygon(?, ?)}");
			statement.setString(1, (String) id.get("LEADSHEET_ID"));
			statement.registerOutParameter(2, Types.LONGVARCHAR);
			return statement;
		    }
		}, parameters);
		Clob clob = (Clob) map.get("@customPolygon");
		if (clob != null) {

		    String coOrd = clob.getSubString(1, new Integer("" + clob.length()));
		    polygon.getTerritory().setName((String) id.get("LEADSHEET_ID") + " :LeadCount :" + id.get("LEAD_COUNT"));
		    polygon.getTerritory().setDescription("TerritoryType:LeadSheet");
		    polygon.setCoordinates(coOrd);
		    polygon.setColor(color.randomHex());
		    polygon.setType("TerritoryType:LeadSheet");
		    polygons.add(polygon);
		}
	    } catch (NumberFormatException e1) {
		e1.printStackTrace();
	    } catch (SQLException e1) {
		e1.printStackTrace();
	    }

	}
	return polygons;
    }
    
    /**
     * @param leadSheetId
     * @return
     */
    public Polygon getBoundaryForSavedLeadSheet(String leadSheetId) {
	
		Polygon polygon = null;
		if(leadSheetId != null ){
			RandomColor color = new RandomColor();
			final String lsId = leadSheetId;
				try {
					polygon = new Polygon();
					List<SqlParameter> parameters = new ArrayList<SqlParameter>();
					parameters.add(new SqlParameter("@leadSheetIds", Types.VARCHAR));
					parameters.add(new SqlOutParameter("@customPolygon", Types.LONGVARCHAR));
		
					Map map = jdbcTemplate.call(new CallableStatementCreator() {
		
						public CallableStatement createCallableStatement(Connection con) throws SQLException {
							CallableStatement statement = con.prepareCall("{call dbo.GetSavedLeadSheetPolygon(?, ?)}");
							statement.setString(1, lsId);
							statement.registerOutParameter(2, Types.LONGVARCHAR);
							return statement;
						}
					}, parameters);
					Clob clob = (Clob) map.get("@customPolygon");
					if (clob != null) {
		
						String coOrd = clob.getSubString(1, new Integer("" + clob.length()));
						polygon.getTerritory().setName(lsId);
						polygon.getTerritory().setDescription("TerritoryType:LeadSheet");
						polygon.setCoordinates(coOrd);
						polygon.setColor(color.randomHex());
						polygon.setType("TerritoryType:LeadSheet");
					}
				} catch (NumberFormatException e1) {
					logger.error(e1);
				} catch (SQLException e1) {
					logger.error(e1);
				}
		}
		return polygon;
    }

    public String generateTerritoryKeyForLeadsheet(String leadSheetId, String drawnPolygonCoords) {
	Integer key = null;
	if (StringUtils.isNotBlank(drawnPolygonCoords)) {
	    java.util.Date now = new java.util.Date();
	    Territory territory = new Territory();
	    territory.setName("DRAWN_POLYGON");
	    territory.setActive(true);
	    territory.setType("CUSTOM");
	    territory.setDescription("TerritoryType:CUSTOM_POLYGON");
	    territory.setCreatedDate(now);
	    territory.setModifiedDate(now);
	    String color = new RandomColor().randomHex();
	    key = insertTerritory(territory);

	    territory.setKey(key);
	    Polygon polygon = new Polygon();
	    polygon.setCoordinates(drawnPolygonCoords);
	    polygon.setColor(color);
	    // polygon.setFill(new
	    // Boolean(req.getParameter("styleFill").trim()));
	    polygon.setOutline(true);
	    polygon.setOpacity("5");
	    polygon.setLineColor(color);
	    polygon.setLineOpacity("1");

	    polygon.setCreatedDate(now);
	    polygon.setCreatedUser(""/* req.getUserPrincipal().getName() */);
	    polygon.setModifiedDate(now);
	    polygon.setModifiedUser(""/* req.getUserPrincipal().getName() */);

	    polygon.getTerritory().setKey(key);
	    polygon.setTerritory(territory);
	    List<Polygon> polygons = new ArrayList<Polygon>();
	    polygons.add(polygon);
	    insertPolygons(polygons);

	}
	return key + "";
    }

    public void insertLeadSheets(final TempLeadSheet tempLeadSheet, final String territoryKey) {

	jdbcTemplate.batchUpdate(QueryUtils.getQuery().get("INSERT_LEAD_SHEETS"), new BatchPreparedStatementSetter() {
	    java.util.Date date = new java.util.Date();
	    Timestamp cDate = new Timestamp(date.getTime());

	    public void setValues(PreparedStatement ps, int i) throws SQLException {
		if (tempLeadSheet.getLeadIds().get(i).equals("Y")) {
		    insertLSValues(ps, tempLeadSheet.getLeadSheetId(), tempLeadSheet.getLeadSheetName(), tempLeadSheet.getLeadIds().get(i), tempLeadSheet
			    .getRowIds().get(i), tempLeadSheet.getGeoPoints().get(i), tempLeadSheet.getCampaignSeq(), tempLeadSheet.getOfficeSeq(),
			    territoryKey);
		} 
		else {
		    insertLSValues(ps, tempLeadSheet.getLeadSheetId(), tempLeadSheet.getLeadSheetName(), tempLeadSheet.getLeadIds().get(i), tempLeadSheet
			    .getRowIds().get(i), null, tempLeadSheet.getCampaignSeq(), tempLeadSheet.getOfficeSeq(), territoryKey);
		}
	    }

	    private void insertLSValues(PreparedStatement ps, String leadSheetId, String leadSheetName, String leadId, Long rowId, String geoPoint,
		    String campaignSeq, String officeSeq, String territoryKey) throws SQLException {
		ps.setString(1, leadSheetId);
		ps.setString(2, leadSheetName);
		ps.setLong(3, rowId);
		ps.setString(4, leadId);
		ps.setTimestamp(5, cDate);
		ps.setLong(6, new Long(campaignSeq));
		ps.setLong(7, new Long(officeSeq));
		ps.setString(8, geoPoint);
		ps.setString(9, territoryKey);
	    }
	    
	    public int getBatchSize() {
		return tempLeadSheet.getLeadIds().size();
	    }
	});
    }
    //code added by Sanjay
    public void insertLeadSheets(final TempLeadSheet tempLeadSheet) {

	jdbcTemplate.batchUpdate(QueryUtils.getQuery().get("INSERT_LEAD_SHEETS"), new BatchPreparedStatementSetter() {
	    java.util.Date date = new java.util.Date();
	    Timestamp cDate = new Timestamp(date.getTime());

	    public void setValues(PreparedStatement ps, int i) throws SQLException {
	    	try{
	    	if (tempLeadSheet.getLeadIds().get(i).equals("Y")) {
	    		insertLSValues(ps, tempLeadSheet.getLeadSheetId(), tempLeadSheet.getLeadSheetName(), tempLeadSheet
	    				.getRowIds().get(i), tempLeadSheet.getLeadIds().get(i), tempLeadSheet.getGeoPoints().get(i), tempLeadSheet.getCampaignSeq(), tempLeadSheet.getOfficeSeq(), null);
	    	} else {
	    		insertLSValues(ps, tempLeadSheet.getLeadSheetId(), tempLeadSheet.getLeadSheetName(), tempLeadSheet
	    				.getRowIds().get(i), tempLeadSheet.getLeadIds().get(i), null, tempLeadSheet.getCampaignSeq(), tempLeadSheet.getOfficeSeq(), null);
	    	}}
	    	catch(Exception e)
	    	{
	    		System.out.println(e);
	    	}
	    	
	    }

	    private void insertLSValues(PreparedStatement ps, String leadSheetId, String leadSheetName,Long rowId, String leadId,String geoPoint,
		    String campaignSeq, String officeSeq, String territoryKey) throws SQLException {
		ps.setString(1, leadSheetId);
		ps.setString(2, leadSheetName);
		ps.setLong(3, rowId);
		ps.setString(4, leadId);
		ps.setTimestamp(5, cDate);
		ps.setLong(6, new Long(campaignSeq));
		ps.setLong(7, new Long(officeSeq));
		ps.setString(8, geoPoint);
		ps.setString(9, territoryKey);
	    }

	    public int getBatchSize() {
		return tempLeadSheet.getLeadIds().size();
	    }
	});
    }
    //code added by Sanjay
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Object[] getLookupOffers(String columnName, String campaignId) {
	Object[] returnObject = new Object[2];
	List<ListOrderedMap> list = jdbcTemplate.queryForList(
		"SELECT OFFER_LOOKUP_COLUMN, LS_TEMPLATE_COMPARE_VALUE FROM IMS.IMS_PRODUCTS WHERE PRODUCT_COLUMN_NAME=? and CAMPAIGN_SEQ=?", new Object[] {
			columnName, campaignId });
	if (list != null && !list.isEmpty()) {
	    Map map = list.get(0);
	    returnObject[0] = map.get("LS_TEMPLATE_COMPARE_VALUE");
	    if (map.get("OFFER_LOOKUP_COLUMN") != null && !"".equalsIgnoreCase(map.get("OFFER_LOOKUP_COLUMN").toString().trim())) {
		List<ListOrderedMap> values = jdbcTemplate.queryForList("SELECT OFFER_VALUE FROM IMS.IMS_OFFER_LOOKUP_VALUES WHERE "
			+ map.get("OFFER_LOOKUP_COLUMN").toString().trim() + "='Y'");
		if (values != null && !values.isEmpty()) {
		    String[] returnValues = new String[values.size()];
		    int i = 0;
		    for (ListOrderedMap map2 : values) {
			returnValues[i++] = "'" + (String) map2.get("OFFER_VALUE") + "'";
		    }
		    returnObject[1] = returnValues;
		}
	    }
	}
	return returnObject;
    }

    public List<LeadMaster> selectUnAssingedLeadsByProductsUsingFiles(String campaignId, String officeSeq, String selectedFiles, String products,
	    String useWirecenters) {

	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PRODUCTS");
	try {
	    if (StringUtils.isNotBlank(products)) {
		String[] spiltSql = sql.split(" ORDER BY ");
		String[] columns = products.split(":");
		sql = spiltSql[0];
		for (int i = 1; i < columns.length; i++) {
		    Object[] values = getLookupOffers(columns[i], campaignId);
		    if (values[1] == null) {
			sql = sql + " AND " + columns[i] + " = '"
				+ ((values[0] != null && !"".equals(values[0].toString().trim())) ? values[0].toString().trim() : "Y") + "' ";
		    } else {
			sql = sql + " AND OFFER IN (" + StringUtils.join((Object[]) values[1], ", ") + ") ";
		    }
		}
		sql = sql + " ORDER BY " + spiltSql[1];
		String[] fromSplitSql = sql.split(" FROM ");
		sql = fromSplitSql[0];
		for (int i = 1; i < columns.length; i++) {
		    sql = sql + ", " + columns[i];
		}
		for (int i = 1; i < fromSplitSql.length; i++)
		    sql = sql + " FROM " + fromSplitSql[i];

	    }

	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql2 = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql2[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }

	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByTVNewUsingFiles(String campaignId, String officeSeq, String selectedFiles, String products,
	    String useWirecenters) {

	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_TVNEW");
	try {
	    if (StringUtils.isNotBlank(products)) {
		String[] spiltSql = sql.split(" ORDER BY ");
		String[] columns = products.split(":");
		String[] values = new String[columns.length-1];
		for(int i=1;i< columns.length;i++){
		    values[i-1] = "'"+columns[i]+"'";
		}
		sql = spiltSql[0];
			sql = sql + " AND VideoGATenure IN (" + StringUtils.join(values, ", ") + ") ";
		sql = sql + " ORDER BY " + spiltSql[1];
	    }

	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }

	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }
    public List<LeadMaster> centurylink_selectUnAssingedLeadsBySpeedUsingFiles(String campaignId
    		, String officeSeq
    		, String selectedFiles
    		, String products,
    	    String useWirecenters
    	    ,String SpeedTerritory
    		) {

    	logger.debug(">>centurylink_selectUnAssingedLeadsBySpeedUsingFiles");
    	
    	logger.debug("selectedFiles:"+selectedFiles);
    	logger.debug("products:"+products);
    	logger.debug("useWirecenters:"+useWirecenters);
    	logger.debug("SpeedTerritory:"+SpeedTerritory);
    	
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_SPEED");
    	try {
    	    if (StringUtils.isNotBlank(products)) {
    		String[] spiltSql = sql.split(" ORDER BY ");
    		String[] columns = products.split(":");
    		String[] values = new String[columns.length-1];
    		for(int i=1;i< columns.length;i++){
    		    values[i-1] = "'"+columns[i]+"'";
    		}
    		sql = spiltSql[0];
    			sql = sql + " AND hsi_avial IN (" + StringUtils.join(values, ", ") + ") ";
    			

    			 if(SpeedTerritory!=null)
    				{
    	    	    	String colname=null;
    					if(SpeedTerritory.equalsIgnoreCase("byDA"))
    					{
    						colname="da";
    					}
    					else if(SpeedTerritory.equalsIgnoreCase("byWireCenter"))
    					{
    						colname="clli";
    					}
    					else if(SpeedTerritory.equalsIgnoreCase("byZip"))
    					{
    						colname="zip";
    					}
    					if(colname!=null)
    					{
    					 
    					 sql = sql + " AND "+colname+"  is not null ";
    					 logger.debug("sql after:"+sql);
    					}
    				}
    			 
    		sql = sql + " ORDER BY " + spiltSql[1];
    	    }

    	    
    	   
    	    
    	    if (StringUtils.isNotBlank(selectedFiles)) {
    		if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		}
    	    }

    	    logger.debug("Unassigned leads by Speed query:"+sql);
    	    
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
        }
    
    /**
     * This method is added for CenturyLink Prism Vegas campaign.
     * @param campaignId
     * @param officeSeq
     * @param selectedFiles
     * @param products
     * @param useWirecenters
     * @param SpeedTerritory
     * @return
     */
    public List<LeadMaster> centurylink_Prism_Vegas_selectUnAssingedLeadsBySpeedUsingFiles(String campaignId
    		, String officeSeq
    		, String selectedFiles
    		, String products,
    	    String useWirecenters
    	    ,String SpeedTerritory
    		) {

    	logger.debug(">>centurylink_selectUnAssingedLeadsBySpeedUsingFiles");
    	
    	logger.debug("selectedFiles:"+selectedFiles);
    	logger.debug("products:"+products);
    	logger.debug("useWirecenters:"+useWirecenters);
    	logger.debug("SpeedTerritory:"+SpeedTerritory);
    	
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_SPEED_PRISM_VEGAS");
    	try {
    	    if (StringUtils.isNotBlank(products)) {
    	    	String[] spiltSql = sql.split(" ORDER BY ");
    	    	String[] columns = products.split(":");
    	    	String[] values = new String[columns.length-1];
    	    	for(int i=1;i< columns.length;i++){
    	    		values[i-1] = "'"+columns[i]+"'";
    	    	}
    	    	sql = spiltSql[0];
    	    	sql = sql + " AND DSL_SPEED_AVAIL IN (" + StringUtils.join(values, ", ") + ") ";
    			
	    		if(SpeedTerritory!=null)
	    		{
	    			String colname=null;
	    			if(SpeedTerritory.equalsIgnoreCase("byZip"))
	    			{
	    				colname="zip";
	    			}
	    			if(colname!=null)
	    			{
	    				sql = sql + " AND "+colname+"  is not null ";
	    				logger.debug("sql after:"+sql);
	    			}
	    		}
    			 
	    		sql = sql + " ORDER BY " + spiltSql[1];
    	    }
    	    
    	    
    	    if (StringUtils.isNotBlank(selectedFiles)) {
	    		if ("ZIP".equalsIgnoreCase(useWirecenters)) {
	    		    String[] spiltSql = sql.split(" ORDER BY ");
	    		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
	    			String[] spiltSql = sql.split(" ORDER BY ");
	    			sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
	    			String[] spiltSql = sql.split(" ORDER BY ");
	    			sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		} else {
	    			String[] spiltSql = sql.split(" ORDER BY ");
	    			sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		}
    	    }

    	    logger.debug("Unassigned leads by Speed query:"+sql);
    	    
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }

    /**
     * This method is added for CenturyLink Prism Vegas campaign. It fetches unassigned leads by Leadsheet id.
     * @param campaignId
     * @param officeSeq
     * @param selectedFiles
     * @param products
     * @param useWirecenters
     * @param SpeedTerritory
     * @return
     */
    public List<LeadMaster> centurylink_Prism_Vegas_selectUnAssingedLeadsBySpeedUsingSheet(String campaignId
    		, String officeSeq
    		, String products
    	    ,String SpeedTerritory
    	    , String leadSheetIdGenerated
    		) {

    	logger.debug(">>centurylink_selectUnAssingedLeadsBySpeedUsingFiles");
    	
    	logger.debug("leadSheetIdGenerated:"+leadSheetIdGenerated);
    	logger.debug("products:"+products);
    	logger.debug("SpeedTerritory:"+SpeedTerritory);
    	
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_SPEED_SHEET_PRISM_VEGAS");
    	try {
    	    if (StringUtils.isNotBlank(products)) {
    	    	String[] spiltSql = sql.split(" ORDER BY ");
    	    	String[] columns = products.split(":");
    	    	String[] values = new String[columns.length-1];
    	    	for(int i=1;i< columns.length;i++){
    	    		values[i-1] = "'"+columns[i]+"'";
    	    	}
    	    	sql = spiltSql[0];
    	    	sql = sql + " AND DSL_SPEED_AVAIL IN (" + StringUtils.join(values, ", ") + ") ";
    			
	    		if(SpeedTerritory!=null)
	    		{
	    			String colname=null;
	    			if(SpeedTerritory.equalsIgnoreCase("byZip"))
	    			{
	    				colname="zip";
	    			}
	    			if(colname!=null)
	    			{
	    				sql = sql + " AND "+colname+"  is not null ";
	    				logger.debug("sql after:"+sql);
	    			}
	    		}
    			 
	    		sql = sql + " ORDER BY " + spiltSql[1];
    	    }
    	    
    	    
    	   /* if (StringUtils.isNotBlank(selectedFiles)) {
	    		if ("ZIP".equalsIgnoreCase(useWirecenters)) {
	    		    String[] spiltSql = sql.split(" ORDER BY ");
	    		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
	    			String[] spiltSql = sql.split(" ORDER BY ");
	    			sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
	    			String[] spiltSql = sql.split(" ORDER BY ");
	    			sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		} else {
	    			String[] spiltSql = sql.split(" ORDER BY ");
	    			sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		}
    	    }
    	    */

    	    logger.debug("Unassigned leads by Speed query:"+sql);
    	    
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId,leadSheetIdGenerated }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }
    
    public List<LeadMaster> selectUnAssingedLeadsByNewLeadsUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_NEW_LEADS");
	try {

	    if (StringUtils.isNotBlank(selectedFiles)) {
		if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByProducts(String campaignId, String officeSeq, String products, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PRODUCTS_FROM_LEADSHEET");
	try {
	    if (StringUtils.isNotBlank(products)) {
		String[] spiltSql = sql.split(" ORDER BY ");
		String[] columns = products.split(":");
		sql = spiltSql[0];
		for (int i = 1; i < columns.length; i++) {
		    Object[] values = getLookupOffers(columns[i], campaignId);
		    if (values[1] == null) {
			sql = sql + " AND " + columns[i] + " = '"
				+ ((values[0] != null && !"".equals(values[0].toString().trim())) ? values[0].toString().trim() : "Y") + "' ";
		    } else {
			sql = sql + " AND OFFER IN (" + StringUtils.join((Object[]) values[1], ", ") + ") ";
		    }
		}
		sql = sql + " ORDER BY " + spiltSql[1];

		String[] fromSplitSql = sql.split(" FROM ");
		sql = fromSplitSql[0];
		for (int i = 1; i < columns.length; i++) {
		    sql = sql + ", " + columns[i];
		}
		for (int i = 1; i < fromSplitSql.length; i++)
		    sql = sql + " FROM " + fromSplitSql[i];

	    }

	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByTVNew(String campaignId, String officeSeq, String products, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_TVNEW_FROM_LEADSHEET");
	try {
	    if (StringUtils.isNotBlank(products)) {
		String[] spiltSql = sql.split(" ORDER BY ");
		String[] columns = products.split(":");
		String[] values = new String[columns.length-1];
		for(int i=1;i< columns.length;i++){
		    values[i-1] = "'"+columns[i]+"'";
		}
		sql = spiltSql[0];
			sql = sql + " AND VideoGATenure IN (" + StringUtils.join(values, ", ") + ") ";
		sql = sql + " ORDER BY " + spiltSql[1];
	    }

	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByNewLeadsUsingFilesAndSheet(String campaignId, String officeSeq, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_NEW_LEADS_FROM_LEADSHEET");
	try {
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByLFAUsingFiles(String campaignId, String officeSeq, String selectedFiles, String LFA, String useWirecenters) {
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_LFA");
	LFA = "'" + LFA.replaceAll(",", "', '") + "'";
	try {
	    String[] spiltSql = sql.split(" ORDER BY ");
	    sql = spiltSql[0] + " AND LFA_CODE IN (" + LFA + ") ORDER BY " + spiltSql[1];

	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql2 = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql2[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }
    
    /**
     * Task Id : 2013116
     * Method added for CLuster YN template
     * @param campaignId
     * @param officeSeq
     * @param selectedFiles
     * @param custerYNValue
     * @param useWirecenters
     * @return
     */
    public List<LeadMaster> selectUnAssingedLeadsByClusterYNUsingFiles(String campaignId, String officeSeq, String selectedFiles, String custerYNValue,
    	    String useWirecenters) {
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_CLUSTER_YN");
    	custerYNValue = "'" + custerYNValue.replaceAll(",", "', '") + "'";
    	try {
    	    String[] spiltSql = sql.split(" ORDER BY ");
    	    sql = spiltSql[0] + " AND CLUSTER_LEAD IN (" + custerYNValue + ") ORDER BY " + spiltSql[1];

    	    if (StringUtils.isNotBlank(selectedFiles)) {
    			if(selectedFiles.contains("|")){
    				String[] combinationFiles = selectedFiles.split(",");
    				String[] spiltSql2 = sql.split(" ORDER BY ");
    				String[] territories = useWirecenters.split("\\|");
    				territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
    				territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
    				sql = spiltSql2[0] + " AND (";
    				for(String fileCombination: combinationFiles){				
    					sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
    				}
    				sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
    			}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    	    		spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    	    		spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    	    		spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    	    		spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else {
    	    		spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	}
    	    }

    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }
    
    /**
     * Task Id : 2013116
     * Method added for iONT template
     * @param campaignId
     * @param officeSeq
     * @param selectedFiles
     * @param custerYNValue
     * @param useWirecenters
     * @return
     */
    public List<LeadMaster> selectUnAssingedLeadsByIontUsingFiles(String campaignId, String officeSeq, String selectedFiles, String iOntValue,
	    String useWirecenters) {
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_IONT");
    	try {
    		if (StringUtils.isNotBlank(selectedFiles)) {
    			if(selectedFiles.contains("|")){
    				String[] combinationFiles = selectedFiles.split(",");
    				String[] spiltSql2 = sql.split(" ORDER BY ");
    				String[] territories = useWirecenters.split("\\|");
    				territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
    				territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
    				sql = spiltSql2[0] + " AND (";
    				for(String fileCombination: combinationFiles){				
    					sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
    				}
    				sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
    			}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    				String[] spiltSql = sql.split(" ORDER BY ");
    				sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    			} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    				String[] spiltSql = sql.split(" ORDER BY ");
    				sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    			} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    				String[] spiltSql = sql.split(" ORDER BY ");
    				sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    			} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    				String[] spiltSql = sql.split(" ORDER BY ");
    				sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    			} else {
    				String[] spiltSql = sql.split(" ORDER BY ");
    				sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    			}
    		}

    		list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, iOntValue }, new LeadMasterMapper());
    	} catch (Exception e) {
    		logger.debug(e);
    		logger.error(e);
    	}
    	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByPropIDUsingFiles(String campaignId, String officeSeq, String selectedFiles, String propId,
	    String useWirecenters) {
	List<LeadMaster> list = null;
	propId = "'" + propId.replaceAll(",", "', '") + "'";
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PROP_ID");
	try {
	    String[] spiltSql = sql.split(" ORDER BY ");
	    sql = spiltSql[0] + " AND PROPERTYID IN (" + propId + ") ORDER BY " + spiltSql[1];

	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql2 = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql2[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }

	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByPromoUsingFiles(String campaignId, String officeSeq, String selectedFiles, String Promo,
	    String useWirecenters) {
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PROMO");
	Promo = "'" + Promo.replaceAll(",", "', '") + "'";
	try {
	    String[] spiltSql = sql.split(" ORDER BY ");
	    sql = spiltSql[0] + " AND PROMO_CODE =" + Promo + " ORDER BY " + spiltSql[1];

	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql2 = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql2[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByLFAUsingFilesAndSheet(String campaignId, String officeSeq, String LFA, String leadSheetIdGenerated) {
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_LFA_FROM_LEADSHEET");
	LFA = "'" + LFA.replaceAll(",", "', '") + "'";
	String[] spiltSql = sql.split(" ORDER BY ");
	sql = spiltSql[0] + " AND LFA_CODE IN (" + LFA + ") ORDER BY " + spiltSql[1];

	try {
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }
    
    /**
     * Task Id  : 2013116
     * Method added for Cluster Y/N template using files and sheet
     * @param campaignId
     * @param officeSeq
     * @param iont
     * @param leadSheetIdGenerated
     * @return
     */
    public List<LeadMaster> selectUnAssingedLeadsByClusterYNUsingFilesAndSheet(String campaignId, String officeSeq, String clusterYN,
	    String leadSheetIdGenerated) {
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_CLUSTER_YN_FROM_LEADSHEET");
    	clusterYN = "'" + clusterYN.replaceAll(",", "', '") + "'";
    	String[] spiltSql = sql.split(" ORDER BY ");
    	sql = spiltSql[0] + " AND CLUSTER_LEAD IN (" + clusterYN + ") ORDER BY " + spiltSql[1];

		try {
		    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, leadSheetIdGenerated }, new LeadMasterMapper());
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
    }
    
    /**
     * Task Id  : 2013116
     * Method added for IONT template using files and sheet
     * @param campaignId
     * @param officeSeq
     * @param iont
     * @param leadSheetIdGenerated
     * @return
     */
    public List<LeadMaster> selectUnAssingedLeadsByIontUsingFilesAndSheet(String campaignId, String officeSeq, String iont,
	    String leadSheetIdGenerated) {
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_IONT_FROM_LEADSHEET");
    	try {
		    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, iont, leadSheetIdGenerated }, new LeadMasterMapper());
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByPropIDUsingFilesAndSheet(String campaignId, String officeSeq, String propID, String leadSheetIdGenerated) {
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PROP_ID_FROM_LEADSHEET");
	propID = "'" + propID.replaceAll(",", "', '") + "'";
	String[] spiltSql = sql.split(" ORDER BY ");
	sql = spiltSql[0] + " AND PROPERTYID IN (" + propID + ") ORDER BY " + spiltSql[1];

	try {
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByPromoUsingFilesAndSheet(String campaignId, String officeSeq, String promo, String leadSheetIdGenerated) {
	List<LeadMaster> list = null;
	promo = "'" + promo.replaceAll(",", "', '") + "'";
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PROMO_FROM_LEADSHEET");
	String[] spiltSql = sql.split(" ORDER BY ");
	sql = spiltSql[0] + " AND PROMO_CODE IN (" + promo + ") ORDER BY " + spiltSql[1];

	try {
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByCityUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_CITY");
	try {
	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql2 = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql2[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByCity(String campaignId, String officeSeq, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	try {
	    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_CITY_FROM_LEADSHEET"), new Object[] { officeSeq, campaignId,
		    leadSheetIdGenerated }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public String generateTempSheetForProximity(String campaignId, String officeSeq, String selectedEntries, String type) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String tempSql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_SELETED_TYPE");
	try {

	    if (StringUtils.isNotBlank(selectedEntries)) {
		if(selectedEntries.contains("|")){
			String[] combinationFiles = selectedEntries.split(",");			
			String[] territories = type.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			tempSql = tempSql + " AND (";
			for(String fileCombination: combinationFiles){				
				tempSql = tempSql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			
			tempSql = tempSql.substring(0, (tempSql.length()-3))+ ") ";
			
		} else if ("CLLI".equalsIgnoreCase(type)) {
		    tempSql = tempSql + " AND CLLI IN (" + selectedEntries + ")";
		} else if ("ZIP".equalsIgnoreCase(type)) {
		    tempSql = tempSql + " AND ZIP IN (" + selectedEntries + ")";
		} else if ("CITY".equalsIgnoreCase(type)) {
		    tempSql = tempSql + " AND CITY IN (" + selectedEntries + ")";
		} else if ("LFA".equalsIgnoreCase(type)) {
		    tempSql = tempSql + " AND LFA_CODE IN (" + selectedEntries + ")";
		} else {
		    tempSql = tempSql + " AND RUN_ID IN (" + selectedEntries + ")";
		}
	    }
	    String sheetId = "TEMPSHEET_" + System.nanoTime();
	    String sql = "SELECT '" + sheetId + "', '" + sheetId + "', ";
	    String[] spiltSql = tempSql.split("SELECT ");
	    for (int i = 0; i < spiltSql.length; i++) {

		if (!spiltSql[i].equalsIgnoreCase("")) {
		    String select = " SELECT ";
		    if (i == 1) {
			select = " ";
		    }

		    sql = sql + select + spiltSql[i];
		}
	    }
	    sql = "INSERT INTO IMS.IMS_TEMP_LEADSHEET(LEADSHEET_ID, LEADSHEET_NAME, ROW_ID, LEAD_ID, CAMPAIGN_SEQ,  OFFICE_SEQ, CREATED_DATE, GEO_POINT) "
		    + sql;
	    jdbcTemplate.update(sql, new Object[] { officeSeq, campaignId });
	    return sheetId;
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return null;
    }

    public List<LeadMaster> selectUnAssingedLeadsByMDUUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	String sql = null;
	//atiwari : Modified for Prism Vegas campaign
	if(campaignId.equals("67976206") || campaignId.equals("91039497")){
		sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_MDU_PRISM_VEGAS");
	}else{
		sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_MDU");
	}
	try {
	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql2 = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql2[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    if("67976206".equals(campaignId) || "91039497".equals(campaignId)){
		    	sql = spiltSql[0] + " AND NODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		    }else{
		    	sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		    }
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    String[] spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByMDU(String campaignId, String officeSeq, String leadSheetIdGenerated) {
	/*
	 * String sql =
	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
	 * + campaignId;
	 */
	List<LeadMaster> list = null;
	try {
		//atiwari : Modified for Prism Vegas campaign.
		if(campaignId.equals("67976206") || campaignId.equals("91039497")){
			list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_MDU_FROM_LEADSHEET_PRISM_VEGAS"), new Object[] { officeSeq, campaignId,
				leadSheetIdGenerated }, new LeadMasterMapper());
		}else{
			list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_MDU_FROM_LEADSHEET"), new Object[] { officeSeq, campaignId,
			    leadSheetIdGenerated }, new LeadMasterMapper());
		}
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }
    
    public List<LeadMaster> selectUnAssingedLeadsByBlockMDUUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_BLOCK_MDU");
    	try {
    	    if (StringUtils.isNotBlank(selectedFiles)) {
			if(selectedFiles.contains("|")){
				String[] combinationFiles = selectedFiles.split(",");
				String[] spiltSql2 = sql.split(" ORDER BY ");
				String[] territories = useWirecenters.split("\\|");
				territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
				territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
				sql = spiltSql2[0] + " AND (";
				for(String fileCombination: combinationFiles){				
					sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
				}
				sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
			}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		} else {
    		    String[] spiltSql = sql.split(" ORDER BY ");
    		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    		}
    	    }
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByBlockMDU(String campaignId, String officeSeq,
    	    String leadSheetIdGenerated) {
        	List<LeadMaster> list = null;
        	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_BLOCK_MDU_FROM_LEADSHEET");
        	try {
    		    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, leadSheetIdGenerated }, new LeadMasterMapper());
    		} catch (Exception e) {
    		    logger.debug(e);
    		    logger.error(e);
    		}
    		return list;
    }    
    
    /**
     * Get unassigned leads by Cluster value.
     * @param campaignId
     * @param officeSeq
     * @param selectedFiles
     * @param Promo
     * @param useWirecenters
     * @return
     */
    public List<LeadMaster> selectUnAssingedLeadsByClusterUsingFiles(String campaignId, String officeSeq, String selectedFiles, String clusterValue,
	    String useWirecenters) {
	List<LeadMaster> list = null;
	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_CLUSTER");
	clusterValue = "'" + clusterValue.replaceAll(",", "', '") + "'";
	try {
	    String[] spiltSql = sql.split(" ORDER BY ");
	    sql = spiltSql[0] + " AND IONT_CLUSTER = " + clusterValue + " ORDER BY " + spiltSql[1];

	    if (StringUtils.isNotBlank(selectedFiles)) {
		if(selectedFiles.contains("|")){
			String[] combinationFiles = selectedFiles.split(",");
			String[] spiltSql2 = sql.split(" ORDER BY ");
			String[] territories = useWirecenters.split("\\|");
			territories[0] = "LFA".equalsIgnoreCase(territories[0])?territories[0]+"_CODE":territories[0];
			territories[1] = "LFA".equalsIgnoreCase(territories[1])?territories[1]+"_CODE":territories[1];
			sql = spiltSql2[0] + " AND (";
			for(String fileCombination: combinationFiles){				
				sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
			}
			sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql2[1];
		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		} else {
		    spiltSql = sql.split(" ORDER BY ");
		    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
		}
	    }
	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
	} catch (Exception e) {
	    logger.debug(e);
	    logger.error(e);
	}
	return list;
    }
    
    /**
     * Gets unassinged leads based on Cluster value : Generate leadsheet through Polygon
     * @param campaignId
     * @param officeSeq
     * @param clusterValue
     * @param leadSheetIdGenerated
     * @return
     */
    public List<LeadMaster> selectUnAssingedLeadsByClusterUsingFilesAndSheet(String campaignId, String officeSeq, String clusterValue, String leadSheetIdGenerated) {
    	List<LeadMaster> list = null;
    	clusterValue = "'" + clusterValue.replaceAll(",", "', '") + "'";
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_CLUSTER_FROM_LEADSHEET");
    	String[] spiltSql = sql.split(" ORDER BY ");
    	sql = spiltSql[0] + " AND IONT_CLUSTER  = " + clusterValue + " ORDER BY " + spiltSql[1];

		try {
		    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId, leadSheetIdGenerated }, new LeadMasterMapper());
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
    }

	public List<Polygon> getLeadSheetPolygons(String campaignSeq,
			String officeSeq, String leadSheetType) {
	
		if (campaignSeq.equalsIgnoreCase("-1") || officeSeq.equalsIgnoreCase("-1"))
		    return null;	
		
		final RandomColor color = new RandomColor();
		try {
		    List<Polygon> list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_LEADSHEET_POLYGONS_"+leadSheetType), new Object[] {campaignSeq, officeSeq}, new RowMapper() {
			public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			    final Polygon polygon = new Polygon();
			    
			    polygon.getTerritory().setName(rs.getString(1) + " :LeadCount :" + rs.getString(2));
			    polygon.getTerritory().setDescription("TerritoryType:LeadSheet");
			    polygon.setCoordinates(rs.getString(3));
			    polygon.setColor(color.randomHex());
			    polygon.setType("TerritoryType:LeadSheet");
			    return polygon;
			}
		    });
			return list;
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return null;
	}
	
	public List<Polygon> getLeadSheetPolygonsForMasterMap(String campaignSeq,
			String officeSeq, String leadSheetType, String leadSheetId,String territoryType) {
	
		if (campaignSeq.equalsIgnoreCase("-1") || officeSeq.equalsIgnoreCase("-1"))
		    return null;	

		final RandomColor color = new RandomColor();
		try {
//			List<String> resultList = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_TERRITORY_TYPE_LEADSHEET_"+leadSheetType), new Object[]{leadSheetId},new RowMapper() {
//				public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
//				String territoryType = rs.getString(1);
//				return territoryType;
//				}
//			});
//			String territoryType = resultList.get(0);
			territoryType = StringUtils.isNotBlank(territoryType)?territoryType:"CLLI";
			String territoryTypeValue = null;
			String[] splitSql = QueryUtils.getQuery().get("SELECT_LEADSHEET_POLYGONS_MASTERMAP_"+leadSheetType).split(" GROUP BY");
			StringBuffer sql = new StringBuffer(splitSql[0]);
			if(territoryType.contains(":")){
				territoryTypeValue = territoryType.split(":")[1];
				if(territoryType.contains("|")){
					String[] territoryTypeArr = territoryType.split(":")[0].split("\\|");
					sql.append(" AND ( ");
					for(String fileCombination: territoryTypeValue.split(",")){				
						sql.append("( " + "LLC."+territoryTypeArr[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + "LLC."+territoryTypeArr[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ");
					}
					sql = new StringBuffer(sql.substring(0, (sql.length()-3))+ " ) ");
				}else{
					sql.append(" AND LLC."+territoryType.split(":")[0]+" in ("+territoryTypeValue+")");
				}
			}else{ 
				if(territoryType.contains("|")){
					String[] territoryTypeArr = territoryType.split("\\|");
					sql.append(" AND LLC."+territoryTypeArr[0]+" in ("+QueryUtils.getQuery().get("SELECT_DISTINCT_TERRITORY_TYPE_LEADSHEET_"+leadSheetType).replaceAll("TERRITORY_TYPE", "LLC."+territoryTypeArr[0])+")");
					sql.append(" AND LLC."+territoryTypeArr[1]+" in ("+QueryUtils.getQuery().get("SELECT_DISTINCT_TERRITORY_TYPE_LEADSHEET_"+leadSheetType).replaceAll("TERRITORY_TYPE", "LLC."+territoryTypeArr[1])+")");
				}else{
					sql.append(" AND LLC."+territoryType+" in ("+QueryUtils.getQuery().get("SELECT_DISTINCT_TERRITORY_TYPE_LEADSHEET_"+leadSheetType).replaceAll("TERRITORY_TYPE", "LLC."+territoryType)+")");
				}
				territoryTypeValue = territoryType;
			}
			if(splitSql.length>1){
				sql.append(" GROUP BY "+splitSql[1]);
			}
			logger.debug("In getLeadSheetPolygonsForMasterMap() sql="+sql);
			List<Polygon> list = new ArrayList<Polygon>();
			if(territoryType.contains(":")){
				list = jdbcTemplate.query(sql.toString(), new Object[]{campaignSeq, officeSeq}, new RowMapper() {
				public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
				    final Polygon polygon = new Polygon();
				    
				    polygon.getTerritory().setName(rs.getString(1) + " :LeadCount :" + rs.getString(2));
				    polygon.getTerritory().setDescription("TerritoryType:LeadSheet");
				    polygon.setCoordinates(rs.getString(3));
				    polygon.setColor(color.randomHex());
				    polygon.setType("TerritoryType:LeadSheet");
				    return polygon;
				}
			    });				
			}else if(territoryType.contains("|")){
				list = jdbcTemplate.query(sql.toString(), new Object[]{campaignSeq, officeSeq, leadSheetId, leadSheetId}, new RowMapper() {
				public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
				    final Polygon polygon = new Polygon();
				    
				    polygon.getTerritory().setName(rs.getString(1) + " :LeadCount :" + rs.getString(2));
				    polygon.getTerritory().setDescription("TerritoryType:LeadSheet");
				    polygon.setCoordinates(rs.getString(3));
				    polygon.setColor(color.randomHex());
				    polygon.setType("TerritoryType:LeadSheet");
				    return polygon;
				}
			    });
			}else{
				list = jdbcTemplate.query(sql.toString(), new Object[]{campaignSeq, officeSeq, leadSheetId}, new RowMapper() {
				public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
				    final Polygon polygon = new Polygon();
				    
				    polygon.getTerritory().setName(rs.getString(1) + " :LeadCount :" + rs.getString(2));
				    polygon.getTerritory().setDescription("TerritoryType:LeadSheet");
				    polygon.setCoordinates(rs.getString(3));
				    polygon.setColor(color.randomHex());
				    polygon.setType("TerritoryType:LeadSheet");
				    return polygon;
				}
			    });
			}
			return list;
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return null;
	}
	
	public List<Polygon> getLeadSheetPolygonsByIds(String leadSheetType, List<String> leadSheetIds) {
	
		if (leadSheetIds==null || leadSheetIds.isEmpty())
		    return null;	
		
		final RandomColor color = new RandomColor();
		try {
			String[] splitSql = QueryUtils.getQuery().get("SELECT_LEADSHEET_POLYGONS_BY_ID_"+leadSheetType).split(" GROUP BY");
			StringBuffer sql = new StringBuffer(splitSql[0]);
			sql.append(" AND LS.LEADSHEET_ID IN ( ");
			for(int i=0;i<leadSheetIds.size();i++){
				if(i!=leadSheetIds.size()-1)
					sql.append("'"+leadSheetIds.get(i)+"',");
				else
					sql.append("'"+leadSheetIds.get(i)+"'");
			}
			sql.append(" ) ");
			if(splitSql.length>1){
				sql.append(" GROUP BY "+splitSql[1]);
			}
		    List<Polygon> list = jdbcTemplate.query(sql.toString(), new RowMapper() {
			public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			    final Polygon polygon = new Polygon();
			    
			    polygon.getTerritory().setName(rs.getString(1) + " :LeadCount :" + rs.getString(2));
			    polygon.getTerritory().setDescription("TerritoryType:LeadSheet");
			    polygon.setCoordinates(rs.getString(3));
			    polygon.setColor(color.randomHex());
			    polygon.setType("TerritoryType:LeadSheet");
			    return polygon;
			}
		    });
			return list;
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return null;
	}
	public void insertTempLeadSheetPolygon(final TempLeadSheet tempLeadSheet){
		List<SqlParameter> parameters = new ArrayList<SqlParameter>();
		parameters.add(new SqlParameter("@leadSheetId", Types.VARCHAR));
		parameters.add(new SqlParameter("@boundaryCoordinates", Types.VARCHAR));
		parameters.add(new SqlOutParameter("@customPolygon", Types.LONGVARCHAR));
	
		logger.debug("->insertLeadSheets() Inserting LeadSheet Polygon. leadSheetId="+tempLeadSheet.getLeadSheetId()+" Coordinates="+tempLeadSheet.getCoordinatesForDB());
		Map map = jdbcTemplate.call(new CallableStatementCreator() {
			public CallableStatement createCallableStatement(Connection con) throws SQLException {
				CallableStatement statement = con.prepareCall("{call dbo.saveLeadSheetPolygon(?,?,?)}");
				statement.setString(1, tempLeadSheet.getLeadSheetId());
				statement.setString(2, tempLeadSheet.getCoordinatesForDB());
				statement.registerOutParameter(3, Types.LONGVARCHAR);
				return statement;
			}
		}, parameters);				
	}	
}
