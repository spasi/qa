package com.cydcor.framework.census;

import java.util.Date;

/**
 * County class
 */
public class Territory {
	private Integer key;
	private long parentTerritorykey;
	private String name;
	private String countyTypeId;
	private String stateId;
	private String stateName;
	private String type = "CUSTOM";
	private String description;
	private int numberOfBusinesses;
	private int numberOfOrders;
	private boolean active = true;
	private Date createdDate;
	private String createdUser;
	private Date modifiedDate;
	private String modifiedUser;
	private Integer salesRepKey;
	private Integer officeKey;
	private String group;

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	/**
	 * @return the parentTerritorykey
	 */
	public long getParentTerritorykey() {
		return parentTerritorykey;
	}

	/**
	 * @param parentTerritorykey
	 *            the parentTerritorykey to set
	 */
	public void setParentTerritorykey(long parentTerritorykey) {
		this.parentTerritorykey = parentTerritorykey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountyTypeId() {
		return countyTypeId;
	}

	public void setCountyTypeId(String countyTypeId) {
		this.countyTypeId = countyTypeId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNumberOfBusinesses() {
		return numberOfBusinesses;
	}

	public void setNumberOfBusinesses(int numberOfBusinesses) {
		this.numberOfBusinesses = numberOfBusinesses;
	}

	public int getNumberOfOrders() {
		return numberOfOrders;
	}

	public void setNumberOfOrders(int numberOfOrders) {
		this.numberOfOrders = numberOfOrders;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedUser() {
		return modifiedUser;
	}

	public void setModifiedUser(String modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

	public Integer getSalesRepKey() {
		return salesRepKey;
	}

	public void setSalesRepKey(Integer salesRepKey) {
		this.salesRepKey = salesRepKey;
	}

	public Integer getOfficeKey() {
		return officeKey;
	}

	public void setOfficeKey(Integer officeKey) {
		this.officeKey = officeKey;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * Return a Unique Nae for this territory
	 * 
	 * @return
	 */
	public String getUniqueName() {
		try {
			return name.replaceAll(":", "").replaceAll(" ", "").replaceAll(",",
					"");
		} catch (Exception e) {
			return name;
		}
	}

}
