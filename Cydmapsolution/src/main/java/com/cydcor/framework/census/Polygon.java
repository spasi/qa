package com.cydcor.framework.census;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.utils.CydcorUtils;

/**
 * 
 * @author Aswin
 * 
 */
public class Polygon {
	private Integer key;
	private String type = "CUSTOM";
	private String countyTypeId;
	private String coordinates;
	private boolean active = true;
	private String color;
	private String colorMode;
	private boolean fill = true;
	private String opacity = "0.0";
	private boolean outline = true;
	private String lineColor;
	private Integer lineWidth = 3;
	private String lineOpacity;
	private Date createdDate;
	private String createdUser;
	private Date modifiedDate;
	private String modifiedUser;
	private Territory territory = new Territory();

	public String getCoordinates() {
		return coordinates;
	}

	public String getCoordinatesForDB() {
		return "POLYGON((" + coordinates + "))";
	}

	public String getCoordinatesForKML() {
		String a = this.coordinates.replaceAll(
				"\\(|\\)|POLYGON |MULTI|POINT |LINESTRING ", "").replaceAll(
				", ", "\n").replaceAll(" ", ",");
		// if (type.equalsIgnoreCase("COUNTY"))
		// a = a.substring(a.indexOf("\n"), a.lastIndexOf("\n"));
		return a;

	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public String getColor() {
		if (StringUtils.isBlank(opacity))
			this.opacity = "0.0";
		return CydcorUtils.rgbToHexCode(color, Float.valueOf(opacity));
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getColorMode() {
		return colorMode;
	}

	public void setColorMode(String colorMode) {
		this.colorMode = colorMode;
	}

	public boolean getFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public boolean getOutline() {
		return outline;
	}

	public void setOutline(boolean outline) {
		this.outline = outline;
	}

	public String getOpacity() {
		return opacity;
	}

	public void setOpacity(String fillOpacity) {
		this.opacity = fillOpacity;
	}

	public String getLineColor() {
		return CydcorUtils.rgbToHexCode(lineColor, Float.valueOf(opacity));
	}

	public void setLineColor(String lineColor) {
		this.lineColor = lineColor;
	}

	public Integer getLineWidth() {
		return lineWidth;
	}

	public void setLineWidth(Integer lineWidth) {
		this.lineWidth = lineWidth;
	}

	public String getLineOpacity() {
		return lineOpacity;
	}

	public void setLineOpacity(String lineOpacity) {
		this.lineOpacity = lineOpacity;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getCountyTypeId() {
		return countyTypeId;
	}

	public void setCountyTypeId(String countyTypeId) {
		this.countyTypeId = countyTypeId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedUser() {
		return modifiedUser;
	}

	public void setModifiedUser(String modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Territory getTerritory() {
		return territory;
	}

	public void setTerritory(Territory territory) {
		this.territory = territory;
	}

}
