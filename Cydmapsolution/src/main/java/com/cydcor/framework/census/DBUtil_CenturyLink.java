package com.cydcor.framework.census;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.TempLeadSheet;
import com.cydcor.framework.rowmapper.LeadMasterMapper;
import com.cydcor.framework.rowmapper.LeadMasterStreamMapper;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.RandomColor;

public class DBUtil_CenturyLink extends DBUtil {

	public DBUtil_CenturyLink(JdbcTemplate template,
			PlatformTransactionManager transactionManager) {
		super(template, transactionManager);
	}
	
	@SuppressWarnings("unchecked")
    public List<LeadMaster> selectUnAssingedLeadsByWCUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters, String hasHSI) {
		/*
		 * String sql =
		 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
		 * + campaignId;
		 */
		List<LeadMaster> list = null;
		String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_WC");
		try {
			
			//Issue : 2013111
			if(null != hasHSI && !hasHSI.trim().equals("")){
				String[] spiltSql = sql.split(" ORDER BY ");
				if("YN".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI IN ('Y','N') ORDER BY " + spiltSql[1];
				}else if("Y".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI = 'Y' ORDER BY " + spiltSql[1];
				}else{//2013154 - Get HSI as N for blank value
					sql = spiltSql[0] + " AND (HAS_HSI = 'N' OR HAS_HSI = '') ORDER BY " + spiltSql[1];
				}
			}
		    if (StringUtils.isNotBlank(selectedFiles)) {
				if ("CLLI".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    if(campaignId.equalsIgnoreCase("67976206") || campaignId.equalsIgnoreCase("91039497"))
					{
						sql = spiltSql[0] + " AND NODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
					}else{
						sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
					}
				} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				}else if ("CLLI|DA".equalsIgnoreCase(useWirecenters)) {
					String[] combinationFiles = selectedFiles.split(",");
					String[] spiltSql = sql.split(" ORDER BY ");
					String[] territories = useWirecenters.split("\\|");
					sql = spiltSql[0] + " AND (";
					for(String fileCombination: combinationFiles){				
						sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
					}
					sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
				} else {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				}
		    }
		    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
    }
	
	@SuppressWarnings("unchecked")
	public List<LeadMaster> selectUnAssingedLeadsByDAUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters, String hasHSI) {
		/*
		 * String sql =
		 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
		 * + campaignId;
		 */
		List<LeadMaster> list = null;
		String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_DA");
		try {
			
			if(null != hasHSI && !hasHSI.trim().equals("")){
				String[] spiltSql = sql.split(" ORDER BY ");
				if("YN".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI IN ('Y','N') ORDER BY " + spiltSql[1];
				}else if("Y".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI = 'Y' ORDER BY " + spiltSql[1];
				}else{//2013154 - Get HSI as N for blank value
					sql = spiltSql[0] + " AND (HAS_HSI = 'N' OR HAS_HSI = '') ORDER BY " + spiltSql[1];
				}
			}
			
		    if (StringUtils.isNotBlank(selectedFiles)) {
				if ("CLLI".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    if(campaignId.equalsIgnoreCase("67976206") || campaignId.equalsIgnoreCase("91039497"))
					{
						sql = spiltSql[0] + " AND NODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
					}else{
						sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
					}
				} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("CLLI|DA".equalsIgnoreCase(useWirecenters)) {
					String[] combinationFiles = selectedFiles.split(",");
					String[] spiltSql = sql.split(" ORDER BY ");
					String[] territories = useWirecenters.split("\\|");
					sql = spiltSql[0] + " AND (";
					for(String fileCombination: combinationFiles){				
						sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
					}
					sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
				} else {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				}
		    }
		    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
	}
	
	public List<LeadMaster> selectUnAssingedLeadsByCityUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
		/*
		 * String sql =
		 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
		 * + campaignId;
		 */
		List<LeadMaster> list = null;
		String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_CITY");
		try {
		    if (StringUtils.isNotBlank(selectedFiles)) {
		  if ("CLLI".equalsIgnoreCase(useWirecenters)) {
			  String[] spiltSql = sql.split(" ORDER BY ");
			  if(campaignId.equalsIgnoreCase("67976206") || campaignId.equalsIgnoreCase("91039497"))
				{
					sql = spiltSql[0] + " AND NODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				}else{
					sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				}
			} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
			    String[] spiltSql = sql.split(" ORDER BY ");
			    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
			} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
			    String[] spiltSql = sql.split(" ORDER BY ");
			    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
			} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
			    String[] spiltSql = sql.split(" ORDER BY ");
			    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
			}else if ("CLLI|DA".equalsIgnoreCase(useWirecenters)) {
				String[] combinationFiles = selectedFiles.split(",");
				String[] spiltSql = sql.split(" ORDER BY ");
				String[] territories = useWirecenters.split("\\|");
				sql = spiltSql[0] + " AND (";
				for(String fileCombination: combinationFiles){				
					sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
				}
				sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
			} else {
			    String[] spiltSql = sql.split(" ORDER BY ");
			    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
			}
		    }
		    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
	    }

	    public List<LeadMaster> selectUnAssingedLeadsByCity(String campaignId, String officeSeq, String leadSheetIdGenerated) {
		/*
		 * String sql =
		 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
		 * + campaignId;
		 */
		List<LeadMaster> list = null;
		try {
		    list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_CITY_FROM_LEADSHEET"), new Object[] { officeSeq, campaignId,
			    leadSheetIdGenerated }, new LeadMasterMapper());
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
	    }

	
	@SuppressWarnings("unchecked")
	public List<LeadMaster> selectUnAssingedLeadsByhsiSpeedUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters) {
		/*
		 * String sql =
		 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
		 * + campaignId;
		 */
		List<LeadMaster> list = null;
		String sql ="";
		if(campaignId.equalsIgnoreCase("67976206") || campaignId.equalsIgnoreCase("91039497"))
		{
			sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_HSISPEED_Prsm");
			
		}
		else
		{
		   sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_HSISPEED");
		}
		try {
			if (StringUtils.isNotBlank(selectedFiles)) {
				if ("CLLI".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
					if(campaignId.equalsIgnoreCase("67976206") || campaignId.equalsIgnoreCase("91039497"))
					{
						sql = spiltSql[0] + " AND NODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
					}else{
						sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
					}
				} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("CLLI|DA".equalsIgnoreCase(useWirecenters)) {
					String[] combinationFiles = selectedFiles.split(",");
					String[] spiltSql = sql.split(" ORDER BY ");
					String[] territories = useWirecenters.split("\\|");
					sql = spiltSql[0] + " AND (";
					for(String fileCombination: combinationFiles){				
						sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
					}
					sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
				}else {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				}
		    }
		    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
	}
	
	public List<LeadMaster> selectUnAssingedLeadsByhsiSpeed(String campaignId, String officeSeq,String leadSheetIdGenerated) {
		/*
		 * String sql =
		 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
		 * + campaignId;
		 */
		List<LeadMaster> list = null;
		String sql ="";
		if(campaignId.equalsIgnoreCase("67976206") || campaignId.equalsIgnoreCase("91039497"))
		{
			sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_HSISPEED_Prsm_From_LeadSheet");
			
		}
		else
		{
		   sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_HSISPEED_From_LeadSheet");
		}
		try {
			  list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId ,leadSheetIdGenerated}, new LeadMasterMapper());
		    } catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
	}
	
	public List<LeadMaster> selectUnAssingedLeadsByZipUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters, String hasHSI) {
		/*
		 * String sql =
		 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
		 * + campaignId;
		 */
		List<LeadMaster> list = null;
		String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_ZIP");
		try {
			//Issue : 2013111
			if(!"67976206".equals(campaignId) && !"91039497".equals(campaignId)){
				if(null != hasHSI && !hasHSI.trim().equals("")){
					String[] spiltSql = sql.split(" ORDER BY ");
					if("YN".equals(hasHSI)){
						sql = spiltSql[0] + " AND HAS_HSI IN ('Y','N') ORDER BY " + spiltSql[1];
					}else if("Y".equals(hasHSI)){
						sql = spiltSql[0] + " AND HAS_HSI = 'Y' ORDER BY " + spiltSql[1];
					}else{//2013154 - Get HSI as N for blank value
						sql = spiltSql[0] + " AND (HAS_HSI = 'N' OR HAS_HSI = '') ORDER BY " + spiltSql[1];
					}
				}
			}
		    if (StringUtils.isNotBlank(selectedFiles)) {
				if ("CLLI".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    if("67976206".equals(campaignId) || "91039497".equals(campaignId)){
				    	sql = spiltSql[0] + " AND NODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				    }else{
				    	sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				    }
				} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				}else if ("CLLI|DA".equalsIgnoreCase(useWirecenters)) {
					String[] combinationFiles = selectedFiles.split(",");
					String[] spiltSql = sql.split(" ORDER BY ");
					String[] territories = useWirecenters.split("\\|");
					sql = spiltSql[0] + " AND (";
					for(String fileCombination: combinationFiles){				
						sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
					}
					sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
				} else {
				    String[] spiltSql = sql.split(" ORDER BY ");
				    sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				}
		    }
		    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
		} catch (Exception e) {
		    logger.debug(e);
		    logger.error(e);
		}
		return list;
	}
	
	/**
     * This method is added for CenturyLink Prism Vegas campaign.
     * @param campaignId
     * @param officeSeq
     * @param selectedFiles
     * @param products
     * @param useWirecenters
     * @param SpeedTerritory
     * @return
     */
    public List<LeadMaster> centurylink_Prism_Vegas_selectUnAssingedLeadsBySpeedUsingFiles(String campaignId
    		, String officeSeq
    		, String selectedFiles
    		, String products,
    	    String useWirecenters
    	    ,String SpeedTerritory
    	    ) {

    	logger.debug(">>centurylink_selectUnAssingedLeadsBySpeedUsingFiles");
    	
    	logger.debug("selectedFiles:"+selectedFiles);
    	logger.debug("products:"+products);
    	logger.debug("useWirecenters:"+useWirecenters);
    	logger.debug("SpeedTerritory:"+SpeedTerritory);
    	
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_SPEED_PRISM_VEGAS");
    	try {
    	    if (StringUtils.isNotBlank(products)) {
    	    	String[] spiltSql = sql.split(" ORDER BY ");
    	    	String[] columns = products.split(":");
    	    	String[] values = new String[columns.length-1];
    	    	/*for(int i=1;i< columns.length;i++){
    	    		values[i-1] = "'"+columns[i]+"'";
    	    	}
    	    	sql = spiltSql[0];
    	    	sql = sql + " AND DSL_SPEED_AVAIL IN (" + StringUtils.join(values, ", ") + ") ";
    			*/
    	    
    	    	sql = spiltSql[0];    	
    
    	  	if(columns.length>1){
    	    		sql = sql + " AND (  ";
	    	    	for(int i=1;i< columns.length;i++){
	    	    		values[i-1] = columns[i];
	    	    		if(columns[i].contains("1000001"))
	    	    		{
	    	    			String col[]=columns[i].split("and");
	    	    			sql= sql + " dsl_speed_avail >="+ col[0]+ " OR";
	    	    		}else if (columns[i].contains("0 and below"))
	    	    		{
	    	    			sql= sql + " dsl_speed_avail = 0 OR";
	    	    		}else if (columns[i].contains("PB"))
	    	    		{
		                     sql = sql + " dsl_speed_avail = "+"'"+columns[i]+"'" +" OR";
		    	    	}else
	    	    		{
	    	    		 String[] val=values[i-1].split("-");
	                     sql = sql + " dsl_speed_avail >="+val[0]+ " AND dsl_speed_avail <="+val[1]+" OR";
	    	    		}
	    	    	}
	    	    	sql = sql.substring(0, sql.length()-2);
	    	    	sql = sql + " ) ";
      	    	}
    	    	
	    		if(SpeedTerritory!=null)
	    		{
	    			String colname=null;
	    			if(SpeedTerritory.equalsIgnoreCase("byZip"))
	    			{
	    				colname="zip";
	    			}
	    			if(colname!=null)
	    			{
	    				sql = sql + " AND "+colname+"  is not null ";
	    				logger.debug("sql after:"+sql);
	    			}
	    		}
    			 
	    		sql = sql + " ORDER BY " + spiltSql[1];
    	    }
    	    
    	    
    	    if (StringUtils.isNotBlank(selectedFiles)) {
	    		if ("ZIP".equalsIgnoreCase(useWirecenters)) {
	    		    String[] spiltSql = sql.split(" ORDER BY ");
	    		    sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		}else if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND NODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				}else if ("CITY".equalsIgnoreCase(useWirecenters)) {
	    			String[] spiltSql = sql.split(" ORDER BY ");
	    			sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
	    			String[] spiltSql = sql.split(" ORDER BY ");
	    			sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		} else {
	    			String[] spiltSql = sql.split(" ORDER BY ");
	    			sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
	    		}
    	    }

    	    logger.debug("Unassigned leads by Speed query:"+sql);
    	    
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }
    
    public List<LeadMaster> centurylink_selectUnAssingedLeadsBySpeedUsingFiles(String campaignId
    		, String officeSeq
    		, String selectedFiles
    		, String products,
    	    String useWirecenters
    	    ,String SpeedTerritory
    	    ,String hasHSI
    		) {

    	logger.debug(">>centurylink_selectUnAssingedLeadsBySpeedUsingFiles");
    	
    	logger.debug("selectedFiles:"+selectedFiles);
    	logger.debug("products:"+products);
    	logger.debug("useWirecenters:"+useWirecenters);
    	logger.debug("SpeedTerritory:"+SpeedTerritory);
    	
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_SPEED");
    	try {
    		//Issue : 2013111
    		if(null != hasHSI && !hasHSI.trim().equals("")){
				String[] spiltSql = sql.split(" ORDER BY ");
				if("YN".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI IN ('Y','N') ORDER BY " + spiltSql[1];
				}else if("Y".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI = 'Y' ORDER BY " + spiltSql[1];
				}else{//2013154 - Get HSI as N for blank value
					sql = spiltSql[0] + " AND (HAS_HSI = 'N' OR HAS_HSI = '') ORDER BY " + spiltSql[1];
				}
			}
			
    	    if (StringUtils.isNotBlank(products)) {
    	    	String[] spiltSql = sql.split(" ORDER BY ");
    	    	String[] columns = products.split(":");
    	    	String[] values = new String[columns.length-1];
    	    	/*for(int i=1;i< columns.length;i++){
    	    		values[i-1] = "'"+columns[i]+"'";
    	    	}*/
    	    	sql = spiltSql[0];
    	    	
    	    	if(columns.length>1)
    	    		
    	    	{
    	    		sql = sql + " AND ( ";
    	    		for(int i=1;i<columns.length;i++)
    	    		{
    	    			values[i-1] = columns[i];
    	    			
    	    			if(columns[i].contains("1000001"))
	    	    		{
	    	    			String col[]=columns[i].split("and");
	    	    			sql = sql + " CONVERT(INT,SUBSTRING(HSI_AVAIL,0,CHARINDEX('/',HSI_AVAIL))) >= "+ "'"+col[0]+"'"+ " OR";
	    	    		}
    	    			else if (columns[i].contains("10000 and below"))
	    	    		{
	    	    			sql = sql + " CONVERT(INT,SUBSTRING(HSI_AVAIL,0,CHARINDEX('/',HSI_AVAIL))) <= '10000' OR";
	    	    		}
	    	    		else
	    	    		{
	    	    		 String[] val=values[i-1].split("-");
	    	    		 sql = sql + " CONVERT(INT,SUBSTRING(HSI_AVAIL,0,CHARINDEX('/',HSI_AVAIL))) >="+"'"+val[0]+"'" +" AND CONVERT(INT,SUBSTRING(HSI_AVAIL,0,CHARINDEX('/',HSI_AVAIL))) <="+"'"+val[1]+"'"+" OR";
	    	    		}
    	    			
    	    			
    	    			
    	    			
    	    		/*	if(values[i-1].contains("10001-30000"))
    	    			{
    	    			   String[] val=values[i-1].split("-");
    	    			   //sql=sql +" hsi_avail IN ('12000/2000','12000/896','12128/12128','12160/12160','18000/896','20000/2000','20000/896','20128/20128','20160/20160')"+" OR";
    	    			   sql = sql + " CONVERT(INT,SUBSTRING(HSI_AVAIL,0,CHARINDEX('/',HSI_AVAIL))) >="+"'"+val[0]+"'" +" AND CONVERT(INT,SUBSTRING(HSI_AVAIL,0,CHARINDEX('/',HSI_AVAIL))) <="+"'"+val[1]+"'"+" OR";
    	    			}else if(values[i-1].contains("30001- 50000"))
    	    			{
    	    				String[] val=values[i-1].split("-");
    	    				//sql=sql +" hsi_avail IN ('35000/3000','40000/2000','40000/20000','12160/12160','40000/5000','40128/40128')"+" OR";
    	    				sql = sql + " CONVERT(INT,SUBSTRING(HSI_AVAIL,0,CHARINDEX('/',HSI_AVAIL))) >="+"'"+val[0]+"'" +" AND CONVERT(INT,SUBSTRING(HSI_AVAIL,0,CHARINDEX('/',HSI_AVAIL))) <="+"'"+val[1]+"'"+" OR";
    	    			}*/
	    			}	
    	    		
    	    		sql = sql.substring(0, sql.length()-2);
	    	    	sql = sql + " ) ";
    	    	}
    	    	
    	    	//sql = sql + " AND hsi_avail IN (" + StringUtils.join(values, ", ") + ") ";
    			
    	    	if(SpeedTerritory!=null){
    	    		String colname=null;
    	    		if(SpeedTerritory.equalsIgnoreCase("byDA"))
    	    		{
    	    			colname="da";
    	    		}
    	    		else if(SpeedTerritory.equalsIgnoreCase("byWireCenter"))
    	    		{
    	    			colname="clli";
    	    		}
    	    		else if(SpeedTerritory.equalsIgnoreCase("byZip"))
    	    		{
    	    			colname="zip";
    	    		}
    	    		
    	    		if(colname!=null)
    	    		{
    	    			sql = sql + " AND "+colname+"  is not null ";
    	    			logger.debug("sql after:"+sql);
    	    		}
    	    		
    	    	}
    			 
    	    	sql = sql + " ORDER BY " + spiltSql[1];
    	    }
    	    
    	    if (StringUtils.isNotBlank(selectedFiles)) {
    	    	if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	}else if ("CLLI|DA".equalsIgnoreCase(useWirecenters)) {
					String[] combinationFiles = selectedFiles.split(",");
					String[] spiltSql = sql.split(" ORDER BY ");
					String[] territories = useWirecenters.split("\\|");
					sql = spiltSql[0] + " AND (";
					for(String fileCombination: combinationFiles){				
						sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
					}
					sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
				} else {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	}
    	    }

    	    logger.debug("Unassigned leads by Speed query:"+sql);
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }
    
    public List<LeadMaster> selectUnAssingedLeadsByPrismNewDateUsingFiles(String campaignId
    		, String officeSeq
    		, String selectedFiles
    		, String products,
    	    String useWirecenters
    		) {

    	logger.debug(">>selectUnAssingedLeadsByPrismNewDateUsingFiles");
    	
    	logger.debug("selectedFiles:"+selectedFiles);
    	logger.debug("products:"+products);
    	logger.debug("useWirecenters:"+useWirecenters);
    	

    	List<LeadMaster> list = null;
    	String sql = "";
    	if(campaignId.equals("67976206") || campaignId.equals("91039497")){
    		sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PRISM_NEW_DATE_PRISM");
    	}else{
    		sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PRISM_NEW_DATE");
    	}
    	try {
			
    	    if (StringUtils.isNotBlank(products)) {
    	    	String[] spiltSql = sql.split(" ORDER BY ");
    	    	String[] columns = products.split(":");
    	    	String[] values = new String[columns.length-1];
    	    	
    	    	sql = spiltSql[0];    	
    	    	if(columns.length>1){
    	    		sql = sql + " AND (  ";
	    	    	for(int i=1;i< columns.length;i++){
	    	    		values[i-1] = "'"+columns[i]+"'";
	    	    		if("0-30".equalsIgnoreCase(columns[i])){
	    	    			sql = sql + " DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())<=30 OR";
	    	    		}else if("31-60".equalsIgnoreCase(columns[i])){
	    	    			sql = sql + " (DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())>=31 AND DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())<=60) OR";
	    	    		}else if("61-90".equalsIgnoreCase(columns[i])){
	    	    			sql = sql + " (DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())>=61 AND DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())<=90) OR";
	    	    		}else if("91-180".equalsIgnoreCase(columns[i])){
	    	    			sql = sql + " (DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())>=91 AND DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())<=180) OR";
	    	    		}
	    	    	}
	    	    	sql = sql.substring(0, sql.length()-2);
	    	    	sql = sql + " ) ";
      	    	}
    			 
    	    	sql = sql + " ORDER BY " + spiltSql[1];
    	    }
    	    
    	    if (StringUtils.isNotBlank(selectedFiles)) {
    	    	if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		if(campaignId.equals("67976206") || campaignId.equals("91039497")){
    	    			sql = spiltSql[0] + " AND NODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    		}else{
    	    			sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    		}
    	    	} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	}else if ("CLLI|DA".equalsIgnoreCase(useWirecenters)) {
					String[] combinationFiles = selectedFiles.split(",");
					String[] spiltSql = sql.split(" ORDER BY ");
					String[] territories = useWirecenters.split("\\|");
					sql = spiltSql[0] + " AND (";
					for(String fileCombination: combinationFiles){				
						sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
					}
					sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
				} else {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	}
    	    }

    	    logger.debug("Unassigned leads by Prism New Date query:"+sql);
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }

    public List<LeadMaster> selectUnAssingedLeadsByPrismNewDateUsingSheet(String campaignId
    		, String officeSeq
    		, String leadSheetIdGenerated
    		, String products
    		) {

    	logger.debug(">>selectUnAssingedLeadsByPrismNewDateUsingSheet");
    	
    	logger.debug("leadSheetIdGenerated:"+leadSheetIdGenerated);
    	logger.debug("products:"+products);
    	

    	List<LeadMaster> list = null;
    	String sql = "";
    	if(campaignId.equals("67976206") || campaignId.equals("91039497")){
    		sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PRISM_NEW_DATE_SHEET_PRISM");
    	}else{
    		sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_PRISM_NEW_DATE_SHEET");
    	}
    	try {
			
    	    if (StringUtils.isNotBlank(products)) {
    	    	String[] spiltSql = sql.split(" ORDER BY ");
    	    	String[] columns = products.split(":");
    	    	String[] values = new String[columns.length-1];
    	    	
    	    	sql = spiltSql[0];    	
    	    	if(columns.length>1){
    	    		sql = sql + " AND (  ";
	    	    	for(int i=1;i< columns.length;i++){
	    	    		values[i-1] = "'"+columns[i]+"'";
	    	    		if("0-30".equalsIgnoreCase(columns[i])){
	    	    			sql = sql + " DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())<=30 OR";
	    	    		}else if("31-60".equalsIgnoreCase(columns[i])){
	    	    			sql = sql + " (DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())>=31 AND DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())<=60) OR";
	    	    		}else if("61-90".equalsIgnoreCase(columns[i])){
	    	    			sql = sql + " (DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())>=61 AND DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())<=90) OR";
	    	    		}else if("91-180".equalsIgnoreCase(columns[i])){
	    	    			sql = sql + " (DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())>=91 AND DATEDIFF(day,IPTV_NEWLY_AVAIL,GETDATE())<=180) OR";
	    	    		}
	    	    	}
	    	    	sql = sql.substring(0, sql.length()-2);
	    	    	sql = sql + " ) ";
      	    	}
    			 
    	    	sql = sql + " ORDER BY " + spiltSql[1];
    	    }

    	    logger.debug("Unassigned leads by Prism New Date Sheet query:"+sql);
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId,leadSheetIdGenerated }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }

    
    public List<LeadMaster> selectUnAssingedLeadsByMDUUsingFiles(String campaignId, String officeSeq, String selectedFiles, String useWirecenters, String hasHSI) {
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	String sql = null;
    	//atiwari : Modified for Prism Vegas campaign
    	if(campaignId.equals("67976206") || campaignId.equals("91039497")){
    		sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_MDU_PRISM_VEGAS");
    	}else{
    		sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_MDU");
    		//Issue : 2013111
    		if(null != hasHSI && !hasHSI.trim().equals("")){
				String[] spiltSql = sql.split(" ORDER BY ");
				if("YN".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI IN ('Y','N') ORDER BY " + spiltSql[1];
				}else if("Y".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI = 'Y' ORDER BY " + spiltSql[1];
				}else{//2013154 - Get HSI as N for blank value
					sql = spiltSql[0] + " AND (HAS_HSI = 'N' OR HAS_HSI = '') ORDER BY " + spiltSql[1];
				}
			}
    	}
    	try {
    	    if (StringUtils.isNotBlank(selectedFiles)) {
    	    	if ("CLLI".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		if("67976206".equals(campaignId) || "91039497".equals(campaignId)){
				    	sql = spiltSql[0] + " AND NODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				    }else{
				    	sql = spiltSql[0] + " AND CLLI IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
				    }
    	    	} else if ("ZIP".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND ZIP IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND CITY IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND LFA_CODE IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	}else if ("CLLI|DA".equalsIgnoreCase(useWirecenters)) {
					String[] combinationFiles = selectedFiles.split(",");
					String[] spiltSql = sql.split(" ORDER BY ");
					String[] territories = useWirecenters.split("\\|");
					sql = spiltSql[0] + " AND (";
					for(String fileCombination: combinationFiles){				
						sql = sql + "(" + territories[0]+"='" + fileCombination.split("\\|")[0].replaceAll("'", "") + "' AND " + territories[1]+"='" + fileCombination.split("\\|")[1].replaceAll("'", "") +"') OR ";
					}
					sql = sql.substring(0, (sql.length()-3))+ ") ORDER BY " + spiltSql[1];
				} else {
    	    		String[] spiltSql = sql.split(" ORDER BY ");
    	    		sql = spiltSql[0] + " AND RUN_ID IN (" + selectedFiles + ") ORDER BY " + spiltSql[1];
    	    	}
    	    }
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }
    
    @SuppressWarnings("unchecked")
    public List<LeadMaster> selectUnAssingedLeadsByWC(String campaignId, String officeSeq, String leadSheetIdGenerated, String hasHSI) {
		/*
		 * String sql =
		 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
		 * + campaignId;
		 */
    	List<LeadMaster> list = null;
    	try {
    		String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_WC_FROM_LEADSHEET");
    		//Issue : 2013111
    		if(null != hasHSI && !hasHSI.trim().equals("")){
				String[] spiltSql = sql.split(" ORDER BY ");
				if("YN".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI IN ('Y','N') ORDER BY " + spiltSql[1];
				}else if("Y".equals(hasHSI)){
					sql = spiltSql[0] + " AND HAS_HSI = 'Y' ORDER BY " + spiltSql[1];
				}else{//2013154 - Get HSI as N for blank value
					sql = spiltSql[0] + " AND (HAS_HSI = 'N' OR HAS_HSI = '') ORDER BY " + spiltSql[1];
				}
			}
    		list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId,
    		leadSheetIdGenerated }, new LeadMasterMapper());
    	} catch (Exception e) {
    		logger.debug(e);
    		logger.error(e);
    	}
    	return list;
    }
    
    public List<LeadMaster> selectUnAssingedLeadsByDA(String campaignId, String officeSeq, String leadSheetIdGenerated, String hasHSI) {
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	try {	
    			String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_DA_FROM_LEADSHEET");
    			//Issue : 2013111
    			if(null != hasHSI && !hasHSI.trim().equals("")){
    				String[] spiltSql = sql.split(" ORDER BY ");
    				if("YN".equals(hasHSI)){
    					sql = spiltSql[0] + " AND HAS_HSI IN ('Y','N') ORDER BY " + spiltSql[1];
    				}else if("Y".equals(hasHSI)){
    					sql = spiltSql[0] + " AND HAS_HSI = 'Y' ORDER BY " + spiltSql[1];
    				}else{//2013154 - Get HSI as N for blank value
    					sql = spiltSql[0] + " AND (HAS_HSI = 'N' OR HAS_HSI = '') ORDER BY " + spiltSql[1];
    				}
    			}
    	    	list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId,
    		    leadSheetIdGenerated }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }
    
        
    public List<LeadMaster> selectUnAssingedLeadsByZip(String campaignId, String officeSeq, String leadSheetIdGenerated, String hasHSI) {
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	try {
    		String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_ZIP_FROM_LEADSHEET");
    		if(!campaignId.equals("67976206") && !campaignId.equals("91039497")){
    			//Issue : 2013111
    			if(null != hasHSI && !hasHSI.trim().equals("")){
    				String[] spiltSql = sql.split(" ORDER BY ");
    				if("YN".equals(hasHSI)){
    					sql = spiltSql[0] + " AND HAS_HSI IN ('Y','N') ORDER BY " + spiltSql[1];
    				}else if("Y".equals(hasHSI)){
    					sql = spiltSql[0] + " AND HAS_HSI = 'Y' ORDER BY " + spiltSql[1];
    				}else{//2013154 - Get HSI as N for blank value
    					sql = spiltSql[0] + " AND (HAS_HSI = 'N' OR HAS_HSI = '') ORDER BY " + spiltSql[1];
    				}
    			}
    		}
    		list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId,
    	    	    leadSheetIdGenerated }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }
    
    /**
     * This method is added for CenturyLink Prism Vegas campaign. It fetches unassigned leads by Leadsheet id.
     * @param campaignId
     * @param officeSeq
     * @param selectedFiles
     * @param products
     * @param useWirecenters
     * @param SpeedTerritory
     * @return
     */
    public List<LeadMaster> centurylink_Prism_Vegas_selectUnAssingedLeadsBySpeedUsingSheet(String campaignId
    		, String officeSeq
    		, String products
    	    ,String SpeedTerritory
    	    , String leadSheetIdGenerated
    		) {

    	logger.debug(">>centurylink_selectUnAssingedLeadsBySpeedUsingFiles");
    	
    	logger.debug("leadSheetIdGenerated:"+leadSheetIdGenerated);
    	logger.debug("products:"+products);
    	logger.debug("SpeedTerritory:"+SpeedTerritory);
    	
    	List<LeadMaster> list = null;
    	String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_SPEED_SHEET_PRISM_VEGAS");
    	try {
    	    if (StringUtils.isNotBlank(products)) {
    	    	String[] spiltSql = sql.split(" ORDER BY ");
    	    	String[] columns = products.split(":");
    	    	String[] values = new String[columns.length-1];
    	    	/*for(int i=1;i< columns.length;i++){
    	    		values[i-1] = "'"+columns[i]+"'";
    	    	}
    	    	sql = spiltSql[0];
    	    	sql = sql + " AND DSL_SPEED_AVAIL IN (" + StringUtils.join(values, ", ") + ") ";*/
    			
    	    	sql = spiltSql[0];    	
    	        
        	  	if(columns.length>1){
        	    		sql = sql + " AND (  ";
    	    	    	for(int i=1;i< columns.length;i++){
    	    	    		values[i-1] = columns[i];
    	    	    		if(columns[i].contains("1000001"))
    	    	    		{
    	    	    			String col[]=columns[i].split("and");
    	    	    			sql= sql + " dsl_speed_avail >="+ col[0]+ " OR";
    	    	    		}else if (columns[i].contains("0 and below"))
    	    	    		{
    	    	    			sql= sql + " dsl_speed_avail = 0 OR";
    	    	    		}else if (columns[i].contains("PB"))
    	    	    		{
    		                     sql = sql + " dsl_speed_avail = "+"'"+columns[i]+"'" +" OR";
    		    	    	}else
    	    	    		{
    	    	    		 String[] val=values[i-1].split("-");
    	                     sql = sql + " dsl_speed_avail >="+val[0]+ " AND dsl_speed_avail <="+val[1]+" OR";
    	    	    		}
    	    	    	}
    	    	    	sql = sql.substring(0, sql.length()-2);
    	    	    	sql = sql + " ) ";
          	    	}
    	    	
    	    	
    	    	
	    		if(SpeedTerritory!=null)
	    		{
	    			String colname=null;
	    			if(SpeedTerritory.equalsIgnoreCase("byZip"))
	    			{
	    				colname="zip";
	    			}
	    			if(colname!=null)
	    			{
	    				sql = sql + " AND "+colname+"  is not null ";
	    				logger.debug("sql after:"+sql);
	    			}
	    		}
    			 
	    		sql = sql + " ORDER BY " + spiltSql[1];
    	    }
    	    logger.debug("Unassigned leads by Speed query:"+sql);
    	    
    	    list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId,leadSheetIdGenerated }, new LeadMasterMapper());
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }
    
    public List<LeadMaster> selectUnAssingedLeadsByMDU(String campaignId, String officeSeq, String leadSheetIdGenerated, String hasHSI) {
    	/*
    	 * String sql =
    	 * "select LEAD_ID, GEO_POINT.ToString() GEO_POINT, VALIDATION_CODE, RESPONSE_TEXT, CAMPAIGN_SEQ, OFFICE_SEQ, PERSON_ID, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY from IMS.IMS_LEAD_LIFECYCLE ilm WHERE ilm.OFFICE_SEQ IS NULL and ilm.CAMPAIGN_SEQ="
    	 * + campaignId;
    	 */
    	List<LeadMaster> list = null;
    	try {
    		//atiwari : Modified for Prism Vegas campaign.
    		if(campaignId.equals("67976206") || campaignId.equals("91039497")){
    			list = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_MDU_FROM_LEADSHEET_PRISM_VEGAS"), new Object[] { officeSeq, campaignId,
    				leadSheetIdGenerated }, new LeadMasterMapper());
    		}else{
    			String sql = QueryUtils.getQuery().get("SELECT_UNASSIGNED_LEADS_BY_MDU_FROM_LEADSHEET");
    			//Issue : 2013111
    			if(null != hasHSI && !hasHSI.trim().equals("")){
    				String[] spiltSql = sql.split(" ORDER BY ");
    				if("YN".equals(hasHSI)){
    					sql = spiltSql[0] + " AND HAS_HSI IN ('Y','N') ORDER BY " + spiltSql[1];
    				}else if("Y".equals(hasHSI)){
    					sql = spiltSql[0] + " AND HAS_HSI = 'Y' ORDER BY " + spiltSql[1];
    				}else{//2013154 - Get HSI as N for blank value
    					sql = spiltSql[0] + " AND (HAS_HSI = 'N' OR HAS_HSI = '') ORDER BY " + spiltSql[1];
    				}
    			}
    			list = jdbcTemplate.query(sql, new Object[] { officeSeq, campaignId,
    				leadSheetIdGenerated }, new LeadMasterMapper());
    		}
    	} catch (Exception e) {
    	    logger.debug(e);
    	    logger.error(e);
    	}
    	return list;
    }
    

    public void insertLeadSheets(final TempLeadSheet tempLeadSheet, final String territoryKey) {

	jdbcTemplate.batchUpdate(QueryUtils.getQuery().get("INSERT_LEAD_SHEETS"), new BatchPreparedStatementSetter() {
	    java.util.Date date = new java.util.Date();
	    Timestamp cDate = new Timestamp(date.getTime());

	    public void setValues(PreparedStatement ps, int i) throws SQLException {
		if (tempLeadSheet.getLeadIds().get(i).equals("Y")) {
		    insertLSValues(ps, tempLeadSheet.getLeadSheetId(), tempLeadSheet.getLeadSheetName(), tempLeadSheet.getLeadIds().get(i), tempLeadSheet
			    .getRowIds().get(i), tempLeadSheet.getGeoPoints().get(i), tempLeadSheet.getCampaignSeq(), tempLeadSheet.getOfficeSeq(),tempLeadSheet.getTerritoryType(),
			    territoryKey);
		} else {
		    insertLSValues(ps, tempLeadSheet.getLeadSheetId(), tempLeadSheet.getLeadSheetName(), tempLeadSheet.getLeadIds().get(i), tempLeadSheet
			    .getRowIds().get(i), null, tempLeadSheet.getCampaignSeq(), tempLeadSheet.getOfficeSeq(), tempLeadSheet.getTerritoryType(), territoryKey);
		}
	    }

	    private void insertLSValues(PreparedStatement ps, String leadSheetId, String leadSheetName, String leadId, Long rowId, String geoPoint,
		    String campaignSeq, String officeSeq, String territoryType, String territoryKey) throws SQLException {
		ps.setString(1, leadSheetId);
		ps.setString(2, leadSheetName);
		ps.setLong(3, rowId);
		ps.setString(4, leadId);
		ps.setTimestamp(5, cDate);
		ps.setLong(6, new Long(campaignSeq));
		ps.setLong(7, new Long(officeSeq));
		ps.setString(8, geoPoint);
		if(territoryType.contains("LFA") && !territoryType.contains("LFA_CODE")){
			territoryType = territoryType.replaceAll("LFA","LFA_CODE");
		}
		if((campaignSeq.equalsIgnoreCase("67976206") || campaignSeq.equalsIgnoreCase("91039497")) && territoryType.contains("CLLI")){
			territoryType = territoryType.replaceAll("CLLI","NODE");
		}
		ps.setString(9, territoryType);
		ps.setString(10, territoryKey);
	    }
	    
	    public int getBatchSize() {
		return tempLeadSheet.getLeadIds().size();
	    }
	});
		insertTempLeadSheetPolygon(tempLeadSheet);			
    }

    public void insertLeadSheets(final TempLeadSheet tempLeadSheet) {

		jdbcTemplate.batchUpdate(QueryUtils.getQuery().get("INSERT_LEAD_SHEETS"), new BatchPreparedStatementSetter() {
		    java.util.Date date = new java.util.Date();
		    Timestamp cDate = new Timestamp(date.getTime());
	
		    public void setValues(PreparedStatement ps, int i) throws SQLException {
		    	if (tempLeadSheet.getLeadIds().get(i).equals("Y")) {
		    		insertLSValues(ps, tempLeadSheet.getLeadSheetId(), tempLeadSheet.getLeadSheetName(), tempLeadSheet.getLeadIds().get(i), tempLeadSheet
		    				.getRowIds().get(i), tempLeadSheet.getGeoPoints().get(i), tempLeadSheet.getCampaignSeq(), tempLeadSheet.getOfficeSeq(), tempLeadSheet.getTerritoryType(), null);
		    	} else {
		    		insertLSValues(ps, tempLeadSheet.getLeadSheetId(), tempLeadSheet.getLeadSheetName(), tempLeadSheet.getLeadIds().get(i), tempLeadSheet
		    				.getRowIds().get(i), null, tempLeadSheet.getCampaignSeq(), tempLeadSheet.getOfficeSeq(), tempLeadSheet.getTerritoryType(), null);
		    	}
		    }
	
		    private void insertLSValues(PreparedStatement ps, String leadSheetId, String leadSheetName, String leadId, Long rowId, String geoPoint,
			    String campaignSeq, String officeSeq, String territoryType, String territoryKey) throws SQLException {
			ps.setString(1, leadSheetId);
			ps.setString(2, leadSheetName);
			ps.setLong(3, rowId);
			ps.setString(4, leadId);
			ps.setTimestamp(5, cDate);
			ps.setLong(6, new Long(campaignSeq));
			ps.setLong(7, new Long(officeSeq));
			ps.setString(8, geoPoint);
			if(territoryType.contains("LFA") && !territoryType.contains("LFA_CODE")){
				territoryType = territoryType.replaceAll("LFA","LFA_CODE");
			}
			if((campaignSeq.equalsIgnoreCase("67976206") || campaignSeq.equalsIgnoreCase("91039497")) && territoryType.contains("CLLI")){
				territoryType = territoryType.replaceAll("CLLI","NODE");
			}
			ps.setString(9, territoryType);
			ps.setString(10, territoryKey);
		    }
	
		    public int getBatchSize() {
			return tempLeadSheet.getLeadIds().size();
		    }
		});		
		insertTempLeadSheetPolygon(tempLeadSheet);	
    }
   
    
    
}
