package com.cydcor.framework.encryption;

/*************************************************************************
 *  Compilation:  javac RSA.java
 *  Execution:    java RSA N
 *  
 *  Generate an N-bit public and private RSA key and use to encrypt
 *  and decrypt a random message.
 * 
 *  % java RSA 50
 *  public  = 65537
 *  private = 553699199426609
 *  modulus = 825641896390631
 *  message   = 48194775244950
 *  encrpyted = 321340212160104
 *  decrypted = 48194775244950
 *
 *  Known bugs (not addressed for simplicity)
 *  -----------------------------------------
 *  - It could be the case that the message >= modulus. To avoid, use
 *    a do-while loop to generate key until modulus happen to be exactly N bits.
 *
 *  - It's possible that gcd(phi, publicKey) != 1 in which case
 *    the key generation fails. This will only happen if phi is a
 *    multiple of 65537. To avoid, use a do-while loop to generate
 *    keys until the gcd is 1.
 *
 *************************************************************************/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RSA {
	private static final transient Log logger = LogFactory.getLog(RSA.class);
	private final static BigInteger one = new BigInteger("1");
	private final static SecureRandom random = new SecureRandom();

	private BigInteger privateKey;
	private BigInteger publicKey;
	private BigInteger modulus;

	// generate an N-bit (roughly) public and private key
	RSA(int N) {
		BigInteger p = BigInteger.probablePrime(N / 2, random);
		BigInteger q = BigInteger.probablePrime(N / 2, random);
		BigInteger phi = (p.subtract(one)).multiply(q.subtract(one));

		modulus = p.multiply(q);
		publicKey = new BigInteger("65537"); // common value in practice = 2^16
		// + 1
		privateKey = publicKey.modInverse(phi);
	}

	/*************************************************************************
	 * Compilation: javac RSA.java Execution: java RSA N
	 * 
	 * Generate an N-bit public and private RSA key and use to encrypt and
	 * decrypt a random message.
	 * 
	 * % java RSA 50 public = 65537 private = 553699199426609 modulus =
	 * 825641896390631 message = 48194775244950 encrpyted = 321340212160104
	 * decrypted = 48194775244950
	 * 
	 * Known bugs (not addressed for simplicity)
	 * ----------------------------------------- - It could be the case that the
	 * message >= modulus. To avoid, use a do-while loop to generate key until
	 * modulus happen to be exactly N bits.
	 * 
	 * - It's possible that gcd(phi, publicKey) != 1 in which case the key
	 * generation fails. This will only happen if phi is a multiple of 65537. To
	 * avoid, use a do-while loop to generate keys until the gcd is 1.
	 * 
	 *************************************************************************/
	BigInteger encrypt(BigInteger message) {
		return message.modPow(publicKey, modulus);
	}

	BigInteger decrypt(BigInteger encrypted) {
		return encrypted.modPow(privateKey, modulus);
	}

	public String toString() {
		String s = "";
		s += "public  = " + publicKey + "\n";
		s += "private = " + privateKey + "\n";
		s += "modulus = " + modulus;
		return s;
	}

	public static void main(String[] args) throws Exception {
		Security
				.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		// int N = Integer.parseInt(args[0]);
		RSA key = new RSA(512);
		logger.debug(key);

		// create random message, encrypt and decrypt
		// BigInteger message = new BigInteger(N - 1, random);

		// create message by converting string to integer
		String s = "1231313";
		byte[] bytes = s.getBytes();
		BigInteger message = new BigInteger(s);

		BigInteger encrypt = key.encrypt(message);
		BigInteger decrypt = key.decrypt(encrypt);
		logger.debug("message   = " + message);
		logger.debug("encrpyted = " + encrypt);
		logger.debug("decrypted = " + decrypt);

		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		kpg.initialize(512);
		KeyPair kp = kpg.genKeyPair();
		Key publicKey = kp.getPublic();
		Key privateKey = kp.getPrivate();

		KeyFactory fact = KeyFactory.getInstance("RSA");
		RSAPublicKeySpec pub = fact.getKeySpec(kp.getPublic(),
				RSAPublicKeySpec.class);
		RSAPrivateKeySpec priv = fact.getKeySpec(kp.getPrivate(),
				RSAPrivateKeySpec.class);

		saveToFile("public.key", pub.getModulus(), pub.getPublicExponent());
		saveToFile("private.key", priv.getModulus(), priv.getPrivateExponent());

		logger.debug(pub.getModulus().toString(16));
		logger.debug(pub.getPublicExponent());

		// Try DEcrypting
		// 276adafb4d274f3e63a5e8efec4b90fa5c349344cdaf460037b2a7253550cfac3e48ab7095e5b078b0742377f251fd4c83d5f4766ef6c9eea64631ffe109dd84

		Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);
		byte[] cipherData = cipher.doFinal("ashwin_test".getBytes());
		logger.debug(new String(cipherData));
	}

	/**
	 * @param fileName
	 * @param mod
	 * @param exp
	 * @throws Exception
	 */
	public static void saveToFile(String fileName, BigInteger mod,
			BigInteger exp) throws Exception {
		ObjectOutputStream oout = new ObjectOutputStream(
				new BufferedOutputStream(new FileOutputStream(fileName)));
		try {
			oout.writeObject(mod);
			oout.writeObject(exp);
		} catch (Exception e) {
			throw e;
		} finally {
			oout.close();
		}
	}

	/**
	 * @param keyFileName
	 * @return
	 * @throws IOException
	 */
	static PublicKey readKeyFromFile(String keyFileName) throws IOException {
		InputStream in = new FileInputStream(keyFileName);
		ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(
				in));
		try {
			BigInteger m = (BigInteger) oin.readObject();
			BigInteger e = (BigInteger) oin.readObject();
			RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);
			KeyFactory fact = KeyFactory.getInstance("RSA");
			PublicKey pubKey = fact.generatePublic(keySpec);
			return pubKey;
		} catch (Exception e) {
			throw new RuntimeException("Spurious serialisation error", e);
		} finally {
			oin.close();
		}
	}
}
