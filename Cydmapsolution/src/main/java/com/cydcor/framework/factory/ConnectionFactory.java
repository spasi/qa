/**
 * 
 */
package com.cydcor.framework.factory;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author ashwin
 * 
 */
public class ConnectionFactory {
	private static final transient Log logger = LogFactory.getLog(ConnectionFactory.class);
	private static DataSource dbDataSource = null;

	/**
	 * @return the dbDataSource
	 */
	public static DataSource getDbDataSource() {
		return dbDataSource;
	}

	/**
	 * @param dbDataSource
	 *            the dbDataSource to set
	 */
	public static void setDbDataSource(DataSource dbDataSource) {
		ConnectionFactory.dbDataSource = dbDataSource;
	}

	/**
	 * @return
	 */
	public static Connection getDBConnection() {
		try {
			return getDbDataSource().getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.debug(e);
		}

		return null;
	}

}
