package com.cydcor.framework.filters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.RequestContext;
import com.cydcor.framework.context.ResponseContext;
import com.cydcor.framework.model.UserObject;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;
import com.cydcor.framework.wrapper.CydcorResponseWrapper;

/**
 * Servlet Filter implementation class CydcorSecurityFilter
 */
public class CydcorSecurityFilter implements Filter {
    protected final Log logger = LogFactory.getLog(getClass());

    /**
     * Default constructor.
     */
    public CydcorSecurityFilter() {
	// TODO Auto-generated constructor stub
    }

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
	// TODO Auto-generated method stub
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

	propogate(request, response, chain);

    }

    /**
     * Add Db Connection and user Info in RequestContext and propogate
     * 
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    private void propogate(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	RequestContext requestContext = null;
	logger.debug("Inside propogate.");
	try {
	    UserObject userObject = new UserObject();
	    // Wrap Request so as we can extend it's existing Functionality
	    CydcorRequestWrapper requestWrapper = new CydcorRequestWrapper((HttpServletRequest) request);
	    CydcorResponseWrapper responseWrapper = new CydcorResponseWrapper((HttpServletResponse) response);

	 
	    //if(!CydcorUtils.clientKeyNotPresent(request,response))
	    //{
	    //	logger.error("Client Key Not present..");
	    //	return;
	    //}
	    
	    String userId = request.getParameter("userId");
	    String clientKey = request.getParameter("clientKey");
	    
	    
	    
	    requestContext = new RequestContext();
	    requestContext.setRequestWrapper(requestWrapper);
	    requestContext.setUser(userObject);
	    CydcorContext.getInstance().setRequestContext(requestContext);

	    ResponseContext responseContext = new ResponseContext();
	    responseContext.setResponseWrapper(responseWrapper);
	    CydcorContext.getInstance().setResponseContext(responseContext);
	    
	    boolean bAllowToBrowse = CydcorUtils.clientKeyAndProfilesMatch(userId,clientKey);
	    
	    if(!bAllowToBrowse)
	    {
	    	String accessErrMsg=CydcorUtils.getProperty("ROLE_NOT_PRESENT");
	    	logger.error("User is not allowed to browse this page. ");
	    	CydcorUtils.showErrorPage(
	    			accessErrMsg,request,response
	    			);
	    	return;
	    	//throw new ServletException("User is not allowed to browser this page.");
	    }
	    

	    chain.doFilter(request, response);
	} catch (Throwable th) {
	    logger.fatal("Exception in Security Filter:", th);
	    th.printStackTrace();
	} finally {
	    response.flushBuffer();
	    CydcorContext.getInstance().setRequestContext(null);
	}
    }

   
	/**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
	// TODO Auto-generated method stub
    }

}
