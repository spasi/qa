/**
 * 
 */
package com.cydcor.framework.component;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * @author ashwin
 * 
 */
public class LOVCreator implements ComponentCreator {
	protected final Log logger = LogFactory.getLog(getClass());
	private PlatformService platformService = null;

	/**
	 * @return
	 */
	protected PlatformService getPlatformService() {
		return ServiceLocator.getService(PlatformService.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.component.ComponentCreator#getComponent(com.cydcor
	 * .framework.context.DynamicUIContext)
	 */
	public GenericUIControl getComponent(DynamicUIContext uiContext) {
		DatabaseResult dbResult = null;
		String controlDefnintion = "account/CampaignAdmin.html";
		GenericUIControl selectorControl = new GenericUIControl();

		if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
				"campaignSelector")) {
			dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));
			selectorControl.setTemplateName(controlDefnintion);
			selectorControl.addParameter(uiContext.getControlName(), true);
			selectorControl.addParameter("result", dbResult);

			uiContext.addControl(selectorControl);
		}

		return selectorControl;
	}

}
