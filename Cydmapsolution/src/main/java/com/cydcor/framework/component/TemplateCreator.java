/**
 * 
 */
package com.cydcor.framework.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.dao.PlatformDAO;

/**
 * @author ashwin
 * 
 */
public class TemplateCreator implements ComponentCreator {
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private PlatformDAO platformDAO;

	/**
	 * @return the platformDAO
	 */
	public PlatformDAO getPlatformDAO() {
		return platformDAO;
	}

	/**
	 * @param platformDAO
	 *            the platformDAO to set
	 */
	public void setPlatformDAO(PlatformDAO platformDAO) {
		this.platformDAO = platformDAO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.component.ComponentCreator#getComponent(com.cydcor
	 * .framework.context.DynamicUIContext)
	 */
	public GenericUIControl getComponent(DynamicUIContext uiContext) {
		
		return null;
	}

}
