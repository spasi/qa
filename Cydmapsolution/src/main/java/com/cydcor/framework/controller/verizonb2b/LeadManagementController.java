package com.cydcor.framework.controller.verizonb2b;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.LeadAssignmentSorter;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;
@Controller("verizonb2bLeadManagementController")
@Scope("singleton")
public class LeadManagementController extends GenericController {
	private static final transient Log logger = LogFactory.getLog(LeadManagementController.class);

	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

		String pageDefnintion = "lead/LeadManagementDef.html";
		String controlDefnintion = "lead/LeadManagement.html";

		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
						"true"))) {
			// Add Second Control the Campaign Selector
			GenericUIControl leadOperationsControl = new GenericUIControl();
			leadOperationsControl.setTemplateName(pageDefnintion);
			leadOperationsControl.addParameter("leadOperations", true);
			uiContext.addControl(leadOperationsControl);
		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "disabledCities")) {

			DatabaseResult dbResult = null;
			dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_DISABLED_CITIES"));

			GenericUIControl citiesSelectorControl = new GenericUIControl();
			citiesSelectorControl.setTemplateName(controlDefnintion);
			citiesSelectorControl.addParameter("disabledCities", true);
			citiesSelectorControl.addParameter("result", dbResult);

			uiContext.addControl(citiesSelectorControl);

		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "leadTable")) {
			String mainString = "";

			int grpByIdx = 0;
			String queryString = "";
			List<Object> inputs = new ArrayList<Object>();
			TableUIControl leadUploadHistoryTableControl = new TableUIControl();
			// leadUploadHistoryTableControl.setDataSource("LMS");

			boolean whereAdded = false;

			mainString = QueryUtils.getQuery().get("SELCT_LEAD_MANAGEMENT").replaceAll("\\s*,\\s*", ", ");
			grpByIdx = mainString.lastIndexOf(" GROUP BY ");
			queryString = mainString.substring(0, grpByIdx);
			String grpString = mainString.substring(grpByIdx);
			String operator = "";
			int fromIndex = 0;
			if (StringUtils.isNotBlank(requestWrapper.getParameter("fieldsAndValues"))) {
				String[] fields = requestWrapper.getParameter("fieldsAndValues").split("::");
				for (String field : fields) {
					String values[] = field.split(":");
					fromIndex = queryString.indexOf(" FROM IMS.IMS_LEAD_LIFECYCLE L ");
					if (queryString.substring(fromIndex).indexOf(" WHERE ") == -1) {
						queryString = queryString + " WHERE ";
					} else {
						if (StringUtils.isNotBlank(values[0])) {
							queryString = queryString + " " + values[0] + " ";
						} else {
							queryString = queryString + " AND ";
						}

					}
					operator = values[2];
					boolean isSubQuery = false;
					if (values[1].equalsIgnoreCase("CAMPAIGN_SEQ")) {
						values[1] = "CAMPAIGN_SEQ IN (SELECT C.CAMPAIGN_SEQ FROM MERLIN.CAMPAIGNS C WHERE C.CAMPAIGN_NAME ";
						isSubQuery = true;
					}

					if (values[1].equalsIgnoreCase("DA")) {
						fromIndex = queryString.indexOf(" FROM IMS.IMS_LEAD_LIFECYCLE L ");
						queryString = queryString.substring(0, fromIndex) + ", L." + values[1]
								+ queryString.substring(fromIndex);
						grpString = grpString + ", DA";
						leadUploadHistoryTableControl.addParameter("isDAAdded", true);
					} else if (values[1].equalsIgnoreCase("ZIP")) {
						fromIndex = queryString.indexOf(" FROM IMS.IMS_LEAD_LIFECYCLE L ");
						queryString = queryString.substring(0, fromIndex) + ", L." + values[1]
								+ queryString.substring(fromIndex);
						grpString = grpString + ", ZIP";
						leadUploadHistoryTableControl.addParameter("isZIPAdded", true);
					} else if (values[1].equalsIgnoreCase("CITY")) {
						queryString = queryString.replaceAll("CLLI", "CITY, STATE");
						grpString = grpString.replaceAll("CLLI", "CITY, STATE");
						leadUploadHistoryTableControl.addParameter("isCityAdded", true);
						leadUploadHistoryTableControl.addParameter("isWCRemoved", true);
					}

					queryString = queryString + " L." + values[1] + " ";
					String value = values[3].trim();
					if (operator.equals("1")) {
						queryString = queryString + " = '" + value + "'";
					} else if (operator.equals("2")) {
						queryString = queryString + " != '" + value + "'";
					} else if (operator.equals("3")) {
						value = value.replaceAll("\\s*,\\s*", "','");
						value = "'" + value + "'";
						queryString = queryString + " IN ( " + value + " ) ";
					} else if (operator.equals("4")) {
						value = value.replaceAll("\\s*,\\s*", "','");
						value = "'" + value + "'";
						queryString = queryString + " NOT IN ( " + value + " ) ";
					} else if (operator.equals("5")) {
						queryString = queryString + " LIKE '%" + value + "%'";
					} else if (operator.equals("6")) {
						queryString = queryString + " NOT LIKE '%" + value + "%'";
					}
					if (isSubQuery) {
						queryString = queryString + " ) ";
					}

				}
			}
			boolean typeAdded = false;
			if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))) {
				String[] types = requestWrapper.getParameter("leadsType").split(":");
				if (types.length != 4) {
					for (String type : types) {
						if (type.equals("1")) {
							queryString = queryString + " AND ";
							if (!typeAdded)
								queryString = queryString + " ( ";
							queryString = queryString
									+ " ( L.LEAD_SHEET_ID IS NULL AND L.ROW_ID NOT IN ( SELECT TL.ROW_ID FROM IMS.IMS_TEMP_LEADSHEET TL WHERE TL.ROW_ID=L.ROW_ID))";
							typeAdded = true;
						} else if (type.equals("2")) {
							queryString = queryString + (typeAdded ? " OR " : " AND ");
							if (!typeAdded)
								queryString = queryString + " ( ";
							queryString = queryString
									+ " L.ROW_ID  IN ( SELECT TL.ROW_ID FROM IMS.IMS_TEMP_LEADSHEET TL WHERE TL.ROW_ID=L.ROW_ID)";
							typeAdded = true;
						} else if (type.equals("3")) {
							queryString = queryString + (typeAdded ? " OR " : " AND ");
							if (!typeAdded)
								queryString = queryString + " ( ";
							queryString = queryString + " ( L.LEAD_SHEET_ID IS NOT NULL AND L.PERSON_ID IS NULL)";
							typeAdded = true;
						} else if (type.equals("4")) {
							queryString = queryString + (typeAdded ? " OR " : " AND ");
							if (!typeAdded)
								queryString = queryString + " ( ";
							queryString = queryString + " ( L.LEAD_SHEET_ID IS NOT NULL AND L.PERSON_ID IS NOT NULL)";
							typeAdded = true;
						}
					}
				}
			}
			if (typeAdded)
				queryString = queryString + " ) ";
			fromIndex = queryString.indexOf(" FROM IMS.IMS_LEAD_LIFECYCLE L ");
			queryString = queryString.substring(0, fromIndex) + ", COUNT(*) AS TOTAT_COUNT "
					+ queryString.substring(fromIndex);

			leadUploadHistoryTableControl.setColumnName("UPLOAD_FILENAME");
			String leadsStatus = "";
			if ("Lead Enable".equalsIgnoreCase(requestWrapper.getParameter("operation"))) {
				leadsStatus = " AND IS_ACTIVE ='X' ";
			} else {
				leadsStatus = " AND IS_ACTIVE IN ('Y','I','E') ";
			}

			queryString = queryString + leadsStatus + grpString;

			leadUploadHistoryTableControl.setQueryString(queryString);
			leadUploadHistoryTableControl.setFilterParamList(inputs);
			leadUploadHistoryTableControl.setTableName(uiContext.getControlName());
			leadUploadHistoryTableControl.setRowsPerPage(20);
			leadUploadHistoryTableControl.setJavaScriptMethodName("refreshLeadTable");
			leadUploadHistoryTableControl.addParameter("leadTable", true);
			leadUploadHistoryTableControl.setTemplateName("../" + controlDefnintion);
			uiContext.addControl(leadUploadHistoryTableControl);
		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "reAssignLeads")) {
			String lifeCycleQueryString = "UPDATE IMS.IMS_LEAD_LIFECYCLE  SET CAMPAIGN_SEQ="
					+ requestWrapper.getParameter("campaignSeq") + ", OFFICE_SEQ="
					+ requestWrapper.getParameter("officeSeq");
			lifeCycleQueryString = getCriteria(requestWrapper, lifeCycleQueryString);
			String whereCondition = lifeCycleQueryString.substring(lifeCycleQueryString.indexOf(" WHERE "));
			String tmpLeadSheetsQueryString = "UPDATE IMS.IMS_TEMP_LEADSHEET SET CAMPAIGN_SEQ="
					+ requestWrapper.getParameter("campaignSeq") + ", OFFICE_SEQ="
					+ requestWrapper.getParameter("officeSeq")
					+ " WHERE ROW_ID IN (SELECT ROW_ID FROM IMS.IMS_LEAD_LIFECYCLE  " + whereCondition + " )";
			String leadSheetsQueryString = "UPDATE IMS.IMS_LEADSHEET SET CAMPAIGN_SEQ="
					+ requestWrapper.getParameter("campaignSeq") + ", OFFICE_SEQ="
					+ requestWrapper.getParameter("officeSeq")
					+ " WHERE LEADSHEET_ID IN (SELECT LEAD_SHEET_ID FROM IMS.IMS_LEAD_LIFECYCLE  " + whereCondition
					+ " )";

			GenericUIControl updateICLCtrl = new GenericUIControl();
			try {
				getPlatformService().update(lifeCycleQueryString, new Object[]{});
				if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))
						&& requestWrapper.getParameter("leadsType").contains("2")) {
					getPlatformService().update(tmpLeadSheetsQueryString, new Object[]{});
				}
				if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))
						&& (requestWrapper.getParameter("leadsType").contains("3") || requestWrapper.getParameter(
								"leadsType").contains("4"))) {
					getPlatformService().update(leadSheetsQueryString, new Object[]{});
				}
				Timestamp curTime = new Timestamp(System.currentTimeMillis());
				getPlatformService().update(
						QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
						new Object[]{curTime, requestWrapper.getParameter("userId"), "Lead Re-Assign",
								lifeCycleQueryString});
				getPlatformService().update(
						QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
						new Object[]{curTime, requestWrapper.getParameter("userId"), "Lead Re-Assign",
								tmpLeadSheetsQueryString});
				getPlatformService().update(
						QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
						new Object[]{curTime, requestWrapper.getParameter("userId"), "Lead Re-Assign",
								leadSheetsQueryString});

				updateICLCtrl.setResponseText("SUCCESS");
			} catch (Exception e) {
				logger.error(e);
				updateICLCtrl.setResponseText("ERROR");
			}
			uiContext.addControl(updateICLCtrl);

		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "changeStatus")) {
			String status = "";
			if (requestWrapper.getParameter("action").equalsIgnoreCase("Enable")) {
				status = "L";
			} else {
				status = "X";
			}
			GenericUIControl updatestatusCtrl = new GenericUIControl();

			String lifeCycleQueryString = "UPDATE IMS.IMS_LEAD_LIFECYCLE  SET IS_ACTIVE='" + status + "' ";

			lifeCycleQueryString = getCriteria(requestWrapper, lifeCycleQueryString);
			String whereCondition = lifeCycleQueryString.substring(lifeCycleQueryString.indexOf(" WHERE "));
			String tmpLeadSheetsQueryString = "DELETE FROM IMS.IMS_TEMP_LEADSHEET WHERE ROW_ID IN ( SELECT ROW_ID FROM IMS.IMS_LEAD_LIFECYCLE  "
					+ whereCondition + " )";

			try {
				getPlatformService().update(lifeCycleQueryString, new Object[]{});
				if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))
						&& requestWrapper.getParameter("leadsType").contains("2") && status.equalsIgnoreCase("X")) {
					getPlatformService().update(tmpLeadSheetsQueryString, new Object[]{});
				}
				Timestamp curTime = new Timestamp(System.currentTimeMillis());
				if (requestWrapper.getParameter("statusChangeType").equals("byCity")) {
					if ("L".equalsIgnoreCase(status)) {
						String[] enableCityState = requestWrapper.getParameter("enableCity").split(":");
						String city = enableCityState[0];
						String state = enableCityState[1];
						String campaignSeq = enableCityState[2];
						getPlatformService().update(QueryUtils.getQuery().get("UPDATE_DISABLED_CITIES"),
								new Object[]{"N", requestWrapper.getParameter("userId"), curTime, city, state,
										campaignSeq});

					} else {
						String sql = "SELECT C.CAMPAIGN_SEQ FROM MERLIN.CAMPAIGNS C WHERE C.CAMPAIGN_NAME ='"
								+ requestWrapper.getParameter("campaignName") + "'";
						// TODO
						DatabaseResult result = getPlatformService().loadResult(sql);
						Integer campaignSeq = (Integer) result.getData().get(0).get(0).getActualValue();
						getPlatformService().update(
								QueryUtils.getQuery().get("INSERT_DISABLED_CITIES"),
								new Object[]{requestWrapper.getParameter("city"), requestWrapper.getParameter("state"),
										campaignSeq,
										"Y", requestWrapper.getParameter("userId"), curTime,
										requestWrapper.getParameter("userId"), curTime});

					}
				} else {

					getPlatformService().update(
							QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
							new Object[]{curTime, requestWrapper.getParameter("userId"),
									requestWrapper.getParameter("action"), lifeCycleQueryString});
					getPlatformService().update(
							QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
							new Object[]{curTime, requestWrapper.getParameter("userId"),
									requestWrapper.getParameter("action"), tmpLeadSheetsQueryString});
				}
				updatestatusCtrl.setResponseText("SUCCESS");
			} catch (Exception e) {
				logger.error(e);
				updatestatusCtrl.setResponseText("ERROR");
			}
			uiContext.addControl(updatestatusCtrl);

		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "selectOperations")) {
			GenericUIControl selectOper = new GenericUIControl();
			selectOper.setTemplateName(controlDefnintion);
			selectOper.addParameter("leadOperations", true);
			selectOper.addParameter("showleadOperations", true);
			if(requestWrapper.getParameter("option").equals("true")){
				selectOper.addParameter("reassignOperationSelected", true);
			}else{
				selectOper.addParameter("reassignOperationSelected", false);
			}
			uiContext.addControl(selectOper);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "loadAssignmentTypeValues")) {
			DatabaseResult result = null;
			GenericUIControl selectOper = new GenericUIControl();
			selectOper.setTemplateName(controlDefnintion);
			selectOper.addParameter("leadOperations", true);
			selectOper.addParameter("showleadOperations", true);
			selectOper.addParameter("reassignOperationSelected", true);
			
			if(requestWrapper.getParameter("assignmentType").equals("CLLI")){
				String sql = "";
				if("15791795".equals(requestWrapper.getParameter("campaignSeq"))){
					sql = "Select DISTINCT CLLI from  VerizonMaps.dbo.FIOSCLLILFA_OWNERS_V ORDER BY CLLI";
				}else{
					sql = "Select DISTINCT CLLI from  VerizonMaps.dbo.B2BCLLI_OWNERS_V ORDER BY CLLI";
				}
				result = getPlatformService().loadResult(sql);
				selectOper.addParameter("displayAssignmentTypeList", true);
				selectOper.addParameter("resultAssignmentType", result);
			}else if(requestWrapper.getParameter("assignmentType").equals("MerlinCode")){
				String sql = "";
				if("15791795".equals(requestWrapper.getParameter("campaignSeq"))){
					sql = "Select DISTINCT MerlinCode from  VerizonMaps.dbo.FIOSCLLILFA_OWNERS_V ORDER BY MerlinCode";
				}else{
					sql = "Select DISTINCT MerlinCode from  VerizonMaps.dbo.B2BCLLI_OWNERS_V ORDER BY MerlinCode";
				}
				result = getPlatformService().loadResult(sql);
				selectOper.addParameter("displayAssignmentTypeList", true);
				selectOper.addParameter("resultAssignmentType", result);
			}else{
				String sql = QueryUtils.getQuery().get("SELECT_ADMIN_OFFICE_CODE_LIST");
				result = getPlatformService().loadResult(sql);
				selectOper.addParameter("assignmentListTable", true);
				selectOper.addParameter("showCreateNewWC", true);
				selectOper.addParameter("resultNewOwner", result);
			}
			selectOper.addParameter("campaignSeq", requestWrapper.getParameter("campaignSeq"));
			selectOper.addParameter("assignmentType", requestWrapper.getParameter("assignmentType"));
			uiContext.addControl(selectOper);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "loadAssignmentList")) {
			DatabaseResult result = null;
			GenericUIControl selectOper = new GenericUIControl();
			selectOper.setTemplateName(controlDefnintion);
			selectOper.addParameter("leadOperations", true);
			selectOper.addParameter("showleadOperations", true);
			selectOper.addParameter("reassignOperationSelected", true);
			
			if(requestWrapper.getParameter("assignmentType").equals("CLLI")){
				String sql = "";
				if("15791795".equals(requestWrapper.getParameter("campaignSeq"))){
					sql = "Select DISTINCT CLLI from  VerizonMaps.dbo.FIOSCLLILFA_OWNERS_V ORDER BY CLLI";
				}else{
					sql = "Select DISTINCT CLLI from  VerizonMaps.dbo.B2BCLLI_OWNERS_V ORDER BY CLLI";
				}
				result = getPlatformService().loadResult(sql);
				selectOper.addParameter("resultAssignmentType", result);
			}else{
				String sql = "";
				if("15791795".equals(requestWrapper.getParameter("campaignSeq"))){
					sql = "Select DISTINCT MerlinCode from  VerizonMaps.dbo.FIOSCLLILFA_OWNERS_V ORDER BY MerlinCode";
				}else{
					sql = "Select DISTINCT MerlinCode from  VerizonMaps.dbo.B2BCLLI_OWNERS_V ORDER BY MerlinCode";
				}
				result = getPlatformService().loadResult(sql);
				selectOper.addParameter("resultAssignmentType", result);
			}
			
			String sql = QueryUtils.getQuery().get("SELECT_ADMIN_OFFICE_CODE_LIST");
			result = getPlatformService().loadResult(sql);
			selectOper.addParameter("resultNewOwner", result);
			
			List resultRecords = null;
			if("CLLI".equals(requestWrapper.getParameter("assignmentType"))){
				resultRecords = getPlatformService().fetchLeadAssignmentList(requestWrapper.getParameter("campaignSeq"), 
						requestWrapper.getParameter("assignmentType"), null, requestWrapper.getParameter("assignTypeValue"));
			}else{
				resultRecords = getPlatformService().fetchLeadAssignmentList(requestWrapper.getParameter("campaignSeq"), 
						requestWrapper.getParameter("assignmentType"), requestWrapper.getParameter("assignTypeValue"), null);
			}
			
			Collections.sort(resultRecords, new LeadAssignmentSorter(requestWrapper.getParameter("sortOrder")));
			selectOper.addParameter("displayAssignmentTypeList", true);
			selectOper.addParameter("assignmentListTable", true);
			selectOper.addParameter("resultList", resultRecords);
			selectOper.addParameter("campaignSeq", requestWrapper.getParameter("campaignSeq"));
			selectOper.addParameter("assignmentType", requestWrapper.getParameter("assignmentType"));
			uiContext.addControl(selectOper);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "saveNewCLLIAssignment")) {
			String lfaCode = null;
			if(null != requestWrapper.getParameter("newLFA") && !requestWrapper.getParameter("newLFA").equals("")){
				lfaCode = requestWrapper.getParameter("newLFA");
			}
			
			getPlatformService().saveNewCLLI(requestWrapper.getParameter("campaignSeq"), 
					"NEW", requestWrapper.getParameter("newOwner"), requestWrapper.getParameter("newWC"),lfaCode,requestWrapper.getParameter("userId"));
			
			GenericUIControl saveOper = new GenericUIControl();
			saveOper.setTemplateName(controlDefnintion);
			saveOper.addParameter("leadOperations", true);
			saveOper.addParameter("showleadOperations", true);
			saveOper.addParameter("reassignOperationSelected", true);
			saveOper.addParameter("displayAssignmentTypeList", true);
			saveOper.addParameter("assignmentListTable", true);
			saveOper.addParameter("campaignSeq", requestWrapper.getParameter("campaignSeq"));
			saveOper.addParameter("assignmentType", requestWrapper.getParameter("assignmentType"));
			uiContext.addControl(saveOper);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "saveNewOwnerList")) {
			String campaignSeq =requestWrapper.getParameter("campaignSeq");
			String assignStr = requestWrapper.getParameter("arrayData");
			Map<String, Map<String, HashSet>> mNewOwner = new HashMap<String, Map<String, HashSet>>();
			
			StringTokenizer st1 = new StringTokenizer(assignStr,"|");
			while (st1.hasMoreTokens()) {
				StringTokenizer st2 = new StringTokenizer(st1.nextToken(),":");
				String sClli = st2.nextToken();
				String slfa = st2.nextToken();
				String newOffice = st2.nextToken();
				if(mNewOwner.containsKey(newOffice)){
					Map<String, HashSet> mapCLLI = mNewOwner.get(newOffice);
					if(mapCLLI.containsKey(sClli)){
						HashSet lfaSet = mapCLLI.get(sClli);
						if(campaignSeq.equals("15791795")){
							if(!slfa.equals("NA")){
								if(null == lfaSet){
									lfaSet = new HashSet();
								}
								lfaSet.add(slfa);
							}else{
								lfaSet.add(null);
							}
						}else{
							lfaSet.add(null);
						}
					}else{
						if(campaignSeq.equals("15791795")){
							HashSet lfaSet = new HashSet();
							if(!slfa.equals("NA")){
								
								lfaSet.add(slfa);
								mapCLLI.put(sClli, lfaSet);
							}else{
								lfaSet.add(null);
							}
						}else{
							mapCLLI.put(sClli, null);
						}
					}
				}else{
					Map<String, HashSet> mapCLLI = new HashMap<String, HashSet>();
					if(campaignSeq.equals("15791795")){
						HashSet lfaSet = new HashSet();
						if(!slfa.equals("NA")){
							lfaSet.add(slfa);
							
						}else{
							lfaSet.add(null);
						}
						mapCLLI.put(sClli, lfaSet);
					}else{
						mapCLLI.put(sClli, null);
					}
					mNewOwner.put(newOffice, mapCLLI);
				}
		    }
			getPlatformService().saveAssignments(campaignSeq, mNewOwner, requestWrapper.getParameter("userId"));
			
			GenericUIControl saveOper = new GenericUIControl();
			saveOper.setTemplateName(controlDefnintion);
			saveOper.addParameter("leadOperations", true);
			saveOper.addParameter("showleadOperations", true);
			saveOper.addParameter("reassignOperationSelected", true);
			saveOper.addParameter("displayAssignmentTypeList", true);
			saveOper.addParameter("assignmentListTable", true);
			saveOper.addParameter("campaignSeq", campaignSeq);
			uiContext.addControl(saveOper);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "loadMergeOfficeList")){
			DatabaseResult result = null;
			List<Object> inputs = new ArrayList<Object>();
			TableUIControl mergeList = new TableUIControl();
			mergeList.setQueryString(QueryUtils.getQuery().get("SELECT_ADMIN_MERGE_OFFICES_LIST"));
			mergeList.setFilterParamList(inputs);
			mergeList.setColumnName("Active DESC");
			mergeList.setTableName(uiContext.getControlName());
			mergeList.setRowsPerPage(500);
			mergeList.setJavaScriptMethodName("loadMergeOfficePage");
			mergeList.addParameter("mergeOfficeList", true);
			String sqlOwnerCode = QueryUtils.getQuery().get("SELECT_ADMIN_OFFICE_CODE_LIST");
			result = getPlatformService().loadResult(sqlOwnerCode);
			mergeList.addParameter("resultOwnerCode", result);
			mergeList.setTemplateName("../" + controlDefnintion);
			uiContext.addControl(mergeList);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "loadSearchOfficeList")){
			DatabaseResult result = null;
			List<Object> inputs = new ArrayList<Object>();
			TableUIControl mergeList = new TableUIControl(); 
			String sqlSearchString = QueryUtils.getQuery().get("SELECT_ADMIN_MERGE_OFFICES_LIST");
			if(requestWrapper.getParameter("sOperation").equals("1")){
				sqlSearchString = sqlSearchString + " WHERE [Reps From Office] like '"+requestWrapper.getParameter("sText")+"%' OR [Reps To Office] like '"+requestWrapper.getParameter("sText")+"%' ";
			}else if(requestWrapper.getParameter("sOperation").equals("2")){
				sqlSearchString = sqlSearchString + " WHERE [Reps From Office] = '"+requestWrapper.getParameter("sText")+"' OR [Reps To Office] = '"+requestWrapper.getParameter("sText")+"' ";
			}else if(requestWrapper.getParameter("sOperation").equals("3")){
				sqlSearchString = sqlSearchString + " WHERE [Reps From Office] != '"+requestWrapper.getParameter("sText")+"' AND [Reps To Office] != '"+requestWrapper.getParameter("sText")+"' ";
			}
			mergeList.setQueryString(sqlSearchString);
			mergeList.setFilterParamList(inputs);
			mergeList.setColumnName("Active DESC");
			mergeList.setTableName(uiContext.getControlName());
			mergeList.setRowsPerPage(500);
			mergeList.setJavaScriptMethodName("loadMergeOfficePage");
			mergeList.addParameter("mergeOfficeList", true);
			String sqlOwnerCode = QueryUtils.getQuery().get("SELECT_ADMIN_OFFICE_CODE_LIST");
			result = getPlatformService().loadResult(sqlOwnerCode);
			mergeList.addParameter("resultOwnerCode", result);
			mergeList.setTemplateName("../" + controlDefnintion);
			uiContext.addControl(mergeList);
		}else if(StringUtils.equalsIgnoreCase(uiContext.getControlName(), "checkOwnerAvailability")){
			GenericUIControl checkOwner = new GenericUIControl();
			DatabaseResult result = null;
			String sqlOwnerCode = QueryUtils.getQuery().get("SELECT_ADMIN_MERGE_OFFICES_LIST");
			sqlOwnerCode = sqlOwnerCode+" WHERE [Reps From Office] = '"+requestWrapper.getParameter("fromOffice")+"' OR [Reps To Office] = '"+requestWrapper.getParameter("toOffice")+"' ";
			result = getPlatformService().loadResult(sqlOwnerCode);
			if(null != result && null != result.getData() && result.getData().size() != 0){
				checkOwner.setResponseText("SUCCESS");
			}else{
				checkOwner.setResponseText("ERROR");
			}
			uiContext.addControl(checkOwner);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "saveMergeOffice")){
			getPlatformService().upsertMergedOwners(requestWrapper.getParameter("fromOffice"), requestWrapper.getParameter("toOffice"), 
					requestWrapper.getParameter("userId"), requestWrapper.getParameter("active"), requestWrapper.getParameter("action"), 
					requestWrapper.getParameter("clientKey"));
			GenericUIControl addMergedOwner = new GenericUIControl();
			addMergedOwner.setTemplateName(controlDefnintion);
			uiContext.addControl(addMergedOwner);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "updateMergedOwner")){
			getPlatformService().upsertMergedOwners(requestWrapper.getParameter("fromOffice"), requestWrapper.getParameter("toOffice"), 
					requestWrapper.getParameter("userId"), requestWrapper.getParameter("active"), requestWrapper.getParameter("action"), 
					requestWrapper.getParameter("clientKey"));
			GenericUIControl addMergedOwner = new GenericUIControl();
			addMergedOwner.setTemplateName(controlDefnintion);
			uiContext.addControl(addMergedOwner);
		}
	}

	private String getCriteria(CydcorRequestWrapper requestWrapper, String queryString) {
		String operator = "";
		if (StringUtils.isNotBlank(requestWrapper.getParameter("fieldsAndValues"))) {
			String[] fields = requestWrapper.getParameter("fieldsAndValues").split("::");
			for (String field : fields) {
				String values[] = field.split(":");
				if (queryString.indexOf(" WHERE ") == -1) {
					queryString = queryString + " WHERE ";
				} else {
					if (StringUtils.isNotBlank(values[0])) {
						queryString = queryString + " " + values[0] + " ";
					} else {
						queryString = queryString + " AND ";
					}

				}
				operator = values[2];
				boolean isSubQuery = false;
				if (values[1].equalsIgnoreCase("CAMPAIGN_SEQ")) {
					values[1] = "CAMPAIGN_SEQ IN (SELECT C.CAMPAIGN_SEQ FROM MERLIN.CAMPAIGNS C WHERE C.CAMPAIGN_NAME ";
					isSubQuery = true;
				}

				queryString = queryString + values[1] + " ";
				String value = values[3].trim();
				if (operator.equals("1")) {
					queryString = queryString + " = '" + value + "'";
				} else if (operator.equals("2")) {
					queryString = queryString + " != '" + value + "'";
				} else if (operator.equals("3")) {
					value = value.replaceAll("\\s*,\\s*", "','");
					value = "'" + value + "'";
					queryString = queryString + " IN ( " + value + " ) ";
				} else if (operator.equals("4")) {
					value = value.replaceAll("\\s*,\\s*", "','");
					value = "'" + value + "'";
					queryString = queryString + " NOT IN ( " + value + " ) ";
				} else if (operator.equals("5")) {
					queryString = queryString + " LIKE '%" + value + "%'";
				} else if (operator.equals("6")) {
					queryString = queryString + " NOT LIKE '%" + value + "%'";
				}
				if (isSubQuery) {
					queryString = queryString + " ) ";
				}

			}
		}
		boolean typeAdded = false;
		if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))) {
			String[] types = requestWrapper.getParameter("leadsType").split(":");
			if (types.length != 4) {
				for (String type : types) {
					if (type.equals("1")) {
						queryString = queryString + " AND ";
						if (!typeAdded)
							queryString = queryString + " ( ";
						queryString = queryString
								+ " ( LEAD_SHEET_ID IS NULL AND ROW_ID NOT IN ( SELECT TL.ROW_ID FROM IMS.IMS_TEMP_LEADSHEET TL WHERE TL.ROW_ID=IMS_LEAD_LIFECYCLE.ROW_ID))";
						typeAdded = true;
					} else if (type.equals("2")) {
						queryString = queryString + (typeAdded ? " OR " : " AND ");
						if (!typeAdded)
							queryString = queryString + " ( ";
						queryString = queryString
								+ " ROW_ID  IN ( SELECT TL.ROW_ID FROM IMS.IMS_TEMP_LEADSHEET TL WHERE TL.ROW_ID=IMS_LEAD_LIFECYCLE.ROW_ID)";
						typeAdded = true;
					} else if (type.equals("3")) {
						queryString = queryString + (typeAdded ? " OR " : " AND ");
						if (!typeAdded)
							queryString = queryString + " ( ";
						queryString = queryString + " ( LEAD_SHEET_ID IS NOT NULL AND PERSON_ID IS NULL)";
						typeAdded = true;
					} else if (type.equals("4")) {
						queryString = queryString + (typeAdded ? " OR " : " AND ");
						if (!typeAdded)
							queryString = queryString + " ( ";
						queryString = queryString + " ( LEAD_SHEET_ID IS NOT NULL AND PERSON_ID IS NOT NULL)";
						typeAdded = true;
					}
				}
			}
		}
		if (typeAdded)
			queryString = queryString + " ) ";
		String leadsStatus = "";
		if ("Lead Enable".equalsIgnoreCase(requestWrapper.getParameter("operation"))) {
			leadsStatus = " AND IS_ACTIVE ='X' ";
		} else {
			leadsStatus = " AND IS_ACTIVE IN ('Y','I','E') ";
		}

		return queryString + leadsStatus;
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
