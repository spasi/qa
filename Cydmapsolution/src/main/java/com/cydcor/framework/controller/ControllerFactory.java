/**
 * 
 */
package com.cydcor.framework.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.utils.ServiceLocator;

/**
 * @author ashwin
 * 
 */
public class ControllerFactory {
	protected final Log logger = LogFactory.getLog(getClass());
	private static ControllerFactory controllerFactory = new ControllerFactory();

	private ControllerFactory() {

	}

	/**
	 * @return
	 */
	public static ControllerFactory getInstance() {
		return controllerFactory;
	}

	/**
	 * Every Controller Class Is PageName appended with Controller
	 * 
	 * @param pageName
	 * @return
	 */
	public Controller getController(String pageName, String clientKey) {
		logger.info("clientkey:"+clientKey+", pageName:"+pageName);
		Controller controller = null;
		String className = "com.cydcor.framework.controller." + clientKey + "." + pageName + "Controller";
		/*
		 * try { controller = (Controller)
		 * Class.forName(className).newInstance();
		 * 
		 * } catch (InstantiationException e) {
		 * logger.debug("unsupporter Class " + className + ": " +
		 * e.getMessage()); } catch (IllegalAccessException e) {
		 * logger.debug("unsupporter Class " + className + ": " +
		 * e.getMessage());
		 * 
		 * } catch (ClassNotFoundException e) {
		 * logger.debug("error instantiating Class " + className + ": " +
		 * e.getMessage()); }
		 */
		try {
			logger.debug("Initializing "+clientKey + pageName + "Controller");
			controller = (Controller) ServiceLocator.getService(clientKey + pageName + "Controller");
			logger.debug("Initizlied "+clientKey + pageName + "Controller");
			
		} catch (Exception e) {
			logger.error("exception while getting controller: ",e);
			logger.info("NOTE: In certain cases you may need not to worry about controller not found "
					+" because there may be other handler for such cases when controllers are not found.");
			logger.debug("Initializing "+ pageName + "Controller");
			try {
				controller = (Controller) ServiceLocator.getService(pageName + "Controller");
			} catch (Exception e1) {
				logger.error("Controller can't be initialized.",e1);
				return null;
			}
		}
		// h.init(params);
		return controller;
	}

}
