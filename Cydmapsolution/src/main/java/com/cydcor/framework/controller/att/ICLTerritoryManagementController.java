package com.cydcor.framework.controller.att;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.process.ProcessContext;
import com.cydcor.framework.process.ProcessExecutor;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

/**
 * 
 * Shows Assigned/Unassigned Leads and Applies template
 * 
 * @author ashwin
 * 
 */
@Controller("attICLTerritoryManagementController")
@Scope("singleton")
public class ICLTerritoryManagementController extends GenericController {

	public void execute(DynamicUIContext uiContext) {

		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance()
				.getRequestContext().getRequestWrapper();
		// String officeSeq =
		// CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("officeSeq");
		String pageDefnintion = "icl/ICLTerritoryManagementDef.html";
		String controlDefnintion = "icl/ICLTerritoryManagement.html";
		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
				"true"))) {
			clearOldPage(uiContext, pageDefnintion);
			disableMap(uiContext);

			// Add Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);

			uiContext.addControl(campaignSelectorControl);

			// Add Control the Template Selector
			GenericUIControl templateSelectorControl = new GenericUIControl();
			templateSelectorControl.setTemplateName(pageDefnintion);
			templateSelectorControl.addParameter("templateSelector", true);

			uiContext.addControl(templateSelectorControl);

			// Add selectedTemplateDetails the Template Selector
			GenericUIControl selectedTemplateDetailsControl = new GenericUIControl();
			selectedTemplateDetailsControl.setTemplateName(pageDefnintion);
			selectedTemplateDetailsControl.addParameter(
					"selectedTemplateDetails", true);

			uiContext.addControl(selectedTemplateDetailsControl);

			// Add UnassignLeads Control
			GenericUIControl unAssignLeadsTableControl = new GenericUIControl();
			unAssignLeadsTableControl.setTemplateName(pageDefnintion);
			unAssignLeadsTableControl.addParameter("unAssignLeadsTable", true);
			// iclTableControl.addParameter("officeSeq", officeSeq);

			uiContext.addControl(unAssignLeadsTableControl);

			// Add AssignLeads Control
			GenericUIControl assignLeadsTableControl = new GenericUIControl();
			assignLeadsTableControl.setTemplateName(pageDefnintion);
			assignLeadsTableControl.addParameter("assignLeadsTable", true);
			// iclTableControl.addParameter("officeSeq", officeSeq);

			uiContext.addControl(assignLeadsTableControl);

		} else {
			DatabaseResult dbResult = null;
			String queryString = null;
			/*String iclSql = "SELECT OFFICE_SEQ,OFFICE_NAME from IMS.MERLIN_ICL_DETAILS where CAMPAIGN_SEQ="
					+ requestWrapper.getParameter("campaignSeq");
			DatabaseResult iclResult = getPlatformService().loadResult(iclSql);*/
			List list1 = new ArrayList();
			list1.add(requestWrapper.getParameter("campaignSeq"));
			list1.toArray(new Object[0]);

			DatabaseResult iclResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get(
							"SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list1);
			if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
					"campaignSelector")) {
				/*queryString = "SELECT CAMPAIGN_SEQ,CAMPAIGN_NAME FROM IMS.MERLIN_CAMPAIGN_DETAILS";
				dbResult = getPlatformService().loadResult(queryString);*/
				
				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(campaignSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext
					.getControlName(), "templateSelector")) {
				/*queryString = "SELECT TEMPLATE_NAME FROM IMS.IMS_RULE_TEMPLATE_ASSIGN WHERE CAMPAIGN_SEQ="
						+ requestWrapper.getParameter("campaignSeq");
				dbResult = getPlatformService().loadResult(queryString);*/
				
				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.toArray(new Object[0]);
				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_CAMPAIGN_TEMPLATE_NAMES"),
						list);

				// Add Second Control the Campaign Selector
				GenericUIControl templateSelectorControl = new GenericUIControl();
				templateSelectorControl.setTemplateName(controlDefnintion);
				templateSelectorControl.addParameter("templateSelector", true);
				templateSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(templateSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext
					.getControlName(), "selectedTemplateDetails")) {
				/*queryString = "SELECT PARAM_NAME,PARAM_VALUE FROM IMS.IMS_TEMPLATE_PARAMS where TEMPLATE_NAME='"
						+ requestWrapper.getParameter("templateName") + "'";
				dbResult = getPlatformService().loadResult(queryString);*/
				
				dbResult = getPlatformService().loadCampaignTemplateParams(Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
						requestWrapper.getParameter("templateName"));
				
				 

				// Add Second Control the Campaign Selector
				GenericUIControl templateDetailsControl = new GenericUIControl();
				templateDetailsControl.setTemplateName(controlDefnintion);
				templateDetailsControl.addParameter("selectedTemplateDetails",
						true);
				templateDetailsControl.addParameter("result", dbResult);

				for (List row : dbResult.getData()) {
					MapObject paramNameColumn = (MapObject) row.get(0);
					MapObject paramValueColumn = (MapObject) row.get(1);
					templateDetailsControl.addParameter(paramNameColumn
							.getValue(), paramValueColumn.getValue());
				}

				uiContext.addControl(templateDetailsControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext
					.getControlName(), "unAssignLeadsTable")) {
				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.toArray(new Object[0]);

				TableUIControl unAssignLeadsCtrl = new TableUIControl();
				unAssignLeadsCtrl.setFilterParamList(list);
				unAssignLeadsCtrl.setQueryKeyName("SELECT_UNASSIGNED_LEADS_FOR_CAMPAIGN");
				unAssignLeadsCtrl.setColumnName("DA");
				unAssignLeadsCtrl.setJavaScriptMethodName("loadUnAssgnLeads");
				unAssignLeadsCtrl.addParameter("unAssignLeadsTable", true);
				unAssignLeadsCtrl.addParameter("icls", iclResult);
				unAssignLeadsCtrl.setTemplateName("../" + controlDefnintion);
				
				uiContext.addControl(unAssignLeadsCtrl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext
					.getControlName(), "assignLeadsTable")) {
				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.toArray(new Object[0]);

				TableUIControl assignLeadsCtrl = new TableUIControl();
				assignLeadsCtrl.setFilterParamList(list);
				assignLeadsCtrl.setQueryKeyName("SELECT_ASSIGNED_LEADS_FOR_CAMPAIGN");
				assignLeadsCtrl.setColumnName("DA");
				assignLeadsCtrl.setJavaScriptMethodName("loadAssgnLeads");
				assignLeadsCtrl.addParameter("assignLeadsTable", true);
				assignLeadsCtrl.addParameter("icls", iclResult);
				assignLeadsCtrl.setTemplateName("../" + controlDefnintion);

				uiContext.addControl(assignLeadsCtrl);

			}
		}
		// Actions
		if (!StringUtils.isBlank(requestWrapper.getParameter("action"))
				&& StringUtils.equalsIgnoreCase("processLeads", requestWrapper
						.getParameter("action"))) {
		    ProcessContext processContext = new ProcessContext();
		    processContext.addParameter("campaignSeq", requestWrapper
				.getParameter("campaignSeq"));
		    processContext.addParameter("templateName", requestWrapper
				.getParameter("templateName"));
		    processContext.addParameter("minNoOfLeadsInICL", requestWrapper
					.getParameter("minNoOfLeadsInICL"));
		    processContext.addParameter("maxNoOfLeadsInICL", requestWrapper
					.getParameter("maxNoOfLeadsInICL"));
		    
		   processContext.setProcessTaskName("AssignCampaignLeadByTerritoryTask_en_US.bsh");
		   ProcessExecutor.executeAssignmentProcess(processContext);
		   
		} else if (!StringUtils.isBlank(requestWrapper.getParameter("action"))
				&& StringUtils.equalsIgnoreCase("assignLead", requestWrapper
						.getParameter("action"))) {
			
			/*String sql = "UPDATE IMS.IMS_LEAD_LIFECYCLE set CAMPAIGN_SEQ=?, OFFICE_SEQ=?  where LEAD_ID=?;";
			getPlatformService().update(
					sql,
					new Object[] { requestWrapper.getParameter("campaignSeq"),
							requestWrapper.getParameter("officeSeq"),
							requestWrapper.getParameter("leadId") });*/

			getPlatformService().update(
					QueryUtils.getQuery().get(
							"UPDATE_IMS_LEAD_LIFECYCLE"), new Object[] {
									requestWrapper
									.getParameter("campaignSeq"),
							requestWrapper
									.getParameter("officeSeq"),
							requestWrapper
									.getParameter("rowId") });

		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
