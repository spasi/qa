package com.cydcor.framework.controller.verizonfios;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;

import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;
@Controller("verizonfiosMasterMapController")
@Scope("singleton")
public class MasterMapController extends GenericController {
    private static final transient Log logger = LogFactory.getLog(MasterMapController.class);
    
	@Override
	public void init(Object params) {
		// TODO Auto-generated method stub

	}

   public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

		String pageDefnintion = "leadsheet/verizonfios/MasterMapDef.html";
		String controlDefnintion = "leadsheet/verizonfios/MasterMap.html";
		String print = requestWrapper.getParameter("print");
		String disableBackLinks = requestWrapper.getParameter("disableBackLinks");
		String readOnly = requestWrapper.getParameter("readOnlyUser");
		String clientKey = requestWrapper.getParameter("clientKey");
		if (StringUtils.isBlank(print)) {
			print = "false";
		} else {
			if (StringUtils.isBlank(disableBackLinks))
				disableBackLinks = "true";
		}
		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
				"true"))) {
			Map<String, String> myParamMap = new HashMap<String, String>();
			if (StringUtils.equalsIgnoreCase("false", print)) {
				myParamMap.put("noPrint", "true");
			}
			myParamMap.put("clientKey", requestWrapper.getParameter("clientKey"));
			clearOldPage(uiContext, pageDefnintion, myParamMap);

			// Get Territory Type for Lead Sheet
			JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator.getService(clientKey + "JdbcTemplate");
			PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator
					.getService(clientKey + "TxManager");
			DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);
			List<String> resultList = jdbcTemplate.query(QueryUtils.getQuery().get("SELECT_TERRITORY_TYPE_LEADSHEET_"+requestWrapper.getParameter("type")), new Object[]{requestWrapper.getParameter("leadSheetId")},new RowMapper() {
				public Object mapRow(final ResultSet rs, final int rowNum) throws SQLException {
					String territoryType = rs.getString(1);
					return territoryType;
				}
			});
			String territoryType = resultList.get(0);
			territoryType = StringUtils.isNotBlank(territoryType)?territoryType:"CLLI";
			// Get Label for Master Map
			String sql = "";
			String mapLabel = territoryType.toUpperCase()+": ";	
			DatabaseResult dbLeadResult = null;
			if(territoryType.contains(":")){				
				mapLabel = territoryType.replace("|", "&").replace("'", "").replace(":", " : ");
			}else{
				if("SAVED".equalsIgnoreCase(requestWrapper.getParameter("type"))){
					if(territoryType.contains("|"))
						sql = "SELECT DISTINCT "+territoryType.split("\\|")[0]+","+territoryType.split("\\|")[1]+" FROM IMS.IMS_LEAD_LIFECYCLE WHERE LEAD_SHEET_ID = '"+requestWrapper.getParameter("leadSheetId")+"'";	
					else
						sql = "SELECT DISTINCT "+territoryType+" FROM IMS.IMS_LEAD_LIFECYCLE WHERE LEAD_SHEET_ID = '"+requestWrapper.getParameter("leadSheetId")+"'";
				}else{
					if(territoryType.contains("|"))
						sql = "SELECT DISTINCT LLC."+territoryType.split("\\|")[0]+","+territoryType.split("\\|")[1]+" FROM IMS.IMS_TEMP_LEADSHEET TL INNER JOIN IMS.IMS_LEAD_LIFECYCLE LLC ON "
							+ "TL.ROW_ID=LLC.ROW_ID WHERE TL.LEADSHEET_ID = '"+requestWrapper.getParameter("leadSheetId")+"'";
					else
						sql = "SELECT DISTINCT LLC."+territoryType+" FROM IMS.IMS_TEMP_LEADSHEET TL INNER JOIN IMS.IMS_LEAD_LIFECYCLE LLC ON "
								+ "TL.ROW_ID=LLC.ROW_ID WHERE TL.LEADSHEET_ID = '"+requestWrapper.getParameter("leadSheetId")+"'";
				}
				dbLeadResult = getPlatformService().loadResult(sql);
				for (List row : dbLeadResult.getData()) {
				    if(territoryType.contains("|"))
					    mapLabel = mapLabel+((MapObject)row.get(0)).getValue()+"&"+((MapObject)row.get(1)).getValue()+",";
				    else
				    	mapLabel = mapLabel+((MapObject)row.get(0)).getValue()+",";
				}
			    if (mapLabel.length() > 0 && mapLabel.charAt(mapLabel.length()-1)==',') {
			    	mapLabel=mapLabel.substring(0, mapLabel.length()-1);
			    }			
			}
			// Add Map Control
			GenericUIControl mapControl = new GenericUIControl();
			mapControl.setTemplateName(pageDefnintion);
			mapControl.addParameter("showLeadsMap", true);
			mapControl.addParameter("leadSheetId", requestWrapper.getParameter("leadSheetId"));
			mapControl.addParameter("clientKey", requestWrapper.getParameter("clientKey"));
			mapControl.addParameter("type", requestWrapper.getParameter("type"));
			mapControl.addParameter("campaignSeq", requestWrapper.getParameter("campaignSeq"));
			mapControl.addParameter("officeSeq", requestWrapper.getParameter("officeSeq"));
			mapControl.addParameter("territoryType", territoryType);
			mapControl.addParameter("mapLabel", mapLabel.toString());			
			uiContext.addControl(mapControl);
		}
   }
	@Override

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

}
