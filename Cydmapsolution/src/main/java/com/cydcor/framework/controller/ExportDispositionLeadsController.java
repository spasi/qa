package com.cydcor.framework.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

@Controller("ExportDispositionLeadsController")
@Scope("singleton")
public class ExportDispositionLeadsController extends GenericController {

	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

		String pageDefnintion = "lead/ExportDispositionLeadsDef.html";
		String controlDefination = "lead/ExportDispositionLeads.html";

		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
						"true"))) {
			clearOldPage(uiContext, pageDefnintion);

			// Add Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);
			uiContext.addControl(campaignSelectorControl);

			// Add Control the ICL Selector
			GenericUIControl iclSelectorControl = new GenericUIControl();
			iclSelectorControl.setTemplateName(pageDefnintion);
			iclSelectorControl.addParameter("iclSelector", true);
			uiContext.addControl(iclSelectorControl);

			// Add Control the leadListTable
			GenericUIControl leadListTable = new GenericUIControl();
			leadListTable.setTemplateName(pageDefnintion);
			leadListTable.addParameter("leadListTable", true);
			uiContext.addControl(leadListTable);

		} else {
			DatabaseResult dbResult = null;
			String queryString = null;
			if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefination);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);
				uiContext.addControl(campaignSelectorControl);

			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "iclSelector")) {

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.toArray(new Object[0]);

				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

				// Add Second Control the Campaign Selector
				GenericUIControl iclSelectorControl = new GenericUIControl();
				iclSelectorControl.setTemplateName(controlDefination);
				iclSelectorControl.addParameter("iclSelector", true);
				// iclSelectorControl.addParameter("templateSelector", true);
				iclSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(iclSelectorControl);
			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "leadListTable")) {
				String sql = QueryUtils.getQuery().get("ICL_REP_LEADLIST_FOR_EXPORT_XLS");
				List irlsInputs = new ArrayList();

				irlsInputs.add(requestWrapper.getParameter("dispositionStDate"));
				irlsInputs.add(requestWrapper.getParameter("dispositionEndDate"));
				irlsInputs.add(requestWrapper.getParameter("clientKey"));
				if (!StringUtils.equalsIgnoreCase("-1", requestWrapper.getParameter("campaignSeq"))) {
					sql = sql + " AND LF.CAMPAIGN_SEQ = ? ";
					irlsInputs.add(requestWrapper.getParameter("campaignSeq"));
				}

				if (!StringUtils.equalsIgnoreCase("-1", requestWrapper.getParameter("officeSeq"))) {
					sql = sql + " AND LF.OFFICE_SEQ=? ";
					irlsInputs.add(requestWrapper.getParameter("officeSeq"));
				}

				TableUIControl leadsDataTableControl = new TableUIControl();
				leadsDataTableControl.setFilterParamList(irlsInputs);
				leadsDataTableControl.setTableName(uiContext.getControlName());
				leadsDataTableControl.setQueryString(sql);
				leadsDataTableControl.setColumnName(" CAMPAIGN_SEQ ASC ");
				leadsDataTableControl.setJavaScriptMethodName("loadLeadList");
				leadsDataTableControl.setTemplateName("../" + controlDefination);
				leadsDataTableControl.setRowsPerPage(25);
				leadsDataTableControl.addParameter("leadListTable", true);

				uiContext.addControl(leadsDataTableControl);

			}
		}

	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
