package com.cydcor.framework.controller.centurylink;

import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.report.Exporter;
import com.cydcor.framework.report.impl.ExportContext;
import com.cydcor.framework.servlet.ReportServlet;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller("centurylinkLeadManagementController")
@Scope("singleton")
public class LeadManagementController extends GenericController {
	private static final transient Log logger = LogFactory.getLog(LeadManagementController.class);
	 private HttpServletRequest req;
	 private HttpServletResponse res;
	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
		
		String pageDefnintion = "lead/LeadManagementDef.html";
		String controlDefnintion = "lead/LeadManagement.html";
	  	Exporter exporter = (Exporter) ServiceLocator.getService("Exporter");
		ExportContext context = new ExportContext();
			  
		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
						"true"))) {
			// Add Second Control the Campaign Selector
			GenericUIControl leadOperationsControl = new GenericUIControl();
			leadOperationsControl.setTemplateName(pageDefnintion);
			leadOperationsControl.addParameter("leadOperations", true);
			uiContext.addControl(leadOperationsControl);
			
			
			GenericUIControl LeadApprovalListTableControl = new GenericUIControl();
			LeadApprovalListTableControl.setTemplateName(pageDefnintion);
			LeadApprovalListTableControl.addParameter("LeadApprovalList", true);
			LeadApprovalListTableControl.addParameter("tableName", "LeadApprovalList");
			uiContext.addControl(LeadApprovalListTableControl);
			
			GenericUIControl LeadownerApprovalListTableControl = new GenericUIControl();
			LeadownerApprovalListTableControl.setTemplateName(pageDefnintion);
			LeadownerApprovalListTableControl.addParameter("LeadOwnereApprovalList", true);
			LeadownerApprovalListTableControl.addParameter("tableName", "LeadOwnereApprovalList");
			uiContext.addControl(LeadownerApprovalListTableControl);
			
			GenericUIControl NewLeadownerListTableControl = new GenericUIControl();
			NewLeadownerListTableControl.setTemplateName(pageDefnintion);
			NewLeadownerListTableControl.addParameter("NewLeadOwnereApprovalList", true);
			NewLeadownerListTableControl.addParameter("tableName", "NewLeadOwnereApprovalList");
			uiContext.addControl(NewLeadownerListTableControl);
			
			
			
		} 
		
		 if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
				DatabaseResult dbResult = null;
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(campaignSelectorControl);

			    }
			
		    else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclSelector")) {
				// System.out.println("campaignSeq for icl selector: " +
				// requestWrapper.getParameter("campaignSeq"));
				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.toArray(new Object[0]);

				DatabaseResult dbResult = null;
				if (!requestWrapper.getParameter("campaignSeq").equalsIgnoreCase("-1"))
				    dbResult = getPlatformService().loadResult(
					    QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

				// Add Second Control the Campaign Selector
				GenericUIControl iclSelectorControl = new GenericUIControl();
				iclSelectorControl.setTemplateName(controlDefnintion);
				iclSelectorControl.addParameter("iclSelector", true);
				// iclSelectorControl.addParameter("templateSelector", true);
				iclSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(iclSelectorControl);
			    } 
			
		
		else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "disabledCities")) {

			DatabaseResult dbResult = null;
			dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_DISABLED_CITIES"));

			GenericUIControl citiesSelectorControl = new GenericUIControl();
			citiesSelectorControl.setTemplateName(controlDefnintion);
			citiesSelectorControl.addParameter("disabledCities", true);
			citiesSelectorControl.addParameter("result", dbResult);

			uiContext.addControl(citiesSelectorControl);

		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "leadTable")) {
			String mainString = "";

			int grpByIdx = 0;
			String queryString = "";
			List<Object> inputs = new ArrayList<Object>();
			TableUIControl leadUploadHistoryTableControl = new TableUIControl();
			// leadUploadHistoryTableControl.setDataSource("LMS");

			boolean whereAdded = false;

			mainString = QueryUtils.getQuery().get("SELCT_LEAD_MANAGEMENT").replaceAll("\\s*,\\s*", ", ");
			grpByIdx = mainString.lastIndexOf(" GROUP BY ");
			queryString = mainString.substring(0, grpByIdx);
			String grpString = mainString.substring(grpByIdx);
			String operator = "";
			int fromIndex = 0;
			if (StringUtils.isNotBlank(requestWrapper.getParameter("fieldsAndValues"))) {
				String[] fields = requestWrapper.getParameter("fieldsAndValues").split("::");
				for (String field : fields) {
					String values[] = field.split(":");
					fromIndex = queryString.indexOf(" FROM IMS.IMS_LEAD_LIFECYCLE L ");
					if (queryString.substring(fromIndex).indexOf(" WHERE ") == -1) {
						queryString = queryString + " WHERE ";
					} else {
						if (StringUtils.isNotBlank(values[0])) {
							queryString = queryString + " " + values[0] + " ";
						} else {
							queryString = queryString + " AND ";
						}

					}
					operator = values[2];
					boolean isSubQuery = false;
					if (values[1].equalsIgnoreCase("CAMPAIGN_SEQ")) {
						values[1] = "CAMPAIGN_SEQ IN (SELECT C.CAMPAIGN_SEQ FROM MERLIN.CAMPAIGNS C WHERE C.CAMPAIGN_NAME ";
						isSubQuery = true;
					}

					if (values[1].equalsIgnoreCase("DA")) {
						fromIndex = queryString.indexOf(" FROM IMS.IMS_LEAD_LIFECYCLE L ");
						queryString = queryString.substring(0, fromIndex) + ", L." + values[1]
								+ queryString.substring(fromIndex);
						grpString = grpString + ", DA";
						leadUploadHistoryTableControl.addParameter("isDAAdded", true);
					} else if (values[1].equalsIgnoreCase("ZIP")) {
						fromIndex = queryString.indexOf(" FROM IMS.IMS_LEAD_LIFECYCLE L ");
						queryString = queryString.substring(0, fromIndex) + ", L." + values[1]
								+ queryString.substring(fromIndex);
						grpString = grpString + ", ZIP";
						leadUploadHistoryTableControl.addParameter("isZIPAdded", true);
					} else if (values[1].equalsIgnoreCase("CITY")) {
						queryString = queryString.replaceAll("CLLI", "CITY, STATE");
						grpString = grpString.replaceAll("CLLI", "CITY, STATE");
						leadUploadHistoryTableControl.addParameter("isCityAdded", true);
						leadUploadHistoryTableControl.addParameter("isWCRemoved", true);
					}

					queryString = queryString + " L." + values[1] + " ";
					String value = values[3].trim();
					if (operator.equals("1")) {
						queryString = queryString + " = '" + value + "'";
					} else if (operator.equals("2")) {
						queryString = queryString + " != '" + value + "'";
					} else if (operator.equals("3")) {
						value = value.replaceAll("\\s*,\\s*", "','");
						value = "'" + value + "'";
						queryString = queryString + " IN ( " + value + " ) ";
					} else if (operator.equals("4")) {
						value = value.replaceAll("\\s*,\\s*", "','");
						value = "'" + value + "'";
						queryString = queryString + " NOT IN ( " + value + " ) ";
					} else if (operator.equals("5")) {
						queryString = queryString + " LIKE '%" + value + "%'";
					} else if (operator.equals("6")) {
						queryString = queryString + " NOT LIKE '%" + value + "%'";
					}
					if (isSubQuery) {
						queryString = queryString + " ) ";
					}

				}
			}
			boolean typeAdded = false;
			if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))) {
				String[] types = requestWrapper.getParameter("leadsType").split(":");
				if (types.length != 4) {
					for (String type : types) {
						if (type.equals("1")) {
							queryString = queryString + " AND ";
							if (!typeAdded)
								queryString = queryString + " ( ";
							queryString = queryString
									+ " ( L.LEAD_SHEET_ID IS NULL AND L.ROW_ID NOT IN ( SELECT TL.ROW_ID FROM IMS.IMS_TEMP_LEADSHEET TL WHERE TL.ROW_ID=L.ROW_ID))";
							typeAdded = true;
						} else if (type.equals("2")) {
							queryString = queryString + (typeAdded ? " OR " : " AND ");
							if (!typeAdded)
								queryString = queryString + " ( ";
							queryString = queryString
									+ " L.ROW_ID  IN ( SELECT TL.ROW_ID FROM IMS.IMS_TEMP_LEADSHEET TL WHERE TL.ROW_ID=L.ROW_ID)";
							typeAdded = true;
						} else if (type.equals("3")) {
							queryString = queryString + (typeAdded ? " OR " : " AND ");
							if (!typeAdded)
								queryString = queryString + " ( ";
							queryString = queryString + " ( L.LEAD_SHEET_ID IS NOT NULL AND L.PERSON_ID IS NULL)";
							typeAdded = true;
						} else if (type.equals("4")) {
							queryString = queryString + (typeAdded ? " OR " : " AND ");
							if (!typeAdded)
								queryString = queryString + " ( ";
							queryString = queryString + " ( L.LEAD_SHEET_ID IS NOT NULL AND L.PERSON_ID IS NOT NULL)";
							typeAdded = true;
						}
					}
				}
			}
			if (typeAdded)
				queryString = queryString + " ) ";
			fromIndex = queryString.indexOf(" FROM IMS.IMS_LEAD_LIFECYCLE L ");
			queryString = queryString.substring(0, fromIndex) + ", COUNT(*) AS TOTAT_COUNT "
					+ queryString.substring(fromIndex);

			leadUploadHistoryTableControl.setColumnName("UPLOAD_FILENAME");
			String leadsStatus = "";
			if ("Lead Enable".equalsIgnoreCase(requestWrapper.getParameter("operation"))) {
				leadsStatus = " AND IS_ACTIVE ='X' ";
			} else {
				leadsStatus = " AND IS_ACTIVE IN ('Y','I','E') ";
			}

			queryString = queryString + leadsStatus + grpString;

			leadUploadHistoryTableControl.setQueryString(queryString);
			leadUploadHistoryTableControl.setFilterParamList(inputs);
			leadUploadHistoryTableControl.setTableName(uiContext.getControlName());
			leadUploadHistoryTableControl.setRowsPerPage(20);
			leadUploadHistoryTableControl.setJavaScriptMethodName("refreshLeadTable");
			leadUploadHistoryTableControl.addParameter("leadTable", true);
			leadUploadHistoryTableControl.setTemplateName("../" + controlDefnintion);
			uiContext.addControl(leadUploadHistoryTableControl);
		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "reAssignLeads")) {
			String lifeCycleQueryString = "UPDATE IMS.IMS_LEAD_LIFECYCLE  SET CAMPAIGN_SEQ="
					+ requestWrapper.getParameter("campaignSeq") + ", OFFICE_SEQ="
					+ requestWrapper.getParameter("officeSeq");
			lifeCycleQueryString = getCriteria(requestWrapper, lifeCycleQueryString);
			String whereCondition = lifeCycleQueryString.substring(lifeCycleQueryString.indexOf(" WHERE "));
			String tmpLeadSheetsQueryString = "UPDATE IMS.IMS_TEMP_LEADSHEET SET CAMPAIGN_SEQ="
					+ requestWrapper.getParameter("campaignSeq") + ", OFFICE_SEQ="
					+ requestWrapper.getParameter("officeSeq")
					+ " WHERE ROW_ID IN (SELECT ROW_ID FROM IMS.IMS_LEAD_LIFECYCLE  " + whereCondition + " )";
			String leadSheetsQueryString = "UPDATE IMS.IMS_LEADSHEET SET CAMPAIGN_SEQ="
					+ requestWrapper.getParameter("campaignSeq") + ", OFFICE_SEQ="
					+ requestWrapper.getParameter("officeSeq")
					+ " WHERE LEADSHEET_ID IN (SELECT LEAD_SHEET_ID FROM IMS.IMS_LEAD_LIFECYCLE  " + whereCondition
					+ " )";

			GenericUIControl updateICLCtrl = new GenericUIControl();
			try {
				getPlatformService().update(lifeCycleQueryString, new Object[]{});
				if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))
						&& requestWrapper.getParameter("leadsType").contains("2")) {
					getPlatformService().update(tmpLeadSheetsQueryString, new Object[]{});
				}
				if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))
						&& (requestWrapper.getParameter("leadsType").contains("3") || requestWrapper.getParameter(
								"leadsType").contains("4"))) {
					getPlatformService().update(leadSheetsQueryString, new Object[]{});
				}
				Timestamp curTime = new Timestamp(System.currentTimeMillis());
				getPlatformService().update(
						QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
						new Object[]{curTime, requestWrapper.getParameter("userId"), "Lead Re-Assign",
								lifeCycleQueryString});
				getPlatformService().update(
						QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
						new Object[]{curTime, requestWrapper.getParameter("userId"), "Lead Re-Assign",
								tmpLeadSheetsQueryString});
				getPlatformService().update(
						QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
						new Object[]{curTime, requestWrapper.getParameter("userId"), "Lead Re-Assign",
								leadSheetsQueryString});

				updateICLCtrl.setResponseText("SUCCESS");
			} catch (Exception e) {
				logger.error(e);
				updateICLCtrl.setResponseText("ERROR");
			}
			uiContext.addControl(updateICLCtrl);

		} 
		else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "changeStatus")) {
			String status = "";
			if (requestWrapper.getParameter("action").equalsIgnoreCase("Enable")) {
				status = "L";
			} else {
				status = "X";
			}
			GenericUIControl updatestatusCtrl = new GenericUIControl();

			String lifeCycleQueryString = "UPDATE IMS.IMS_LEAD_LIFECYCLE  SET IS_ACTIVE='" + status + "' ";

			lifeCycleQueryString = getCriteria(requestWrapper, lifeCycleQueryString);
			String whereCondition = lifeCycleQueryString.substring(lifeCycleQueryString.indexOf(" WHERE "));
			String tmpLeadSheetsQueryString = "DELETE FROM IMS.IMS_TEMP_LEADSHEET WHERE ROW_ID IN ( SELECT ROW_ID FROM IMS.IMS_LEAD_LIFECYCLE  "
					+ whereCondition + " )";

			try {
				getPlatformService().update(lifeCycleQueryString, new Object[]{});
				if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))
						&& requestWrapper.getParameter("leadsType").contains("2") && status.equalsIgnoreCase("X")) {
					getPlatformService().update(tmpLeadSheetsQueryString, new Object[]{});
				}
				Timestamp curTime = new Timestamp(System.currentTimeMillis());
				if (requestWrapper.getParameter("statusChangeType").equals("byCity")) {
					if ("L".equalsIgnoreCase(status)) {
						String[] enableCityState = requestWrapper.getParameter("enableCity").split(":");
						String city = enableCityState[0];
						String state = enableCityState[1];
						String campaignSeq = enableCityState[2];
						getPlatformService().update(QueryUtils.getQuery().get("UPDATE_DISABLED_CITIES"),
								new Object[]{"N", requestWrapper.getParameter("userId"), curTime, city, state,
										campaignSeq});

					} else {
						String sql = "SELECT C.CAMPAIGN_SEQ FROM MERLIN.CAMPAIGNS C WHERE C.CAMPAIGN_NAME ='"+requestWrapper.getParameter("campaignName")+"'";
						// TODO
						DatabaseResult result = getPlatformService().loadResult(sql);
						Integer campaignSeq = Integer.valueOf(result.getData().get(0).get(0).getValue());
						getPlatformService().update(
								QueryUtils.getQuery().get("INSERT_DISABLED_CITIES"),
								new Object[]{requestWrapper.getParameter("city"), requestWrapper.getParameter("state"),
										campaignSeq,
										"Y", requestWrapper.getParameter("userId"), curTime,
										requestWrapper.getParameter("userId"), curTime});

					}
				} else {

					getPlatformService().update(
							QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
							new Object[]{curTime, requestWrapper.getParameter("userId"),
									requestWrapper.getParameter("action"), lifeCycleQueryString});
					getPlatformService().update(
							QueryUtils.getQuery().get("INSERT_ADMIN_LOG"),
							new Object[]{curTime, requestWrapper.getParameter("userId"),
									requestWrapper.getParameter("action"), tmpLeadSheetsQueryString});
				}
				updatestatusCtrl.setResponseText("SUCCESS");
			} catch (Exception e) {
				logger.error(e);
				updatestatusCtrl.setResponseText("ERROR");
			}
			uiContext.addControl(updatestatusCtrl);

		}
		
		
		 else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updateleadapproval")) 
		 {

			 	String DA = requestWrapper.getParameter("DA");
				String CLLI = requestWrapper.getParameter("CLLI");

				GenericUIControl updateRepCtrl = new GenericUIControl();
				try 
				{
				    getPlatformService().update(QueryUtils.getQuery().get("Update_Lead_Approval"),
					    new Object[] { DA, CLLI });
				    updateRepCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
				    logger.error(e);
				    updateRepCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateRepCtrl);
		}
		
		 else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updateleadowner")) 
		 {

			 	String OWNER = requestWrapper.getParameter("OWNER");
				String LEAD_ASSIGNED = requestWrapper.getParameter("LEAD_ASSIGNED");

				GenericUIControl updateRepCtrl = new GenericUIControl();
				try 
				{
				    getPlatformService().update(QueryUtils.getQuery().get("Update_Lead_Owner"),
					    new Object[] { OWNER, LEAD_ASSIGNED });
				    updateRepCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
				    logger.error(e);
				    updateRepCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateRepCtrl);
		}
		
		 else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updateleadnewowner")) 
		 {

			 	String OWNER = requestWrapper.getParameter("OWNER");
				String LEAD_ASSIGNED = requestWrapper.getParameter("LEAD_ASSIGNED");

				GenericUIControl updateRepCtrl = new GenericUIControl();
				try 
				{
				    getPlatformService().update(QueryUtils.getQuery().get("Update_NEW_Lead_Owner"),
					    new Object[] { OWNER, LEAD_ASSIGNED });
				    updateRepCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
				    logger.error(e);
				    updateRepCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateRepCtrl);
		}
		
		////code added by sanjay
		 else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "Selectleadapproval")) 
		 {
			 GenericUIControl updateNameCtrl = new GenericUIControl();
			 try
			 {
			 String[] DA = requestWrapper.getParameterValues("DA");
			 for(String s : DA)
			 {
				 System.out.println(s);
				 getPlatformService().update(QueryUtils.getQuery().get("Update_ALL_Lead_Approval"),
						    new Object[] {s});
					    updateNameCtrl.setResponseText("SUCCESS");
			 }
			 }
			 catch (Exception e) {
				    logger.error(e);
				    updateNameCtrl.setResponseText("ERROR");
				}
			 	uiContext.addControl(updateNameCtrl);

		}
		
		 else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "Selectleadownerapproval")) 
		 {
			 GenericUIControl updateNameCtrl = new GenericUIControl();
			 try
			 {
			 String[] DA = requestWrapper.getParameterValues("DA");
			 for(String s : DA)
			 {
				 System.out.println(s);
				 getPlatformService().update(QueryUtils.getQuery().get("Update_ALL_LeadOwner_Approval"),
						    new Object[] {s});
					    updateNameCtrl.setResponseText("SUCCESS");
			 }
			 }
			 catch (Exception e) {
				    logger.error(e);
				    updateNameCtrl.setResponseText("ERROR");
				}
			 	uiContext.addControl(updateNameCtrl);

		}
		
		
		 else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "SelectNEWleadownerapproval")) 
		 {
			 GenericUIControl updateNameCtrl = new GenericUIControl();
			 try
			 {
			 String[] DA = requestWrapper.getParameterValues("DA");
			 for(String s : DA)
			 {
				 System.out.println(s);
				 getPlatformService().update(QueryUtils.getQuery().get("Update_ALL_NEW_LeadOwner_Approval"),
						    new Object[] {s});
					    updateNameCtrl.setResponseText("SUCCESS");
			 }
			 }
			 catch (Exception e) {
				    logger.error(e);
				    updateNameCtrl.setResponseText("ERROR");
				}
			 	uiContext.addControl(updateNameCtrl);

		}
		
		 else if(StringUtils.equalsIgnoreCase(uiContext.getControlName(), "NewLeadOwnereApprovalList"))
		 {
			 TableUIControl NewLeadownerListTableControl = new TableUIControl();
			 String mainString = "";
				
			 String uploadStartDate = requestWrapper.getParameter("uploadStartDate");
			 String uploadEndDate = requestWrapper.getParameter("uploadEndDate");
			 String viewbylead = requestWrapper.getParameter("viewbylead");
			 String leadid = requestWrapper.getParameter("leadid");
				
			 int grpByIdx = 0;
			 String queryString = "";
			 List<Object> inputs = new ArrayList<Object>();
			 
			 if ("NEWLEADOWNER".equalsIgnoreCase(viewbylead))
				{	
					mainString = QueryUtils.getQuery().get("SELECT_LEAD_APPROVAL_LIST_LEAD_NEWOWNER");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					
					
					if (StringUtils.isNotBlank(uploadStartDate)
							&& StringUtils.isNotBlank(uploadEndDate)) {
						queryString = queryString + " where CREATED_DATE BETWEEN ? AND ? AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
						inputs.add((uploadStartDate));
						inputs.add((uploadEndDate));
						
					} else if (StringUtils.isNotBlank(uploadStartDate)) {
						queryString = queryString + " where CREATED_DATE = ? ";
						inputs.add((uploadStartDate));
					} else if (StringUtils.isNotBlank(uploadEndDate)) {
						queryString = queryString + " where CREATED_DATE = ? ";
						inputs.add((uploadEndDate));
					}
					
					queryString = queryString + mainString.substring(grpByIdx);
					NewLeadownerListTableControl.setColumnName("RUN_ID DESC,counts");
					NewLeadownerListTableControl.setQueryString(queryString);
					NewLeadownerListTableControl.setFilterParamList(inputs);
					NewLeadownerListTableControl.setTableName(uiContext.getControlName());
					NewLeadownerListTableControl.setRowsPerPage(20);
					NewLeadownerListTableControl.setJavaScriptMethodName("refreshleadapprovallist");
					NewLeadownerListTableControl.addParameter("NewLeadOwnereApprovalList", true);
					NewLeadownerListTableControl.setTemplateName("../" + controlDefnintion);
					uiContext.addControl(NewLeadownerListTableControl);
				}
			 
			 if(leadid != null)
				{
					mainString = QueryUtils.getQuery().get("SELECT_NEW_LEADOWNER_SEARCH_QUERY");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					
					
					if (StringUtils.isNotBlank(leadid)
							&& StringUtils.isNotBlank(leadid)) 
					{
						//queryString = queryString + " where CLLI LIKE " +  "'%" + leadid + "%'" + "  OR DA LIKE " +   "'%" + leadid + "%'" + "AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
					    //queryString = queryString + " where (MO.OWNER LIKE " + "'%" + leadid + "%')" + "AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
						  queryString = queryString + " where MO.OWNER LIKE " +  "'%" + leadid + "%'" + "  OR CSF_CURRENT_ICL_OWNERNAME LIKE " +   "'%" + leadid + "%'" + "AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
					} 
					
					queryString = queryString + mainString.substring(grpByIdx);
					NewLeadownerListTableControl.setColumnName("RUN_ID DESC,counts");
					NewLeadownerListTableControl.setQueryString(queryString);
					NewLeadownerListTableControl.setFilterParamList(inputs);
					NewLeadownerListTableControl.setTableName(uiContext.getControlName());
					NewLeadownerListTableControl.setRowsPerPage(20);
					NewLeadownerListTableControl.setJavaScriptMethodName("refreshleadapprovallist");
					NewLeadownerListTableControl.addParameter("NewLeadOwnereApprovalList", true);
					NewLeadownerListTableControl.setTemplateName("../" + controlDefnintion);
					uiContext.addControl(NewLeadownerListTableControl);
					
				}
			 
			 if ("TEST".equalsIgnoreCase(viewbylead))
				{
					mainString = QueryUtils.getQuery().get("SELECT_LEAD_APPROVAL_LIST_LEAD_NEWOWNER");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					
					
					if (StringUtils.isNotBlank(uploadStartDate)
							&& StringUtils.isNotBlank(uploadEndDate)) {
						queryString = queryString + " where CREATED_DATE BETWEEN ? AND ? AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
						inputs.add((uploadStartDate));
						inputs.add((uploadEndDate));
					} else if (StringUtils.isNotBlank(uploadStartDate)) {
						queryString = queryString + " where CREATED_DATE = ? ";
						inputs.add((uploadStartDate));
					} else if (StringUtils.isNotBlank(uploadEndDate)) {
						queryString = queryString + " where CREATED_DATE = ? ";
						inputs.add((uploadEndDate));
					}
					
					queryString = queryString + mainString.substring(grpByIdx);
					NewLeadownerListTableControl.setColumnName("RUN_ID DESC,counts");
					NewLeadownerListTableControl.setQueryString(queryString);
					NewLeadownerListTableControl.setFilterParamList(inputs);
					NewLeadownerListTableControl.setTableName(uiContext.getControlName());
					NewLeadownerListTableControl.setRowsPerPage(20);
					NewLeadownerListTableControl.setJavaScriptMethodName("refreshleadapprovallist");
					NewLeadownerListTableControl.addParameter("NewLeadOwnereApprovalList", true);
					NewLeadownerListTableControl.setTemplateName("../" + controlDefnintion);
					uiContext.addControl(NewLeadownerListTableControl);
				}
		 }
		
		 else if(StringUtils.equalsIgnoreCase(uiContext.getControlName(), "LeadOwnereApprovalList"))
		 {
			 TableUIControl LeadownerApprovalListTableControl = new TableUIControl();
			 String mainString = "";
				
			 String uploadStartDate = requestWrapper.getParameter("uploadStartDate");
			 String uploadEndDate = requestWrapper.getParameter("uploadEndDate");
			 String viewbylead = requestWrapper.getParameter("viewbylead");
			 String leadid = requestWrapper.getParameter("leadid");
				
			 int grpByIdx = 0;
			 String queryString = "";
			 List<Object> inputs = new ArrayList<Object>();
			 
			 if ("LEADOWNER".equalsIgnoreCase(viewbylead))
				{	
					mainString = QueryUtils.getQuery().get("SELECT_LEAD_APPROVAL_LIST_LEAD_OWNER");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					
					
					if (StringUtils.isNotBlank(uploadStartDate)
							&& StringUtils.isNotBlank(uploadEndDate)) {
						queryString = queryString + " where CREATED_DATE BETWEEN ? AND ? AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
						inputs.add((uploadStartDate));
						inputs.add((uploadEndDate));
						
					} else if (StringUtils.isNotBlank(uploadStartDate)) {
						queryString = queryString + " where CREATED_DATE = ? ";
						inputs.add((uploadStartDate));
					} else if (StringUtils.isNotBlank(uploadEndDate)) {
						queryString = queryString + " where CREATED_DATE = ? ";
						inputs.add((uploadEndDate));
					}
					
					queryString = queryString + mainString.substring(grpByIdx);
					LeadownerApprovalListTableControl.setColumnName("RUN_ID DESC,counts");
					LeadownerApprovalListTableControl.setQueryString(queryString);
					LeadownerApprovalListTableControl.setFilterParamList(inputs);
					LeadownerApprovalListTableControl.setTableName(uiContext.getControlName());
					LeadownerApprovalListTableControl.setRowsPerPage(20);
					LeadownerApprovalListTableControl.setJavaScriptMethodName("refreshleadapprovallist");
					LeadownerApprovalListTableControl.addParameter("LeadOwnereApprovalList", true);
					LeadownerApprovalListTableControl.setTemplateName("../" + controlDefnintion);
					uiContext.addControl(LeadownerApprovalListTableControl);
					
				}	
			 	
			 if(leadid != null)
				{
					mainString = QueryUtils.getQuery().get("SELECT_OWNER_SEARCH_QUERY");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					
					
					if (StringUtils.isNotBlank(leadid)
							&& StringUtils.isNotBlank(leadid)) 
					{
						//queryString = queryString + " where CLLI LIKE " +  "'%" + leadid + "%'" + "  OR DA LIKE " +   "'%" + leadid + "%'" + "AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
					   //queryString = queryString + " where (CSF_CURRENT_ICL_OWNERNAME LIKE " + "'%" + leadid + "%')" + "AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
					     queryString = queryString + " where CSF_CURRENT_ICL_OWNERNAME LIKE " +  "'%" + leadid + "%'" + "  OR MO.OWNER LIKE " +   "'%" + leadid + "%'" + "AND CSF_PROCESS_STATUS LIKE '%HLD%' ";

					} 
					
					queryString = queryString + mainString.substring(grpByIdx);
					LeadownerApprovalListTableControl.setColumnName("RUN_ID DESC,counts");
					LeadownerApprovalListTableControl.setQueryString(queryString);
					LeadownerApprovalListTableControl.setFilterParamList(inputs);
					LeadownerApprovalListTableControl.setTableName(uiContext.getControlName());
					LeadownerApprovalListTableControl.setRowsPerPage(20);
					LeadownerApprovalListTableControl.setJavaScriptMethodName("refreshleadapprovallist");
					LeadownerApprovalListTableControl.addParameter("LeadOwnereApprovalList", true);
					LeadownerApprovalListTableControl.setTemplateName("../" + controlDefnintion);
					uiContext.addControl(LeadownerApprovalListTableControl);
					
				}
				 if ("TEST".equalsIgnoreCase(viewbylead))
					{
						mainString = QueryUtils.getQuery().get("SELECT_LEAD_APPROVAL_LIST_LEAD_OWNER");
						grpByIdx = mainString.lastIndexOf(" GROUP BY ");
						queryString = mainString.substring(0, grpByIdx);
						
						
						if (StringUtils.isNotBlank(uploadStartDate)
								&& StringUtils.isNotBlank(uploadEndDate)) {
							queryString = queryString + " where CREATED_DATE BETWEEN ? AND ? AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
							inputs.add((uploadStartDate));
							inputs.add((uploadEndDate));
						} else if (StringUtils.isNotBlank(uploadStartDate)) {
							queryString = queryString + " where CREATED_DATE = ? ";
							inputs.add((uploadStartDate));
						} else if (StringUtils.isNotBlank(uploadEndDate)) {
							queryString = queryString + " where CREATED_DATE = ? ";
							inputs.add((uploadEndDate));
						}
						
						queryString = queryString + mainString.substring(grpByIdx);
						LeadownerApprovalListTableControl.setColumnName("RUN_ID DESC,counts");
						LeadownerApprovalListTableControl.setQueryString(queryString);
						LeadownerApprovalListTableControl.setFilterParamList(inputs);
						LeadownerApprovalListTableControl.setTableName(uiContext.getControlName());
						LeadownerApprovalListTableControl.setRowsPerPage(20);
						LeadownerApprovalListTableControl.setJavaScriptMethodName("refreshleadapprovallist");
						LeadownerApprovalListTableControl.addParameter("LeadOwnereApprovalList", true);
						LeadownerApprovalListTableControl.setTemplateName("../" + controlDefnintion);
						uiContext.addControl(LeadownerApprovalListTableControl);
					}
		
		 }
		else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "LeadApprovalList"))
		{
			TableUIControl LeadApprovalListTableControl = new TableUIControl();
			
			String mainString = "";
			
			String uploadStartDate = requestWrapper.getParameter("uploadStartDate");
			String uploadEndDate = requestWrapper.getParameter("uploadEndDate");
			String viewbylead = requestWrapper.getParameter("viewbylead");
			String leadid = requestWrapper.getParameter("leadid");
			
			int grpByIdx = 0;
			String queryString = "";
			List<Object> inputs = new ArrayList<Object>();
		
			
			
			if ("CLLI".equalsIgnoreCase(viewbylead))
			{	
				mainString = QueryUtils.getQuery().get("SELECT_LEAD_APPROVAL_LIST_CLLI");
				grpByIdx = mainString.lastIndexOf(" GROUP BY ");
				queryString = mainString.substring(0, grpByIdx);
				
				
				if (StringUtils.isNotBlank(uploadStartDate)
						&& StringUtils.isNotBlank(uploadEndDate)) {
					queryString = queryString + " where CREATED_DATE BETWEEN ? AND ? AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
					inputs.add((uploadStartDate));
					inputs.add((uploadEndDate));
					
				} else if (StringUtils.isNotBlank(uploadStartDate)) {
					queryString = queryString + " where CREATED_DATE = ? ";
					inputs.add((uploadStartDate));
				} else if (StringUtils.isNotBlank(uploadEndDate)) {
					queryString = queryString + " where CREATED_DATE = ? ";
					inputs.add((uploadEndDate));
				}
				
				queryString = queryString + mainString.substring(grpByIdx);
				LeadApprovalListTableControl.setColumnName("CLLI DESC,counts");
				LeadApprovalListTableControl.setQueryString(queryString);
				LeadApprovalListTableControl.setFilterParamList(inputs);
				LeadApprovalListTableControl.setTableName(uiContext.getControlName());
				LeadApprovalListTableControl.setRowsPerPage(20);
				LeadApprovalListTableControl.setJavaScriptMethodName("refreshleadapprovallist");
				LeadApprovalListTableControl.addParameter("LeadApprovalList", true);
				LeadApprovalListTableControl.setTemplateName("../" + controlDefnintion);
				uiContext.addControl(LeadApprovalListTableControl);
				
			}	
				

			if ("DA".equalsIgnoreCase(viewbylead))
			{	
				mainString = QueryUtils.getQuery().get("SELECT_LEAD_APPROVAL_LIST_DA");
				grpByIdx = mainString.lastIndexOf(" GROUP BY ");
				queryString = mainString.substring(0, grpByIdx);
				
				
				if (StringUtils.isNotBlank(uploadStartDate)
						&& StringUtils.isNotBlank(uploadEndDate)) {
					queryString = queryString + " where CREATED_DATE BETWEEN ? AND ? AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
					inputs.add((uploadStartDate));
					inputs.add((uploadEndDate));
				} else if (StringUtils.isNotBlank(uploadStartDate)) {
					queryString = queryString + " where CREATED_DATE = ? ";
					inputs.add((uploadStartDate));
				} else if (StringUtils.isNotBlank(uploadEndDate)) {
					queryString = queryString + " where CREATED_DATE = ? ";
					inputs.add((uploadEndDate));
				}
				
				queryString = queryString + mainString.substring(grpByIdx);
				LeadApprovalListTableControl.setColumnName("DA DESC,counts");
				LeadApprovalListTableControl.setQueryString(queryString);
				LeadApprovalListTableControl.setFilterParamList(inputs);
				LeadApprovalListTableControl.setTableName(uiContext.getControlName());
				LeadApprovalListTableControl.setRowsPerPage(20);
				LeadApprovalListTableControl.setJavaScriptMethodName("refreshleadapprovallist");
				LeadApprovalListTableControl.addParameter("LeadApprovalList", true);
				LeadApprovalListTableControl.setTemplateName("../" + controlDefnintion);
				uiContext.addControl(LeadApprovalListTableControl);
			}	
			
			if ("TEST".equalsIgnoreCase(viewbylead))
			{
				mainString = QueryUtils.getQuery().get("SELECT_LEAD_APPROVAL_LIST_REQUERY");
				grpByIdx = mainString.lastIndexOf(" GROUP BY ");
				queryString = mainString.substring(0, grpByIdx);
				
				
				if (StringUtils.isNotBlank(uploadStartDate)
						&& StringUtils.isNotBlank(uploadEndDate)) {
					queryString = queryString + " where CREATED_DATE BETWEEN ? AND ? AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
					inputs.add((uploadStartDate));
					inputs.add((uploadEndDate));
				} else if (StringUtils.isNotBlank(uploadStartDate)) {
					queryString = queryString + " where CREATED_DATE = ? ";
					inputs.add((uploadStartDate));
				} else if (StringUtils.isNotBlank(uploadEndDate)) {
					queryString = queryString + " where CREATED_DATE = ? ";
					inputs.add((uploadEndDate));
				}
				
				queryString = queryString + mainString.substring(grpByIdx);
				LeadApprovalListTableControl.setColumnName("RUN_ID DESC,counts");
				LeadApprovalListTableControl.setQueryString(queryString);
				LeadApprovalListTableControl.setFilterParamList(inputs);
				LeadApprovalListTableControl.setTableName(uiContext.getControlName());
				LeadApprovalListTableControl.setRowsPerPage(20);
				LeadApprovalListTableControl.setJavaScriptMethodName("refreshleadapprovallist");
				LeadApprovalListTableControl.addParameter("LeadApprovalList", true);
				LeadApprovalListTableControl.setTemplateName("../" + controlDefnintion);
				uiContext.addControl(LeadApprovalListTableControl);
				
				
			}
			if(leadid != null)
			{
				mainString = QueryUtils.getQuery().get("SELECT_LEAD_SEARCH_QUERY");
				grpByIdx = mainString.lastIndexOf(" GROUP BY ");
				queryString = mainString.substring(0, grpByIdx);
				
				
				if (StringUtils.isNotBlank(leadid) && StringUtils.isNotBlank(leadid)) 
				{
					// code changes by sanjay
					queryString = queryString + " where CLLI LIKE " +  "'%" + leadid + "%'" + "  OR DA LIKE " +   "'%" + leadid + "%'" + "AND CSF_PROCESS_STATUS LIKE '%HLD%' ";
				

				} 
				
				queryString = queryString + mainString.substring(grpByIdx);
				LeadApprovalListTableControl.setColumnName("RUN_ID DESC,counts");
				LeadApprovalListTableControl.setQueryString(queryString);
				LeadApprovalListTableControl.setFilterParamList(inputs);
				LeadApprovalListTableControl.setTableName(uiContext.getControlName());
				LeadApprovalListTableControl.setRowsPerPage(20);
				LeadApprovalListTableControl.setJavaScriptMethodName("SearchLead");
				LeadApprovalListTableControl.addParameter("LeadApprovalList", true);
				LeadApprovalListTableControl.setTemplateName("../" + controlDefnintion);
				uiContext.addControl(LeadApprovalListTableControl);
				
			}
				
		}
		
		  else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "IMS_LeadApproval_report")) 
		    {
				GenericUIControl Leadreport = new GenericUIControl();
				String reportName="Lead_Approval_";
				String mainString = "";
				String queryString = "";
			  try
			  {
				 
			  	 //List list = new ArrayList();
				 //list.add(requestWrapper.getParameter("DA"));
				// list.add(requestWrapper.getParameter("CLLI"));
				// DatabaseResult countResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_LeadApproval_report"), list);
				 //String count = countResult.getData().get(0).get(0).getValue();
				String DA = requestWrapper.getParameter("DA");
				String CLLI = requestWrapper.getParameter("CLLI");
				List<Object> inputs = new ArrayList<Object>();
				mainString = QueryUtils.getQuery().get("SELECT_IMS_LeadApproval_report");		    
				queryString = " WHERE DA = " + DA  + "  AND CLLI = " +  CLLI  + " AND CSF_PROCESS_STATUS LIKE '%HLD%' ORDER BY CLLI,DA,ZIP,APT_NUM ASC";
				
				queryString = mainString + queryString;  
				 logger.debug("final sql:"+ queryString);
				 context.setSqlString(queryString);
				 OutputStream outputStream = res.getOutputStream();
				 context.setExportStream(outputStream);
				 
					if (Exporter.XLS.equalsIgnoreCase(requestWrapper.getParameter("reportType"))) 
					{
						res.setHeader(
								"Content-Disposition",
								"attachment; filename=" + reportName.replaceAll(" ", "_") + "."
								+ requestWrapper.getParameter("reportType"));
					
						res.setContentType("application/vnd.ms-excel");
						context.setReportType(Exporter.XLS);
						exporter.export(context);
					}
				
					Leadreport.setResponseText("SUCCESS");
				 
				
				
			  }
			  catch (Exception e) {
				    logger.error(e);
				    Leadreport.setResponseText("ERROR");
				}
					uiContext.addControl(Leadreport);
			} 
		    
		
		else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "loadMergeOfficeList")){
			DatabaseResult result = null;
			List<Object> inputs = new ArrayList<Object>();
			TableUIControl mergeList = new TableUIControl();
			mergeList.setQueryString(QueryUtils.getQuery().get("SELECT_ADMIN_MERGE_OFFICES_LIST"));
			mergeList.setFilterParamList(inputs);
			mergeList.setColumnName("Active DESC");
			mergeList.setTableName(uiContext.getControlName());
			mergeList.setRowsPerPage(500);
			mergeList.setJavaScriptMethodName("loadMergeOfficePage");
			mergeList.addParameter("mergeOfficeList", true);
			String sqlOwnerCode = QueryUtils.getQuery().get("SELECT_ADMIN_OFFICE_CODE_LIST");
			result = getPlatformService().loadResult(sqlOwnerCode);
			mergeList.addParameter("resultOwnerCode", result);
			mergeList.setTemplateName("../" + controlDefnintion);
			uiContext.addControl(mergeList);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "loadSearchOfficeList")){
			DatabaseResult result = null;
			List<Object> inputs = new ArrayList<Object>();
			TableUIControl mergeList = new TableUIControl(); 
			String sqlSearchString = QueryUtils.getQuery().get("SELECT_ADMIN_MERGE_OFFICES_LIST");
			if(requestWrapper.getParameter("sOperation").equals("1")){
				sqlSearchString = sqlSearchString + " WHERE [Reps From Office] like '"+requestWrapper.getParameter("sText")+"%' OR [Reps To Office] like '"+requestWrapper.getParameter("sText")+"%' ";
			}else if(requestWrapper.getParameter("sOperation").equals("2")){
				sqlSearchString = sqlSearchString + " WHERE [Reps From Office] = '"+requestWrapper.getParameter("sText")+"' OR [Reps To Office] = '"+requestWrapper.getParameter("sText")+"' ";
			}else if(requestWrapper.getParameter("sOperation").equals("3")){
				sqlSearchString = sqlSearchString + " WHERE [Reps From Office] != '"+requestWrapper.getParameter("sText")+"' AND [Reps To Office] != '"+requestWrapper.getParameter("sText")+"' ";
			}
			mergeList.setQueryString(sqlSearchString);
			mergeList.setFilterParamList(inputs);
			mergeList.setColumnName("Active DESC");
			mergeList.setTableName(uiContext.getControlName());
			mergeList.setRowsPerPage(500);
			mergeList.setJavaScriptMethodName("loadMergeOfficePage");
			mergeList.addParameter("mergeOfficeList", true);
			String sqlOwnerCode = QueryUtils.getQuery().get("SELECT_ADMIN_OFFICE_CODE_LIST");
			result = getPlatformService().loadResult(sqlOwnerCode);
			mergeList.addParameter("resultOwnerCode", result);
			mergeList.setTemplateName("../" + controlDefnintion);
			uiContext.addControl(mergeList);
		}else if(StringUtils.equalsIgnoreCase(uiContext.getControlName(), "checkOwnerAvailability")){
			GenericUIControl checkOwner = new GenericUIControl();
			DatabaseResult result = null;
			String sqlOwnerCode = QueryUtils.getQuery().get("SELECT_ADMIN_MERGE_OFFICES_LIST");
			sqlOwnerCode = sqlOwnerCode+" WHERE [Reps From Office] = '"+requestWrapper.getParameter("fromOffice")+"' OR [Reps To Office] = '"+requestWrapper.getParameter("toOffice")+"' ";
			result = getPlatformService().loadResult(sqlOwnerCode);
			if(null != result && null != result.getData() && result.getData().size() != 0){
				checkOwner.setResponseText("SUCCESS");
			}else{
				checkOwner.setResponseText("ERROR");
			}
			uiContext.addControl(checkOwner);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "saveMergeOffice")){
			getPlatformService().upsertMergedOwners(requestWrapper.getParameter("fromOffice"), requestWrapper.getParameter("toOffice"), 
					requestWrapper.getParameter("userId"), requestWrapper.getParameter("active"), requestWrapper.getParameter("action"), 
					requestWrapper.getParameter("clientKey"));
			GenericUIControl addMergedOwner = new GenericUIControl();
			addMergedOwner.setTemplateName(controlDefnintion);
			uiContext.addControl(addMergedOwner);
		}else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "updateMergedOwner")){
			getPlatformService().upsertMergedOwners(requestWrapper.getParameter("fromOffice"), requestWrapper.getParameter("toOffice"), 
					requestWrapper.getParameter("userId"), requestWrapper.getParameter("active"), requestWrapper.getParameter("action"), 
					requestWrapper.getParameter("clientKey"));
			GenericUIControl addMergedOwner = new GenericUIControl();
			addMergedOwner.setTemplateName(controlDefnintion);
			uiContext.addControl(addMergedOwner);
		}
	}

	private String getCriteria(CydcorRequestWrapper requestWrapper, String queryString) {
		String operator = "";
		if (StringUtils.isNotBlank(requestWrapper.getParameter("fieldsAndValues"))) {
			String[] fields = requestWrapper.getParameter("fieldsAndValues").split("::");
			for (String field : fields) {
				String values[] = field.split(":");
				if (queryString.indexOf(" WHERE ") == -1) {
					queryString = queryString + " WHERE ";
				} else {
					if (StringUtils.isNotBlank(values[0])) {
						queryString = queryString + " " + values[0] + " ";
					} else {
						queryString = queryString + " AND ";
					}

				}
				operator = values[2];
				boolean isSubQuery = false;
				if (values[1].equalsIgnoreCase("CAMPAIGN_SEQ")) {
					values[1] = "CAMPAIGN_SEQ IN (SELECT C.CAMPAIGN_SEQ FROM MERLIN.CAMPAIGNS C WHERE C.CAMPAIGN_NAME ";
					isSubQuery = true;
				}

				queryString = queryString + values[1] + " ";
				String value = values[3].trim();
				if (operator.equals("1")) {
					queryString = queryString + " = '" + value + "'";
				} else if (operator.equals("2")) {
					queryString = queryString + " != '" + value + "'";
				} else if (operator.equals("3")) {
					value = value.replaceAll("\\s*,\\s*", "','");
					value = "'" + value + "'";
					queryString = queryString + " IN ( " + value + " ) ";
				} else if (operator.equals("4")) {
					value = value.replaceAll("\\s*,\\s*", "','");
					value = "'" + value + "'";
					queryString = queryString + " NOT IN ( " + value + " ) ";
				} else if (operator.equals("5")) {
					queryString = queryString + " LIKE '%" + value + "%'";
				} else if (operator.equals("6")) {
					queryString = queryString + " NOT LIKE '%" + value + "%'";
				}
				if (isSubQuery) {
					queryString = queryString + " ) ";
				}

			}
		}
		boolean typeAdded = false;
		if (StringUtils.isNotBlank(requestWrapper.getParameter("leadsType"))) {
			String[] types = requestWrapper.getParameter("leadsType").split(":");
			if (types.length != 4) {
				for (String type : types) {
					if (type.equals("1")) {
						queryString = queryString + " AND ";
						if (!typeAdded)
							queryString = queryString + " ( ";
						queryString = queryString
								+ " ( LEAD_SHEET_ID IS NULL AND ROW_ID NOT IN ( SELECT TL.ROW_ID FROM IMS.IMS_TEMP_LEADSHEET TL WHERE TL.ROW_ID=IMS_LEAD_LIFECYCLE.ROW_ID))";
						typeAdded = true;
					} else if (type.equals("2")) {
						queryString = queryString + (typeAdded ? " OR " : " AND ");
						if (!typeAdded)
							queryString = queryString + " ( ";
						queryString = queryString
								+ " ROW_ID  IN ( SELECT TL.ROW_ID FROM IMS.IMS_TEMP_LEADSHEET TL WHERE TL.ROW_ID=IMS_LEAD_LIFECYCLE.ROW_ID)";
						typeAdded = true;
					} else if (type.equals("3")) {
						queryString = queryString + (typeAdded ? " OR " : " AND ");
						if (!typeAdded)
							queryString = queryString + " ( ";
						queryString = queryString + " ( LEAD_SHEET_ID IS NOT NULL AND PERSON_ID IS NULL)";
						typeAdded = true;
					} else if (type.equals("4")) {
						queryString = queryString + (typeAdded ? " OR " : " AND ");
						if (!typeAdded)
							queryString = queryString + " ( ";
						queryString = queryString + " ( LEAD_SHEET_ID IS NOT NULL AND PERSON_ID IS NOT NULL)";
						typeAdded = true;
					}
				}
			}
		}
		if (typeAdded)
			queryString = queryString + " ) ";
		String leadsStatus = "";
		if ("Lead Enable".equalsIgnoreCase(requestWrapper.getParameter("operation"))) {
			leadsStatus = " AND IS_ACTIVE ='X' ";
		} else {
			leadsStatus = " AND IS_ACTIVE IN ('Y','I','E') ";
		}

		return queryString + leadsStatus;
	}
	// new code added by sanjay
	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
