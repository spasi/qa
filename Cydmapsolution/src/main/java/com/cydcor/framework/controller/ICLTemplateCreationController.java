package com.cydcor.framework.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;
@Controller("ICLTemplateCreationController")
@Scope("singleton")

public class ICLTemplateCreationController extends GenericController {

	DatabaseResult dbResult = null;
	String queryString = null;


	public void execute(DynamicUIContext uiContext) {
		// TODO Auto-generated method stub
		String campaignSeq = CydcorContext.getInstance().getRequestContext()
		.getRequestWrapper().getParameter("campaignSeq");

		String pageDefnintion = "icl/ICLTemplateCreationDef.html";
		String controlDefnintion = "icl/ICLTemplateCreation.html";
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance()
				.getRequestContext().getRequestWrapper();
		disableMap(uiContext);

		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
				"true"))) {
			// clearOldPage(uiContext, pageDefnintion);

			// Add Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);

			uiContext.addControl(campaignSelectorControl);

			// Add Control the Template Selector
			GenericUIControl templateSelectorControl = new GenericUIControl();
			templateSelectorControl.setTemplateName(pageDefnintion);
			templateSelectorControl.addParameter("templateSelector", true);

			uiContext.addControl(templateSelectorControl);

			// Add selectedTemplateDetails the Template Selector
			GenericUIControl selectedTemplateDetailsControl = new GenericUIControl();
			selectedTemplateDetailsControl.setTemplateName(pageDefnintion);
			selectedTemplateDetailsControl.addParameter(
					"selectedTemplateDetails", true);

			uiContext.addControl(selectedTemplateDetailsControl);

		} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
				"campaignSelector")) {

			dbResult=getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

			// Add Second Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(controlDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);
			campaignSelectorControl.addParameter("result", dbResult);

			uiContext.addControl(campaignSelectorControl);

		} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
				"templateSelector")) { 
			
			List list = new ArrayList();
			list.add(campaignSeq);					
			list.toArray(new Object[0]);
			dbResult=getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_TEMPLATE_NAMES"),list);

			// Add Second Control the Template Selector
			GenericUIControl templateSelectorControl = new GenericUIControl();
			templateSelectorControl.setTemplateName(controlDefnintion);
			templateSelectorControl.addParameter("templateSelector", true);
			templateSelectorControl.addParameter("result", dbResult);

			uiContext.addControl(templateSelectorControl);

		} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
				"selectedTemplateDetails")) { 

			List param = new ArrayList();

			param.add(requestWrapper.getParameter("campaignSeq"));
			param.add(requestWrapper.getParameter("templateName"));

			dbResult = getPlatformService().loadCampaignTemplateParams(Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
					requestWrapper.getParameter("templateName"));

			GenericUIControl selectedTemplateDetailsContol = new GenericUIControl();
			selectedTemplateDetailsContol.setTemplateName(controlDefnintion);
			selectedTemplateDetailsContol.addParameter("selectedTemplateDetails", true);
			selectedTemplateDetailsContol.addParameter("templateName",
					requestWrapper.getParameter("templateName"));
			
			if (dbResult.getData().size() > 0) {
				for (List<MapObject> row : dbResult.getData()) {
					selectedTemplateDetailsContol.addParameter(row.get(0).getValue(), row.get(1).getValue());
				}
			}
			
			uiContext.addControl(selectedTemplateDetailsContol);

		} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
				"saveTemplateDetails")) {

			campaignSeq = requestWrapper.getParameter("campaignSeq");
			
			Map<String, String> templateParams = new HashMap<String, String>();
			
			if(StringUtils.equalsIgnoreCase("true",requestWrapper.getParameter("AssignLeadsByInICL"))){
				templateParams.put("AssignLeadsByInICL",requestWrapper.getParameter("AssignLeadsByInICL"));
			}
			if(StringUtils.equalsIgnoreCase("true",requestWrapper.getParameter("checkMaxDistanceOfficeInICL"))){
				templateParams.put("checkMaxDistanceOfficeInICL",requestWrapper.getParameter("checkMaxDistanceOfficeInICL"));
				templateParams.put("MaxDistanceOfficeInICL",requestWrapper.getParameter("MaxDistanceOfficeInICL"));
			}
			if(StringUtils.equalsIgnoreCase("true",requestWrapper.getParameter("checkNoOfLeadsInICL"))){
				templateParams.put("checkNoOfLeadsInICL",requestWrapper.getParameter("checkNoOfLeadsInICL"));
				templateParams.put("minNoOfLeadsInICL",requestWrapper.getParameter("minNoOfLeadsInICL"));
				templateParams.put("maxNoOfLeadsInICL",requestWrapper.getParameter("maxNoOfLeadsInICL"));
			}
			/*if(StringUtils.equalsIgnoreCase("true",requestWrapper.getParameter("checkAssignLeadsByWireCenter"))){
				templateParams.put("checkAssignLeadsByWireCenter",requestWrapper.getParameter("checkAssignLeadsByWireCenter"));
			}*/
			
			
			

			long templateKey = -1;
			List params = new ArrayList();
			params.add(campaignSeq);
			params.add(requestWrapper.getParameter("templateName"));
			dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get("SELECT_IMS_CAMPAIGN_RULE_TEMPLATE"),
					params);

			for (List<MapObject> row : dbResult.getData()) {
				MapObject column = row.get(0);
				templateKey = Long.parseLong(column.getValue());
				break;
			}

			if (dbResult.getData().size() != 0) {
				String oldTemplateName=dbResult.getData().get(0).get(1).getValue();
				getPlatformService()
						.update(
								QueryUtils.getQuery().get(
										"DELETE_CAMPAIGN_TEMPLATE_PARAMS"),
								params.toArray());
				getPlatformService()
				.saveAndGetKey(	QueryUtils.getQuery().get(
								"UPDATE_IMS_CAMPAIGN_RULE_TEMPLATE"),
						new Object[] {requestWrapper.getParameter("templateName"),oldTemplateName,campaignSeq});
			}else{
				Date now = new Date();
				Timestamp cDate = new Timestamp(now.getTime());
				String templateType = "";
					
				templateKey = getPlatformService()
					.saveAndGetKey(	QueryUtils.getQuery().get(
									"INSERT_CAMPAIGN_TEMPLATE_RULES"),
							new Object[] { requestWrapper.getParameter("templateName"), templateType.trim(),
									Boolean.TRUE, cDate, "admin",
									cDate, "admin", campaignSeq});
			}
				for (Map.Entry<String, String> value : templateParams
					.entrySet()) {
				getPlatformService()
						.update(
								QueryUtils.getQuery().get(
										"INSERT_CAMPAIGN_TEMPLATE_PARAMS"),
								new Object[] { templateKey, requestWrapper.getParameter("templateName"),value.getKey(),
										value.getValue() });
				if(templateKey!=-1){
					GenericUIControl saveTemplateController = new GenericUIControl();
					saveTemplateController.setResponseText("Template Successfully Saved");
				}
			}
		} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
				"deleteTemplateDetails")) {

			List list = new ArrayList();
			list.add(requestWrapper.getParameter("campaignSeq"));
			list.add(requestWrapper.getParameter("templateName"));

			getPlatformService().update(
					QueryUtils.getQuery().get("DELETE_CAMPAIGN_TEMPLATE_PARAMS"),
					list.toArray());

			getPlatformService().update(
					QueryUtils.getQuery().get("DELETE_CAMPAIGN_RULE_TEMPLATE"),
					list.toArray());
		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub
	}
}