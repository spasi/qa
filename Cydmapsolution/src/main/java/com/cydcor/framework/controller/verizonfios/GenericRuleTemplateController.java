package com.cydcor.framework.controller.verizonfios;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

/**
 * @author ashwin
 * 
 */
@Controller("verizonfiosGenericRuleTemplateController")
@Scope("singleton")
public class GenericRuleTemplateController extends GenericController {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.controller.Controller#execute(com.cydcor.framework
	 * .context.DynamicUIContext)
	 */
	public void execute(DynamicUIContext uiContext) {
		String pageDefnintion = "icl/verizonfios/GenericRuleTemplate.html";
		String controlDefnintion = "icl/verizonfios/components.xml";

		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

		String campaignSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter(
				"campaignSeq");

		String officeSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper()
				.getParameter("officeSeq");
		String templateName = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter(
				"templateName");

		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
						"true"))) {
			// Add Control the Campaign Selector
			clearOldPage(uiContext, pageDefnintion);
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);

			uiContext.addControl(campaignSelectorControl);

			// Add Control the ICL Selector
			GenericUIControl iclSelectorControl = new GenericUIControl();
			iclSelectorControl.setTemplateName(pageDefnintion);
			iclSelectorControl.addParameter("iclSelector", true);

			uiContext.addControl(iclSelectorControl);

			// Add templateSelector Control
			GenericUIControl templateSelectorControl = new GenericUIControl();
			templateSelectorControl.setTemplateName(pageDefnintion);
			templateSelectorControl.addParameter("templateSelector", true);

			uiContext.addControl(templateSelectorControl);

			// Add templateSelector Control
			GenericUIControl templateHolderControl = new GenericUIControl();
			templateHolderControl.setTemplateName(pageDefnintion);
			templateHolderControl.addParameter("templateHolderDiv", true);

			uiContext.addControl(templateHolderControl);

			// // Add templateSelector Control
			// GenericUIControl templateColorCodeHolderControl = new
			// GenericUIControl();
			// templateColorCodeHolderControl.setTemplateName(pageDefnintion);
			// templateColorCodeHolderControl.addParameter(
			// "templateColorCodeHolderDiv", true);
			//
			// uiContext.addControl(templateColorCodeHolderControl);

		} else {
			DatabaseResult dbResult = null;
			String queryString = null;
			if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(campaignSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclSelector")) {

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.toArray(new Object[0]);

				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

				// Add Second Control the Campaign Selector
				GenericUIControl iclSelectorControl = new GenericUIControl();
				iclSelectorControl.setTemplateName(controlDefnintion);
				iclSelectorControl.addParameter("iclSelector", true);
				// iclSelectorControl.addParameter("templateSelector", true);
				iclSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(iclSelectorControl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "templateSelector")) {

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				// officeSeq
				list.add(requestWrapper.getParameter("officeSeq"));
				list.toArray(new Object[0]);

				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TEMPLATE_NAMES"), list);

				// Add Second Control the Template Selector
				GenericUIControl templateSelectorControl = new GenericUIControl();
				templateSelectorControl.setTemplateName(controlDefnintion);
				// templateSelectorControl.addParameter("campaignSeq",
				// campaignSeq);
				templateSelectorControl.addParameter("templateSelector", true);
				templateSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(templateSelectorControl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveTemplate")) {

				campaignSeq = requestWrapper.getParameter("campaignSeq");
				officeSeq = requestWrapper.getParameter("officeSeq");
				String templateNameModified = requestWrapper.getParameter("templateNameModified");
				Map<String, String> templateParams = new HashMap<String, String>();

				Enumeration<String> keys = requestWrapper.getParameterNames();
				String templateSetupvalue = requestWrapper.getParameter("templateSetup");
				if (templateSetupvalue != null) {
					if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byNoOfLeads")) {
						templateParams.put("byNoOfLeads", "true");
						String noOfLeads = requestWrapper.getParameter("NoOfLeads");
						// if(StringUtils.isNotBlank(noOfLeads)){
						templateParams.put("NoOfLeads", noOfLeads); 
						// }
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byDA")) {
						templateParams.put("byDA", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsByDAInSet");
						// if(StringUtils.isNotBlank(maxLeads)){
						templateParams.put("maxLeadsByDAInSet", maxLeads);
						// }
					} 
					
					
					else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byDACRR")) {
						templateParams.put("byDACRR", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsByDACRRInSet");
						// if(StringUtils.isNotBlank(maxLeads)){
						templateParams.put("maxLeadsByDACRRInSet", maxLeads);
						// }
					} 
					
					
					else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byPS01")) {
						templateParams.put("byPS01", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsByPS01InSet");
						// if(StringUtils.isNotBlank(maxLeads)){
						templateParams.put("maxLeadsByPS01InSet", maxLeads);
						// }
					}
					
					else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byWireCenter")) {
						templateParams.put("byWireCenter", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsByWireCenterInSet");
						// if(StringUtils.isNotBlank(maxLeads)){
						templateParams.put("maxLeadsByWireCenterInSet", maxLeads);
						// }
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byZip")) {
						templateParams.put("byZip", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsbyZipInSet");
						// if(StringUtils.isNotBlank(maxLeads)){
						templateParams.put("maxLeadsbyZipInSet", maxLeads);
						// }
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byCity")) {
						templateParams.put("byCity", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsByCity");
						// if(StringUtils.isNotBlank(maxLeads)){
						templateParams.put("maxLeadsbyCityInSet", maxLeads);
						// }
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byMDU")) {
						templateParams.put("byMDU", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsByMDU");
						// if(StringUtils.isNotBlank(maxLeads)){
						templateParams.put("maxLeadsbyMDUInSet", maxLeads);
						// }
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byNewLead")) {
						templateParams.put("byNewLead", "true");
						//String maxLeads = requestWrapper.getParameter("maxLeadsbyNewLeadInSet");
						// if(StringUtils.isNotBlank(maxLeads)){
						//templateParams.put("maxLeadsbyNewLeadInSet", maxLeads);

						templateParams.put("selectedTVNewsNames",requestWrapper.getParameter("selectedTVNewsNames"));
						String selectedTVNews = requestWrapper.getParameter("selectedTVNews");
						String val = "";
						for (String product : selectedTVNews.split("~~")) {
							val = val + ":" + product  ;
						}
						templateParams.put("maxLeadsbyNewLeadInSet", requestWrapper.getParameter("maxLeadsbyNewLeadInSet")+val);
					
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byLFA")) {
						templateParams.put("byLFA", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsbyLFAInSet");
						templateParams.put("maxLeadsbyLFAInSet", maxLeads);
						String LFAValue = requestWrapper.getParameter("LFAValue");
						templateParams.put("LFAValue", LFAValue);
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byClusterYN")) {
						templateParams.put("byClusterYN", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsbyClusterYNInSet");
						templateParams.put("maxLeadsbyClusterYNInSet", maxLeads);
						String ClusterYN = requestWrapper.getParameter("ClusterYNValue");
						templateParams.put("ClusterYNValue", ClusterYN);
					}else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byIont")) {
						templateParams.put("byIont", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsbyIontInSet");
						templateParams.put("maxLeadsbyIontInSet", maxLeads);
						String IontValue = requestWrapper.getParameter("IontValue");
						templateParams.put("IontValue", IontValue);
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byPropID")) {
						templateParams.put("byPropID", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsbyPropIDInSet");
						templateParams.put("maxLeadsbyPropIDInSet", maxLeads);
						String PropIDValue = requestWrapper.getParameter("PropIDValue");
						templateParams.put("PropIDValue", PropIDValue);
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byPromo")) {
						templateParams.put("byPromo", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsbyPromoInSet");
						templateParams.put("maxLeadsbyPromoInSet", maxLeads);
						String PromoValue = requestWrapper.getParameter("PromoValue");
						templateParams.put("PromoValue", PromoValue);
						String PromoValueText = requestWrapper.getParameter("PromoValueText");
						templateParams.put("PromoValueText", PromoValueText);

					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byCluster")) {
						templateParams.put("byCluster", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsbyClusterInSet");
						templateParams.put("maxLeadsbyClusterInSet", maxLeads);
						String clusterValue = requestWrapper.getParameter("ClusterValue");
						//templateParams.put("iONTSSValue", PromoValue.replaceAll("C", ""));
						templateParams.put("ClusterValue", "Cluster_"+clusterValue);

					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byLeadProximity")) {
						templateParams.put("byLeadProximity", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsbyLeadProximityInSet");
						// if(StringUtils.isNotBlank(maxLeads)){
						templateParams.put("maxLeadsbyLeadProximityInSet", maxLeads);
						// }
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byProducts")) {
						templateParams.put("byProducts", "true");
						templateParams.put("selectedProductsNames",requestWrapper.getParameter("selectedProductsNames"));
						String selectedProducts = requestWrapper.getParameter("selectedProducts");
						String val = "";
						for (String product : selectedProducts.split("~~")) {
							val = val + ":" + product  ;
						}
						templateParams.put("maxLeadsByProducts", requestWrapper.getParameter("maxLeadsByProducts")+val);
					} else if (StringUtils.equalsIgnoreCase(templateSetupvalue, "byBlockMdu")) {
						templateParams.put("byBlockMdu", "true");
						String maxLeads = requestWrapper.getParameter("maxLeadsbyBlockMduInSet");
						// if(StringUtils.isNotBlank(maxLeads)){
						templateParams.put("maxLeadsbyBlockMduInSet", maxLeads);
						// }
					}
				}
				/*
				 * while (keys.hasMoreElements()) { String key =
				 * keys.nextElement(); if (StringUtils.startsWithIgnoreCase(key,
				 * "check")) { if (StringUtils.equalsIgnoreCase("true",
				 * requestWrapper .getParameter(key))) { String valueKey =
				 * key.replace("check", ""); if
				 * (StringUtils.endsWithIgnoreCase(valueKey, "InSet")) {
				 * Enumeration<String> subLoopKeys = requestWrapper
				 * .getParameterNames(); while (subLoopKeys.hasMoreElements()) {
				 * String subLoopKey = subLoopKeys .nextElement(); if
				 * (StringUtils.startsWithIgnoreCase( subLoopKey, "min" +
				 * valueKey) || StringUtils .startsWithIgnoreCase( subLoopKey,
				 * "max" + valueKey)) { templateParams.put(subLoopKey,
				 * requestWrapper.getParameter( subLoopKey).trim()); } } } else
				 * { templateParams.put(valueKey, requestWrapper
				 * .getParameter(valueKey).trim()); } } } }
				 */
				String defaultCheck = requestWrapper.getParameter("defaultValue");

				long templateKey = -1;
				List params = new ArrayList();
				params.add(campaignSeq);
				params.add(officeSeq);
				params.add(requestWrapper.getParameter("templateName"));
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_RULE_TEMPLATE"),
						params);
				Date now = new Date();
				Timestamp cDate = new Timestamp(now.getTime());
				for (List<MapObject> row : dbResult.getData()) {
					MapObject column = row.get(0);
					templateKey = Long.parseLong(column.getValue());
					break;
				}
				if (StringUtils.equalsIgnoreCase(requestWrapper.getParameter("templateName"), requestWrapper
						.getParameter("templateID"))) {
					if (dbResult.getData().size() != 0) {
						getPlatformService().update(QueryUtils.getQuery().get("DELETE_TEMPLATE_PARAMS"),
								params.toArray());
					}
					String templateType = "LEAD_SHEET";
					if (defaultCheck.equalsIgnoreCase("true")) {
						try {
							templateType = "";
							try {
								getPlatformService().saveAndGetKey(
										QueryUtils.getQuery().get("UPDATE_IMS_RULE_TEMPLATE"),
										new Object[] { campaignSeq, officeSeq });
							} catch (Exception e) {

							}
							templateType = "DEFAULT";
							getPlatformService().update(
									QueryUtils.getQuery().get("UPDATE_RULE_TEMPLATE_DATA"),
									new Object[] { templateType.trim(), Boolean.TRUE, cDate, "admin", cDate, "admin",
											campaignSeq, officeSeq, requestWrapper.getParameter("templateName") });
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						try {
							templateType = "LEAD_SHEET";
							getPlatformService().update(
									QueryUtils.getQuery().get("UPDATE_RULE_TEMPLATE_DATA"),
									new Object[] { templateType.trim(), Boolean.TRUE, cDate, "admin", cDate, "admin",
											campaignSeq, officeSeq, requestWrapper.getParameter("templateName") });
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				} else {
					String templateType = "LEAD_SHEET";

					if (defaultCheck.equalsIgnoreCase("true")) {
						templateType = "";
						getPlatformService().saveAndGetKey(QueryUtils.getQuery().get("UPDATE_IMS_RULE_TEMPLATE"),
								new Object[] { campaignSeq, officeSeq });

						templateType = "DEFAULT";
						templateKey = getPlatformService().saveAndGetKey(
								QueryUtils.getQuery().get("INSERT_TEMPLATE_RULES"),
								new Object[] { requestWrapper.getParameter("templateName"), templateType.trim(),
										Boolean.TRUE, cDate, "admin", cDate, "admin", campaignSeq, officeSeq });
					} else {
						templateKey = getPlatformService().saveAndGetKey(
								QueryUtils.getQuery().get("INSERT_TEMPLATE_RULES"),
								new Object[] { requestWrapper.getParameter("templateName"), templateType.trim(),
										Boolean.TRUE, cDate, "admin", cDate, "admin", campaignSeq, officeSeq });
					}
				}

				for (Map.Entry<String, String> value : templateParams.entrySet()) {
					getPlatformService().update(
							QueryUtils.getQuery().get("INSERT_TEMPLATE_PARAMS"),
							new Object[] { templateKey, requestWrapper.getParameter("templateName"), value.getKey(),
									value.getValue() });
				}
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "templateHolderDiv")) {
				List param = new ArrayList();

				param.add(requestWrapper.getParameter("campaignSeq"));
				// officeSeq
				param.add(requestWrapper.getParameter("officeSeq"));
				param.add(requestWrapper.getParameter("templateName"));

				dbResult = getPlatformService().loadTemplateParams(
						Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
						Integer.parseInt(requestWrapper.getParameter("officeSeq")),
						requestWrapper.getParameter("templateName"));
				// Add Second Control the Template Selector
				GenericUIControl templateHolderDivControl = new GenericUIControl();
				templateHolderDivControl.setTemplateName(controlDefnintion);
				templateHolderDivControl.addParameter("templateHolderDiv", true);
				templateHolderDivControl.addParameter("templateName", requestWrapper.getParameter("templateName"));
				if (dbResult.getData().size() > 0) {
					for (List<MapObject> row : dbResult.getData()) {

						templateHolderDivControl.addParameter(row.get(0).getValue(), row.get(1).getValue());

					}
				}
				List list2 = new ArrayList();
				list2.add(requestWrapper.getParameter("campaignSeq"));
				list2.add(requestWrapper.getParameter("officeSeq"));
				list2.add(requestWrapper.getParameter("templateName"));
				list2.toArray(new Object[0]);

				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_TEMPLATE_TYPE_IMS_RULE_TEMPLATE"), list2);

				String check = "";

				if (dbResult.getData().size() > 0) {
					for (List<MapObject> row : dbResult.getData()) {

						if (StringUtils.equalsIgnoreCase("DEFAULT", row.get(0).getValue().trim())) {
							check = "checked";
						} else {
							check = "";
						}

						break;
					}
				}

				templateHolderDivControl.addParameter("defaultValue", check);
				List list1 = new ArrayList();
				list1.add(requestWrapper.getParameter("officeSeq"));
				list1.add(requestWrapper.getParameter("campaignSeq"));
				list1.toArray(new Object[0]);

				DatabaseResult result1 = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_LEADS_NUMBER"), list1);
				String defleads = "";
				String maxleads = "";
				if (result1.getData().size() > 0) {
					for (List<MapObject> row : result1.getData()) {
						if (row.get(0).getValue() != null) {
							defleads = row.get(0).getValue();
						}
						if (row.get(1).getValue() != null) {
							maxleads = row.get(1).getValue();
						}
						break;
					}
				}
				templateHolderDivControl.addParameter("defleads", defleads);
				templateHolderDivControl.addParameter("maxleads", maxleads);
				list1.clear();
				list1.add(requestWrapper.getParameter("campaignSeq"));
				list1.add("PROMO_CODE");
				DatabaseResult promoResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_LOOKUP_VALUES"), list1);
				templateHolderDivControl.addParameter("promoResult", promoResult);

				uiContext.addControl(templateHolderDivControl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "deleteTemplate")) {

				List params = new ArrayList();
				params.add(requestWrapper.getParameter("campaignSeq"));
				params.add(requestWrapper.getParameter("officeSeq"));
				params.add(requestWrapper.getParameter("templateName"));

				getPlatformService().update(QueryUtils.getQuery().get("DELETE_TEMPLATE_PARAMS"), params.toArray());

				getPlatformService().update(QueryUtils.getQuery().get("DELETE_RULE_TEMPLATE"), params.toArray());
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "loadProducts")) {
				GenericUIControl selectedProducts = new GenericUIControl();
				selectedProducts.setTemplateName(controlDefnintion);
				selectedProducts.addParameter("loadProducts", true);
				selectedProducts.addParameter("templateName", requestWrapper.getParameter("templateName"));

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));

				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_CAMPAIGN_PRODUCTS"),
						list);
				selectedProducts.addParameter("result", dbResult);
				uiContext.addControl(selectedProducts);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "loadTVNew")) {
				GenericUIControl selectedProducts = new GenericUIControl();
				selectedProducts.setTemplateName(controlDefnintion);
				selectedProducts.addParameter("loadTVNew", true);
				selectedProducts.addParameter("templateName", requestWrapper.getParameter("templateName"));

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));

				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_CAMPAIGN_TVNEW"),
						list);
				selectedProducts.addParameter("result", dbResult);
				uiContext.addControl(selectedProducts);
			}

		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
