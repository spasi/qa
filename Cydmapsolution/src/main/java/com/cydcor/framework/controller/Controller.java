/**
 * 
 */
package com.cydcor.framework.controller;

import java.util.Map;

import com.cydcor.framework.context.DynamicUIContext;

/**
 * @author ashwin
 * 
 */
public interface Controller {

	/**
	 * Any Initilizaton Sequence Has to be Run Here
	 * @param params
	 */
	public void init(Object params);

	/**
	 * @param uiContext
	 */
	public void execute(DynamicUIContext uiContext);

	/**
	 * @param imput
	 * @param paramMap 
	 * @return
	 */

	public Object getResult(Object imput, Map<String, Object> paramMap);
}
