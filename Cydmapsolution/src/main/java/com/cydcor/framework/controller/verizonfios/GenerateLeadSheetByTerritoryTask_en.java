package com.cydcor.framework.controller.verizonfios;
import com.cydcor.framework.task.Task;
import com.cydcor.framework.task.TaskContext;
import com.cydcor.framework.process.ProcessContext;  
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.utils.ServiceLocator;

import org.apache.commons.lang.StringUtils;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.model.TempLeadSheet;
  
public class GenerateLeadSheetByTerritoryTask_en 
{
	
	
	public void execute(TaskContext context) {
		// TODO Auto-generated method stub
		String clientKey = (String) context.getProcessContext().getParameters().get("clientKey");
				if (clientKey == null)
				clientKey = "";
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator
				.getService(clientKey+"JdbcTemplate");
		PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator
				.getService(clientKey+"TxManager");
		DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);

		String campaignSeq = (String) context.getProcessContext()
				.getParameters().get("campaignSeq");
		String officeSeq = (String) context.getProcessContext().getParameters()
				.get("iclID");
		int noOfLeadsAssgn = 0;
		
		List unassignedLeads = null;
		String type = "";
		
		String territoryType = (String)context.getProcessContext().getParameters().get("territoryType");
		
		try {
			//System.out.println(context.getProcessContext().getParameters().get("NoOfLeads"));
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("NoOfLeads"));

		} catch (Exception e) {
			context.addErrorLogs("Max Number of leads not there");
			context.addErrorLogs(e.getMessage());
			noOfLeadsAssgn = 9999;
		}

		if (context.getProcessContext().getParameters().get(
				"maxLeadsByWireCenterInSet") != null) {
			type = "byWC";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsByWireCenterInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByWCUsingFiles((String) context
					.getProcessContext().getParameters().get("campaignSeq"),
					(String) context.getProcessContext().getParameters().get(
							"iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			
		}  else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyZipInSet") != null) {
			type = "byZip";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyZipInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByZipUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			
		} else if (context.getProcessContext().getParameters().get("NoOfLeads") != null) {
			type = "byNoOfLeads";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("NoOfLeads"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsUsingFiles((String) context
					.getProcessContext().getParameters().get("campaignSeq"),
					(String) context.getProcessContext().getParameters().get(
							"iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			
		} else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyNewLeadInSet") != null) {
			type = "byNewLeads";
			String[] vals = ((String)context.getProcessContext().getParameters().get("maxLeadsbyNewLeadInSet")).split(":");
			System.out.println(vals+":"+vals[0]);
			noOfLeadsAssgn = new Integer((String) vals[0]);
			unassignedLeads = dbUtil.selectUnAssingedLeadsByTVNewUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context.getProcessContext().getParameters().get("maxLeadsbyNewLeadInSet"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			


		} else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyLFAInSet") != null) {
			type = "byLFA";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyLFAInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByLFAUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("LFAValue"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			
		}else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyClusterYNInSet") != null) {
			type = "byClusterYN";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyClusterYNInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByClusterYNUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("ClusterYNValue"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			
		}else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyIontInSet") != null) {
			type = "byIont";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyIontInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByIontUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("IontValue"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			
		} else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyPropIDInSet") != null) {
			type = "byPropID";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyPropIDInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByPropIDUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("PropIDValue"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			
		} else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyPromoInSet") != null) {
			type = "byPromo";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyPromoInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByPromoUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("PromoValue"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			


		} else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyClusterInSet") != null) {
			type = "byCluster";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyClusterInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByClusterUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("ClusterValue"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			


		} else if (context.getProcessContext().getParameters().get(
				"maxLeadsByProducts") != null) {
			type = "byProducts";
			String[] vals = ((String) context.getProcessContext().getParameters().get("maxLeadsByProducts")).split(":");
			
			noOfLeadsAssgn = new Integer((String) vals[0]);
			unassignedLeads = dbUtil.selectUnAssingedLeadsByProductsUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context.getProcessContext().getParameters().get("maxLeadsByProducts"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			
		} 
		
		else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyCityInSet") != null) {
			type = "byCity";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyCityInSet"));
					
			System.out.println("City noOfLeadsAssgn.  " + noOfLeadsAssgn);
			
			unassignedLeads = dbUtil.selectUnAssingedLeadsByCityUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));	
			System.out.println("City unassignedLeads.  " + unassignedLeads);	
				
		} else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyMDUInSet") != null) {
			type = "byMDU";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyMDUInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByMDUUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));			
		} else if (context.getProcessContext().getParameters().get(
				"maxLeadsbyBlockMduInSet") != null) {
			type = "byBlockMdu";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsbyBlockMduInSet"));
			unassignedLeads = dbUtil.selectUnAssingedLeadsByBlockMDUUsingFiles(
					(String) context.getProcessContext().getParameters().get(
							"campaignSeq"), (String) context
							.getProcessContext().getParameters().get("iclID"),(String) context
					.getProcessContext().getParameters().get("selectedFiles"),(String) context
					.getProcessContext().getParameters().get("useWirecenters"));	
					
		} 
		
		
		else  if (context.getProcessContext().getParameters().get(
				"maxLeadsByDAInSet") != null) {
			type = "byDA";
			noOfLeadsAssgn = new Integer((String) context.getProcessContext()
					.getParameters().get("maxLeadsByDAInSet"));
			
			String selectedFiles = (String) context.getProcessContext().getParameters().get("selectedFiles");
			
			if (StringUtils.isNotBlank(selectedFiles)) {
			  if(selectedFiles.contains("|")){
			       String[] combinationFiles = selectedFiles.split(",");
				
				for(String fileCombination: combinationFiles){
				    unassignedLeads = dbUtil.selectUnAssingedLeadsByDAUsingFile((String) context
							.getProcessContext().getParameters().get("campaignSeq"),
							(String) context.getProcessContext().getParameters().get("iclID"),
							fileCombination,
							(String) context.getProcessContext().getParameters().get("useWirecenters"));
				    generateLeadSheets(unassignedLeads, campaignSeq, officeSeq, territoryType,type,context,dbUtil, noOfLeadsAssgn);
				}
				
			   }
			    else if(selectedFiles.replaceAll("|", "") != null)
				{
			    	unassignedLeads = dbUtil.selectUnAssingedLeadsByDAUsingFiles((String) context
							.getProcessContext().getParameters().get("campaignSeq"),
							(String) context.getProcessContext().getParameters().get(
									"iclID"),(String) context
							.getProcessContext().getParameters().get("selectedFiles"),(String) context
							.getProcessContext().getParameters().get("useWirecenters"));
				}
			  
			}
		}

		System.out.println("VerizonFiOS type------------>" + type);
		System.out.println("VerizonFiOS noOfLeadsAssgn-->" + noOfLeadsAssgn);
		
		// other than DA	
		if (context.getProcessContext().getParameters().get("maxLeadsByDAInSet") == null) {
		    generateLeadSheets(unassignedLeads, campaignSeq, officeSeq, territoryType,type,context,dbUtil, noOfLeadsAssgn); 
	        }
		
	}


	private void generateLeadSheets(List unassignedLeads, String campaignSeq, String officeSeq, String territoryType,String type,TaskContext context,DBUtil dbUtil,int noOfLeadsAssgn) {
		List finalSheets = new ArrayList();
		TempLeadSheet tempLeadSheet = new TempLeadSheet();
		tempLeadSheet.setCampaignSeq(campaignSeq);
		tempLeadSheet.setOfficeSeq(officeSeq);	
		tempLeadSheet.setTerritoryType(territoryType);	
		System.out.println("VerizonFiOS About to create Leadsheets Now Using Rules");
		System.out.println("VerizonFiOS unassignedLeads.size()="+unassignedLeads.size());
		String availableLeadType="";
		int increment = 1 ;
		try {
			for (int j = 0; j < unassignedLeads.size(); j++) {
				LeadMaster leadMaster = (LeadMaster) unassignedLeads.get(j);
				
				if(type.equalsIgnoreCase("byWC"))	{
					tempLeadSheet.setCurrentValue(leadMaster.getWireCenter());
				} 
				else if(type.equalsIgnoreCase("byDA"))
				{
					tempLeadSheet.setCurrentValue(leadMaster.getDa()+"-"+leadMaster.getWireCenter());
					tempLeadSheet.setLastValue(leadMaster.getDa()+"-"+leadMaster.getWireCenter());
				} 
			
				else if(type.equalsIgnoreCase("byZip")){
					tempLeadSheet.setCurrentValue(leadMaster.getZip());
				} else if(type.equalsIgnoreCase("byNoOfLeads")){
					tempLeadSheet.setCurrentValue(leadMaster.getWireCenter());
				} else if(type.equalsIgnoreCase("byProducts")){
					tempLeadSheet.setCurrentValue(leadMaster.getProductsString());
				} else if(type.equalsIgnoreCase("byNewLeads")){
					tempLeadSheet.setCurrentValue(leadMaster.getVideoGATenure());
				} else if(type.equalsIgnoreCase("byLFA")){
					tempLeadSheet.setCurrentValue(leadMaster.getLfaCode());
				} else if(type.equalsIgnoreCase("byClusterYN")){
					tempLeadSheet.setCurrentValue(leadMaster.getClusterLead());
				}else if(type.equalsIgnoreCase("byIont")){
					tempLeadSheet.setCurrentValue(leadMaster.getiONT());
				} else if(type.equalsIgnoreCase("byPropID")){
					tempLeadSheet.setCurrentValue(leadMaster.getPropId());
				} else if(type.equalsIgnoreCase("byPromo")){
					tempLeadSheet.setCurrentValue(leadMaster.getPromoCode());
				} else if(type.equalsIgnoreCase("byCluster")){
					tempLeadSheet.setCurrentValue(leadMaster.getIontCluster());
				} else if(type.equalsIgnoreCase("byCity")){
					tempLeadSheet.setCurrentValue(leadMaster.getCity()+"-"+leadMaster.getState());
				} else if(type.equalsIgnoreCase("byMDU")){
					tempLeadSheet.setCurrentValue(leadMaster.getMdu());
				}else if(type.equalsIgnoreCase("byBlockMdu")){
					tempLeadSheet.setCurrentValue(leadMaster.getBlockMdu());
				}
				
				System.out.println("TempLeadSheet current value------------>" + tempLeadSheet.getCurrentValue());
				
				
				if(StringUtils.isBlank(tempLeadSheet.getLeadSheetId())){
					tempLeadSheet.setWireCenterName(leadMaster.getWireCenter());
					String uniqueID = new Long(System.nanoTime()).toString();
					String tempString = "" + increment;
					String incrementString = tempString.length()==2?"0"+tempString:tempString.length()==1?"00"+tempString:tempString;
					if("CLLI".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getWireCenter();
					}else if("CITY".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getCity();
						
					}else if("ZIP".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getZip();
					}else if("DA".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
							availableLeadType = leadMaster.getDa();
					}else if("LFA".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getLfaCode();
					}else if(((String)context.getProcessContext().getParameters().get("useWirecenters")).contains("|")){
					
						String first = ((String)context.getProcessContext().getParameters().get("useWirecenters")).split("\\|")[0];
						String second = ((String)context.getProcessContext().getParameters().get("useWirecenters")).split("\\|")[1];
						String available1 = "";
						String available2 = "";
						if("CLLI".equalsIgnoreCase(first)){
							available1 = leadMaster.getWireCenter();
						}else if("CITY".equalsIgnoreCase(first)){
							available1 = leadMaster.getCity();
							System.out.println("VerizonFiOS CITY available1" + available1);
							
						}else if("ZIP".equalsIgnoreCase(first)){
							available1 = leadMaster.getZip();
						}else if("DA".equalsIgnoreCase(first)){
							available1 = leadMaster.getDa();
						}else if("LFA".equalsIgnoreCase(first)){
							available1 = leadMaster.getLfaCode();
						}						
						if("CLLI".equalsIgnoreCase(second)){
							available2 = leadMaster.getWireCenter();
						}else if("CITY".equalsIgnoreCase(second)){
							available2 = leadMaster.getCity();
							System.out.println("VerizonFiOS available2" + available2);
						}else if("ZIP".equalsIgnoreCase(second)){
							available2 = leadMaster.getZip();
						}else if("DA".equalsIgnoreCase(second)){
							available2 = leadMaster.getDa();
						}else if("LFA".equalsIgnoreCase(second)){
							available2 = leadMaster.getLfaCode();
						}
						
						availableLeadType = available1 + "-" +available2;
						
						System.out.println("VerizonFiOS available1" + available1);
						System.out.println("VerizonFiOS available2" + available2);
						System.out.println("VerizonFiOS availableLeadType CITY availableLeadType " + availableLeadType);
					}
					
					if(availableLeadType!=null && !availableLeadType.contains(tempLeadSheet.getCurrentValue().split("-")[0])){
						tempLeadSheet.setLeadSheetId(availableLeadType+"-"+tempLeadSheet.getCurrentValue().split("-")[0]+"-"+incrementString+"-"+uniqueID);
						tempLeadSheet.setLeadSheetName(availableLeadType+"-"+tempLeadSheet.getCurrentValue().split("-")[0]+"-"+incrementString+"-"+uniqueID.substring(11));
						availableLeadType=null;
					}else if(availableLeadType!=null){
						tempLeadSheet.setLeadSheetId(availableLeadType+"-"+incrementString+"-"+uniqueID);
						tempLeadSheet.setLeadSheetName(availableLeadType+"-"+incrementString+"-"+uniqueID.substring(11));
						availableLeadType=null;
					}
				}
				System.out.println("VerizonFiOS tempLeadSheet.getLastValue()------------>" + tempLeadSheet.getLastValue());
				System.out.println("VerizonFiOS tempLeadSheet.getCurrentValue()------------>" + tempLeadSheet.getCurrentValue());
				
				if (tempLeadSheet.getLastValue() != null && !tempLeadSheet.getLastValue().equals(tempLeadSheet.getCurrentValue()) ) {
				
					if(!tempLeadSheet.getLeadIds().isEmpty() && StringUtils.isNotBlank(tempLeadSheet.getLeadSheetId())){
						increment = increment+1;
						System.out.println("VerizonFiOS E-OLD :"+tempLeadSheet.getLastValue());
						System.out.println("VerizonFiOS E-NEW :"+tempLeadSheet.getCurrentValue());
						
						dbUtil.insertLeadSheets(tempLeadSheet);
					}
					System.out.println("VerizonFiOS Template Rules-Gentng New LSHTS as template parameter Changed :"+tempLeadSheet.getLeadIds().size()+"   WITH Leadsheet Id:"+tempLeadSheet.getLeadSheetId()+"  Last and Present Values:  "+tempLeadSheet.getLastValue()+"   --    "+tempLeadSheet.getCurrentValue());		
					
					tempLeadSheet = new TempLeadSheet();
					tempLeadSheet.setCampaignSeq(campaignSeq);
					tempLeadSheet.setOfficeSeq(officeSeq);
					tempLeadSheet.setTerritoryType(territoryType);	
					tempLeadSheet.getLeadIds().clear();
					tempLeadSheet.getRowIds().clear();
					tempLeadSheet.getGeoPoints().clear();						
					tempLeadSheet.getDaForLeads().clear();
					tempLeadSheet.getIsActives().clear();
					
					if(type.equalsIgnoreCase("byWC"))	{
						tempLeadSheet.setCurrentValue(leadMaster.getWireCenter());
					} else if(type.equalsIgnoreCase("byDA")){
						tempLeadSheet.setCurrentValue(leadMaster.getDa()+"-"+leadMaster.getWireCenter());
					} else if(type.equalsIgnoreCase("byZip")){
						tempLeadSheet.setCurrentValue(leadMaster.getZip());
					} else if(type.equalsIgnoreCase("byNoOfLeads")){
						tempLeadSheet.setCurrentValue(leadMaster.getWireCenter());
					} else if(type.equalsIgnoreCase("byProducts")){
						tempLeadSheet.setCurrentValue(leadMaster.getProductsString());
					} else if(type.equalsIgnoreCase("byNewLeads")){
						tempLeadSheet.setCurrentValue(leadMaster.getVideoGATenure());
					} else if(type.equalsIgnoreCase("byLFA")){
						tempLeadSheet.setCurrentValue(leadMaster.getLfaCode());
					} else if(type.equalsIgnoreCase("byClusterYN")){
						tempLeadSheet.setCurrentValue(leadMaster.getClusterLead());
					} else if(type.equalsIgnoreCase("byIont")){
						tempLeadSheet.setCurrentValue(leadMaster.getiONT());
					} else if(type.equalsIgnoreCase("byPropID")){
						tempLeadSheet.setCurrentValue(leadMaster.getPropId());
					} else if(type.equalsIgnoreCase("byPromo")){
						tempLeadSheet.setCurrentValue(leadMaster.getPromoCode());
					} else if(type.equalsIgnoreCase("byCluster")){
						tempLeadSheet.setCurrentValue(leadMaster.getIontCluster());
					} else if(type.equalsIgnoreCase("byCity")){
						tempLeadSheet.setCurrentValue(leadMaster.getCity()+"-"+leadMaster.getState());
					} else if(type.equalsIgnoreCase("byMDU")){
						tempLeadSheet.setCurrentValue(leadMaster.getMdu());
					} else if(type.equalsIgnoreCase("byBlockMdu")){
						tempLeadSheet.setCurrentValue(leadMaster.getBlockMdu());
					}
					tempLeadSheet.setWireCenterName(leadMaster.getWireCenter());
					
					System.out.println("TempLeadSheet current value secound------------>" + tempLeadSheet.getCurrentValue());
					
					
					String uniqueID = new Long(System.nanoTime()).toString();
					String tempString = "" + increment;
					String incrementString = tempString.length()==2?"0"+tempString:tempString.length()==1?"00"+tempString:tempString;
					if("CLLI".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getWireCenter();
					}else if("CITY".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getCity();
						
						
					}else if("ZIP".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getZip();
					}else if("DA".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getDa();
					}else if("LFA".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getLfaCode();
					}else if(((String)context.getProcessContext().getParameters().get("useWirecenters")).contains("|")){
					
						String first = ((String)context.getProcessContext().getParameters().get("useWirecenters")).split("\\|")[0];
						String second = ((String)context.getProcessContext().getParameters().get("useWirecenters")).split("\\|")[1];
						String available1 = "";
						String available2 = "";
						if("CLLI".equalsIgnoreCase(first)){
							available1 = leadMaster.getWireCenter();
						}else if("CITY".equalsIgnoreCase(first)){
							available1 = leadMaster.getCity();
						}else if("ZIP".equalsIgnoreCase(first)){
							available1 = leadMaster.getZip();
						}else if("DA".equalsIgnoreCase(first)){
							available1 = leadMaster.getDa();
						}else if("LFA".equalsIgnoreCase(first)){
							available1 = leadMaster.getLfaCode();
						}						
						if("CLLI".equalsIgnoreCase(second)){
							available2 = leadMaster.getWireCenter();
						}else if("CITY".equalsIgnoreCase(second)){
							available2 = leadMaster.getCity();
						}else if("ZIP".equalsIgnoreCase(second)){
							available2 = leadMaster.getZip();
						}else if("DA".equalsIgnoreCase(second)){
							available2 = leadMaster.getDa();
						}else if("LFA".equalsIgnoreCase(second)){
							available2 = leadMaster.getLfaCode();
						}
						
						availableLeadType = available1 + "-" +available2;
						
						System.out.println("VerizonFiOS available1 secound" + available1);
						System.out.println("VerizonFiOS available2 secound" + available2);
						System.out.println("VerizonFiOS availableLeadType CITY secound availableLeadType " + availableLeadType);
						
					}
					
					if(availableLeadType!=null && !availableLeadType.contains(tempLeadSheet.getCurrentValue().split("-")[0])){
						tempLeadSheet.setLeadSheetId(availableLeadType+"-"+tempLeadSheet.getCurrentValue().split("-")[0]+"-"+incrementString+"-"+uniqueID);
						tempLeadSheet.setLeadSheetName(availableLeadType+"-"+tempLeadSheet.getCurrentValue().split("-")[0]+"-"+incrementString+"-"+uniqueID.substring(11));
						availableLeadType=null;
					}else if(availableLeadType!=null){
						tempLeadSheet.setLeadSheetId(availableLeadType+"-"+incrementString+"-"+uniqueID);
						tempLeadSheet.setLeadSheetName(availableLeadType+"-"+incrementString+"-"+uniqueID.substring(11));
						availableLeadType=null;
					}
				} 
				tempLeadSheet.getLeadIds().add((String)leadMaster.getLeadId());
				tempLeadSheet.getRowIds().add(leadMaster.getRowId());
				tempLeadSheet.getGeoPoints().add((String)leadMaster.getGeoPoint());	
				tempLeadSheet.getIsActives().add((String)leadMaster.getActive());
				if (tempLeadSheet.getLeadIds().size() == noOfLeadsAssgn) {
					if(!tempLeadSheet.getLeadIds().isEmpty() && StringUtils.isNotBlank(tempLeadSheet.getLeadSheetId())){
						increment = increment +1;
						System.out.println("VerizonFiOS M-OLD :"+tempLeadSheet.getLastValue());
						System.out.println("VerizonFiOS M-NEW :"+tempLeadSheet.getCurrentValue());
					
						dbUtil.insertLeadSheets(tempLeadSheet);
					}
					System.out.println("VerizonFiOS Template Rules--Generating New Leadsheets as Max Leads Reached :"+tempLeadSheet.getLeadIds().size()+"   WITH Leadsheet Id:"+tempLeadSheet.getLeadSheetId());		
					tempLeadSheet = new TempLeadSheet();
					tempLeadSheet.setCampaignSeq(campaignSeq);
					tempLeadSheet.setOfficeSeq(officeSeq);
					tempLeadSheet.setTerritoryType(territoryType);	
					tempLeadSheet.getLeadIds().clear();
					tempLeadSheet.getRowIds().clear();
					tempLeadSheet.getGeoPoints().clear();
					tempLeadSheet.getIsActives().clear();
					tempLeadSheet.getDaForLeads().clear();

					if(type.equalsIgnoreCase("byWC"))	{
						tempLeadSheet.setCurrentValue(leadMaster.getWireCenter());
					} else if(type.equalsIgnoreCase("byDA")){
						tempLeadSheet.setCurrentValue(leadMaster.getDa()+"-"+leadMaster.getWireCenter());
					} else if(type.equalsIgnoreCase("byZip")){
						tempLeadSheet.setCurrentValue(leadMaster.getZip());
					} else if(type.equalsIgnoreCase("byNoOfLeads")){
						tempLeadSheet.setCurrentValue(leadMaster.getWireCenter());
					} else if(type.equalsIgnoreCase("byProducts")){
						tempLeadSheet.setCurrentValue(leadMaster.getProductsString());
					} else if(type.equalsIgnoreCase("byNewLeads")){
						tempLeadSheet.setCurrentValue(leadMaster.getVideoGATenure());
					} else if(type.equalsIgnoreCase("byLFA")){
						tempLeadSheet.setCurrentValue(leadMaster.getLfaCode());
					} else if(type.equalsIgnoreCase("byClusterYN")){
						tempLeadSheet.setCurrentValue(leadMaster.getClusterLead());
					}else if(type.equalsIgnoreCase("byIont")){
						tempLeadSheet.setCurrentValue(leadMaster.getiONT());
					} else if(type.equalsIgnoreCase("byPropID")){
						tempLeadSheet.setCurrentValue(leadMaster.getPropId());
					} else if(type.equalsIgnoreCase("byPromo")){
						tempLeadSheet.setCurrentValue(leadMaster.getPromoCode());
					} else if(type.equalsIgnoreCase("byCluster")){
						tempLeadSheet.setCurrentValue(leadMaster.getIontCluster());
					} else if(type.equalsIgnoreCase("byCity")){
						tempLeadSheet.setCurrentValue(leadMaster.getCity()+"-"+leadMaster.getState());
					} else if(type.equalsIgnoreCase("byMDU")){
						tempLeadSheet.setCurrentValue(leadMaster.getMdu());
					} else if(type.equalsIgnoreCase("byBlockMdu")){
						tempLeadSheet.setCurrentValue(leadMaster.getBlockMdu());
					}
					tempLeadSheet.setWireCenterName(leadMaster.getWireCenter());
					String uniqueID = new Long(System.nanoTime()).toString();
					String tempString = "" + increment;
					String incrementString = tempString.length()==2?"0"+tempString:tempString.length()==1?"00"+tempString:tempString;
					if("CLLI".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getWireCenter();
					}else if("CITY".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getCity();
					}else if("ZIP".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getZip();
					}else if("DA".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getDa();
					}else if("LFA".equalsIgnoreCase((String)context.getProcessContext().getParameters().get("useWirecenters"))){
						availableLeadType = leadMaster.getLfaCode();
					}else if(((String)context.getProcessContext().getParameters().get("useWirecenters")).contains("|")){
					
						String first = ((String)context.getProcessContext().getParameters().get("useWirecenters")).split("\\|")[0];
						String second = ((String)context.getProcessContext().getParameters().get("useWirecenters")).split("\\|")[1];
						String available1 = "";
						String available2 = "";
						if("CLLI".equalsIgnoreCase(first)){
							available1 = leadMaster.getWireCenter();
						}else if("CITY".equalsIgnoreCase(first)){
							available1 = leadMaster.getCity();
						}else if("ZIP".equalsIgnoreCase(first)){
							available1 = leadMaster.getZip();
						}else if("DA".equalsIgnoreCase(first)){
							available1 = leadMaster.getDa();
						}else if("LFA".equalsIgnoreCase(first)){
							available1 = leadMaster.getLfaCode();
						}						
						if("CLLI".equalsIgnoreCase(second)){
							available2 = leadMaster.getWireCenter();
						}else if("CITY".equalsIgnoreCase(second)){
							available2 = leadMaster.getCity();
						}else if("ZIP".equalsIgnoreCase(second)){
							available2 = leadMaster.getZip();
						}else if("DA".equalsIgnoreCase(second)){
							available2 = leadMaster.getDa();
						}else if("LFA".equalsIgnoreCase(second)){
							available2 = leadMaster.getLfaCode();
						}
						
						availableLeadType = available1 + "-" +available2;
					    System.out.println("VerizonFiOS CITY availableLeadTyp//////////////////// " + availableLeadType);
					}
					
					if(availableLeadType!=null && !availableLeadType.contains(tempLeadSheet.getCurrentValue().split("-")[0])){
						tempLeadSheet.setLeadSheetId(availableLeadType+"-"+tempLeadSheet.getCurrentValue().split("-")[0]+"-"+incrementString+"-"+uniqueID);
						tempLeadSheet.setLeadSheetName(availableLeadType+"-"+tempLeadSheet.getCurrentValue().split("-")[0]+"-"+incrementString+"-"+uniqueID.substring(11));
						availableLeadType=null;
					}else if(availableLeadType!=null){
						tempLeadSheet.setLeadSheetId(availableLeadType+"-"+incrementString+"-"+uniqueID);
						tempLeadSheet.setLeadSheetName(availableLeadType+"-"+incrementString+"-"+uniqueID.substring(11));
						availableLeadType=null;
					}
				}else if (unassignedLeads.size() == j + 1) {
					if(!tempLeadSheet.getLeadIds().isEmpty() && StringUtils.isNotBlank(tempLeadSheet.getLeadSheetId())){
						increment = increment+1;
						System.out.println("VerizonFiOS U-OLD :"+tempLeadSheet.getLastValue());
						System.out.println("VerizonFiOS U-NEW :"+tempLeadSheet.getCurrentValue());
						dbUtil.insertLeadSheets(tempLeadSheet);
					}
					System.out.println("VerizonFiOS Using Template Only-----Done Generation"+tempLeadSheet.getLeadIds().size()+"   WITH Leadsheet Id:"+tempLeadSheet.getLeadSheetId());
					break;
				}
				if(type.equalsIgnoreCase("byWC"))	{
					tempLeadSheet.setLastValue(leadMaster.getWireCenter());
				} else if(type.equalsIgnoreCase("byDA")){
					tempLeadSheet.setLastValue(leadMaster.getDa()+"-"+leadMaster.getWireCenter());
				}else if(type.equalsIgnoreCase("byZip")){
					tempLeadSheet.setLastValue(leadMaster.getZip());
				}else if(type.equalsIgnoreCase("byNoOfLeads")){
					tempLeadSheet.setLastValue(leadMaster.getWireCenter());
				} else if(type.equalsIgnoreCase("byProducts")){
					tempLeadSheet.setLastValue(leadMaster.getProductsString());
				} else if(type.equalsIgnoreCase("byNewLeads")){
					tempLeadSheet.setLastValue(leadMaster.getVideoGATenure());
				} else if(type.equalsIgnoreCase("byLFA")){
					tempLeadSheet.setLastValue(leadMaster.getLfaCode());
				} else if(type.equalsIgnoreCase("byClusterYN")){
					tempLeadSheet.setLastValue(leadMaster.getClusterLead());
				} else if(type.equalsIgnoreCase("byIont")){
					tempLeadSheet.setLastValue(leadMaster.getiONT());
				} else if(type.equalsIgnoreCase("byPropID")){
					tempLeadSheet.setLastValue(leadMaster.getPropId());
				} else if(type.equalsIgnoreCase("byPromo")){
					tempLeadSheet.setLastValue(leadMaster.getPromoCode());
				} else if(type.equalsIgnoreCase("byCluster")){
					tempLeadSheet.setLastValue(leadMaster.getIontCluster());
				} else if(type.equalsIgnoreCase("byCity")){
					tempLeadSheet.setLastValue(leadMaster.getCity()+"-"+leadMaster.getState());
				} else if(type.equalsIgnoreCase("byMDU")){
					tempLeadSheet.setLastValue(leadMaster.getMdu());
				} else if(type.equalsIgnoreCase("byBlockMdu")){
					tempLeadSheet.setLastValue(leadMaster.getBlockMdu());
				}					
			}
			
			context.getProcessContext().getParameters().put(
					"generatedLeadSheetIds", finalSheets);
			context.getProcessContext().getParameters().put(
					"STATUS", "SUCCESS");
		} catch (Exception e) {
			context.addErrorLogs("Exception While Inserting Logs:"
					+ e.getMessage());
			context.getProcessContext().getParameters().put(
					"STATUS", "FAIL");

			e.printStackTrace();				
			System.out.println("VerizonFiOS Error while Inserting Temp Leadsheet --------------"+e.getMessage());
		}
	}

		//return (Task)this;
}
