package com.cydcor.framework.controller.verizonb2b;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl_Report;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.B2BReport;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.B2BReportFilter;
import com.cydcor.framework.utils.B2BReportMultiColumnTableSorter;
import com.cydcor.framework.utils.B2BReportTableSorter;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

@Controller("verizonb2bICLTerritoryAnalysisController")
@Scope("singleton")
public class ICLTerritoryAnalysisController extends GenericController {
	private static final transient Log logger = LogFactory.getLog(ICLTerritoryAnalysisController.class);
	
	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
		
		String pageDefnintion = "icl/verizonb2b/ICLTerritoryAnalysisDef.html";
		String controlDefnintion = "icl/verizonb2b/ICLTerritoryAnalysis.html";
		
		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
				"true"))) {
			clearOldPage(uiContext, pageDefnintion);

			// Add Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);

			uiContext.addControl(campaignSelectorControl);

			// Add Control the ICL Selector
			GenericUIControl iclSelectorControl = new GenericUIControl();
			iclSelectorControl.setTemplateName(pageDefnintion);
			iclSelectorControl.addParameter("iclSelector", true);
			uiContext.addControl(iclSelectorControl);
			
			GenericUIControl iclTerritoryListTable = new GenericUIControl();
			iclTerritoryListTable.setTemplateName(pageDefnintion);
			iclTerritoryListTable.addParameter("iclTerritoryListTable", true);
			uiContext.addControl(iclTerritoryListTable);
			
			GenericUIControl iclNotes = new GenericUIControl();
			iclNotes.setTemplateName(pageDefnintion);
			iclNotes.addParameter("ICLNotes", true);
			uiContext.addControl(iclNotes);
		}else {
			DatabaseResult dbResult = null;
			String queryString = null;
			if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS_TERRITORY_ANALYSIS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(campaignSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclSelector")) {

				List<Object> list = new ArrayList<Object>();
				list.add(requestWrapper.getParameter("campaignSeq"));

				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

				// Add Second Control the Campaign Selector
				GenericUIControl iclSelectorControl = new GenericUIControl();
				iclSelectorControl.setTemplateName(controlDefnintion);
				iclSelectorControl.addParameter("iclSelector", true);
				iclSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(iclSelectorControl);

			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclTerritoryListTable")) {
				TableUIControl_Report iclTerritoryList = new TableUIControl_Report();
				List<B2BReport> recordList = new LinkedList<B2BReport>();
				String territoryType = requestWrapper.getParameter("territoryType");
				if(!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")){					
					if(!("-1".equalsIgnoreCase(territoryType))){	
						recordList = getPlatformService().getB2BTrackerByTerritoryType(requestWrapper.getParameter("campaignSeq"), 
								requestWrapper.getParameter("officeSeq"),
								territoryType);
					}
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST", recordList);
//					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST"+"_"+territoryType);
//					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST"+"_"+territoryType, recordList);
					if(null != recordList && recordList.size() > 0){
						Collections.sort(recordList, new B2BReportTableSorter("TERRITORY","ASC"));
						iclTerritoryList.setListRecords(recordList);
					}else{
						iclTerritoryList.setListRecords(new LinkedList<B2BReport>());
					}
				}
				iclTerritoryList.setTemplateName("../" + controlDefnintion);
				iclTerritoryList.setTableName(uiContext.getControlName());
				iclTerritoryList.setJavaScriptMethodName("loadPagination");
				iclTerritoryList.setPageNumber(1);
				iclTerritoryList.addParameter("iclTerritoryList", true);
				iclTerritoryList.addParameter("territoryType",territoryType);
				uiContext.addControl(iclTerritoryList);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "filterListTable")) {
				List<B2BReport> list = null;
				List<B2BReport> filteredList = null;
				String territoryType = requestWrapper.getParameter("territoryType");
				list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST");
				//list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST"+"_"+territoryType);
				if(null != list && list.size() > 0){
					filteredList = new B2BReportFilter().filterTerritoryList(list, requestWrapper.getParameter("filterColumn"), 
							requestWrapper.getParameter("filterOperation"), requestWrapper.getParameter("filterValue"));
				}
				if(null != filteredList && filteredList.size() > 0){
					//Collections.sort(filteredList, new B2BReportTableSorter().getComparator(sortColumn, 
					//		!sortOrder.equals("ASC")?true:false));
					Collections.sort(list, new B2BReportTableSorter(requestWrapper.getParameter("sortColumn"),requestWrapper.getParameter("sortOrder")));
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_FILTERED_LIST", filteredList);
//					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST"+"_"+territoryType);
//					CydcorContext.getInstance().getCrmContext().put("TERRITORY_FILTERED_LIST"+"_"+territoryType, filteredList);
				}
				
				TableUIControl_Report filterTable= new TableUIControl_Report();
				filterTable.setListRecords(filteredList);
				filterTable.setTemplateName("../" + controlDefnintion);
				filterTable.setTableName(uiContext.getControlName());
				filterTable.setJavaScriptMethodName("loadPagination");
				filterTable.setPageNumber(1);
				filterTable.addParameter("iclTerritoryList", true);
				filterTable.addParameter("filterColumn", requestWrapper.getParameter("filterColumn"));
				filterTable.addParameter("filterOperation", requestWrapper.getParameter("filterOperation"));
				filterTable.addParameter("filterValue", requestWrapper.getParameter("filterValue"));
				filterTable.addParameter("territoryType",territoryType);
				uiContext.addControl(filterTable);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "showICLNotes")) {
				GenericUIControl displayNotes = new GenericUIControl();
				displayNotes.setTemplateName(controlDefnintion);
				//List list = getPlatformService().getB2BNotesByZip(requestWrapper.getParameter("officeSeq"), requestWrapper.getParameter("zip"));
				List list = getNotes(requestWrapper);
				if(null != list && list.size() > 0){
					displayNotes.addParameter("result", list);
				}
				displayNotes.addParameter("officeSeq", requestWrapper.getParameter("officeSeq"));
				displayNotes.addParameter("zip", requestWrapper.getParameter("zip"));
				displayNotes.addParameter("clli", requestWrapper.getParameter("clli"));
				displayNotes.addParameter("displayICLNotes", true);
				uiContext.addControl(displayNotes);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveNotes")) {
				//List<B2BReport> list = null;
				GenericUIControl savedNotes = new GenericUIControl();
				//savedNotes.setTemplateName("../" + controlDefnintion);
				savedNotes.setTemplateName(controlDefnintion);
//				getPlatformService().updateNotesByZip(requestWrapper.getParameter("officeSeq"), 
//					requestWrapper.getParameter("zip"), requestWrapper.getParameter("clli"), requestWrapper.getParameter("notes"));
				getPlatformService().update(QueryUtils.getQuery().get("INSERT_B2B_TRACKER_NOTES"),
						new Object[] { requestWrapper.getParameter("zip"), 
					requestWrapper.getParameter("clli"), requestWrapper.getParameter("city"), requestWrapper.getParameter("lfa"),requestWrapper.getParameter("notes")});
				//List list = getPlatformService().getB2BNotesByZip(requestWrapper.getParameter("officeSeq"), requestWrapper.getParameter("zip"));
				List list = getNotes(requestWrapper);
				if(null != list && list.size() > 0){
					savedNotes.addParameter("result", list);
				}
				//savedNotes.addParameter("officeSeq", requestWrapper.getParameter("officeSeq"));
				savedNotes.addParameter("zip", requestWrapper.getParameter("zip"));
				savedNotes.addParameter("clli", requestWrapper.getParameter("clli"));
				savedNotes.addParameter("displayICLNotes", true);
				uiContext.addControl(savedNotes);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "showICLHistory")) {
				
				GenericUIControl displayHistory = new GenericUIControl();
				String territoryType = requestWrapper.getParameter("territoryType");
				displayHistory.setTemplateName(controlDefnintion);
				List list = getPlatformService().getB2BHistory(requestWrapper.getParameter("officeSeq"), territoryType, requestWrapper.getParameter("zip"),requestWrapper.getParameter("clli"));
				if(null != list && list.size() > 0){
					displayHistory.addParameter("result", list);
				}
				displayHistory.addParameter("displayICLHistory", true);
				uiContext.addControl(displayHistory);
				/*GenericUIControl displayHistory = new GenericUIControl();
				String territoryType = requestWrapper.getParameter("territoryType");
				displayHistory.setTemplateName(controlDefnintion);
				List list = getPlatformService().getB2BHistory(requestWrapper.getParameter("officeSeq"), territoryType, requestWrapper.getParameter("zip"),requestWrapper.getParameter("clli"));
				if(null != list && list.size() > 0){
					displayHistory.addParameter("result", list);
				}
				displayHistory.addParameter("displayICLHistory", true);
				uiContext.addControl(displayHistory);
				GenericUIControl displayHistory = new GenericUIControl();
				displayHistory.setTemplateName("../" + controlDefnintion);
				List list = getPlatformService().getB2BHistoryByZip(requestWrapper.getParameter("officeSeq"), requestWrapper.getParameter("zip"));
				List list = getB2BHistory(requestWrapper);
				if(null != list && list.size() > 0){
					displayHistory.addParameter("result", list);
				}
				displayHistory.addParameter("displayICLHistory", true);
				uiContext.addControl(displayHistory);*/
				
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "sortTable")) {
				List<B2BReport> list = null;
				String territoryType = requestWrapper.getParameter("territoryType");
				if(requestWrapper.getParameter("filter").equals("true")){
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST");
					//list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST"+"_"+territoryType);
				}else{
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST");
					//list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST"+"_"+territoryType);
				}
				Collections.sort(list, new B2BReportTableSorter(requestWrapper.getParameter("columnId"),requestWrapper.getParameter("order")));
				if(requestWrapper.getParameter("filter").equals("true")){
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_FILTERED_LIST", list);
//					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST"+"_"+territoryType);
//					CydcorContext.getInstance().getCrmContext().put("TERRITORY_FILTERED_LIST"+"_"+territoryType, list);
				}else{
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST", list);
//					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST"+"_"+territoryType);
//					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST"+"_"+territoryType, list);
				}
				TableUIControl_Report sortTable= new TableUIControl_Report();
				sortTable.setListRecords(list);
				sortTable.setTemplateName("../" + controlDefnintion);
				sortTable.setTableName(uiContext.getControlName());
				sortTable.setJavaScriptMethodName("loadPagination");
				sortTable.setPageNumber(1);
				sortTable.addParameter("iclTerritoryList", true);
				sortTable.addParameter("territoryType", territoryType);
				uiContext.addControl(sortTable);
			}/*else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "exportReport")) {
				List<B2BReport> list = null;
				if(requestWrapper.getParameter("filter").equals("true")){
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST"+"_"+territoryType);
				}else{
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST"+"_"+territoryType);
				}
			}*/
			else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "pagination")) {
				List<B2BReport> list = null;
				String territoryType = requestWrapper.getParameter("territoryType");
				if(requestWrapper.getParameter("filter").equals("true")){
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST");
					//list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST"+"_"+territoryType);
				}else{
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST");
					//list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST"+"_"+territoryType);
				}
				
				TableUIControl_Report paginationTable = new TableUIControl_Report();
				paginationTable.setListRecords(list);
				paginationTable.setTemplateName("../" + controlDefnintion);
				paginationTable.setTableName(uiContext.getControlName());
				paginationTable.setJavaScriptMethodName("loadPagination");
				paginationTable.setPageNumber(new Integer(requestWrapper.getParameter("pageNumber")));
				paginationTable.addParameter("iclTerritoryList", true);
				paginationTable.addParameter("territoryType", territoryType);
				uiContext.addControl(paginationTable);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "clearRecordList")) {
				String territoryType = requestWrapper.getParameter("territoryType");
				CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST");
				CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST");
//				CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST"+"_"+territoryType);
//				CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST"+"_"+territoryType);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "multiColumnSorting")) {
				List<B2BReport> list = null;
				String territoryType = requestWrapper.getParameter("territoryType");
				if(requestWrapper.getParameter("filter").equals("true")){
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST");
					//list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST"+"_"+territoryType);
				}else{
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST");
					//list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST"+"_"+territoryType);
				}
				Collections.sort(list, new B2BReportMultiColumnTableSorter(
								requestWrapper.getParameter("columnId1"),requestWrapper.getParameter("order1"),
								requestWrapper.getParameter("columnId2"),requestWrapper.getParameter("order2"),
								requestWrapper.getParameter("columnId3"),requestWrapper.getParameter("order3"),
								requestWrapper.getParameter("columnId4"),requestWrapper.getParameter("order4")));
				if(requestWrapper.getParameter("filter").equals("true")){
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_FILTERED_LIST", list);
//					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST"+"_"+territoryType);
//					CydcorContext.getInstance().getCrmContext().put("TERRITORY_FILTERED_LIST"+"_"+territoryType, list);
				}else{
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST", list);
//					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST"+"_"+territoryType);
//					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST"+"_"+territoryType, list);
				}
				TableUIControl_Report sortTable= new TableUIControl_Report();
				sortTable.setListRecords(list);
				sortTable.setTemplateName("../" + controlDefnintion);
				sortTable.setTableName(uiContext.getControlName());
				sortTable.setJavaScriptMethodName("loadPagination");
				sortTable.setPageNumber(1);
				sortTable.addParameter("iclTerritoryList", true);
				sortTable.addParameter("territoryType", territoryType);
				uiContext.addControl(sortTable);
			}
		}
	}

    private List getNotes(CydcorRequestWrapper requestWrapper){
    	String territoryType = requestWrapper.getParameter("territoryType");
    	List inputParams = new ArrayList();
		//inputParams.add(requestWrapper.getParameter("officeSeq"));
		if("ZIP".equalsIgnoreCase(territoryType)){
			inputParams.add(requestWrapper.getParameter("zip"));
		}else if("CLLI".equalsIgnoreCase(territoryType)||"CLLI|DA".equalsIgnoreCase(territoryType)){
			territoryType = "CLLI";
			inputParams.add(requestWrapper.getParameter("clli"));
		}else if("CITY".equalsIgnoreCase(territoryType)||"CITY|DA".equalsIgnoreCase(territoryType)){
			territoryType = "CITY";
			inputParams.add(requestWrapper.getParameter("city"));
		}else if("LFA".equalsIgnoreCase(territoryType)||"LFA|DA".equalsIgnoreCase(territoryType)){
			territoryType = "LFA";
			inputParams.add(requestWrapper.getParameter("lfa"));
		}else if("LFA|CLLI".equalsIgnoreCase(territoryType)){
			inputParams.add(requestWrapper.getParameter("lfa"));
			inputParams.add(requestWrapper.getParameter("clli"));
			territoryType=territoryType.replace("|", "_");
		}
		DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_B2B_TRACKER_NOTES_"+territoryType), inputParams);
//		if(dbResult!=null){
//			List list = dbResult.getData();			
//			List<ListOrderedMap> notesList = new ArrayList<ListOrderedMap>();			
//			if(null != list && list.size() > 0){
//			for(int i=0;i<list.size();i++){
//				List mapObjects = (List)list.get(i);
//				ListOrderedMap lmap = new ListOrderedMap();
////				for(Object obj: mapObjects){
////					MapObject mapObject = (MapObject)obj;
////					lmap.put(mapObject.getKey(), mapObject.getValue());
////				}
//				lmap.put(((MapObject)mapObjects.get(0)).getKey(), ((MapObject)mapObjects.get(0)).getValue());
//				notesList.add(lmap);
//			}
//				return notesList;
//			}
//			return list;
//		}    	
		if(dbResult!=null){
			List list = dbResult.getData();
			List<String> notesList = new ArrayList<String>();
			if(null != list && list.size() > 0){
				for(Object mapObjList: list){
					List mapObjects = (List)mapObjList;
					notesList.add(((MapObject)mapObjects.get(0)).getValue());
				}
				return notesList;
			}
		}
		return null;
    }
     private List getB2BHistory(CydcorRequestWrapper requestWrapper){
    	String territoryType = requestWrapper.getParameter("territoryType");
    	List inputParams = new ArrayList();
		if("ZIP".equalsIgnoreCase(territoryType)){
			/*inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("zip"));
			inputParams.add(requestWrapper.getParameter("officeSeq"));*/
			inputParams.add(requestWrapper.getParameter("zip"));
		}else if("CLLI".equalsIgnoreCase(territoryType)||"CLLI|DA".equalsIgnoreCase(territoryType)){
			territoryType = "CLLI";
			/*inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("clli"));
			inputParams.add(requestWrapper.getParameter("officeSeq"));*/
			inputParams.add(requestWrapper.getParameter("clli"));
		}else if("CITY".equalsIgnoreCase(territoryType)||"CITY|DA".equalsIgnoreCase(territoryType)){
			territoryType = "CITY";
			/*inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("city"));
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			*/
			inputParams.add(requestWrapper.getParameter("city"));
		}else if("LFA".equalsIgnoreCase(territoryType)||"LFA|DA".equalsIgnoreCase(territoryType)){
			territoryType = "LFA";
			inputParams.add(requestWrapper.getParameter("lfa"));
		}else if("LFA|CLLI".equalsIgnoreCase(territoryType)){
			/*inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("lfa"));
			inputParams.add(requestWrapper.getParameter("clli"));
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			*/
			inputParams.add(requestWrapper.getParameter("lfa"));
			inputParams.add(requestWrapper.getParameter("clli"));
			territoryType = territoryType.replace("|", "_");
		}
		DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_B2B_TRACKER_HISTORY_"+territoryType), inputParams);
		if(dbResult!=null){
			List list = dbResult.getData();
			List<String> historyList = new ArrayList<String>();
			if(null != list && list.size() > 0){
				for(Object mapObjList: list){
					List mapObjects = (List)mapObjList;
					historyList.add(((MapObject)mapObjects.get(0)).getValue());
				}
				return historyList;
			}
		}
		return null;
    }
    
    
	public void init(Object params) {
		// TODO Auto-generated method stub
		
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}
}
