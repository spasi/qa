package com.cydcor.framework.controller.centurylink;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.CodeUtils;
import com.cydcor.framework.utils.DateUtils;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

@Controller("centurylinkDispositionManagementController")
@Scope("singleton")
public class DispositionManagementController extends GenericController {
	private static final transient Log logger = LogFactory.getLog(DispositionManagementController.class);

	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

		String pageDefnintion = "leadsheet/centurylink/DispositionManagementDef.html";
		String controlDefnintion = "leadsheet/centurylink/DispositionManagement.html";
		String printStatus = requestWrapper.getParameter("print");

		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
						"true"))) {
			clearOldPage(uiContext, pageDefnintion);

			// Add Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);

			campaignSelectorControl.addParameter("printCampaignSeq", requestWrapper.getParameter("printCampaignSeq"));
			campaignSelectorControl.addParameter("printOfficeSeq", requestWrapper.getParameter("printOfficeSeq"));
			campaignSelectorControl.addParameter("printLeadSheetId", requestWrapper.getParameter("printLeadSheetId"));
			campaignSelectorControl.addParameter("print", "false");
			if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
				campaignSelectorControl.addParameter("print", "true");
			}

			uiContext.addControl(campaignSelectorControl);

			// Add Control the ICL Selector
			GenericUIControl iclSelectorControl = new GenericUIControl();
			iclSelectorControl.setTemplateName(pageDefnintion);
			iclSelectorControl.addParameter("iclSelector", true);
			iclSelectorControl.addParameter("print", "false");
			if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
				iclSelectorControl.addParameter("print", "true");
			}
			uiContext.addControl(iclSelectorControl);

			// Add Control the LeadSheet Generated Table
			GenericUIControl repSelectorControl = new GenericUIControl();
			repSelectorControl.setTemplateName(pageDefnintion);
			repSelectorControl.addParameter("repSelector", true);
			repSelectorControl.addParameter("repAssignedDateField", true);
			repSelectorControl.addParameter("print", "false");
			if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
				repSelectorControl.addParameter("print", "true");
			}
			uiContext.addControl(repSelectorControl);

			// Add Control the LeadSheet Generated Table
			GenericUIControl leadSheetSelectorControl = new GenericUIControl();
			leadSheetSelectorControl.setTemplateName(pageDefnintion);
			leadSheetSelectorControl.addParameter("leadSheetSelector", true);
			leadSheetSelectorControl.addParameter("print", "false");
			if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
				leadSheetSelectorControl.addParameter("print", "true");
			}
			uiContext.addControl(leadSheetSelectorControl);

			// Add Control the LeadSheet Generated Table
			GenericUIControl leadListtableControl = new GenericUIControl();
			leadListtableControl.setTemplateName(pageDefnintion);
			leadListtableControl.addParameter("leadListTable", true);

			leadListtableControl.addParameter("print", "false");
			if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
				leadListtableControl.addParameter("print", "true");
			}
			uiContext.addControl(leadListtableControl);

		} else {

			DatabaseResult dbResult = null;
			String queryString = null;
			if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);
				campaignSelectorControl.addParameter("print", "false");

				if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
					campaignSelectorControl.addParameter("print", "true");
				}
				uiContext.addControl(campaignSelectorControl);

			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "iclSelector")) {

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.toArray(new Object[0]);

				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

				// Add Second Control the Campaign Selector
				GenericUIControl iclSelectorControl = new GenericUIControl();
				iclSelectorControl.setTemplateName(controlDefnintion);
				iclSelectorControl.addParameter("iclSelector", true);
				// iclSelectorControl.addParameter("templateSelector", true);
				iclSelectorControl.addParameter("result", dbResult);
				iclSelectorControl.addParameter("print", "false");
				if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
					iclSelectorControl.addParameter("print", "true");
				}

				uiContext.addControl(iclSelectorControl);
			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "repSelector")) {

				List repInputs = new ArrayList();
				repInputs.add(new Integer(requestWrapper.getParameter("officeSeq")));
				DatabaseResult repResult;
				repResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_MERGED_OFFICES"), repInputs);
				String sql = QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
				for (List<MapObject> objects : repResult.getData()) {
					sql = sql + " UNION ALL " + QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
					repInputs.add(new Integer(objects.get(0).getValue()));
				}

				sql = "SELECT * FROM ( " + sql + " ) AS TEMP ORDER BY REP_NAME";
				repResult = getPlatformService().loadResult(sql, repInputs);

				GenericUIControl repSelectorControl = new GenericUIControl();
				repSelectorControl.addParameter("repSelector", true);
				repSelectorControl.addParameter("repAssignedDateField", true);
				repSelectorControl.addParameter("result", repResult);
				repSelectorControl.addParameter("print", "false");
				if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
					repSelectorControl.addParameter("print", "true");
				}
				repSelectorControl.setTemplateName("../" + controlDefnintion);

				uiContext.addControl(repSelectorControl);
			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "leadSheetSelector")) {
				String sqlQuery = QueryUtils.getQuery().get("SELECT_ALL_LEADSHEET_IDS");
				String archivedSqlQuery = QueryUtils.getQuery().get("SELECT_ALL_ARCHIVED_LEADSHEET_IDS");
				List list = new ArrayList();
				list.add(new Integer(requestWrapper.getParameter("officeSeq")));
				list.add(new Integer(requestWrapper.getParameter("campaignSeq")));

				List archlist = new ArrayList();
				archlist.add(new Integer(requestWrapper.getParameter("officeSeq")));
				archlist.add(new Integer(requestWrapper.getParameter("campaignSeq")));

				if (requestWrapper.getParameter("repId") != null && !"".equalsIgnoreCase(requestWrapper.getParameter("repId"))) {
				    sqlQuery += " AND L.PERSON_ID=?";
				    list.add(new Integer(requestWrapper.getParameter("repId")));
				    archivedSqlQuery += " AND AL.PERSON_ID=?";
				    archlist.add(new Integer(requestWrapper.getParameter("repId")));
				}

				if (requestWrapper.getParameter("stDate") != null && !"".equalsIgnoreCase(requestWrapper.getParameter("stDate"))
					&& requestWrapper.getParameter("endDate") != null && !"".equalsIgnoreCase(requestWrapper.getParameter("endDate"))) {
				    sqlQuery += " AND L.CREATED_DATE BETWEEN ? AND ? ";
				    archivedSqlQuery += " AND AL.CREATED_DATE BETWEEN ? AND ? ";
				    String strDate = requestWrapper.getParameter("stDate");
				    String strEndDate = requestWrapper.getParameter("endDate");
				    Date stDate = null;
				    Date endDate = null;
				    try {
					stDate = DateUtils.getCalenderDate(strDate).getTime();
					endDate = DateUtils.getCalenderDate(strEndDate).getTime();
				    } catch (ParseException e) {
				    }
				    list.add(strDate);
				    list.add(strEndDate);
				    archlist.add(strDate);
				    archlist.add(strEndDate);
				} else if (requestWrapper.getParameter("stDate") != null && !"".equalsIgnoreCase(requestWrapper.getParameter("stDate"))) {
				    sqlQuery += " AND L.CREATED_DATE = ?";
				    archivedSqlQuery += " AND AL.CREATED_DATE = ?";
				    String strDate = requestWrapper.getParameter("stDate");
				    Date date = null;
				    try {
					date = DateUtils.getCalenderDate(strDate).getTime();
				    } catch (ParseException e) {
				    }
				    list.add(date);
				    archlist.add(date);
				} else if (requestWrapper.getParameter("endDate") != null && !"".equalsIgnoreCase(requestWrapper.getParameter("endDate"))) {
				    sqlQuery += " AND L.CREATED_DATE = ?";
				    archivedSqlQuery += " AND AL.CREATED_DATE = ?";
				    String strDate = requestWrapper.getParameter("endDate");
				    Date date = null;
				    try {
					date = DateUtils.getCalenderDate(strDate).getTime();
				    } catch (ParseException e) {
				    }
				    list.add(date);
				    archlist.add(date);
				}
				if (requestWrapper.getParameter("leadSheetsId") != null && !"".equalsIgnoreCase(requestWrapper.getParameter("leadSheetsId"))) {
				    sqlQuery += " AND L.LEADSHEET_ID LIKE '" + requestWrapper.getParameter("leadSheetsId") + "%'";
				    archivedSqlQuery += " AND AL.LEADSHEET_ID LIKE '" + requestWrapper.getParameter("leadSheetsId") + "%'";
				}
				sqlQuery +=  " UNION ALL " + archivedSqlQuery;
				list.addAll(archlist);
				dbResult = getPlatformService().loadResult(sqlQuery, list);

				GenericUIControl leadSheetSelectorControl = new GenericUIControl();
				leadSheetSelectorControl.addParameter("leadSheetSelector", true);
				leadSheetSelectorControl.addParameter("result", dbResult);
				leadSheetSelectorControl.addParameter("print", "false");

				if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
				    leadSheetSelectorControl.addParameter("print", "true");
				}
				leadSheetSelectorControl.setTemplateName("../" + controlDefnintion);

				uiContext.addControl(leadSheetSelectorControl);
			    } else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "leadListTable")) {

				List irlsInputs = new ArrayList();
				if (requestWrapper.getParameter("print").equalsIgnoreCase("true")) {
				    irlsInputs.add(requestWrapper.getParameter("printLeadSheetId"));
				    irlsInputs.add(requestWrapper.getParameter("printLeadSheetId"));
				} else {
				    irlsInputs.add(requestWrapper.getParameter("leadSheetID"));
				    irlsInputs.add(requestWrapper.getParameter("leadSheetID"));
				}
				GenericUIControl leadListTableUIControl = new GenericUIControl();
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_LEADLIST_BY_LEADSHEET_ID"), irlsInputs);
				leadListTableUIControl.addParameter("result", dbResult);
				CodeUtils codeUtils = new CodeUtils();
				leadListTableUIControl.addParameter("dispositionMap", codeUtils.getDispositionMap());

				leadListTableUIControl.addParameter("leadListTable", true);
				leadListTableUIControl.addParameter("print", "false");

				if (printStatus != null && printStatus.equalsIgnoreCase("true")) {
				    leadListTableUIControl.addParameter("print", "true");
				}
				leadListTableUIControl.setTemplateName(controlDefnintion);
				uiContext.addControl(leadListTableUIControl);

			    } else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "saveDispostionAndNotes")) {

				List dIList = new ArrayList();
				List notesList = new ArrayList();
				List descList = new ArrayList();
				List leadIdList = new ArrayList();
				List rowIdList = new ArrayList();

				String leadSheetID = requestWrapper.getParameter("leadSheetID");
				String archived = requestWrapper.getParameter("archived");
				String leadIds = requestWrapper.getParameter("leadIdArray").trim();
				String rowIds = requestWrapper.getParameter("rowArray").trim();
				String dIs = requestWrapper.getParameter("DIArray").trim();
				String noteValues = requestWrapper.getParameter("notesArray");

				StringTokenizer st = null;
				// dIs = dIs.replaceAll(",,", ",-,");
				// Splitting comma separated DI's and storing in List
				st = new StringTokenizer(dIs, "~~");
				while (st.hasMoreTokens()) {
				    try {
					String temp = st.nextToken();
					if (StringUtils.isBlank(temp) || StringUtils.equalsIgnoreCase(temp, "-")) {
					    dIList.add(null);
					    descList.add(null);
					} else {
					    if (temp.equalsIgnoreCase("NONE"))
						temp = null;
					    dIList.add(temp);
					    if (temp == null) {
						descList.add(null);
					    } else {
						CodeUtils codeUtils = new CodeUtils();
						descList.add(codeUtils.getDispositionDescription(temp));
					    }
					}
				    } catch (NoSuchElementException e) {
					dIList.add(null);
				    }
				}

				// noteValues = noteValues.replaceAll(",,", ",-,");
				// Splitting comma separated Note's and storing in List
				st = new StringTokenizer(noteValues, "~~");
				while (st.hasMoreTokens()) {
				    try {
					String temp = st.nextToken();
					if (StringUtils.isBlank(temp) || StringUtils.equalsIgnoreCase(temp, "-")) {
					    notesList.add(null);
					} else {
					    notesList.add(temp.trim());
					}
				    } catch (NoSuchElementException e) {
					notesList.add(null);
				    }
				}
				// Splitting comma separated Lead Id's and storing in List
				st = new StringTokenizer(leadIds, "~~");
				while (st.hasMoreTokens()) {
				    leadIdList.add(st.nextToken());
				}
				// Splitting comma separated Row Id's and storing in List
				st = new StringTokenizer(rowIds, "~~");
				while (st.hasMoreTokens()) {
				    rowIdList.add(st.nextToken());
				}
				try {
				    getPlatformService().updateDispositionAndNotes(dIList, descList, notesList, rowIdList, leadSheetID, archived);
				} catch (Exception e) {
				    logger.error(e);
				}

				// String rowId = null;
				// String leadId = null;
				// String dI = null;
				// String notes = null;
				// String dIDesc = null;
				// boolean isStatusInProgress = true;
				// for (int i = 0; i < rowIdList.size(); i++) {
				// rowId = (String) rowIdList.get(i);
				// leadId = (String) leadIdList.get(i);
				// dI = (String) dIList.get(i);
				// try {
				// notes = (String) notesList.get(i);
				// } catch (Exception e) {
				// notes = "";
				// }
				// dIDesc = CodeUtils.getDispositionDescription(dI);
				//
				// if (dI != null && dI.contains("**")) {
				// // Update current Dispositions to Empty in IMS_LEADSHEET
				// getPlatformService().update(QueryUtils.getQuery().get("UPDATE_LEADLIST_DI_NOTES"),
				// new Object[]{null, null, null, rowId, leadSheetID});
				//
				// // Update Dispositions into LMS LEADS_MASTER
				// GenericWS.getLMSPlatformService().update(QueryUtils.getQuery().get("UPDATE_LEADSMASTER_DI_NOTES"),
				// new Object[]{null, null, null, rowId, leadId});
				// } else {
				// // Update selected Dispositions into IMS_LEADSHEET
				// getPlatformService().update(QueryUtils.getQuery().get("UPDATE_LEADLIST_DI_NOTES"),
				// new Object[]{dI, dIDesc, notes, new
				// Timestamp(System.currentTimeMillis()), rowId, leadSheetID});
				// isStatusInProgress = false;
				// }
				//
				// }

			    }
		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		return null;
	}

	public void init(Object params) {

	}

}
