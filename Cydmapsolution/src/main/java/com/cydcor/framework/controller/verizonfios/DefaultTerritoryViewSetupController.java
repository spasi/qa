package com.cydcor.framework.controller.verizonfios;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

@Controller("verizonfiosDefaultTerritoryViewSetupController")
@Scope("singleton")
public class DefaultTerritoryViewSetupController extends GenericController {

	DatabaseResult dbResult = null;
	String queryString = null;

	public void execute(DynamicUIContext uiContext) {
		String campaignSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter(
				"campaignSeq");

		String officeSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper()
				.getParameter("officeSeq");

		String pageDefnintion = "leadsheet/verizonfios/DefaultTerritoryViewSetUpDef.html";
		String controlDefnintion = "leadsheet/verizonfios/DefaultTerritoryViewSetUp.html";
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
		disableMap(uiContext);

		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
				"true"))) {
			// clearOldPage(uiContext, pageDefnintion);

			// Add Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);

			uiContext.addControl(campaignSelectorControl);

			// Add Control the Template Selector
			GenericUIControl iclSelectorControl = new GenericUIControl();
			iclSelectorControl.setTemplateName(pageDefnintion);
			iclSelectorControl.addParameter("iclSelector", true);

			uiContext.addControl(iclSelectorControl);

			// Add selectedTemplateDetails the Template Selector
			GenericUIControl defaultViewDetailsControl = new GenericUIControl();
			defaultViewDetailsControl.setTemplateName(pageDefnintion);
			defaultViewDetailsControl.addParameter("defaultViewDetails", true);

			uiContext.addControl(defaultViewDetailsControl);

		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "campaignSelector")) {

			dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(controlDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);
			campaignSelectorControl.addParameter("result", dbResult);

			uiContext.addControl(campaignSelectorControl);

		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "iclSelector")) {

			List list1 = new ArrayList();
			list1.add(requestWrapper.getParameter("campaignSeq"));
			list1.toArray(new Object[0]);

			DatabaseResult iclResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list1);

			GenericUIControl iclSelectorControl = new GenericUIControl();
			iclSelectorControl.setTemplateName(controlDefnintion);
			iclSelectorControl.addParameter("iclSelector", true);
			iclSelectorControl.addParameter("result", iclResult);

			uiContext.addControl(iclSelectorControl);

		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "defaultViewDetails")) {

			List list = new ArrayList();
			list.add(requestWrapper.getParameter("campaignSeq"));
			list.add(requestWrapper.getParameter("officeSeq"));
			list.toArray(new Object[0]);
			DatabaseResult dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get("SELECT_IMS_TERRITORY_VIEW"), list);

			GenericUIControl defaultViewDetailsControl = new GenericUIControl();
			defaultViewDetailsControl.setTemplateName(controlDefnintion);
			defaultViewDetailsControl.addParameter("defaultViewDetails", true);
			defaultViewDetailsControl.addParameter("result", dbResult);

			uiContext.addControl(defaultViewDetailsControl);

		} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "saveDefaultViewDetails")) {
			campaignSeq = requestWrapper.getParameter("campaignSeq");
			officeSeq = requestWrapper.getParameter("officeSeq");

			String defaultViewParam = requestWrapper.getParameter("defaultValue");

			List list = new ArrayList();
			list.add(requestWrapper.getParameter("campaignSeq"));
			list.add(requestWrapper.getParameter("officeSeq"));
			list.toArray(new Object[0]);
			DatabaseResult dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get("SELECT_IMS_TERRITORY_VIEW"), list);

			GenericUIControl defaultViewController = new GenericUIControl();

			if (dbResult.getData() != null && dbResult.getData().size() > 0) {
				getPlatformService().update(QueryUtils.getQuery().get("UPDATE_IMS_TERRITORY_VIEW"),
						new Object[] { defaultViewParam, new Integer(campaignSeq), new Integer(officeSeq) });
				defaultViewController.setResponseText("Default View Selection Updated Successfully");
			} else {
				getPlatformService().update(QueryUtils.getQuery().get("INSERT_IMS_TERRITORY_VIEW"),
						new Object[] { new Integer(campaignSeq), new Integer(officeSeq), defaultViewParam });
				defaultViewController.setResponseText("Default View Selection Successfully Saved");
			}
		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
