package com.cydcor.framework.controller.verizonb2b;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

@Controller("verizonb2bReportController")
@Scope("singleton")
public class ReportController extends GenericController {
	private static final transient Log logger = LogFactory.getLog(ReportController.class);

	public void execute(DynamicUIContext uiContext) {
		 CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

		String pageDefnintion = "reports/" + requestWrapper.getParameter("pageDefination");

		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
				"true"))) {
			// Add Second Control the Campaign Selector
			GenericUIControl leadOperationsControl = new GenericUIControl();
			leadOperationsControl.setTemplateName(pageDefnintion);
			leadOperationsControl.addParameter("report", true);
			uiContext.addControl(leadOperationsControl);
		}
	}
	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
