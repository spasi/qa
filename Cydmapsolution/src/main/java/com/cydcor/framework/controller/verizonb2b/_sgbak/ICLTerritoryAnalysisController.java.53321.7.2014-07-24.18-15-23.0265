
package com.cydcor.framework.controller.verizonb2b;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl_Report;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.B2BReport;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.B2BReportFilter;
import com.cydcor.framework.utils.B2BReportMultiColumnTableSorter;
import com.cydcor.framework.utils.B2BReportTableSorter;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

@Controller("verizonb2bICLTerritoryAnalysisController")
@Scope("singleton")
public class ICLTerritoryAnalysisController extends GenericController {
	private static final transient Log logger = LogFactory.getLog(ICLTerritoryAnalysisController.class);
	
	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
		
		String pageDefnintion = "icl/verizonb2b/ICLTerritoryAnalysisDef.html";
		String controlDefnintion = "icl/verizonb2b/ICLTerritoryAnalysis.html";
		
		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
				"true"))) {
			clearOldPage(uiContext, pageDefnintion);

			// Add Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);

			uiContext.addControl(campaignSelectorControl);

			// Add Control the ICL Selector
			GenericUIControl iclSelectorControl = new GenericUIControl();
			iclSelectorControl.setTemplateName(pageDefnintion);
			iclSelectorControl.addParameter("iclSelector", true);
			uiContext.addControl(iclSelectorControl);
			
			GenericUIControl iclTerritoryListTable = new GenericUIControl();
			iclTerritoryListTable.setTemplateName(pageDefnintion);
			iclTerritoryListTable.addParameter("iclTerritoryListTable", true);
			uiContext.addControl(iclTerritoryListTable);
			
			GenericUIControl iclNotes = new GenericUIControl();
			iclNotes.setTemplateName(pageDefnintion);
			iclNotes.addParameter("ICLNotes", true);
			uiContext.addControl(iclNotes);
		}else {
			DatabaseResult dbResult = null;
			String queryString = null;
			if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS_TERRITORY_ANALYSIS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(campaignSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclSelector")) {

				List<Object> list = new ArrayList<Object>();
				list.add(requestWrapper.getParameter("campaignSeq"));

				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

				// Add Second Control the Campaign Selector
				GenericUIControl iclSelectorControl = new GenericUIControl();
				iclSelectorControl.setTemplateName(controlDefnintion);
				iclSelectorControl.addParameter("iclSelector", true);
				iclSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(iclSelectorControl);

			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclTerritoryListTable")) {
				TableUIControl_Report iclTerritoryList = new TableUIControl_Report();
				List<B2BReport> recordList = new LinkedList<B2BReport>();
				if(!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")){
					recordList = getPlatformService().getB2BTerritoryDetailsByZip(requestWrapper.getParameter("campaignSeq"), requestWrapper.getParameter("officeSeq"));
					if(null != recordList && recordList.size() > 0){
						Collections.sort(recordList, new B2BReportTableSorter("ZIP","ASC"));
						iclTerritoryList.setListRecords(recordList);
						CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST");
						CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST", recordList);
					}
				}
				iclTerritoryList.setTemplateName("../" + controlDefnintion);
				iclTerritoryList.setTableName(uiContext.getControlName());
				iclTerritoryList.setJavaScriptMethodName("loadPagination");
				iclTerritoryList.setPageNumber(1);
				iclTerritoryList.addParameter("iclTerritoryList", true);
				uiContext.addControl(iclTerritoryList);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "filterListTable")) {
				List<B2BReport> list = null;
				List<B2BReport> filteredList = null;
				
				list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST");
				if(null != list && list.size() > 0){
					filteredList = new B2BReportFilter().filterTerritoryList(list, requestWrapper.getParameter("filterColumn"), 
							requestWrapper.getParameter("filterOperation"), requestWrapper.getParameter("filterValue"));
				}
				if(null != filteredList && filteredList.size() > 0){
					//Collections.sort(filteredList, new B2BReportTableSorter().getComparator(sortColumn, 
					//		!sortOrder.equals("ASC")?true:false));
					Collections.sort(list, new B2BReportTableSorter(requestWrapper.getParameter("sortColumn"),requestWrapper.getParameter("sortOrder")));
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_FILTERED_LIST", filteredList);
				}
				
				TableUIControl_Report filterTable= new TableUIControl_Report();
				filterTable.setListRecords(filteredList);
				filterTable.setTemplateName("../" + controlDefnintion);
				filterTable.setTableName(uiContext.getControlName());
				filterTable.setJavaScriptMethodName("loadPagination");
				filterTable.setPageNumber(1);
				filterTable.addParameter("iclTerritoryList", true);
				filterTable.addParameter("filterColumn", requestWrapper.getParameter("filterColumn"));
				filterTable.addParameter("filterOperation", requestWrapper.getParameter("filterOperation"));
				filterTable.addParameter("filterValue", requestWrapper.getParameter("filterValue"));
				uiContext.addControl(filterTable);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "showICLNotes")) {
				GenericUIControl displayNotes = new GenericUIControl();
				displayNotes.setTemplateName(controlDefnintion);
				List list = getPlatformService().getB2BNotesByZip(requestWrapper.getParameter("officeSeq"), requestWrapper.getParameter("zip"));
				if(null != list && list.size() > 0){
					displayNotes.addParameter("result", list);
				}
				displayNotes.addParameter("officeSeq", requestWrapper.getParameter("officeSeq"));
				displayNotes.addParameter("zip", requestWrapper.getParameter("zip"));
				displayNotes.addParameter("clli", requestWrapper.getParameter("clli"));
				displayNotes.addParameter("displayICLNotes", true);
				uiContext.addControl(displayNotes);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveNotes")) {
				//List<B2BReport> list = null;
				GenericUIControl savedNotes = new GenericUIControl();
				//savedNotes.setTemplateName("../" + controlDefnintion);
				savedNotes.setTemplateName(controlDefnintion);
				getPlatformService().updateNotesByZip(requestWrapper.getParameter("officeSeq"), 
					requestWrapper.getParameter("zip"), requestWrapper.getParameter("clli"), requestWrapper.getParameter("notes"));
				List list = getPlatformService().getB2BNotesByZip(requestWrapper.getParameter("officeSeq"), requestWrapper.getParameter("zip"));
				if(null != list && list.size() > 0){
					savedNotes.addParameter("result", list);
				}
				savedNotes.addParameter("officeSeq", requestWrapper.getParameter("officeSeq"));
				savedNotes.addParameter("zip", requestWrapper.getParameter("zip"));
				savedNotes.addParameter("clli", requestWrapper.getParameter("clli"));
				savedNotes.addParameter("displayICLNotes", true);
				uiContext.addControl(savedNotes);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "showICLHistory")) {
				GenericUIControl displayHistory = new GenericUIControl();
				displayHistory.setTemplateName(controlDefnintion);
				List list = getPlatformService().getB2BHistoryByZip(requestWrapper.getParameter("officeSeq"), requestWrapper.getParameter("zip"));
				if(null != list && list.size() > 0){
					displayHistory.addParameter("result", list);
				}
				displayHistory.addParameter("displayICLHistory", true);
				uiContext.addControl(displayHistory);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "sortTable")) {
				List<B2BReport> list = null;
				if(requestWrapper.getParameter("filter").equals("true")){
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST");
				}else{
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST");
				}
				Collections.sort(list, new B2BReportTableSorter(requestWrapper.getParameter("columnId"),requestWrapper.getParameter("order")));
				if(requestWrapper.getParameter("filter").equals("true")){
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_FILTERED_LIST", list);
				}else{
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST", list);
				}
				TableUIControl_Report sortTable= new TableUIControl_Report();
				sortTable.setListRecords(list);
				sortTable.setTemplateName("../" + controlDefnintion);
				sortTable.setTableName(uiContext.getControlName());
				sortTable.setJavaScriptMethodName("loadPagination");
				sortTable.setPageNumber(1);
				sortTable.addParameter("iclTerritoryList", true);
				uiContext.addControl(sortTable);
			}/*else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "exportReport")) {
				List<B2BReport> list = null;
				if(requestWrapper.getParameter("filter").equals("true")){
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST");
				}else{
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST");
				}
			}*/
			else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "pagination")) {
				List<B2BReport> list = null;
				if(requestWrapper.getParameter("filter").equals("true")){
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST");
				}else{
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST");
				}
				
				TableUIControl_Report paginationTable = new TableUIControl_Report();
				paginationTable.setListRecords(list);
				paginationTable.setTemplateName("../" + controlDefnintion);
				paginationTable.setTableName(uiContext.getControlName());
				paginationTable.setJavaScriptMethodName("loadPagination");
				paginationTable.setPageNumber(new Integer(requestWrapper.getParameter("pageNumber")));
				paginationTable.addParameter("iclTerritoryList", true);
				uiContext.addControl(paginationTable);
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "clearRecordList")) {
				CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST");
				CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST");
			}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "multiColumnSorting")) {
				List<B2BReport> list = null;
				
				if(requestWrapper.getParameter("filter").equals("true")){
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST");
				}else{
					list = (List<B2BReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST");
				}
				Collections.sort(list, new B2BReportMultiColumnTableSorter(
								requestWrapper.getParameter("columnId1"),requestWrapper.getParameter("order1"),
								requestWrapper.getParameter("columnId2"),requestWrapper.getParameter("order2"),
								requestWrapper.getParameter("columnId3"),requestWrapper.getParameter("order3"),
								requestWrapper.getParameter("columnId4"),requestWrapper.getParameter("order4")));
				if(requestWrapper.getParameter("filter").equals("true")){
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_FILTERED_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_FILTERED_LIST", list);
				}else{
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST");
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST", list);
				}
				TableUIControl_Report sortTable= new TableUIControl_Report();
				sortTable.setListRecords(list);
				sortTable.setTemplateName("../" + controlDefnintion);
				sortTable.setTableName(uiContext.getControlName());
				sortTable.setJavaScriptMethodName("loadPagination");
				sortTable.setPageNumber(1);
				sortTable.addParameter("iclTerritoryList", true);
				uiContext.addControl(sortTable);
			}
		}
	}

	public void init(Object params) {
		// TODO Auto-generated method stub
		
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}
}
