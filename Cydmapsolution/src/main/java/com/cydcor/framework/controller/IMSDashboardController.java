package com.cydcor.framework.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

@Controller("IMSDashboardController")
@Scope("singleton")
public class IMSDashboardController extends GenericController {
	private static final transient Log logger = LogFactory.getLog(IMSDashboardController.class);
	
	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

		String pageDefnintion = "icl/IMSDashboardDef.html";
		String controlDefnintion = "icl/IMSDashboard.html";
		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
						"true"))) {
			clearOldPage(uiContext, pageDefnintion);

			GenericUIControl searchCriteriaControl = new GenericUIControl();
			searchCriteriaControl.setTemplateName(pageDefnintion);
			searchCriteriaControl.addParameter("searchCriteriaTable", true);
			uiContext.addControl(searchCriteriaControl);

			// Add Control the leadListTable Selector
			GenericUIControl leadUploadHistoryTableControl = new GenericUIControl();
			leadUploadHistoryTableControl.setTemplateName(pageDefnintion);
			leadUploadHistoryTableControl.addParameter("leadUploadHistoryTable", true);
			List<String> profileNames = (List<String>) CydcorContext.getInstance().getCrmContext().get("profileName");
			String profileName = "";
			if (profileNames.size() == 1)
				profileName = profileNames.get(0);
			leadUploadHistoryTableControl.addParameter("profileName", profileName);
			leadUploadHistoryTableControl.addParameter("profileNames", profileNames);
			leadUploadHistoryTableControl.addParameter("clientKey", CydcorUtils.getProfileKey(profileName));
			leadUploadHistoryTableControl.addParameter("tableName", "leadUploadHistoryTable");

			uiContext.addControl(leadUploadHistoryTableControl);

		} else {
			String loggedInUserRole = FreeMarkerEngine.getInstance().evaluateString(
					"${GlobalContext.crmContext.roleNames}");

			DatabaseResult dbResult = new DatabaseResult();
			if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				if (loggedInUserRole != null
						&& (loggedInUserRole.equals("Application Administrator") || loggedInUserRole
								.equals("Cydcor Executive"))) {
					campaignSelectorControl.addParameter("campaignSelector", true);
				} else {
					campaignSelectorControl.addParameter("iclCampaignSelector", true);
				}
				campaignSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(campaignSelectorControl);
			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "leadUploadHistoryTable")) {
				String mainString = "";

				int grpByIdx = 0;
				String queryString = "";
				List<Object> inputs = new ArrayList<Object>();
				TableUIControl leadUploadHistoryTableControl = new TableUIControl();
				// leadUploadHistoryTableControl.setDataSource("LMS");

				boolean whereAdded = false;

				if (loggedInUserRole != null
						&& (loggedInUserRole.equals("Application Administrator") || loggedInUserRole
								.equals("Cydcor Executive"))) {
					mainString = QueryUtils.getQuery().get("SELECT_IMS_DASHBOARD_ADMIN");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					logger.debug("Show the 1....." + queryString);
					if (StringUtils.isNotBlank(requestWrapper.getParameter("ownerName"))) {
						queryString = queryString + " AND LEAD_OWNER=? ";
						logger.debug("Show the 2....." + queryString);
						inputs.add(requestWrapper.getParameter("ownerName"));
					}

					if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))
							&& StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + " AND CREATED_DATE BETWEEN ? AND ? ";
						logger.debug("Show the 3....." + queryString);
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))) {
						queryString = queryString + " AND CREATED_DATE = ? ";
						logger.debug("Show the 4....." + queryString);
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + " AND CREATED_DATE = ? ";
						logger.debug("Show the 5....." + queryString);
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					}
					if (StringUtils.isNotBlank(requestWrapper.getParameter("campainId"))) {
						queryString = queryString + " AND " + " CAMPAIGN_SEQ = ?";
						logger.debug("Show the 6....." + queryString);
						inputs.add(new Long(requestWrapper.getParameter("campainId")));
					}

					leadUploadHistoryTableControl.setColumnName("UPLOAD_DATE DESC,UPLOAD_FILENAME");
				} else {
					//where clause is already there in query..
					whereAdded=true;
					
					mainString = QueryUtils.getQuery().get("SELECT_IMS_DASHBOARD_OTHERS");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					logger.debug("Show the 7....." + queryString);
					
					leadUploadHistoryTableControl
							.setColumnName("MODIFIED_DATE DESC, CAMPAIGN_NAME,ICL_CODE,LEAD_SHEET_ID");
					if (StringUtils.isNotBlank(requestWrapper.getParameter("ownerName"))) {
						String cond = (whereAdded ? " AND " : " WHERE ");
						queryString = queryString + cond+ " LEAD_OWNER=? ";
						logger.debug("ownerName:"+requestWrapper.getParameter("ownerName"));
						logger.debug("Show the 8....." + queryString);
						inputs.add(requestWrapper.getParameter("ownerName"));
						whereAdded = true;
					}

					if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))
							&& StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						
						String cond = (whereAdded ? " AND " : " WHERE ");
						queryString = queryString + cond + " MODIFIED_DATE BETWEEN ? AND ? ";
						logger.debug("Show the 9....." + queryString);
						if (cond.contains("WHERE"))
							whereAdded = true;
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))) {
						String cond = (whereAdded ? " AND " : " WHERE ");
						queryString = queryString + cond + " MODIFIED_DATE = ? ";
						logger.debug("Show the 10....." + queryString);
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
						if (cond.contains("WHERE"))
							whereAdded = true;
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						String cond = (whereAdded ? " AND " : " WHERE ");
						queryString = queryString + cond + "  MODIFIED_DATE = ? ";
						logger.debug("Show the 11....." + queryString);
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
						if (cond.contains("WHERE"))
							whereAdded = true;
					}
					//logger.debug("campaignid:"+requestWrapper.getParameter("campainId"));
					if (StringUtils.isNotBlank(requestWrapper.getParameter("campainId"))) {
						queryString = queryString + " AND " + " CAMPAIGN_SEQ = ?";
						logger.debug("Show the 12....." + queryString);
						inputs.add(new Long(requestWrapper.getParameter("campainId")));
					}

				}

				queryString = queryString + mainString.substring(grpByIdx);
				logger.debug("Show the Lion Dashboard result....." + queryString);
				leadUploadHistoryTableControl.setQueryString(queryString);
				leadUploadHistoryTableControl.setFilterParamList(inputs);
				leadUploadHistoryTableControl.setTableName(uiContext.getControlName());
				leadUploadHistoryTableControl.setRowsPerPage(20);
				leadUploadHistoryTableControl.setJavaScriptMethodName("refreshleadUploadHistoryTable");
				leadUploadHistoryTableControl.addParameter("leadUploadHistoryTable", true);
				leadUploadHistoryTableControl.setTemplateName("../" + controlDefnintion);
				uiContext.addControl(leadUploadHistoryTableControl);
			}
			
			else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "showAlerts")) {
				// SELECT_ALERT_FOR_NEW_LEADS

				DatabaseResult newLeadsResult = null;
				DatabaseResult expiringLeadsResult = null;
				String queryString = null;
				newLeadsResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_ALERT_FOR_NEW_LEADS"));

				expiringLeadsResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_ALERT_FOR_EXPIRING_LEADS"));

				// Add Second Control the Campaign Selector
				GenericUIControl alertsControl = new GenericUIControl();
				alertsControl.setTemplateName(controlDefnintion);
				alertsControl.addParameter("showAlerts", true);
				alertsControl.addParameter("newLeadsResult", newLeadsResult);
				alertsControl.addParameter("expiringLeadsResult", expiringLeadsResult);

				uiContext.addControl(alertsControl);

				// SELECT_ALERT_FOR_EXPIRING_LEADS

			}
		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
