package com.cydcor.framework.controller.verizonfios;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.Cookie;

import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONSerializer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;

import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.clickframework.TableUIControlReportFios;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.model.TempLeadSheet;
import com.cydcor.framework.model.VerizonFiosReport;
import com.cydcor.framework.process.ProcessContext;
import com.cydcor.framework.process.ProcessExecutor;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;
import com.cydcor.framework.utils.comparator.verizonfiostracker.DelegateComparator;
import com.cydcor.framework.utils.comparator.verizonfiostracker.GenericComparator;
import com.cydcor.framework.utils.filter.VerizonFiosTrackerFilter;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;
import com.cydcor.framework.wrapper.CydcorResponseWrapper;

@Controller("verizonfiosLeadSheetGenerationTrackerController")
@Scope("singleton")
public class LeadSheetGenerationTrackerController extends GenericController {
    private static final transient Log logger = LogFactory.getLog(LeadSheetGenerationTrackerController.class);

    public void execute(DynamicUIContext uiContext) {

    	logger.debug("check1");
	CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
	CydcorResponseWrapper responseWrapper = CydcorContext.getInstance().getResponseContext().getResponseWrapper();

	String campaignSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("campaignSeq");

	String officeSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("officeSeq");
	String pageDefinition = "leadsheet/verizonfios/LeadSheetGenerationTrackerDef.html";
	String controlDefinition = "leadsheet/verizonfios/LeadSheetGenerationTracker.html";
	if (StringUtils.isBlank(uiContext.getControlName())
		|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals("true"))) {
	    clearOldPage(uiContext, pageDefinition);
	    logger.debug("check2");
	    // Add Control the Campaign Selector
	    GenericUIControl campaignSelectorControl = new GenericUIControl();
	    campaignSelectorControl.setTemplateName(pageDefinition);
	    campaignSelectorControl.addParameter("campaignSelector", true);

	    uiContext.addControl(campaignSelectorControl);

	    // Add Control the ICL Selector
	    GenericUIControl iclSelectorControl = new GenericUIControl();
	    iclSelectorControl.setTemplateName(pageDefinition);
	    iclSelectorControl.addParameter("iclSelector", true);

	    uiContext.addControl(iclSelectorControl);
	    //code added by Sanjay
	    // Add templateSelector Control
	    GenericUIControl templateSelectorControl = new GenericUIControl();
	    templateSelectorControl.setTemplateName(pageDefinition);
	    templateSelectorControl.addParameter("templateSelector", true);

	    uiContext.addControl(templateSelectorControl);

	    // Add Map Control Noe
	    GenericUIControl mapControl = new GenericUIControl();
	    mapControl.setTemplateName(pageDefinition);
	    mapControl.addParameter("showCampaignMap", true);
	    uiContext.addControl(mapControl);
	    
	    // Add Territory Tracker
	    GenericUIControl territoryTracker = new GenericUIControl();
	    territoryTracker.setTemplateName(pageDefinition);
	    territoryTracker.addParameter("territoryTracker", true);
	    uiContext.addControl(territoryTracker);
//	    
//		GenericUIControl iclTerritoryListTable = new GenericUIControl();
//		iclTerritoryListTable.setTemplateName(pageDefinition);
//		iclTerritoryListTable.addParameter("iclTerritoryListTable", true);
//		uiContext.addControl(iclTerritoryListTable);
		
		GenericUIControl iclNotes = new GenericUIControl();
		iclNotes.setTemplateName(pageDefinition);
		iclNotes.addParameter("ICLNotes", true);
		uiContext.addControl(iclNotes);

	    // Add Color Codes Control
	    GenericUIControl colorCodesControl = new GenericUIControl();
	    colorCodesControl.setTemplateName(pageDefinition);
	    colorCodesControl.addParameter("colorCodes", true);
	    uiContext.addControl(colorCodesControl);

	    // Add Default View Selection Details Control
	    GenericUIControl defaultViewDetailsControl = new GenericUIControl();
	    defaultViewDetailsControl.setTemplateName(pageDefinition);
	    defaultViewDetailsControl.addParameter("defaultViewDetails", true);
	    uiContext.addControl(defaultViewDetailsControl);

	    // Add SelectedTemplateDetails Control
	    GenericUIControl templateDetailsControl = new GenericUIControl();
	    templateDetailsControl.setTemplateName(pageDefinition);
	    templateDetailsControl.addParameter("selectedTemplateDetails", true);
	    uiContext.addControl(templateDetailsControl);

	    // Add SelectedTemplateDetails Control
//	    GenericUIControl availableLeadsDataControl = new GenericUIControl();
//	    availableLeadsDataControl.setTemplateName(pageDefinition);
//	    availableLeadsDataControl.addParameter("availableLeadsData", true);
//	    availableLeadsDataControl.addParameter("tableName", "availableLeadsData");
//	    uiContext.addControl(availableLeadsDataControl);

	    // Add Control the LeadSheet Generated Table
	    GenericUIControl leadSheetGeneratedtableControl = new GenericUIControl();
	    leadSheetGeneratedtableControl.setTemplateName(pageDefinition);
	    leadSheetGeneratedtableControl.addParameter("leadSheetGeneratedtable", true);
	    leadSheetGeneratedtableControl.addParameter("tableName", "leadSheetGeneratedtable");
	    uiContext.addControl(leadSheetGeneratedtableControl);

	    // Add Control the LeadSheet Generated Table
	    GenericUIControl leadSheetSavedtableControl = new GenericUIControl();
	    leadSheetSavedtableControl.setTemplateName(pageDefinition);
	    leadSheetSavedtableControl.addParameter("leadSheetSavedtable", true);
	    leadSheetSavedtableControl.addParameter("tableName", "leadSheetSavedtable");
	    uiContext.addControl(leadSheetSavedtableControl);
	    logger.debug("check3");
	} else {
		logger.debug("check4"+uiContext.getControlName());
	    String queryString = null;

	    
	    if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
	    	logger.debug("check5");
		DatabaseResult dbResult = null;
		dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

		// Add Second Control the Campaign Selector
		GenericUIControl campaignSelectorControl = new GenericUIControl();
		campaignSelectorControl.setTemplateName(controlDefinition);
		campaignSelectorControl.addParameter("campaignSelector", true);
		campaignSelectorControl.addParameter("result", dbResult);

		uiContext.addControl(campaignSelectorControl);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclSelector")) {
	    	logger.debug("check6");
		// System.out.println("campaignSeq for icl selector: " +
		// requestWrapper.getParameter("campaignSeq"));
		List list = new ArrayList();
		list.add(requestWrapper.getParameter("campaignSeq"));
		list.toArray(new Object[0]);

		DatabaseResult dbResult = null;
		if (!requestWrapper.getParameter("campaignSeq").equalsIgnoreCase("-1"))
		    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

		// Add Second Control the Campaign Selector
		GenericUIControl iclSelectorControl = new GenericUIControl();
		iclSelectorControl.setTemplateName(controlDefinition);
		iclSelectorControl.addParameter("iclSelector", true);
		// iclSelectorControl.addParameter("templateSelector", true);
		iclSelectorControl.addParameter("result", dbResult);

		uiContext.addControl(iclSelectorControl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "templateSelector")) {

	    	logger.debug("check7");
		List list = new ArrayList();
		list.add(requestWrapper.getParameter("campaignSeq"));
		// officeSeq
		list.add(requestWrapper.getParameter("officeSeq"));
		list.toArray(new Object[0]);
		// System.out.println("campaignSeq for templateSelector : " +
		// requestWrapper.getParameter("campaignSeq"));
		// System.out.println("officeSeq for templateSelector: " +
		// requestWrapper.getParameter("officeSeq"));
		DatabaseResult dbResult = null;
		if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
		    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TEMPLATE_WITH_DEFAULT_NAMES"), list);

		// Add Second Control the Template Selector
		GenericUIControl templateSelectorControl = new GenericUIControl();
		templateSelectorControl.setTemplateName(controlDefinition);
		// templateSelectorControl.addParameter("campaignSeq",
		// campaignSeq);
		templateSelectorControl.addParameter("templateSelector", true);
		templateSelectorControl.addParameter("result", dbResult);

		uiContext.addControl(templateSelectorControl);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "selectedTemplateDetails")) {
	    	logger.debug("check8");
		DatabaseResult dbResult = null;
		// System.out.println("campaignSeq for selectedTemplateDetails : "
		// + requestWrapper.getParameter("campaignSeq"));
		// System.out.println("officeSeq for selectedTemplateDetails: "
		// + requestWrapper.getParameter("officeSeq"));
		// System.out.println("templateName for selectedTemplateDetails: "
		// + requestWrapper.getParameter("templateName"));

		if (StringUtils.equalsIgnoreCase("SYSTEM_DEFAULT", requestWrapper.getParameter("templateName"))) {

		    // Load Template Params for this Campaign and Office
		    if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
			dbResult = getPlatformService().loadSystemDefaultTemplateParams(Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
				Integer.parseInt(requestWrapper.getParameter("officeSeq")), requestWrapper.getParameter("templateName"));
		} else {

		    // Load Template Params for this Campaign and Office
		    if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
			dbResult = getPlatformService().loadTemplateParams(Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
				Integer.parseInt(requestWrapper.getParameter("officeSeq")), requestWrapper.getParameter("templateName"));
		}

		GenericUIControl selectedTemplateDetailsControl = new GenericUIControl();
		selectedTemplateDetailsControl.setTemplateName(controlDefinition);
		selectedTemplateDetailsControl.addParameter("selectedTemplateDetails", true);
		selectedTemplateDetailsControl.addParameter("result", dbResult);
		selectedTemplateDetailsControl.addParameter("templateName", requestWrapper.getParameter("templateName"));
		if (dbResult != null)
		    for (List row : dbResult.getData()) {
			MapObject paramNameColumn = (MapObject) row.get(0);
			MapObject paramValueColumn = (MapObject) row.get(1);
			selectedTemplateDetailsControl.addParameter(paramNameColumn.getValue(), paramValueColumn.getValue());
		    }
		uiContext.addControl(selectedTemplateDetailsControl);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "availableLeadsData")) {
		List listLeads = new ArrayList();
		listLeads.add(new Integer(requestWrapper.getParameter("campaignSeq")));
		listLeads.add(new Integer(requestWrapper.getParameter("officeSeq")));

		if (StringUtils.equalsIgnoreCase(requestWrapper.getParameter("loadAvailableLeadsDataTable"), "true")) {
		    if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
			/*listLeads.add(new Integer(requestWrapper.getParameter("campaignSeq")));
			listLeads.add(new Integer(requestWrapper.getParameter("officeSeq")));*/
			TableUIControl availableLeadsDataTableControl = new TableUIControl();
			String wireCenters = requestWrapper.getParameter("wirecenterName");
			if (wireCenters == null || StringUtils.isBlank(wireCenters.replaceAll("%", "")))
			    wireCenters = "";
			String sql = "";
			String useWirecenters = requestWrapper.getParameter("useWirecenters");
			if ("CLLI".equalsIgnoreCase(useWirecenters)) {
			    sql = QueryUtils.getQuery().get("SELECT_AVAILABLE_LEADS_LIST");
			    if (StringUtils.isNotBlank(wireCenters)) {
				String[] split = sql.split(" GROUP BY ");
				sql = split[0] + " AND CLLI LIKE '" + wireCenters + "' GROUP BY " + split[1];
				availableLeadsDataTableControl.getFilterParamList().add(wireCenters.trim());
			    }
			    availableLeadsDataTableControl.setColumnName("CLLI_NAME ASC ");
			} else if ("CITY".equalsIgnoreCase(useWirecenters)) {
			    sql = QueryUtils.getQuery().get("SELECT_AVAILABLE_LEADS_LIST_BY_CITY");
			    if (StringUtils.isNotBlank(wireCenters)) {
				String[] split = sql.split(" GROUP BY ");
				sql = split[0] + " AND CITY LIKE '" + wireCenters + "' GROUP BY " + split[1];
				availableLeadsDataTableControl.getFilterParamList().add(wireCenters.trim());
			    }
			    availableLeadsDataTableControl.setColumnName("CITY ASC ");
			} else if ("LFA".equalsIgnoreCase(useWirecenters)) {
			    sql = QueryUtils.getQuery().get("SELECT_AVAILABLE_LEADS_LIST_BY_LFA");
			    if (StringUtils.isNotBlank(wireCenters)) {
				String[] split = sql.split(" GROUP BY ");
				sql = split[0] + " AND LFA_CODE LIKE '" + wireCenters + "' GROUP BY " + split[1];
				availableLeadsDataTableControl.getFilterParamList().add(wireCenters.trim());
			    }
			    availableLeadsDataTableControl.setColumnName("LFA_CODE ASC ");

			}

			availableLeadsDataTableControl.setFilterParamList(listLeads);
			availableLeadsDataTableControl.setTableName(uiContext.getControlName());
			availableLeadsDataTableControl.setQueryString(sql);
			availableLeadsDataTableControl.setJavaScriptMethodName("showLeadsByUploadFile");
			availableLeadsDataTableControl.setTemplateName("../" + controlDefinition);
			availableLeadsDataTableControl.setRowsPerPage(10);
			availableLeadsDataTableControl.addParameter("mapLeadsfilterByString", requestWrapper.getParameter("useWirecenters"));
			availableLeadsDataTableControl.addParameter("availableLeadsDataTable", true);

			uiContext.addControl(availableLeadsDataTableControl);
		    }
		} else {
		    String sql = null;
		    DatabaseResult dbLeadResult = null;

		    GenericUIControl availableLeadsDataControl = new GenericUIControl();
		    availableLeadsDataControl.setTemplateName(controlDefinition);
		    availableLeadsDataControl.addParameter("availableLeadsData", true);
		    if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
			sql = QueryUtils.getQuery().get("COUNT_OF_AVAILABLE_LEADS");
			if (StringUtils.isNotBlank(requestWrapper.getParameter("selectedFiles"))) {
			    String useWirecenters = requestWrapper.getParameter("useWirecenters");
//			    if ("CLLI".equalsIgnoreCase(useWirecenters)) {
//				sql = sql + " AND CLLI IN (" + requestWrapper.getParameter("selectedFiles") + ")";
//			    } else if ("CITY".equalsIgnoreCase(useWirecenters)) {
//				sql = sql + " AND CITY IN (" + requestWrapper.getParameter("selectedFiles") + ")";
//			    } else {
//				sql = sql + " AND LFA_CODE IN (" + requestWrapper.getParameter("selectedFiles") + ")";
//			    }
			    if(useWirecenters!=null && useWirecenters.contains("|")){
			    	String territory1 = "LFA".equalsIgnoreCase(useWirecenters.split("\\|")[0])?useWirecenters.split("\\|")[0]+"_CODE":useWirecenters.split("\\|")[0];
			    	String territory2 = "LFA".equalsIgnoreCase(useWirecenters.split("\\|")[1])?useWirecenters.split("\\|")[1]+"_CODE":useWirecenters.split("\\|")[1];
			    	String[] selectedFilesArray = requestWrapper.getParameter("selectedFiles").split(",");
			    	sql = sql + " AND "+territory1+" IN ( ";
			    	for(String file: selectedFilesArray){
			    		sql = sql + file.split("\\|")[0]+"',";
			    	}
			    	sql = sql.endsWith(",")?sql.substring(0, sql.length()-1) + " )":sql + " )";
			    	sql = sql + " AND "+territory2+" IN ( ";
			    	for(String file: selectedFilesArray){
			    		sql = sql + "'"+file.split("\\|")[1]+",";
			    	}
			    	sql = sql.endsWith(",")?sql.substring(0, sql.length()-1) + " )":sql + " )";
			    }else{
			    	useWirecenters = "LFA".equalsIgnoreCase(useWirecenters)?useWirecenters+"_CODE":useWirecenters;
			    	sql = sql + " AND "+useWirecenters+" IN ( "+ requestWrapper.getParameter("selectedFiles") + " )";
			    }
			}
			logger.debug("selectedFiles for File filtering: " + requestWrapper.getParameter("selectedFiles"));
			logger.debug("Available lead count query: " + sql);
			dbLeadResult = getPlatformService().loadResult(sql, listLeads);
			for (List row : dbLeadResult.getData()) {
			    MapObject paramValueColumn = (MapObject) row.get(0);
			    availableLeadsDataControl.addParameter("NumberOfLeads", paramValueColumn.getValue());
			}
		    } else
			availableLeadsDataControl.addParameter("NumberOfLeads", 0);
		    if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
			sql = QueryUtils.getQuery().get("COUNT_OF_UNMAPPED_LEADS");
			if (StringUtils.isNotBlank(requestWrapper.getParameter("selectedFiles"))) {
//			    sql = sql + " AND CLLI IN (" + requestWrapper.getParameter("selectedFiles") + ")";
			    String useWirecenters = requestWrapper.getParameter("useWirecenters");
			    if(useWirecenters!=null && useWirecenters.contains("|")){
			    	String territory1 = "LFA".equalsIgnoreCase(useWirecenters.split("\\|")[0])?useWirecenters.split("\\|")[0]+"_CODE":useWirecenters.split("\\|")[0];
			    	String territory2 = "LFA".equalsIgnoreCase(useWirecenters.split("\\|")[1])?useWirecenters.split("\\|")[1]+"_CODE":useWirecenters.split("\\|")[1];
			    	String[] selectedFilesArray = requestWrapper.getParameter("selectedFiles").split(",");
			    	sql = sql + " AND "+territory1+" IN ( ";
			    	for(String file: selectedFilesArray){
			    		sql = sql + file.split("\\|")[0]+"',";
			    	}
			    	sql = sql.endsWith(",")?sql.substring(0, sql.length()-1) + " )":sql + " )";
			    	sql = sql + " AND "+territory2+" IN ( ";
			    	for(String file: selectedFilesArray){
			    		sql = sql + "'"+file.split("\\|")[1]+",";
			    	}
			    	sql = sql.endsWith(",")?sql.substring(0, sql.length()-1) + " )":sql + " )";
			    }else{
			    	useWirecenters = "LFA".equalsIgnoreCase(useWirecenters)?useWirecenters+"_CODE":useWirecenters;
			    	sql = sql + " AND "+useWirecenters+" IN ( "+ requestWrapper.getParameter("selectedFiles") + " )";
			    }				
			}
			logger.debug("selectedFiles for File filtering: " + requestWrapper.getParameter("selectedFiles"));
			logger.debug("Available lead count query: " + sql);
			dbLeadResult = getPlatformService().loadResult(sql, listLeads);
			for (List row : dbLeadResult.getData()) {
			    MapObject paramValueColumn = (MapObject) row.get(0);
			    availableLeadsDataControl.addParameter("NumberOfUnMappedLeads", paramValueColumn.getValue());
			    break;
			}
		    } else
			availableLeadsDataControl.addParameter("NumberOfUnMappedLeads", 0);
		    availableLeadsDataControl.addParameter("territoryType",requestWrapper.getParameter("territoryType"));
		    uiContext.addControl(availableLeadsDataControl);
		}
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "leadSheetGeneratedtable")) {
		if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
		    // Sample Implementation for Genric Table Control
		    List list = new ArrayList();
		    list.add(new Integer(requestWrapper.getParameter("officeSeq")));
		    list.add(new Integer(requestWrapper.getParameter("campaignSeq")));
		    String leadSheetID = requestWrapper.getParameter("leadSheetID");
		    String sql = QueryUtils.getQuery().get("SELECT_ALL_TEMP_LEADSHEETS");
		    if (StringUtils.isNotBlank(leadSheetID) && StringUtils.isNotBlank(leadSheetID.replaceAll("%", ""))) {
			sql = sql + " AND L.LEADSHEET_NAME LIKE ?";
			list.add(leadSheetID.trim());
		    }

		    // System.out.println("campaignSeq for leadSheetGeneratedtable: "
		    // + requestWrapper.getParameter("campaignSeq"));
		    // System.out.println("officeSeq for leadSheetGeneratedtable: "
		    // + requestWrapper.getParameter("officeSeq"));

		    TableUIControl leadSheetGeneratedtableControl = new TableUIControl();
		    leadSheetGeneratedtableControl.setFilterParamList(list);
		    leadSheetGeneratedtableControl.setQueryString(sql);
		    leadSheetGeneratedtableControl.setColumnName("GENERATED_DATE DESC");
		    leadSheetGeneratedtableControl.setTableName(uiContext.getControlName());
		    leadSheetGeneratedtableControl.setJavaScriptMethodName("loadLeadSheetGenerated");
		    leadSheetGeneratedtableControl.addParameter("leadSheetGeneratedtable", true);
		    leadSheetGeneratedtableControl.setTemplateName("../" + controlDefinition);
		    leadSheetGeneratedtableControl.setRowsPerPage(10);

		    uiContext.addControl(leadSheetGeneratedtableControl);
		}
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "generatedLeadsCount")) {
		if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
		    // Sample Implementation for Genric Table Control
		    List list = new ArrayList();
		    list.add(new Integer(requestWrapper.getParameter("officeSeq")));
		    list.add(new Integer(requestWrapper.getParameter("campaignSeq")));
		    DatabaseResult countResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_GENERATED_LS_LEADS_COUNT"), list);
		    GenericUIControl leadSheetGeneratedtableControl = new GenericUIControl();
		    leadSheetGeneratedtableControl.addParameter("generatedLeadsCount", true);
		    leadSheetGeneratedtableControl.addParameter("generatedCount", countResult.getData().get(0).get(0).getValue());
		    leadSheetGeneratedtableControl.setTemplateName("../" + controlDefinition);
		    uiContext.addControl(leadSheetGeneratedtableControl);
		}
	    } 
	    
	 	else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "generatedproperty")) {
            GenericUIControl propertyCtrl = new GenericUIControl();
	    	try
	    	{
	    			String property=null;
			        List param_values = new ArrayList();
			        param_values.add(requestWrapper.getParameter("templateID"));
			        param_values.add("PropIDValue");
			        DatabaseResult result = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_PARAM_PROPERTY_BY_TEMPLATE_ID"), param_values);
			    	for (List row : result.getData()) 
			    	{
					    MapObject paramValueColumn = (MapObject) row.get(0);
					    if(result != null && result.getData() != null && result.getData().get(0) != null && result.getData().get(0).get(0) != null)
				        {
				        	property = result.getData().get(0).get(0).getValue();
				        }	
					}
			        if(property != null && !"".equals(property))
			        {
			               List values = new ArrayList();
			               values.add(new Integer(requestWrapper.getParameter("campaignSeq")));
			               values.add(new Integer(requestWrapper.getParameter("officeSeq")));
			               values.add(requestWrapper.getParameter("templateID"));
			               ProcessContext processContext = new ProcessContext();
			               DatabaseResult databaseResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TEMPLATE_PROPERTY"), values);
			               String assignresult = databaseResult.getData().get(0).get(0).getValue();
			               if(assignresult==null || assignresult.equalsIgnoreCase(""))
			               {
			                   propertyCtrl.setResponseText(assignresult);
			               }
			               else
			               {
			                   propertyCtrl.setResponseText(assignresult);
			               }
			        }
			        else
			        {
			        	 propertyCtrl.setResponseText("false");
			        }
	    		}	catch (Exception e) 
	    			{
			    		propertyCtrl.setResponseText("ERROR");
					}
	    				uiContext.addControl(propertyCtrl);
	          }
	    
	   
	 	else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "generatedPS01"))
	 	{
	 		GenericUIControl ps01control = new GenericUIControl();
	 		String territoryType = requestWrapper.getParameter("useWirecenters");
	 		try
	    	{
	 			 List values = new ArrayList();
	             values.add(new Integer(requestWrapper.getParameter("campaignSeq")));
	             values.add(new Integer(requestWrapper.getParameter("officeSeq")));
	             values.add(requestWrapper.getParameter("selectedUploadedFiles"));
	             values.add(requestWrapper.getParameter("useWirecenters"));
	             DatabaseResult databaseResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_PS01_LEAD"), values);
	             String assignresult = databaseResult.getData().get(0).get(0).getValue();
	               if(assignresult==null || assignresult.equalsIgnoreCase(""))
	               {
	            	   ps01control.setResponseText(assignresult);
	               }
	               else
	               {
	            	   ps01control.setResponseText(assignresult);
	               }
	             
	             
	    	}
	 		catch(Exception e)
	 		{
	 			ps01control.setResponseText("ERROR");
	 		}
	 			uiContext.addControl(ps01control);
	 	}
	    
	    else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "savedLeadsCount")) {
		if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
		    // Sample Implementation for Genric Table Control
		    List list = new ArrayList();
		    list.add(new Integer(requestWrapper.getParameter("officeSeq")));
		    list.add(new Integer(requestWrapper.getParameter("campaignSeq")));
		    DatabaseResult countResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_SAVED_LS_LEADS_COUNT"), list);
		    GenericUIControl leadSheetGeneratedtableControl = new GenericUIControl();
		    leadSheetGeneratedtableControl.addParameter("savedLeadsCount", true);
		    String count = countResult.getData().get(0).get(0).getValue();
		    leadSheetGeneratedtableControl.addParameter("savedCount", StringUtils.isEmpty(count)?"0":count);
		    leadSheetGeneratedtableControl.setTemplateName("../" + controlDefinition);
		    uiContext.addControl(leadSheetGeneratedtableControl);
		}
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "leadSheetSavedtable")) {
		if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
		    List repInputs = new ArrayList();
		    repInputs.add(new Integer(requestWrapper.getParameter("officeSeq")));
		    DatabaseResult repResult = null;
		    if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
			repResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_MERGED_OFFICES"), repInputs);
		    String sql = QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
		    if (repResult != null)
			for (List<MapObject> objects : repResult.getData()) {
			    sql = sql + " UNION ALL " + QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
			    repInputs.add(new Integer(objects.get(0).getValue()));
			}

		    sql = "SELECT * FROM ( " + sql + " ) AS TEMP ORDER BY REP_NAME";
		    if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
			repResult = getPlatformService().loadResult(sql, repInputs);

		    // System.out.println("campaignSeq for leadSheetGeneratedtable: "
		    // + requestWrapper.getParameter("campaignSeq"));
		    // System.out.println("officeSeq for leadSheetGeneratedtable: "
		    // + requestWrapper.getParameter("officeSeq"));
		    List list = new ArrayList();
		    // for 1st query
		    list.add(new Integer(requestWrapper.getParameter("officeSeq")));
		    list.add(new Integer(requestWrapper.getParameter("campaignSeq")));
		    TableUIControl leadSheetSavedControl = new TableUIControl();
		    String leadSheetID = requestWrapper.getParameter("leadSheetID");

		    String sql2 = QueryUtils.getQuery().get("SELECT_ALL_LEADSHEETS_BY_LEADSHEET_ID");

		    if (StringUtils.isNotBlank(leadSheetID) && !"%".equals(leadSheetID)) {
			sql2 = sql2 + "AND L.LEADSHEET_NAME LIKE ?";
			list.add(leadSheetID.trim());
		    }
		    leadSheetSavedControl.setQueryString(sql2);

		    leadSheetSavedControl.setFilterParamList(list);
		    // leadSheetSavedControl.setQueryKeyName("SELECT_ALL_LEADSHEETS");
		    leadSheetSavedControl.setColumnName("CREATED_DATE DESC");
		    leadSheetSavedControl.setTableName(uiContext.getControlName());
		    leadSheetSavedControl.setJavaScriptMethodName("loadLeadSheetSaved");
		    leadSheetSavedControl.addParameter("leadSheetSavedtable", true);
		    leadSheetSavedControl.addParameter("repResult", repResult);
		    leadSheetSavedControl.setTemplateName("../" + controlDefinition);
		    leadSheetSavedControl.setRowsPerPage(25);
		    uiContext.addControl(leadSheetSavedControl);
		}
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "generateLeadSheet")) {

		ProcessContext processContext = new ProcessContext();
		processContext.setProcessTaskName("verizonfios/LeadSheetGeneration.bsh");
		Enumeration<String> paramNames = requestWrapper.getParameterNames();
		while (paramNames.hasMoreElements()) {
		    String param = paramNames.nextElement();
		    processContext.addParameter(param, requestWrapper.getParameter(param));
		}

		DatabaseResult templateParamsResult = null;

		if (StringUtils.equalsIgnoreCase("SYSTEM_DEFAULT", requestWrapper.getParameter("templateName"))) {

		    // Load Template Params for this Campaign and Office
		    if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
			templateParamsResult = getPlatformService().loadSystemDefaultTemplateParams(
				Integer.parseInt(requestWrapper.getParameter("campaignSeq")), Integer.parseInt(requestWrapper.getParameter("officeSeq")),
				requestWrapper.getParameter("templateName"));
		} else {

		    // Load Template Params for this Campaign and Office
		    if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
			templateParamsResult = getPlatformService().loadTemplateParams(Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
				Integer.parseInt(requestWrapper.getParameter("officeSeq")), requestWrapper.getParameter("templateName"));
		}

		boolean byLeadProximity = false;
		Map<String, String> templateParamMap = new HashMap<String, String>();
		if (templateParamsResult.getData().size() > 0) {
		    for (List<MapObject> row : templateParamsResult.getData()) {
			try {
			    if ("byLeadProximity".equalsIgnoreCase(row.get(0).getValue()))
				byLeadProximity = true;

			    processContext.addParameter(row.get(0).getValue(), row.get(1).getValue());
			} catch (Exception e) {
			    e.printStackTrace();
			}
		    }
		}
		JSONArray unAssignedLeads = JSONArray.fromObject(JSONSerializer.toJSON(processContext.getParameters().get("leads")));
		for (int i = 0; i < unAssignedLeads.size(); i++) {
		    if (unAssignedLeads.get(i) instanceof JSONNull) {
			unAssignedLeads.remove(i);
			i--;
		    }
		}
		processContext.addParameter("unAssignedLeads", unAssignedLeads);

		JSONArray drawnPolygons = JSONArray.fromObject(JSONSerializer.toJSON(processContext.getParameters().get("drawnPolygons")));
		for (int i = 0; i < drawnPolygons.size(); i++) {
		    if (drawnPolygons.get(i) instanceof JSONNull) {
			drawnPolygons.remove(i);
			i--;
		    }
		}
		processContext.addParameter("drawnPolygons", drawnPolygons);

		String selectedFiles = null;
		if (StringUtils.isNotBlank(requestWrapper.getParameter("selectedUploadedFiles"))) {
		    selectedFiles = requestWrapper.getParameter("selectedUploadedFiles");
		    System.out.println("Show the Selected File from Generation Pages" + selectedFiles);
		}
		processContext.addParameter("selectedFiles", selectedFiles);
		processContext.addParameter("useWirecenters", requestWrapper.getParameter("useWirecenters"));
		if (byLeadProximity) {

		    String clientKey = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("clientKey");
		    JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator.getService(clientKey + "JdbcTemplate");
		    PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator.getService(clientKey + "TxManager");
		    DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);

		    if (unAssignedLeads.size() > 0) {
			Map leadsMap = new HashMap();
			for (int i = 0; i < unAssignedLeads.size(); i++) {
			    String array = (String) unAssignedLeads.get(i);
			    // System.out.println("Processing For One Polygon...with lead Count :"+array.split(",").length);

			    TempLeadSheet tempLeadSheet = new TempLeadSheet();
			    tempLeadSheet.setCampaignSeq(campaignSeq);
			    tempLeadSheet.setOfficeSeq(officeSeq);

			    for (String leadRow : array.split(",")) {
				try {
				    if (leadsMap.containsKey(leadRow)) {
					continue;
				    } else {
					leadsMap.put(leadRow, leadRow);
				    }

				    if (StringUtils.isBlank(tempLeadSheet.getLeadSheetId())) {
					tempLeadSheet.setWireCenterName(leadRow.split(":")[2]);
					String uniqueID = new Long(System.nanoTime()).toString();
					tempLeadSheet.setLeadSheetId(leadRow.split(":")[4] + "-" + tempLeadSheet.getWireCenterName() + "-" + uniqueID);
					tempLeadSheet.setLeadSheetName(leadRow.split(":")[4] + "-" + tempLeadSheet.getWireCenterName() + "-"
						+ uniqueID.substring(11));
				    }
				    tempLeadSheet.getLeadIds().add(leadRow.split(":")[0]);
				    tempLeadSheet.getRowIds().add(new Long(leadRow.split(":")[1]));
				    tempLeadSheet.getGeoPoints().add(leadRow.split(":")[5]);
				} catch (Exception e) {
				    e.printStackTrace();
				    logger.error(e);
				}
			    }
			    if (!tempLeadSheet.getLeadIds().isEmpty()) {
			    	String drawnPolygonCord = null ;
			    	if (drawnPolygons != null && drawnPolygons.size() != 0 && drawnPolygons.size() <= i){
					
			    		drawnPolygonCord = (String) drawnPolygons.get(i);
			    	}
				String tKey = dbUtil.generateTerritoryKeyForLeadsheet(tempLeadSheet.getLeadSheetId(), drawnPolygonCord);
				tempLeadSheet.setTerritoryType(requestWrapper.getParameter("useWirecenters"));
				dbUtil.insertLeadSheets(tempLeadSheet, tKey);
				getPlatformService().generateL2LProximityLeadSheetsByTempSheets(tempLeadSheet.getLeadSheetId(),
					new Integer(processContext.getParameters().get("maxLeadsbyLeadProximityInSet").toString()),
					requestWrapper.getParameter("officeSeq"), tKey,  requestWrapper.getParameter("useWirecenters"));

				tempLeadSheet.setCampaignSeq(campaignSeq);
				tempLeadSheet.setOfficeSeq(officeSeq);
				tempLeadSheet.getLeadIds().clear();
				tempLeadSheet.getRowIds().clear();
				tempLeadSheet.getGeoPoints().clear();

			    }

			}
		    } else {
			String tempSheetId = dbUtil.generateTempSheetForProximity(requestWrapper.getParameter("campaignSeq"),
				requestWrapper.getParameter("officeSeq"), requestWrapper.getParameter("selectedUploadedFiles"),
				requestWrapper.getParameter("useWirecenters"));
			getPlatformService().generateFiosL2LProximityLeadSheetsByTempSheets(tempSheetId,
				new Integer(processContext.getParameters().get("maxLeadsbyLeadProximityInSet").toString()),
				requestWrapper.getParameter("officeSeq"), null, requestWrapper.getParameter("useWirecenters"));

		    }

		}

		else {
		    ProcessExecutor.executeAssignmentProcess(processContext);
		}

		GenericUIControl control = new GenericUIControl();

		/*
		 * List<TempLeadSheet> finalLeadSheets = (List) processContext
		 * .getParameters().get("generatedLeadSheetIds"); String
		 * response = ""; String leadSheetIds = "";
		 * 
		 * for (TempLeadSheet tempLeadSheet : finalLeadSheets) {
		 * leadSheetIds = leadSheetIds + ",'" +
		 * tempLeadSheet.getLeadSheetId() + "'"; }
		 * 
		 * if (StringUtils.isNotBlank(leadSheetIds)) leadSheetIds =
		 * leadSheetIds.substring(1); if
		 * ("NONE".equalsIgnoreCase(requestWrapper
		 * .getParameter("templateName"))) { response =
		 * CydcorUtils.getKMLforLeadSheets(leadSheetIds);
		 * 
		 * } else { response = "ALL-GENERATED"; } finalLeadSheets =
		 * null;
		 */
		System.gc();
		List listLeads = new ArrayList();
		listLeads.add(new Integer(requestWrapper.getParameter("campaignSeq")));
		listLeads.add(new Integer(requestWrapper.getParameter("officeSeq")));

		String sql = QueryUtils.getQuery().get("COUNT_OF_AVAILABLE_LEADS");
		if (StringUtils.isNotBlank(requestWrapper.getParameter("selectedUploadedFiles"))) {
		    sql = sql + " AND CLLI IN (" + requestWrapper.getParameter("selectedUploadedFiles") + ")";
		}

		DatabaseResult dbLeadResult = getPlatformService().loadResult(sql, listLeads);
		control.setResponseText(dbLeadResult.getData().get(0).get(0).getValue());
		uiContext.addControl(control);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveLeadSheets")) {
		GenericUIControl mergeCtrl = new GenericUIControl();
		try {
		    String result = getPlatformService().saveOrMergeLeadsheets(requestWrapper.getParameter("leadSheetIds"),
			    requestWrapper.getParameter("savedLeadSheet"), true, false, campaignSeq, officeSeq);
		    mergeCtrl.setResponseText(result);
		} catch (Exception e) {
		    mergeCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(mergeCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "mergeGeneratedAndSavedLeadSheets")) {
		List values = new ArrayList();
		values.add(requestWrapper.getParameter("savedLeadSheet"));
		ProcessContext processContext = new ProcessContext();
		DatabaseResult databaseResult = getPlatformService().loadResult("SELECT PERSON_ID FROM IMS.IMS_LEADSHEET WHERE LEADSHEET_ID = ?", values);
		String assognedRep = databaseResult.getData().get(0).get(0).getValue();
		boolean isAssigned = false;
		GenericUIControl mergeCtrl = new GenericUIControl();
		if (StringUtils.isNotBlank(assognedRep)) {
		    isAssigned = true;
		} else {
		    isAssigned = false;
		}
		try {
		    String result = getPlatformService().saveOrMergeLeadsheets(requestWrapper.getParameter("leadSheetIds").replaceAll("'", ""),
			    requestWrapper.getParameter("savedLeadSheet"), false, isAssigned, campaignSeq, officeSeq);
		    mergeCtrl.setResponseText(result);
		} catch (Exception e) {
		    mergeCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(mergeCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "deleteAllLeadSheets")) {
		GenericUIControl control = new GenericUIControl();
		// To Delete all Temp Leadsheets
		getPlatformService().update("DELETE FROM IMS.IMS_TEMP_LEADSHEET WHERE CAMPAIGN_SEQ =" + campaignSeq + " AND OFFICE_SEQ=" + officeSeq,
			new Object[] {});
		control.setResponseText("ALL-DELETED");
		uiContext.addControl(control);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "AssignReps")) {
		ProcessContext processContext = new ProcessContext();
		processContext.setProcessTaskName("verizonfios/AssignReps.bsh");
		String leadSheetReps = requestWrapper.getParameter("leadSheetReps");
		String[] pairs = leadSheetReps.split(",");

		Enumeration<String> paramNames = requestWrapper.getParameterNames();
		while (paramNames.hasMoreElements()) {
		    String param = paramNames.nextElement();
		    processContext.addParameter(param, requestWrapper.getParameter(param));
		}
		processContext.addParameter("leadSheetReps", pairs);
		ProcessExecutor.executeAssignmentProcess(processContext);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "deleteTempLeadSheet")) {
		String leadSheetId = requestWrapper.getParameter("leadSheetId");
		GenericUIControl control = new GenericUIControl();
		control.setResponseText(CydcorUtils.getKMLforLeadSheets("'" + leadSheetId + "'"));
		uiContext.addControl(control);
		getPlatformService().update(QueryUtils.getQuery().get("DELETE_TEMP_LEAD_SHEET"), new Object[] { leadSheetId });

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveTempLeadSheet")) {
		GenericUIControl mergeCtrl = new GenericUIControl();
		try {
		    String result = getPlatformService().saveOrMergeLeadsheets(requestWrapper.getParameter("leadSheetIds").replaceAll("'", ""),
			    requestWrapper.getParameter("savedLeadSheet"), false, false, campaignSeq, officeSeq);
		    mergeCtrl.setResponseText(result);
		} catch (Exception e) {
		    mergeCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(mergeCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "deleteSavedLeadSheet")) {
		String leadSheetId = requestWrapper.getParameter("leadSheetId");
		GenericUIControl control = new GenericUIControl();
		/*
		 * control.setResponseText(CydcorUtils.getKMLforLeadSheets("'" +
		 * leadSheetId + "'"));
		 */
		control.setResponseText("deleted");
		uiContext.addControl(control);

		/*
		 * getPlatformService().update(
		 * QueryUtils.getQuery().get("DELETE_SAVED_LEAD_SHEET"), new
		 * Object[] { leadSheetId });
		 */

		// DELETE FROM IMS.IMS_LEADSHEET WHERE LEADSHEET_ID=?
		getPlatformService().update("DELETE FROM IMS.IMS_LEADSHEET WHERE LEADSHEET_ID IN (" + leadSheetId + ")", new Object[] {});

		/*
		 * getPlatformService().update( QueryUtils.getQuery().get(
		 * "UPDATE_LEAD_SHEET_ID_LEAD_LIFECYCLE"), new Object[] {
		 * leadSheetId });
		 */

		// UPDATE IMS.IMS_LEAD_LIFECYCLE SET PERSON_ID=NULL
		// ,LEAD_SHEET_ID=NULL WHERE LEAD_SHEET_ID=?
		getPlatformService().update(
			"UPDATE IMS.IMS_LEAD_LIFECYCLE SET PERSON_ID=NULL ,LEAD_SHEET_ID=NULL WHERE LEAD_SHEET_ID IN (" + leadSheetId + ")", new Object[] {});

	    } 
	    else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "defaultViewDetails")) {

		List list = new ArrayList();
		list.add(requestWrapper.getParameter("campaignSeq"));
		list.add(requestWrapper.getParameter("officeSeq"));
		list.toArray(new Object[0]);
		DatabaseResult dbResult = null;
		if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
		    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_TERRITORY_VIEW"), list);

		GenericUIControl defaultViewDetailsControl = new GenericUIControl();
		defaultViewDetailsControl.setTemplateName(controlDefinition);
		defaultViewDetailsControl.addParameter("defaultViewDetails", true);
		defaultViewDetailsControl.addParameter("result", dbResult);

		/*
		 * List listLeads = new ArrayList(); listLeads.add(new
		 * Integer(requestWrapper .getParameter("campaignSeq")));
		 * listLeads.add(new Integer(requestWrapper
		 * .getParameter("officeSeq")));
		 * 
		 * DatabaseResult dbLeadResult =
		 * getPlatformService().loadResult(
		 * QueryUtils.getQuery().get("COUNT_OF_AVAILABLE_LEADS"),
		 * listLeads); for (List row : dbLeadResult.getData()) {
		 * MapObject paramValueColumn = (MapObject) row.get(0);
		 * defaultViewDetailsControl.addParameter("NumberOfLeads",
		 * paramValueColumn.getValue()); }
		 */

		uiContext.addControl(defaultViewDetailsControl);

	    } else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "mergeLeadSheets")) {
		String selectedSheets = requestWrapper.getParameter("selectedLeadSheets");
		logger.info("selectedLeadSheets :" + selectedSheets);
		String firstSheetId = selectedSheets.split(":")[0].split("'")[1];
		DatabaseResult result = getPlatformService().loadResult(
			"SELECT LEADSHEET_NAME FROM IMS.IMS_TEMP_LEADSHEET WHERE LEADSHEET_ID ='" + firstSheetId + "'");
		String firstSheetName = result.getData().get(0).get(0).getValue();
		
			
		getPlatformService().update(
			"UPDATE IMS.IMS_TEMP_LEADSHEET SET LEADSHEET_ID=?, LEADSHEET_NAME=?, CREATED_DATE=GETDATE() WHERE LEADSHEET_ID IN (" + selectedSheets.replaceAll(":", ",")
				+ ")", new Object[] { firstSheetId, firstSheetName });
		GenericUIControl control = new GenericUIControl();
		control.setResponseText("SUCCESS");
		uiContext.addControl(control);

	    } else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "deleteSelectedTempLeadSheets")) {
		String selectedSheets = requestWrapper.getParameter("selectedLeadSheets");
		getPlatformService().update("DELETE FROM IMS.IMS_TEMP_LEADSHEET WHERE LEADSHEET_ID IN (" + selectedSheets.replaceAll(":", ",") + ")", new Object[] {});
		GenericUIControl control = new GenericUIControl();
		control.setResponseText("SUCCESS");
		uiContext.addControl(control);
	    } else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "colorCodes")) {
		DatabaseResult dbResult = null;

		List inputs = new ArrayList();
		inputs.add(CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("clientKey"));
		dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_DISPOSITION_LIST"), inputs);

		List<MapObject> defaultDisp = new ArrayList<MapObject>();
		MapObject object = new MapObject();
		object.setValue("Default");
		defaultDisp.add(object);
		object = new MapObject();
		object.setValue("Not Dispositioned");
		defaultDisp.add(object);
		object = new MapObject();
		object.setValue("assets/images/mapicons/red-dot.png");
		defaultDisp.add(object);
		dbResult.getData().add(0, defaultDisp);
		List returnList = new ArrayList();
		int size = (dbResult.getData().size() / 3) + (dbResult.getData().size() % 3 > 0 ? 1 : 0);
		boolean hasReminder = dbResult.getData().size() % 3 > 0 ? true : false;
		int j = 1;
		for (int i = 1; i <= size; i++) {
		    int startIdx = j - 1;
		    int lastIdx = i * 3;
		    if (i == size && hasReminder)
			lastIdx = dbResult.getData().size();
		    returnList.add(dbResult.getData().subList(startIdx, lastIdx));
		    j = j + 3;
		}

		GenericUIControl colorCodesControl = new GenericUIControl();
		colorCodesControl.setTemplateName(controlDefinition);
		colorCodesControl.addParameter("colorCodes", true);
		colorCodesControl.addParameter("dispositionResult", returnList);
		uiContext.addControl(colorCodesControl);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "renameLeadSheet")) {
		GenericUIControl updateNameCtrl = new GenericUIControl();

		try {
		    getPlatformService().update("UPDATE IMS.IMS_TEMP_LEADSHEET SET LEADSHEET_NAME =? WHERE LEADSHEET_ID = ?",
			    new Object[] { requestWrapper.getParameter("leadSheetName"), requestWrapper.getParameter("leadSheetId") });
		    updateNameCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    logger.error(e);
		    updateNameCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateNameCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "checkExpiredLeadSheets")) {
		GenericUIControl expiryCheckCtrl = new GenericUIControl();
		String leadSheets = requestWrapper.getParameter("selectedLeadSheets");
		// DatabaseResult result = getPlatformService()
		// .loadResult(
		// "SELECT LEADSHEET_ID FROM IMS.IMS_LEADSHEET WHERE  (GETDATE()-CREATED_DATE < 60  OR CAMPAIGN_SEQ = '29958807') AND LEADSHEET_ID in ("
		// + leadSheets + ")");
		// if (result.getData().size() < leadSheets.split(",").length) {
		// String validSheets = "";
		// for (List<MapObject> objects : result.getData()) {
		// for (MapObject object : objects) {
		// validSheets = validSheets + "," + object.getValue();
		// }
		// }
		// if (!validSheets.equals("")) {
		// validSheets = validSheets.substring(1);
		// }
		// expiryCheckCtrl.setResponseText("ERROR:" +
		// result.getData().size() + ":" + validSheets);
		// } else {
		// expiryCheckCtrl.setResponseText("SUCCESS:" +
		// leadSheets.replaceAll("'", ""));
		// }
		expiryCheckCtrl.setResponseText("SUCCESS:" + leadSheets.replaceAll("'", ""));
		uiContext.addControl(expiryCheckCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeRep")) {

		String repId = requestWrapper.getParameter("repId");
		String assgnDate = requestWrapper.getParameter("assignedDate");
		String sheetId = requestWrapper.getParameter("leadSheetId");
		if (StringUtils.equalsIgnoreCase(repId, "-1")) {
		    repId = null;
		    assgnDate = null;
		}
		GenericUIControl updateRepCtrl = new GenericUIControl();

		try {
		    getPlatformService().update(QueryUtils.getQuery().get("ASSIGN_LEADSHEET_REP"), new Object[] { repId, assgnDate, sheetId });
		    getPlatformService().update(QueryUtils.getQuery().get("ASSIGN_LEADS_REP"), new Object[] { repId, sheetId });

		    updateRepCtrl.setResponseText("SUCCESS:" + StringUtils.isNotBlank(repId));
		} catch (Exception e) {
		    logger.error(e);
		    updateRepCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateRepCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updateReturnDate")) {

		String returnDate = requestWrapper.getParameter("returnDate");
		String sheetId = requestWrapper.getParameter("leadSheetId");

		GenericUIControl updateRepCtrl = new GenericUIControl();
		try {
		    getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_RETURNDATE"), new Object[] { returnDate, sheetId });
		    updateRepCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    logger.error(e);
		    updateRepCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateRepCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeNotes")) {

		String notes = requestWrapper.getParameter("notes").trim();
		String sheetId = requestWrapper.getParameter("leadSheetId");

		GenericUIControl updateRepCtrl = new GenericUIControl();
		try {
		    getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_NOTES"), new Object[] { notes, sheetId });
		    updateRepCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    logger.error(e);
		    updateRepCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateRepCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeSales")) {
		String sales = requestWrapper.getParameter("sales").trim();
		String sheetId = requestWrapper.getParameter("leadSheetId");

		GenericUIControl updateRepCtrl = new GenericUIControl();
		try {
		    getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_SALES"), new Object[] { sales, sheetId });
		    updateRepCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    logger.error(e);
		    updateRepCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateRepCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeDecisionMaker")) {

		String dm = requestWrapper.getParameter("decisionMaker").trim();
		String sheetId = requestWrapper.getParameter("leadSheetId");

		GenericUIControl updateRepCtrl = new GenericUIControl();
		try {
		    getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_DM"), new Object[] { dm, sheetId });
		    updateRepCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    logger.error(e);
		    updateRepCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateRepCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updateRedLeadFlag")) {
		String leadsheets = "'" + requestWrapper.getParameter("leadSheetId") + "'";
		if (StringUtils.isNotBlank(requestWrapper.getParameter("leadSheets"))) {
		    leadsheets = "'" + requestWrapper.getParameter("leadSheets") + "'";
		    leadsheets = leadsheets.replaceAll("!", "', '");
		}
		getPlatformService().update("UPDATE IMS.IMS_LEADSHEET SET RED_LEAD_FLAG=? WHERE LEADSHEET_ID IN (" + leadsheets + ")  AND RED_LEAD_FLAG =?",
			new Object[] { "N", "Y" });

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeDoorsKnocked")) {

		String dm = requestWrapper.getParameter("doorsKnocked").trim();
		String sheetId = requestWrapper.getParameter("leadSheetId");

		GenericUIControl updateRepCtrl = new GenericUIControl();
		try {
		    getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_DK"), new Object[] { dm, sheetId });
		    updateRepCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    logger.error(e);
		    updateRepCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateRepCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "getModalWindow")) {

		GenericUIControl modalWindowCtrl = new GenericUIControl();
		modalWindowCtrl.addParameter("modalWindow", true);
		modalWindowCtrl.setTemplateName(controlDefinition);
		try {
		    List repInputs = new ArrayList();
		    repInputs.add(new Integer(requestWrapper.getParameter("officeSeq")));
		    DatabaseResult repResult;
		    repResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_MERGED_OFFICES"), repInputs);
		    String sql = QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
		    for (List<MapObject> objects : repResult.getData()) {
			sql = sql + " UNION ALL " + QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
			repInputs.add(new Integer(objects.get(0).getValue()));
		    }

		    sql = "SELECT * FROM ( " + sql + " ) AS TEMP ORDER BY REP_NAME";
		    repResult = getPlatformService().loadResult(sql, repInputs);

		    modalWindowCtrl.addParameter("repResult", repResult);
		} catch (Exception e) {
		    logger.error(e);
		}
		uiContext.addControl(modalWindowCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "editSavedLeadSheet")) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String repId = requestWrapper.getParameter("repAssigned").trim();
		String assnDate = requestWrapper.getParameter("repAssignedDate").trim();
		boolean alreadyAssigned = true;

		String status = "INP";
		if (StringUtils.equalsIgnoreCase(repId, "-1") || StringUtils.equalsIgnoreCase(repId, "")) {
		    repId = null;
		    assnDate = null;
		}
		if (repId != null && assnDate.trim().equalsIgnoreCase("")) {
		    assnDate = dateFormat.format(System.currentTimeMillis());
		    alreadyAssigned = false;
		}

		String returnedDate = requestWrapper.getParameter("returnedDate").trim();
		if (returnedDate.equalsIgnoreCase("")) {
		    returnedDate = null;
		} else {
		    status = "X";
		}

		String sales = requestWrapper.getParameter("sales").trim();
		String dm = requestWrapper.getParameter("decisionMaker").trim();
		String dk = requestWrapper.getParameter("doorsKnocked").trim();
		String notes = requestWrapper.getParameter("notes").trim();
		String sheetId = requestWrapper.getParameter("leadSheetId");

		GenericUIControl updateLSCtrl = new GenericUIControl();
		try {
		    getPlatformService().update(QueryUtils.getQuery().get("UPDATE_LEADSHEET"),
			    new Object[] { repId, assnDate, returnedDate, sales, dm, dk, notes, status, sheetId });
		    if (assnDate != null && !alreadyAssigned) {
			getPlatformService().update(QueryUtils.getQuery().get("ASSIGN_LEADS_REP"), new Object[] { repId, sheetId });
		    }
		    updateLSCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    logger.error(e);
		    updateLSCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateLSCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updatePrinted")) {

		GenericUIControl updatePrintedCtrl = new GenericUIControl();
		List<String> values = new ArrayList<String>();
//		for(String sheet : requestWrapper.getParameter("leadSheets").split(",")){
		for(String sheet : requestWrapper.getParameter("leadSheets").split("!")){
		    values.add(sheet);
		}
		try {
		    getPlatformService().updateLeadsheetPrintStatus(values);
		    updatePrintedCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    updatePrintedCtrl.setResponseText("ERROR");
		    logger.error(e);
		}
		uiContext.addControl(updatePrintedCtrl);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "checkCookie")) {
		GenericUIControl cookieCtrl = new GenericUIControl();

		Cookie[] cookies = requestWrapper.getCookies();
		String cookieKey = "map_" + officeSeq;
		String cookieValue = checkInCookies(cookies, cookieKey);
		cookieCtrl.setResponseText(cookieValue);
		uiContext.addControl(cookieCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "setCookie")) {
		GenericUIControl cookieCtrl = new GenericUIControl();
		String cookieKey = "map_" + officeSeq;
		String value = requestWrapper.getParameter("open");
		String cookieValue = "OPEN";
		if ("true".equalsIgnoreCase(value)) {
		    cookieValue = "OPEN";
		} else {
		    cookieValue = "CLOSE";
		}
		Cookie cookie = new Cookie(cookieKey, cookieValue);
		cookie.setMaxAge(365 * 24 * 60 * 1000);
		responseWrapper.addCookie(cookie);
		cookieCtrl.setResponseText(cookieValue);
		uiContext.addControl(cookieCtrl);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updateLeadsheets")) {

		JSONArray rds = JSONArray.fromObject(JSONSerializer.toJSON(requestWrapper.getParameter("Rds")));
		JSONArray sheetIds = JSONArray.fromObject(JSONSerializer.toJSON(requestWrapper.getParameter("ids")));
		JSONArray notes = JSONArray.fromObject(JSONSerializer.toJSON(requestWrapper.getParameter("Notes")));
		JSONArray dms = JSONArray.fromObject(JSONSerializer.toJSON(requestWrapper.getParameter("Dms")));
		JSONArray dks = JSONArray.fromObject(JSONSerializer.toJSON(requestWrapper.getParameter("Dks")));
		JSONArray sales = JSONArray.fromObject(JSONSerializer.toJSON(requestWrapper.getParameter("Sales")));
		JSONArray reps = JSONArray.fromObject(JSONSerializer.toJSON(requestWrapper.getParameter("Reps")));
		JSONArray ads = JSONArray.fromObject(JSONSerializer.toJSON(requestWrapper.getParameter("Ads")));
		List<String[]> values = new ArrayList<String[]>();
		for (int i = 0; i < sheetIds.size(); i++) {
		    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		    String repId = (String) reps.get(i);
		    String assnDate = (String) ads.get(i);
		    String status = "INP";
		    if (StringUtils.equalsIgnoreCase(repId, "-1") || StringUtils.equalsIgnoreCase(repId, "")) {
			repId = null;
			assnDate = null;
		    }
		    if (repId != null && assnDate.trim().equalsIgnoreCase("")) {
			assnDate = dateFormat.format(System.currentTimeMillis());
		    }

		    String returnedDate = (String) rds.get(i);
		    if (returnedDate.equalsIgnoreCase("")) {
			returnedDate = null;
		    } else {
			status = "X";
		    }

		    String[] valObject = new String[] { repId, assnDate, returnedDate,
			    (((String) sales.get(i)).equalsIgnoreCase("") ? null : (String) sales.get(i)),
			    ((String) dms.get(i)).equalsIgnoreCase("") ? null : (String) dms.get(i),
			    ((String) dks.get(i)).equalsIgnoreCase("") ? null : (String) dks.get(i),
			    ((String) notes.get(i)).equalsIgnoreCase("") ? null : (String) notes.get(i), status, (String) sheetIds.get(i) };
		    values.add(valObject);
		}
		GenericUIControl updateRepCtrl = new GenericUIControl();
		try {
		    getPlatformService().updateLeadSheets(values);
		    getPlatformService().updateLeadSheetNotes(values);
		    updateRepCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    logger.error(e);
		    updateRepCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateRepCtrl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclTerritoryListTable")) {
			TableUIControlReportFios iclTerritoryList = new TableUIControlReportFios();			
			List<VerizonFiosReport> recordList = new LinkedList<VerizonFiosReport>();
			String territoryType = requestWrapper.getParameter("territoryType");
			if(!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")){
				if(requestWrapper.getParameter("reload").equalsIgnoreCase("true")){
					if(requestWrapper.getParameter("filter")!=null && requestWrapper.getParameter("filter").equals("true")){
						if(!("-1".equalsIgnoreCase(territoryType))){							
							recordList = filterTerritoryList((List<VerizonFiosReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST_FIOS_"+
									requestWrapper.getParameter("officeSeq")+"_"+territoryType), 
								requestWrapper);//(List<VerizonFiosReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST_FIOS");
						}
					}else{
						recordList = (List<VerizonFiosReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST_FIOS_"+
					requestWrapper.getParameter("officeSeq")+"_"+territoryType);
					}
				}else{
					if(!("-1".equalsIgnoreCase(territoryType))){	
						recordList = getPlatformService().getFiosTrackerByTerritoryType(requestWrapper.getParameter("campaignSeq"), 
								requestWrapper.getParameter("officeSeq"),
								territoryType);
					}
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST_FIOS_"+requestWrapper.getParameter("officeSeq")+"_"+territoryType);
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST_FIOS_"+requestWrapper.getParameter("officeSeq")+"_"+territoryType, recordList);
				}
				if(null != recordList && recordList.size() > 0){
					recordList = sortTerritoryList(recordList, requestWrapper);
					iclTerritoryList.setListRecords(recordList);
				}else{
					iclTerritoryList.setListRecords(new LinkedList<VerizonFiosReport>());
				}
			}
			iclTerritoryList.setTemplateName("../" + controlDefinition);
			iclTerritoryList.setTableName("iclTerritoryListTable");
			iclTerritoryList.setJavaScriptMethodName("loadPagination");
			iclTerritoryList.setPageNumber(1);
			iclTerritoryList.addParameter("iclTerritoryList", true);
			iclTerritoryList.addParameter("territoryType",territoryType);
			iclTerritoryList.addParameter("trackerDataFormat",requestWrapper.getParameter("trackerDataFormat"));
			if(StringUtils.isNotBlank(requestWrapper.getParameter("rowsPerPage")) && !"undefined".equalsIgnoreCase(requestWrapper.getParameter("rowsPerPage"))){				
				iclTerritoryList.setRowsPerPage(Integer.valueOf(requestWrapper.getParameter("rowsPerPage")));
			}
			uiContext.addControl(iclTerritoryList);

		}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "sortTable")) {
			//System.out.println("in sortTable controller "+requestWrapper.getParameter("columnId")+" "+requestWrapper.getParameter("order"));
			List<VerizonFiosReport> list = null;	
			String territoryType = requestWrapper.getParameter("territoryType");
			if(requestWrapper.getParameter("filter")!=null && requestWrapper.getParameter("filter").equals("true")){
				list = filterTerritoryList((List<VerizonFiosReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST_FIOS_"+
						requestWrapper.getParameter("officeSeq")+"_"+territoryType), 
						requestWrapper);//(List<VerizonFiosReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_FILTERED_LIST_FIOS");
			}else{
				list = (List<VerizonFiosReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST_FIOS_"+
						requestWrapper.getParameter("officeSeq")+"_"+territoryType);
			}
			list = sortTerritoryList(list, requestWrapper);

			TableUIControlReportFios sortTable= new TableUIControlReportFios();
			sortTable.setListRecords(list);
			sortTable.setTemplateName("../" + controlDefinition);
			sortTable.setTableName("iclTerritoryListTable");
			sortTable.setJavaScriptMethodName("loadPagination");
			sortTable.setPageNumber(1);
			sortTable.addParameter("iclTerritoryList", true);
			sortTable.addParameter("trackerDataFormat",requestWrapper.getParameter("trackerDataFormat"));
			sortTable.addParameter("territoryType",territoryType);
			if(StringUtils.isNotBlank(requestWrapper.getParameter("rowsPerPage"))){
				sortTable.setRowsPerPage(Integer.valueOf(requestWrapper.getParameter("rowsPerPage")));
			}
			uiContext.addControl(sortTable);
		}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "pagination")) {
			List<VerizonFiosReport> list = null;
			String territoryType = requestWrapper.getParameter("territoryType");
			if(!requestWrapper.getParameter("reload").equalsIgnoreCase("true")){
				if(!("-1".equalsIgnoreCase(territoryType))){	
					list = getPlatformService().getFiosTrackerByTerritoryType(requestWrapper.getParameter("campaignSeq"), 
							requestWrapper.getParameter("officeSeq"),
							territoryType);
					CydcorContext.getInstance().getCrmContext().remove("TERRITORY_MASTER_LIST_FIOS_"+requestWrapper.getParameter("officeSeq")+"_"+territoryType);
					CydcorContext.getInstance().getCrmContext().put("TERRITORY_MASTER_LIST_FIOS_"+requestWrapper.getParameter("officeSeq")+"_"+territoryType, list);
				}
			}
			if(requestWrapper.getParameter("filter").equals("true")){
				list = filterTerritoryList((List<VerizonFiosReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST_FIOS_"+
						requestWrapper.getParameter("officeSeq")+"_"+territoryType), 
						requestWrapper);
			}else{
				list = (List<VerizonFiosReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST_FIOS_"+
			requestWrapper.getParameter("officeSeq")+"_"+territoryType);
			}
			if(null != list && list.size() > 0){
				list = sortTerritoryList(list, requestWrapper);
			}else{
				list = new ArrayList<VerizonFiosReport>();
			}
			TableUIControlReportFios paginationTable = new TableUIControlReportFios();
			paginationTable.setListRecords(list);
			paginationTable.setTemplateName("../" + controlDefinition);
			paginationTable.setTableName("iclTerritoryListTable");
			paginationTable.setJavaScriptMethodName("loadPagination");
			paginationTable.setPageNumber(new Integer(requestWrapper.getParameter("pageNumber")));
			paginationTable.addParameter("iclTerritoryList", true);
			paginationTable.addParameter("trackerDataFormat",requestWrapper.getParameter("trackerDataFormat"));
			paginationTable.addParameter("territoryType",territoryType);
			if(StringUtils.isNotBlank(requestWrapper.getParameter("rowsPerPage"))){
				paginationTable.setRowsPerPage(Integer.valueOf(requestWrapper.getParameter("rowsPerPage")));
			}
			uiContext.addControl(paginationTable);
		}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "filterListTable")) {
			List<VerizonFiosReport> list = null;
			List<VerizonFiosReport> filteredList = null;
			String territoryType = requestWrapper.getParameter("territoryType");
			list = (List<VerizonFiosReport>) CydcorContext.getInstance().getCrmContext().get("TERRITORY_MASTER_LIST_FIOS_"+
			requestWrapper.getParameter("officeSeq")+"_"+territoryType);
			if(null != list && list.size() > 0){
				filteredList = filterTerritoryList(list, requestWrapper);
				filteredList = sortTerritoryList(filteredList, requestWrapper);				
			}
			
			TableUIControlReportFios filteredTable= new TableUIControlReportFios();
			filteredTable.setListRecords(filteredList);
			filteredTable.setTemplateName("../" + controlDefinition);
			filteredTable.setTableName("iclTerritoryListTable");
			filteredTable.setJavaScriptMethodName("loadPagination");
			filteredTable.setPageNumber(1);
			filteredTable.addParameter("iclTerritoryList", true);
			filteredTable.addParameter("trackerDataFormat",requestWrapper.getParameter("trackerDataFormat"));
			filteredTable.addParameter("territoryType",territoryType);
			if(StringUtils.isNotBlank(requestWrapper.getParameter("rowsPerPage"))){
				filteredTable.setRowsPerPage(Integer.valueOf(requestWrapper.getParameter("rowsPerPage")));
			}
			uiContext.addControl(filteredTable);
		}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "showICLNotes")) {
			GenericUIControl displayNotes = new GenericUIControl();
			displayNotes.setTemplateName("../" + controlDefinition);
			//List list = getPlatformService().getB2BNotesByZip(requestWrapper.getParameter("officeSeq"), requestWrapper.getParameter("zip"));
			List list = getFiosNotes(requestWrapper);
			if(list!=null && list.size()>0){
				displayNotes.addParameter("result", list);				
			}
			//displayNotes.addParameter("officeSeq", requestWrapper.getParameter("officeSeq"));
			displayNotes.addParameter("zip", requestWrapper.getParameter("zip"));
			displayNotes.addParameter("clli", requestWrapper.getParameter("clli"));
			displayNotes.addParameter("city", requestWrapper.getParameter("city"));
			displayNotes.addParameter("lfa", requestWrapper.getParameter("lfa"));
			displayNotes.addParameter("displayICLNotes", true);
			uiContext.addControl(displayNotes);
		}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveNotes")) {
			GenericUIControl saveNotes = new GenericUIControl();
			saveNotes.setTemplateName("../" + controlDefinition);
//			getPlatformService().updateNotesByZip(requestWrapper.getParameter("officeSeq"), 
//					requestWrapper.getParameter("zip"), requestWrapper.getParameter("clli"), requestWrapper.getParameter("notes"));
			getPlatformService().update(QueryUtils.getQuery().get("INSERT_FIOS_TRACKER_NOTES"),
					new Object[] { requestWrapper.getParameter("zip"), 
				requestWrapper.getParameter("clli"), requestWrapper.getParameter("city"), requestWrapper.getParameter("lfa"),requestWrapper.getParameter("notes")});
			List list = getFiosNotes(requestWrapper);
			if(list!=null && list.size()>0){
				saveNotes.addParameter("result", list);				
			}
			//saveNotes.addParameter("officeSeq", requestWrapper.getParameter("officeSeq"));
			saveNotes.addParameter("zip", requestWrapper.getParameter("zip"));
			saveNotes.addParameter("clli", requestWrapper.getParameter("clli"));
			saveNotes.addParameter("city", requestWrapper.getParameter("city"));
			saveNotes.addParameter("lfa", requestWrapper.getParameter("lfa"));
			saveNotes.addParameter("displayICLNotes", true);
			uiContext.addControl(saveNotes);
		}else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "showICLHistory")) {
			GenericUIControl displayHistory = new GenericUIControl();
			displayHistory.setTemplateName("../" + controlDefinition);
			//List list = getPlatformService().getB2BHistoryByZip(requestWrapper.getParameter("officeSeq"), requestWrapper.getParameter("zip"));
			List list = getFiosHistory(requestWrapper);
			if(null != list && list.size() > 0){
				displayHistory.addParameter("result", list);
			}
			displayHistory.addParameter("displayICLHistory", true);
			uiContext.addControl(displayHistory);
		}
	}
    }

    public Object getResult(Object imput, Map<String, Object> paramMap) {
	// TODO Auto-generated method stub
	return null;
    }

    public void init(Object params) {
	// TODO Auto-generated method stub

    }

    private List<VerizonFiosReport> filterTerritoryList(List<VerizonFiosReport> list, CydcorRequestWrapper requestWrapper){

    	for(int i=1;i<=4;i++){
        	String column = requestWrapper.getParameter("filtercol"+i);
        	String operation = requestWrapper.getParameter("filterop"+i);
        	String value = requestWrapper.getParameter("filterval"+i);
        	boolean percentage = "percentage".equalsIgnoreCase(requestWrapper.getParameter("trackerDataFormat"))?true:false;
        	if(column.equalsIgnoreCase("promo")){
        		column=column+":"+requestWrapper.getParameter("selectedPromoCode");
        	}
        	if(StringUtils.isNotBlank(column) && StringUtils.isNotBlank(operation) && StringUtils.isNotBlank(value))
        		list = VerizonFiosTrackerFilter.filterList(list, value, column, operation, percentage);
    	}
    	return list;   	
    }
    
    private List<VerizonFiosReport> sortTerritoryList(List<VerizonFiosReport> list, CydcorRequestWrapper requestWrapper){
 		List<Comparator> comparators = new LinkedList<Comparator>();
 		boolean percentage = "percentage".equalsIgnoreCase(requestWrapper.getParameter("trackerDataFormat"))?true:false;
 		comparators.add(new GenericComparator(false,"getSortSeq",requestWrapper.getParameter("selectedPromoCode"),percentage));
 		JSONArray sortedColumns = JSONArray.fromObject(JSONSerializer.toJSON(requestWrapper.getParameter("sortedColumnsJsonArray")));
		if(sortedColumns.size()>0){
			//System.out.println("Sort options exist");			
			for (int i = 0; i < sortedColumns.size(); i++) {
			    if (sortedColumns.get(i) instanceof JSONNull) {
			    	//System.out.println("inst of null");
			    	continue;
			    }else{
			    	 String sortColumnName = ((String)sortedColumns.get(i)).split(":")[0].replace("_", ""); 
			    	 if(sortColumnName.equalsIgnoreCase("dateToBeWorked") || sortColumnName.equalsIgnoreCase("datelastWorked"))
			    		 sortColumnName = sortColumnName + "Date";
			    	 Method[] methods = new VerizonFiosReport().getClass().getMethods();
			         for (int j = 0; j < methods.length; j++) {
			             String methodName = methods[j].getName();
			             if (!methodName.startsWith("get")
			                     || methods[j].getParameterTypes().length > 0)
			                 continue;
			             String fieldName = methodName.substring(3);					             
			             if (fieldName.equalsIgnoreCase(sortColumnName)){
			            	comparators.add(new GenericComparator(((String)sortedColumns.get(i)).split(":")[1].equals("DESC")?true:false,
			            			methodName,requestWrapper.getParameter("selectedPromoCode"),percentage));
			            	 //System.out.println("fieldName="+fieldName);
				             //System.out.println("sortedColumns.get(i)).split(\",\")[0]="+sortedColumns.get(i));
			            	 break;		
			             }
			         }
			    }
			}
		}else{
			//System.out.println("Sort options do not exist");
			comparators.add(new GenericComparator(false,"getZip",requestWrapper.getParameter("selectedPromoCode"),percentage));
		}
		DelegateComparator delegateComparator = new DelegateComparator(comparators);
 		Collections.sort(list, delegateComparator);
 		return list;
    }
    
    private List getFiosNotes(CydcorRequestWrapper requestWrapper){
    	String territoryType = requestWrapper.getParameter("territoryType");
    	List inputParams = new ArrayList();
		//inputParams.add(requestWrapper.getParameter("officeSeq"));
		if("ZIP".equalsIgnoreCase(territoryType)){
			inputParams.add(requestWrapper.getParameter("zip"));
		}else if("CLLI".equalsIgnoreCase(territoryType)||"CLLI|DA".equalsIgnoreCase(territoryType)){
			territoryType = "CLLI";
			inputParams.add(requestWrapper.getParameter("clli"));
		}else if("CITY".equalsIgnoreCase(territoryType)||"CITY|DA".equalsIgnoreCase(territoryType)){
			territoryType = "CITY";
			inputParams.add(requestWrapper.getParameter("city"));
		}else if("LFA".equalsIgnoreCase(territoryType)||"LFA|DA".equalsIgnoreCase(territoryType)){
			territoryType = "LFA";
			inputParams.add(requestWrapper.getParameter("lfa"));
		}else if("LFA|CLLI".equalsIgnoreCase(territoryType)){
			inputParams.add(requestWrapper.getParameter("lfa"));
			inputParams.add(requestWrapper.getParameter("clli"));
			territoryType=territoryType.replace("|", "_");
		}
		DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_FIOS_TRACKER_NOTES_"+territoryType), inputParams);
		if(dbResult!=null){
			List list = dbResult.getData();
			List<String> notesList = new ArrayList<String>();
			if(null != list && list.size() > 0){
				for(Object mapObjList: list){
					List mapObjects = (List)mapObjList;
					notesList.add(((MapObject)mapObjects.get(0)).getValue());
				}
				return notesList;
			}
		}    	
		return null;
    }
    
    private List getFiosHistory(CydcorRequestWrapper requestWrapper){
    	String territoryType = requestWrapper.getParameter("territoryType");
    	List inputParams = new ArrayList();
		if("ZIP".equalsIgnoreCase(territoryType)){
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("zip"));
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("zip"));
		}else if("CLLI".equalsIgnoreCase(territoryType)||"CLLI|DA".equalsIgnoreCase(territoryType)){
			territoryType = "CLLI";
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("clli"));
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("clli"));
		}else if("CITY".equalsIgnoreCase(territoryType)||"CITY|DA".equalsIgnoreCase(territoryType)){
			territoryType = "CITY";
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("city"));
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("city"));
		}else if("LFA".equalsIgnoreCase(territoryType)||"LFA|DA".equalsIgnoreCase(territoryType)){
			territoryType = "LFA";
			inputParams.add(requestWrapper.getParameter("lfa"));
		}else if("LFA|CLLI".equalsIgnoreCase(territoryType)){
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("lfa"));
			inputParams.add(requestWrapper.getParameter("clli"));
			inputParams.add(requestWrapper.getParameter("officeSeq"));
			inputParams.add(requestWrapper.getParameter("lfa"));
			inputParams.add(requestWrapper.getParameter("clli"));
			territoryType = territoryType.replace("|", "_");
		}
		DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_FIOS_TRACKER_HISTORY_"+territoryType), inputParams);
		if(dbResult!=null){
			List list = dbResult.getData();
			List<String> historyList = new ArrayList<String>();
			if(null != list && list.size() > 0){
				for(Object mapObjList: list){
					List mapObjects = (List)mapObjList;
					historyList.add(((MapObject)mapObjects.get(0)).getValue());
				}
				return historyList;
			}
		}
		return null;
    }
    
    @SuppressWarnings("unchecked")
    private void saveAndMergeSheets(CydcorRequestWrapper requestWrapper, ProcessContext processContext) {

	processContext.setProcessTaskName("verizonfios/SaveLeadSheets.bsh");
	Enumeration<String> paramNames = requestWrapper.getParameterNames();
	while (paramNames.hasMoreElements()) {
	    String param = paramNames.nextElement();
	    processContext.addParameter(param, requestWrapper.getParameter(param));
	}

	ProcessExecutor.executeAssignmentProcess(processContext);

    }
    private String checkInCookies(Cookie[] cookies, String cookieKey) {
	for (int i = 0; i < cookies.length; i++) {
	    Cookie cookie = cookies[i];
	    if (cookieKey.equals(cookie.getName()))
		return cookie.getValue();
	}
	return null;
    }
    
    private List<VerizonFiosReport> getRepordDataList(String territoryType) {
    List<VerizonFiosReport>	recordList = new LinkedList<VerizonFiosReport>();
	for (int i = 0; i < 50; i++) {
		Random rand = new Random();
		int  n = rand.nextInt(30) + i;
		int m = rand.nextInt(3);
		VerizonFiosReport vr = new VerizonFiosReport();
		vr.setDataEligible(n*i);
		vr.setDatelastWorked("04/1"+m+"/2014");
		vr.setDateToBeWorked("05/2"+rand.nextInt(4)+"/2014");
		vr.setDatelastWorkedDate(new Date(vr.getDatelastWorked()));
		vr.setDateToBeWorkedDate(new Date(vr.getDateToBeWorked()));
		vr.setDisconnectDateUnderTwoYears(n*3);
		vr.setDslMigration(m+425);
		vr.setExistingVoiceCustomer(n+1200);
//		vr.setExistingVoiceCustomers(n*6);
		vr.setIontDifferentCustomers(n+515);
		vr.setIontSameCustomer(rand.nextInt(3)+571);
		vr.setLeadsBetweenThirtyOneAndNinetyDays(rand.nextInt(6)+2166);
		vr.setLeadsThirtyDaysAndUnder(n+915);
		vr.setMduMduY(n+2553);
		vr.setOpenSeasonMduY(n+5256);
		vr.setDisabledMdu(rand.nextInt(3)*6);
		vr.setPercentageNewlyLitBetweenThirtyOneAndNinetyDays(new Double("56.56"));
		vr.setPercentageNewlyLitThirtyDaysOrLess(new Double(".27")*n);
//		vr.setPromo(2*n*i*m);
		vr.setRowId(n);
		vr.setStatus("status12345");
		vr.setTerritoryName("CHASMDCH"+i);
		vr.setTotal(125010 + rand.nextInt(20));
		vr.setTotalAvailable(95650 + rand.nextInt(30));
		vr.setTotalIont(1521+rand.nextInt(3));
		vr.setTvPenetrationRate(".59");
		vr.setVoiceAndData(rand.nextInt(3)+5565);
		vr.setVoiceDataTv(n+665);
		vr.setNotes("NotesNotes");
		vr.setSortSeq(3);
		vr.setWcLatitude("34.0500");
		vr.setWcLongitude("-118.2500");
		vr.setZip("12345");
		vr.setCity("city");
		vr.setClli("clli");
		vr.setLfa("lfa");
		vr.setOffice_seq("61843277");
		vr.setTerritoryType("CLLI");
		recordList.add(vr);
	}
	return recordList;
    }

}
