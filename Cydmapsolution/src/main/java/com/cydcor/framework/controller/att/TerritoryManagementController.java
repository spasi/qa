/**
 * 
 */
package com.cydcor.framework.controller.att;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;

import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.census.Territory;
import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.GenericController;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

/**
 * Manages all Territory Assignment In The system
 * 
 * @author ashwin
 * 
 */
@Controller("attTerritoryManagementController")
@Scope("singleton")
public class TerritoryManagementController extends GenericController {
    private static final transient Log logger = LogFactory.getLog(TerritoryManagementController.class);
    String pageDefnintion = "components/TerritoryManagementDef_en_US.xml";
    String controlDefnintion = "components/TerritoryManagement_en_US.xml";

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cydcor.framework.controller.Controller#execute(com.cydcor.framework
     * .context.DynamicUIContext)
     */
    public void execute(DynamicUIContext uiContext) {
	CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
	SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	Calendar todayDate = new GregorianCalendar();

	// Load Page Here
	if (StringUtils.isBlank(uiContext.getControlName())
			|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
			"true"))) {
	    GenericUIControl territoryDefControl = new GenericUIControl();
	    territoryDefControl.setTemplateName(pageDefnintion);
	    territoryDefControl.addParameter("campaignSeq", requestWrapper.getParameter("campaignSeq"));
	    territoryDefControl.addParameter("officeSeq", requestWrapper.getParameter("officeSeq"));

	    /*
	     * As this is common territory definition for all territory pages.
	     * This parameter is used to distinguish where we have to store
	     * corresponding child territories
	     * 
	     * For Example if territoryFor == ICL then we shall store data in
	     * IMS_ICL_TERRITORY_DEF else if it is ICLLEAD we store in
	     * IMS_ICLLEAD_TERRITORY_DEF
	     */

	    uiContext.addControl(territoryDefControl);

	} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "loadTerritory")) {
	    GenericUIControl territoryUIControl = new GenericUIControl();
	    territoryUIControl.setTemplateName(controlDefnintion);

	    uiContext.addControl(territoryUIControl);
	} else if ("wireCenterStatesSelector".equalsIgnoreCase(uiContext.getControlName())) {
	    DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_STATES"));
	    GenericUIControl statesControl = new GenericUIControl();
	    statesControl.setTemplateName(controlDefnintion);
	    statesControl.addParameter("wireCenterStatesSelector", true);
	    statesControl.addParameter("result", dbResult);

	    uiContext.addControl(statesControl);

	} else if ("wireCenterSelector".equalsIgnoreCase(uiContext.getControlName())) {
	    /*
	     * String queryString =
	     * "SELECT DISTINCT WIRECENTER_NAME FROM IMS.IMS_WIRECENTERS WHERE WIRECENTER_NAME IS NOT NULL AND STATE_NAME like '"
	     * + requestWrapper.getParameter("stateId") +
	     * "' ORDER BY WIRECENTER_NAME ";
	     */

	    List list = new ArrayList();
	    list.add(requestWrapper.getParameter("stateId"));
	    list.toArray(new Object[0]);

	    DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_WIRECENTER_BY_STATEID"), list);
	    GenericUIControl statesControl = new GenericUIControl();
	    statesControl.setTemplateName(controlDefnintion);
	    statesControl.addParameter("wireCenterSelector", true);
	    statesControl.addParameter("result", dbResult);

	    uiContext.addControl(statesControl);

	} else if ("daSelector".equalsIgnoreCase(uiContext.getControlName())) {
	    /*
	     * String queryString =
	     * "SELECT DISTINCT DA_NAME FROM IMS.IMS_DA WHERE DA_NAME IS NOT NULL AND WIRECENTER_NAME like '"
	     * + requestWrapper.getParameter("wcId") + "' ORDER BY DA_NAME";
	     */
	    List list = new ArrayList();
	    list.add(requestWrapper.getParameter("wcId"));
	    list.toArray(new Object[0]);

	    DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_DA"), list);
	    GenericUIControl statesControl = new GenericUIControl();
	    statesControl.setTemplateName(controlDefnintion);
	    statesControl.addParameter("daSelector", true);
	    statesControl.addParameter("result", dbResult);

	    uiContext.addControl(statesControl);

	} else if ("countiesSelector".equalsIgnoreCase(uiContext.getControlName())) {

	    /*
	     * String queryString =
	     * "SELECT DISTINCT TERRITORY_NAME,TERRITORY_KEY FROM IMS.IMS_TERRITORY WHERE STATE_NAME IS NOT NULL AND STATE_NAME LIKE '"
	     * + requestWrapper.getParameter("stateName") +
	     * "' ORDER BY TERRITORY_NAME"; DatabaseResult dbResult =
	     * getPlatformService().loadResult( queryString);
	     */
	    List list = new ArrayList();
	    list.add(requestWrapper.getParameter("stateId"));
	    list.toArray(new Object[0]);

	    DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_COUNTIES"), list);
	    GenericUIControl countiesControl = new GenericUIControl();
	    countiesControl.setTemplateName(controlDefnintion);
	    countiesControl.addParameter("countiesSelector", true);
	    countiesControl.addParameter("result", dbResult);

	    uiContext.addControl(countiesControl);
	} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveTerritory")) {
	    /**
	     * Whole Logic Here is Generic and Based On Followign Parameters
	     * 
	     * controlName : "saveTerritory", wireCenter : wireCenter, da : da,
	     * stateName : stateName, repId: personID, loadTemplate : "false" ,
	     * campaignSeq : campaignSeq, officeSeq : officeSeq, territoryType:
	     * territoryType, countyName: countyName, territoryFor: territoryFor
	     * 
	     */

		String clientKey = (String) requestWrapper.getParameter("clientKey");
		if (clientKey == null)
			clientKey = "";
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator
				.getService(clientKey+"JdbcTemplate");
		PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator
				.getService(clientKey+"TxManager");
	    DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);

	    String wireCenter = requestWrapper.getParameter("wireCenter");
	    String da = requestWrapper.getParameter("da");
	    String stateName = requestWrapper.getParameter("stateName");
	    String repId = requestWrapper.getParameter("repId");
	    String campaignSeq = requestWrapper.getParameter("campaignSeq");
	    String officeSeq = requestWrapper.getParameter("officeSeq");
	    String territoryType = requestWrapper.getParameter("territoryType");
	    String countyName = requestWrapper.getParameter("countyName");
	    String territoryFor = requestWrapper.getParameter("territoryFor");
	    String parentTerritoryKey = requestWrapper.getParameter("parentTerritoryKey");

	    if (StringUtils.equalsIgnoreCase("-1", parentTerritoryKey)) {
		parentTerritoryKey = null;
	    }
	    String territoryName = "";

	    /***
	     * if territoryFor == ICL and parentTerritoryKey is Null then 1)
	     * create one record in IMS_ICL_TERRITORY_DEF along with campaign
	     * and officeseq. 2) Get the key of above inserted record and then
	     * 3) Store this territory in IMS_ICL_TERRITORY_MAP
	     * 
	     * if territoryFor == ICLLEAD and parentTerritoryKey is Null then 1)
	     * create one record in IMS_ICLLEAD_TERRITORY_DEF along with
	     * campaign and officeseq. 2) Get the key of above inserted record
	     * and then 3) Store this territory in IMS_ICLLEAD_TERRITORY_MAP
	     */

	    Territory territory = new Territory();
	    // If it is DA then Group of DA is Wirecenter
	    if (territoryType.equalsIgnoreCase("DA") && !StringUtils.isBlank(da)) {
		territoryName = da;
		territory.setGroup(wireCenter);
	    } else if (territoryType.equalsIgnoreCase("WIRECENTER") && !StringUtils.isBlank(da)) {
		territoryName = wireCenter;
		territory.setGroup(wireCenter);
	    } else if (territoryType.equalsIgnoreCase("COUNTY")) {
		territoryName = countyName;
	    } else {
		territoryName = wireCenter;
	    }

	    territory.setName(territoryName);
	    territory.setCountyTypeId(null);
	    territory.setType(territoryType);
	    territory.setDescription("TerritoryType:" + territory.getType());
	    territory.setStateName(stateName);
	    territory.setStateId(stateName);
	    territory.setActive(Boolean.TRUE);

	    /**
	     * 
	     * if territoryFor == ICLLEAD and parentTerritoryKey is Null then
	     * Insert into IMS_ICLLEAD_TERRITORY_DEF and get
	     * LEAD_TERRITORY_DEF_SEQ (ONLY INASERT CAMPAING& OFFICE SEQ) select
	     * territory_key from IMS_TERRITORY WHERE TERRITORY_NAME = ? AND
	     * TERRITORY_TYPE = ? ADD TERRITORY_GROUP TO ABOVE CONDITION IF
	     * TERRITORY_TYPE = 'DA'
	     * 
	     * 
	     * AND NOW CHECK IN IMS_ICLLEAD_TERRITORY_MAP IF ABOVE KEYS EXISTS.
	     * IF NOT EXIST THEN INSERT NEW RECORD
	     * 
	     * 
	     */

	    if (territoryFor.equalsIgnoreCase("ICLLEAD")) {
		handleICLLeadSave(uiContext, requestWrapper, da, territoryType, parentTerritoryKey, territoryName, territory);
	    } else if (territoryFor.equalsIgnoreCase("ICL")) {
		handleSaveICLTerritory(uiContext, requestWrapper, da, territoryType, parentTerritoryKey, territoryName, territory);
	    }

	    List<Territory> list = new ArrayList<Territory>();
	    list.add(territory);

	    /*
	     * GenericUIControl territoryUIControl = new GenericUIControl();
	     * territoryUIControl.setResponseText("Successfully Saved");
	     * uiContext.addControl(territoryUIControl);
	     */

	} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "deleteTerritory")) {
	    String territoryFor = requestWrapper.getParameter("territoryFor");

	    String territoryMapSeq = requestWrapper.getParameter("territoryMapSeq");
	    if ("ICLLEAD".equalsIgnoreCase(territoryFor)) {
		getPlatformService().update(QueryUtils.getQuery().get("DELETE_TERRITORY_DETAILS_BY_LEADMAP_SEQ"),
			new Object[] { territoryMapSeq });

	    } else if ("ICL".equalsIgnoreCase(territoryFor)) {
		getPlatformService().update(QueryUtils.getQuery().get("DELETE_TERRITORY_DETAILS_BY_ICL_MAP_SEQ"),
			new Object[] { territoryMapSeq });
	    }
	} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "loadTerritoryDetails")) {
	    String territoryFor = requestWrapper.getParameter("territoryFor");
	    String campaignSeq = requestWrapper.getParameter("campaignSeq");
	    String officeSeq = requestWrapper.getParameter("officeSeq");
	    String parentTerritoryKey = requestWrapper.getParameter("parentTerritoryKey");
	    if ("ICLLEAD".equalsIgnoreCase(territoryFor)) {
		loadTerritoryForICLLEAD(uiContext, campaignSeq, officeSeq, parentTerritoryKey);

	    } else if ("ICL".equalsIgnoreCase(territoryFor)) {
		loadTerritoryForICL(uiContext, campaignSeq, officeSeq, parentTerritoryKey);

	    }
	} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "territoryDefSelector")) {
	    DatabaseResult dbResult = null;
	    GenericUIControl territoryDefControl = new GenericUIControl();
	    String queryString = null;
	    String pageDefnintion = "icl/ICLTerritoryManagementB2BDef.html";
	    String controlDefnintion = "icl/ICLTerritoryManagementB2B.html";
	    String campaignSeq = requestWrapper.getParameter("campaignSeq");
	    String officeSeq = requestWrapper.getParameter("officeSeq");
	    String parentTerritoryKey = requestWrapper.getParameter("parentTerritoryKey");
	    /*
	     * queryString =
	     * "SELECT CAMPAIGN_SEQ,CAMPAIGN_NAME FROM IMS.MERLIN_CAMPAIGN_DETAILS"
	     * ; dbResult = getPlatformService().loadResult(queryString);
	     */

	    List<Object> paramList = new ArrayList<Object>();
	    paramList.add(Integer.parseInt(campaignSeq));
	    paramList.toArray(new Object[0]);
	    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS_BY_KEY"), paramList);

	    String campaignName = "";
	    String officeName = "";

	    for (List<MapObject> row : dbResult.getData()) {
		MapObject campaignNameColumn = row.get(1);
		campaignName = campaignNameColumn.getValue();
		break;
	    }

	    paramList.clear();
	    paramList.add(Integer.parseInt(campaignSeq));
	    paramList.add(Integer.parseInt(officeSeq));
	    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_ICL_DETAILS_BY_KEY"), paramList);

	    for (List<MapObject> row : dbResult.getData()) {
		MapObject officeNameColumn = row.get(2);
		officeName = officeNameColumn.getValue();
		break;
	    }

	    // GET_ALL_REP_FOR_CAMPAIGN_AND_ICL
	    paramList.clear();
	    paramList.add(Integer.parseInt(officeSeq));
		DatabaseResult repResult;
		repResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_MERGED_OFFICES"), paramList);
		String sql = QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
		for (List<MapObject> objects : repResult.getData()) {
			sql = sql + " UNION ALL " + QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
			paramList.add(new Integer(objects.get(0).getValue()));
		}

		sql = "SELECT * FROM ( "+ sql + " ) AS TEMP ORDER BY REP_NAME";
		repResult = getPlatformService().loadResult(sql, paramList);

	    territoryDefControl.addParameter("repList", repResult);

	    paramList.clear();
	    paramList.add(officeSeq);
	    paramList.add(campaignSeq);
	    paramList.add(parentTerritoryKey);
	    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TERRITORY_ICL_DETAILS"), paramList);

	    String territoryName = "";
	    String territoryDescription = "";
	    String activeFlag = "";
	    String createdDate = "";
	    String agentName = "";
	    String repAssignedDate = "";
	    String repReturnedDate = "";
	    String numberOfBusiness = "";
	    String numberOfOrders = "";

	    // DEF.TERRITORY_NAME,DEF.TERRITORY_DESCRIPTION,DEF.ACTIVE,DEF.CREATED_DATE,AGENT_NAME,
	    // SHEET.REP_ASSIGNED_DATE,SHEET.REP_RETURNED_DATE
	    // ,DEF.NUMBER_OF_BUSINESS,DEF.NUMBER_OF_ORDERS

	    for (List<MapObject> row : dbResult.getData()) {
		MapObject column = row.get(0);
		territoryName = column.getValue();

		column = row.get(1);
		territoryDescription = column.getValue();

		column = row.get(2);
		activeFlag = column.getValue();

		column = row.get(3);
		createdDate = column.getValue();

		column = row.get(4);
		agentName = column.getValue();

		column = row.get(5);
		repAssignedDate = column.getValue();

		column = row.get(6);
		repReturnedDate = column.getValue();

		column = row.get(7);
		numberOfBusiness = column.getValue();

		column = row.get(8);
		numberOfOrders = column.getValue();
		break;
	    }

	    // Add Second Control the Campaign Selector
	    territoryDefControl.setTemplateName(controlDefnintion);
	    territoryDefControl.addParameter("territoryDefSelector", true);

	    territoryDefControl.addParameter("campaignName", campaignName);
	    territoryDefControl.addParameter("officeName", officeName);
	    territoryDefControl.addParameter("territoryName", territoryName);
	    territoryDefControl.addParameter("territoryDescription", territoryDescription);
	    territoryDefControl.addParameter("activeFlag", activeFlag);
	    if (StringUtils.isNotBlank(repAssignedDate))
		territoryDefControl.addParameter("createdDate", createdDate);
	    else
		territoryDefControl.addParameter("createdDate", dateFormat.format(todayDate.getTime()));
	    territoryDefControl.addParameter("agentName", agentName);
	    if (StringUtils.isNotBlank(repAssignedDate))
		territoryDefControl.addParameter("repAssignedDate", repAssignedDate);
	    else
		territoryDefControl.addParameter("repAssignedDate", dateFormat.format(todayDate.getTime()));
	    territoryDefControl.addParameter("repReturnedDate", repReturnedDate);
	    territoryDefControl.addParameter("numberOfBusiness", numberOfBusiness);
	    territoryDefControl.addParameter("numberOfOrders", numberOfOrders);

	    uiContext.addControl(territoryDefControl);

	} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveOrUpdateTerritory")) {
	    // For ICLLead Parent Territory Save or Update
	    GenericUIControl saveOrUpdateControl = new GenericUIControl();
	    DatabaseResult dbResult = null;
	    String queryString = null;
	    String pageDefnintion = "icl/ICLTerritoryManagementB2BDef.html";
	    String controlDefnintion = "icl/ICLTerritoryManagementB2B.html";
	    String campaignSeq = requestWrapper.getParameter("campaignSeq");
	    String officeSeq = requestWrapper.getParameter("officeSeq");
	    String personId = requestWrapper.getParameter("personID");
	    String territoryName = requestWrapper.getParameter("territoryName");
	    String status = requestWrapper.getParameter("status");
	    String noOfBusines = requestWrapper.getParameter("noOfBusines");
	    String noOfOrders = requestWrapper.getParameter("noOfOrders");
	    String description = requestWrapper.getParameter("description");
	    String parentTerritoryKey = requestWrapper.getParameter("parentTerritoryKey");
	    String returnedDate = requestWrapper.getParameter("returnedDate");
	    if (StringUtils.isBlank(returnedDate))
		returnedDate = null;
	    String repAssignedDate = requestWrapper.getParameter("repAssignedDate");
	    String createdDate = requestWrapper.getParameter("createdDate");
	    boolean isRepChanged = new Boolean(requestWrapper.getParameter("isRepChanged"));
	    long leadTerritoryDefSeq = 0;
	    if (StringUtils.isBlank(parentTerritoryKey) || StringUtils.equalsIgnoreCase("-1", parentTerritoryKey)) {
		List<Object> alist = new ArrayList<Object>();
		alist.add(campaignSeq);
		alist.add(officeSeq);
		alist.add(territoryName);

		dbResult = getPlatformService().loadResult(
			QueryUtils.getQuery().get("SELECT_TERRITORY_DEF_SEQ_FROM_IMS_ICLLEAD_TERRITORY_DEF"), alist);

		if (dbResult.getData() != null) {
		    for (List<MapObject> row : dbResult.getData()) {
			leadTerritoryDefSeq = Long.parseLong((row.get(0).getValue()));
		    }
		}
	    } else {
		leadTerritoryDefSeq = Long.parseLong(parentTerritoryKey);
	    }

	    if (leadTerritoryDefSeq != 0) {
		List<Object> list = new ArrayList<Object>();
		list.add(territoryName);
		list.add(description);
		list.add(status);
		list.add(noOfBusines);
		list.add(noOfOrders);
		list.add(personId);
		list.add(returnedDate);
		if (personId.equalsIgnoreCase("-1")) {
		    list.add(null);
		} else if (isRepChanged) {
		    list.add(dateFormat.format(todayDate.getTime()));
		} else {
		    list.add(repAssignedDate);
		}
		list.add(officeSeq);
		list.add(campaignSeq);
		list.add(leadTerritoryDefSeq);

		getPlatformService()
			.update(QueryUtils.getQuery().get("UPDATE_IMS_ICL_LEAD_TERRITORY_DEF_PARENT_TERRITORY"), list.toArray());
		saveOrUpdateControl.setResponseText(leadTerritoryDefSeq + "|Successfully Updated");
	    } else {
		// TERRITORY_NAME,TERRITORY_DESCRIPTION, ACTIVE,
		// NUMBER_OF_BUSINESS,
		// NUMBER_OF_ORDERS,PERSON_ID,CAMPAIGN_SEQ,OFFICE_SEQ

		List<Object> list = new ArrayList<Object>();
		list.add(territoryName);
		list.add(description);
		list.add(status);
		list.add(noOfBusines);
		list.add(noOfOrders);
		list.add(personId);
		list.add(campaignSeq);
		list.add(officeSeq);
		list.add(createdDate);
		list.add(returnedDate);
		if (personId.equalsIgnoreCase("-1"))
		    list.add(null);
		else
		    list.add(repAssignedDate);

		leadTerritoryDefSeq = getPlatformService().saveAndGetKey(
			QueryUtils.getQuery().get("INSERT_IMS_ICL_LEAD_TERRITORY_DEF_PARENT_TERRITORY"), list.toArray());
		saveOrUpdateControl.setResponseText(leadTerritoryDefSeq + "|Successfully Saved");
	    }
	    uiContext.addControl(saveOrUpdateControl);
	} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveICLTerritory")) {
	    pageDefnintion = "account/ICLAdminDef.html";
	    controlDefnintion = "account/ICLAdmin.html";
	    String campaignSeq = requestWrapper.getParameter("campaignSeq");
	    String officeSeq = requestWrapper.getParameter("officeSeq");
	    String defaultleads = requestWrapper.getParameter("defaultleads");
	    String maxleads = requestWrapper.getParameter("maxleads");

	    List<Object> list = new ArrayList<Object>();
	    list.add(defaultleads);
	    list.add(maxleads);
	    list.add(campaignSeq);
	    list.add(officeSeq);

	    List<Object> list1 = new ArrayList<Object>();
	    list1.add(requestWrapper.getParameter("officeSeq"));
	    list1.add(requestWrapper.getParameter("campaignSeq"));

	    DatabaseResult result1 = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_LEADS_NUMBER"), list1);

	    if (result1.getData().size() > 0) {
		for (List<MapObject> row : result1.getData()) {
		    if (row.get(0).getValue() == null && row.get(1).getValue() == null) {
			try {

			    updateLeadsInIclTerrotory(campaignSeq, officeSeq, defaultleads, maxleads);
			    break;

			} catch (Exception e) {
			    e.printStackTrace();
			}
		    } else if (Integer.parseInt(row.get(1).getValue()) == 0 && Integer.parseInt(row.get(1).getValue()) == 0) {
			updateLeadsInIclTerrotory(campaignSeq, officeSeq, defaultleads, maxleads);
			break;
		    } else {
			updateLeadsInIclTerrotory(campaignSeq, officeSeq, defaultleads, maxleads);
		    }
		}
	    } else {
		try {
		    getPlatformService().update(QueryUtils.getQuery().get("INSERT_LEADS_IMS_ICL_TERRITORY_DEF"), list.toArray());
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }

	} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "deleteParentTerritory")) {
	    String campaignSeq = requestWrapper.getParameter("campaignSeq");
	    String officeSeq = requestWrapper.getParameter("officeSeq");
	    String territoryFor = requestWrapper.getParameter("territoryFor");
	    String territoryID = requestWrapper.getParameter("territoryID");

	    String pageDefnintion = "icl/RepManagementDef.html";
	    String controlDefnintion = "icl/RepManagement.html";

	    getPlatformService().update(QueryUtils.getQuery().get("DELETE_PARENT_LEAD_TERRITORY_DEF"), new Object[] { territoryID });

	    getPlatformService().update(QueryUtils.getQuery().get("DELETE_PARENT_LEAD_TERRITORY_MAP"), new Object[] { territoryID });
	}
    }

    private void updateLeadsInIclTerrotory(String campaignSeq, String officeSeq, String defaultleads, String maxleads) {
	getPlatformService().update(
		QueryUtils.getQuery().get("UPDATE_IMS_ICL_TERRITORY_DEF_PARENT_TERRITORY"),
		new Object[] { new Integer(defaultleads.toString()), new Integer(maxleads.toString()), new Integer(officeSeq),
			new Integer(campaignSeq) });
    }

    private void loadTerritoryForICL(DynamicUIContext uiContext, String campaignSeq, String officeSeq, String parentTerritoryKey) {
	List<Object> list = new ArrayList<Object>();
	list.add(officeSeq.replaceAll("'", ""));
	list.add(campaignSeq.replaceAll("'", ""));
	pageDefnintion = "account/ICLAdminDef.html";
	controlDefnintion = "account/ICLAdmin.html";
	TableUIControl territoryTableControl = new TableUIControl();

	if (StringUtils.isBlank(parentTerritoryKey) || StringUtils.equalsIgnoreCase(parentTerritoryKey, "-1")) {
	    territoryTableControl.setFilterParamList(list);
	    territoryTableControl.setQueryKeyName("SELECT_ICL_TERRITORIES");
	} else {
	    list.add(parentTerritoryKey);
	    list.toArray(new Object[0]);
	    territoryTableControl.setFilterParamList(list);
	    territoryTableControl.setQueryKeyName("SELECT_ICLLEAD_TERRITORIES");
	}
	territoryTableControl.setColumnName("TERRITORY_NAME");
	territoryTableControl.setJavaScriptMethodName("refreshTerritory");
	territoryTableControl.addParameter("territoryListTable", true);
	territoryTableControl.setTemplateName("../" + controlDefnintion);

	uiContext.addControl(territoryTableControl);
    }

    private void loadTerritoryForICLLEAD(DynamicUIContext uiContext, String campaignSeq, String officeSeq, String parentTerritoryKey) {
	List<Object> list = new ArrayList<Object>();
	list.add(new Integer(campaignSeq));
	list.add(new Integer(officeSeq));

	pageDefnintion = "icl/ICLTerritoryManagementB2BDef.html";
	controlDefnintion = "icl/ICLTerritoryManagementB2B.html";
	TableUIControl territaryListTableControl = new TableUIControl();

	/*
	 * if (StringUtils.isBlank(parentTerritoryKey) ||
	 * StringUtils.equalsIgnoreCase(parentTerritoryKey, "-1")) {
	 * territaryListTableControl.setFilterParamList(list);
	 * territaryListTableControl
	 * .setQueryKeyName("SELECT_ICLREP_TERRITORY_LIST"); } else {
	 */
	list.add(new Integer(parentTerritoryKey));
	territaryListTableControl.setFilterParamList(list);
	territaryListTableControl.setQueryKeyName("SELECT_ICLLEAD_TERRITORY_LIST");
	// }

	territaryListTableControl.setColumnName("TERRITORY_TYPE");
	territaryListTableControl.setJavaScriptMethodName("loadTerritoryTable");
	territaryListTableControl.addParameter("territoryListTable", true);
	territaryListTableControl.setTemplateName("../" + controlDefnintion);

	uiContext.addControl(territaryListTableControl);
    }

    /**
     * @param requestWrapper
     * @param da
     * @param territoryType
     * @param parentTerritoryKey
     * @param territoryName
     * @param territory
     */
    private void handleSaveICLTerritory(DynamicUIContext uiContext, CydcorRequestWrapper requestWrapper, String da, String territoryType,
	    String parentTerritoryKey, String territoryName, Territory territory) {
	long territoryKey = -1;
	long leadTeritoryDefKey = 0;
	List<Object> list = new ArrayList<Object>();
	list.add(requestWrapper.getParameter("campaignSeq"));
	list.add(requestWrapper.getParameter("officeSeq"));

	if (parentTerritoryKey == null) {
	    // Get ICL_TERRITORY_DEF_SEQ where campaign_seq = ? and
	    // office_seq = ?
	    /**
	     * try to get territory Key from database. If not found then insert
	     */

	    DatabaseResult dbResult = getPlatformService().loadResult(
		    QueryUtils.getQuery().get("SELECT_TERRITORY_DEF_SEQ_FROM_IMS_ICL_TERRITORY_DEF"), list);
	    if (dbResult.getData() != null) {
		for (List<MapObject> row : dbResult.getData()) {
		    leadTeritoryDefKey = Long.parseLong((row.get(0).getValue()));
		    break;
		}
	    }

	    if (leadTeritoryDefKey == 0) {
		leadTeritoryDefKey = getPlatformService().saveAndGetKey(QueryUtils.getQuery().get("INSERT_IMS_ICL_TERRITORY_DEF"),
			list.toArray(new Object[0]));
	    }
	} else {
	    leadTeritoryDefKey = Long.parseLong(parentTerritoryKey);
	}
	DatabaseResult dbResult = null;
	if (!StringUtils.isBlank(territoryType) && territoryType.equalsIgnoreCase("DA")) {
	    List<Object> queryList1 = new ArrayList<Object>();
	    queryList1.add(territoryName);
	    queryList1.add(territoryType);
	    queryList1.add(territory.getGroup());
	    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TERRITORY_KEY_FROM_IMS_TERRITORY_COND"),
		    queryList1);

	} else {
	    List<Object> queryList = new ArrayList<Object>();
	    queryList.add(territoryName);
	    queryList.add(territoryType);
	    queryList.add(territory.getStateName());
	    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TERRITORY_KEY_FROM_IMS_TERRITORY"), queryList);
	}

	if (dbResult.getData() != null) {
	    for (List<MapObject> row : dbResult.getData()) {
		territoryKey = Long.parseLong((row.get(0).getValue()));
		break;
	    }
	}

	List<Object> queryParamList = new ArrayList<Object>();
	queryParamList.add(leadTeritoryDefKey);
	queryParamList.add(territoryKey);

	DatabaseResult dbResultTeritoryMapList = getPlatformService().loadResult(
		QueryUtils.getQuery().get("CHECK_TERITORYKEY_ICL_TERRITORY_MAP"), queryParamList);
	if (territoryKey != -1) {
	    if (dbResultTeritoryMapList != null && dbResultTeritoryMapList.getData().size() == 0) {
		getPlatformService().update(QueryUtils.getQuery().get("INSERT_IMS_ICL_TERRITORY_MAP"),
			new Object[] { leadTeritoryDefKey, territoryKey });
	    } else {
		GenericUIControl territoryUIControl = new GenericUIControl();
		territoryUIControl.setResponseText("Territory Already Exists.");
		uiContext.addControl(territoryUIControl);
	    }
	}
    }

    /**
     * @param requestWrapper
     * @param da
     * @param territoryType
     * @param parentTerritoryKey
     * @param territoryName
     * @param territory
     */
    private void handleICLLeadSave(DynamicUIContext uiContext, CydcorRequestWrapper requestWrapper, String da, String territoryType,
	    String parentTerritoryKey, String territoryName, Territory territory) {
	long territoryKey = -1;
	long leadTeritoryDefKey = -1;
	List<Object> list = new ArrayList<Object>();
	list.add(requestWrapper.getParameter("campaignSeq"));
	list.add(requestWrapper.getParameter("officeSeq"));

	if (parentTerritoryKey == null) {
	    list.add(territoryName);
	    // Get ICLLEAD_TERRITORY_DEF_SEQ where campaign_seq = ? and
	    // office_seq = ?
	    /**
	     * try to get territory Key from database. If not found then insert
	     */

	    DatabaseResult dbResult = getPlatformService().loadResult(
		    QueryUtils.getQuery().get("SELECT_TERRITORY_DEF_SEQ_FROM_IMS_ICLLEAD_TERRITORY_DEF"), list);

	    if (dbResult.getData() != null) {
		for (List<MapObject> row : dbResult.getData()) {
		    leadTeritoryDefKey = Long.parseLong((row.get(0).getValue()));
		    break;
		}
	    }
	    List<Object> paramsList = new ArrayList<Object>();
	    paramsList.add(requestWrapper.getParameter("campaignSeq"));
	    paramsList.add(requestWrapper.getParameter("officeSeq"));
	    if (leadTeritoryDefKey == -1) {
		leadTeritoryDefKey = getPlatformService().saveAndGetKey(QueryUtils.getQuery().get("INSERT_IMS_ICLLEAD_TERRITORY_DEF"),
			paramsList.toArray(new Object[0]));
	    }

	} else {
	    leadTeritoryDefKey = Long.parseLong(parentTerritoryKey);
	}

	DatabaseResult dbResult = null;
	if (!StringUtils.isBlank(territoryType) && territoryType.equalsIgnoreCase("DA")) {
	    List<Object> queryList1 = new ArrayList<Object>();
	    queryList1.add(territoryName);
	    queryList1.add(territoryType);
	    queryList1.add(territory.getGroup());
	    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TERRITORY_KEY_FROM_IMS_TERRITORY_COND"),
		    queryList1);

	} else {
	    List<Object> queryList = new ArrayList<Object>();
	    queryList.add(territoryName);
	    queryList.add(territoryType);
	    queryList.add(territory.getStateName());
	    dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TERRITORY_KEY_FROM_IMS_TERRITORY"), queryList);
	}

	if (dbResult.getData() != null) {
	    for (List<MapObject> row : dbResult.getData()) {
		territoryKey = Long.parseLong((row.get(0).getValue()));
		break;
	    }
	}

	List<Object> queryParamList = new ArrayList<Object>();
	queryParamList.add(leadTeritoryDefKey);
	queryParamList.add(territoryKey);

	DatabaseResult dbResultTeritoryMapList = getPlatformService().loadResult(
		QueryUtils.getQuery().get("CHECK_TERITORYKEY_ICLLEAD_TERRITORY_MAP"), queryParamList);
	if (territoryKey != -1) {
	    if (dbResultTeritoryMapList != null && dbResultTeritoryMapList.getData().size() == 0) {
		getPlatformService().update(QueryUtils.getQuery().get("INSERT_IMS_ICLLEAD_TERRITORY_MAP"),
			new Object[] { leadTeritoryDefKey, territoryKey });
	    } else {
		GenericUIControl territoryUIControl = new GenericUIControl();
		territoryUIControl.setResponseText("Territory Already Exists.");
		uiContext.addControl(territoryUIControl);
	    }
	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cydcor.framework.controller.Controller#getResult(java.lang.Object,
     * java.util.Map)
     */
    public Object getResult(Object imput, Map<String, Object> paramMap) {
	// TODO Auto-generated method stub
	return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cydcor.framework.controller.Controller#init(java.lang.Object)
     */
    public void init(Object params) {
	// TODO Auto-generated method stub

    }

}
