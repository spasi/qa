package com.cydcor.framework.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONSerializer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.process.ProcessContext;
import com.cydcor.framework.process.ProcessExecutor;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

@Controller("LeadSheetGenerationController")
@Scope("singleton")
public class LeadSheetGenerationController extends GenericController {
	private static final transient Log logger = LogFactory.getLog(LeadSheetGenerationController.class);

	public void execute(DynamicUIContext uiContext) {
		
		logger.debug("Came inside default LeadSheetGenerationController..");

		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

		String campaignSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter(
				"campaignSeq");

		String officeSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper()
				.getParameter("officeSeq");
		String pageDefinition = "leadsheet/LeadSheetGenerationDef.html";
		String controlDefinition = "leadsheet/LeadSheetGeneration.html";
		if (StringUtils.isBlank(uiContext.getControlName())
				|| (requestWrapper.getParameter("loadPage") != null && requestWrapper.getParameter("loadPage").equals(
						"true"))) {
			clearOldPage(uiContext, pageDefinition);

			// Add Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefinition);
			campaignSelectorControl.addParameter("campaignSelector", true);

			uiContext.addControl(campaignSelectorControl);

			// Add Control the ICL Selector
			GenericUIControl iclSelectorControl = new GenericUIControl();
			iclSelectorControl.setTemplateName(pageDefinition);
			iclSelectorControl.addParameter("iclSelector", true);

			uiContext.addControl(iclSelectorControl);

			// Add templateSelector Control
			GenericUIControl templateSelectorControl = new GenericUIControl();
			templateSelectorControl.setTemplateName(pageDefinition);
			templateSelectorControl.addParameter("templateSelector", true);

			uiContext.addControl(templateSelectorControl);

			// Add Map Control Noe
			GenericUIControl mapControl = new GenericUIControl();
			mapControl.setTemplateName(pageDefinition);
			mapControl.addParameter("showCampaignMap", true);
			uiContext.addControl(mapControl);

			// Add Default View Selection Details Control
			GenericUIControl defaultViewDetailsControl = new GenericUIControl();
			defaultViewDetailsControl.setTemplateName(pageDefinition);
			defaultViewDetailsControl.addParameter("defaultViewDetails", true);
			uiContext.addControl(defaultViewDetailsControl);

			// Add SelectedTemplateDetails Control
			GenericUIControl templateDetailsControl = new GenericUIControl();
			templateDetailsControl.setTemplateName(pageDefinition);
			templateDetailsControl.addParameter("selectedTemplateDetails", true);
			uiContext.addControl(templateDetailsControl);

			// Add SelectedTemplateDetails Control
			GenericUIControl availableLeadsDataControl = new GenericUIControl();
			availableLeadsDataControl.setTemplateName(pageDefinition);
			availableLeadsDataControl.addParameter("availableLeadsData", true);
			availableLeadsDataControl.addParameter("tableName", "availableLeadsData");
			uiContext.addControl(availableLeadsDataControl);

			// Add Control the LeadSheet Generated Table
			GenericUIControl leadSheetGeneratedtableControl = new GenericUIControl();
			leadSheetGeneratedtableControl.setTemplateName(pageDefinition);
			leadSheetGeneratedtableControl.addParameter("leadSheetGeneratedtable", true);
			leadSheetGeneratedtableControl.addParameter("tableName", "leadSheetGeneratedtable");
			uiContext.addControl(leadSheetGeneratedtableControl);

			// Add Control the LeadSheet Generated Table
			GenericUIControl leadSheetSavedtableControl = new GenericUIControl();
			leadSheetSavedtableControl.setTemplateName(pageDefinition);
			leadSheetSavedtableControl.addParameter("leadSheetSavedtable", true);
			leadSheetSavedtableControl.addParameter("tableName", "leadSheetSavedtable");
			uiContext.addControl(leadSheetSavedtableControl);

			// Add Color Codes Control
			GenericUIControl colorCodesControl = new GenericUIControl();
			colorCodesControl.setTemplateName(pageDefinition);
			colorCodesControl.addParameter("colorCodes", true);
			uiContext.addControl(colorCodesControl);

		} else {
			String queryString = null;

			if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
				DatabaseResult dbResult = null;
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefinition);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(campaignSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclSelector")) {
				// System.out.println("campaignSeq for icl selector: " +
				// requestWrapper.getParameter("campaignSeq"));
				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.toArray(new Object[0]);

				DatabaseResult dbResult = null;
				if (!requestWrapper.getParameter("campaignSeq").equalsIgnoreCase("-1"))
					dbResult = getPlatformService().loadResult(
							QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

				// Add Second Control the Campaign Selector
				GenericUIControl iclSelectorControl = new GenericUIControl();
				iclSelectorControl.setTemplateName(controlDefinition);
				iclSelectorControl.addParameter("iclSelector", true);
				// iclSelectorControl.addParameter("templateSelector", true);
				iclSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(iclSelectorControl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "templateSelector")) {

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				// officeSeq
				list.add(requestWrapper.getParameter("officeSeq"));
				list.toArray(new Object[0]);
				// System.out.println("campaignSeq for templateSelector : " +
				// requestWrapper.getParameter("campaignSeq"));
				// System.out.println("officeSeq for templateSelector: " +
				// requestWrapper.getParameter("officeSeq"));
				DatabaseResult dbResult = null;
				if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
					dbResult = getPlatformService().loadResult(
							QueryUtils.getQuery().get("SELECT_TEMPLATE_WITH_DEFAULT_NAMES"), list);

				// Add Second Control the Template Selector
				GenericUIControl templateSelectorControl = new GenericUIControl();
				templateSelectorControl.setTemplateName(controlDefinition);
				// templateSelectorControl.addParameter("campaignSeq",
				// campaignSeq);
				templateSelectorControl.addParameter("templateSelector", true);
				templateSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(templateSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "selectedTemplateDetails")) {
				DatabaseResult dbResult = null;
				// System.out.println("campaignSeq for selectedTemplateDetails : "
				// + requestWrapper.getParameter("campaignSeq"));
				// System.out.println("officeSeq for selectedTemplateDetails: "
				// + requestWrapper.getParameter("officeSeq"));
				// System.out.println("templateName for selectedTemplateDetails: "
				// + requestWrapper.getParameter("templateName"));

				if (StringUtils.equalsIgnoreCase("SYSTEM_DEFAULT", requestWrapper.getParameter("templateName"))) {

					// Load Template Params for this Campaign and Office
					if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
						dbResult = getPlatformService().loadSystemDefaultTemplateParams(
								Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
								Integer.parseInt(requestWrapper.getParameter("officeSeq")),
								requestWrapper.getParameter("templateName"));
				} else {

					// Load Template Params for this Campaign and Office
					if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
						dbResult = getPlatformService().loadTemplateParams(
								Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
								Integer.parseInt(requestWrapper.getParameter("officeSeq")),
								requestWrapper.getParameter("templateName"));
				}

				GenericUIControl selectedTemplateDetailsControl = new GenericUIControl();
				selectedTemplateDetailsControl.setTemplateName(controlDefinition);
				selectedTemplateDetailsControl.addParameter("selectedTemplateDetails", true);
				selectedTemplateDetailsControl.addParameter("result", dbResult);
				selectedTemplateDetailsControl
						.addParameter("templateName", requestWrapper.getParameter("templateName"));
				if (dbResult != null)
					for (List row : dbResult.getData()) {
						MapObject paramNameColumn = (MapObject) row.get(0);
						MapObject paramValueColumn = (MapObject) row.get(1);
						selectedTemplateDetailsControl.addParameter(paramNameColumn.getValue(), paramValueColumn
								.getValue());
					}
				uiContext.addControl(selectedTemplateDetailsControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "availableLeadsData")) {
				List listLeads = new ArrayList();
				listLeads.add(new Integer(requestWrapper.getParameter("campaignSeq")));
				listLeads.add(new Integer(requestWrapper.getParameter("officeSeq")));
				if (StringUtils.equalsIgnoreCase(requestWrapper.getParameter("loadAvailableLeadsDataTable"), "true")) {
					if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
						TableUIControl availableLeadsDataTableControl = new TableUIControl();
						availableLeadsDataTableControl.setFilterParamList(listLeads);
						availableLeadsDataTableControl.setTableName(uiContext.getControlName());
						availableLeadsDataTableControl.setQueryKeyName("SELECT_AVAILABLE_LEADS_LIST");
						availableLeadsDataTableControl.setColumnName("LEAD_ASSIGNED DESC ");
						availableLeadsDataTableControl.setJavaScriptMethodName("showLeadsByUploadFile");
						availableLeadsDataTableControl.setTemplateName("../" + controlDefinition);
						availableLeadsDataTableControl.setRowsPerPage(5);
						availableLeadsDataTableControl.addParameter("availableLeadsDataTable", true);

						uiContext.addControl(availableLeadsDataTableControl);
					}
				} else {
					String sql = null;
					DatabaseResult dbLeadResult = null;

					GenericUIControl availableLeadsDataControl = new GenericUIControl();
					availableLeadsDataControl.setTemplateName(controlDefinition);
					availableLeadsDataControl.addParameter("availableLeadsData", true);
					if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
						sql = QueryUtils.getQuery().get("COUNT_OF_AVAILABLE_LEADS");
						if (StringUtils.isNotBlank(requestWrapper.getParameter("selectedFiles"))) {
							sql = sql + " AND RUN_ID IN (" + requestWrapper.getParameter("selectedFiles") + ")";
						}
						logger.debug("selectedFiles for File filtering: "
								+ requestWrapper.getParameter("selectedFiles"));
						logger.debug("Available lead count query: " + sql);
						dbLeadResult = getPlatformService().loadResult(sql, listLeads);
						for (List row : dbLeadResult.getData()) {
							MapObject paramValueColumn = (MapObject) row.get(0);
							availableLeadsDataControl.addParameter("NumberOfLeads", paramValueColumn.getValue());
						}
					} else
						availableLeadsDataControl.addParameter("NumberOfLeads", 0);
					if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
						sql = QueryUtils.getQuery().get("COUNT_OF_UNMAPPED_LEADS");
						if (StringUtils.isNotBlank(requestWrapper.getParameter("selectedFiles"))) {
							sql = sql + " AND RUN_ID IN (" + requestWrapper.getParameter("selectedFiles") + ")";
						}
						logger.debug("selectedFiles for File filtering: "
								+ requestWrapper.getParameter("selectedFiles"));
						logger.debug("Available lead count query: " + sql);
						dbLeadResult = getPlatformService().loadResult(sql, listLeads);
						for (List row : dbLeadResult.getData()) {
							MapObject paramValueColumn = (MapObject) row.get(0);
							availableLeadsDataControl
									.addParameter("NumberOfUnMappedLeads", paramValueColumn.getValue());
							break;
						}
					} else
						availableLeadsDataControl.addParameter("NumberOfUnMappedLeads", 0);
					uiContext.addControl(availableLeadsDataControl);
				}
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "leadSheetGeneratedtable")) {
				if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
					// Sample Implementation for Genric Table Control
					List list = new ArrayList();
					list.add(new Integer(requestWrapper.getParameter("officeSeq")));
					list.add(new Integer(requestWrapper.getParameter("campaignSeq")));
					String leadSheetID = requestWrapper.getParameter("leadSheetID");
					if (StringUtils.isBlank(leadSheetID))
						leadSheetID = "%%";
					list.add(leadSheetID.trim());

					// System.out.println("campaignSeq for leadSheetGeneratedtable: "
					// + requestWrapper.getParameter("campaignSeq"));
					// System.out.println("officeSeq for leadSheetGeneratedtable: "
					// + requestWrapper.getParameter("officeSeq"));

					TableUIControl leadSheetGeneratedtableControl = new TableUIControl();
					leadSheetGeneratedtableControl.setFilterParamList(list);
					leadSheetGeneratedtableControl.setQueryKeyName("SELECT_ALL_TEMP_LEADSHEETS");
					leadSheetGeneratedtableControl.setColumnName("GENERATED_DATE DESC");
					leadSheetGeneratedtableControl.setTableName(uiContext.getControlName());
					leadSheetGeneratedtableControl.setJavaScriptMethodName("loadLeadSheetGenerated");
					leadSheetGeneratedtableControl.addParameter("leadSheetGeneratedtable", true);
					leadSheetGeneratedtableControl.setTemplateName("../" + controlDefinition);
					leadSheetGeneratedtableControl.setRowsPerPage(10);

					uiContext.addControl(leadSheetGeneratedtableControl);
				}
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "leadSheetSavedtable")) {
				if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1")) {
					List repInputs = new ArrayList();
					repInputs.add(new Integer(requestWrapper.getParameter("officeSeq")));
					DatabaseResult repResult = null;
					if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
						repResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_MERGED_OFFICES"),
								repInputs);
					String sql = QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
					if (repResult != null)
						for (List<MapObject> objects : repResult.getData()) {
							sql = sql + " UNION ALL " + QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
							repInputs.add(new Integer(objects.get(0).getValue()));
						}

					sql = "SELECT * FROM ( " + sql + " ) AS TEMP ORDER BY REP_NAME";
					if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
						repResult = getPlatformService().loadResult(sql, repInputs);

					// System.out.println("campaignSeq for leadSheetGeneratedtable: "
					// + requestWrapper.getParameter("campaignSeq"));
					// System.out.println("officeSeq for leadSheetGeneratedtable: "
					// + requestWrapper.getParameter("officeSeq"));
					List list = new ArrayList();
					// for 1st query
					list.add(new Integer(requestWrapper.getParameter("officeSeq")));
					list.add(new Integer(requestWrapper.getParameter("campaignSeq")));

					TableUIControl leadSheetSavedControl = new TableUIControl();
					String leadSheetID = requestWrapper.getParameter("leadSheetID");

					if (StringUtils.isBlank(leadSheetID))
						leadSheetID = "%%";

					leadSheetSavedControl.setQueryKeyName("SELECT_ALL_LEADSHEETS_BY_LEADSHEET_ID");
					list.add(leadSheetID.trim());

					leadSheetSavedControl.setFilterParamList(list);
					// leadSheetSavedControl.setQueryKeyName("SELECT_ALL_LEADSHEETS");
					leadSheetSavedControl.setColumnName("RED_LEAD_FLAG DESC, CREATED_DATE DESC");
					leadSheetSavedControl.setTableName(uiContext.getControlName());
					leadSheetSavedControl.setJavaScriptMethodName("loadLeadSheetSaved");
					leadSheetSavedControl.addParameter("leadSheetSavedtable", true);
					leadSheetSavedControl.addParameter("repResult", repResult);
					leadSheetSavedControl.setTemplateName("../" + controlDefinition);
					leadSheetSavedControl.setRowsPerPage(10);
					uiContext.addControl(leadSheetSavedControl);
				}
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "generateLeadSheet")) {

				ProcessContext processContext = new ProcessContext();
				processContext.setProcessTaskName("LeadSheetGeneration.bsh");
				Enumeration<String> paramNames = requestWrapper.getParameterNames();
				while (paramNames.hasMoreElements()) {
					String param = paramNames.nextElement();
					processContext.addParameter(param, requestWrapper.getParameter(param));
				}

				DatabaseResult templateParamsResult = null;

				if (StringUtils.equalsIgnoreCase("SYSTEM_DEFAULT", requestWrapper.getParameter("templateName"))) {

					// Load Template Params for this Campaign and Office
					if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
						templateParamsResult = getPlatformService().loadSystemDefaultTemplateParams(
								Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
								Integer.parseInt(requestWrapper.getParameter("officeSeq")),
								requestWrapper.getParameter("templateName"));
				} else {

					// Load Template Params for this Campaign and Office
					if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
						templateParamsResult = getPlatformService().loadTemplateParams(
								Integer.parseInt(requestWrapper.getParameter("campaignSeq")),
								Integer.parseInt(requestWrapper.getParameter("officeSeq")),
								requestWrapper.getParameter("templateName"));
				}

				Map<String, String> templateParamMap = new HashMap<String, String>();
				if (templateParamsResult.getData().size() > 0) {
					for (List<MapObject> row : templateParamsResult.getData()) {
						try {
							processContext.addParameter(row.get(0).getValue(), row.get(1).getValue());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				JSONArray unAssignedLeads = JSONArray.fromObject(JSONSerializer.toJSON(processContext.getParameters()
						.get("leads")));
				for (int i = 0; i < unAssignedLeads.size(); i++) {
					if (unAssignedLeads.get(i) instanceof JSONNull) {
						unAssignedLeads.remove(i);
						i--;
					}
				}
				processContext.addParameter("unAssignedLeads", unAssignedLeads);
				String selectedFiles = null;
				if (StringUtils.isNotBlank(requestWrapper.getParameter("selectedUploadedFiles"))) {
					selectedFiles = requestWrapper.getParameter("selectedUploadedFiles");
				}
				processContext.addParameter("selectedFiles", selectedFiles);

				ProcessExecutor.executeAssignmentProcess(processContext);

				GenericUIControl control = new GenericUIControl();

				/*
				 * List<TempLeadSheet> finalLeadSheets = (List) processContext
				 * .getParameters().get("generatedLeadSheetIds"); String
				 * response = ""; String leadSheetIds = "";
				 * 
				 * for (TempLeadSheet tempLeadSheet : finalLeadSheets) {
				 * leadSheetIds = leadSheetIds + ",'" +
				 * tempLeadSheet.getLeadSheetId() + "'"; }
				 * 
				 * if (StringUtils.isNotBlank(leadSheetIds)) leadSheetIds =
				 * leadSheetIds.substring(1); if
				 * ("NONE".equalsIgnoreCase(requestWrapper
				 * .getParameter("templateName"))) { response =
				 * CydcorUtils.getKMLforLeadSheets(leadSheetIds);
				 * 
				 * } else { response = "ALL-GENERATED"; } finalLeadSheets =
				 * null;
				 */
				System.gc();
				control.setResponseText("");
				uiContext.addControl(control);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveLeadSheets")) {
				GenericUIControl mergeCtrl = new GenericUIControl();
				try {
					String result = getPlatformService().saveOrMergeLeadsheets(
							requestWrapper.getParameter("leadSheetIds"), requestWrapper.getParameter("savedLeadSheet"),
							true, false, campaignSeq, officeSeq);
					mergeCtrl.setResponseText(result);
				} catch (Exception e) {
					mergeCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(mergeCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "mergeGeneratedAndSavedLeadSheets")) {
				List values = new ArrayList();
				values.add(requestWrapper.getParameter("savedLeadSheet"));
				ProcessContext processContext = new ProcessContext();
				DatabaseResult databaseResult = getPlatformService().loadResult(
						"SELECT PERSON_ID FROM IMS.IMS_LEADSHEET WHERE LEADSHEET_ID = ?", values);
				String assognedRep = databaseResult.getData().get(0).get(0).getValue();
				boolean isAssigned = false;
				GenericUIControl mergeCtrl = new GenericUIControl();
				if (StringUtils.isNotBlank(assognedRep)) {
					isAssigned = true;
				} else {
					isAssigned = false;
				}
				try {
					String result = getPlatformService().saveOrMergeLeadsheets(
							requestWrapper.getParameter("leadSheetIds").replaceAll("'", ""),
							requestWrapper.getParameter("savedLeadSheet"), false, isAssigned, campaignSeq, officeSeq);
					mergeCtrl.setResponseText(result);
				} catch (Exception e) {
					mergeCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(mergeCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "deleteAllLeadSheets")) {
				GenericUIControl control = new GenericUIControl();
				// To Delete all Temp Leadsheets
				getPlatformService().update(
						"DELETE FROM IMS.IMS_TEMP_LEADSHEET WHERE CAMPAIGN_SEQ =" + campaignSeq + " AND OFFICE_SEQ="
								+ officeSeq, new Object[] {});
				control.setResponseText("ALL-DELETED");
				uiContext.addControl(control);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "AssignReps")) {
				ProcessContext processContext = new ProcessContext();
				processContext.setProcessTaskName("AssignReps.bsh");
				String leadSheetReps = requestWrapper.getParameter("leadSheetReps");
				String[] pairs = leadSheetReps.split(",");

				Enumeration<String> paramNames = requestWrapper.getParameterNames();
				while (paramNames.hasMoreElements()) {
					String param = paramNames.nextElement();
					processContext.addParameter(param, requestWrapper.getParameter(param));
				}
				processContext.addParameter("leadSheetReps", pairs);
				ProcessExecutor.executeAssignmentProcess(processContext);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "deleteTempLeadSheet")) {
				String leadSheetId = requestWrapper.getParameter("leadSheetId");
				GenericUIControl control = new GenericUIControl();
				control.setResponseText(CydcorUtils.getKMLforLeadSheets("'" + leadSheetId + "'"));
				uiContext.addControl(control);
				getPlatformService().update(QueryUtils.getQuery().get("DELETE_TEMP_LEAD_SHEET"),
						new Object[] { leadSheetId });

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "saveTempLeadSheet")) {
				GenericUIControl mergeCtrl = new GenericUIControl();
				try {
					String result = getPlatformService().saveOrMergeLeadsheets(
							requestWrapper.getParameter("leadSheetIds").replaceAll("'", ""),
							requestWrapper.getParameter("savedLeadSheet"), false, false, campaignSeq, officeSeq);
					mergeCtrl.setResponseText(result);
				} catch (Exception e) {
					mergeCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(mergeCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "deleteSavedLeadSheet")) {
				String leadSheetId = requestWrapper.getParameter("leadSheetId");
				GenericUIControl control = new GenericUIControl();
				/*
				 * control.setResponseText(CydcorUtils.getKMLforLeadSheets("'" +
				 * leadSheetId + "'"));
				 */
				control.setResponseText("deleted");
				uiContext.addControl(control);

				/*
				 * getPlatformService().update(
				 * QueryUtils.getQuery().get("DELETE_SAVED_LEAD_SHEET"), new
				 * Object[] { leadSheetId });
				 */

				// DELETE FROM IMS.IMS_LEADSHEET WHERE LEADSHEET_ID=?
				getPlatformService().update(
						"DELETE FROM IMS.IMS_LEADSHEET WHERE LEADSHEET_ID IN (" + leadSheetId + ")", new Object[] {});

				/*
				 * getPlatformService().update( QueryUtils.getQuery().get(
				 * "UPDATE_LEAD_SHEET_ID_LEAD_LIFECYCLE"), new Object[] {
				 * leadSheetId });
				 */

				// UPDATE IMS.IMS_LEAD_LIFECYCLE SET PERSON_ID=NULL
				// ,LEAD_SHEET_ID=NULL WHERE LEAD_SHEET_ID=?
				getPlatformService().update(
						"UPDATE IMS.IMS_LEAD_LIFECYCLE SET PERSON_ID=NULL ,LEAD_SHEET_ID=NULL WHERE LEAD_SHEET_ID IN ("
								+ leadSheetId + ")", new Object[] {});

			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "defaultViewDetails")) {

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.add(requestWrapper.getParameter("officeSeq"));
				list.toArray(new Object[0]);
				DatabaseResult dbResult = null;
				if (!requestWrapper.getParameter("officeSeq").equalsIgnoreCase("-1"))
					dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_TERRITORY_VIEW"),
							list);

				GenericUIControl defaultViewDetailsControl = new GenericUIControl();
				defaultViewDetailsControl.setTemplateName(controlDefinition);
				defaultViewDetailsControl.addParameter("defaultViewDetails", true);
				defaultViewDetailsControl.addParameter("result", dbResult);

				/*
				 * List listLeads = new ArrayList(); listLeads.add(new
				 * Integer(requestWrapper .getParameter("campaignSeq")));
				 * listLeads.add(new Integer(requestWrapper
				 * .getParameter("officeSeq")));
				 * 
				 * DatabaseResult dbLeadResult =
				 * getPlatformService().loadResult(
				 * QueryUtils.getQuery().get("COUNT_OF_AVAILABLE_LEADS"),
				 * listLeads); for (List row : dbLeadResult.getData()) {
				 * MapObject paramValueColumn = (MapObject) row.get(0);
				 * defaultViewDetailsControl.addParameter("NumberOfLeads",
				 * paramValueColumn.getValue()); }
				 */

				uiContext.addControl(defaultViewDetailsControl);

			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "mergeLeadSheets")) {
				String selectedSheets = requestWrapper.getParameter("selectedLeadSheets");
				logger.info("selectedLeadSheets :" + selectedSheets);
				String firstSheetId = selectedSheets.split(",")[0].split("'")[1];
				DatabaseResult result = getPlatformService().loadResult(
						"SELECT LEADSHEET_NAME FROM IMS.IMS_TEMP_LEADSHEET WHERE LEADSHEET_ID ='" + firstSheetId + "'");
				String firstSheetName = result.getData().get(0).get(0).getValue();
				getPlatformService().update(
						"UPDATE IMS.IMS_TEMP_LEADSHEET SET LEADSHEET_ID=?, LEADSHEET_NAME=?, CREATED_DATE=GETDATE() WHERE LEADSHEET_ID IN ("
								+ selectedSheets + ")", new Object[] { firstSheetId, firstSheetName });
			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "deleteSelectedTempLeadSheets")) {
				String selectedSheets = requestWrapper.getParameter("selectedLeadSheets");

				String uniqueId = new Long(System.nanoTime()).toString();
				String wireCenter = selectedSheets.split(",")[0].split("'")[1].split("-")[0];
				String newLeadSheetId = wireCenter + "-CUSTOM-" + uniqueId;
				String newLeadSheetName = wireCenter + "-CUSTOM-" + uniqueId.substring(11);

				getPlatformService().update(
						"DELETE FROM IMS.IMS_TEMP_LEADSHEET WHERE LEADSHEET_ID IN (" + selectedSheets + ")",
						new Object[] {});
			} else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "colorCodes")) {
				DatabaseResult dbResult = null;
				List inputs = new ArrayList();
				inputs.add(CydcorContext.getInstance().getRequestContext().getRequestWrapper()
						.getParameter("clientKey"));
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_DISPOSITION_LIST"),
						inputs);

				List<MapObject> defaultDisp = new ArrayList<MapObject>();
				MapObject object = new MapObject();
				object.setValue("Default");
				defaultDisp.add(object);
				object = new MapObject();
				object.setValue("Not Dispositioned");
				defaultDisp.add(object);
				object = new MapObject();
				object.setValue("assets/images/mapicons/red-dot.png");
				defaultDisp.add(object);
				dbResult.getData().add(0, defaultDisp);
				List returnList = new ArrayList();
				int size = (dbResult.getData().size() / 3) + (dbResult.getData().size() % 3 > 0 ? 1 : 0);
				boolean hasReminder = dbResult.getData().size() % 3 > 0 ? true : false;
				int j = 1;
				for (int i = 1; i <= size; i++) {
					int startIdx = j - 1;
					int lastIdx = i * 3;
					if (i == size && hasReminder)
						lastIdx = dbResult.getData().size();
					returnList.add(dbResult.getData().subList(startIdx, lastIdx));
					j = j + 3;
				}
				GenericUIControl colorCodesControl = new GenericUIControl();
				colorCodesControl.setTemplateName(controlDefinition);
				colorCodesControl.addParameter("colorCodes", true);
				colorCodesControl.addParameter("dispositionResult", returnList);
				uiContext.addControl(colorCodesControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "renameLeadSheet")) {
				GenericUIControl updateNameCtrl = new GenericUIControl();

				try {
					getPlatformService().update(
							"UPDATE IMS.IMS_TEMP_LEADSHEET SET LEADSHEET_NAME =? WHERE LEADSHEET_ID = ?",
							new Object[] { requestWrapper.getParameter("leadSheetName"),
									requestWrapper.getParameter("leadSheetId") });
					updateNameCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
					logger.error(e);
					updateNameCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateNameCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "checkExpiredLeadSheets")) {
				GenericUIControl expiryCheckCtrl = new GenericUIControl();
				String leadSheets = requestWrapper.getParameter("selectedLeadSheets");
				DatabaseResult result = getPlatformService()
						.loadResult(
								"SELECT LEADSHEET_ID FROM IMS.IMS_LEADSHEET WHERE  (GETDATE()-CREATED_DATE < 60  OR CAMPAIGN_SEQ = '29958807') AND LEADSHEET_ID in ("
										+ leadSheets + ")");
				if (result.getData().size() < leadSheets.split(",").length) {
					String validSheets = "";
					for (List<MapObject> objects : result.getData()) {
						for (MapObject object : objects) {
							validSheets = validSheets + "," + object.getValue();
						}
					}
					if (!validSheets.equals("")) {
						validSheets = validSheets.substring(1);
					}
					expiryCheckCtrl.setResponseText("ERROR:" + result.getData().size() + ":" + validSheets);
				} else {
					expiryCheckCtrl.setResponseText("SUCCESS:" + leadSheets.replaceAll("'", ""));
				}
				uiContext.addControl(expiryCheckCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeRep")) {

				String repId = requestWrapper.getParameter("repId");
				String assgnDate = requestWrapper.getParameter("assignedDate");
				String sheetId = requestWrapper.getParameter("leadSheetId");
				if (StringUtils.equalsIgnoreCase(repId, "-1")) {
					repId = null;
					assgnDate = null;
				}
				GenericUIControl updateRepCtrl = new GenericUIControl();

				try {
					getPlatformService().update(QueryUtils.getQuery().get("ASSIGN_LEADSHEET_REP"),
							new Object[] { repId, assgnDate, sheetId });
					getPlatformService().update(QueryUtils.getQuery().get("ASSIGN_LEADS_REP"),
							new Object[] { repId, sheetId });

					updateRepCtrl.setResponseText("SUCCESS:" + StringUtils.isNotBlank(repId));
				} catch (Exception e) {
					logger.error(e);
					updateRepCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateRepCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updateReturnDate")) {

				String returnDate = requestWrapper.getParameter("returnDate");
				String sheetId = requestWrapper.getParameter("leadSheetId");

				GenericUIControl updateRepCtrl = new GenericUIControl();
				try {
					getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_RETURNDATE"),
							new Object[] { returnDate, sheetId });
					updateRepCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
					logger.error(e);
					updateRepCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateRepCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeNotes")) {

				String notes = requestWrapper.getParameter("notes").trim();
				String sheetId = requestWrapper.getParameter("leadSheetId");

				GenericUIControl updateRepCtrl = new GenericUIControl();
				try {
					getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_NOTES"),
							new Object[] { notes, sheetId });
					updateRepCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
					logger.error(e);
					updateRepCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateRepCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeSales")) {
				String sales = requestWrapper.getParameter("sales").trim();
				String sheetId = requestWrapper.getParameter("leadSheetId");

				GenericUIControl updateRepCtrl = new GenericUIControl();
				try {
					getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_SALES"),
							new Object[] { sales, sheetId });
					updateRepCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
					logger.error(e);
					updateRepCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateRepCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeDecisionMaker")) {

				String dm = requestWrapper.getParameter("decisionMaker").trim();
				String sheetId = requestWrapper.getParameter("leadSheetId");

				GenericUIControl updateRepCtrl = new GenericUIControl();
				try {
					getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_DM"),
							new Object[] { dm, sheetId });
					updateRepCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
					logger.error(e);
					updateRepCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateRepCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updateRedLeadFlag")) {
				String leadsheets = "'" + requestWrapper.getParameter("leadSheetId") + "'";
				if (StringUtils.isNotBlank(requestWrapper.getParameter("leadSheets"))) {
					leadsheets = "'" + requestWrapper.getParameter("leadSheets") + "'";
					leadsheets = leadsheets.replaceAll(",", "', '");
				}
				getPlatformService().update(
						"UPDATE IMS.IMS_LEADSHEET SET RED_LEAD_FLAG=? WHERE LEADSHEET_ID IN (" + leadsheets
								+ ")  AND RED_LEAD_FLAG =?", new Object[] { "N", "Y" });

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "changeDoorsKnocked")) {

				String dm = requestWrapper.getParameter("doorsKnocked").trim();
				String sheetId = requestWrapper.getParameter("leadSheetId");

				GenericUIControl updateRepCtrl = new GenericUIControl();
				try {
					getPlatformService().update(QueryUtils.getQuery().get("UPDATE_SHEET_DK"),
							new Object[] { dm, sheetId });
					updateRepCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
					logger.error(e);
					updateRepCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateRepCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "getModalWindow")) {

				GenericUIControl modalWindowCtrl = new GenericUIControl();
				modalWindowCtrl.addParameter("modalWindow", true);
				modalWindowCtrl.setTemplateName(controlDefinition);
				try {
					List repInputs = new ArrayList();
					repInputs.add(new Integer(requestWrapper.getParameter("officeSeq")));
					DatabaseResult repResult;
					repResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_MERGED_OFFICES"),
							repInputs);
					String sql = QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
					for (List<MapObject> objects : repResult.getData()) {
						sql = sql + " UNION ALL " + QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
						repInputs.add(new Integer(objects.get(0).getValue()));
					}

					sql = "SELECT * FROM ( " + sql + " ) AS TEMP ORDER BY REP_NAME";
					repResult = getPlatformService().loadResult(sql, repInputs);

					modalWindowCtrl.addParameter("repResult", repResult);
				} catch (Exception e) {
					logger.error(e);
				}
				uiContext.addControl(modalWindowCtrl);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "updatePrinted")) {

				GenericUIControl updatePrintedCtrl = new GenericUIControl();
				List<String> values = new ArrayList<String>();
				for(String sheet : requestWrapper.getParameter("leadSheets").split(",")){
				    values.add(sheet);
				}
				try {
				    getPlatformService().updateLeadsheetPrintStatus(values);
				    updatePrintedCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
				    updatePrintedCtrl.setResponseText("ERROR");
				    logger.error(e);
				}
				uiContext.addControl(updatePrintedCtrl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "editSavedLeadSheet")) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				String repId = requestWrapper.getParameter("repAssigned").trim();
				String assnDate = requestWrapper.getParameter("repAssignedDate").trim();
				boolean alreadyAssigned = true;

				String status = "INP";
				if (StringUtils.equalsIgnoreCase(repId, "-1") || StringUtils.equalsIgnoreCase(repId, "")) {
					repId = null;
					assnDate = null;
				}
				if (repId != null && assnDate.trim().equalsIgnoreCase("")) {
					assnDate = dateFormat.format(System.currentTimeMillis());
					alreadyAssigned = false;
				}

				String returnedDate = requestWrapper.getParameter("returnedDate").trim();
				if (returnedDate.equalsIgnoreCase("")) {
					returnedDate = null;
				} else {
					status = "X";
				}

				String sales = requestWrapper.getParameter("sales").trim();
				String dm = requestWrapper.getParameter("decisionMaker").trim();
				String dk = requestWrapper.getParameter("doorsKnocked").trim();
				String notes = requestWrapper.getParameter("notes").trim();
				String sheetId = requestWrapper.getParameter("leadSheetId");

				GenericUIControl updateLSCtrl = new GenericUIControl();
				try {
					getPlatformService().update(QueryUtils.getQuery().get("UPDATE_LEADSHEET"),
							new Object[] { repId, assnDate, returnedDate, sales, dm, dk, notes, status, sheetId });
					if (assnDate != null && !alreadyAssigned) {
						getPlatformService().update(QueryUtils.getQuery().get("ASSIGN_LEADS_REP"),
								new Object[] { repId, sheetId });
					}
					updateLSCtrl.setResponseText("SUCCESS");
				} catch (Exception e) {
					logger.error(e);
					updateLSCtrl.setResponseText("ERROR");
				}
				uiContext.addControl(updateLSCtrl);
			}

		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	private void saveAndMergeSheets(CydcorRequestWrapper requestWrapper, ProcessContext processContext) {

		processContext.setProcessTaskName("SaveLeadSheets.bsh");
		Enumeration<String> paramNames = requestWrapper.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			processContext.addParameter(param, requestWrapper.getParameter(param));
		}

		ProcessExecutor.executeAssignmentProcess(processContext);

	}

}
