/**
 * 
 */
package com.cydcor.framework.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cydcor.framework.process.ProcessContext;

/**
 * @author ashwin
 * 
 */
public class TaskContext {

	private Map<String, Object> parameters = new HashMap<String, Object>();
	private List<String> errorLogs = new ArrayList<String>();
	private List<String> executionLogs = new ArrayList<String>();

	private ProcessContext processContext = new ProcessContext();

	/**
	 * @return the processContext
	 */
	public ProcessContext getProcessContext() {
		return processContext;
	}

	/**
	 * @return the errorLogs
	 */
	public List<String> getErrorLogs() {
		return errorLogs;
	}

	/**
	 * @param errorLogs
	 *            the errorLogs to set
	 */
	public void setErrorLogs(List<String> errorLogs) {
		this.errorLogs = errorLogs;
	}

	/**
	 * @param errorMessage
	 */
	public void addErrorLogs(String errorMessage) {
		this.errorLogs.add(errorMessage);
	}

	/**
	 * @return the executionLogs
	 */
	public List<String> getExecutionLogs() {
		return executionLogs;
	}

	/**
	 * @param executionLogs
	 *            the executionLogs to set
	 */
	public void setExecutionLogs(List<String> executionLogs) {
		this.executionLogs = executionLogs;
	}

	/**
	 * @param processContext
	 *            the processContext to set
	 */
	public void setProcessContext(ProcessContext processContext) {
		this.processContext = processContext;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

	public void addParameter(String key, Object value) {
		this.parameters.put(key, value);
	}

}
