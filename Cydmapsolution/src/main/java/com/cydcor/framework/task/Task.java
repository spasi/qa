/**
 * 
 */
package com.cydcor.framework.task;

/**
 * @author ashwin
 * 
 */
public interface Task {

	public void execute(TaskContext context);
}
