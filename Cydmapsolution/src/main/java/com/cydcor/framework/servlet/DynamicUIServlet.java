package com.cydcor.framework.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.click.util.HtmlStringBuffer;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.Controller;
import com.cydcor.framework.controller.ControllerFactory;
import com.cydcor.framework.exceptions.RoleNotFoundException;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.FreeMarkerEngine;

/**
 * Servlet implementation class DynamicUIServlet
 */
public class DynamicUIServlet extends HttpServlet {
	private static final transient Log logger = LogFactory.getLog(DynamicUIServlet.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DynamicUIServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		String pageName = request.getParameter("pageName");
		String controlName = request.getParameter("controlName");
		String loadTemplate = request.getParameter("loadTemplate");
		String userId = request.getParameter("userId");
		String ssoToken = request.getParameter("ssoToken");
		String customController = request.getParameter("useCustomController");
		String clientKey = request.getParameter("clientKey");
		
		
		if ( StringUtils.isBlank(clientKey))
		{
			/// replace this with the default server or default client -- Lakshmi
			clientKey = "verizonfios";
			List<String> profileNamesList = com.cydcor.ext.imap.db.DBUtil.getDistinctApplicationProfiles(request.getParameter("userId"), clientKey);
			String defaultServer2 = clientKey;
			if( profileNamesList.size()>0 )
			{
				defaultServer2 = profileNamesList.get(0);
				String shortProfileLabel = defaultServer2.endsWith(" Leads Profile")? defaultServer2.replace(" Leads Profile", "") : defaultServer2;
				shortProfileLabel = shortProfileLabel.trim();
				defaultServer2 = "verizonfios";
				if( shortProfileLabel.matches("Verizon B2B")) //== "Verizon B2B" )
				{
					defaultServer2 = "verizonb2b";
				}
				if( shortProfileLabel.matches("Verizon FIOS")) // == "Verizon FIOS" )
				{
					defaultServer2 = "verizonfios";
				}			
				if( shortProfileLabel.matches("AT&T")) 
				{
					defaultServer2 = "att";
				}				
				if( shortProfileLabel.matches("Century Link")) 
				{
					defaultServer2 = "centurylink";
				}				
			}
			clientKey = defaultServer2;
		}
		
		logger.debug("pageName:"+pageName);
		logger.debug("controlName:"+controlName);
		logger.debug("loadTemplate:"+loadTemplate);
		logger.debug("customController:"+customController);
		logger.debug("clientKey:"+clientKey);
		
	
		
		
//	    if(!CydcorUtils.clientKeyNotPresent(request,response))
//	    {
//	    	logger.error("Client Key Not present..");
//	    	return;
//	    }
	    boolean bAllowToBrowse = CydcorUtils.clientKeyAndProfilesMatch(userId,clientKey);
	    
	    if(!bAllowToBrowse)
	    {
	    	String accessErrMsg=CydcorUtils.getProperty("ROLE_NOT_PRESENT");
	    	logger.error("User is not allowed to browse this page. ");
	    	CydcorUtils.showErrorPage(
	    			accessErrMsg,request,response
	    			);
	    	return;
	    	//throw new ServletException("User is not allowed to browser this page.");
	    }

	    
		response.setContentType("text/html");

		HtmlStringBuffer responseString = new HtmlStringBuffer();
		DynamicUIContext uiContext = new DynamicUIContext();
		//uiContext.setPageName(pageName);
		uiContext.setControlName(controlName);
		
		 //System.out.println("pageName:"+pageName);
		if("verizonfios".equalsIgnoreCase(clientKey) && "LeadSheetGeneration".equalsIgnoreCase(pageName)){
			
	    	//pageName = "LeadSheetGenerationTracker";
	    	//uiContext.setPageName(pageName);
	    	
			 String officeSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("sessionOfficeSeq");
			 //System.out.println("officeSeq: " + officeSeq);
			 
			Properties custProperty = loadProperties("/CustomProps.properties");
			String allowedUsers = custProperty.getProperty("FiosTrackerUsers");
			
			//code added by Sanjay FIOS Tracker for New Users
			if(StringUtils.isNotBlank(userId))
			{
				
					pageName = "LeadSheetGenerationTracker";
				
			}
			//Sanjay FIOS Tracker for New Users
			
			/*if (StringUtils.isNotBlank(allowedUsers) && StringUtils.isNotBlank(userId)){
				if(allowedUsers.toUpperCase().contains(userId.toUpperCase()))
				{
					//System.out.println("ICL Allowed : " + officeSeq);
			    	pageName = "LeadSheetGenerationTracker";
			    	
				}
//				else
//				{
//					//System.out.println("ICL Not Allowed : " + officeSeq);
//				}
			}*/

	    }
		uiContext.setPageName(pageName);
		//System.out.println("pageName:"+pageName);
		
		logger.debug("campaignSeq:"+request.getParameter("campaignSeq"));
		
		//calculate client key based on campaign (fios/b2b??)not applicable for clients having seperate tab in crm
		 if (StringUtils.isNotBlank(customController) && new Boolean(customController)) {
			String temp_clientKey = CydcorUtils.getProperty("CustomController_" + request.getParameter("campaignSeq"))
					+ ((CydcorUtils.getProperty("CustomController_" + request.getParameter("campaignSeq")) != null && !CydcorUtils
							.getProperty("CustomController_" + request.getParameter("campaignSeq"))
							.equalsIgnoreCase("")) ? "" : "");
			if(temp_clientKey!=null&&!temp_clientKey.trim().equals(""))
			{
			clientKey=temp_clientKey;
			  logger.debug("clientKey overridden:"+clientKey);
			}

		}else{//Else part is add to resolve VerizonB2B issue id - 2013088
			String campaignSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("sessionCampaignSeq");
			String temp_clientKey = CydcorUtils.getProperty("CustomController_" + campaignSeq)
					+ ((CydcorUtils.getProperty("CustomController_" + campaignSeq) != null && !CydcorUtils
							.getProperty("CustomController_" + campaignSeq)
							.equalsIgnoreCase("")) ? "" : "");
			if(temp_clientKey!=null&&!temp_clientKey.trim().equals(""))
			{
				clientKey=temp_clientKey;
				logger.debug("clientKey overridden from sessionCampaignSeq: "+clientKey);
			}
		}
		
		 //20130911 - Verizon B2B - IMS Territory development 
		 if("ICLTerritoryAnalysis".equals(pageName) || "ICLTerritoryPopup".equals(pageName)){
			 clientKey = "verizonb2b";
		 }
		 
//		 System.out.println("pageName:"+pageName);
//		if("verizonfios".equalsIgnoreCase(clientKey) && "LeadSheetGeneration".equalsIgnoreCase(pageName)){
//			
//	    	//pageName = "LeadSheetGenerationTracker";
//	    	//uiContext.setPageName(pageName);
//	    	
//			 String officeSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("sessionOfficeSeq");
//			 System.out.println("officeSeq: " + officeSeq);
//			 
//			Properties custProperty = loadProperties("/CustomProps.properties");
//			String allowedICLS = custProperty.getProperty("AllowedICLs");
//			
//			if (StringUtils.isNotBlank(allowedICLS) && StringUtils.isNotBlank(officeSeq)){
//				if(allowedICLS.contains(officeSeq))
//				{
//					System.out.println("ICL Allowed : " + officeSeq);
//			    	pageName = "LeadSheetGenerationTracker";
//			    	uiContext.setPageName(pageName);
//				}
//				else
//				{
//					System.out.println("ICL Not Allowed : " + officeSeq);
//				}
//			}
//
//	    }
	
		 
		Controller controller = ControllerFactory.getInstance().getController(pageName, clientKey);
		boolean hasRole = false;
		if ("checkCustomController".equalsIgnoreCase(controlName)) {
			if (StringUtils.isNotBlank(CydcorUtils.getProperty("CustomController_"
					+ request.getParameter("campaignSeq"))))
			{
				logger.debug("CustomController_"+
						CydcorUtils.getProperty("CustomController_" + request.getParameter("campaignSeq")
						));

				response.getWriter().append(
						"true:" + CydcorUtils.getProperty("CustomController_" + request.getParameter("campaignSeq")));
			}
			else
				response.getWriter().append("false:");

		} else if (controller != null) {
			if (StringUtils.isBlank(loadTemplate) && StringUtils.isBlank(controlName)) {
				CydcorContext.getInstance().getCache().put("roleObjMap_" + userId, new HashMap());
			}

			controller.execute(uiContext);
			for (GenericUIControl uiControl : uiContext.getControls()) {
				uiControl.render(responseString);
			}
			logger.debug("Rendered..");

			// If loadTemplate is not defined as false Page is loaded with
			// template
			if (StringUtils.isBlank(loadTemplate) && StringUtils.isBlank(controlName)) {
				Map paramMap = new HashMap();
				paramMap.put("customPage", responseString.toString());
				paramMap.put("userId", userId);
				paramMap.put("ssoToken", ssoToken);
				List<ListOrderedMap> roleNamesList;
				if (StringUtils.equalsIgnoreCase(CydcorUtils.getProperty("crmmode"), "true")) {
					// Add CRM Code here
					try {
						roleNamesList = DBUtil.getAccessLinksByRoleName();
						if (roleNamesList != null && !roleNamesList.isEmpty() && roleNamesList.size() > 0) {
							addRoleMappings(paramMap, roleNamesList);
							addUserNameAndRole(paramMap);
							hasRole = true;
						}
					} catch (Exception e) {
						logger.error(e.getMessage());
					}

				} else {
					// Add CRM Code here
					try {
						roleNamesList = DBUtil.getAllAccessLinks();
						addRoleMappings(paramMap, roleNamesList);
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				}

				if (!uiContext.isShowMap()) {
					paramMap.put("pageName", "PopupWindow");
				} else {
					paramMap.put("pageName", "TotalWindow");
				}
				paramMap.put("GoogleMapURL", CydcorUtils.getProperty("GoogleMapURL"));

				String ricoTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("template/GenericTemplate.html",
						paramMap);
				response.getWriter().append(ricoTemplate);
			} else {
				response.getWriter().append(responseString.toString());
			}

		} else {
			if (StringUtils.isBlank(loadTemplate)) {
				CydcorContext.getInstance().getCache().put("roleObjMap_" + userId, new HashMap());
			}

			Map paramMap = new HashMap();
			if ("HomePage".equalsIgnoreCase(pageName)) {
//				if (!StringUtils.equalsIgnoreCase(clientKey, "centurylink")) {
//					clientKey= null;
//				}
				if (StringUtils.isBlank(clientKey)) {
					List<String> profileNames = (List<String>) CydcorContext.getInstance().getCrmContext().get(
							"profileName");
					String profileName = "";
					if (profileNames.size() > 0) {
						profileName = profileNames.get(0);
						paramMap.put("clientKey", CydcorUtils.getProfileKey(profileName));
					}
				} else {
					paramMap.put("clientKey", clientKey);
				}

			}
			
			//20130911 - Verizon B2B - IMS Territory Analysis
			if(isTerritoryAnalysisEnabled(paramMap)){
				paramMap.put("IsTerritoryAnalysisEnabled","IsTerritoryAnalysisEnabled");
			}
			
			paramMap.put("userId", userId);
			paramMap.put("ssoToken", ssoToken);

			List<ListOrderedMap> roleNamesList;
			if (StringUtils.equalsIgnoreCase(CydcorUtils.getProperty("crmmode"), "true")) {
				// Add CRM Code here
				try {
					roleNamesList = DBUtil.getAccessLinksByRoleName();
					if (roleNamesList != null && !roleNamesList.isEmpty() && roleNamesList.size() > 0) {
						addRoleMappings(paramMap, roleNamesList);
						// add UserName and UserRole into paramMap
						addUserNameAndRole(paramMap);
						hasRole = true;
					} else {
						paramMap
								.put("message",
										"Your Role is not yet given access to the Application. Kindly contact your Administrator");
					}
				} catch (RoleNotFoundException e) {
					paramMap.put("message", e.getMessage() + " - " + userId);
					logger.error(e.getMessage() + " - " + userId);
				} catch (Exception e) {
					paramMap.put("message", e.getMessage() + " - " + userId);
					logger.error(e.getMessage() + "-" + userId);
				}
			} else {
				// Add CRM Code here
				try {
					roleNamesList = DBUtil.getAllAccessLinks();
					addRoleMappings(paramMap, roleNamesList);
					addUserNameAndRole(paramMap);
					hasRole = true;
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
			// If there is no Controller Just Render the Page
			if (StringUtils.isBlank(loadTemplate)) {

				paramMap.put("GoogleMapURL", CydcorUtils.getProperty("GoogleMapURL"));
				paramMap.put("pageName", pageName);

				if (!hasRole) {
					paramMap.put("customFile", "ErrorPage.html");
				} else {
					paramMap.put("customFile", pageName + ".html");
				}

				String ricoTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("template/GenericTemplate.html",
						paramMap);
				response.getWriter().append(ricoTemplate);
			} else {
				// Render Page wihtout Template
				paramMap.put("pageName", "TotalWindow");
				if (StringUtils.equalsIgnoreCase(CydcorUtils.getProperty("crmmode"), "true")) {
					// Add CRM Code here
					try {
						roleNamesList = DBUtil.getAccessLinksByRoleName();
						if (roleNamesList != null && !roleNamesList.isEmpty() && roleNamesList.size() > 0) {
							addRoleMappings(paramMap, roleNamesList);
							hasRole = true;
						}
					} catch (RoleNotFoundException e) {
						paramMap.put("message", e.getMessage() + " - " + userId);
						logger.error(e.getMessage() + "-" + userId);
					} catch (Exception e) {
						logger.error(e.getMessage() + "-" + userId);
						paramMap.put("message", e.getMessage());
					}

				} else {
					// Add CRM Code here
					try {
						roleNamesList = DBUtil.getAllAccessLinks();
						addRoleMappings(paramMap, roleNamesList);
						hasRole = true;
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				}
				String ricoTemplate = "";

				if (!hasRole) {
					ricoTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("ErrorPage.html", paramMap);
				} else {
					ricoTemplate = FreeMarkerEngine.getInstance().evaluateTemplate(pageName + ".html", paramMap);
				}
				response.getWriter().append(ricoTemplate);
			}
		}
		response.flushBuffer();
	}

	public Properties loadProperties(String filename) throws IOException {
		// create properties object
		Properties properties = new Properties();
		// Load properties from class loader
		properties.load(DynamicUIServlet.class.getResourceAsStream(filename));
		// Return
		return properties;
	}

	private void addUserNameAndRole(Map paramMap) {
		String userRoleFreeMarkerString = "${GlobalContext.crmContext.accessLinks}";
		String userRole = null;
		try {
			userRole = FreeMarkerEngine.getInstance().evaluateString(userRoleFreeMarkerString);
			paramMap.put("userRole", userRole);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		String userNameFreeMarkerString = "${GlobalContext.crmContext.userName}";
		String userName = null;
		try {
			userName = FreeMarkerEngine.getInstance().evaluateString(userNameFreeMarkerString);
			paramMap.put("userName", userName);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		

	}

	private void addRoleMappings(Map paramMap, List<ListOrderedMap> roleNamesList) {
		for (Map map : roleNamesList) {
			if (StringUtils.isNotBlank((String) map.get("LINK_NAME"))) {
				paramMap.put(map.get("LINK_NAME"), true);
			}
		}
	}
	
	/**
	 * Amit
	 * 20130911 - Verizon B2B - IMS Territory Analysis
	 * @param clientKey
	 * @return
	 */
	private boolean isTerritoryAnalysisEnabled(Map paramMap) {
		String userRoleFreeMarkerString = "${GlobalContext.crmContext.accessLinks}";
		String userRole = null;
		try {
			userRole = FreeMarkerEngine.getInstance().evaluateString(userRoleFreeMarkerString);
			if("Application Administrator".equals(userRole) || ("ICL Owner".equals(userRole) && "verizonb2b".equals(paramMap.get("clientKey")))){
				return true;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return false;
		}
		return false;
	}
}
