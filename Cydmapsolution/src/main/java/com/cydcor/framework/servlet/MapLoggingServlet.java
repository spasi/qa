package com.cydcor.framework.servlet;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.context.LogContext;
import com.cydcor.framework.utils.CydcorUtils;

public class MapLoggingServlet extends HttpServlet {
	private static final transient Log logger = LogFactory.getLog(MapLoggingServlet.class);
    public void service(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, IOException {
	LogContext context = new LogContext();
	context.setAccessUser(/* req.getUserPrincipal().getName() */"admin");
	context.setAccessDate(new Timestamp(System.currentTimeMillis()));
	context.setIpAdress(req.getLocalAddr());
	context.setPageName("pageName");
	context.setMapType("mapType");
	context.setMapModule("mapModule");
	CydcorUtils.log(context);
    }

}
