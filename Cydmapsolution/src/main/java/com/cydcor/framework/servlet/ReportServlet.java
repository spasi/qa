package com.cydcor.framework.servlet;

import groovyjarjarbackport.java.util.Arrays;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.report.Exporter;
import com.cydcor.framework.report.impl.ExportContext;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * Hello world!
 * 
 */
public class ReportServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final Log logger = LogFactory.getLog(getClass());

	@SuppressWarnings("unchecked")
	@Override
	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String clientKey = (String) req.getParameter("clientKey");
		if (clientKey == null)
			clientKey = "";

		Exporter exporter = (Exporter) ServiceLocator.getService(clientKey + "Exporter");
		ExportContext context = new ExportContext();
		// System.out.println(req.getParameter("reportType"));

		Enumeration<String> paramNames = req.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			context.getReportParameters().put(param, req.getParameter(param));
		}
		context.setReportDescription("Report desc");
		context.setReportName(req.getParameter("leadSheetId"));
		context.getReportParameters().put("titleImage", new ClassPathResource("cydcor_logo.gif").getPath());
		context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
		context.getReportParameters().put("columnWidths",
				CydcorUtils.getProperty(clientKey + "_" + req.getParameter("queryString") + "_WIDTHS"));
		// context.getReportParameters().put("subTitle",
		// "Leads under this Lead Sheet");
		if (StringUtils.isNotBlank(clientKey + "_" + req.getParameter("queryString"))) {
			context.getReportParameters().put(
					"ignorableColumns",
					Arrays
							.asList(CydcorUtils.getProperty(clientKey + "_" + req.getParameter("queryString")).split(
									",")));
		}
		context.getReportParameters().put("showPointColorInReport", CydcorUtils.getProperty("showPointColorInReport"));
		context.getReportParameters().put("leadsPerMap", CydcorUtils.getProperty(clientKey + "leadsPerMap"));
		context.getReportParameters().put("rowsPerPage", CydcorUtils.getProperty(clientKey + "rowsPerPage"));
		context.getReportParameters().put("rowsFirstPage", CydcorUtils.getProperty(clientKey + "rowsFirstPage"));
		try {
			context.setSqlConnecion(((JdbcTemplate) ServiceLocator.getService(clientKey + "JdbcTemplate"))
					.getDataSource().getConnection());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = QueryUtils.getQuery().get(req.getParameter("queryString"));
		if ("ICL_REP_LEADLIST_BY_LEADSHEET_ID".equalsIgnoreCase(req.getParameter("queryString"))) {
			context.getSqlParameters().add(req.getParameter("leadSheetId"));

		} //atiwari : Modified for Vegas campaign
		else if ("ICL_REP_LEAD_SHEET_PRINT".equalsIgnoreCase(req.getParameter("queryString")) 
				|| "ICL_REP_LEAD_SHEET_PRISM_VEGAS_PRINT".equalsIgnoreCase(req.getParameter("queryString"))
				|| "ICL_REP_LEAD_SHEET_PRISM_PRINT".equalsIgnoreCase(req.getParameter("queryString"))) {
			logger.debug(req.getParameter("queryString"));
			if ("true".equalsIgnoreCase(req.getParameter("showMapInReport")))
				context.setShowMapInReport(true);
			context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
			context.getSqlParameters().add(req.getParameter("leadSheetId"));
			if (clientKey.startsWith("verizon"))
				context.getSqlParameters().add(req.getParameter("leadSheetId"));
			else if (clientKey.startsWith("centurylink"))
				context.getSqlParameters().add(req.getParameter("leadSheetId"));
			// sql = sql.replaceAll(":leadSheetId", "'" +
			// req.getParameter("leadSheetId") + "'");
			// context.getReportParameters().put("enableMapContent", "true");
		} 
		else if ("SELECT_IMS_DASHBOARD_BY_RUNID".equalsIgnoreCase(req.getParameter("queryString"))) 
		{
			context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
			context.getSqlParameters().add(req.getParameter("runId"));
		} 
		
		else if ("SELECT_IMS_LeadApproval_report".equalsIgnoreCase(req.getParameter("queryString"))) 
		{
			context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
			context.getSqlParameters().add(req.getParameter("DA"));
			context.getSqlParameters().add(req.getParameter("CLLI"));
		}
		
		else if ("SELECT_IMS_LeadOwner_report".equalsIgnoreCase(req.getParameter("queryString"))) 
		{
			context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
			context.getSqlParameters().add(req.getParameter("OWNER"));
		    context.getSqlParameters().add(req.getParameter("LEAD_ASSIGNED"));
		} 
		 // new code added by sanjay  
		else if ("SELECT_IMS_New_LeadOwner_report".equalsIgnoreCase(req.getParameter("queryString"))) 
		{
			context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
			context.getSqlParameters().add(req.getParameter("OWNER"));
		    context.getSqlParameters().add(req.getParameter("LEAD_ASSIGNED"));
		} 

		else if ("SELECT_LEAD_APPROVAL_BY_DOWNLOAD".equalsIgnoreCase(req.getParameter("queryString"))) 
		{
			context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
		    context.getSqlParameters().add(req.getParameter("uploadStartDate"));
		    context.getSqlParameters().add(req.getParameter("uploadEndDate"));
		} 

		else if ("SELECT_LEAD_OWNER_DOWNLOAD".equalsIgnoreCase(req.getParameter("queryString"))) 
		{
			context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
			context.getSqlParameters().add(req.getParameter("uploadStartDate"));
			context.getSqlParameters().add(req.getParameter("uploadEndDate"));
		} 

		
		else if ("ICL_REP_LEADLIST_FOR_EXPORT_XLS".equalsIgnoreCase(req.getParameter("queryString"))) {
			sql = QueryUtils.getQuery().get(req.getParameter("queryString"));

			context.getReportParameters().put("CUSTOM_HEADERS", true);
			context.getSqlParameters().add(req.getParameter("dispositionStDate"));
			context.getSqlParameters().add(req.getParameter("dispositionEndDate"));
			context.getSqlParameters().add(clientKey);
			if (!StringUtils.equalsIgnoreCase("-1", req.getParameter("campaignId"))) {
				sql = sql + " AND LF.CAMPAIGN_SEQ = ? ";
				context.getSqlParameters().add(req.getParameter("campaignId"));
			}
			if (!StringUtils.equalsIgnoreCase("-1", req.getParameter("officeId"))) {
				sql = sql + " AND LF.OFFICE_SEQ = ? ";
				context.getSqlParameters().add(req.getParameter("officeId"));
			}

			context.setSqlString(sql);
			context.setReportName("AT&T Format Template");
			String[] columnLengths = new String[3];
			List inputs = new ArrayList();
			inputs.add(clientKey);
			DatabaseResult databaseResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get("ICL_REP_LEADLIST_FOR_EXPORT_XLS_LENGTHS"), inputs);
			for (int i = 0; i < databaseResult.getData().get(0).size(); i++) {
				if (databaseResult.getData().get(0).get(i).getValue() != null)
				    columnLengths[i] = databaseResult.getData().get(0).get(i).getValue();
				else
				    columnLengths[i] = databaseResult.getData().get(1).get(i).getValue();
			    }
			context.getReportParameters().put("columnLengths", columnLengths);
		} else if ("SELECT_LEADSHEETS".equalsIgnoreCase(req.getParameter("queryString"))) {
			sql = QueryUtils.getQuery().get(req.getParameter("queryString"));
			String archivedSheetsSql="";
			if(req.getParameter("campaignSeq").equalsIgnoreCase("67976206") 
					|| req.getParameter("campaignSeq").equalsIgnoreCase("91039497"))
			{
				archivedSheetsSql = QueryUtils.getQuery().get("SELECT_CLOSED_LEADSHEETS_CLVEGAS");
			}
			else
			{
				archivedSheetsSql = QueryUtils.getQuery().get("SELECT_CLOSED_LEADSHEETS");
			}	
			List<Object> archivedSylheetsInputs = new ArrayList<Object>();
			archivedSylheetsInputs.add(req.getParameter("startDate"));
			archivedSylheetsInputs.add(req.getParameter("endDate"));
			context.getSqlParameters().add(req.getParameter("startDate"));
			context.getSqlParameters().add(req.getParameter("endDate"));

			if (StringUtils.isNotBlank(req.getParameter("campaignSeq"))) {
				sql = sql + " " + QueryUtils.getQuery().get("ADD_CAMPAIGN_SEQ");
				context.getSqlParameters().add(new Long(req.getParameter("campaignSeq")));
				archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_CAMPAIGN_SEQ");
				archivedSylheetsInputs.add(new Long(req.getParameter("campaignSeq")));

			}

			if (StringUtils.isNotBlank(req.getParameter("officeSeq"))) {
				sql = sql + " " + QueryUtils.getQuery().get("ADD_OFFICE_SEQ");
				context.getSqlParameters().add(new Long(req.getParameter("officeSeq")));
				archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_OFFICE_SEQ");
				archivedSylheetsInputs.add(new Long(req.getParameter("officeSeq")));
			}

			if (StringUtils.isNotBlank(req.getParameter("repId"))) {
				if (!req.getParameter("repId").equalsIgnoreCase("UA")) {
					sql = sql + " " + QueryUtils.getQuery().get("ADD_PERSON_ID");
					context.getSqlParameters().add(new Long(req.getParameter("repId")));
					archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_PERSON_ID");
					archivedSylheetsInputs.add(new Long(req.getParameter("repId")));
				} else {
					sql = sql + " " + QueryUtils.getQuery().get("ADD_UN_ASSGN_PERSON_ID");
					archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_UN_ASSGN_PERSON_ID");
				}
			}

			if (StringUtils.isNotBlank(req.getParameter("status"))) {
				sql = sql + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_STATUS");
				context.getSqlParameters().add(req.getParameter("status"));
				archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_STATUS");
				archivedSylheetsInputs.add(req.getParameter("status"));
			}

			String leadSheetID = req.getParameter("leadSheetID");
			if (leadSheetID != null && "%".equalsIgnoreCase(leadSheetID.trim()))
				leadSheetID = null;
			if (StringUtils.isNotBlank(leadSheetID)) {
				sql = sql + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_NAME");
				context.getSqlParameters().add(leadSheetID.trim());
				archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_NAME");
				archivedSylheetsInputs.add(leadSheetID.trim());
			}
			String[] columnLengths = CydcorUtils.getProperty(
					clientKey + "_" + req.getParameter("queryString") + "_WIDTHS").replaceAll(" ", "").split(",");
			context.getReportParameters().put("columnLengths", columnLengths);
			sql = sql + " UNION ALL " + archivedSheetsSql;
			context.getSqlParameters().addAll(archivedSylheetsInputs);
		}

		logger.debug("final sql:"+sql);
		context.setSqlString(sql);
		OutputStream outputStream = res.getOutputStream();
		context.setExportStream(outputStream);

		if (Exporter.PDF.equalsIgnoreCase(req.getParameter("reportType"))) {
			context.setReportType(Exporter.PDF);
			res.setContentType("application/pdf");
			//atiwari : Modified for Vegas campaign
			if ("ICL_REP_LEAD_SHEET_PRINT".equalsIgnoreCase(req.getParameter("queryString"))
					|| "ICL_REP_LEAD_SHEET_PRISM_VEGAS_PRINT".equalsIgnoreCase(req.getParameter("queryString"))
					|| "ICL_REP_LEAD_SHEET_PRISM_PRINT".equalsIgnoreCase(req.getParameter("queryString"))
					|| "ICL_REP_LEADLIST_BY_LEADSHEET_ID".equalsIgnoreCase(req.getParameter("queryString"))) {
				List inputs = new ArrayList();
				String[] leadSheetsIds = null;
				boolean leadsExist = false;
				if ("ICL_REP_LEADLIST_BY_LEADSHEET_ID".equalsIgnoreCase(req.getParameter("queryString"))) {
					leadsExist = true;
				} else {
					if (StringUtils.isNotBlank(req.getParameter("leadSheets"))) {
//						leadSheetsIds = (req.getParameter("leadSheets")).split(",");
						if (clientKey.equalsIgnoreCase("verizonfios"))
							leadSheetsIds = (req.getParameter("leadSheets")).split("!");
						else if(clientKey.equalsIgnoreCase("centurylink"))
							leadSheetsIds = (req.getParameter("leadSheets")).split("!");
						else
							leadSheetsIds = (req.getParameter("leadSheets")).split(",");
					} else {
						leadSheetsIds = new String[1];
						leadSheetsIds[0] = req.getParameter("leadSheetId");
					}

					for (String sheet : leadSheetsIds) {
						inputs.clear();
						inputs.add(sheet);
						//atiwari : Modified for Vegas campaign
						if(!"ICL_REP_LEAD_SHEET_PRISM_VEGAS_PRINT".equalsIgnoreCase(req.getParameter("queryString"))){
							if (clientKey.startsWith("verizon"))
								inputs.add(sheet);
							else if (clientKey.startsWith("centurylink"))
								inputs.add(sheet);
						}
						if (getPlatformService().loadResult(QueryUtils.getQuery().get(req.getParameter("queryString")),
								inputs).getData().size() > 0) {
							leadsExist = true;
							break;
						}
					}
				}
				if (leadsExist) {
					res.setHeader("Content-Disposition", "inline; filename=" + req.getParameter("reportName") + "."
							+ req.getParameter("reportType"));
					exporter.buildPDFReport(context);
				} else {
					res.setContentType("text/html");
					outputStream.write("No Leads availabe to export. All leads are expired/dispositioned".getBytes());
				}

			} else {
				res.setHeader("Content-Disposition", "attachment; filename=" + req.getParameter("reportName") + "."
						+ req.getParameter("reportType"));
				exporter.export(context);
			}
		} else if (Exporter.XLS.equalsIgnoreCase(req.getParameter("reportType"))) {
			res.setHeader(
					"Content-Disposition",
					"attachment; filename=" + req.getParameter("reportName").replaceAll(" ", "_") + "."
					+ req.getParameter("reportType"));
			
			
			
			
			res.setContentType("application/vnd.ms-excel");
			context.setReportType(Exporter.XLS);
			exporter.export(context);
		}

	}

	protected PlatformService getPlatformService() {
		return ServiceLocator.getService(PlatformService.class);
	}

}
