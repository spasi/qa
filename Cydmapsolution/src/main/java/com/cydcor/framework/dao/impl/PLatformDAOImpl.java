/**
 *
 */
package com.cydcor.framework.dao.impl;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.TableMetaDataCache;
import com.cydcor.framework.dao.PlatformDAO;
import com.cydcor.framework.model.B2BReport;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.GenericModel;
import com.cydcor.framework.model.LeadAssignment;
import com.cydcor.framework.model.LeadLifeCycle;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.MerOffice;
import com.cydcor.framework.model.MerOfficeInsert;
import com.cydcor.framework.model.ReportComponent;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.model.Template;
import com.cydcor.framework.model.UserObject;
import com.cydcor.framework.model.VerizonFiosReport;
import com.cydcor.framework.rowmapper.B2BReportRowMapper;
import com.cydcor.framework.rowmapper.LeadAssignmentRowMapper;
import com.cydcor.framework.rowmapper.LeadMasterMapper;
import com.cydcor.framework.rowmapper.ReportColumnRowMapper;
import com.cydcor.framework.rowmapper.ReportComponentRowMapper;
import com.cydcor.framework.rowmapper.ReportParamRowMapper;
import com.cydcor.framework.rowmapper.TableComponentRowMapper;
import com.cydcor.framework.rowmapper.TemplateMapper;
import com.cydcor.framework.rowmapper.VerizonFiosReportRowMapper;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * @author ashwin
 * 
 */
@Repository(value = "platformDAO")
@Scope("prototype")
public class PLatformDAOImpl implements PlatformDAO {

	private static final transient Log logger = LogFactory.getLog(PLatformDAOImpl.class);

	private static final String LEAD_MASTER_INSERT = "INSERT INTO IMS.IMS_LEAD_LIFECYCLE (LEAD_ID,GEO_POINT,VALIDATION_CODE,RESPONSE_TEXT,IS_ACTIVE,CREATED_DATE,CREATED_BY,"
		+ "MODIFIED_DATE,MODIFIED_BY,LEAD_STATUS,RUN_ID,CAMPAIGN_SEQ,OFFICE_SEQ,CLLI,DA,PERSON_ID,MARKER_ICON_IMAGE,CREDIT_CLASS,GREEN_STATUS,CONN_TECH,DISPOSITION_CODE,DISPOSITION,CUST_NAME,FULL_ADDR,APT_NUM,CITY,STATE,LEAD_EXPIRES,NOTES,ZIP,LEAD_OWNER,LEAD_DOB,LMS_ROWID,LEAD_ASSIGNED,LEAD_RELEASE_DATE,STREET_ADDRESS,APT_IND) VALUES (?,geography::STGeomFromText(?,4623),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String LEAD_MASTER_UPDATE = "UPDATE IMS.IMS_LEAD_LIFECYCLE SET GEO_POINT=geography::STGeomFromText(?,4623),VALIDATION_CODE=?,RESPONSE_TEXT=?,IS_ACTIVE=?,CREATED_DATE=?,CREATED_BY=?,"
		+ "MODIFIED_DATE=?,MODIFIED_BY=?,LEAD_STATUS=? ,CAMPAIGN_SEQ=?,OFFICE_SEQ=?,CLLI=?,DA=?,PERSON_ID=?,MARKER_ICON_IMAGE=?,CREDIT_CLASS=?,GREEN_STATUS=?,CONN_TECH=?,DISPOSITION_CODE=?,DISPOSITION=?,CUST_NAME=?,FULL_ADDR=?,APT_NUM=?,CITY=?,STATE=?,LEAD_EXPIRES=?,NOTES=?,LEAD_OWNER=?,LEAD_DOB=?,LMS_ROWID=?,LEAD_ASSIGNED=?,LEAD_RELEASE_DATE=?,STREET_ADDRESS=?,APT_IND=?,ZIP=? WHERE LMS_ROWID=?";

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		logger.debug(">>getJdbcTemplate");
		JdbcTemplate jdbcTemplate = null;
		try {
//			String clientkey=
//					CydcorContext.getInstance()
//					.getRequestContext().getRequestWrapper().getParameter("clientKey");
//			
//			logger.debug("Connecting for clientkey:"+clientkey);
//			
//			jdbcTemplate = (JdbcTemplate) ServiceLocator
//					.getService(clientkey+ "JdbcTemplate");
			
			jdbcTemplate = (JdbcTemplate) ServiceLocator
			.getService((CydcorContext.getInstance().getRequestContext() != null
					&& CydcorContext.getInstance().getRequestContext().getRequestWrapper()
					.getParameter("clientKey") != null ? CydcorContext.getInstance()
							.getRequestContext().getRequestWrapper().getParameter("clientKey") : "")
							+ "JdbcTemplate");
			
		/*	jdbcTemplate = (JdbcTemplate) ServiceLocator
			.getService((CydcorContext.getInstance().getRequestContext() != null
					&& CydcorContext.getInstance().getRequestContext().getRequestWrapper()
					.getParameter("clientKey") != null ? CydcorContext.getInstance()
							.getRequestContext().getRequestWrapper().getParameter("clientKey") : "")
							+ "JdbcTemplate");*/
		} catch (Exception e) {
			logger.error(e);
		
		}
		return jdbcTemplate;
	}

	public JdbcTemplate getJdbcTemplateByClientId(String clientkey) {
		logger.debug(">>getJdbcTemplateByClientId. clientkey>"+clientkey);
		JdbcTemplate jdbcTemplate = null;
		try {
			
			if(StringUtils.isBlank(clientkey))
			{
				logger.error("ClientKey could not be null.");
				return null;
			}
			
			jdbcTemplate = (JdbcTemplate) ServiceLocator
					.getService(clientkey+ "JdbcTemplate");
			
		/*	jdbcTemplate = (JdbcTemplate) ServiceLocator
			.getService((CydcorContext.getInstance().getRequestContext() != null
					&& CydcorContext.getInstance().getRequestContext().getRequestWrapper()
					.getParameter("clientKey") != null ? CydcorContext.getInstance()
							.getRequestContext().getRequestWrapper().getParameter("clientKey") : "")
							+ "JdbcTemplate");*/
		} catch (Exception e) {
			logger.error(e);
		
		}
		return jdbcTemplate;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.linkwithweb.framework.dao.PlatformDAO#loadUser(java.lang.String)
	 */
	public UserObject loadUser(String lwbCode) {
		List resultList = getJdbcTemplate().queryForList("select * from employees");

		for (Iterator it = resultList.iterator(); it.hasNext();) {
			Map rowMap = (Map) it.next();
			Set s = rowMap.entrySet();
			for (Iterator i = s.iterator(); i.hasNext();) {
				Map.Entry me = (Map.Entry) i.next();
				logger.debug(me.getKey() + ":" + me.getValue() + "      ");
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.linkwithweb.framework.dao.PlatformDAO#loadResult(java.lang.String)
	 */
	public DatabaseResult loadResult(String sql) {
		return loadResult(sql, new ArrayList());
	}

	/**
	 * @param sql
	 * @param values
	 * @return
	 */
	public DatabaseResult loadResult(String sql, List values) {

		logger.debug(">loadResult");
		/**
		 * Evaluate SQL To addin any Runtime Filters
		 * 
		 */
		try {
			sql = FreeMarkerEngine.getInstance().evaluateString(sql);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}

		TableComponentRowMapper rowMapper = new TableComponentRowMapper();
		List resultList = new ArrayList();

		logger.debug("sql to execute:"+sql);
		if (values != null && !values.isEmpty()) {
			resultList = getJdbcTemplate().query(sql, values.toArray(new Object[0]), rowMapper);
		} else {
			resultList = getJdbcTemplate().query(sql, rowMapper);
		}

		DatabaseResult result = new DatabaseResult();

		result.setData(resultList);
		result.setMetadata(TableMetaDataCache.getInstance().getMetaData(rowMapper.getEntityName()));
		return result;
	}

	/*
	 * public DatabaseResult query(String sql, List values) { DatabaseResult
	 * result = new DatabaseResult();
	 * result.setData(getJdbcTemplate().queryForList(sql, values.toArray(new
	 * Object[0]))); return result; }
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.linkwithweb.framework.dao.PlatformDAO#loadReport(java.lang.String)
	 */
	public ReportComponent loadReport(String reportName) {
		logger.debug(">loadReport:"+reportName);
		/**
		 * Load Report,Report Columns and Report Parameters
		 */
		ReportComponent reportComponent = null;
		/*
		 * String reportMetadataSql =
		 * "SELECT LWB_FRM_ENTITY.NAME,LWB_FRM_ENTITY.DESCRIPTION,LWB_FRM_ENTITY.ENTITY_TYPE,LWB_FRM_ENTITY.ENTITY_VALUE,LWB_FRM_ENTITY.SCHEMA_NAME,LWB_FRM_REPORT.REPORT_NAME,LWB_FRM_REPORT.DESCRIPTION,LWB_FRM_REPORT.EXPORT_ENABLED,LWB_FRM_REPORT.EXPORT_FORMATS_SUPPORTED,LWB_FRM_REPORT.STYLESHEET,LWB_FRM_REPORT.OUTPUT_TEMPLATE FROM LWB_FRM_ENTITY,LWB_FRM_REPORT WHERE LWB_FRM_REPORT.REF_ENTITY_NAME=LWB_FRM_ENTITY.NAME AND LWB_FRM_REPORT.IS_ACTIVE='1' AND LWB_FRM_ENTITY.IS_ACTIVE='1' AND LWB_FRM_REPORT.REPORT_NAME='"
		 * + reportName + "'";
		 */

		ReportComponentRowMapper rowMapper = new ReportComponentRowMapper();
		List resultList = getJdbcTemplate().query(QueryUtils.getQuery().get("SELECT_REPORT_META_DATA"),
				new Object[]{reportName}, rowMapper);

		// If Resultset is not null or empty assign value to report component
		reportComponent = (resultList != null && !resultList.isEmpty()) ? (ReportComponent) resultList.get(0) : null;

		// @todo In Future Append all Framework Tables with Framework SchemaName
		// By Evaluating it Dynamically With Freemarker
		// Now Load Data For Report Columns
		if (reportComponent != null) {

			String componentReportName = reportComponent.getReportName();
			/*
			 * String reportColumnsSql =
			 * "SELECT * FROM LWB_FRM_REPORT_COLUMNS WHERE REF_REPORT_NAME = '"
			 * + reportComponent.getReportName() + "'";
			 */

			ReportColumnRowMapper columnRowMapper = new ReportColumnRowMapper();
			resultList = getJdbcTemplate().query(QueryUtils.getQuery().get("SELECT_LWB_FRM_REPORT_COLUMNS"),
					new Object[]{componentReportName}, columnRowMapper);

			reportComponent.setColumns(resultList);
			/**
			 * Now add All Report Parameters
			 */
			/*
			 * String reportParamsSql =
			 * "SELECT * FROM LWB_FRM_REPORT_PARAMS WHERE REF_REPORT_NAME = '" +
			 * reportComponent.getReportName() + "'";
			 */

			ReportParamRowMapper paramRowMapper = new ReportParamRowMapper();
			resultList = getJdbcTemplate().query(QueryUtils.getQuery().get("SELECT_LWB_FRM_REPORT_PARAMS"),
					new Object[]{componentReportName}, paramRowMapper);

			reportComponent.setParameters(resultList);
		}

		return reportComponent;
	}

	public List<? extends GenericModel> loadRowMapper(String sql, RowMapper rowMapper) {
		return getJdbcTemplate().query(sql, rowMapper);
	}

	public List<? extends GenericModel> loadRowMapper(String sql, RowMapper rowMapper,String clientKey) {
		return getJdbcTemplateByClientId(clientKey).query(sql, rowMapper);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.linkwithweb.framework.dao.PlatformDAO#countRows(java.lang.String)
	 */
	public int countRows(String sql) {

		logger.debug(">countRows: sql>"+sql);
		/**
		 * Evaluate SQL To addin any Runtime Filters
		 * 
		 */
		try {
			sql = FreeMarkerEngine.getInstance().evaluateString(sql);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		return getJdbcTemplate().queryForInt(sql);
	}

	public void save(String query, Object[] values) {
		logger.debug("save> query:"+query);
		getJdbcTemplate().update(query, values);
	}

	public void populateLeadInput(final List<LeadLifeCycle> values) {
		logger.debug("populateLeadInput");
		getJdbcTemplate().batchUpdate(LEAD_MASTER_INSERT, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				populateLeadInputValues(ps, values.get(i));
			}

			public int getBatchSize() {
				return values.size();
			}
		});
	}

	public void updateLeadLifeCycle(final List<LeadLifeCycle> values) {
		logger.debug(">updateLeadLifeCycle");
		// Check if the lead record exists in LEAD_LIFECYCLE. IF yes then update
		// else call populateLeadInput()

		getJdbcTemplate().batchUpdate(LEAD_MASTER_UPDATE, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				updateLeadLifeCycleValues(ps, values.get(i));
			}

			public int getBatchSize() {
				return values.size();
			}
		});
	}

	private void populateLeadInputValues(PreparedStatement ps, LeadLifeCycle inputInsert) throws SQLException {
		logger.debug(">populateLeadInputValues");
		try {
			ps.setString(1, inputInsert.getLeadId());
			ps.setString(2, inputInsert.getGeoPoint());
			ps.setString(3, inputInsert.getValidationCode());
			ps.setString(4, inputInsert.getReponseFromGoogle());
			ps.setString(5, inputInsert.getIsActive());
			ps.setTimestamp(6, inputInsert.getCreatedDate());
			ps.setString(7, inputInsert.getCreatedBy());
			ps.setTimestamp(8, inputInsert.getModifiedDate());
			ps.setString(9, inputInsert.getModifiedBy());
			ps.setString(10, inputInsert.getLeadStatus());
			ps.setString(11, inputInsert.getRunId());
			ps.setLong(12, inputInsert.getCampaignSeq());
			if (!StringUtils.isBlank(inputInsert.getOfficeSeq())) {
				ps.setLong(13, new Long(inputInsert.getOfficeSeq()));
			} else {
				ps.setObject(13, null);
			}
			ps.setString(14, inputInsert.getWireCenter());
			ps.setString(15, inputInsert.getDa());
			ps.setString(16, inputInsert.getPersonId());
			ps.setString(17, inputInsert.getMarkerIconImage());
			ps.setString(18, inputInsert.getCreditClass());
			ps.setString(19, inputInsert.getGreenStatus());
			ps.setString(20, inputInsert.getConnTech());
			ps.setString(21, inputInsert.getDispositionCode());
			ps.setString(22, inputInsert.getDisposition());
			ps.setString(23, inputInsert.getCustName());
			ps.setString(24, inputInsert.getFullAddr());
			ps.setString(25, inputInsert.getAptNum());
			ps.setString(26, inputInsert.getCity());
			ps.setString(27, inputInsert.getState());
			ps.setTimestamp(28, inputInsert.getLeadExpires());
			ps.setString(29, inputInsert.getNotes());
			ps.setString(30, inputInsert.getZip());
			ps.setString(31, inputInsert.getLeadOwner());
			ps.setTimestamp(32, inputInsert.getLead_dob());
			ps.setLong(33, inputInsert.getLmsRowID());
			ps.setTimestamp(34, inputInsert.getLeadAssigned());
			ps.setTimestamp(35, inputInsert.getLeadReleaseDate());
			ps.setString(36, inputInsert.getStreetAddress());
			ps.setString(37, inputInsert.getAptInd());
		} catch (Exception e) {
			logger.debug(e);
		}
	}

	private void updateLeadLifeCycleValues(PreparedStatement ps, LeadLifeCycle inputInsert) throws SQLException {
		ps.setString(1, inputInsert.getGeoPoint());
		ps.setString(2, inputInsert.getValidationCode());
		ps.setString(3, inputInsert.getReponseFromGoogle());
		ps.setString(4, inputInsert.getIsActive());
		ps.setTimestamp(5, inputInsert.getCreatedDate());
		ps.setString(6, inputInsert.getCreatedBy());
		ps.setTimestamp(7, inputInsert.getModifiedDate());
		ps.setString(8, inputInsert.getModifiedBy());
		ps.setString(9, inputInsert.getLeadStatus());
		ps.setLong(10, inputInsert.getCampaignSeq());
		ps.setLong(11, new Long(inputInsert.getOfficeSeq()));
		if (!StringUtils.isBlank(inputInsert.getOfficeSeq())) {
			ps.setLong(12, new Long(inputInsert.getOfficeSeq()));
		} else {
			ps.setObject(12, null);
		}
		ps.setString(13, inputInsert.getDa());
		ps.setString(14, inputInsert.getPersonId());
		ps.setString(15, inputInsert.getMarkerIconImage());
		ps.setString(16, inputInsert.getCreditClass());
		ps.setString(17, inputInsert.getGreenStatus());
		ps.setString(18, inputInsert.getConnTech());
		ps.setString(19, inputInsert.getDispositionCode());
		ps.setString(20, inputInsert.getDisposition());
		ps.setString(21, inputInsert.getCustName());
		ps.setString(22, inputInsert.getFullAddr());
		ps.setString(23, inputInsert.getAptNum());
		ps.setString(24, inputInsert.getCity());
		ps.setString(25, inputInsert.getState());
		ps.setTimestamp(26, inputInsert.getLeadExpires());
		ps.setString(27, inputInsert.getNotes());
		ps.setString(28, inputInsert.getLeadOwner());
		ps.setTimestamp(29, inputInsert.getLead_dob());
		ps.setLong(30, inputInsert.getLmsRowID());
		ps.setTimestamp(31, inputInsert.getLeadAssigned());
		ps.setTimestamp(32, inputInsert.getLeadReleaseDate());
		ps.setString(33, inputInsert.getStreetAddress());
		ps.setString(34, inputInsert.getAptInd());
		ps.setString(35, inputInsert.getZip());
		ps.setLong(36, inputInsert.getLmsRowID());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cydcor.framework.dao.PlatformDAO#execute(java.lang.String)
	 */
	public void execute(String sql) {

		/**
		 * Evaluate SQL To addin any Runtime Filters
		 * 
		 */
		try {
			sql = FreeMarkerEngine.getInstance().evaluateString(sql);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		getJdbcTemplate().execute(sql);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cydcor.framework.dao.PlatformDAO#update(java.lang.String,
	 * java.lang.Object[])
	 */
	public void update(String sql, Object[] args) {

		/**
		 * Evaluate SQL To addin any Runtime Filters
		 * 
		 */
		try {
			sql = FreeMarkerEngine.getInstance().evaluateString(sql);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		getJdbcTemplate().update(sql, args);
	}

	public void update(String sql, Object[] args,String clientKey) {

		/**
		 * Evaluate SQL To addin any Runtime Filters
		 * 
		 */
		try {
			sql = FreeMarkerEngine.getInstance().evaluateString(sql);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		getJdbcTemplateByClientId(clientKey).update(sql, args);
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.dao.PlatformDAO#populateIclMaster(java.util.List)
	 */
	public void populateIclMaster(final List<MerOfficeInsert> merOfficeInserts) {
		logger.debug("ICL_MASTER_INSERT::: " + QueryUtils.getQuery().get("ICL_MASTER_INSERT"));
		getJdbcTemplate().batchUpdate(QueryUtils.getQuery().get("ICL_MASTER_INSERT"),
				new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				populateIclMasterValues(ps, merOfficeInserts.get(i));
			}

			public int getBatchSize() {
				return merOfficeInserts.size();
			}
		});

	}
	public void populateIclMaster(final List<MerOfficeInsert> merOfficeInserts,String clientKey) {
		logger.debug("ICL_MASTER_INSERT::: " + QueryUtils.getQuery().get("ICL_MASTER_INSERT"));
		getJdbcTemplateByClientId(clientKey).batchUpdate(QueryUtils.getQuery().get("ICL_MASTER_INSERT"),
				new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				populateIclMasterValues(ps, merOfficeInserts.get(i));
			}

			public int getBatchSize() {
				return merOfficeInserts.size();
			}
		});

	}
	
	public void deleteOfficeSeqs(final List<MerOffice> merOffice) {
		logger.debug("ICL_MASTER_DELETE::: " + QueryUtils.getQuery().get("DELETE_FROM_ICLMASTER_BY_OFFICE_SEQ"));
		getJdbcTemplate().batchUpdate(QueryUtils.getQuery().get("DELETE_FROM_ICLMASTER_BY_OFFICE_SEQ"),
				new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				setOfficeSeqs(ps, merOffice.get(i));
			}

			public int getBatchSize() {
				return merOffice.size();
			}
		});

	}

	private void setOfficeSeqs(PreparedStatement ps, MerOffice merOffices) throws SQLException {
		ps.setInt(1, merOffices.getOffice_seq());
	}

	/**
	 * @param ps
	 * @param merOfficeInsert
	 * @throws SQLException
	 */
	private void populateIclMasterValues(PreparedStatement ps, MerOfficeInsert merOfficeInsert) throws SQLException {
		ps.setInt(1, merOfficeInsert.getOfficeSeq());
		ps.setString(2, merOfficeInsert.getGeoLocation());
		ps.setString(3, merOfficeInsert.getIsActive());
		ps.setTimestamp(4, merOfficeInsert.getCreatedDate());
		ps.setLong(5, merOfficeInsert.getCreatedBy());
		ps.setTimestamp(6, merOfficeInsert.getModifiedDate());
		ps.setLong(7, merOfficeInsert.getModifiedBy());
		ps.setString(8, merOfficeInsert.getFullAddr());
		ps.setInt(9, merOfficeInsert.getOfficeSeq());
		ps.setString(10, merOfficeInsert.getGeoLocation());
		ps.setString(11, merOfficeInsert.getIsActive());
		ps.setTimestamp(12, merOfficeInsert.getCreatedDate());
		ps.setLong(13, merOfficeInsert.getCreatedBy());
		ps.setTimestamp(14, merOfficeInsert.getModifiedDate());
		ps.setLong(15, merOfficeInsert.getModifiedBy());
		ps.setString(16, merOfficeInsert.getFullAddr());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.dao.PlatformDAO#loadTemplate(com.cydcor.framework
	 * .model.Template)
	 */
	public void loadTemplate(Template template) {
		List<Template> resultList = getJdbcTemplate().query(QueryUtils.getQuery().get("LOAD_TEMPLATE"),
				new TemplateMapper());

		if (!resultList.isEmpty()) {
			Template result = resultList.get(0);
			template.setTemplateType(result.getTemplateType());
			template.setTemplateName(result.getTemplateName());
			template.setTemplateContent(result.getTemplateContent());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cydcor.framework.dao.PlatformDAO#saveAndGetKey(java.lang.String,
	 * java.lang.Object[])
	 */
	public Long saveAndGetKey(final String sql, final Object[] args) {

		KeyHolder keyHolder = new GeneratedKeyHolder();

		getJdbcTemplate().update(new PreparedStatementCreator() {

			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int i = 1;
				for (Object object : args) {
					statement.setObject(i, object);
					i++;
				}

				return statement;

			}
		}, keyHolder);
		if (keyHolder.getKey() != null)
			return new Long(keyHolder.getKey().longValue());
		return null;
	}

	public void insertWireCenters(final String[] values) {
		// Check if the lead record exists in LEAD_LIFECYCLE. IF yes then update
		// else call populateLeadInput()

		getJdbcTemplate().batchUpdate(QueryUtils.getQuery().get("INSERT_WC_SQL"), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				insertWCValues(ps, values[i]);
			}

			private void insertWCValues(PreparedStatement ps, String object) throws SQLException {
				ps.setString(2, object.split(":")[0]);
				ps.setString(1, object.split(":")[1]);
			}

			public int getBatchSize() {
				return values.length;
			}
		});
	}

	public void insertDAs(final String[] values) {
		// Check if the lead record exists in LEAD_LIFECYCLE. IF yes then update
		// else call populateLeadInput()

		getJdbcTemplate().batchUpdate(QueryUtils.getQuery().get("INSERT_DA_SQL"), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				insertWCValues(ps, values[i]);
			}

			private void insertWCValues(PreparedStatement ps, String object) throws SQLException {
				ps.setString(1, object.split(":")[1]);
				ps.setString(2, object.split(":")[0].split("-")[1]);
				ps.setString(3, object.split(":")[0].split("-")[0]);
			}

			public int getBatchSize() {
				return values.length;
			}
		});
	}

	public List<LeadMaster> selectLeadsByLeadSheets(String leadSheetIds) {

		List<LeadMaster> list = null;
		try {
			list = getJdbcTemplate()
			.query("SELECT ilm.LEAD_ID, ilm.GEO_POINT.ToString() GEO_POINT,MARKER_ICON_IMAGE,  ROW_ID, VALIDATION_CODE, RESPONSE_TEXT, ilm.CAMPAIGN_SEQ, ilm.OFFICE_SEQ, ilm.PERSON_ID, IS_ACTIVE, ilm.CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY,RUN_ID,DISPOSITION,LEAD_SHEET_ID,DA,CLLI,ZIP,FULL_ADDR from IMS.IMS_LEAD_LIFECYCLE ilm left outer  join IMS.IMS_TEMP_LEADSHEET TL on  ( TL.LEAD_ID= ilm.LEAD_ID  AND ilm.ROW_ID=TL.ROW_ID ) WHERE   (TL.LEADSHEET_ID IN ("
					+ leadSheetIds + ") or ilm.LEAD_SHEET_ID in (" + leadSheetIds + "))",
					new LeadMasterMapper());
		} catch (Exception e) {
			logger.debug(e);
			logger.error(e);
		}
		return list;
	}

	public String saveOrMergeLeadsheets(final String tempLeadSheetIds, final String savedLeadSheetId,
			final boolean saveAllSheets, final boolean isAssigned, final String campaignSeq, final String officeSeq) {
		try {
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@tempLeadSheetIds", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@selectedSavedLeadSheetId", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@campaignSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@isAssigned", Types.BOOLEAN));
			parameters.add(new SqlParameter("@saveAllSheets", Types.BOOLEAN));

			parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));

			Map map = getJdbcTemplate().call(new CallableStatementCreator() {

				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.saveAndMergeTempLeadSheets(?, ?, ?, ?, ?, ?, ?)}");
					statement.setString(1, tempLeadSheetIds);
					statement.setString(2, savedLeadSheetId);
					statement.setInt(3, new Integer(campaignSeq));
					statement.setInt(4, new Integer(officeSeq));
					statement.setString(6, new Boolean(saveAllSheets).toString());
					statement.setString(5, new Boolean(isAssigned).toString());
					statement.registerOutParameter(7, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			Clob clob = (Clob) map.get("@result");
			return clob.getSubString(1, new Integer("" + clob.length()));
		} catch (Exception e) {
			logger.error(e);
			return "ERROR";
		}

	}

	public void updateDispositionAndNotes(final List<String> dis, final List<String> diDescs, final List<String> notes,
			final List<String> rowIds, final String leadSheetId,String archived) {
		// Check if the lead record exists in LEAD_LIFECYCLE. IF yes then update
		// else call populateLeadInput()
		String sql =  QueryUtils.getQuery().get("UPDATE_LEADLIST_DI_NOTES");
		if(archived.equalsIgnoreCase("Y"))
			sql = QueryUtils.getQuery().get("UPDATE_ARCHIVED_LEADLIST_DI_NOTES");
		getJdbcTemplate().batchUpdate(sql,
				new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Timestamp currTime = new Timestamp(System.currentTimeMillis());
				updateDANotes(ps, dis.get(i), diDescs.get(i), notes.get(i), rowIds.get(i), leadSheetId,
						currTime);
			}

			private void updateDANotes(PreparedStatement ps, String di, String diDesc, String notes,
					String rowId, String sheetId, Timestamp currTime) throws SQLException {
				ps.setString(1, di);
				ps.setString(2, diDesc);
				ps.setString(3, notes);
				ps.setTimestamp(4, currTime);
				ps.setString(5, rowId);
				ps.setString(6, sheetId);
			}

			public int getBatchSize() {
				return dis.size();
			}
		});
	}

	public void updateLeadSheets(final List<String[]> values) {
		try {
			getJdbcTemplate().batchUpdate(QueryUtils.getQuery().get("UPDATE_LEADSHEET"),
					new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					updateLeadsheetValues(ps, values.get(i));
				}

				private void updateLeadsheetValues(PreparedStatement ps, String[] objects) throws SQLException {
					ps.setString(1, objects[0]);
					ps.setString(2, objects[1]);
					ps.setString(3, objects[2]);
					ps.setString(4, objects[3]);
					ps.setString(5, objects[4]);
					ps.setString(6, objects[5]);
					ps.setString(7, objects[6]);
					ps.setString(8, objects[7]);
					ps.setString(9, objects[8]);
				}

				public int getBatchSize() {
					return values.size();
				}
			});
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void updateLeadSheetNotes(final List<String[]> values) {
		try {			
			getJdbcTemplate().batchUpdate(QueryUtils.getQuery().get("INSERT_NOTES_INP"),
					new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					updateLeadsheetValues(ps, values.get(i));
				}

				private void updateLeadsheetValues(PreparedStatement ps, String[] objects) throws SQLException {
					ps.setString(1, objects[8]);
				}

				public int getBatchSize() {
					return values.size();
				}
			});
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void generateLeadsheetsByProximity(final String campaignSeq, final String iclSeq, final String selectedType,
			final String values, final int maxLeads) {
		try {
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@campaign_seq", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@office_seq", Types.LONGVARCHAR));
			// parameters.add(new SqlParameter("@selectedType",
			// Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@zip", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@maxLeadsPerSheet", Types.INTEGER));
			parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {

				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.GETSTREETORDERFORZIP1(?, ?, ?, ?, ?)}");
					statement.setString(1, campaignSeq);
					statement.setString(2, iclSeq);
					// statement.setString(3, selectedType);
					statement.setString(3, values);
					statement.setInt(4, new Integer(maxLeads));
					statement.registerOutParameter(5, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			Clob clob = (Clob) map.get("@result");
			String result = clob.getSubString(1, new Integer("" + clob.length()));
			logger.debug(result);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void generateStreetProximityLeadSheetsByTempSheets(final String tempLeadSheet, final int maxLeadsPerSheet,
			final String officeSeq, final String territorySeq) {
		try {
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@tempSheetId", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@maxLeadsPerSheet", Types.INTEGER));
			parameters.add(new SqlParameter("@territorySeq", Types.LONGVARCHAR));
			parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {

				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.GenerateLeadSheetsByProximity(?, ?, ?, ?, ?)}");
					statement.setString(1, tempLeadSheet);
					statement.setInt(2, new Integer(officeSeq));
					statement.setInt(3, maxLeadsPerSheet);
					statement.setString(4, territorySeq);
					statement.registerOutParameter(5, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			Clob clob = (Clob) map.get("@result");
			String result = clob.getSubString(1, new Integer("" + clob.length()));
			logger.debug(result);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void generateStreetProximityLeadSheetsByTempSheets(final String tempLeadSheet, final int maxLeadsPerSheet,
			final String officeSeq, final String territorySeq, final String territoryType) {
		try {
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@tempSheetId", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@maxLeadsPerSheet", Types.INTEGER));
			parameters.add(new SqlParameter("@territorySeq", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@territoryType", Types.LONGVARCHAR));
			parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {

				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.GenerateLeadSheetsByProximity(?, ?, ?, ?, ?, ?)}");
					statement.setString(1, tempLeadSheet);
					statement.setInt(2, new Integer(officeSeq));
					statement.setInt(3, maxLeadsPerSheet);
					statement.setString(4, territorySeq);
					statement.setString(5, territoryType);
					statement.registerOutParameter(6, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			Clob clob = (Clob) map.get("@result");
			String result = clob.getSubString(1, new Integer("" + clob.length()));
			logger.debug(result);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void generateL2LProximityLeadSheetsByTempSheets(final String tempLeadSheet, final int maxLeadsPerSheet,
			final String officeSeq, final String territorySeq) {
		try {
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@tempSheetId", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@maxLeadsPerSheet", Types.INTEGER));
			parameters.add(new SqlParameter("@territorySeq", Types.LONGVARCHAR));
			parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {

				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.GenerateLeadSheetsByLeadProximity(?, ?, ?, ?, ?)}");
					statement.setString(1, tempLeadSheet);
					statement.setInt(2, new Integer(officeSeq));
					statement.setInt(3, maxLeadsPerSheet);
					statement.setString(4, territorySeq);
					statement.registerOutParameter(5, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			Clob clob = (Clob) map.get("@result");
			String result = clob.getSubString(1, new Integer("" + clob.length()));
			logger.debug(result);
		} catch (Exception e) {
			logger.error(e);
		}
	}	
	public void generateL2LProximityLeadSheetsByTempSheets(final String tempLeadSheet, final int maxLeadsPerSheet,
			final String officeSeq, final String territorySeq, final String territoryType) {
		try {
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@tempSheetId", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@maxLeadsPerSheet", Types.INTEGER));
			parameters.add(new SqlParameter("@territorySeq", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@territoryType", Types.LONGVARCHAR));
			parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {

				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.GenerateLeadSheetsByLeadProximity(?, ?, ?, ?, ?, ?)}");
					statement.setString(1, tempLeadSheet);
					statement.setInt(2, new Integer(officeSeq));
					statement.setInt(3, maxLeadsPerSheet);
					statement.setString(4, territorySeq);
					statement.setString(5,territoryType);
					statement.registerOutParameter(6, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			Clob clob = (Clob) map.get("@result");
			String result = clob.getSubString(1, new Integer("" + clob.length()));
			logger.debug(result);
		} catch (Exception e) {
			logger.error(e);
		}
	}	
	public void populateCRMPrivileges(final List<RoleObject> roleObjects) {
		logger.debug("UPDATE_PRIVILEGES::: " + QueryUtils.getQuery().get("UPDATE_PRIVILEGES"));
		try{
			
			getJdbcTemplate().execute(QueryUtils.getQuery().get("RESET_PRIVILEGES"));
			
			getJdbcTemplate().batchUpdate(QueryUtils.getQuery().get("UPDATE_PRIVILEGES"),
					new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					populateCRMPrivilegesValues(ps, roleObjects.get(i));
				}

				private void populateCRMPrivilegesValues(PreparedStatement ps, RoleObject roleObject) throws SQLException {
					int i=1;
					ps.setString(i++, roleObject.getOwnerIntegrationId());
					ps.setString(i++,  roleObject.getRoleName());
					ps.setString(i++,  roleObject.getOwnerAlias());
					ps.setString(i++,  roleObject.getProfilesString());
					ps.setString(i++,  roleObject.getCampaignsString());
					ps.setString(i++,  roleObject.getOfficesString());
					ps.setString(i++, roleObject.getCreated());
					ps.setString(i++, roleObject.getModified());
					ps.setString(i++, roleObject.getOwnerFirstName());
					ps.setString(i++, roleObject.getOwnerLastName());
					ps.setString(i++, roleObject.getOwnerFullName());
					ps.setString(i++, roleObject.getOwnerEmailAddress());
					ps.setString(i++, roleObject.getOwnerUserSignInId());

					ps.setString(i++, roleObject.getOwnerIntegrationId());
					ps.setString(i++,  roleObject.getRoleName());
					ps.setString(i++,  roleObject.getOwnerAlias());
					ps.setString(i++,  roleObject.getProfilesString());
					ps.setString(i++,  roleObject.getCampaignsString());
					ps.setString(i++,  roleObject.getOfficesString());
					ps.setString(i++, roleObject.getCreated());
					ps.setString(i++, roleObject.getModified());
					ps.setString(i++, roleObject.getOwnerFirstName());
					ps.setString(i++, roleObject.getOwnerLastName());
					ps.setString(i++, roleObject.getOwnerFullName());
					ps.setString(i++, roleObject.getOwnerEmailAddress());
					ps.setString(i++, roleObject.getOwnerUserSignInId());
				}

				public int getBatchSize() {
					return roleObjects.size();
				}
			});
			
			getJdbcTemplate().execute(QueryUtils.getQuery().get("DELETE_PRIVILEGES"));
		}catch(Exception e){
			logger.error(e);
		}

	}
	public void renameTempLeadsheets(final List<String[]> values) {
		try {
			getJdbcTemplate().batchUpdate(QueryUtils.getQuery().get("RENAME_TEMP_LEADSHEET"),
					new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					updateLeadsheetValues(ps, values.get(i));
				}

				private void updateLeadsheetValues(PreparedStatement ps, String[] objects) throws SQLException {
					ps.setString(1, objects[0]);
					ps.setString(2, objects[1]);
				}

				public int getBatchSize() {
					return values.size();
				}
			});
		} catch (Exception e) {
			logger.error(e);
		}
	}
	public void updateLeadsheetPrintStatus(final List<String> values) {
		try {
			getJdbcTemplate().batchUpdate(QueryUtils.getQuery().get("UPDATE_LEADSHEET_PRINTED"),
					new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					updateLeadsheetValues(ps, values.get(i));
				}

				private void updateLeadsheetValues(PreparedStatement ps, String lsId) throws SQLException {
					ps.setString(1, lsId);
				}

				public int getBatchSize() {
					return values.size();
				}
			});
		} catch (Exception e) {
			logger.error(e);
		}
	}

	
	public void generateFiosL2LProximityLeadSheetsByTempSheets(final String tempLeadSheet, final int maxLeadsPerSheet,
		final String officeSeq, final String territorySeq, final String territoryType) {
		try {
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@tempSheetId", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@maxLeadsPerSheet", Types.INTEGER));
			parameters.add(new SqlParameter("@territorySeq", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@territorytype", Types.LONGVARCHAR));
			parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {
	
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.GenerateFiosLeadSheetsByLeadProximity(?, ?, ?, ?, ?, ?)}");
					statement.setString(1, tempLeadSheet);
					statement.setInt(2, new Integer(officeSeq));
					statement.setInt(3, maxLeadsPerSheet);
					statement.setString(4, territorySeq);
					statement.setString(5, territoryType);
					statement.registerOutParameter(6, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			Clob clob = (Clob) map.get("@result");
			String result = clob.getSubString(1, new Integer("" + clob.length()));
			logger.debug(result);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	/**
	 * 
	 */
	public List<B2BReport> getB2BTerritoryDetailsByZip(final String campaignSeq, final String officeSeq){
		List<ListOrderedMap> territoryList = new ArrayList<ListOrderedMap>();
		List<B2BReport> dbResult = null;
		B2BReportRowMapper rowMapper = new B2BReportRowMapper();
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@campaignSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			
			//System.out.println(""+dbResult.size());
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call VerizonMaps.dbo.prB2BICLTerrRTByZip(?, ?)}");
					statement.setString(1, campaignSeq);
					statement.setString(2, officeSeq);
					return statement;
				}
			}, parameters);
			territoryList = (List<ListOrderedMap>) map.get("#result-set-1");
			dbResult = rowMapper.mapRecords(territoryList);
		}catch(Exception e){
			logger.debug(e);
		}
		return dbResult;
	}
	
	public List getB2BHistoryByZip(final String officeSeq, final String zip) {
		List history = new ArrayList();
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@campaignSeq", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@officeSeq", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@zip", Types.LONGVARCHAR));
			//parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));
			
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {
				
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call VrznB2BArchive.dbo.prGetVrznB2BRepHistory(?, ?, ?)}");
					statement.setString(1, "17429706");
					statement.setString(2, officeSeq);
					statement.setString(3, zip);
					//statement.registerOutParameter(4, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			history = (List) map.get("#result-set-1");
		}catch(Exception e){
			logger.debug(e);
		}
		return history;
	}
	
	public List getB2BHistory(final String officeSeq, final String territoryType, final String zip, final String clli) {
		List history = new ArrayList();
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@campaignSeq", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@officeSeq", Types.LONGVARCHAR));
			//if(territoryType.equalsIgnoreCase("zip"))
				parameters.add(new SqlParameter("@zip", Types.LONGVARCHAR));
			//else
				parameters.add(new SqlParameter("@clli", Types.LONGVARCHAR));
			//parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));
			
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {
				
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call VrznB2BArchive.dbo.prGetVrznB2BRepHistory(?, ?, ?, ?)}");
					statement.setString(1, "17429706");
					statement.setString(2, officeSeq);
					if(territoryType.equalsIgnoreCase("zip")){
						statement.setString(3, zip);
						statement.setString(4, null);
					}else{
						statement.setString(3, null);
						statement.setString(4, clli);
					}
					//statement.registerOutParameter(4, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			history = (List) map.get("#result-set-1");
		}catch(Exception e){
			logger.debug(e);
		}
		return history;
	}

	public List getB2BNotesByZip(final String officeSeq, final String zip) {
		List notes = new ArrayList();
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@campaignSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@Zip", Types.LONGVARCHAR));
			
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {
				
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.GETVrznB2BNotes(?, ?, ?)}");
					statement.setString(1, "17429706");
					statement.setString(2, officeSeq);
					statement.setString(3, zip);
					//statement.registerOutParameter(4, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			notes = (List) map.get("#result-set-1");
			logger.debug(notes.get(0));

		}catch(Exception e){
			logger.debug(e);
		}
		return notes;
	}

	public List getFiosHistoryByZip(final String officeSeq, final String zip) {
		List history = new ArrayList();
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@campaignSeq", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@officeSeq", Types.LONGVARCHAR));
//			parameters.add(new SqlParameter("@zip", Types.LONGVARCHAR));
			//parameters.add(new SqlOutParameter("@result", Types.LONGVARCHAR));
			
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {
				
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call VrznB2BArchive.dbo.prGetVrznFIOSRepHistory(?, ?)}");
					statement.setString(1, "17429706");
					statement.setString(2, officeSeq);
					statement.setString(3, zip);
					//statement.registerOutParameter(4, Types.LONGVARCHAR);
					return statement;
				}
			}, parameters);
			history = (List) map.get("#result-set-1");
		}catch(Exception e){
			logger.debug(e);
		}
		return history;
	}
	
	public void updateNotesByZip(final String officeSeq,
		final String zip, final String clli, final String notes) {
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@campaignSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@Notes", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@Zip", Types.LONGVARCHAR));
			
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.prAddICLNotesbyZip(?, ?, ?, ?, ?)}");
					statement.setString(1, "17429706");
					statement.setString(2, clli);
					statement.setString(3, officeSeq);
					statement.setString(4, notes);
					statement.setString(5, zip);
					return statement;
				}
			}, parameters);
			logger.debug("Notes or Distance Saved !!");

		}catch(Exception e){
			logger.debug(e);
		}
	}
	
	public List fetchLeadAssignmentList(final String campaignSeq, final String assignmentType, final String merlinCode, final String clli) {
		List<ListOrderedMap> assignmentList = new ArrayList<ListOrderedMap>();
		List<LeadAssignment> result = null;
		final String viewBy = null;
		
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			
			parameters.add(new SqlParameter("@Campaign_Seq", Types.INTEGER));
			parameters.add(new SqlParameter("@AssignmentType", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@MerlinCode", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@CLLI", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@ViewBy", Types.LONGVARCHAR));
			
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call VerizonMaps.dbo.prGetCLLIOwnerAssignDetails(?,?,?,?,?)}");
					statement.setString(1, campaignSeq);
					statement.setString(2, assignmentType);
					statement.setString(3, merlinCode);
					statement.setString(4, clli);
					statement.setString(5, viewBy);
					return statement;
				}
			}, parameters);
			logger.debug("Notes or Distance Saved !!");
			assignmentList = (List<ListOrderedMap>) map.get("#result-set-1");
			if(campaignSeq.equals("15791795")){
				result = (List<LeadAssignment>) new LeadAssignmentRowMapper(true).mapRecords(assignmentList);
			}else{
				result = (List<LeadAssignment>) new LeadAssignmentRowMapper(false).mapRecords(assignmentList);
			}
		}catch(Exception e){
			System.out.println("Exception : "+e);
		}
		return result;
	}
	
	public void saveNewCLLI(final String campaignSeq, final String assignmentType, final String merlinCode, final String clli, final String lfa, final String user){
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@Campaign_Seq", Types.INTEGER));
			parameters.add(new SqlParameter("@AssignmentType", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@MerlinCode", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@CLLI", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@LFACode", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@User", Types.LONGVARCHAR));
			
			Map map = getJdbcTemplate().call(new CallableStatementCreator() {
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call VerizonMaps.dbo.prAddNewCLLILFAAssignment(?, ?, ?, ?, ?,? )}");
					statement.setString(1, campaignSeq);
					statement.setString(2, assignmentType);
					statement.setString(3, merlinCode);
					statement.setString(4, clli);
					statement.setString(5, lfa);
					statement.setString(6, user);
					return statement;
				}
			}, parameters);
			logger.debug("New CLLI saved !!");
		}catch(Exception e){
			System.out.println("Exception : "+e);
		}
	}
	
	public void saveAssignments(final String campaignSeq, final Map<String, Map<String, HashSet>> map, final String user){
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@Campaign_Seq", Types.INTEGER));
			parameters.add(new SqlParameter("@CLLIList", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@LFACodeList", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@NewOwner", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@User", Types.LONGVARCHAR));
			
			if("15791795".equals(campaignSeq)){
				for(String noKey : map.keySet()){
					Map clliMap = map.get(noKey);
					for(String cKey : (Set<String>)clliMap.keySet()){
						StringBuffer lfa = new StringBuffer();
						HashSet lSet = (HashSet) clliMap.get(cKey);
						if(null != lSet){
							for(Object strLFA : lSet){
								lfa.append(strLFA+",");
							}
							final String fOwner = noKey;
							final String fCLLI = cKey;
							final String fLFA = lfa.toString();//lfa.toString().substring(0, (lfa.toString().length() - 1));
							Map map1 = getJdbcTemplate().call(new CallableStatementCreator() {
								public CallableStatement createCallableStatement(Connection con) throws SQLException {
									CallableStatement statement = con.prepareCall("{call VerizonMaps.dbo.prSaveCLLIOwnerAssignDetails(?, ?, ?, ?, ? )}");
									statement.setString(1, campaignSeq);
									statement.setString(2, fCLLI);
									statement.setString(3, fLFA);
									statement.setString(4, fOwner);
									statement.setString(5, user);
									return statement;
								}
							}, parameters);
							logger.debug("Assignment saved !!");
						}
					}
				}
			}else{
				for(String noKey : map.keySet()){
					StringBuffer clli = new StringBuffer();
					Map clliMap = map.get(noKey);
					for(String cKey : (Set<String>)clliMap.keySet()){
						clli.append(cKey+",");
					}
					final String fOwner = noKey;
					final String fCLLI = clli.toString();//clli.toString().substring(0, (clli.toString().length() - 1));
					Map map1 = getJdbcTemplate().call(new CallableStatementCreator() {
						public CallableStatement createCallableStatement(Connection con) throws SQLException {
							CallableStatement statement = con.prepareCall("{call VerizonMaps.dbo.prSaveCLLIOwnerAssignDetails(?, ?, ?, ?, ? )}");
							statement.setString(1, campaignSeq);
							statement.setString(2, fCLLI);
							statement.setString(3, "NULL");
							statement.setString(4, fOwner);
							statement.setString(5, user);
							return statement;
						}
					}, parameters);
					logger.debug("Assignment saved !!");
				}
			}
		}catch(Exception e){
			System.out.println("Exception : "+e);
		}
	}
	
	public void upsertMergedOwners(final String fromOffice, final String toOffice, final String user, final String active, final String action, final String clientKey){
		try{
			String cKey = clientKey;
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@FromOffice", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@ToOffice", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@User", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@Active", Types.LONGVARCHAR));
			parameters.add(new SqlParameter("@Action", Types.LONGVARCHAR));
			
			
			Map map = getJdbcTemplateByClientId(cKey).call(new CallableStatementCreator() {
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = null;
					if("centurylink".equals(clientKey)){
						statement = con.prepareCall("{call CenturyLink_IMS.dbo.prUpsertMergedOwners(?, ?, ?, ?, ? )}");
					}else{
						statement = con.prepareCall("{call VMaps_Prod.dbo.prUpsertMergedOwners(?, ?, ?, ?, ? )}");
					}
					statement.setString(1, fromOffice);
					statement.setString(2, toOffice);
					statement.setString(3, user);
					statement.setString(4, active);
					statement.setString(5, action);
					return statement;
				}
			}, parameters);
			logger.debug("Upsert Merged Owneres saved !!!");
		}catch(Exception e){
			System.out.println("Exception : "+e);
		}
	}

	/**
	 * 
	 */
	public List<VerizonFiosReport> getFiosTrackerByTerritoryType(final String campaignSeq, final String officeSeq, final String territoryType){
		List<ListOrderedMap> territoryList = new ArrayList<ListOrderedMap>();
		List<VerizonFiosReport> dbResult = null;
		VerizonFiosReportRowMapper rowMapper = new VerizonFiosReportRowMapper();
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@campaignSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			if(territoryType.contains("|")){
				parameters.add(new SqlParameter("@Terrritory1", Types.VARCHAR));
				parameters.add(new SqlParameter("@Terrritory2", Types.VARCHAR));
				String[] params = territoryType.split("\\|");
				params[0] = "LFA".equalsIgnoreCase(params[0])?params[0]+"_CODE":params[0];
				final String param1 = params[0];
				final String param2 = params[1];
				//System.out.println(""+dbResult.size());
				Map map = getJdbcTemplate().call(new CallableStatementCreator() {
					public CallableStatement createCallableStatement(Connection con) throws SQLException {
						CallableStatement statement = con.prepareCall("{call VerizonMaps.dbo.prFIOSTracker(?, ?, ?, ?)}");
						statement.setString(1, campaignSeq);
						statement.setString(2, officeSeq);
						statement.setString(3, param1);
						statement.setString(4, param2);
						return statement;
					}
				}, parameters);	
				territoryList = (List<ListOrderedMap>) map.get("#result-set-1");
			}else{
				//System.out.println(""+dbResult.size());
				final String territory1 = "LFA".equalsIgnoreCase(territoryType)?territoryType+"_CODE":territoryType;
				parameters.add(new SqlParameter("@Terrritory1", Types.VARCHAR));
				Map map = getJdbcTemplate().call(new CallableStatementCreator() {
					public CallableStatement createCallableStatement(Connection con) throws SQLException {
						CallableStatement statement = con.prepareCall("{call VerizonMaps.dbo.prFIOSTracker(?, ?, ?)}");
						logger.debug("Show the statement....." + statement);
						statement.setString(1, campaignSeq);
						statement.setString(2, officeSeq);
						statement.setString(3, territory1);
						return statement;
					}
				}, parameters);
				territoryList = (List<ListOrderedMap>) map.get("#result-set-1");
				logger.debug("Show the territoryList....." + territoryList);
			}			
			dbResult = rowMapper.mapRecords(territoryList,territoryType);
			logger.debug("Show the Result....." + dbResult);
			
		}catch(Exception e){
			logger.error("Exception in getFiosTrackerByTerritoryType() for officeSeq "+officeSeq+ " territoryType "+territoryType,e);
		}
		return dbResult;
	}
	
	public List<B2BReport> getB2BTrackerByTerritoryType(final String campaignSeq, final String officeSeq, final String territoryType){
		List<ListOrderedMap> territoryList = new ArrayList<ListOrderedMap>();
		List<B2BReport> dbResult = null;
		B2BReportRowMapper rowMapper = new B2BReportRowMapper();
		try{
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlParameter("@campaignSeq", Types.INTEGER));
			parameters.add(new SqlParameter("@officeSeq", Types.INTEGER));
			if(territoryType.contains("|")){
				parameters.add(new SqlParameter("@Terrritory1", Types.VARCHAR));
				parameters.add(new SqlParameter("@Terrritory2", Types.VARCHAR));
				String[] params = territoryType.split("\\|");
				params[0] = "LFA".equalsIgnoreCase(params[0])?params[0]+"_CODE":params[0];
				final String param1 = params[0];
				final String param2 = params[1];
				//System.out.println(""+dbResult.size());
				Map map = getJdbcTemplate().call(new CallableStatementCreator() {
					public CallableStatement createCallableStatement(Connection con) throws SQLException {
						CallableStatement statement = con.prepareCall("{call VerizonMaps.dbo.prB2Btracker(?, ?, ?, ?)}");
						statement.setString(1, campaignSeq);
						statement.setString(2, officeSeq);
						statement.setString(3, param1);
						statement.setString(4, param2);
						return statement;
					}
				}, parameters);	
				territoryList = (List<ListOrderedMap>) map.get("#result-set-1");
			}else{
				//System.out.println(""+dbResult.size());
				final String territory1 = "LFA".equalsIgnoreCase(territoryType)?territoryType+"_CODE":territoryType;
				parameters.add(new SqlParameter("@Terrritory1", Types.VARCHAR));
				Map map = getJdbcTemplate().call(new CallableStatementCreator() {
					public CallableStatement createCallableStatement(Connection con) throws SQLException {
						CallableStatement statement = con.prepareCall("{call VerizonMaps.dbo.prB2Btracker(?, ?, ?)}");
						statement.setString(1, campaignSeq);
						statement.setString(2, officeSeq);
						statement.setString(3, territory1);
						return statement;
					}
				}, parameters);
				territoryList = (List<ListOrderedMap>) map.get("#result-set-1");
			}			
			dbResult = rowMapper.mapRecords(territoryList,territoryType);
		}catch(Exception e){
			logger.error("Exception in getFiosTrackerByTerritoryType() for officeSeq "+officeSeq+ " territoryType "+territoryType,e);
		}
		return dbResult;
	}	
}
