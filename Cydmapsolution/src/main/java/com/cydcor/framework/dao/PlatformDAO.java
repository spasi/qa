/**
 * 
 */
package com.cydcor.framework.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.B2BReport;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.GenericModel;
import com.cydcor.framework.model.LeadLifeCycle;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.MerOffice;
import com.cydcor.framework.model.MerOfficeInsert;
import com.cydcor.framework.model.ReportComponent;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.model.Template;
import com.cydcor.framework.model.UserObject;
import com.cydcor.framework.model.VerizonFiosReport;

/**
 * Generic DAO which is used to load all Generic Table References
 * 
 * @author ashwin
 * 
 */

public interface PlatformDAO {

	/**
	 * @param lwbCode
	 * @return
	 */
	public UserObject loadUser(String lwbCode);

	/**
	 * @param sql
	 * @return
	 */
	public DatabaseResult loadResult(String sql);

	/**
	 * @param reportName
	 * @return
	 */
	public ReportComponent loadReport(String reportName);

	/**
	 * Count Number of Rows Returned by this Query
	 * 
	 * @param sql
	 * @return
	 */
	public int countRows(String sql);

	/**
	 * @param sql
	 * @param rowMapper
	 * @return
	 */
	public List<? extends GenericModel> loadRowMapper(String sql, RowMapper rowMapper);
	
	public List<? extends GenericModel> loadRowMapper(String sql, RowMapper rowMapper,String clientKey);

	/**
	 * @param query
	 * @param values
	 */
	public void save(String query, Object[] values);

	/**
	 * @param insertSql
	 * @param values
	 */
	public void populateLeadInput(List<LeadLifeCycle> values);

	/**
	 * @param merOfficeInserts
	 */
	public void populateIclMaster(List<MerOfficeInsert> merOfficeInserts);

	public void populateIclMaster(List<MerOfficeInsert> merOfficeInserts,String clientKey);
	
	/**
	 * @param sql
	 */
	public void execute(String sql);

	/**
	 * @param sql
	 * @param args
	 */
	public void update(String sql, Object[] args);
	
	public void update(String sql, Object[] args,String clientKey);

	/**
	 * @param sql
	 * @param values
	 * @return
	 */
	public DatabaseResult loadResult(String sql, List values);

	/**
	 * @param template
	 */
	public void loadTemplate(Template template);

	/**
	 * 
	 * @param values
	 */
	public void updateLeadLifeCycle(final List<LeadLifeCycle> values);

	public void deleteOfficeSeqs(List<MerOffice> merOffice);

	public Long saveAndGetKey(final String sql, final Object[] args);

	void insertWireCenters(String[] values);

	void insertDAs(String[] values);

	List<LeadMaster> selectLeadsByLeadSheets(String leadSheetIds);

	String saveOrMergeLeadsheets(final String tempLeadSheetIds, final String savedLeadSheetId, final boolean saveAllSheets, final boolean isAssigned,
			final String campaignSeq, final String officeSeq);
	
	public void updateDispositionAndNotes(final List<String> dis, final List<String> diDescs, final List<String> notes, final List<String> rowIds,
			final String leadSheetId, String archived);

	void updateLeadSheets(List<String[]> values);
	
	void updateLeadSheetNotes(List<String[]> values);
	
	public void generateLeadsheetsByProximity(String campaignSeq, String iclSeq, String selectedType, String values,
			int maxLeads);

	public void generateStreetProximityLeadSheetsByTempSheets(String leadSheet, int maxLeadsPerSheet, String officeSeq,
			String territorySeq);
	public void generateStreetProximityLeadSheetsByTempSheets(String leadSheet, int maxLeadsPerSheet, String officeSeq,
			String territorySeq, String territoryType);
	public void generateL2LProximityLeadSheetsByTempSheets(final String tempLeadSheet, final int maxLeadsPerSheet,
			final String officeSeq, final String territorySeq);
	public void generateL2LProximityLeadSheetsByTempSheets(final String tempLeadSheet, final int maxLeadsPerSheet,
			final String officeSeq, final String territorySeq, final String territoryType);
	void populateCRMPrivileges(List<RoleObject> roleObjects);
	public void renameTempLeadsheets(final List<String[]> values) ;
	public void updateLeadsheetPrintStatus(final List<String> values);

	/**
	 * @param tempLeadSheet
	 * @param maxLeadsPerSheet
	 * @param officeSeq
	 * @param territorySeq
	 */
	public void generateFiosL2LProximityLeadSheetsByTempSheets(String tempLeadSheet, int maxLeadsPerSheet, String officeSeq, String territorySeq,
		    String territoryType);
	/**
	 * 
	 * @param campaignSeq
	 * @param officeSeq
	 * @return
	 */
	public List<B2BReport> getB2BTerritoryDetailsByZip(final String campaignSeq, final String officeSeq);
	
	public List getB2BHistoryByZip(final String officeSeq, final String zip);
	public List getB2BHistory(final String officeSeq, final String territoryType, final String zip, final String clli);
	public List getB2BNotesByZip(final String officeSeq, final String zip);
	public void updateNotesByZip(final String officeSeq, final String zip, final String clli, final String notes);
	public List getFiosHistoryByZip(final String officeSeq, final String zip);
	
	public List fetchLeadAssignmentList(final String campaignSeq, final String assignmentType, final String merlinCode, final String clli);
	public void saveNewCLLI(final String campaignSeq, final String assignmentType, final String merlinCode, final String clli, final String lfa, final String user);
	public void saveAssignments(final String campaignSeq, final Map<String, Map<String, HashSet>> map, final String user);
	
	public void upsertMergedOwners(final String fromOffice, final String toOffice, final String user, final String active, final String action, final String clientKey);

	public List<VerizonFiosReport> getFiosTrackerByTerritoryType(
			String campaignSeq, String officeSeq, String territoryType);

	public List<B2BReport> getB2BTrackerByTerritoryType(String campaignSeq,
			String officeSeq, String territoryType);
}
