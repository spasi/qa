package com.cydcor.framework.dao;

import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.GenericModel;

/**
 * 
 * @author ashwin
 * 
 */
public interface LMSPlatformDAO extends PlatformDAO {

	/**
	 * @param sql
	 * @param rowMapper
	 * @return
	 */
	public List<? extends GenericModel> loadLMSRowMapper(String sql,
			RowMapper rowMapper);

	/**
	 * @param leadListIds
	 */
	public void updateLeadMaster(List<Long> leadListIds);

}
