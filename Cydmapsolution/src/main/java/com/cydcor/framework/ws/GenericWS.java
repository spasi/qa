package com.cydcor.framework.ws;

import com.cydcor.framework.service.LMSPlatformService;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.utils.PropertyUtils;
import com.cydcor.framework.utils.ServiceLocator;

public abstract class GenericWS {

	public static PlatformService getPlatformService(){
		return (PlatformService) ServiceLocator.getService("platformService");
	}
	
	public static LMSPlatformService getLMSPlatformService(){
		return (LMSPlatformService) ServiceLocator.getService("lmsPlatformService");
	}
	
	public static PropertyUtils getPropertyUtils(){
		return (PropertyUtils) ServiceLocator.getService("propertyUtils");
	}
	
}
