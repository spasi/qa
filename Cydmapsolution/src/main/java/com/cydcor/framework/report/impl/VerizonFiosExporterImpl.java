
package com.cydcor.framework.report.impl;

import groovyjarjarbackport.java.util.Arrays;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import ar.com.fdvs.dj.core.DJConstants;
import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;

import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.census.Polygon;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.report.Exporter;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.GClientGeocoder;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;
import com.cydcor.framework.utils.URLSigner;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class VerizonFiosExporterImpl implements Exporter {
    protected final Log log = LogFactory.getLog(getClass());

    public VerizonFiosExporterImpl() {
	// TODO Auto-generated constructor stub
    }

    @SuppressWarnings("unchecked")
    public void export(ExportContext exportContext) {
	PreparedStatement statement;
	ResultSet resultSet = null;
	try {
	    String sql = exportContext.getSqlString();
	    try {
		sql = FreeMarkerEngine.getInstance().evaluateString(sql);
	    } catch (Exception e) {
	    }

	    int index = 1;
	    statement = exportContext.getSqlConnecion().prepareStatement(sql);
	    for (Object value : exportContext.getSqlParameters()) {
		statement.setObject(index++, value);
	    }
	    statement.execute();
	    resultSet = statement.getResultSet();
	} catch (SQLException e2) {
	    // TODO Auto-generated catch block
	    log.error(e2);
	}

	Style subTitleStyle = new Style("subTitleStyle");
	subTitleStyle.setTextColor(Color.BLACK);
	subTitleStyle.setHorizontalAlign(HorizontalAlign.LEFT);
	subTitleStyle.setFont(new ar.com.fdvs.dj.domain.constants.Font(12, "ARIAL", true));
	subTitleStyle.setPaddingBottom(10);

	Style titleStyle = new Style("titleStyle");
	// titleStyle.setBackgroundColor(new Color(66, 66, 66));
	// titleStyle.setTextColor(Color.BLUE);
	titleStyle.setFont(new Font(15, "ARIAL", true));
	titleStyle.setHorizontalAlign(HorizontalAlign.LEFT);

	Style columnHeaderStyle = new Style("columnHeaderStyle");
	if (exportContext.getReportParameters().containsKey("CUSTOM_HEADERS")) {
	    columnHeaderStyle.setTextColor(new Color(255, 255, 255));
	    columnHeaderStyle.setBackgroundColor(new Color(0, 128, 128));
	    columnHeaderStyle.setTransparency(Transparency.OPAQUE);
	    columnHeaderStyle.setBorder(Border.PEN_4_POINT);
	    columnHeaderStyle.setBorderColor(new Color(0, 0, 0));
	    columnHeaderStyle.setVerticalAlign(VerticalAlign.MIDDLE);
	    columnHeaderStyle.setFont(new Font(10, "ARIAL", true));
	} else {
	    columnHeaderStyle.setTextColor(new Color(46, 85, 140));
	    columnHeaderStyle.setBackgroundColor(new Color(207, 224, 241));
	    columnHeaderStyle.setTransparency(Transparency.OPAQUE);
	    columnHeaderStyle.setBorder(Border.THIN);
	    columnHeaderStyle.setVerticalAlign(VerticalAlign.MIDDLE);
	    columnHeaderStyle.setFont(new Font(10, "ARIAL", true));
	}
	Style dataStyle = new Style("dataStyle");
	if (exportContext.getReportParameters().containsKey("CUSTOM_HEADERS")) {
	    dataStyle.setBorder(Border.PEN_2_POINT);
	    dataStyle.setFont(new Font(10, "ARIAL", false));
	} else {
	    dataStyle.setBorder(Border.THIN);
	    dataStyle.setFont(new Font(8, "ARIAL", false));

	}
	Style oddDataStyle = new Style("oddDataStyle");
	oddDataStyle.setBackgroundColor(Color.LIGHT_GRAY);
	oddDataStyle.setTransparency(Transparency.OPAQUE);
	oddDataStyle.setTextColor(Color.BLACK);

	FastReportBuilder builder = new FastReportBuilder();

	if (!exportContext.getReportParameters().containsKey("CUSTOM_HEADERS")) {
	    builder.setTitle(exportContext.getReportName()).setTitleStyle(titleStyle);
	}
	// builder.addImageBanner((String)exportContext.getReportParameters().get("titleImage"),
	// 234, 81, ImageBanner.ALIGN_LEFT)
	// .setSubtitle((String)
	// exportContext.getReportParameters().get("subTitle"
	// )).setSubtitleStyle(subTitleStyle)

	if (exportContext.getReportParameters().containsKey("CUSTOM_HEADERS")) {
	    builder.setUseFullPageWidth(false);
	} else {
	    builder.setUseFullPageWidth(true);
	}
	builder.setTopMargin(35);
	builder.setLeftMargin(0);
	builder.setIgnorePagination(true);

	// builder.addAutoText("Report generated on :" + new
	// Date(),AutoText.POSITION_HEADER, AutoText.ALIGNMENT_LEFT);
	List<String> ignorableColumns = null;
	if (exportContext.getReportParameters().containsKey("ignorableColumns"))
	    ignorableColumns = (List<String>) exportContext.getReportParameters().get("ignorableColumns");
	try {
	    ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
	    for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
		if (ignorableColumns == null || !ignorableColumns.contains(resultSetMetaData.getColumnName(i).toUpperCase())) {
		    if ("NOTES".equalsIgnoreCase(resultSetMetaData.getColumnName(i).toUpperCase())) {
			builder.addColumn(resultSetMetaData.getColumnName(i).replaceAll("_", " "), resultSetMetaData.getColumnName(i), String.class.getName(),
				120, dataStyle, columnHeaderStyle).setHeaderHeight(12);

		    } else {
			if (exportContext.getReportParameters().containsKey("columnLengths")) {
			    builder.addColumn(resultSetMetaData.getColumnName(i).replaceAll("_", " "), resultSetMetaData.getColumnName(i),
				    String.class.getName(), (new Integer(((String[]) exportContext.getReportParameters().get("columnLengths"))[i - 1])) * 8,
				    dataStyle, columnHeaderStyle).setHeaderHeight(12);
			} else {
			    builder.addColumn(resultSetMetaData.getColumnName(i).replaceAll("_", " "), resultSetMetaData.getColumnName(i),
				    String.class.getName(), 60, dataStyle, columnHeaderStyle).setHeaderHeight(12);
			}
		    }
		}
	    }
	    Page page = Page.Page_Legal_Landscape();
	    builder.setPageSizeAndOrientation(page);
	    // builder.setOddRowBackgroundStyle(oddDataStyle).setPrintBackgroundOnOddRows(true);
	} catch (ColumnBuilderException e) {
	    // TODO Auto-generated catch block
	    log.error(e);
	} catch (ClassNotFoundException e) {
	    // TODO Auto-generated catch block
	    log.error(e);
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    log.error(e);
	} catch (Exception e) {
	    log.error(e);
	}
	Map params = new HashMap();
	// DynamicReport subreport = buildSubReport(exportContext);
	// params.put("statistics", new ArrayList());
	// if (subreport != null) {
	// try {
	// builder.addConcatenatedReport(subreport, new ClassicLayoutManager(),
	// "directions",
	// DJConstants.DATA_SOURCE_ORIGIN_PARAMETER,
	// DJConstants.DATA_SOURCE_TYPE_COLLECTION);
	// } catch (DJBuilderException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }
	builder.setQuery(exportContext.getSqlString(), DJConstants.QUERY_LANGUAGE_SQL);
	DynamicReport dynamicReport = builder.build();
	JRDataSource jrDataSource = new JRResultSetDataSource(resultSet);
	try {
	    JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(dynamicReport, new ClassicLayoutManager(), resultSet);
	    String a = DynamicJasperHelper.generateJRXML(dynamicReport, new ClassicLayoutManager(), null, "UTF-8");

	    String exportLocation = System.getProperty("java.io.tmpdir");
	    String exportType = exportContext.getReportType();
	    String reportName = exportContext.getReportName();
	    OutputStream exportStream = exportContext.getExportStream();
	    if (Exporter.PDF.equalsIgnoreCase(exportType)) {
		generatePDF(jasperPrint, exportLocation, reportName, exportType, exportStream);
	    } else if (Exporter.XLS.equalsIgnoreCase(exportType)) {
		generateXLS(jasperPrint, "Sheet1", exportLocation, reportName, exportType, exportStream);
	    } else if (Exporter.CSV.equalsIgnoreCase(exportType)) {
		generateCSV(jasperPrint, exportLocation, reportName, exportType, exportStream);
	    } else if (Exporter.HTML.equalsIgnoreCase(exportType)) {
		generateHTML(jasperPrint, exportLocation, reportName, exportType, exportStream);
	    } else if (Exporter.TXT.equalsIgnoreCase(exportType)) {
		generateTXT(jasperPrint, exportLocation, exportContext.getReportName(), exportType, exportStream);
	    } else if (Exporter.XML.equalsIgnoreCase(exportType)) {
		generateXML(jasperPrint, exportLocation, reportName, exportType, exportStream);
	    }
	} catch (JRException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	    log.error(e1.fillInStackTrace());
	}
    }

    /**
     * 
     * @param jasperPrint
     * @param exportLocation
     * @param reportName
     * @param fileExtension
     */
    private void generatePDF(JasperPrint jasperPrint, String exportLocation, String reportName, String fileExtension, OutputStream outputStream) {
	try {
	    // JasperExportManager.exportReportToPdfFile(jasperPrint,
	    // exportLocation + File.separator + reportName + "."
	    // + fileExtension);
	    JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
	} catch (JRException e) {
	    e.printStackTrace();
	    log.error(e.fillInStackTrace());
	}
    }

    /**
     * 
     * @param jasperPrint
     * @param exportLocation
     * @param reportName
     * @param fileExtension
     */
    private void generateHTML(JasperPrint jasperPrint, String exportLocation, String reportName, String fileExtension, OutputStream outputStream) {
	try {
	    // JasperExportManager.exportReportToHtmlFile(jasperPrint,
	    // exportLocation + File.separator + reportName + "."
	    // + fileExtension);
	    JRHtmlExporter htmlExporter = new JRHtmlExporter();
	    htmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	    htmlExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
	    htmlExporter.exportReport();
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error(e.fillInStackTrace());
	}
    }

    /**
     * 
     * @param jasperPrint
     * @param exportLocation
     * @param reportName
     * @param fileExtension
     */
    private void generateXML(JasperPrint jasperPrint, String exportLocation, String reportName, String fileExtension, OutputStream outputStream) {
	try {
	    // JasperExportManager.exportReportToXmlFile(jasperPrint,
	    // exportLocation + File.separator + reportName + "."
	    // + fileExtension, Boolean.TRUE);
	    JasperExportManager.exportReportToXmlStream(jasperPrint, outputStream);

	} catch (JRException e) {
	    e.printStackTrace();
	    log.error(e.fillInStackTrace());
	}
    }

    /**
     * 
     * @param jasperPrint
     * @param sheetName
     * @param exportLocation
     * @param reportName
     * @param fileExtension
     */
    private void generateXLS(JasperPrint jasperPrint, String sheetName, String exportLocation, String reportName, String fileExtension,
	    OutputStream outputStream) {
	// JRXlsExporter exporter = new JRXlsExporter();
	JExcelApiExporter exporter = new JExcelApiExporter();
	exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	exporter.setParameter(JRXlsExporterParameter.CHARACTER_ENCODING, "UTF-8");
	// File destFile = new File(exportLocation, reportName + "."
	// + fileExtension);
	// exporter.setParameter(JRExporterParameter.OUTPUT_FILE, destFile);
	exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputStream);
	exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, Boolean.TRUE);
	exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
	exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
	exporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
	exporter.setParameter(JRXlsExporterParameter.SHEET_NAMES, new String[] { sheetName });
	try {
	    exporter.exportReport();
	} catch (JRException e) {
	    e.printStackTrace();
	    log.error(e.fillInStackTrace());
	}

    }

    /**
     * 
     * @param jasperPrint
     * @param exportLocation
     * @param reportName
     * @param fileExtension
     */
    private void generateTXT(JasperPrint jasperPrint, String exportLocation, String reportName, String fileExtension, OutputStream outputStream) {

	JRTextExporter textexp = new JRTextExporter();

	textexp.setParameter(JRTextExporterParameter.CHARACTER_WIDTH, new Integer(5));
	textexp.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT, new Integer(8));
	textexp.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	// File destFile = new File(exportLocation, reportName + "."
	// + fileExtension);
	// textexp.setParameter(JRExporterParameter.OUTPUT_FILE, destFile);
	textexp.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
	try {
	    textexp.exportReport();
	} catch (JRException e) {
	    e.printStackTrace();
	    log.error(e.fillInStackTrace());
	}

    }

    /**
     * 
     * @param jasperPrint
     * @param exportLocation
     * @param reportName
     * @param fileExtension
     */
    private void generateCSV(JasperPrint jasperPrint, String exportLocation, String reportName, String fileExtension, OutputStream outputStream) {
	JRCsvExporter exporter = new JRCsvExporter();
	exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	// File destFile = new File(exportLocation, reportName + "."
	// + fileExtension);
	// exporter.setParameter(JRExporterParameter.OUTPUT_FILE, destFile);
	exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
	try {
	    exporter.exportReport();
	} catch (JRException e) {
	    e.printStackTrace();
	    log.error(e.fillInStackTrace());
	}

    }

    public String getSqlParameters(Map<String, Object> sqlParams) {
	StringBuffer params = new StringBuffer();
	for (Map.Entry<String, Object> entry : sqlParams.entrySet()) {
	    params.append(entry.getKey()).append("=").append(entry.getValue()).append(" ");
	}
	return params.toString();

    }

    public DynamicReport buildPDFReport(ExportContext exportContext) {
    	
    List<Object> irInputs = new ArrayList<Object>();

	Document document = new Document();
	document.setPageSize(PageSize.A4.rotate());
	document.setMargins(5, 5, 5, 5);
	// step 2
	try {
	    // PdfWriter.getInstance(document, new
	    // FileOutputStream(System.getProperty("java.io.tmpdir") +
	    // File.separator +
	    // exportContext.getReportParameters().get("leadSheetId") +
	    // "Map.pdf"));
	    PdfWriter.getInstance(document, exportContext.getExportStream());
	} catch (DocumentException e) {
	    log.error(e);
	}
	// step 3
	com.lowagie.text.Font font2 = new com.lowagie.text.Font();
	font2 = new com.lowagie.text.Font();
	font2.setFamily("ARIAL");
	font2.setSize(5.5f);
	font2.setColor(new Color(0, 0, 0));
	font2.setStyle(com.lowagie.text.Font.NORMAL);
	HeaderFooter footer = new HeaderFooter(
			new Phrase(CydcorUtils.getProperty("footerMessage"),font2), true);
	footer.setBorder(Rectangle.NO_BORDER);
	System.out.println(footer.getTop());
	System.out.println(footer.getRight());
	System.out.println(footer.getLeft());
	document.setFooter(footer);//2013187 - Legal Message 	
	document.open();
	String[] leadSheetsIds = null;
	if (StringUtils.isNotBlank((String) exportContext.getReportParameters().get("leadSheets"))) {
//	    leadSheetsIds = ((String) exportContext.getReportParameters().get("leadSheets")).split(",");
		leadSheetsIds = ((String) exportContext.getReportParameters().get("leadSheets")).split("!");
	} else {
	    leadSheetsIds = new String[1];
	    leadSheetsIds[0] = (String) exportContext.getReportParameters().get("leadSheetId");
	}
	for (int c = 0; c < leadSheetsIds.length; c++) {
	    
	    irInputs.clear();
	    irInputs.add(leadSheetsIds[c]);
	    // irInputs.add(req.getParameter("repId"));
	    irInputs.add(leadSheetsIds[c]);
	    
	    DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_LEAD_ADDRESSES"), irInputs);
	    DatabaseResult repInfo = null;
	    if (!dbResult.getData().isEmpty() && StringUtils.isNotBlank(dbResult.getData().get(0).get(6).getValue())) {
		List repList = new ArrayList();
		repList.add(dbResult.getData().get(0).get(6).getActualValue());
		repInfo = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_INFO"), repList);
	    }

	    String[] points = new String[dbResult.getData().size()];
	    String[] colors = new String[dbResult.getData().size()];
	    List<MapObject> row = null;
	    for (int i = 0; i < dbResult.getData().size(); i++) {
		row = dbResult.getData().get(i);
		points[i] = row.get(3).getValue().replaceAll("\\(|\\)|POLYGON |MULTI|POINT |LINESTRING ", "").replaceAll(", ", "\n").replaceAll(" ", ",");
		colors[i] = "blue";
		if (StringUtils.isBlank(row.get(5).getValue())) {
		    colors[i] = "yellow";
		}
	    }

	    try {
		irInputs.clear();
		irInputs.add(leadSheetsIds[c]);
		irInputs.add(leadSheetsIds[c]);
		DatabaseResult leadsResult = getPlatformService().loadResult(exportContext.getSqlString(), irInputs);
		int recordsSize = leadsResult.getData().size();
		irInputs.add(leadSheetsIds[c]);
		DatabaseResult repName = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REPS"), irInputs);

		List<String> ignorableColumns = null;
		irInputs.clear();
		if (exportContext.isShowMapInReport())
		    for (List<MapObject> leads : leadsResult.getData()) {
			if (leads.get(38).getValue().equals("Y")) {
			    irInputs.add(leads.get(32).getValue());
			    irInputs.add(leads.get(32).getValue());
			    irInputs.add(leads.get(32).getValue());
			    break;
			}
		    }

		List<String> directions = new ArrayList<String>();
		if (exportContext.isShowMapInReport()) {

		    if (irInputs.size() != 0) {
			dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_FIRST_LEAD_ADDRESSES"), irInputs);

			for (List<MapObject> direcs : dbResult.getData()) {
			    try {
				directions = GClientGeocoder.getV3DrivingDirections(direcs.get(1).getValue(), direcs.get(0).getValue());
			    } catch (HttpException e) {
				// TODO Auto-generated catch block
				log.error(e);
			    } catch (IOException e) {
				// TODO Auto-generated catch block
				log.error(e);
			    }
			    break;
			}
		    }
		}
		if (exportContext.getReportParameters().containsKey("ignorableColumns"))
		    ignorableColumns = (List<String>) exportContext.getReportParameters().get("ignorableColumns");
		boolean showPointColorInReport = new Boolean((String) exportContext.getReportParameters().get("showPointColorInReport"));
		int leadsPerMap = 500;
		//int polyPerMap = 100;
		if(exportContext.isShowMapInReport()){
			leadsPerMap = new Integer((String) exportContext.getReportParameters().get("leadsPerMap"));
			//polyPerMap= new Integer((String) exportContext.getReportParameters().get("polyPerMap"));
		}
		int rowsPerPage = new Integer((String) exportContext.getReportParameters().get("rowsPerPage"));
		boolean firstPageTable = true;
		String clientKey= (String) exportContext.getReportParameters().get("clientKey");
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator.getService( clientKey+"JdbcTemplate");
		PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator.getService(clientKey+"TxManager");
		DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);
		/*Polygon polygon = null;
        List<Polygon> polygons = dbUtil.getLeadSheetPolygonsByIds("SAVED", Arrays.asList(new String[]{leadSheetsIds[c]}));//dbUtil.getBoundaryForSavedLeadSheet(leadSheetsIds[0]);
        if(polygons!=null && !polygons.isEmpty())
        	polygon = polygons.get(0);
        String polyUrlParam="";
        String polyMapUrl="";
        Image img2 = null;
	  
	    if(polygon!=null && polygon.getCoordinates()!=null){
		    String polyCoordinates = polygon.getCoordinates().replaceAll("\\(|\\)|POLYGON |MULTI|POINT |LINESTRING ", "");
		    String polyColor = "blue";
		    
	    polyUrlParam = "&path="+"color:"+polyColor+"|"+"weight:2|"+"fillcolor:"+polyColor+"|";	 
		    
			String[] polyPoints = polyCoordinates.split(",");
			int k=0;
			
			if(polyPoints.length>0){
				for(k=0; k<polyPoints.length-1; k++){						
					polyUrlParam = polyUrlParam + polyPoints[k].trim().split(" ")[1].substring(0, polyPoints[k].trim().split(" ")[1].indexOf(".") + 5) +",";
				polyUrlParam = polyUrlParam + polyPoints[k].trim().split(" ")[0].substring(0, polyPoints[k].trim().split(" ")[0].indexOf(".") + 5) +"|";
			}
			polyUrlParam = polyUrlParam + polyPoints[k].trim().split(" ")[1].substring(0, polyPoints[k].trim().split(" ")[1].indexOf(".") + 5) + ",";
			polyUrlParam = polyUrlParam + polyPoints[k].trim().split(" ")[0].substring(0, polyPoints[k].trim().split(" ")[0].indexOf(".") + 5);

			polyMapUrl = "http://maps.google.com/maps/api/staticmap?client=" + CydcorUtils.getProperty("ClientID")
				+ "&size=" + "750x500" + "&maptype=roadmap" + polyUrlParam + "&sensor=false";
			

			
			try {
			    img2 = Image.getInstance(URLSigner.getInstance().getSignedURL(polyMapUrl));
			    polyMapUrl = "";
			} catch (Exception e) {
			    // TODO Auto-generated catch block
			    log.error("Error getting polygon image : ",e);
			}
			}
    }
*/        
        
        
        
        
        /* String coOrd= polygon.getCoordinatesForKML();
         String[] path= coOrd.split(" ");
         String latlng[]= path[0].split(",");
         
         
         String staticUrlParams2="";
         staticUrlParams2="|";
       
*/         
      
		//if (exportContext.isShowMapInReport()) {
		//	rowsPerPage = leadsPerMap;
		//}
		
		/*
		 * int leadPages = ((recordsSize / rowsPerPage) + (recordsSize %
		 * rowsPerPage > 0 ? 1 : 0)); int mapPages = ((recordsSize /
		 * leadsPerMap) + (recordsSize % leadsPerMap > 0 ? 1 : 0)); int
		 * numberOfPages = leadPages + mapPages;
		 */
		
       

		boolean isFirstPage = true;
		int recordCountAdded = 0;
		int currentPageNum = 1;

		Paragraph header = new Paragraph();
		String headerTest = leadSheetsIds[c] + "               			               			               			"
			+ "               			               			               			" + "             						   	        				  		         Page ";
		for (int i = 0; i < recordsSize; i++) {
		    String urlParams = "";
		    String url = "";
		    String urlParams2 = "";
		    String url2 = "";
		    Image img = null;
            
            
		    // To create map URL
		    if (exportContext.isShowMapInReport()) {
		 
		    	
			for (int j = i; j < recordsSize && j < i + leadsPerMap - 1; j++) {
			    String staticUrlParams = "&markers=";
			   
			    try {
				if (showPointColorInReport) {
				    staticUrlParams = staticUrlParams + "color:" + colors[j] + "|label:" + (char) ((64 + j % 26) + 1) + "|";
				}
				staticUrlParams = staticUrlParams + points[j].split(",")[1].substring(0, points[j].split(",")[1].indexOf(".") + 5) + ","
					+ points[j].split(",")[0].substring(0, points[j].split(",")[0].indexOf(".") + 5);
				
			    } catch (StringIndexOutOfBoundsException e) {
				staticUrlParams = staticUrlParams + points[j].split(",")[1] + "," + points[j].split(",")[0];
			    } catch (ArrayIndexOutOfBoundsException a) {
				continue;
			    } catch (NullPointerException e) {
				continue;
			    }
			    urlParams = urlParams + staticUrlParams;
			   
			}
		    }
		    com.lowagie.text.Font font = new com.lowagie.text.Font();
		    font.setFamily("ARIAL");
		    font.setSize(7f);

		    if (StringUtils.isNotBlank(urlParams)) {
			url = "http://maps.google.com/maps/api/staticmap?client=" + CydcorUtils.getProperty("ClientID") + "&size="
				+ (isFirstPage ? "610x585" : "750x510") + "&maptype=roadmap" + urlParams + "&sensor=false";
			System.out.println("url for Fios ExporterImpl = $$" + url + "$$");//2013187 - Legal Message - changed "610x610" : "750x500" to "610x585" : "750x510"
			try {
			    img = Image.getInstance(URLSigner.getInstance().getSignedURL(url));
			    urlParams = "";
		
			} catch (Exception e) {
			    // TODO Auto-generated catch block
			    log.error(e);
			}
		    }

		    if (isFirstPage) {	
		  /*  	if(img2!=null){
			    header = new Paragraph(headerTest + currentPageNum);
			    header.setSpacingAfter(5f);
			    document.add(header);
			    document.add(new Paragraph(""));

			    img2.setAlignment(Element.ALIGN_CENTER);
			    document.add(img2);
			    currentPageNum = currentPageNum + 1;
			    document.newPage();
			    img2 = null;
	    	}
*/		    	
			isFirstPage = false;
			
		      if (img != null) {
		    	header = new Paragraph(headerTest + currentPageNum);
			    header.setSpacingAfter(2f);
			    document.add(header);
			    document.add(new Paragraph(""));

			    PdfPTable table = new PdfPTable(2);
			   
			    table.setWidthPercentage(96);//2013187 - Legal Message - Reduced from 100 to 96
			   
			    int[] cellWidths = new int[] { 630, 300 };
			    table.setWidths(cellWidths);
			   // table2.setWidths(cellWidths);
			    if (img != null) {
				img.setAlignment(Element.ALIGN_LEFT);
				table.addCell(img);
			    }
			    PdfPTable dirTable = new PdfPTable(1);
			    PdfPCell cell = new PdfPCell(new Paragraph("DIRECTIONS"));
			    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			    cell.setBackgroundColor(new Color(128, 200, 128));
			    dirTable.addCell(cell);
			    String directionsStr = "";
			    int k = 1;
			    for (String direction : directions) {
				directionsStr = k + "  " + direction;
				dirTable.addCell(new Paragraph(directionsStr, font));
				k++;
			    }
			    table.addCell(dirTable);
			    document.add(table);
			    currentPageNum = currentPageNum + 1;
			    document.newPage();
			    /*PdfPTable table2 = new PdfPTable(2);
			    table2.setWidthPercentage(96);
			    if (img2 != null) {
					img2.setAlignment(Element.ALIGN_LEFT);
					table2.addCell(img2);
				    }
			    document.add(table2);
			    currentPageNum = currentPageNum + 1;
			    document.newPage();*/
			}
		    } else {
			if (img != null) {
				firstPageTable = true;
			    header = new Paragraph(headerTest + currentPageNum);
			    header.setSpacingAfter(5f);
			    document.add(header);
			    document.add(new Paragraph(""));

			    img.setAlignment(Element.ALIGN_CENTER);
			    document.add(img);
			    currentPageNum = currentPageNum + 1;
			    document.newPage();
			}
		    }

		
		    // To create Table Data
		    while (leadsPerMap > recordCountAdded && i < recordsSize && exportContext.isShowMapInReport()) { //2013187 - Legal Message-Removed fixed rows per page logic
		    	int recordsCount = 0;
		    	int rowsFirstPage = new Integer((String) exportContext.getReportParameters().get("rowsFirstPage"));
//			int recordsCount = leadsPerMap - recordCountAdded > rowsPerPage ? rowsPerPage : leadsPerMap - recordCountAdded;
//		    int recordsCount = rowsPerPage - recordCountAdded > rowsPerPage ? rowsPerPage : rowsPerPage - recordCountAdded;

//			header = new Paragraph(headerTest + currentPageNum);
//			header.setSpacingAfter(5f);
			if(firstPageTable){
				firstPageTable = false;
				recordsCount = leadsPerMap - recordCountAdded > rowsFirstPage ? rowsFirstPage : leadsPerMap - recordCountAdded;
				header = new Paragraph(headerTest + currentPageNum);
				header.setSpacingAfter(5f);
				document.add(header);
				document.add(new Paragraph(""));
				document.add(createLegendTable(repName, repInfo));
				document.add(createRecordsLegendTable());
				document.add(createPromoRecordsLegendTable());
//				Paragraph emptyParagraph = new Paragraph("");
//				emptyParagraph.setSpacingAfter(5f);
//				document.add(emptyParagraph);//2013187 - Legal Message - Removed extra line space
			}else{
				if(exportContext.isShowMapInReport()){
					recordsCount = leadsPerMap - recordCountAdded > rowsFirstPage ? rowsFirstPage : leadsPerMap - recordCountAdded;
				}else{
					recordsCount = leadsPerMap - recordCountAdded > rowsPerPage ? rowsPerPage : leadsPerMap - recordCountAdded;
				}
				
			}
			recordsCount = leadsPerMap;//2013187 - Legal Message-Removed fixed rows per page logic
			document.add(setHeadersAndData(leadsResult, i, recordsCount, ignorableColumns,
				(String) exportContext.getReportParameters().get("columnWidths")));
			currentPageNum = currentPageNum + 1;
			document.newPage();
			recordCountAdded = recordCountAdded + recordsCount;
			i = i + recordsCount;
		    }
		    if(exportContext.isShowMapInReport()){//2013187 - Legal Message-Removed fixed rows per page logic
		    	i = i - 1;
		    }else{
				header = new Paragraph(headerTest + currentPageNum);
				header.setSpacingAfter(5f);
				document.add(header);
				document.add(new Paragraph(""));
				document.add(createLegendTable(repName, repInfo));
				document.add(createRecordsLegendTable());
				document.add(createPromoRecordsLegendTable());
				document.add(setHeadersAndData(leadsResult, 0, recordsSize, ignorableColumns,
						(String) exportContext.getReportParameters().get("columnWidths")));
					currentPageNum = currentPageNum + 1;
				document.newPage();
					break;
		    }
		    recordCountAdded = 0;
		}
	    } catch (Exception e) {
		e.printStackTrace();
		log.error(e);
	    }
	}
	document.close();
	return null;
    }

    private PdfPTable setHeadersAndData(DatabaseResult dbResult, int startIndex, int rowSize, List<String> ignorableColumns, String columnWidths) {
	String[] widths = columnWidths.split(",");
	int[] cellWidths = new int[widths.length];
	int index = 0;
	for (String w : widths) {
	    if (!StringUtils.isNumeric(w.trim()))
		w = "10";
	    try {
		cellWidths[index] = new Integer(w.trim());
	    } catch (Exception e) {
		cellWidths[index] = 10;
	    }
	    index++;
	}
	PdfPTable pdfPTable = new PdfPTable(dbResult.getData().get(0).size() - ignorableColumns.size());
	pdfPTable.setWidthPercentage(100);	
	pdfPTable.setHeaderRows(1);//2013187 - Legal Message-Removed fixed rows per page logic
	try {
	    pdfPTable.setWidths(cellWidths);
	} catch (DocumentException e) {
	    log.error(e);
	}
	PdfPCell cell = null;
	com.lowagie.text.Font font = new com.lowagie.text.Font();
	font.setFamily("ARIAL");
	font.setSize(6.5f);
	font.setStyle(com.lowagie.text.Font.BOLD);
	font.setColor(new Color(46, 85, 140));
	for (MapObject object : dbResult.getData().get(0)) {
	    Paragraph paragraph = new Paragraph(object.getKey().replaceAll("_", ""), font);
	    cell = new PdfPCell(paragraph);
	    // cell.setFixedHeight(4f);
	    cell.setBackgroundColor(new Color(207, 224, 241));
	    if (!ignorableColumns.contains(object.getKey()))
		pdfPTable.addCell(cell);
	}
	for (int i = startIndex; i < dbResult.getData().size() && i < startIndex + rowSize; i++) {
	    List<MapObject> objects = dbResult.getData().get(i);
	    try { // 2013124 ashish : FD 21 22 23 Bold In Oracle ashish 03-10-2014 - Fios lead sheet changes added VZW column 
			if (objects.get(39) != null && objects.get(38).getValue() != null
					&& ( objects.get(39).getValue().equals("FD21") || objects.get(39).getValue().equals("FD22") || objects.get(39).getValue().equals("FD23"))
					) {
				    font = new com.lowagie.text.Font();
				    font.setFamily("ARIAL");
				    font.setSize(6);
				    font.setColor(new Color(0, 0, 0));
				    font.setStyle(com.lowagie.text.Font.NORMAL);
			}else{
			    font = new com.lowagie.text.Font();
			    font.setFamily("ARIAL");
			    font.setSize(6.5f);
			    font.setColor(new Color(0, 0, 0));
			    font.setStyle(com.lowagie.text.Font.NORMAL);
			}
		} catch (Exception e1) {
			font = new com.lowagie.text.Font();
			font.setFamily("ARIAL");
			font.setSize(6);
			font.setColor(new Color(0, 0, 0));
			font.setStyle(com.lowagie.text.Font.NORMAL);
		}
	    for (MapObject object : objects) {
		try {
			if(object.getKey().equalsIgnoreCase("Cap") && (objects.get(40).getValue().equals("VDSL1") || objects.get(40).getValue().equals("VDSL2")))
			{
				cell = new PdfPCell(new Paragraph(StringEscapeUtils.unescapeHtml(object.getValue()+"*"), font));
			}
			else
			{
		    cell = new PdfPCell(new Paragraph(StringEscapeUtils.unescapeHtml(object.getValue()), font));
			}
		} catch (Exception e) {
		    cell = new PdfPCell(new Paragraph(StringEscapeUtils.unescapeHtml(object.getValue()), font));
		}
		cell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
		cell.setNoWrap(false);

		if (!ignorableColumns.contains(object.getKey()))
		    pdfPTable.addCell(cell);
	    }
	}
	return pdfPTable;
    }

    public PlatformService getPlatformService() {
	return ServiceLocator.getService(PlatformService.class);
    }

    public void concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate) {

	Document document = new Document(new Rectangle(1008, 612));
	try {
	    List<InputStream> pdfs = streamOfPDFFiles;
	    List<PdfReader> readers = new ArrayList<PdfReader>();
	    int totalPages = 0;
	    Iterator<InputStream> iteratorPDFs = pdfs.iterator();

	    // Create Readers for the pdfs.
	    while (iteratorPDFs.hasNext()) {
		InputStream pdf = iteratorPDFs.next();
		PdfReader pdfReader = new PdfReader(pdf);
		readers.add(pdfReader);
		totalPages += pdfReader.getNumberOfPages();
	    }
	    // Create a writer for the outputstream
	    PdfWriter writer = PdfWriter.getInstance(document, outputStream);

	    document.open();
	    BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
	    PdfContentByte cb = writer.getDirectContent(); // Holds the PDF
	    // data

	    PdfImportedPage page;
	    int currentPageNumber = 0;
	    int pageOfCurrentReaderPDF = 0;
	    Iterator<PdfReader> iteratorPDFReader = readers.iterator();

	    // Loop through the PDF files and add to the output.
	    while (iteratorPDFReader.hasNext()) {
		PdfReader pdfReader = iteratorPDFReader.next();

		// Create a new page in the target for each source page.
		while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
		    document.newPage();
		    pageOfCurrentReaderPDF++;
		    currentPageNumber++;
		    page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
		    cb.addTemplate(page, 0, 0);

		    // Code for pagination.
		    if (paginate) {
			cb.beginText();
			cb.setFontAndSize(bf, 9);
			cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "" + currentPageNumber + " of " + totalPages, 520, 5, 0);
			cb.endText();
		    }
		}
		pageOfCurrentReaderPDF = 0;
	    }
	    outputStream.flush();
	    document.close();
	    outputStream.close();
	} catch (Exception e) {
	    log.error(e);
	} finally {
	    if (document.isOpen())
		document.close();
	    try {
		if (outputStream != null)
		    outputStream.close();
	    } catch (IOException ioe) {
		log.error(ioe);
	    }
	}
    }

    public PdfPTable createLegendTable(DatabaseResult repName, DatabaseResult repInfo) {
	String repNameStr = "";
	String repCode = "";
	String lastWorked = "";
	String dateDistributed = "";
	String dateTurnedIn = "";
	if (repInfo != null && repName != null && !repInfo.getData().isEmpty() && !repName.getData().isEmpty()) {
	    repNameStr = repName.getData().get(0).get(1).getValue();
	    repCode = repInfo.getData().get(0).get(0).getValue();
	    lastWorked = repInfo.getData().get(0).get(1).getValue();
	    dateDistributed = repInfo.getData().get(0).get(2).getValue();
	    dateTurnedIn = repInfo.getData().get(0).get(3).getValue();
	}
	com.lowagie.text.Font font = new com.lowagie.text.Font();
	font.setFamily("ARIAL");
	font.setSize(7f);
	int[] cellWidths = new int[] { 20, 20, 20, 20, 20, 24 };
	PdfPTable table = new PdfPTable(cellWidths.length);
	table.setWidthPercentage(100);
	try {
	    table.setWidths(cellWidths);
	    table.setSpacingAfter(3f);
	} catch (DocumentException e) {
	    log.error(e);
	}
	List inputs = new ArrayList();
	inputs.add("VERIZONFIOS");
	DatabaseResult result = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_DISPOSITION_LIST"), inputs);
	if (result.getData().size() % 5 != 0) {
	    addEmptyDispositions(result, 6 - result.getData().size() % 5);
	}
	PdfPCell cell = null;
	for (int i = 0; i < result.getData().size(); i++) {
	    List<MapObject> obj = result.getData().get(i);
	    switch (i + 1) {
	    case 6:
		cell = new PdfPCell(new Paragraph("Rep Name:   " + repNameStr, font));
		table.addCell(cell);
		break;
	    case 11:
		cell = new PdfPCell(new Paragraph("Rep Code:   " + repCode, font));
		table.addCell(cell);
		break;
	    case 16:
		cell = new PdfPCell(new Paragraph("Last Worked:   " + lastWorked, font));
		table.addCell(cell);
		break;
	    case 21:
		cell = new PdfPCell(new Paragraph("Date Distributed:   " + dateDistributed, font));
		table.addCell(cell);
		break;
	    case 26:
		cell = new PdfPCell(new Paragraph("", font));
		table.addCell(cell);
		break;
	    default:
		System.out.println("Def");
	    }
	    try {
		cell = new PdfPCell(new Paragraph(obj.get(0).getValue() + " - " + obj.get(1).getValue(), font));
	    } catch (Exception e) {
		cell = new PdfPCell(new Paragraph("", font));
	    }

	    table.addCell(cell);

	}

	return table;

    }

    private void addEmptyDispositions(DatabaseResult result, int i) {
	for (int j = 0; j < i; j++)
	    result.getData().add(new ArrayList<MapObject>());
    }

    public PdfPTable createRecordsLegendTable() {
	com.lowagie.text.Font font = new com.lowagie.text.Font();
	font.setFamily("ARIAL");
	font.setSize(6f);
	int[] cellWidths = new int[] { 25, 40, 35, 30 };
	PdfPTable table = new PdfPTable(cellWidths.length);
	table.setWidthPercentage(100);
	try {
	    table.setWidths(cellWidths);
	    table.setSpacingAfter(3f);
	} catch (DocumentException e) {
	    log.error(e);
	}
	PdfPCell cell = null;

	font.setStyle(com.lowagie.text.Font.NORMAL);
	cell = new PdfPCell(new Paragraph("In columns: V, TV, Data, iONT, DSL; N= Do not sell,          Y= Sell, U= Upgrade, R= Renewal", font));
	table.addCell(cell);

	cell = new PdfPCell(new Paragraph("ONT dsc: In M=Move, C=Customer Expectation, L=Loss To Competitor, Q=Service Quality, P=Product, U=Unknown", font));
	table.addCell(cell);

	cell = new PdfPCell(new Paragraph("Res sta: Blank (no data available), D=Different Occupant, S=Same Occupant, V=Vacant/Unknown Occupant", font));
	table.addCell(cell);

	cell = new PdfPCell(new Paragraph("C# = iONT Cluster- Each # is a unique set of leads within that state", font));
	table.addCell(cell);
	return table;

    }

    public PdfPTable createPromoRecordsLegendTable() {
	com.lowagie.text.Font font = new com.lowagie.text.Font();
	font.setFamily("ARIAL");
	font.setSize(6f);
	font.setStyle(com.lowagie.text.Font.NORMAL);

	int[] cellWidths = new int[] { 23, 22, 22, 22, 22, 22, 22,22 };
	PdfPTable table = new PdfPTable(cellWidths.length);
	table.setWidthPercentage(100);

	try {
	    table.setWidths(cellWidths);
	    table.setSpacingAfter(3f);
	} catch (DocumentException e) {
	    log.error(e);
	}
		PdfPCell cell = null;
	
		cell = new PdfPCell(new Paragraph("Promo Code Legends:", font));
		// table.addCell(cell);
		
		cell = new PdfPCell(new Paragraph("PC01 : PC01 ", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC02: IONT Same Occupant (waive install)", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC03 : 3M HSI Migrator", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC04 : Regional 3M Bundle Offer", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC05 : Regional Voice Only Loyalty Offer", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC06 : NYC 90-Day OFS Trial", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC07 : 60 Day Free FiOS Ready", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC08 : Brandon FL Triple Play Price for Life", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC09 : 25M + FiOS Standalone About Me", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC10 : Network Evolution - Cluster", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC11 : CA & TX FiOS TV Challenge", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC15 : Network Evolution - Chronic", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC17 :  iONT $5X12", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("SP01 : Contract Ending", font));
		table.addCell(cell);
		
		cell = new PdfPCell(new Paragraph("PC : 19 NrE Seniors", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC20: VzWireless", font));//anaravanekar: 03-10-2014 - FiOS Lead Sheet Changes 
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("PC21: IONT Different occupant", font));
		table.addCell(cell);
	
		// TFS task 4245 code added by sanjay
		cell = new PdfPCell(new Paragraph("PC25: FiOS Platinum Eligible", font));
		table.addCell(cell);
		// TFS task 4245
		
		//code changes by Sanjay added new legend
		cell = new PdfPCell(new Paragraph("Promo: WN - Wireless not eligible for 50% more data", font));
		table.addCell(cell);
		
		cell = new PdfPCell(new Paragraph("Promo: WY - Wireless can get 50% more data", font));
		table.addCell(cell);
		//code changes by Sanjay
		
		// LION : 109 Promo code 
		cell = new PdfPCell(new Paragraph("SP02 - Asked to contacted for service", font));
		table.addCell(cell);
		// LION : 109 Promo code
		
	
		// LION : 202 Promo code 
		cell = new PdfPCell(new Paragraph("PC19- Senior offer to key", font));
		table.addCell(cell);
		
		cell = new PdfPCell(new Paragraph("I - instant internet upgrade eligible", font));
		table.addCell(cell);
		
		// LION : 202 Promo code
		
		cell = new PdfPCell(new Paragraph("", font));
		table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("", font));
		table.addCell(cell);
	
		//cell = new PdfPCell(new Paragraph("", font));
		//table.addCell(cell);
	
		cell = new PdfPCell(new Paragraph("", font));
		table.addCell(cell);
		
		return table;
	}
    
    
}
