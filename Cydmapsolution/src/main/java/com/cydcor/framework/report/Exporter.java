package com.cydcor.framework.report;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import ar.com.fdvs.dj.domain.DynamicReport;

import com.cydcor.framework.report.impl.ExportContext;

/**
 * @author ashwin kumar
 *
 */
public interface Exporter {

    static String PDF = "PDF";
    static String XLS = "XLS";
    static String HTML = "HTML";
    static String CSV = "CSV";
    static String XML = "XML";
    static String TXT = "TXT";

    void export(ExportContext exportContext);

    void concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate);

	/**
	 * @param exportContext
	 * @return
	 */
	DynamicReport buildPDFReport(ExportContext exportContext);
}
