package com.cydcor.framework.scheduler.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.jdbc.core.JdbcTemplate;

import com.cydcor.framework.mail.MailManager;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.PropertyUtils;
import com.cydcor.framework.utils.ServiceLocator;

public class EmailNotificationJob implements StatefulJob {

	private static final transient Log logger = LogFactory.getLog(EmailNotificationJob.class);
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		try {
			logger.debug("Email Notification Job Begin");
			String sql = "SELECT STATUS = CASE WHEN DATEDIFF(mi,(SELECT min(LAST_UPDATED_DATE) "
					+ "FROM IMS.IMS_LEAD_ADDRESSES WHERE IS_ACTIVE = 'L'),max(GETDATE())) >"+ PropertyUtils.getInstance().getProperty().get("GEOCODING_MAX_INACTIVE")
					+" THEN 'INACTIVE' ELSE 'ACTIVE' END FROM IMS.IMS_LEAD_ADDRESSES";
			List<String> apps = new ArrayList<String>();
			//apps.add("TEST");
			JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator.getService("centurylink" + "JdbcTemplate");
			Map resultMap = jdbcTemplate.queryForMap(sql);
		    if (resultMap!=null && "INACTIVE".equals(resultMap.get("STATUS"))){
		    	logger.debug("GEOCODING INACTIVE - CENTURYLINK");
		    	apps.add("CENTURYLINK");
		    }
		    
			jdbcTemplate = (JdbcTemplate) ServiceLocator.getService("verizonfios" + "JdbcTemplate");
			resultMap = jdbcTemplate.queryForMap(sql);
		    if (resultMap!=null && "INACTIVE".equals(resultMap.get("STATUS"))){
		    	logger.debug("GEOCODING INACTIVE - VERIZON ");
		    	apps.add("VERIZON");
		    }
		    
		    if(!apps.isEmpty()){
		    	Map data = new HashMap<String, Object>();
		    	data.put("GEOCODING_MAX_INACTIVE", PropertyUtils.getInstance().getProperty().get("GEOCODING_MAX_INACTIVE"));
		    	data.put("apps", apps);
		    	String to = CydcorUtils.getProperty("MAIL_TO");
		    	String[] receipients = null;
		    	if(StringUtils.isNotBlank(to) && to.contains(",")){
		    		receipients = to.split(",");
		    	}
		    	((MailManager) ServiceLocator.getService("mailManager")).sendMail(CydcorUtils.getProperty("MAIL_FROM"),
		    			receipients, CydcorUtils.getProperty("MAIL_SUBJECT"), CydcorUtils.getProperty("MAIL_TEMPLATE"), data);
		    }
		    
		    logger.debug("Email Notification Job End");
		} catch (Exception e) {
			logger.error(e);
		}
	}

}
