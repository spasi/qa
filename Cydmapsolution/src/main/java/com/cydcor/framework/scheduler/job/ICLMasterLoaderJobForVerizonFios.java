/**
 * 
 */
package com.cydcor.framework.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.cydcor.framework.test.VerizonFiosICLDataLoader;

/**
 * @author ashwin
 * 
 */
public class ICLMasterLoaderJobForVerizonFios implements StatefulJob {

	/*
	 * The implementation may wish to set a result object on the
	 * JobExecutionContext before this method exits.
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		VerizonFiosICLDataLoader.executeProcess();
	}

}
