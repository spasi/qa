package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.B2BZipOffice;

public class B2BZipOfficeRowMapper  implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		B2BZipOffice zipOffice = new B2BZipOffice();
		try {
			//zipOffice.setRowId(rs.getInt("ROWID"));//RowId
			zipOffice.setZip(rs.getString("ZIP"));//ZIP
			zipOffice.setOfficeSeq(rs.getInt("OFFICE_SEQ"));//OFFICE_SEQ
			zipOffice.setFullAddress(rs.getString("FULL_ADDR"));//FULL_ADDR
			//zipOffice.setDistance(rs.getLong("DISTANCE"));//Distance
			zipOffice.setLatitude(null != rs.getString("LATITUDE")?rs.getString("LATITUDE"):null);//LATITUDE
			zipOffice.setLongitude(null != rs.getString("LONGITUDE")?rs.getString("LONGITUDE"):null);//LONGITUDE
			//zipOffice.setUpdatedFlag(rs.getString("LASTUPDATE_FLAG"));//LASTUPDATE_FLAG
			zipOffice.setRetryCount(rs.getInt("RETRY_COUNT"));
		} catch (Exception e) {
			System.out.println("Exception : "+e);
		}
		return zipOffice;
	}
}
