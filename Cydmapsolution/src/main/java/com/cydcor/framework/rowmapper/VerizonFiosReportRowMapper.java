package com.cydcor.framework.rowmapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.VerizonFiosReport;

public class VerizonFiosReportRowMapper  implements RowMapper {

	private static final transient Log logger = LogFactory.getLog(VerizonFiosReportRowMapper.class);

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		VerizonFiosReport reportData = new VerizonFiosReport();
		return reportData;
	}
	
	public List<VerizonFiosReport> mapRecords(List<ListOrderedMap> records, String territoryType) throws ParseException{
		List<VerizonFiosReport> reportList = new LinkedList<VerizonFiosReport>();
		int incr = 1;
		SimpleDateFormat sdfReport = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdfDb =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");		
		TimeZone gmt = TimeZone.getTimeZone("GMT");
		sdfDb.setTimeZone(gmt);
		sdfDb.setLenient(false);
		try{
//			System.out.println("rsize:"+records.size());
			for(ListOrderedMap a : records){
				VerizonFiosReport reportData = new VerizonFiosReport();
				Set keySet = (Set) a.keySet();
				reportData.setRowId(incr);
				System.out.println("incr:"+incr);

				reportData.setImageIcon("bulletRed");

				if(territoryType!=null){
					reportData.setTerritoryType(territoryType);
				}else{
					reportData.setTerritoryType("");
				}
				reportData.setSortSeq((Integer) a.get("SeqNo"));
				keySet.remove("SeqNo");
				
				if(a.get("SellData")!=null)
					reportData.setDataEligible((Integer) a.get("SellData"));
				else
					reportData.setDataEligible(0);
				keySet.remove("SellData");
				
				if(a.get("LastWorked")!=null){
					try{						
						Calendar c = Calendar.getInstance();
						c.setTime((Timestamp)a.get("LastWorked"));
						c.add(Calendar.DATE, 90);  // number of days to add						
						reportData.setDateToBeWorked(sdfReport.format(c.getTime()));
						reportData.setDateToBeWorkedDate(c.getTime());
						c.add(Calendar.DATE, -90);
						reportData.setDatelastWorked(sdfReport.format(c.getTime()));
						reportData.setDatelastWorkedDate(c.getTime());
					}catch(Exception e){						
						logger.error("Error parsing date last worked for tracker", e);
						e.printStackTrace();
					}
				}
				keySet.remove("LastWorked");
				
				if(a.get("DisabledMDU")!=null)
					reportData.setDisabledMdu((Integer) a.get("DisabledMDU"));
				else
					reportData.setDisabledMdu(0);
				keySet.remove("DisabledMDU");
				
				if(a.get("iONTDiscDate")!=null)
					reportData.setDisconnectDateUnderTwoYears((Integer) a.get("iONTDiscDate"));
				else
					reportData.setDisconnectDateUnderTwoYears(0);
				keySet.remove("iONTDiscDate");
				
				if(a.get("DSLMig")!=null)
					reportData.setDslMigration((Integer) a.get("DSLMig"));
				else
					reportData.setDslMigration(0);
				keySet.remove("DSLMig");
				
				if(a.get("ExistingVoice")!=null)
					reportData.setExistingVoiceCustomer((Integer) a.get("ExistingVoice"));
				else
					reportData.setExistingVoiceCustomer(0);
				keySet.remove("ExistingVoice");
				
				if(a.get("iONTDiff")!=null)
					reportData.setIontDifferentCustomers((Integer) a.get("iONTDiff"));
				else
					reportData.setIontDifferentCustomers(0);
				keySet.remove("iONTDiff");
				
				if(a.get("iONTSame")!=null)
					reportData.setIontSameCustomer((Integer) a.get("iONTSame"));
				else
					reportData.setIontSameCustomer(0);
				keySet.remove("iONTSame");
				
				if(a.get("MoreThan30Days")!=null)
					reportData.setLeadsBetweenThirtyOneAndNinetyDays((Integer) a.get("MoreThan30Days"));
				else
					reportData.setLeadsBetweenThirtyOneAndNinetyDays(0);
				keySet.remove("MoreThan30Days");
				
				if(a.get("LessThan30Days")!=null)
					reportData.setLeadsThirtyDaysAndUnder((Integer) a.get("LessThan30Days"));
				else
					reportData.setLeadsThirtyDaysAndUnder(0);
				keySet.remove("LessThan30Days");
				
				if(a.get("MDU")!=null)
					reportData.setMduMduY((Integer) a.get("MDU"));
				else
					reportData.setMduMduY(0);
				keySet.remove("MDU");
				
				reportData.setNotes((String) a.get("NOTES"));
				keySet.remove("NOTES");
				
				if(a.get("OpenSeason")!=null)
					reportData.setOpenSeasonMduY((Integer) a.get("OpenSeason"));
				else
					reportData.setOpenSeasonMduY(0);
				keySet.remove("OpenSeason");

				if(a.get("TSTATUS")!=null){
					reportData.setStatus((String) a.get("TSTATUS"));
					if(reportData.getSortSeq()==3){
						String status = StringUtils.isNotBlank(reportData.getStatus())?sdfReport.format(sdfDb.parse(reportData.getStatus())):"";
//						System.out.println("status="+status);
						reportData.setStatus(status);
					}
				}else{
					reportData.setStatus("");
				}
				keySet.remove("TSTATUS");

				if("ZIP".equalsIgnoreCase(territoryType)){
					if(a.get("ZIP")!=null){
						reportData.setTerritoryName((String) a.get("ZIP"));
						reportData.setZip((String) a.get("ZIP"));
					}else{
						reportData.setTerritoryName("");
						reportData.setZip("");
					}
					keySet.remove("ZIP");
					keySet.remove("ZIP");
				}else if("CLLI".equalsIgnoreCase(territoryType)){
					if(a.get("CLLI")!=null){
						reportData.setTerritoryName((String) a.get("CLLI"));
						reportData.setZip("");
					}else{
						reportData.setTerritoryName("");
						reportData.setZip("");
					}
					keySet.remove("CLLI");
					keySet.remove("CLLI");
				}else if("CITY".equalsIgnoreCase(territoryType)){
					if(a.get("CITY")!=null){
						reportData.setTerritoryName((String) a.get("CITY"));
						reportData.setZip("");
					}else{
						reportData.setTerritoryName("CITY");
						reportData.setZip("");
					}
					keySet.remove("CITY");
					keySet.remove("CITY");
				}
				//Code added by sanjay FIOS tracker 30 Dec
				else if("CLLINameForNotes".equalsIgnoreCase(territoryType)){
					if(a.get("CLLINameForNotes")!=null){
						reportData.setTerritoryName((String) a.get("CLLINameForNotes"));
						reportData.setZip("");
					}else{
						reportData.setTerritoryName("CLLINameForNotes");
						reportData.setZip("");
					}
					keySet.remove("CLLINameForNotes");
					keySet.remove("CLLINameForNotes");
				}
				else if("LFA".equalsIgnoreCase(territoryType)){
					if(a.get("LFA_CODE")!=null){
						reportData.setTerritoryName((String) a.get("LFA_CODE"));
						reportData.setZip("");
					}else{
						reportData.setTerritoryName("");
						reportData.setZip("");
					}
					keySet.remove("LFA_CODE");
					keySet.remove("LFA_CODE");
				}else if(territoryType.contains("|")){
					
					String t1 = "LFA".equalsIgnoreCase(territoryType.split("\\|")[0])?territoryType.split("\\|")[0]+"_CODE":territoryType.split("\\|")[0];
					String t2 = "LFA".equalsIgnoreCase(territoryType.split("\\|")[1])?territoryType.split("\\|")[1]+"_CODE":territoryType.split("\\|")[1];
					if(a.get(t1)!=null && a.get(t2)!=null){
						reportData.setTerritoryName((String) a.get(t1)+" & "+(String) a.get(t2));
						reportData.setTerritoryNameFirst((String) a.get(t1));
						reportData.setTerritoryNameSecond((String) a.get(t2));
						reportData.setZip("");
					}else{
						reportData.setTerritoryName("");
						reportData.setZip("");
					}
					keySet.remove(t1);
					keySet.remove(t1);	
					keySet.remove(t2);
					keySet.remove(t2);	
				}
				
				if(a.get("TOTALS")!=null)
					reportData.setTotal((Integer) a.get("TOTALS"));
				else
					reportData.setTotal(0);
				keySet.remove("TOTALS");
				
				if(a.get("Avail")!=null)
					reportData.setTotalAvailable((Integer) a.get("Avail"));
				else
					reportData.setTotalAvailable(0);
				keySet.remove("Avail");
				
				if(a.get("iONT")!=null)
					reportData.setTotalIont((Integer) a.get("iONT"));
				else
					reportData.setTotalIont(0);
				keySet.remove("iONT");
				
				if(a.get("TVPen")!=null)
					reportData.setTvPenetrationRate((String)a.get("TVPen"));
				else
					reportData.setTvPenetrationRate("0");
				keySet.remove("TVPen");
				
				if(a.get("SellVoiceAndData")!=null)
					reportData.setVoiceAndData((Integer) a.get("SellVoiceAndData"));
				else
					reportData.setVoiceAndData(0);
				keySet.remove("SellVoiceAndData");
				
				if(a.get("SellTriple")!=null)
					reportData.setVoiceDataTv((Integer) a.get("SellTriple"));
				else
					reportData.setVoiceDataTv(0);
				keySet.remove("SellTriple");
				
				if(a.get("Latitude")!=null && StringUtils.isNotBlank((String)a.get("Latitude"))){
					Double lat = Double.parseDouble(((String)a.get("Latitude")).replace(",","")) + 0.000005d;
					reportData.setWcLatitude(lat.toString());
				}
				keySet.remove("Latitude");
				
				if(a.get("Longitude")!=null && StringUtils.isNotBlank((String)a.get("Longitude"))){
					Double lng = Double.parseDouble(((String)a.get("Longitude")).replace(",","")) + +0.000005d;;
					reportData.setWcLongitude(lng.toString());
				}
				keySet.remove("Longitude");			
				
				if(a.get("OffSeq")!=null)
					reportData.setOffice_seq(((Integer) a.get("OffSeq")).toString());	
				else
					reportData.setOffice_seq("");
				keySet.remove("OffSeq");
				
				if(a.get("CLLIForNotes")!=null)
					reportData.setClli((String) a.get("CLLIForNotes"));
				else
					reportData.setClli("");
				keySet.remove("CLLIForNotes");
				
				
				if(a.get("CLLINameForNotes")!=null)
					reportData.setCLLiName((String) a.get("CLLINameForNotes"));
				else
					reportData.setCLLiName("");
				keySet.remove("CLLINameForNotes");
				
				
				
				
				if(a.get("CityForNotes")!=null)
					reportData.setCity((String) a.get("CityForNotes"));
				else
					reportData.setCity("");
				keySet.remove("CityForNotes");
				
				if(a.get("LFAForNotes")!=null)
					reportData.setLfa((String) a.get("LFAForNotes"));
				else
					reportData.setLfa("");
				keySet.remove("LFAForNotes");

				if(a.containsKey("Distance") && a.get("Distance")!=null){
					BigDecimal d = new BigDecimal(((String)a.get("Distance")).replace(",",""));
			    	d = d.setScale(1, BigDecimal.ROUND_HALF_UP);
			    	reportData.setDistance(d.doubleValue());
				}else{
					reportData.setDistance(0d);
				}
				keySet.remove("Distance");
				
				int prevTotal =  a.get("LeadsPreCycle")!=null?(Integer)a.get("LeadsPreCycle"):0;	
				int diffInCount = reportData.getTotal()-prevTotal;
				reportData.setDiffInCount(diffInCount);
				keySet.remove("LeadsPreCycle");
				
				LinkedHashMap<String, Integer> promoList = new LinkedHashMap<String, Integer>();
				for(Object key: keySet.toArray()){
//					System.out.println("key="+key);
					if(a.get((String)key)==null){
						promoList.put((String)key, 0);
					}else{
						promoList.put((String)key, (Integer) a.get((String)key));
					}
				}
				reportData.setPromo(promoList);
				incr++;
				reportList.add(reportData);
			}
		}catch(NullPointerException e){
			e.printStackTrace();
//			System.out.println("vrmapper error:"+e.getMessage());
			logger.error("Exception in VerizonFiosReportRowMapper->mapRecords: ",e);
		}
		return reportList;
	}
}
