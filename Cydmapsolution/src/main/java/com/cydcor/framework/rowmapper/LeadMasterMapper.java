package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.LeadMaster;

public class LeadMasterMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		LeadMaster leadMaster = new LeadMaster();
		try {
			leadMaster.setLeadId(rs.getString("LEAD_ID"));
		} catch (Exception e) {
		}
		
		try {
			leadMaster.setNEW_LEAD(rs.getString("NEW_LEAD"));
		} catch (Exception e) {
		}
		
		
		
		try {
			leadMaster.setGeoPoint(rs.getString("GEO_POINT"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setValidationCode(rs.getString("VALIDATION_CODE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setResponseText(rs.getString("RESPONSE_TEXT"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCampaignSeq(rs.getLong("CAMPAIGN_SEQ"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setOfficeSeq(rs.getLong("OFFICE_SEQ"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setPersonId(rs.getLong("PERSON_ID"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setActive(rs.getString("IS_ACTIVE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCreatedBy(rs.getString("CREATED_BY"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setModifiedDate(rs.getTimestamp("MODIFIED_DATE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setModifiedBy(rs.getString("MODIFIED_BY"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setRunId(rs.getString("RUN_ID"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setDisposition(rs.getString("DISPOSITION"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setDa(rs.getString("DA"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setWireCenter(rs.getString("CLLI"));
		} catch (Exception e) {
		}
		try {
			// For all columns which are not standard and using this mapper
			String leadIcon = rs.getString("MARKER_ICON_IMAGE");
			leadMaster.setImageIcon(leadIcon);
		} catch (Exception e) {
		}
		try {
			leadMaster.setFullAddr(rs.getString("FULL_ADDR"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCity(rs.getString("CITY"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setState(rs.getString("STATE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setZip(rs.getString("ZIP"));

		} catch (Exception e) {
		}
		try {
			leadMaster.setAptNum(rs.getString("APT_NUM"));

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setMdu(rs.getString("APT_IND"));

		} catch (Exception e) {
			// TODO: handle exception
		}

		// LEAD_SHEET_ID
		try {
			leadMaster.setLeadSheetId(rs.getString("LEAD_SHEET_ID"));

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setRowId(rs.getLong("ROW_ID"));

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setDispositionalImage(rs.getString("DISPOSITION_IMAGE"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setFiosDigitalVoiceCapable(rs.getString("ISFIOSDIGITAL_VOICE_CPBL"));
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			leadMaster.setDSLorHSICand(rs.getString("ISDSLORHSI_CAND"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setDSLorHSICurrentCust(rs.getString("ISDSLORHSI_CURRENTCUST"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setFiosDataCand(rs.getString("ISFiOSDATA_CAND"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setFiosTVCand(rs.getString("ISFiOSTV_CAND"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setWirelessCust(rs.getString("ISWIRELESSCUST"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setPromoCode(rs.getString("PROMO_CODE"));
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			leadMaster.setLfaCode(rs.getString("LFA_CODE"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setPropId(rs.getString("PROPERTYID"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setIontOffer(rs.getString("iONT_OFFER"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setVideoGATenure(rs.getString("VideoGATenure"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setIontCluster(rs.getString("IONT_CLUSTER"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setClusterLead(rs.getString("CLUSTER_LEAD"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setiONT(rs.getString("IONT"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			leadMaster.setNode(rs.getString("NODE"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setDslSpeedAvail(rs.getString("DSL_SPEED_AVAIL"));
		} catch (Exception e) {
			// TODO: handle exception
		}try {
			leadMaster.setMdu(rs.getString("MDU"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setPrismNewDate(rs.getString("PRSM_NEW_DATE"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setBlockMdu(rs.getString("BLOCK_MDU"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return leadMaster;
	}
}
