package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.MerICLDetails;

public class MerICLDetailsRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		MerICLDetails merICLDetails = new MerICLDetails();
		merICLDetails.setOffice_seq(rs.getString("OFFICE_SEQ"));
		merICLDetails.setOffice_code(rs.getString("OFFICE_CODE"));
		merICLDetails.setOffice_state(rs.getString("OFFICE_STATE"));
		merICLDetails.setOffice_city(rs.getString("OFFICE_CITY"));
		merICLDetails.setOffice_address1(rs.getString("OFFICE_ADDRESS1"));
		merICLDetails.setOffice_address2(rs.getString("OFFICE_ADDRESS2"));
		merICLDetails.setOffice_zip(rs.getString("OFFICE_ZIP"));
		merICLDetails.setActive(rs.getString("ACTIVE"));
		merICLDetails.setHub_manager(rs.getString("HUB_MANAGER"));
		merICLDetails.setOwner(rs.getString("OWNER"));
		merICLDetails.setPromoting_manager(rs.getString("PROMOTING_MANAGER"));
		merICLDetails.setComments(rs.getString("COMMENTS"));
		merICLDetails.setEmail(rs.getString("EMAIL"));
		merICLDetails.setPhone(rs.getString("PHONE"));
		merICLDetails.setOffice_name(rs.getString("OFFICE_NAME"));
		merICLDetails.setFax(rs.getString("FAX"));
		merICLDetails.setCell(rs.getString("CELL"));
		merICLDetails.setFed_ex(rs.getString("FED_EX"));
		merICLDetails.setStart_date(rs.getTimestamp("START_DATE"));
		merICLDetails.setClose_date(rs.getTimestamp("CLOSE_DATE"));
		merICLDetails.setFein(rs.getString("FEIN"));
		merICLDetails.setHub_manager_seq(rs.getString("HUB_MANAGER_SEQ"));
		merICLDetails.setOffice_update(rs.getString("OFFICE_UPDATE"));
		merICLDetails.setQuarterback(rs.getString("QUARTERBACK"));
		merICLDetails.setHot_jobs(rs.getString("HOT_JOBS"));
		merICLDetails.setClass_id(rs.getString("CLASS_ID"));
		merICLDetails.setWeb_user(rs.getString("WEB_USER"));
		merICLDetails.setLast_update(rs.getTimestamp("LAST_UPDATE"));
		merICLDetails.setLast_update_by(rs.getString("LAST_UPDATE_BY"));
		merICLDetails.setOffice_country(rs.getString("OFFICE_COUNTRY"));
		merICLDetails.setInsurance_exp_date(rs
				.getTimestamp("INSURANCE_EXP_DATE"));
		merICLDetails.setWork_comp_exp_date(rs
				.getTimestamp("WORK_COMP_EXP_DATE"));
		merICLDetails.setPayroll_processor(rs.getString("PAYROLL_PROCESSOR"));
		merICLDetails.setDate_of_birth(rs.getTimestamp("DATE_OF_BIRTH"));
		merICLDetails.setAddress_change(rs.getString("ADDRESS_CHANGE"));
		merICLDetails.setConsult_seq(rs.getString("CONSULT_SEQ"));
		merICLDetails.setConsultant(rs.getString("CONSULTANT"));
		merICLDetails.setClosure_reason_seq(rs.getString("CLOSURE_REASON_SEQ"));
		merICLDetails.setClosed(rs.getString("CLOSED"));
		merICLDetails.setOffice_legal_name(rs.getString("OFFICE_LEGAL_NAME"));
		merICLDetails.setSecondary_email(rs.getString("SECONDARY_EMAIL"));
		merICLDetails.setConsultant_function_call(rs
				.getString("CONSULTANT_FUNCTION_CALL"));
		merICLDetails.setPromoting_mgr_function_call(rs
				.getString("PROMOTING_MGR_FUNCTION_CALL"));
		merICLDetails.setProcess_status(rs.getString("PROCESS_STATUS"));
		merICLDetails.setProcess_date(rs.getTimestamp("PROCESS_DATE"));
		merICLDetails.setError_message(rs.getString("ERROR_MESSAGE"));
		merICLDetails.setRetry_count(rs.getLong("RETRY_COUNT"));
		return merICLDetails;
	}
}