package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.map.ListOrderedMap;
import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.B2BReport;

public class B2BReportRowMapper  implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		B2BReport reportData = new B2BReport();
//		try {
//			//reportData.setCurrentRep(rs.getString(""));//CurrentRep
//			//reportData.setDateToBeWorked(rs.getDate(""));//DateToBeWorked
//			//reportData.setDistance(rs.getInt(""));//Distance
//			reportData.setFiosData(rs.getInt("FIOS Data"));//FiosData
//			reportData.setFiosMigration(rs.getInt("FIOS Migration"));//FiosMigration
//			reportData.setFiosTV(rs.getInt("FIOS TV"));//FiosTV
//			//reportData.setHistory(rs.getString(""));//History
//			reportData.setHsi(rs.getInt("HSI"));//HSI
//			//reportData.setNotes(rs.getString(""));//Notes
//			//reportData.setNumOfSharedLeads(rs.getInt(""));//Number of Shared Leads
//			reportData.setOffice_code(rs.getString("OfficeCode"));//OfficeCode
//			//reportData.setOffice_seq(rs.getInt(""));//OfficeSeq
//			reportData.setProspect(rs.getInt("Prospect"));//Prospect
//			//reportData.setSharedBy(rs.getString(""));//SharedBy
//			reportData.setTotal(rs.getInt("Total"));//Total
//			reportData.setTotalMaxSpeedDnLoadAbove3(rs.getInt("Total DSL Max Speed Download Equal to or Above3"));//TotalMaxSpeedDownloadAbove3
//			reportData.setTotalMaxSpeedDnLoadBelow3(rs.getInt("Total DSL Max Speed Download Below3"));//TotalMaxSpeedDownloadBelow3
//			reportData.setTown(rs.getString("Town"));//Town
//			reportData.setWinback(rs.getInt("Winback"));//Winback
//			reportData.setZip(rs.getString("Zip"));//Zip
//			reportData.setClli(rs.getString("CLLI"));//Zip
//		} catch (Exception e) {
//			System.out.println("Exception : "+e);
//		}
		return reportData;
	}


	public List<B2BReport> mapRecords(List<ListOrderedMap> records, String territoryType){
		List<B2BReport> lReport = new LinkedList<B2BReport>();
		int incr = 1;
		try{
			if(records==null)
				return lReport;
			for(ListOrderedMap a : records){
				B2BReport reportData = new B2BReport();
				reportData.setRowId(incr);
				if(territoryType!=null){
					reportData.setTerritoryType(territoryType);
				}else{
					reportData.setTerritoryType("");
				}
				if("ZIP".equalsIgnoreCase(territoryType)){
					reportData.setTerritoryName(((String) a.get("Zip")).trim());
				}else if("CLLI".equalsIgnoreCase(territoryType)){
					reportData.setTerritoryName(((String) a.get("CLLI")).trim());
				}else if("CITY".equalsIgnoreCase(territoryType)){
					reportData.setTerritoryName(((String) a.get("City")).trim());
				}else if("DA".equalsIgnoreCase(territoryType)){
					reportData.setTerritoryName(((String) a.get("DA")).trim());
				}else if(territoryType.contains("|")){
					String t1 = "LFA".equalsIgnoreCase(territoryType.split("\\|")[0])?territoryType.split("\\|")[0]+"_CODE":territoryType.split("\\|")[0];
					String t2 = "LFA".equalsIgnoreCase(territoryType.split("\\|")[1])?territoryType.split("\\|")[1]+"_CODE":territoryType.split("\\|")[1];
					if(a.get(t1)!=null && a.get(t2)!=null){
						reportData.setTerritoryName(((String) a.get(t1)).trim()+" & "+((String) a.get(t2)).trim());
					}else{
						reportData.setTerritoryName("");
					}
				}
				reportData.setCurrentRep((String) a.get("SalesReps"));//SalesReps
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String tempDateStr = (String) a.get("ClosedDate");
				if(tempDateStr!=null)
				{
				 Date d=sdf.parse(tempDateStr);
				 sdf.applyPattern("MM-dd-yyyy");
				 String newDateString = sdf.format(d);
				 reportData.setClosedDate(newDateString);//ClosedDate
				}
				SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
				String tempDateStr2 = (String) a.get("DatetoBeWorked");
				if(tempDateStr2!=null)
				{
				 Date d1=sdf2.parse(tempDateStr2);
				 sdf2.applyPattern("MM-dd-yyyy");
				 String newDateString2 = sdf.format(d1);
				 reportData.setDateToBeWorked(newDateString2);//DateToBeWorked
				}
				if(null != a.get("Distance")){
					reportData.setDistance((String)a.get("Distance"));//Distance
				}
				reportData.setFiosData((Integer) a.get("FIOSData"));//FiosData
				reportData.setFiosMigration((Integer) a.get("FIOSMigration"));//FiosMigration
				reportData.setFiosTV((Integer) a.get("FIOSTV"));//FiosTV
				//reportData.setHistory(a.get(key));//History
				reportData.setHsi((Integer) a.get("HSI"));//HSI
				reportData.setNotes((String) a.get("Notes"));//Notes
				reportData.setNumOfSharedLeads((Integer) a.get("SharedLeads"));//Number of Shared Leads
				//reportData.setOffice_code(rs.getString("OfficeCode"));//OfficeCode
				//reportData.setOffice_seq(((Integer)a.get("OffSeq")).toString());//OfficeSeq
				reportData.setProspect((Integer) a.get("Prospect"));//Prospect
				reportData.setSharedBy((String) a.get("SharedBy"));//SharedBy
				reportData.setTotal((Integer) a.get("Totals"));//Total
				reportData.setTotalMaxSpeedDnLoadAbove3((Integer) a.get("TotalDSLMaxSpeedDownloadAbove3"));//TotalMaxSpeedDownloadAbove3
				reportData.setTotalMaxSpeedDnLoadBelow3((Integer) a.get("TotalDSLMaxSpeedDownloadBelow3"));//TotalMaxSpeedDownloadBelow3
				if(a.get("Town") != null)
					reportData.setTown((String) a.get("Town"));//Town
				else
					reportData.setTown("");
				reportData.setWinback((Integer) a.get("wb"));//Winback
				if(a.get("Zip") != null)
					reportData.setZip(((String) a.get("Zip")).trim());//Zip
				else
					reportData.setZip("");
				//reportData.setClli(rs.getString("CLLI"));//Zip
				reportData.setCreatedOn((String) a.get("CreatedOn"));
				if(a.get("SortSeq") != null)
				{
				 reportData.setSortSeq((Integer) a.get("SortSeq"));
				}
				reportData.setVoiceOnly((Integer) a.get("VoiceOnly"));
				if(a.get("CLLI") != null)
					reportData.setClli(((String)a.get("CLLI")).trim());//CLLI
				else
					reportData.setClli("");
				if(a.get("City") != null)
					reportData.setCity(((String) a.get("City")).trim());
				else
					reportData.setCity("");
				if(a.get("DA") != null)
					reportData.setDa(((String) a.get("DA")).trim());
				else
					reportData.setDa("");
				
				int prevTotal =  a.get("LeadsPreCycle")!=null?(Integer)a.get("LeadsPreCycle"):0;	
				int diffInCount = reportData.getTotal()-prevTotal;
				reportData.setDiffInCount(diffInCount);
				
				incr = incr++;
				lReport.add(reportData);
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Exception : "+e);
		}
		return lReport;
	}
	
	public List<B2BReport> mapRecords(List<ListOrderedMap> records){
		List<B2BReport> lReport = new LinkedList<B2BReport>();
		int incr = 1;
		try{
			for(ListOrderedMap a : records){
				B2BReport reportData = new B2BReport();
				reportData.setRowId(incr);
				reportData.setCurrentRep((String) a.get("SalesReps"));//SalesReps
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String tempDateStr = (String) a.get("ClosedDate");
				if(tempDateStr!=null)
				{
				 Date d=sdf.parse(tempDateStr);
				 sdf.applyPattern("MM-dd-yyyy");
				 String newDateString = sdf.format(d);
				 reportData.setClosedDate(newDateString);//ClosedDate
				}
				SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
				String tempDateStr2 = (String) a.get("DatetoBeWorked");
				if(tempDateStr2!=null)
				{
				 Date d1=sdf2.parse(tempDateStr2);
				 sdf2.applyPattern("MM-dd-yyyy");
				 String newDateString2 = sdf.format(d1);
				 reportData.setDateToBeWorked(newDateString2);//DateToBeWorked
				}
				if(null != a.get("Distance")){
					reportData.setDistance((String)a.get("Distance"));//Distance
				}
				reportData.setFiosData((Integer) a.get("FIOSData"));//FiosData
				reportData.setFiosMigration((Integer) a.get("FIOSMigration"));//FiosMigration
				reportData.setFiosTV((Integer) a.get("FIOSTV"));//FiosTV
				//reportData.setHistory(a.get(key));//History
				reportData.setHsi((Integer) a.get("HSI"));//HSI
				reportData.setNotes((String) a.get("Notes"));//Notes
				reportData.setNumOfSharedLeads((Integer) a.get("SharedLeads"));//Number of Shared Leads
				//reportData.setOffice_code(rs.getString("OfficeCode"));//OfficeCode
				//reportData.setOffice_seq(((Integer)a.get("OffSeq")).toString());//OfficeSeq
				reportData.setProspect((Integer) a.get("Prospect"));//Prospect
				reportData.setSharedBy((String) a.get("SharedBy"));//SharedBy
				reportData.setTotal((Integer) a.get("Totals"));//Total
				reportData.setTotalMaxSpeedDnLoadAbove3((Integer) a.get("TotalDSLMaxSpeedDownloadAbove3"));//TotalMaxSpeedDownloadAbove3
				reportData.setTotalMaxSpeedDnLoadBelow3((Integer) a.get("TotalDSLMaxSpeedDownloadBelow3"));//TotalMaxSpeedDownloadBelow3
				reportData.setTown((String) a.get("Town"));//Town
				reportData.setWinback((Integer) a.get("wb"));//Winback
				reportData.setZip((String) a.get("Zip"));//Zip
				//reportData.setClli(rs.getString("CLLI"));//Zip
				reportData.setCreatedOn((String) a.get("CreatedOn"));
				reportData.setSortSeq((Integer) a.get("SortSeq"));
				reportData.setVoiceOnly((Integer) a.get("VoiceOnly"));
				reportData.setClli((String)a.get("CLLI"));//CLLI
				
				incr = incr++;
				lReport.add(reportData);
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Exception : "+e);
		}
		return lReport;
	}
	
}
