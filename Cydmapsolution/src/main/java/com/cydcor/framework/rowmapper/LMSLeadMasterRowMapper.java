package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.LMSLeadMaster;

public class LMSLeadMasterRowMapper implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	LMSLeadMaster lMSLeadMaster = new LMSLeadMaster();
	lMSLeadMaster.setRowid(rs.getLong("ROWID"));
	lMSLeadMaster.setCampaign_seq(rs.getLong("CAMPAIGN_SEQ"));
	lMSLeadMaster.setRegion(rs.getString("REGION"));
	lMSLeadMaster.setMarket(rs.getString("MARKET"));
	lMSLeadMaster.setLeadid(rs.getString("LEADID"));
	lMSLeadMaster.setClli(rs.getString("CLLI"));
	lMSLeadMaster.setDa(rs.getString("DA"));
	lMSLeadMaster.setLead_type(rs.getString("LEAD_TYPE"));
	lMSLeadMaster.setLead_status(rs.getString("LEAD_STATUS"));
	lMSLeadMaster.setCust_name(rs.getString("CUST_NAME"));
	lMSLeadMaster.setFull_addr(rs.getString("FULL_ADDR"));
	lMSLeadMaster.setHouse_num(rs.getString("HOUSE_NUM"));
	lMSLeadMaster.setStr_pre(rs.getString("STR_PRE"));
	lMSLeadMaster.setStr_name(rs.getString("STR_NAME"));
	lMSLeadMaster.setStr_type(rs.getString("STR_TYPE"));
	lMSLeadMaster.setStr_post(rs.getString("STR_POST"));
	lMSLeadMaster.setApt_num(rs.getString("APT_NUM"));
	lMSLeadMaster.setCity(rs.getString("CITY"));
	lMSLeadMaster.setState(rs.getString("STATE"));
	lMSLeadMaster.setZip(rs.getString("ZIP"));
	lMSLeadMaster.setZip4(rs.getString("ZIP4"));
	lMSLeadMaster.setGreen_status(rs.getString("GREEN_STATUS"));
	lMSLeadMaster.setCredit_class(rs.getString("CREDIT_CLASS"));
	lMSLeadMaster.setConn_tech(rs.getString("CONN_TECH"));
	lMSLeadMaster.setApt_ind(rs.getString("APT_IND"));
	lMSLeadMaster.setUverse_ind(rs.getString("UVERSE_IND"));
	lMSLeadMaster.setDsl_ind(rs.getString("DSL_IND"));
	lMSLeadMaster.setDish_ind(rs.getString("DISH_IND"));
	lMSLeadMaster.setDisposition_code(rs.getString("DISPOSITION_CODE"));
	lMSLeadMaster.setLead_dob(rs.getTimestamp("LEAD_DOB"));
	lMSLeadMaster.setLead_assigned(rs.getTimestamp("LEAD_ASSIGNED"));
	lMSLeadMaster.setLead_expires(rs.getTimestamp("LEAD_EXPIRES"));
	lMSLeadMaster.setDisposition(rs.getString("DISPOSITION"));
	lMSLeadMaster.setNotes(rs.getString("NOTES"));
	lMSLeadMaster.setLead_status_notes(rs.getString("LEAD_STATUS_NOTES"));
	lMSLeadMaster.setUpload_filename(rs.getString("UPLOAD_FILENAME"));
	lMSLeadMaster.setUpdate_status(rs.getString("UPDATE_STATUS"));
	lMSLeadMaster.setLast_updated(rs.getTimestamp("LAST_UPDATED"));
	lMSLeadMaster.setUpdated_by(rs.getString("UPDATED_BY"));
	lMSLeadMaster.setLead_owner(rs.getString("lead_owner"));
	lMSLeadMaster.setLast_worked(rs.getTimestamp("LAST_WORKED"));
	return lMSLeadMaster;
    }
}