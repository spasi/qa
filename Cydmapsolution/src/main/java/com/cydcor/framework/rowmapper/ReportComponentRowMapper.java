package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.ReportComponent;

/**
 * SELECT
 * LWB_FRM_ENTITY.NAME,LWB_FRM_ENTITY.DESCRIPTION,LWB_FRM_ENTITY.ENTITY_TYPE
 * ,LWB_FRM_ENTITY
 * .ENTITY_VALUE,LWB_FRM_ENTITY.SCHEMA_NAME,LWB_FRM_REPORT.REPORT_NAME
 * ,LWB_FRM_REPORT.DESCRIPTION,LWB_FRM_REPORT.EXPORT_ENABLED,LWB_FRM_REPORT.
 * EXPORT_FORMATS_SUPPORTED
 * ,LWB_FRM_REPORT.STYLESHEET,LWB_FRM_REPORT.OUTPUT_TEMPLATE FROM
 * LWB_FRM_ENTITY,LWB_FRM_REPORT WHERE
 * LWB_FRM_REPORT.REF_ENTITY_NAME=LWB_FRM_ENTITY.NAME AND
 * LWB_FRM_REPORT.IS_ACTIVE='1' AND LWB_FRM_ENTITY.IS_ACTIVE='1' This Class
 * Loads ReportComponet
 * 
 * @author ashwin
 * 
 */
public class ReportComponentRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReportComponent reportComponent = new ReportComponent();
		reportComponent.setEntityName(rs.getString("LWB_FRM_ENTITY.NAME"));
		reportComponent.setEntityDescription(rs
				.getString("LWB_FRM_ENTITY.DESCRIPTION"));

		reportComponent.setEntityType(rs
				.getString("LWB_FRM_ENTITY.ENTITY_TYPE"));
		reportComponent.setEntityValue(rs
				.getString("LWB_FRM_ENTITY.ENTITY_VALUE"));
		reportComponent.setSchemaName(rs
				.getString("LWB_FRM_ENTITY.SCHEMA_NAME"));

		reportComponent.setReportName(rs
				.getString("LWB_FRM_REPORT.REPORT_NAME"));
		reportComponent.setReportDescription(rs
				.getString("LWB_FRM_REPORT.DESCRIPTION"));
		reportComponent.setExportEnabled(rs
				.getBoolean("LWB_FRM_REPORT.EXPORT_ENABLED"));
		reportComponent.setSupportedFormats(rs
				.getString("LWB_FRM_REPORT.EXPORT_FORMATS_SUPPORTED"));
		reportComponent.setStyleSheetName(rs
				.getString("LWB_FRM_REPORT.STYLESHEET"));
		reportComponent.setTemplateName(rs
				.getString("LWB_FRM_REPORT.OUTPUT_TEMPLATE"));

		return reportComponent;
	}

}