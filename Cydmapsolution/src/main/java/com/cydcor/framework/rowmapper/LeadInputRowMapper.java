package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.cydcor.framework.model.LeadInput;

public class LeadInputRowMapper extends AbstractRowMapper{

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		LeadInput leadInput = new LeadInput();
		
		leadInput.setRowId(rs.getLong("ROWID"));
		leadInput.setCampaignSeq(rs.getLong("CAMPAIGN_SEQ"));
		leadInput.setRegion(rs.getString("REGION"));
		leadInput.setMarket(rs.getString("MARKET"));
		leadInput.setInputLayout(rs.getString("INPUT_LAYOUT"));
		leadInput.setLeadId(rs.getString("LEADID"));
		leadInput.setClli(rs.getString("CLLI"));
		leadInput.setDa(rs.getString("DA"));
		leadInput.setLeadType(rs.getString("LEAD_TYPE"));
		leadInput.setCustName(rs.getString("CUST_NAME"));
		
		leadInput.setHouseNum(rs.getString("HOUSE_NUM"));
		leadInput.setStrPre(rs.getString("STR_PRE"));
		leadInput.setStrName(rs.getString("STR_NAME"));
		leadInput.setStrType(rs.getString("STR_TYPE"));
		leadInput.setStrPost(rs.getString("STR_POST"));
		leadInput.setAptNum(rs.getString("APT_NUM"));
		leadInput.setZip4(rs.getString("ZIP4"));
		
		leadInput.setFullAddr(rs.getString("FULL_ADDR"));
		leadInput.setCity(rs.getString("CITY"));
		leadInput.setState(rs.getString("STATE"));
		leadInput.setZip(rs.getString("ZIP"));
		leadInput.setDisposition(rs.getString("DISPOSITION"));

		leadInput.setGreenStatus(rs.getString("GREEN_STATUS"));
		leadInput.setCreditClass(rs.getString("CREDIT_CLASS"));
		leadInput.setConnTech(rs.getString("CONN_TECH"));
		leadInput.setAptInd(rs.getString("APT_IND"));
		leadInput.setUverseInd(rs.getString("UVERSE_IND"));
		leadInput.setDslInd(rs.getString("DSL_IND"));
		leadInput.setDishInd(rs.getString("DISH_IND"));
		leadInput.setLeadDOB(rs.getTimestamp("LEAD_DOB"));
		leadInput.setLeadAssigned(rs.getTimestamp("LEAD_ASSIGNED"));
		leadInput.setLeadExpires(rs.getTimestamp("LEAD_EXPIRES"));

		leadInput.setNotes(rs.getString("NOTES"));
		leadInput.setLeadStatus(rs.getString("LEAD_STATUS"));
		leadInput.setUploadFileName(rs.getString("UPLOAD_FILENAME"));
		leadInput.setProcessed(rs.getString("PROCESSED"));
		leadInput.setProcessedDate(rs.getTimestamp("Processed_Date"));
		leadInput.setLeadOwner(rs.getString("LEAD_OWNER"));
		
		return leadInput;
	}

}
