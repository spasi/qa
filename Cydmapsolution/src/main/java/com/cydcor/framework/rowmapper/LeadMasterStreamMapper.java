package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.census.MarkerImage;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.utils.FreeMarkerEngine;

public class LeadMasterStreamMapper implements RowMapper {
	private static final transient Log logger = LogFactory.getLog(LeadMasterStreamMapper.class);

	public LeadMasterStreamMapper(ServletOutputStream outputStream, int count, List<String> dispImgs,
			boolean showAssignedLeads, String clientKey) {
		this.outputStream = outputStream;
		this.recordCount = count;
		this.dispImgs = dispImgs;
		this.showAssignedLeads = showAssignedLeads;
		this.clientKey = clientKey;
	}

	private Set<String> stylesAdded = new HashSet<String>();
	private Map<String, Integer> zips = new HashMap<String, Integer>();
	private Map<String, Integer> wcs = new HashMap<String, Integer>();
	private Map<String, Integer> das = new HashMap<String, Integer>();
	private int recordCount = 0;
	private List<String> dispImgs;
	private ServletOutputStream outputStream;
	private boolean showAssignedLeads;
	private String clientKey;
	Map<String, Object> paramMap = new HashMap<String, Object>();
	LeadMaster leadMaster = new LeadMaster();

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		paramMap.clear();

		try {
			leadMaster.setLeadId(StringEscapeUtils.escapeXml(rs.getString("LEAD_ID")));
		} catch (Exception e) {
		}
		try {
			leadMaster.setGeoPoint(rs.getString("GEO_POINT"));
		} catch (Exception e) {
		}
		if (StringUtils.isBlank(leadMaster.getGeoPoint()))
			return leadMaster;
		try {
			leadMaster.setValidationCode(rs.getString("VALIDATION_CODE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setResponseText(rs.getString("RESPONSE_TEXT"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCampaignSeq(rs.getLong("CAMPAIGN_SEQ"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setOfficeSeq(rs.getLong("OFFICE_SEQ"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setPersonId(rs.getLong("PERSON_ID"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setActive(rs.getString("IS_ACTIVE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCreatedBy(rs.getString("CREATED_BY"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setModifiedDate(rs.getTimestamp("MODIFIED_DATE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setModifiedBy(rs.getString("MODIFIED_BY"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setRunId(rs.getString("RUN_ID"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setDisposition(rs.getString("DISPOSITION"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setDa(rs.getString("DA"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setWireCenter(rs.getString("CLLI"));
		} catch (Exception e) {
		}
		try {
			// For all columns which are not standard and using this mapper
			String leadIcon = rs.getString("MARKER_ICON_IMAGE");
			leadMaster.setImageIcon(leadIcon);
		} catch (Exception e) {
		}
		try {
			leadMaster.setFullAddr(StringEscapeUtils.escapeXml(rs.getString("FULL_ADDR")));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCity(rs.getString("CITY"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setState(rs.getString("STATE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setZip(rs.getString("ZIP"));

		} catch (Exception e) {
		}
		try {
			leadMaster.setLfaCode(rs.getString("LFA_CODE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setAptNum(rs.getString("APT_NUM"));

		} catch (Exception e) {
			// TODO: handle exception
		}

		// LEAD_SHEET_ID
		try {
			leadMaster.setLeadSheetId(rs.getString("LEAD_SHEET_ID"));

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setRowId(rs.getLong("ROW_ID"));

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setFiosDigitalVoiceCapable("ISFIOSDIGITAL_VOICE_CPBL");
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			leadMaster.setDSLorHSICand("ISDSLORHSI_CAND");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setDSLorHSICurrentCust("ISDSLORHSI_CURRENTCUST");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setFiosDataCand("ISFiOSDATA_CAND");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setFiosTVCand("ISFiOSTV_CAND");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setWirelessCust("ISWIRELESSCUST");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setDispositionalImage(rs.getString("DISPOSITION_IMAGE"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setDispositionCode(rs.getString("DISPOSITION_CODE"));
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (dispImgs.size() < 1 && StringUtils.isBlank(leadMaster.getImageIcon())) {
			leadMaster.setImageIcon("redDot");
		} else {
			// Assign Lead Image ICon Based on Rule Template here
			try {
				String imageName = null;
				if (dispImgs.size() > 0) {
					leadMaster.setImageIcon("redDot");
				}
				String imageIcon = leadMaster.getImageIcon();
				if (dispImgs != null && dispImgs.contains(leadMaster.getDispositionalImage()))
					imageIcon = leadMaster.getDispositionalImage();
				int lastIndexOfSlash = imageIcon.lastIndexOf("/");
				int lastIndexOfDot = imageIcon.lastIndexOf(".");
				imageName = imageIcon.substring(lastIndexOfSlash + 1, lastIndexOfDot);
				MarkerImage markerImage = new MarkerImage();
				markerImage.setStyleId(imageName);
				markerImage.setImageUrl(imageIcon);
				if (!stylesAdded.contains(imageName)) {
					paramMap.put("imageLink", markerImage);
					stylesAdded.add(imageName);
				}
				leadMaster.setImageIcon(imageName);

			} catch (Exception e) {
				leadMaster.setImageIcon("redDot");
			}
		}

		if (StringUtils.isNotBlank(leadMaster.getZip())) {
			if (!zips.containsKey(leadMaster.getZip()))
				zips.put(leadMaster.getZip(), 0);
			zips.put(leadMaster.getZip(), zips.get(leadMaster.getZip()) + 1);
		}
		if (StringUtils.isNotBlank(leadMaster.getDa())) {
			if (!das.containsKey(leadMaster.getDa() + "-" + leadMaster.getWireCenter()))
				das.put(leadMaster.getDa() + "-" + leadMaster.getWireCenter(), 0);
			das.put(leadMaster.getDa() + "-" + leadMaster.getWireCenter(), das.get(leadMaster.getDa() + "-"
					+ leadMaster.getWireCenter()) + 1);
			if (!wcs.containsKey(leadMaster.getWireCenter()))
				wcs.put(leadMaster.getWireCenter(), 0);
			wcs.put(leadMaster.getWireCenter(), wcs.get(leadMaster.getWireCenter()) + 1);

		}
		if (StringUtils.isNotBlank(leadMaster.getWireCenter()) && StringUtils.isBlank(leadMaster.getDa())) {
			if (!wcs.containsKey(leadMaster.getWireCenter()))
				wcs.put(leadMaster.getWireCenter(), 0);
			wcs.put(leadMaster.getWireCenter(), wcs.get(leadMaster.getWireCenter()) + 1);
		}

		if (recordCount == rowNum + 1) {

			StringBuffer zipsString = new StringBuffer();
			StringBuffer wcsString = new StringBuffer();
			StringBuffer dasString = new StringBuffer();

			try {
				for (Map.Entry<String, Integer> zip : zips.entrySet()) {
					zipsString.append(", '").append(zip.getKey()).append("'$").append(zip.getValue());
				}
			} catch (Exception e) {
				logger.error("Exception in Processing Zips :", e);
				// System.out.println("Exception in Processing Zips :");
				// e.printStackTrace();
			}

			try {
				for (Map.Entry<String, Integer> wc : wcs.entrySet()) {
					wcsString.append(", '").append(wc.getKey()).append("'$").append(wc.getValue());
				}
			} catch (Exception e) {
				logger.error("Exception in Processing Wcs :", e);
				// System.out.println("Exception in Processing Wcs :");
				// e.printStackTrace();
			}

			try {
				for (Map.Entry<String, Integer> da : das.entrySet()) {
					dasString.append(", '").append(da.getKey()).append("'$").append(da.getValue());
				}
			} catch (Exception e) {
				logger.error("Exception in Processing Das :", e);
				// System.out.println("Exception in Processing Das :");
				// e.printStackTrace();
			}
			paramMap.put("lastRow", true);
			if (StringUtils.isNotBlank(zipsString.toString()))
				paramMap.put("zips", "{" + zipsString.substring(1) + "}");
			if (StringUtils.isNotBlank(dasString.toString()))
				paramMap.put("das", "{" + dasString.substring(1) + "}");
			if (StringUtils.isNotBlank(wcsString.toString()))
				paramMap.put("wcs", "{" + wcsString.substring(1) + "}");

		}
		if (showAssignedLeads)
			addToResponse(outputStream, leadMaster);
		else if (StringUtils.isBlank(leadMaster.getLeadSheetId()))
			addToResponse(outputStream, leadMaster);

		return leadMaster;
	}

	private void addToResponse(ServletOutputStream outputStream, LeadMaster leadMaster2) {
		try {
			paramMap.put("lead", leadMaster);
			outputStream.print(FreeMarkerEngine.getInstance().evaluateTemplate("StreamGeometry.xml", paramMap));
		} catch (Exception e) {
			logger.error("error in addToResponse:",e);
			// TODO: handle exception
			e.printStackTrace();
		}

	}
}
