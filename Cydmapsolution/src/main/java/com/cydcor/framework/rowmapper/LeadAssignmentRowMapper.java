package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.map.ListOrderedMap;
import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.B2BReport;
import com.cydcor.framework.model.LeadAssignment;

public class LeadAssignmentRowMapper  implements RowMapper  {
	
	public boolean hasLFA = false;
	
	public LeadAssignmentRowMapper(boolean b) {
		this.hasLFA = b;
	}

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		LeadAssignment la = new LeadAssignment();
		//la.setRowId(rs.getInt(""));
		la.setClli(rs.getString("clli"));
		la.setCurrent_Owner(rs.getString("current_owner"));
		if(hasLFA){
			la.setLfa(rs.getString("lfa"));
		}
		//la.setNew_Owner(rs.getString(""));
		//la.setOffice_Code(rs.getString(""));
		//la.setOffice_Name(rs.getString(""));
		//la.setOffice_Seq(rs.getString(""));
		//la.setCampaign_Seq(rs.getString(""));
		
		return la;
	}
	
	public Object mapRecords(List<ListOrderedMap> records) throws SQLException {
		List<LeadAssignment> leadAssignList = new LinkedList<LeadAssignment>();
		int incr = 1;
		try{
			for(ListOrderedMap a : records){
				LeadAssignment la = new LeadAssignment();
				la.setClli((String)a.get("CLLI"));
				la.setCurrent_Owner((String)a.get("MerlinCode"));
				if(hasLFA){
					la.setLfa((String)a.get("LFACode"));
				}
				
				incr = incr++;
				leadAssignList.add(la);
			}
		}catch(Exception e){
			System.out.println("Exception : "+e);
		}
		return leadAssignList;
	}
}
