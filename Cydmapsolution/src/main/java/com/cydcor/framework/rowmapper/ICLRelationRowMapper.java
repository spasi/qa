package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.ICLRelation;

public class ICLRelationRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ICLRelation iCLRelation = new ICLRelation();
		iCLRelation.setCampaign_seq(rs.getLong("CAMPAIGN_SEQ"));
		iCLRelation.setOffice_seq(rs.getLong("OFFICE_SEQ"));
		iCLRelation.setCampaign_office_start_date(rs
				.getTimestamp("CAMPAIGN_OFFICE_START_DATE"));
		iCLRelation.setCampaign_office_end_date(rs
				.getTimestamp("CAMPAIGN_OFFICE_END_DATE"));
		iCLRelation.setActive(rs.getString("ACTIVE"));
		iCLRelation.setProcess_status(rs.getString("PROCESS_STATUS"));
		iCLRelation.setProcess_date(rs.getTimestamp("PROCESS_DATE"));
		iCLRelation.setError_msg(rs.getString("ERROR_MSG"));
		iCLRelation.setRetry_count(rs.getString("RETRY_COUNT"));
		return iCLRelation;
	}
}