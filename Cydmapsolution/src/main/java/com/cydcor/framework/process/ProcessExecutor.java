/**
 * 
 */
package com.cydcor.framework.process;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import bsh.EvalError;
import bsh.Interpreter;

import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.task.LeadAssignByCountTask;
import com.cydcor.framework.task.LeadAssignByDistanceTask;
import com.cydcor.framework.task.LeadAssignByTerritoryTask;
import com.cydcor.framework.task.Task;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * @author ashwin
 * 
 */
public class ProcessExecutor {
	private static final transient Log logger = LogFactory.getLog(ProcessExecutor.class);
	private PlatformService platformService = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ProcessEngine processEngine = new LeadAssignmentEngine();

		// Replace with bean shell code to add tasks dynamically

		Task task = new LeadAssignByTerritoryTask();

		processEngine.addTask(task);

		task = new LeadAssignByDistanceTask();
		processEngine.addTask(task);

		task = new LeadAssignByCountTask();
		processEngine.addTask(task);

		ProcessContext processContext = new ProcessContext();

		processEngine.startProcess(processContext);
	}

	/**
	 * @param processContext
	 *            TODO
	 * 
	 */
	public static void executeAssignmentProcess( ProcessContext processContext) {
		DatabaseResult dbResult = null;
		ProcessEngine processEngine = new LeadAssignmentEngine();

		loadTemplateParams((String)processContext.getParameters().get("templateName"), processContext);

		// Task task = new LeadAssignByTerritoryTask();
		// processEngine.addTask(task);

		Task task = null;

		if (StringUtils.isNotBlank(processContext.getProcessTaskName())) {
			logger.debug("processContext.getProcessTaskName():"+processContext.getProcessTaskName());
			task = createDynamicTask(task, processContext.getProcessTaskName());
		} else {
			logger.debug("processContext.getProcessTaskName() is blank");
			task = createDynamicTask(task, "GetTaskListTask_en_US.bsh");
		}
		logger.debug("task:"+task);
		processEngine.addTask(task);
		processEngine.startProcess(processContext);

	}

	/**
	 * @param templateName
	 * @param processContext
	 */
	private static void loadTemplateParams(String templateName,
			ProcessContext processContext) {
	}

	/**
	 * @param task
	 * @return
	 */
	private static Task createDynamicTask(Task task, String template) {
		String templatePath = CydcorUtils.class.getClassLoader().getResource(
				"ftl/tasks/" + template).getPath();

		try {
			templatePath = templatePath.replaceAll("%20", " ");
			task = (Task) new Interpreter().source(templatePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		} catch (EvalError e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return task;
	}

	/**
	 * @return
	 */
	protected static PlatformService getPlatformService() {
		return ServiceLocator.getService(PlatformService.class);
	}

}
