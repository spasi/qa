/**
 * 
 */
package com.cydcor.framework.process;

import com.cydcor.framework.task.Task;

/**
 * @author ashwin
 * 
 */
public interface ProcessEngine {
	
	public void addTask(Task task);
	
	public void deleteTask(Task task);

	public void startProcess(ProcessContext context);

	public void stopProcess(ProcessContext context);

	public void addOrUpdateProcess(ProcessContext context);

	public void deleteProcess(ProcessContext context);

}
