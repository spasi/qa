/**
 * 
 */
package com.cydcor.framework.process;

import java.util.ArrayList;
import java.util.List;

import com.cydcor.framework.task.Task;

/**
 * @author ashwin
 * 
 */
public abstract class AbstractProcessEngine implements ProcessEngine {

	protected List<Task> taskList = new ArrayList<Task>();

	/**
	 * @param task
	 */
	public void addTask(Task task) {
		if (task == null)
			return;
		taskList.add(task);
	}

	/**
	 * @param task
	 */
	public void deleteTask(Task task) {
		taskList.remove(task);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.engine.ProcessEngine#addOrUpdateProcess(com.cydcor
	 * .framework.engine.ProcessContext)
	 */
	public void addOrUpdateProcess(ProcessContext context) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.engine.ProcessEngine#deleteProcess(com.cydcor.framework
	 * .engine.ProcessContext)
	 */
	public void deleteProcess(ProcessContext context) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.engine.ProcessEngine#startProcess(com.cydcor.framework
	 * .engine.ProcessContext)
	 */
	public void startProcess(ProcessContext context) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.engine.ProcessEngine#stopProcess(com.cydcor.framework
	 * .engine.ProcessContext)
	 */
	public void stopProcess(ProcessContext context) {
		// TODO Auto-generated method stub

	}

}
