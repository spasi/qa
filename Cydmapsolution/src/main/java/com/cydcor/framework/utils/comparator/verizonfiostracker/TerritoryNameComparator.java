package com.cydcor.framework.utils.comparator.verizonfiostracker;

import java.util.Comparator;

import com.cydcor.framework.model.VerizonFiosReport;

public class TerritoryNameComparator implements Comparator<VerizonFiosReport>{
	
	private boolean reverseCompare;
	
	public void TerritoryNameComparator(boolean reverseCompare){
		this.reverseCompare=reverseCompare;
	}

	public int compare(VerizonFiosReport first, VerizonFiosReport second) {
		int result = 0;
		if (first.getTerritoryName()==null && second.getTerritoryName()!=null) 
			result = 1;
	    else if (first.getTerritoryName()!=null && second.getTerritoryName()==null) 
	    	result = -1;
	    else if (first.getTerritoryName()==null && second.getTerritoryName()==null) 
	    	result = 0;
	    else
	    	result = first.getTerritoryName().compareTo(second.getTerritoryName());	    
		
        if (result != 0)
        	result = (!reverseCompare) ? result : (-1)*result;
        
		return result;	
	}
}