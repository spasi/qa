/**
 * 
 */
package com.cydcor.framework.utils;


/**
 * @author agkrishnan
 *
 */
public interface Retryable {

	public void run(RetryableRunner r) throws Exception;
	public void reInitForRetry();

}
