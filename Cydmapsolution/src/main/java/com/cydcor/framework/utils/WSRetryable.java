/**
 * 
 */
package com.cydcor.framework.utils;


/**
 * @author agkrishnan
 *
 */
public interface WSRetryable extends Retryable {
	
	public void run(RetryableRunner r) throws Exception;

}
