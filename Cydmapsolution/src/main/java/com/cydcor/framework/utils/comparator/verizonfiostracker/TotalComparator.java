package com.cydcor.framework.utils.comparator.verizonfiostracker;

import java.util.Comparator;

import com.cydcor.framework.model.VerizonFiosReport;

public class TotalComparator implements Comparator<VerizonFiosReport>{
	
	private boolean reverseCompare;
	
	public void TotalComparator(boolean reverseCompare){
		this.reverseCompare=reverseCompare;
	}

	public int compare(VerizonFiosReport first, VerizonFiosReport second) {
		int result = 0;
		if (first.getTotal()==null && second.getTotal()!=null) 
			result = 1;
	    else if (first.getTotal()!=null && second.getTotal()==null) 
	    	result = -1;
	    else if (first.getTotal()==null && second.getTotal()==null) 
	    	result = 0;
	    else
	    	result = first.getTotal().compareTo(second.getTotal());	    
		
        if (result != 0)
        	result = (!reverseCompare) ? result : (-1)*result;
        
		return result;	
	}
}
