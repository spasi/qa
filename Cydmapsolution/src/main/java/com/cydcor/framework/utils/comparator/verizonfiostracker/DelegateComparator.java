package com.cydcor.framework.utils.comparator.verizonfiostracker;

import java.util.Comparator;
import java.util.List;

import com.cydcor.framework.model.VerizonFiosReport;

public class DelegateComparator implements Comparator<VerizonFiosReport> {
	  private List<Comparator> comparators;

	  //constructor to take a list of specific comparators. 
	  //This is in line with 'order by department, salary...'
	  public DelegateComparator(List<Comparator> comparators){
	    if(comparators == null || comparators.isEmpty()){
	      throw new IllegalArgumentException();
	    }
	    this.comparators = comparators;
	  }

	  public int compare(VerizonFiosReport e1, VerizonFiosReport e2) {
	    if(e1 == e2) {
	      return 0; //same object in memory 
	    }
	    int result = 0; 
	    for(final Comparator comparator : comparators) {
	      result = comparator.compare(e1, e2); //delegate to individual comparator
	      if(result != 0) { 
	        //if the objects are not equal in the broader comparison, 
	        //sorting will be done and we won't do the finer comparisons.
	        break; 
	      }
	    }
	    return result;
	  }
	
}
