package com.cydcor.framework.utils.comparator.verizonfiostracker;

import java.util.Comparator;

import com.cydcor.framework.model.VerizonFiosReport;

public class StatusComparator implements Comparator<VerizonFiosReport> {

	private boolean reverseCompare;
	
	public void StatusComparator(boolean reverseCompare){
		this.reverseCompare=reverseCompare;
	}

	public int compare(VerizonFiosReport first, VerizonFiosReport second) {
		int result = 0;
		if (first.getStatus()==null && second.getStatus()!=null) 
			result = 1;
	    else if (first.getStatus()!=null && second.getStatus()==null) 
	    	result = -1;
	    else if (first.getStatus()==null && second.getStatus()==null) 
	    	result = 0;
	    else
	    	result = first.getStatus().compareTo(second.getStatus());	    
		
        if (result != 0)
        	result = (!reverseCompare) ? result : (-1)*result;
        
		return result;	
	}

}
