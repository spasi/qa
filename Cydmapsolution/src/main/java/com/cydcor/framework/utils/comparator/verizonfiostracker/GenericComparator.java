package com.cydcor.framework.utils.comparator.verizonfiostracker;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Comparator;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.model.VerizonFiosReport;

public class GenericComparator implements Comparator<VerizonFiosReport>{
	
	private boolean reverseCompare;
	private boolean percentageDataFormat;
	private String methodName;
	private String promoCode;
	private static final int CANT_COMPARE = 10000;
	
	public GenericComparator(boolean reverseCompare, String methodName, String promoCode, boolean percentageDataFormat){
		this.reverseCompare=reverseCompare;
		this.methodName = methodName;
		this.promoCode = promoCode;
		this.percentageDataFormat = percentageDataFormat;
		//System.out.println("rc="+this.reverseCompare);
		//System.out.println("mname="+this.methodName);
	}

	public int compare(VerizonFiosReport first, VerizonFiosReport second) {
		int result = 0;
		Object firstValue = null;
		Object secondValue = null;
		SimpleDateFormat sdfReport = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Method  method = Class.forName("com.cydcor.framework.model.VerizonFiosReport").getDeclaredMethod(methodName);
			if(methodName.equalsIgnoreCase("getPromo")){
	    		firstValue = first!=null?first.getPromo().get(promoCode):null;
	    		secondValue = second!=null?second.getPromo().get(promoCode):null;
	    	}else if(methodName.equalsIgnoreCase("dateToBeWorked")||methodName.equalsIgnoreCase("datelastWorked")){
					firstValue = method.invoke (first);
					secondValue = method.invoke (second);
	    			firstValue = StringUtils.isNotBlank((String)firstValue)?sdfReport.parse(((String)firstValue)):null;
		    		secondValue =  StringUtils.isNotBlank((String)secondValue)?sdfReport.parse(((String)secondValue)):null;
	    	}else if(methodName.equalsIgnoreCase("getTvPenetrationRate")){
	    		firstValue = StringUtils.isNotBlank(((String)first.getTvPenetrationRate()).replaceAll("%", ""))?
	    				Double.parseDouble(((String)first.getTvPenetrationRate()).replaceAll("%", "")):new Double(0);
	    		secondValue = StringUtils.isNotBlank(((String)second.getTvPenetrationRate()).replaceAll("%", ""))?
	    				Double.parseDouble(((String)second.getTvPenetrationRate()).replaceAll("%", "")):new Double(0);
	    	}else if(methodName.equalsIgnoreCase("getStatus")){
	    		method = Class.forName("com.cydcor.framework.model.VerizonFiosReport").getDeclaredMethod("getSortSeq");
	    		if(first.getSortSeq().equals(second.getSortSeq())){
	    			if(first.getSortSeq()==3){
	    				firstValue = StringUtils.isNotBlank((String)first.getStatus())?sdfReport.parse(((String)first.getStatus())):null;
	    		    	secondValue =  StringUtils.isNotBlank((String)second.getStatus())?sdfReport.parse(((String)second.getStatus())):null;
	    			}else{
	    				firstValue = first.getStatus();
	    				secondValue = second.getStatus();
	    			}
	    		}else{
					firstValue = method.invoke (first);
					secondValue = method.invoke (second);	    			
	    		}
	    	}else{			
				firstValue = method.invoke (first);
				secondValue = method.invoke (second);
	    	}
			

		} catch (Throwable e) {
			e.printStackTrace();
			return CANT_COMPARE;
		}
				
		if (firstValue==null && secondValue!=null){ 
			result = 1;
		}else if (firstValue!=null && secondValue==null){ 
	    	result = -1;
		}else if (firstValue==null && secondValue==null){ 
	    	result = 0;
		}else{
	    	result = doNaturalComparison(firstValue, secondValue, first.getTotal(), second.getTotal());	    
	    }
		
        if (result != 0 && result!=CANT_COMPARE)
        	result = (!reverseCompare) ? result : (-1)*result;
        
		return result;	
	}
	
    @SuppressWarnings("unchecked")
	private int doNaturalComparison(Object leftValue, Object rightValue, Integer leftTotal, Integer rightTotal) {
    	if (leftValue instanceof Comparable){			
    		if(percentageDataFormat && !methodName.equalsIgnoreCase("getSortSeq") && !methodName.equalsIgnoreCase("getTvPenetrationRate") && 
    				!methodName.equalsIgnoreCase("getTotal") && !methodName.equalsIgnoreCase("getDistance")){
        		if(isNumeric(leftValue.toString())){
        			return new BigDecimal(100.00*Double.parseDouble(leftValue.toString())/leftTotal).setScale(0, BigDecimal.ROUND_HALF_UP).compareTo(
        					new BigDecimal(100.00*Double.parseDouble(rightValue.toString())/rightTotal).setScale(0, BigDecimal.ROUND_HALF_UP));        			
    			}else{
        			return ((Comparable) leftValue).compareTo(rightValue);
    			}
        	}else{
        		return ((Comparable) leftValue).compareTo(rightValue);
        	}
        }else{
            return CANT_COMPARE;
        }
    }
    
    private boolean isNumeric(String value)
    {
    	try
    	{
    		double d = Double.parseDouble(value);
    		return(true);
    	}
    	catch(NumberFormatException e)
    	{
    		return(false);
    	}
    }
}