/**
 * 
 */
package com.cydcor.framework.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author suresh.chittala
 *
 */
public class ConnectionUtil {
	private static final transient Log logger = LogFactory.getLog(ConnectionUtil.class);
	private static Connection conn = null;
	private static Statement  statement = null;
	public static Connection getConnection(){
		try {
			String url = "jdbc:jtds:sqlserver://10.40.231.70:1433/CYDCOR";
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			String userName = "sa";
			String password = "idha@123";
			
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, userName, password);
		}catch (Exception e) {
			logger.debug(e);;
		}finally{
			return conn;
		}
	}
	
	public static Statement getStatement(){
		try {
			statement = getConnection().createStatement();
		} catch (Exception e) {
			logger.debug(e);;
		}finally{
			return statement;
		}
	}
	
	public static void closeConnection(){
		try {
			if(conn != null){
				conn.close();
			}
			if(statement != null){
				statement.close();
			}
		} catch (SQLException e) {
			logger.debug(e);;
		}
	}
}
