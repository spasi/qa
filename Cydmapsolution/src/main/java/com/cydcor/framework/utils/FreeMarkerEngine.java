/**
 * 
 */
package com.cydcor.framework.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.exceptions.RoleNotFoundException;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.FileTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.StringTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.cache.WebappTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * This Class Acts Like Utility Class For Freemarker Evaluation
 * 
 * @author ashwin
 * 
 */
public class FreeMarkerEngine {
	private static final transient Log logger = LogFactory
			.getLog(FreeMarkerEngine.class);

	private static FreeMarkerEngine freeMarkerEngine = new FreeMarkerEngine();

	Configuration cfg = null;
	Configuration stringCfg = null;

	/**
	 * Default Parameters to be set before passing to freemarker processing
	 */
	Map defaultParams = new HashMap();

	FileTemplateLoader ftl1 = null;
	ClassTemplateLoader ctl = null;
	WebappTemplateLoader wtl = null;

	MultiTemplateLoader mtl = null;
	TemplateLoader[] loaders = null;

	// Making this private to create singleton class
	private FreeMarkerEngine() {
		cfg = new Configuration();
		stringCfg = new Configuration();
		// Specify the data source where the template files come from.
		// Here I set a file directory for it:
		try {
			// Default is FTL Directory
			// cfg.setDirectoryForTemplateLoading(new File("ftl"));
			ftl1 = new FileTemplateLoader(new File("ftl"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Default Directory ftl not found" + e);
		}

		try {
			ctl = new ClassTemplateLoader(getClass(), "");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Default Directory ftl not found" + e);
		}
		try {
			wtl = new WebappTemplateLoader(CydcorContext.getInstance()
					.getServletContext(), "WEB-INF/classes/ftl");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Default Directory ftl not found" + e);
		}

		setFileLoaders();

		cfg.setTemplateLoader(mtl);

		// Specify how templates will see the data-model. This is an advanced
		// topic...
		// but just use this:
		cfg.setObjectWrapper(ObjectWrapper.BEANS_WRAPPER);
		cfg.setTagSyntax(Configuration.AUTO_DETECT_TAG_SYNTAX);
		defaultParams.put("GlobalContext", CydcorContext.getInstance());

		stringCfg.setObjectWrapper(ObjectWrapper.BEANS_WRAPPER);
		stringCfg.setTagSyntax(Configuration.AUTO_DETECT_TAG_SYNTAX);
	}

	/**
	 * @param directoryPath
	 */
	public void changeFTLDirectory(String directoryPath) {
		// Specify the data source where the template files come from.
		// Here I set a file directory for it:
		try {
			cfg.setDirectoryForTemplateLoading(new File(directoryPath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug(e);
		}
	}

	/**
	 * @return
	 */
	public static FreeMarkerEngine getInstance() {
		return freeMarkerEngine;
	}

	/**
	 * @param template
	 * @return
	 */
	public String evaluateString(String template) {
		Map parameters = new HashMap();
		parameters.putAll(getInstance().defaultParams);
		return evaluateString(template, parameters);
	}

	/**
	 * @param template
	 * @return
	 */
	public String evaluateString(String evauationTemplate, Map parameters) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		OutputStreamWriter output = new OutputStreamWriter(bos);
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		String stringTemplate = "stringTemplate";
		evauationTemplate = evauationTemplate.replaceAll("&lt;", "<");
		evauationTemplate = evauationTemplate.replaceAll("&gt;", ">");
		stringLoader.putTemplate(stringTemplate, evauationTemplate);
		stringCfg.setTemplateLoader(stringLoader);
		try {
			Template template = stringCfg.getTemplate(stringTemplate);
			template.process(parameters, output);
		} catch (RoleNotFoundException e) {
			logger.debug(e);
			throw e;
		} catch (IOException e) {
			logger.debug(e);
			return evauationTemplate;
		} catch (TemplateException e) {
			logger.debug(e);
			if (e.getCauseException() instanceof RoleNotFoundException) {
				throw (RoleNotFoundException) e.getCauseException();
			} else {
				return evauationTemplate;
			}
		} catch (Throwable th) {
			logger.debug(th);
			return evauationTemplate;
		}

		return bos.toString();

	}

	/**
	 * @param fileName
	 * @return
	 */
	public String evaluateTemplate(String fileName) {

		Map parameters = new HashMap();
		return evaluateTemplate(fileName, parameters);
	}

	/**
	 * 
	 */
	private void setFileLoaders() {
		try {

			if (wtl == null) {

			} else {
				if (ftl1 == null) {
					loaders = new TemplateLoader[] { wtl, ctl };
				} else {
					loaders = new TemplateLoader[] { wtl, ftl1, ctl };
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Default Directory ftl not found" + e);
		}

		try {
			mtl = new MultiTemplateLoader(loaders);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Default Directory ftl not found" + e);
		}
		cfg.setTemplateLoader(mtl);
	}

	/**
	 * @param fileName
	 * @param Parameters
	 * @return
	 */
	public String evaluateTemplate(String fileName, Map parameters) {
		//logger.debug("fileName:"+fileName);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		OutputStreamWriter output = new OutputStreamWriter(bos);
		parameters.putAll(getInstance().defaultParams);
		try {
			Template tpl = cfg.getTemplate(fileName);
			tpl.process(parameters, output);

		} catch (java.io.FileNotFoundException e) {
			// Incase of Error try loading from template page

			logger.error("File error:",e);
			try {
				logger.debug("Loading file from "+"template/" + fileName);
				Template tpl = cfg.getTemplate("template/" + fileName);
				tpl.process(parameters, output);
			} catch (Throwable th) {
				// Incase there is Exception in FileREading then REturn
				// Blank
				logger.debug(th);
				// Incase of Error Return the same Template
				try {
					logger.info("Using FileUtils.readFileToString.");
					String returnStr = FileUtils.readFileToString(new File(
							fileName));
					return returnStr;
				} catch (Throwable th2) {
					// Incase there is Exception in FileREading then REturn
					// Blank
					logger.debug(th2);
					return "";
				}
			}
		} catch (Throwable e) {
			logger.debug(e);
			return "";
		}

		return bos.toString();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List mediaFileList = new ArrayList();

		Map rootMap = new HashMap();
		rootMap.put("list", mediaFileList);
		rootMap.putAll(getInstance().defaultParams);

		logger.debug(freeMarkerEngine.evaluateTemplate("Freemarker.ftl",
				rootMap));
	}

}
