package com.cydcor.framework.utils.comparator.verizonfiostracker;

import java.util.Comparator;

import com.cydcor.framework.model.VerizonFiosReport;

public class SortSeqComparator implements Comparator<VerizonFiosReport>{
	
	private boolean reverseCompare;
	
	public void SortSeqComparator(boolean reverseCompare){
		this.reverseCompare=reverseCompare;
	}

	public int compare(VerizonFiosReport first, VerizonFiosReport second) {
		int result = 0;
		if (first.getSortSeq()==null && second.getSortSeq()!=null) 
			result = 1;
	    else if (first.getSortSeq()!=null && second.getSortSeq()==null) 
	    	result = -1;
	    else if (first.getSortSeq()==null && second.getSortSeq()==null) 
	    	result = 0;
	    else
	    	result = first.getSortSeq().compareTo(second.getSortSeq());	    
		
        if (result != 0)
        	result = (!reverseCompare) ? result : (-1)*result;
        
		return result;	
	}
}
