package com.cydcor.framework.utils;

import com.thoughtworks.xstream.XStream;

public class XStreamUtils {
	public static Object getObjectFromXml(Class clazz, String xml) {
		XStream xstream = new XStream();
		xstream.processAnnotations(clazz);
		Object object = (Object) xstream.fromXML(xml.trim());
		return object;
	}

}
