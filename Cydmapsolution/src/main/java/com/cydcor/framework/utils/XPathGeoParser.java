/**
 * 
 */
package com.cydcor.framework.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.cydcor.framework.model.glocation.GeocodeResponse;

/**
 * @author ashwin kumar
 * 
 */

public class XPathGeoParser {

	private static String LATITUDE_EXPRESSION = "//result[1]//location/lat/text()";
	private static String LONGITUDE_EXPRESSION = "//result[1]//location/lng/text()";
	private static String ADDRESS_EXPRESSION = "//result/formatted_address/text()";
	private static XPathFactory factory = XPathFactory.newInstance();

	public static void main(String[] args) throws IOException {

		XPath xpath = factory.newXPath();

		try {
			System.out.print("Geocode Parser 1.0\n");

			// In practice, you'd retrieve your XML via an HTTP request.
			// Here we simply access an existing file.
			File xmlFile = new File("XML_FILE");

			// The xpath evaluator requires the XML be in the format of an
			// InputSource
			InputSource inputXml = new InputSource(new FileInputStream(xmlFile));

			// Because the evaluator may return multiple entries, we specify
			// that the expression
			// return a NODESET and place the result in a NodeList.
			NodeList nodes = (NodeList) xpath.evaluate("XPATH_EXPRESSION",
					inputXml, XPathConstants.NODESET);

			// We can then iterate over the NodeList and extract the content via
			// getTextContent().
			// NOTE: this will only return text for element nodes at the
			// returned context.
			for (int i = 0, n = nodes.getLength(); i < n; i++) {
				String nodeString = nodes.item(i).getTextContent();
				System.out.print(nodeString);
				System.out.print("\n");
			}
		} catch (XPathExpressionException ex) {
			System.out.print("XPath Error");
		} catch (FileNotFoundException ex) {
			System.out.print("File Error");
		}
	}

	/**
	 * @param xmlResponse
	 * @return
	 */
	public static GeocodeResponse getGeoCode(String xmlResponse) {
		XPath xpath = factory.newXPath();
		GeocodeResponse geocodeResponse = new GeocodeResponse();
		try {
			// System.out.print("Geocode Parser 1.0\n");

			// In practice, you'd retrieve your XML via an HTTP request.
			// Here we simply access an existing file.
			// File xmlFile = new File("XML_FILE");

			// The xpath evaluator requires the XML be in the format of an
			// InputSource
			InputSource inputXml = new InputSource(new ByteArrayInputStream(
					(xmlResponse.replaceAll("#", " ").replaceAll(
							"<?xml version=\"1.0\" encoding=\"UTF-8\"?>", ""))
							.getBytes()));

			// Because the evaluator may return multiple entries, we specify
			// that the expression
			// return a NODESET and place the result in a NodeList.
			NodeList nodes = (NodeList) xpath.evaluate(ADDRESS_EXPRESSION,
					inputXml, XPathConstants.NODESET);

			// We can then iterate over the NodeList and extract the content via
			// getTextContent().
			// NOTE: this will only return text for element nodes at the
			// returned context.
			for (int i = 0, n = nodes.getLength(); i < n; i++) {
				String nodeString = nodes.item(i).getTextContent();
				geocodeResponse.setFormattedAddress(nodeString);
				break;
			}

			
			
			inputXml = new InputSource(new ByteArrayInputStream(
					(xmlResponse.replaceAll("#", " ").replaceAll(
							"<?xml version=\"1.0\" encoding=\"UTF-8\"?>", ""))
							.getBytes()));
			// For Latitude
			nodes = (NodeList) xpath.evaluate(LATITUDE_EXPRESSION, inputXml,
					XPathConstants.NODESET);

			// We can then iterate over the NodeList and extract the content via
			// getTextContent().
			// NOTE: this will only return text for element nodes at the
			// returned context.
			for (int i = 0, n = nodes.getLength(); i < n; i++) {
				String nodeString = nodes.item(i).getTextContent();
				geocodeResponse.setLatitude(nodeString);
				break;
			}

			
			inputXml = new InputSource(new ByteArrayInputStream(
					(xmlResponse.replaceAll("#", " ").replaceAll(
							"<?xml version=\"1.0\" encoding=\"UTF-8\"?>", ""))
							.getBytes()));
			// fOR Longitude
			nodes = (NodeList) xpath.evaluate(LONGITUDE_EXPRESSION, inputXml,
					XPathConstants.NODESET);

			// We can then iterate over the NodeList and extract the content via
			// getTextContent().
			// NOTE: this will only return text for element nodes at the
			// returned context.
			for (int i = 0, n = nodes.getLength(); i < n; i++) {
				String nodeString = nodes.item(i).getTextContent();
				geocodeResponse.setLogitude(nodeString);
				break;
			}

		} catch (XPathExpressionException ex) {
			ex.printStackTrace();
			System.out.print("XPath Error");
		}
		return geocodeResponse;
	}

}