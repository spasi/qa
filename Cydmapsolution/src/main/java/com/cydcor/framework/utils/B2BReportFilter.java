package com.cydcor.framework.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.cydcor.framework.model.B2BReport;

public class B2BReportFilter {
	List<B2BReport> filteredList = null;
	//public enum COLUMN_NAME {CURR_REP, ZIP, SHARED_BY, NUM_SHARED_LEADS, TOWN, DISTANCE, TOTAL, FIOS_DATA, FIOS_MIGRATION, FIOS_TV, WINBACK, PROSPECT, 
	//	HSI, DSL_MAX_SPEED_BELOW_3, DSL_MAX_SPEED_ABOVE_3, DT_WORKED};
	
	public List<B2BReport> filterTerritoryList(List<B2BReport> originalList, String columnName, String opName, String text){
			/**
			 * Pass filter to filter the original list
			 */
		filteredList =   new FilterList().filterList(originalList, filter, (String) text,columnName,opName);
		
		return filteredList;
	}
	
	/**
     ** Creating Filter Logic
     **/
     Filter<B2BReport,String> filter = new Filter<B2BReport,String>() {
    	 
         /*public boolean isMatched(Employee object, String text) {
             //return object.getName().startsWith(String.valueOf(text));
         	 return object.getName().contains(String.valueOf(text));
         }
		*/
		public boolean beginsWith(B2BReport object, String text, String colName){
			boolean bReturn = false;
			if("SALES_REPS".equals(colName)){
				if(null != object.getCurrentRep() && !object.getCurrentRep().equals("")){
					String[] strArray = object.getCurrentRep().split(",");
					if(strArray.length > 0){
						for(String s : strArray){
							bReturn =  s.trim().toUpperCase().startsWith(String.valueOf(text).toUpperCase());
							if(bReturn)
								break;
						}
					}
							
				}
			}else if("TERRITORY".equals(colName)){
				bReturn =  object.getTerritoryName().startsWith(String.valueOf(text));
			}else if("SHARED_BY".equals(colName)){
				if(null != object.getSharedBy() && !object.getSharedBy().equals("")){
					String[] strArray = object.getSharedBy().split(",");
					if(strArray.length > 0){
						for(String s : strArray){
							bReturn =  s.trim().toUpperCase().startsWith(String.valueOf(text).toUpperCase());	
							if(bReturn)
								break;
						}
					}
							
				}
			}else if("TOWN".equals(colName)){
				//return object.getCurrentRep().startsWith(String.valueOf(text));
				if(null != object.getTown() && !object.getTown().equals("")){
					String[] strArray = object.getTown().split(",");
					if(strArray.length > 0){
						for(String s : strArray){
							bReturn =  s.trim().toUpperCase().startsWith(String.valueOf(text).toUpperCase());
							if(bReturn)
								break;
						}
					}
				}
			}
			return bReturn;
		}

		public boolean isLike(B2BReport object, String text, String colName) {
			boolean bReturn = false;
			if("SALES_REPS".equals(colName)){
				if(null != object.getCurrentRep() && !object.getCurrentRep().equals("")){
					bReturn = object.getCurrentRep().toUpperCase().trim().contains(String.valueOf(text).toUpperCase());
				}
			}else if("TERRITORY".equals(colName)){
				if(null != object.getTerritoryName() && !object.getTerritoryName().equals("")){
					bReturn =  object.getTerritoryName().trim().contains(String.valueOf(text));
				}
			}else if("SHARED_BY".equals(colName)){
				if(null != object.getSharedBy() && !object.getSharedBy().equals("")){
					bReturn =  object.getSharedBy().trim().toUpperCase().contains(String.valueOf(text).toUpperCase());
				}
			}else if("TOWN".equals(colName)){
				if(null != object.getTown() && !object.getTown().equals("")){
					bReturn =  object.getTown().trim().toUpperCase().contains(String.valueOf(text).toUpperCase());
				}
			}
			return bReturn;
		}

		public boolean isNotLike(B2BReport object, String text, String colName) {
			boolean bReturn = false;
			if("SALES_REPS".equals(colName)){
				if(null != object.getCurrentRep() && !object.getCurrentRep().equals("")){
					bReturn = !object.getCurrentRep().trim().toUpperCase().contains(String.valueOf(text).toUpperCase());
				}else{bReturn = true;}
			}else if("TERRITORY".equals(colName)){
				if(null != object.getTerritoryName() && !object.getTerritoryName().equals("")){
					bReturn =  !object.getTerritoryName().trim().contains(String.valueOf(text));
				}else{bReturn = true;}
			}else if("SHARED_BY".equals(colName)){
				if(null != object.getSharedBy() && !object.getSharedBy().equals("")){
					bReturn =  !object.getSharedBy().trim().toUpperCase().contains(String.valueOf(text).toUpperCase());
				}else{bReturn = true;}
			}else if("TOWN".equals(colName)){
				if(null != object.getTown() && !object.getTown().equals("")){
					bReturn =  !object.getTown().trim().toUpperCase().contains(String.valueOf(text).toUpperCase());
				}else{bReturn = true;}
			}
			return bReturn;
		}

		public boolean isGreaterThan(B2BReport object, String text,
				String colName) {
			boolean bReturn = false;
			if("NUM_SHARED_LEADS".equals(colName)){
				bReturn = (object.getNumOfSharedLeads().compareTo(new Integer(text)) > 0);
			}else if("DISTANCE".equals(colName)){
				if(null != object.getDistance() && !object.getDistance().equals("")){
					bReturn = (new Double (object.getDistance().replaceAll(",", "")).compareTo(new Double(text.replaceAll(",", ""))) > 0);
				}
			}else if("TOTAL".equals(colName)){
				bReturn = (object.getTotal().compareTo(new Integer(text)) > 0);		
			}else if("FIOS_DATA".equals(colName)){
				bReturn = (object.getFiosData().compareTo(new Integer(text)) > 0);			
			}else if("FIOS_MIGRATION".equals(colName)){
				bReturn = (object.getFiosMigration().compareTo(new Integer(text)) > 0);			
			}else if("FIOS_TV".equals(colName)){
				bReturn = (object.getFiosTV().compareTo(new Integer(text)) > 0);			
			}else if("WINBACK".equals(colName)){
				bReturn = (object.getWinback().compareTo(new Integer(text)) > 0);			
			}else if("PROSPECT".equals(colName)){
				bReturn = (object.getProspect().compareTo(new Integer(text)) > 0);			
			}else if("VOICEONLY".equals(colName)){
				bReturn = (object.getVoiceOnly().compareTo(new Integer(text)) > 0);			
			}else if("DIFFINCOUNT".equals(colName)){
				bReturn = (object.getDiffInCount().compareTo(new Integer(text)) > 0);			
			}else if("HSI".equals(colName)){
				bReturn = (object.getHsi().compareTo(new Integer(text)) > 0);
			}else if("DSL_MAX_SPEED_BELOW_3".equals(colName)){
				bReturn = (object.getTotalMaxSpeedDnLoadBelow3().compareTo(new Integer(text)) > 0);			
			}else if("DSL_MAX_SPEED_ABOVE_3".equals(colName)){
				bReturn = (object.getTotalMaxSpeedDnLoadAbove3().compareTo(new Integer(text)) > 0);			
			}
			return bReturn;
		}

		public boolean isLessThan(B2BReport object, String text, String colName) {
			boolean bReturn = false;
			if("NUM_SHARED_LEADS".equals(colName)){
				bReturn = (object.getNumOfSharedLeads().compareTo(new Integer(text)) < 0);
			}else if("DISTANCE".equals(colName)){
				if(null != object.getDistance() && !object.getDistance().equals("")){
					bReturn = (new Double (object.getDistance().replaceAll(",", "")).compareTo(new Double(text.replaceAll(",", ""))) < 0);	
				}
			}else if("TOTAL".equals(colName)){
				bReturn = (object.getTotal().compareTo(new Integer(text)) < 0);		
			}else if("FIOS_DATA".equals(colName)){
				bReturn = (object.getFiosData().compareTo(new Integer(text)) < 0);			
			}else if("FIOS_MIGRATION".equals(colName)){
				bReturn = (object.getFiosMigration().compareTo(new Integer(text)) < 0);			
			}else if("FIOS_TV".equals(colName)){
				bReturn = (object.getFiosTV().compareTo(new Integer(text)) < 0);			
			}else if("WINBACK".equals(colName)){
				bReturn = (object.getWinback().compareTo(new Integer(text)) < 0);			
			}else if("PROSPECT".equals(colName)){
				bReturn = (object.getProspect().compareTo(new Integer(text)) < 0);			
			}else if("VOICEONLY".equals(colName)){
				bReturn = (object.getVoiceOnly().compareTo(new Integer(text)) < 0);			
			}else if("DIFFINCOUNT".equals(colName)){
				bReturn = (object.getDiffInCount().compareTo(new Integer(text)) < 0);			
			}else if("HSI".equals(colName)){
				bReturn = (object.getHsi().compareTo(new Integer(text)) < 0);
			}else if("DSL_MAX_SPEED_BELOW_3".equals(colName)){
				bReturn = (object.getTotalMaxSpeedDnLoadBelow3().compareTo(new Integer(text)) < 0);			
			}else if("DSL_MAX_SPEED_ABOVE_3".equals(colName)){
				bReturn = (object.getTotalMaxSpeedDnLoadAbove3().compareTo(new Integer(text)) < 0);			
			}
			return bReturn;
		}

		public boolean isEquals(B2BReport object, String text, String colName) {
			boolean bReturn = false;
			if("SALES_REPS".equals(colName)){
				if(null != object.getCurrentRep() && !object.getCurrentRep().equals("")){
					String[] strArray = object.getCurrentRep().split(",");
					if(strArray.length > 0){
						for(String s : strArray){
							bReturn =  s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
							if(bReturn)
								break;
						}
					}
							
				}
			}else if("TERRITORY".equals(colName)){
				bReturn =  object.getTerritoryName().equals(String.valueOf(text));
			}else if("SHARED_BY".equals(colName)){
				if(null != object.getSharedBy() && !object.getSharedBy().equals("")){
					String[] strArray = object.getSharedBy().split(",");
					if(strArray.length > 0){
						for(String s : strArray){
							bReturn =  s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
							if(bReturn)
								break;
						}
					}
							
				}
			}else if("TOWN".equals(colName)){
				//return object.getCurrentRep().startsWith(String.valueOf(text));
				if(null != object.getTown() && !object.getTown().equals("")){
					String[] strArray = object.getTown().split(",");
					if(strArray.length > 0){
						for(String s : strArray){
							bReturn =  s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
							if(bReturn)
								break;
						}
					}
				}
			}if("NUM_SHARED_LEADS".equals(colName)){
				bReturn = (object.getNumOfSharedLeads().compareTo(new Integer(text)) == 0);
			}else if("DISTANCE".equals(colName)){
				if(null != object.getDistance() && !object.getDistance().equals("")){
					bReturn = (new Double (object.getDistance().replaceAll(",", "")).compareTo(new Double(text.replaceAll(",", ""))) == 0);
				}
			}else if("TOTAL".equals(colName)){
				bReturn = (object.getTotal().compareTo(new Integer(text)) == 0);		
			}else if("FIOS_DATA".equals(colName)){
				bReturn = (object.getFiosData().compareTo(new Integer(text)) == 0);			
			}else if("FIOS_MIGRATION".equals(colName)){
				bReturn = (object.getFiosMigration().compareTo(new Integer(text)) == 0);			
			}else if("FIOS_TV".equals(colName)){
				bReturn = (object.getFiosTV().compareTo(new Integer(text)) == 0);			
			}else if("WINBACK".equals(colName)){
				bReturn = (object.getWinback().compareTo(new Integer(text)) == 0);			
			}else if("PROSPECT".equals(colName)){
				bReturn = (object.getProspect().compareTo(new Integer(text)) == 0);			
			}else if("VOICEONLY".equals(colName)){
				bReturn = (object.getVoiceOnly().compareTo(new Integer(text)) == 0);			
			}else if("DIFFINCOUNT".equals(colName)){
				bReturn = (object.getDiffInCount().compareTo(new Integer(text)) == 0);			
			}else if("HSI".equals(colName)){
				bReturn = (object.getHsi().compareTo(new Integer(text)) == 0);
			}else if("DSL_MAX_SPEED_BELOW_3".equals(colName)){
				bReturn = (object.getTotalMaxSpeedDnLoadBelow3().compareTo(new Integer(text)) == 0);			
			}else if("DSL_MAX_SPEED_ABOVE_3".equals(colName)){
				bReturn = (object.getTotalMaxSpeedDnLoadAbove3().compareTo(new Integer(text)) == 0);			
			}else if("DT_WORKED".equals(colName)){
				if(null != object.getDateToBeWorked() && !object.getDateToBeWorked().equals("")){
					SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
					try {
						Date ldt = sdf.parse(object.getDateToBeWorked());
						Date cdt = sdf.parse(text);
						if(ldt.equals(cdt)){
							bReturn = true;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						bReturn = false;
					}
				}
			}else if("CLOSED_DT".equals(colName)){
				if(null != object.getClosedDate() && !object.getClosedDate().equals("")){
					SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
					try {
						Date ldt = sdf.parse(object.getClosedDate());
						Date cdt = sdf.parse(text);
						if(ldt.equals(cdt)){
							bReturn = true;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						bReturn = false;
					}
				}			
			}
			return bReturn;
		}

		public boolean isNotEquals(B2BReport object, String text, String colName) {
			boolean bReturn = false;
			if("SALES_REPS".equals(colName)){
				if(null != object.getCurrentRep() && !object.getCurrentRep().equals("")){
					String[] strArray = object.getCurrentRep().split(",");
					if(strArray.length > 0){
						boolean bool = false;
						for(String s : strArray){
							bool =  s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
							if(bool)
								break;
						}
						bReturn = !bool;
					}
							
				}else{bReturn = true;}
			}else if("TERRITORY".equals(colName)){
				bReturn =  !object.getTerritoryName().equals(String.valueOf(text));
			}else if("SHARED_BY".equals(colName)){
				if(null != object.getSharedBy() && !object.getSharedBy().equals("")){
					String[] strArray = object.getSharedBy().split(",");
					if(strArray.length > 0){
						boolean bool = false;
						for(String s : strArray){
							bool =  s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
							if(bool)
								break;
						}
						bReturn = !bool;
					}
							
				}else{bReturn = true;}
			}else if("TOWN".equals(colName)){
				//return object.getCurrentRep().startsWith(String.valueOf(text));
				if(null != object.getTown() && !object.getTown().equals("")){
					String[] strArray = object.getTown().split(",");
					if(strArray.length > 0){
						boolean bool = false;
						for(String s : strArray){
							bool =  s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
							if(bool)
								break;
						}
						bReturn = !bool;
					}
				}else{bReturn = true;}
			}if("NUM_SHARED_LEADS".equals(colName)){
				bReturn = (object.getNumOfSharedLeads().compareTo(new Integer(text)) != 0);
			}else if("DISTANCE".equals(colName)){
				if(null != object.getDistance() && !object.getDistance().equals("")){
					bReturn = (new Double (object.getDistance().replaceAll(",", "")).compareTo(new Double(text.replaceAll(",", ""))) != 0);
				}else{bReturn = true;}
			}else if("TOTAL".equals(colName)){
				bReturn = (object.getTotal().compareTo(new Integer(text)) != 0);		
			}else if("FIOS_DATA".equals(colName)){
				bReturn = (object.getFiosData().compareTo(new Integer(text)) != 0);			
			}else if("FIOS_MIGRATION".equals(colName)){
				bReturn = (object.getFiosMigration().compareTo(new Integer(text)) != 0);			
			}else if("FIOS_TV".equals(colName)){
				bReturn = (object.getFiosTV().compareTo(new Integer(text)) != 0);			
			}else if("WINBACK".equals(colName)){
				bReturn = (object.getWinback().compareTo(new Integer(text)) != 0);			
			}else if("PROSPECT".equals(colName)){
				bReturn = (object.getProspect().compareTo(new Integer(text)) != 0);			
			}else if("VOICEONLY".equals(colName)){
				bReturn = (object.getVoiceOnly().compareTo(new Integer(text)) != 0);			
			}else if("DIFFINCOUNT".equals(colName)){
				bReturn = (object.getDiffInCount().compareTo(new Integer(text)) != 0);			
			}else if("HSI".equals(colName)){
				bReturn = (object.getHsi().compareTo(new Integer(text)) != 0);
			}else if("DSL_MAX_SPEED_BELOW_3".equals(colName)){
				bReturn = (object.getTotalMaxSpeedDnLoadBelow3().compareTo(new Integer(text)) != 0);			
			}else if("DSL_MAX_SPEED_ABOVE_3".equals(colName)){
				bReturn = (object.getTotalMaxSpeedDnLoadAbove3().compareTo(new Integer(text)) != 0);			
			}
			return bReturn;
		}

		public boolean isOnOrBefore(B2BReport object, String text,String colName) {
			boolean bReturn = false;
			if("DT_WORKED".equals(colName)){
				if(null != object.getDateToBeWorked() && !object.getDateToBeWorked().equals("")){
					SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
					try {
						Date ldt = sdf.parse(object.getDateToBeWorked());
						Date cdt = sdf.parse(text);
						if(ldt.before(cdt) || ldt.equals(cdt)){
							bReturn = true;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						bReturn = false;
					}
				}
			}else if("CLOSED_DT".equals(colName)){
				if(null != object.getClosedDate() && !object.getClosedDate().equals("")){
					SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
					try {
						Date ldt = sdf.parse(object.getClosedDate());
						Date cdt = sdf.parse(text);
						if(ldt.before(cdt) || ldt.equals(cdt)){
							bReturn = true;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						bReturn = false;
					}
				}
			}
			return bReturn;
		}

		public boolean isOnOrAfter(B2BReport object, String text, String colName) {
			boolean bReturn = false;
			if("DT_WORKED".equals(colName)){
				if(null != object.getDateToBeWorked() && !object.getDateToBeWorked().equals("")){
					SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
					try {
						Date ldt = sdf.parse(object.getDateToBeWorked());
						Date cdt = sdf.parse(text);
						if(ldt.after(cdt) || ldt.equals(cdt)){
							bReturn = true;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						bReturn = false;
					}
				}
			}else if("CLOSED_DT".equals(colName)){
				if(null != object.getClosedDate() && !object.getClosedDate().equals("")){
					SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
					try {
						Date ldt = sdf.parse(object.getClosedDate());
						Date cdt = sdf.parse(text);
						if(ldt.after(cdt) || ldt.equals(cdt)){
							bReturn = true;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						bReturn = false;
					}
				}
			}
			return bReturn;
		}
     };
}

interface Filter<T,E> {
    public boolean beginsWith(T object, E text, String colName);
    public boolean isLike(T object, E text, String colName);
    public boolean isNotLike(T object, E text, String colName);
    public boolean isGreaterThan(T object, E text, String colName);
    public boolean isLessThan(T object, E text, String colName);
    public boolean isEquals(T object, E text, String colName);
    public boolean isNotEquals(T object, E text, String colName);
    public boolean isOnOrBefore(T object, E text, String colName);
    public boolean isOnOrAfter(T object, E text, String colName);
}

class FilterList<E> {
	//public enum OPERATION {BEGIN_WITH, EQUALS, NOT_EQUALS, IS_LIKE, IS_NOT_LIKE, GREATER_THAN, LESS_THAN, ON_BEFORE, ON_AFTER};
    public  <T> List filterList(List<T> originalList, Filter filter, E text, String columnName, String opName) {
        List<T> filterList = new LinkedList<T>();
        
        	if("BEGIN_WITH".equals(opName)){
        		for (T object : originalList) {
            		if (filter.beginsWith(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("IS_LIKE".equals(opName)){
        		for (T object : originalList) {
            		if (filter.isLike(object, text,columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("NOT_LIKE".equals(opName)){
        		for (T object : originalList) {
            		if (filter.isNotLike(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("GREATER_THAN".equals(opName)){
        		for (T object : originalList) {
            		if (filter.isGreaterThan(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("LESS_THAN".equals(opName)){
        		for (T object : originalList) {
            		if (filter.isLessThan(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("ON_BEFORE".equals(opName)){
        		for (T object : originalList) {
            		if (filter.isOnOrBefore(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("ON_AFTER".equals(opName)){
        		for (T object : originalList) {
            		if (filter.isOnOrAfter(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("EQUALS".equals(opName)){
        		for (T object : originalList) {
            		if (filter.isEquals(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("NOT_EQUALS".equals(opName)){
        		for (T object : originalList) {
            		if (filter.isNotEquals(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}
        	
        return filterList;
    }
}
