package com.cydcor.framework.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileUtils {
	private static final transient Log logger = LogFactory
			.getLog(FileUtils.class);

	public static String readFile(String url) {
		StringBuffer msg = new StringBuffer();
		FileInputStream stream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		String strLine;
		try {
			url = url.replaceAll("file:/", "").replaceAll("%20", " ");
			logger.debug("-----------------Trying to Read File at :" + url);
			stream = new FileInputStream(new File(url));
			// Get the object of DataInputStream
			in = new DataInputStream(stream);
			br = new BufferedReader(new InputStreamReader(in));

			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// Print the content on the console
				msg.append(strLine.trim());
			}
		} catch (Exception e) {
			logger.debug(e);
		} finally {
			// Close the input stream
			try {
				in.close();
				stream.close();
			} catch (IOException e) {
				logger.debug(e);
			}

		}
		return msg.toString();
	}

	/**
	 * @param stream
	 * @return
	 */
	public static String readFile(InputStream stream) {
		StringBuffer msg = new StringBuffer();
		DataInputStream in = null;
		BufferedReader br = null;
		String strLine;
		try {
			// Get the object of DataInputStream
			in = new DataInputStream(stream);
			br = new BufferedReader(new InputStreamReader(in));

			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// Print the content on the console
				msg.append(strLine.trim());
			}
		} catch (Exception e) {
			logger.debug(e);
		} finally {
			// Close the input stream
			try {
				in.close();
				stream.close();
			} catch (IOException e) {
				logger.debug(e);
			}

		}
		return msg.toString();
	}
}
