package com.cydcor.framework.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import bsh.StringUtil;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.model.query.Queries;
import com.cydcor.framework.model.query.Query;

public class QueryUtils {
    private static final transient Log logger = LogFactory.getLog(QueryUtils.class);

    private static String xml = null;

    private static Map<String, Map<String, String>> clientQueries = new HashMap<String, Map<String, String>>();

    private static void loadClientQueries() {
	String xmlPath = null;
	String clientKey = CydcorContext.getInstance().getRequestContext() != null ? CydcorContext.getInstance().getRequestContext().getRequestWrapper()
		.getParameter("clientKey") : null;

	try {
	    if (StringUtils.isNotBlank(clientKey))
		xmlPath = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getRealPath("/") + "/WEB-INF/classes/queries/" + clientKey
			+ "/queries.xml";
	    else
		xmlPath = "/WEB-INF/classes/queries/queries.xml";

	    logger.debug("Trying to Load Queries from :" + xmlPath);
	    init(clientKey, xmlPath);
	} catch (Exception e) {
	    try {
		xmlPath = CydcorContext.getInstance().getBasePath() + "/WEB-INF/classes/queries/queries.xml";
		logger.debug("Trying to Load Queries from as it failed in previous step :" + xmlPath);
		init(clientKey, xmlPath);
	    } catch (Exception e1) {
		reload();
	    }
	}
    }

    private static void init(String clientKey, String url) {
	xml = FileUtils.readFile(url);
	Queries queries = (Queries) XStreamUtils.getObjectFromXml(Queries.class, xml);
	if ("".equals(clientKey))
	    clientKey = null;

	clientQueries.put(clientKey, getQueryMap(queries.getQuery()));
    }

    // reload queries again
    private static void reload() {
	String clientKey = CydcorContext.getInstance().getRequestContext() != null ? CydcorContext.getInstance().getRequestContext().getRequestWrapper()
		.getParameter("clientKey") : null;

	try {
	    if (StringUtils.isNotBlank(clientKey))

		xml = FileUtils.readFile(CydcorUtils.class.getClassLoader().getResourceAsStream("classes/queries/" + clientKey + "/queries.xml"));
	    else
		xml = FileUtils.readFile(CydcorUtils.class.getClassLoader().getResourceAsStream("classes/queries/queries.xml"));

	    logger.debug("Trying to Load Queries from as it failed in last try :classes/queries/queries.xml");
	} catch (Exception e) {
	    // TODO: handle exception
	    xml = FileUtils.readFile(CydcorUtils.class.getClassLoader().getResourceAsStream("queries/queries.xml"));
	    logger.debug("Trying to Load Queries from as it failed in last try :queries/queries.xml");
	}
	Queries queries = (Queries) XStreamUtils.getObjectFromXml(Queries.class, xml);
	if ("".equals(clientKey))
	    clientKey = null;

	clientQueries.put(clientKey, getQueryMap(queries.getQuery()));
    }

    /**
     * @return the queryMap
     */
    private static Map<String, String> getQueryMap(List<Query> query) {
	Map<String, String> queryMap = new HashMap<String, String>();
	queryMap.clear();
	for (Query quer : query) {
	    queryMap.put(quer.getKey().trim(), quer.getValue().trim());
	}
	return queryMap;
    }

    public static Map<String, String> getQuery() {
	String clientKey = CydcorContext.getInstance().getRequestContext() != null ? CydcorContext.getInstance().getRequestContext().getRequestWrapper()
		.getParameter("clientKey") : null;
		logger.debug(">>getQuery, clientKey:"+clientKey);
		if(StringUtils.isBlank(clientKey))
				{
			        logger.error("Client key is blank. Could not proceed.");
				}
				
	if ("".equals(clientKey))
	    clientKey = null;
	if (!clientQueries.containsKey(clientKey)) {
	    loadClientQueries();

	}
	return clientQueries.get(clientKey);
    }
    
    /**
     * Method overLoaded to accept clientKey. 
     * @param cKey
     * @return
     */
    public static Map<String, String> getQuery(String cKey) {
    	String clientKey = cKey != null ? cKey : null;
    	logger.debug(">>getQuery, clientKey:"+clientKey);
    	if(StringUtils.isBlank(clientKey))
    	{
    		logger.error("Client key is blank. Could not proceed.");
    	}
    				
    	if ("".equals(clientKey))
    	    clientKey = null;
    	if (!clientQueries.containsKey(clientKey)) {
    	    loadClientQueries();

    	}
    	return clientQueries.get(clientKey);
    }

    public static void main(String[] args) {
	logger.debug(getQuery().keySet());
    }
}
