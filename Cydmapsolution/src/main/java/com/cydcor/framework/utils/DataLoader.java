/**
 * 
 */
package com.cydcor.framework.utils;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.DBEntityComponent;
import com.cydcor.framework.model.DataLoadCommand;

/**
 * This Class is to load Data from database for any Given Component
 * 
 * @author ashwin
 * 
 */
public abstract class DataLoader {
	protected final Log logger = LogFactory.getLog(getClass());
	/**
	 * @param tableName
	 * @return
	 */
	public abstract DBEntityComponent loadTableMetadata(
			DataLoadCommand dataLoadCommand);

	/**
	 * @param component
	 * @return
	 */
	public abstract List<List<Object>> getData(DBEntityComponent component);

}
