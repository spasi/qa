/**
 * 
 */
package com.cydcor.framework.utils;


/**
 * @author agkrishnan
 *
 */
public interface SQLRetryable extends Retryable {

	public void run(RetryableRunner r) throws Exception;

}
