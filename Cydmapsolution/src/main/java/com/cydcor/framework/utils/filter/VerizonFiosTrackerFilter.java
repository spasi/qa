package com.cydcor.framework.utils.filter;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.model.VerizonFiosReport;



public class VerizonFiosTrackerFilter{
	
	public static boolean beginsWith(VerizonFiosReport object, String text, String colName){
		boolean bReturn = false;
		if("territory_name".equalsIgnoreCase(colName)){
			if(null != object.getTerritoryName() && !object.getTerritoryName().equals("")){
				String[] strArray = object.getTerritoryName().split(",");
				if(strArray.length > 0){
					for(String s : strArray){
						bReturn =  s.trim().toUpperCase().startsWith(String.valueOf(text).toUpperCase());	
						if(bReturn)
							break;
					}
				}
						
			}
		}else if("status".equalsIgnoreCase(colName)){
			if(null != object.getStatus() && !object.getStatus().equals("")){
				String[] strArray = object.getStatus().split(",");
				if(strArray.length > 0){
					for(String s : strArray){
						bReturn =  s.trim().toUpperCase().startsWith(String.valueOf(text).toUpperCase());	
						if(bReturn)
							break;
					}
				}
						
			}
		}else if("city".equalsIgnoreCase(colName)){
			if(null != object.getCity() && !object.getCity().equals("")){
				String[] strArray = object.getCity().split(",");
				if(strArray.length > 0){
					for(String s : strArray){
						bReturn =  s.trim().toUpperCase().startsWith(String.valueOf(text).toUpperCase());	
						if(bReturn)
							break;
					}
				}
						
			}
		}
		return bReturn;	
	}

	public static boolean isLike(VerizonFiosReport object, String text, java.lang.String colName) {
		boolean bReturn = false;
		if("territory_name".equals(colName)){
			if(null != object.getTerritoryName() && !object.getTerritoryName().equals("")){
				bReturn =  object.getTerritoryName().toUpperCase().trim().contains(String.valueOf(text).toUpperCase());
			}
		}else if("status".equals(colName)){
			if(null != object.getStatus() && !object.getStatus().equals("")){
				bReturn =  object.getStatus().toUpperCase().trim().contains(String.valueOf(text).toUpperCase());
			}
		}else if("city".equals(colName)){
			if(null != object.getCity() && !object.getCity().equals("")){
				bReturn =  object.getCity().toUpperCase().trim().contains(String.valueOf(text).toUpperCase());
			}
		}
		return bReturn;
	}

	public static boolean isNotLike(VerizonFiosReport object, String text, java.lang.String colName) {
		boolean bReturn = false;
		if("territory_name".equals(colName)){
			if(null != object.getTerritoryName() && !object.getTerritoryName().equals("")){
				bReturn =  !object.getTerritoryName().toUpperCase().trim().contains(String.valueOf(text).toUpperCase());
			}
		}else if("status".equals(colName)){
			if(null != object.getStatus() && !object.getStatus().equals("")){
				bReturn =  !object.getStatus().toUpperCase().trim().contains(String.valueOf(text).toUpperCase());
			}
		}else if("city".equals(colName)){
			if(null != object.getCity() && !object.getCity().equals("")){
				bReturn =  !object.getCity().toUpperCase().trim().contains(String.valueOf(text).toUpperCase());
			}
		}
		return bReturn;
	}

	public static boolean isGreaterThan(VerizonFiosReport object, String text,
			java.lang.String colName,boolean percentage) {
		boolean bReturn = false;
		if(percentage && object.getTotal()>0){
			if("total".equalsIgnoreCase(colName)){		
				bReturn = ((new BigDecimal(object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("total_available".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getTotalAvailable()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);			
			}else if("leads_thirty_days_and_under".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getLeadsThirtyDaysAndUnder()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("leads_between_thirtyone_and_ninety_days".equals(colName)){	
				bReturn = ((new BigDecimal(100.00*object.getLeadsBetweenThirtyOneAndNinetyDays()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("mdu_mdu_y".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getMduMduY()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("open_season_mdu_y".equals(colName)){	
				bReturn = ((new BigDecimal(100.00*object.getOpenSeasonMduY()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("disabled_mdu".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDisabledMdu()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("dsl_migration".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDslMigration()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("total_iont".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getTotalIont()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("iont_different_customers".equals(colName)){	
				bReturn = ((new BigDecimal(100.00*object.getIontDifferentCustomers()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("iont_same_customer".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getIontSameCustomer()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);						
			}else if("iont_disconnect_date_under_2_years".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDisconnectDateUnderTwoYears()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);						
			}else if("data_eligible".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDataEligible()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);							
			}else if("voice_and_data".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getVoiceAndData()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);						
			}else if("voice_data_tv".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getVoiceDataTv()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);						
			}else if("existing_voice_customer".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getExistingVoiceCustomer()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);							
			}else if("tv_penetration_rate".equals(colName)){
				bReturn = ((new BigDecimal(100.00*Double.valueOf(object.getTvPenetrationRate().replaceAll("%",""))/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);					
			}else if(StringUtils.isNotBlank(colName) && colName.contains("promo")){
				bReturn = ((new BigDecimal(100.00*object.getPromo().get(colName.split(":")[1])/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);					
			}else if("diff_in_count".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDiffInCount()/(object.getTotal()-object.getDiffInCount())).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) > 0);
			}else if("distance".equals(colName)){
				bReturn = (object.getDistance().compareTo(new Double(text)) > 0);
			}
		}else{
			if("total".equalsIgnoreCase(colName)){
				bReturn = (object.getTotal().compareTo(new Integer(text)) > 0);		
			}else if("total_available".equals(colName)){
				bReturn = (object.getTotalAvailable().compareTo(new Integer(text)) > 0);			
			}else if("leads_thirty_days_and_under".equals(colName)){
				bReturn = (object.getLeadsThirtyDaysAndUnder().compareTo(new Integer(text)) > 0);			
			}else if("leads_between_thirtyone_and_ninety_days".equals(colName)){
				bReturn = (object.getLeadsBetweenThirtyOneAndNinetyDays().compareTo(new Integer(text)) > 0);			
			}else if("mdu_mdu_y".equals(colName)){
				bReturn = (object.getMduMduY().compareTo(new Integer(text)) > 0);			
			}else if("open_season_mdu_y".equals(colName)){
				bReturn = (object.getOpenSeasonMduY().compareTo(new Integer(text)) > 0);			
			}else if("disabled_mdu".equals(colName)){
				bReturn = (object.getDisabledMdu().compareTo(new Integer(text)) > 0);
			}else if("dsl_migration".equals(colName)){
				bReturn = (object.getDslMigration().compareTo(new Integer(text)) > 0);			
			}else if("total_iont".equals(colName)){
				bReturn = (object.getTotalIont().compareTo(new Integer(text)) > 0);			
			}else if("iont_different_customers".equals(colName)){
				bReturn = (object.getIontDifferentCustomers().compareTo(new Integer(text)) > 0);			
			}else if("iont_same_customer".equals(colName)){
				bReturn = (object.getIontSameCustomer().compareTo(new Integer(text)) > 0);			
			}else if("iont_disconnect_date_under_2_years".equals(colName)){
				bReturn = (object.getDisconnectDateUnderTwoYears().compareTo(new Integer(text)) > 0);			
			}else if("data_eligible".equals(colName)){
				bReturn = (object.getDataEligible().compareTo(new Integer(text)) > 0);			
			}else if("voice_and_data".equals(colName)){
				bReturn = (object.getVoiceAndData().compareTo(new Integer(text)) > 0);			
			}else if("voice_data_tv".equals(colName)){
				bReturn = (object.getVoiceDataTv().compareTo(new Integer(text)) > 0);			
			}else if("existing_voice_customer".equals(colName)){
				bReturn = (object.getExistingVoiceCustomer().compareTo(new Integer(text)) > 0);			
			}else if("tv_penetration_rate".equals(colName)){
				bReturn = (Double.valueOf(object.getTvPenetrationRate().replaceAll("%","")).compareTo(new Double(text)) > 0);			
			}else if(StringUtils.isNotBlank(colName) && colName.contains("promo")){
				bReturn = (object.getPromo().get(colName.split(":")[1]).compareTo(new Integer(text)) > 0);		
			}else if("diff_in_count".equals(colName)){
				bReturn = (object.getDiffInCount().compareTo(new Integer(text)) > 0);			
			}else if("distance".equals(colName)){
				bReturn = (object.getDistance().compareTo(new Double(text)) > 0);
			}
		}
		return bReturn;
	}

	public static boolean isLessThan(VerizonFiosReport object, String text, java.lang.String colName,boolean percentage) {
		boolean bReturn = false;
		if(percentage && object.getTotal()>0){
			if("total".equalsIgnoreCase(colName)){
				bReturn = ((new BigDecimal(object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);					
			}else if("total_available".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getTotalAvailable()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("leads_thirty_days_and_under".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getLeadsThirtyDaysAndUnder()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("leads_between_thirtyone_and_ninety_days".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getLeadsBetweenThirtyOneAndNinetyDays()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("mdu_mdu_y".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getMduMduY()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("open_season_mdu_y".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getOpenSeasonMduY()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("disabled_mdu".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDisabledMdu()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);
			}else if("dsl_migration".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDslMigration()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("total_iont".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getTotalIont()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("iont_different_customers".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getIontDifferentCustomers()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("iont_same_customer".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getIontSameCustomer()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("iont_disconnect_date_under_2_years".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDisconnectDateUnderTwoYears()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("data_eligible".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDataEligible()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("voice_and_data".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getVoiceAndData()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("voice_data_tv".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getVoiceDataTv()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("existing_voice_customer".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getExistingVoiceCustomer()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);			
			}else if("tv_penetration_rate".equals(colName)){
				bReturn = ((new BigDecimal(100.00*Double.valueOf(object.getTvPenetrationRate().replaceAll("%",""))/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);				
			}else if(StringUtils.isNotBlank(colName) && colName.contains("promo")){
				bReturn = ((new BigDecimal(100.00*object.getPromo().get(colName.split(":")[1])/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);
			}else if("diff_in_count".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDiffInCount()/(object.getTotal()-object.getDiffInCount())).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) < 0);
			}else if("distance".equals(colName)){
				bReturn = (object.getDistance().compareTo(new Double(text)) < 0);
			}			
		}else{
			if("total".equalsIgnoreCase(colName)){
				bReturn = (object.getTotal().compareTo(new Integer(text)) < 0);		
			}else if("total_available".equals(colName)){
				bReturn = (object.getTotalAvailable().compareTo(new Integer(text)) < 0);			
			}else if("leads_thirty_days_and_under".equals(colName)){
				bReturn = (object.getLeadsThirtyDaysAndUnder().compareTo(new Integer(text)) < 0);			
			}else if("leads_between_thirtyone_and_ninety_days".equals(colName)){
				bReturn = (object.getLeadsBetweenThirtyOneAndNinetyDays().compareTo(new Integer(text)) < 0);			
			}else if("mdu_mdu_y".equals(colName)){
				bReturn = (object.getMduMduY().compareTo(new Integer(text)) < 0);			
			}else if("open_season_mdu_y".equals(colName)){
				bReturn = (object.getOpenSeasonMduY().compareTo(new Integer(text)) < 0);			
			}else if("disabled_mdu".equals(colName)){
				bReturn = (object.getDisabledMdu().compareTo(new Integer(text)) < 0);
			}else if("dsl_migration".equals(colName)){
				bReturn = (object.getDslMigration().compareTo(new Integer(text)) < 0);			
			}else if("total_iont".equals(colName)){
				bReturn = (object.getTotalIont().compareTo(new Integer(text)) < 0);			
			}else if("iont_different_customers".equals(colName)){
				bReturn = (object.getIontDifferentCustomers().compareTo(new Integer(text)) < 0);			
			}else if("iont_same_customer".equals(colName)){
				bReturn = (object.getIontSameCustomer().compareTo(new Integer(text)) < 0);			
			}else if("iont_disconnect_date_under_2_years".equals(colName)){
				bReturn = (object.getDisconnectDateUnderTwoYears().compareTo(new Integer(text)) < 0);			
			}else if("data_eligible".equals(colName)){
				bReturn = (object.getDataEligible().compareTo(new Integer(text)) < 0);			
			}else if("voice_and_data".equals(colName)){
				bReturn = (object.getVoiceAndData().compareTo(new Integer(text)) < 0);			
			}else if("voice_data_tv".equals(colName)){
				bReturn = (object.getVoiceDataTv().compareTo(new Integer(text)) < 0);			
			}else if("existing_voice_customer".equals(colName)){
				bReturn = (object.getExistingVoiceCustomer().compareTo(new Integer(text)) < 0);			
			}else if("tv_penetration_rate".equals(colName)){
				bReturn = (Double.valueOf(object.getTvPenetrationRate().replaceAll("%","")).compareTo(new Double(text)) < 0);			
			}else if(StringUtils.isNotBlank(colName) && colName.contains("promo")){
				bReturn = (object.getPromo().get(colName.split(":")[1]).compareTo(new Integer(text)) < 0);
			}else if("diff_in_count".equals(colName)){
				bReturn = (object.getDiffInCount().compareTo(new Integer(text)) < 0);			
			}else if("distance".equals(colName)){
				bReturn = (object.getDistance().compareTo(new Double(text)) < 0);
			}
		}
		return bReturn;
	}

	public static boolean isEquals(VerizonFiosReport object, String text, java.lang.String colName,boolean percentage) {
		boolean bReturn = false;
		if("territory_name".equals(colName)){
			if(null != object.getTerritoryName() && !object.getTerritoryName().equals("")){
				String[] strArray = object.getTerritoryName().split(",");
				if(strArray.length > 0){
					for(String s : strArray){
						bReturn =  s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
						if(bReturn)
							break;
					}
				}

			}						
		}else if("status".equals(colName)){
			if(null != object.getStatus() && !object.getStatus().equals("")){
				String[] strArray = object.getStatus().split(",");
				if(strArray.length > 0){
					for(String s : strArray){
						bReturn =  s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
						if(bReturn)
							break;
					}
				}

			}
		}else if("city".equals(colName)){
			if(null != object.getCity() && !object.getCity().equals("")){
				String[] strArray = object.getCity().split(",");
				if(strArray.length > 0){
					for(String s : strArray){
						bReturn =  s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
						if(bReturn)
							break;
					}
				}

			}
		}else if("date_to_be_worked".equals(colName)){
			if(null != object.getDateToBeWorked() && !object.getDateToBeWorked().equals("")){
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					Date ldt = sdf.parse(object.getDateToBeWorked());
					Date cdt = sdf.parse(text);
					if(ldt.equals(cdt)){
						bReturn = true;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					bReturn = false;
				}
			}
		}else if("date_last_worked".equals(colName)){
			if(null != object.getDatelastWorked() && !object.getDatelastWorked().equals("")){
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					Date ldt = sdf.parse(object.getDatelastWorked());
					Date cdt = sdf.parse(text);
					if(ldt.equals(cdt)){
						bReturn = true;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					bReturn = false;
				}
			}			
		}else if(percentage && object.getTotal()>0){
			if("total".equalsIgnoreCase(colName)){
				bReturn = ((new BigDecimal(object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);					
			}else if("total_available".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getTotalAvailable()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);	
			}else if("leads_thirty_days_and_under".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getLeadsThirtyDaysAndUnder()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("leads_between_thirtyone_and_ninety_days".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getLeadsBetweenThirtyOneAndNinetyDays()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("mdu_mdu_y".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getMduMduY()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("open_season_mdu_y".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getOpenSeasonMduY()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("disabled_mdu".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDisabledMdu()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);			
			}else if("dsl_migration".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDslMigration()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("total_iont".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getTotalIont()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("iont_different_customers".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getIontDifferentCustomers()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("iont_same_customer".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getIontSameCustomer()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("iont_disconnect_date_under_2_years".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDisconnectDateUnderTwoYears()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("data_eligible".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDataEligible()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("voice_and_data".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getVoiceAndData()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("voice_data_tv".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getVoiceDataTv()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("existing_voice_customer".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getExistingVoiceCustomer()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);						
			}else if("tv_penetration_rate".equals(colName)){
				bReturn = ((new BigDecimal(100.00*Double.valueOf(object.getTvPenetrationRate().replaceAll("%",""))/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);		
			}else if(StringUtils.isNotBlank(colName) && colName.contains("promo")){
				bReturn = ((new BigDecimal(100.00*object.getPromo().get(colName.split(":")[1])/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);
			}else if("diff_in_count".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDiffInCount()/(object.getTotal()-object.getDiffInCount())).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) == 0);
			}else if("distance".equals(colName)){
				bReturn = (object.getDistance().compareTo(new Double(text)) == 0);
			}				
		}else{
			if("total".equalsIgnoreCase(colName)){
				bReturn = (object.getTotal().compareTo(new Integer(text)) == 0);		
			}else if("total_available".equals(colName)){
				bReturn = (object.getTotalAvailable().compareTo(new Integer(text)) == 0);			
			}else if("leads_thirty_days_and_under".equals(colName)){
				bReturn = (object.getLeadsThirtyDaysAndUnder().compareTo(new Integer(text)) == 0);			
			}else if("leads_between_thirtyone_and_ninety_days".equals(colName)){
				bReturn = (object.getLeadsBetweenThirtyOneAndNinetyDays().compareTo(new Integer(text)) == 0);			
			}else if("mdu_mdu_y".equals(colName)){
				bReturn = (object.getMduMduY().compareTo(new Integer(text)) == 0);			
			}else if("open_season_mdu_y".equals(colName)){
				bReturn = (object.getOpenSeasonMduY().compareTo(new Integer(text)) == 0);			
			}else if("disabled_mdu".equals(colName)){
				bReturn = (object.getDisabledMdu().compareTo(new Integer(text)) == 0);
			}else if("dsl_migration".equals(colName)){
				bReturn = (object.getDslMigration().compareTo(new Integer(text)) == 0);			
			}else if("total_iont".equals(colName)){
				bReturn = (object.getTotalIont().compareTo(new Integer(text)) == 0);			
			}else if("iont_different_customers".equals(colName)){
				bReturn = (object.getIontDifferentCustomers().compareTo(new Integer(text)) == 0);			
			}else if("iont_same_customer".equals(colName)){
				bReturn = (object.getIontSameCustomer().compareTo(new Integer(text)) == 0);			
			}else if("iont_disconnect_date_under_2_years".equals(colName)){
				bReturn = (object.getDisconnectDateUnderTwoYears().compareTo(new Integer(text)) == 0);			
			}else if("data_eligible".equals(colName)){
				bReturn = (object.getDataEligible().compareTo(new Integer(text)) == 0);			
			}else if("voice_and_data".equals(colName)){
				bReturn = (object.getVoiceAndData().compareTo(new Integer(text)) == 0);			
			}else if("voice_data_tv".equals(colName)){
				bReturn = (object.getVoiceDataTv().compareTo(new Integer(text)) == 0);			
			}else if("existing_voice_customer".equals(colName)){
				bReturn = (object.getExistingVoiceCustomer().compareTo(new Integer(text)) == 0);			
			}else if("tv_penetration_rate".equals(colName)){
				bReturn = (Double.valueOf(object.getTvPenetrationRate().replaceAll("%","")).compareTo(new Double(text)) == 0);			
			}else if(StringUtils.isNotBlank(colName) && colName.contains("promo")){
				bReturn = (object.getPromo().get(colName.split(":")[1]).compareTo(new Integer(text)) == 0);
			}else if("diff_in_count".equals(colName)){
				bReturn = (object.getDiffInCount().compareTo(new Integer(text)) == 0);			
			}else if("distance".equals(colName)){
				bReturn = (object.getDistance().compareTo(new Double(text)) == 0);
			}
		}	
			return bReturn;
		}

	public static boolean isNotEquals(VerizonFiosReport object, String text, java.lang.String colName,boolean percentage) {
		boolean bReturn = false;
		if("territory_name".equals(colName)){
			if(null != object.getTerritoryName() && !object.getTerritoryName().equals("")){
				String[] strArray = object.getTerritoryName().split(",");
				if(strArray.length > 0){
					for(String s : strArray){
						bReturn =  !s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
						if(bReturn)
							break;
					}
				}
			}		
		}else if("status".equals(colName)){
			if(null != object.getStatus() && !object.getStatus().equals("")){
				String[] strArray = object.getStatus().split(",");
				if(strArray.length > 0){
					for(String s : strArray){
						bReturn =  !s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
						if(bReturn)
							break;
					}
				}

			}
		}else if("city".equals(colName)){
			if(null != object.getCity() && !object.getCity().equals("")){
				String[] strArray = object.getCity().split(",");
				if(strArray.length > 0){
					for(String s : strArray){
						bReturn =  !s.trim().toUpperCase().equals(String.valueOf(text).toUpperCase());
						if(bReturn)
							break;
					}
				}

			}
		}else if("date_to_be_worked".equals(colName)){
			if(null != object.getDateToBeWorked() && !object.getDateToBeWorked().equals("")){
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					Date ldt = sdf.parse(object.getDateToBeWorked());
					Date cdt = sdf.parse(text);
					if(!ldt.equals(cdt)){
						bReturn = true;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					bReturn = false;
				}
			}
		}else if("date_last_worked".equals(colName)){
			if(null != object.getDatelastWorked() && !object.getDatelastWorked().equals("")){
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					Date ldt = sdf.parse(object.getDatelastWorked());
					Date cdt = sdf.parse(text);
					if(!ldt.equals(cdt)){
						bReturn = true;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					bReturn = false;
				}
			}			
		}else if(percentage && object.getTotal()>0){
			if("total".equalsIgnoreCase(colName)){
				bReturn = ((new BigDecimal(object.getTotal()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);							
			}else if("total_available".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getTotalAvailable()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("leads_thirty_days_and_under".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getLeadsThirtyDaysAndUnder()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("leads_between_thirtyone_and_ninety_days".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getLeadsBetweenThirtyOneAndNinetyDays()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("mdu_mdu_y".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getMduMduY()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("open_season_mdu_y".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getOpenSeasonMduY()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("disabled_mdu".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDisabledMdu()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);						
			}else if("dsl_migration".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDslMigration()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("total_iont".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getTotalIont()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("iont_different_customers".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getIontDifferentCustomers()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("iont_same_customer".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getIontSameCustomer()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("iont_disconnect_date_under_2_years".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDisconnectDateUnderTwoYears()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("data_eligible".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDataEligible()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("voice_and_data".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getVoiceAndData()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("voice_data_tv".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getVoiceDataTv()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("existing_voice_customer".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getExistingVoiceCustomer()/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if("tv_penetration_rate".equals(colName)){
				bReturn = ((new BigDecimal(100.00*Double.valueOf(object.getTvPenetrationRate().replaceAll("%",""))/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);								
			}else if(StringUtils.isNotBlank(colName) && colName.contains("promo")){
				bReturn = ((new BigDecimal(100.00*object.getPromo().get(colName.split(":")[1])/object.getTotal()).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);
			}else if("diff_in_count".equals(colName)){
				bReturn = ((new BigDecimal(100.00*object.getDiffInCount()/(object.getTotal()-object.getDiffInCount())).setScale(0, BigDecimal.ROUND_HALF_UP)).compareTo(new BigDecimal(text)) != 0);
			}else if("distance".equals(colName)){
				bReturn = (object.getDistance().compareTo(new Double(text)) != 0);
			}					
		}else{
			if("total".equalsIgnoreCase(colName)){
				bReturn = (object.getTotal().compareTo(new Integer(text)) != 0);		
			}else if("total_available".equals(colName)){
				bReturn = (object.getTotalAvailable().compareTo(new Integer(text)) != 0);			
			}else if("leads_thirty_days_and_under".equals(colName)){
				bReturn = (object.getLeadsThirtyDaysAndUnder().compareTo(new Integer(text)) != 0);			
			}else if("leads_between_thirtyone_and_ninety_days".equals(colName)){
				bReturn = (object.getLeadsBetweenThirtyOneAndNinetyDays().compareTo(new Integer(text)) != 0);			
			}else if("mdu_mdu_y".equals(colName)){
				bReturn = (object.getMduMduY().compareTo(new Integer(text)) != 0);			
			}else if("open_season_mdu_y".equals(colName)){
				bReturn = (object.getOpenSeasonMduY().compareTo(new Integer(text)) != 0);			
			}else if("disabled_mdu".equals(colName)){
				bReturn = (object.getDisabledMdu().compareTo(new Integer(text)) != 0);
			}else if("dsl_migration".equals(colName)){
				bReturn = (object.getDslMigration().compareTo(new Integer(text)) != 0);			
			}else if("total_iont".equals(colName)){
				bReturn = (object.getTotalIont().compareTo(new Integer(text)) != 0);			
			}else if("iont_different_customers".equals(colName)){
				bReturn = (object.getIontDifferentCustomers().compareTo(new Integer(text)) != 0);			
			}else if("iont_same_customer".equals(colName)){
				bReturn = (object.getIontSameCustomer().compareTo(new Integer(text)) != 0);			
			}else if("iont_disconnect_date_under_2_years".equals(colName)){
				bReturn = (object.getDisconnectDateUnderTwoYears().compareTo(new Integer(text)) != 0);			
			}else if("data_eligible".equals(colName)){
				bReturn = (object.getDataEligible().compareTo(new Integer(text)) != 0);			
			}else if("voice_and_data".equals(colName)){
				bReturn = (object.getVoiceAndData().compareTo(new Integer(text)) != 0);			
			}else if("voice_data_tv".equals(colName)){
				bReturn = (object.getVoiceDataTv().compareTo(new Integer(text)) != 0);			
			}else if("existing_voice_customer".equals(colName)){
				bReturn = (object.getExistingVoiceCustomer().compareTo(new Integer(text)) != 0);			
			}else if("tv_penetration_rate".equals(colName)){
				bReturn = (Double.valueOf(object.getTvPenetrationRate().replaceAll("%","")).compareTo(new Double(text)) != 0);			
			}else if(StringUtils.isNotBlank(colName) && colName.contains("promo")){
				bReturn = (object.getPromo().get(colName.split(":")[1]).compareTo(new Integer(text)) != 0);
			}else if("diff_in_count".equals(colName)){
				bReturn = (object.getDiffInCount().compareTo(new Integer(text)) != 0);			
			}else if("distance".equals(colName)){
				bReturn = (object.getDistance().compareTo(new Double(text)) != 0);
			}	
		}
			return bReturn;
	}

	public static boolean isOnOrBefore(VerizonFiosReport object, String text,java.lang.String colName) {
		boolean bReturn = false;
		if("date_to_be_worked".equals(colName)){
			if(null != object.getDateToBeWorked() && !object.getDateToBeWorked().equals("")){
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					Date ldt = sdf.parse(object.getDateToBeWorked());
					Date cdt = sdf.parse(text);
					if(ldt.before(cdt) || ldt.equals(cdt)){
						bReturn = true;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					bReturn = false;
				}
			}
		}else if("date_last_worked".equals(colName)){
			if(null != object.getDatelastWorked() && !object.getDatelastWorked().equals("")){
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					Date ldt = sdf.parse(object.getDatelastWorked());
					Date cdt = sdf.parse(text);
					if(ldt.before(cdt) || ldt.equals(cdt)){
						bReturn = true;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					bReturn = false;
				}
			}			
		}			
		return bReturn;
	}

	public static boolean isOnOrAfter(VerizonFiosReport object, String text, java.lang.String colName) {
		boolean bReturn = false;
		if("date_to_be_worked".equals(colName)){
			if(null != object.getDateToBeWorked() && !object.getDateToBeWorked().equals("")){
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					Date ldt = sdf.parse(object.getDateToBeWorked());
					Date cdt = sdf.parse(text);
					if(ldt.after(cdt) || ldt.equals(cdt)){
						bReturn = true;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					bReturn = false;
				}
			}
		}else if("date_last_worked".equals(colName)){
			if(null != object.getDatelastWorked() && !object.getDatelastWorked().equals("")){
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					Date ldt = sdf.parse(object.getDatelastWorked());
					Date cdt = sdf.parse(text);
					if(ldt.after(cdt) || ldt.equals(cdt)){
						bReturn = true;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					bReturn = false;
				}
			}			
		}			
		return bReturn;
	}
	
    public static List<VerizonFiosReport> filterList(List<VerizonFiosReport> originalList, String text, String columnName, String opName, boolean percentage) {
        List<VerizonFiosReport> filterList = new LinkedList<VerizonFiosReport>();
        
        	if("BEGIN_WITH".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (beginsWith(object, text, columnName)){
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("IS_LIKE".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (isLike(object, text,columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("NOT_LIKE".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (isNotLike(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("GREATER_THAN".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (isGreaterThan(object, text, columnName, percentage)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("LESS_THAN".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (isLessThan(object, text, columnName, percentage)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("ON_BEFORE".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (isOnOrBefore(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("ON_AFTER".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (isOnOrAfter(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("EQUALS".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (isEquals(object, text, columnName, percentage)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("NOT_EQUALS".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (isNotEquals(object, text, columnName, percentage)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}
        	
        return filterList;
    }
	
	public static void main(java.lang.String[] args) {
	    List<VerizonFiosReport>	recordList = new LinkedList<VerizonFiosReport>();
		for (int i = 0; i < 100; i++) {
			Random rand = new Random();
			int  n = rand.nextInt(30) + i;
			int  m = rand.nextInt(30) + i;
			VerizonFiosReport vr = new VerizonFiosReport();
			vr.setDataEligible(n*i);
			vr.setDatelastWorked("2014-04-01");
			vr.setDateToBeWorked("2014-04-30");
			vr.setDisconnectDateUnderTwoYears(n*3);
			vr.setDslMigration(n+45);
			vr.setExistingVoiceCustomer(n+100);
//			vr.setExistingVoiceCustomers(n*6);
			vr.setIontDifferentCustomers(n+55);
			vr.setIontSameCustomer(n+77);
			vr.setLeadsBetweenThirtyOneAndNinetyDays(n+66);
			vr.setLeadsThirtyDaysAndUnder(n+95);
			vr.setMduMduY(n+23);
			vr.setOpenSeasonMduY(n+56);
			vr.setDisabledMdu(m*n*6);
			vr.setPercentageNewlyLitBetweenThirtyOneAndNinetyDays(new Double("56.56"));
			vr.setPercentageNewlyLitThirtyDaysOrLess(new Double(".27")*n);
//			vr.setPromo(2*n*i);
			vr.setRowId(n);
			vr.setStatus("statusstatus1");
			vr.setTerritoryName("DORCHESTER"+i);
			vr.setTotal(100000);
			vr.setTotalAvailable(23235);
			vr.setCLLiName("CLLINameForNotes");
			vr.setTotalIont(111+n);
			vr.setTvPenetrationRate(".59");
			vr.setVoiceAndData(n+565);
			vr.setVoiceDataTv(n+665);
			vr.setNotes("NotesNotes");
			vr.setSortSeq(3);
			vr.setDistance(12.5+i*m);
			recordList.add(vr);
		}

//		List<Comparator> comparators = new ArrayList<Comparator>();
//		comparators.add(new GenericComparator(true,"getTotal"));
//		comparators.add(new GenericComparator(false,"getMduMduY"));
//		comparators.add(new GenericComparator(false,"getDisabled"));
//
//		DelegateComparator delegateComparator = new DelegateComparator(comparators);
//		Collections.sort(recordList, delegateComparator);
//		for(int i=0;i<recordList.size();i++)
//			System.out.println("total["+i+"]="+recordList.get(i).getTotal()+"   "+recordList.get(i).getMduMduY()+"   "+recordList.get(i).getDisabledMdu());
		for(VerizonFiosReport record: recordList)
			System.out.println("record.distance="+record.getDistance());
		List<VerizonFiosReport> list = VerizonFiosTrackerFilter.filterList(recordList, "100","distance", "GREATER_THAN", true);
		System.out.println("size="+list.size());
	}
}
