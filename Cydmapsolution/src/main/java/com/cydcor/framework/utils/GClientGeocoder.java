package com.cydcor.framework.utils;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.cydcor.framework.model.glocation.Kml;
import com.cydcor.framework.test.SpringPage;


public class GClientGeocoder {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(GClientGeocoder.class);

    private static SpringPage springPage = new SpringPage();

    /**
     * @param name
     * @return
     * @throws HttpException
     * @throws IOException
     */
    public static Kml getGlocation(String name) throws HttpException, IOException {
	HttpClient client = null;
	GetMethod method = null;
	Kml kml = null;
	String googleKey = CydcorUtils.getProperty("GoogleKey");
	String url = "http://maps.google.com/maps/geo?q=" + name + "&output=kml&sensor=false&client=" + CydcorUtils.getProperty("ClientID");

	client = new HttpClient();
	try {
	    // System.out.println(URLSigner.getInstance().getSignedURL(url));
	    method = new GetMethod(URLSigner.getInstance().getSignedURL(url));
	    // Calendar startTime = new GregorianCalendar();
	    client.executeMethod(method);
	    // Calendar endTime = new GregorianCalendar();
	    /*
	     * System.out.println("TimeTaken :" + (endTime.getTime().getTime() -
	     * startTime.getTime() .getTime()));
	     */
	    String response = method.getResponseBodyAsString();
	    // System.out.println(response);
	    kml = (Kml) XStreamUtils.getObjectFromXml(Kml.class, response);
	    kml.setReponseFromGoogle(response);
	} catch (Exception e) {
	    // TODO: handle exception
	    logger.error("getGlocation(String)", e); //$NON-NLS-1$
	}
	return kml;
    }

    /**
     * @param args
     * @throws HttpException
     * @throws IOException
     */
    public static void main(String[] args) throws HttpException, IOException {
	// getGlocation("314,East,Highland,Mall,Blvd,Suite,411");

	Kml kml = getGlocation("314,East,Highland,Mall,Blvd,Suite,411");

	// Kml kml =
	// getGlocation("35,West,35th,Street,Suite,,,903,New,York,NY,10001,US");
	if (logger.isDebugEnabled()) {
	    logger.debug("main(String[]) - " + kml.getReponseFromGoogle()); //$NON-NLS-1$
	}

	// 8201 Greensboro Dr #605, McLean, VA 22102, USA
	// 7131 San Francisco St, Highland, CA 92346, USA
	List<String> directions = getDrivingDirections("8201 Greensboro Dr #605, McLean, VA 22102, USA", "7131 San Francisco St, Highland, CA 92346, USA");
	for (String direction : directions) {
	    if (logger.isDebugEnabled()) {
		logger.debug("main(String[]) - " + direction); //$NON-NLS-1$
	    }
	}
    }

    /**
     * @param from
     * @param to
     * @return
     * @throws HttpException
     * @throws IOException
     */
    public static List<String> getDrivingDirections(String from, String to) throws HttpException, IOException {

	try {
	    // String url =
	    // "http://maps.google.com/?saddr=9227+Haven+Ave,+Rancho+Cucamonga,+CA+91730&daddr=10938+Rose+Ave,+Los+Angeles,+CA+90034&output=kml";
	    String url = "http://maps.google.com/?saddr=" + from.replaceAll(" ", "+").replaceAll("#", "+") + "&daddr="
		    + to.replaceAll(" ", "+").replaceAll("#", "+") + "&output=kml";

	    HttpClient client = null;
	    GetMethod method = null;
	    Kml kml = null;

	    client = new HttpClient();
	    method = new GetMethod(url);
	    client.executeMethod(method);
	    String response = method.getResponseBodyAsString();
	    // System.out.println(response);

	    List<String> directions = new ArrayList<String>();

	    directions.add(from);

	    int beginIndex = 0;

	    int endIndex = 0;
	    beginIndex = response.indexOf("<Placemark><name>");
	    endIndex = response.indexOf("</name>", beginIndex);

	    while (beginIndex != -1) {
		try {
		    directions.add(response.substring(beginIndex + "<Placemark><name>".length(), endIndex));
		    beginIndex = response.indexOf("<Placemark><name>", endIndex);
		    endIndex = response.indexOf("</name>", beginIndex);
		} catch (Exception e) {
		    logger.error("getDrivingDirections(String, String)", e); //$NON-NLS-1$
		    break;

		}
	    }

	    directions.add(to);
	    // System.out.println(response);
	    //
	    // configure the document builder factory
	    return directions;
	} catch (Exception e) {
	    return Collections.EMPTY_LIST;
	}

    }

    public static List<String> getV3DrivingDirections(String from, String to) throws HttpException, IOException {

	try {
	    // String url =
	    // "http://maps.google.com/?saddr=9227+Haven+Ave,+Rancho+Cucamonga,+CA+91730&daddr=10938+Rose+Ave,+Los+Angeles,+CA+90034&output=kml";
	    String url = "http://maps.googleapis.com/maps/api/directions/xml?origin=" + from.replaceAll(" ", "+").replaceAll("#", "+") + "&destination="
		    + to.replaceAll(" ", "+").replaceAll("#", "+") + "&sensor=false";

	    HttpClient client = null;
	    GetMethod method = null;

	    client = new HttpClient();
	    method = new GetMethod(url);
	    client.executeMethod(method);
	    String response = method.getResponseBodyAsString();
	    // System.out.println(response);

	    List<String> directions = new ArrayList<String>();

	    directions.add(from);

	    int beginIndex = 0;

	    int endIndex = 0;
	    beginIndex = response.indexOf("<html_instructions>");
	    endIndex = response.indexOf("</html_instructions>", beginIndex);

	    while (beginIndex != -1) {
		try {
		    directions.add(StringEscapeUtils.unescapeHtml(response.substring(beginIndex + "<html_instructions>".length(), endIndex)).replaceAll(
			    "\\<.*?>", ""));
		    beginIndex = response.indexOf("<html_instructions>", endIndex);
		    endIndex = response.indexOf("</html_instructions>", beginIndex);
		} catch (Exception e) {
		    logger.error("getDrivingDirections(String, String)", e); //$NON-NLS-1$
		    break;

		}
	    }

	    directions.add(to);
	    // System.out.println(response);
	    //
	    // configure the document builder factory
	    return directions;
	} catch (Exception e) {
	    return Collections.EMPTY_LIST;
	}

    }
    
    /**
     * Get Geocode location for address
     * @param address
     * @return
     */
    public static String getGlocationV3(String address) {
    	String url = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + address
    		+ "&sensor=false&region=us&client=" + CydcorUtils.getProperty("ClientID");

    	HttpClient client = new HttpClient();
    	GetMethod method;
    	try {
    	    method = new GetMethod(URLSigner.getInstance().getSignedURL(url));
    	    client.executeMethod(method);
    	    return method.getResponseBodyAsString();
    	} catch (InvalidKeyException e) {
    	    e.printStackTrace();
    	} catch (MalformedURLException e) {
    	    e.printStackTrace();
    	} catch (NoSuchAlgorithmException e) {
    	    e.printStackTrace();
    	} catch (UnsupportedEncodingException e) {
    	    e.printStackTrace();
    	} catch (URISyntaxException e) {
    	    e.printStackTrace();
    	} catch (HttpException e) {
    	    e.printStackTrace();
    	} catch (IOException e) {
    	    e.printStackTrace();
    	}
    	return null;
    }
    
    private static XPath xpath = XPathFactory.newInstance().newXPath();
    
    /**
     * Get Geocode
     * @param responseText
     * @return
     */
    public static String getGeocode(String responseText) {
    	String lat = "";
    	String lng = "";

    	try {
    		InputSource inStream = new InputSource();
    		inStream.setCharacterStream(new StringReader(responseText));
    		Document xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inStream);
	    
    		try {
    			XPathExpression xPathExpression = xpath.compile("/GeocodeResponse/result/geometry/location/lat/text()");
    			lat = (String) xPathExpression.evaluate(xmlDocument, XPathConstants.STRING);
    			xPathExpression = xpath.compile("/GeocodeResponse/result/geometry/location/lng/text()");
    			lng = (String) xPathExpression.evaluate(xmlDocument, XPathConstants.STRING);
    		} catch (XPathExpressionException ex) {
    			logger.error("This error is from the KML: " + ex);
    			return null;
    		}
    	} catch (Exception e) {
    		logger.error("Could not parse Response.", e);
    		logger.debug("Could not parse Response. responseText="+responseText);
    	}
    	return "POINT(" + lng + " " + lat + ")";
    }
    
    /**
     * Get Geocoded address
     * @param responseText
     * @return
     */
    public static String getGeocodedAddress(String responseText) {
    	String fullAddr = "";

    	try {
    		InputSource inStream = new InputSource();
    		inStream.setCharacterStream(new StringReader(responseText));
    		Document xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inStream);

    		try {
    			XPathExpression xPathExpression = xpath.compile("/GeocodeResponse/result/formatted_address/text()");
    			fullAddr = (String) xPathExpression.evaluate(xmlDocument, XPathConstants.STRING);
    		} catch (XPathExpressionException ex) {
    			ex.printStackTrace();
    			return null;
    		}
    	} catch (Exception e) {    		
    		logger.error("Could not parse Response Addresss.", e);    
    		logger.debug("Could not parse Response Addresss. responseText="+responseText);
    	}
    	return fullAddr;
    }

}
