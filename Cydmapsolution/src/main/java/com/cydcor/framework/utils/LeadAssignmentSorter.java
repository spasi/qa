package com.cydcor.framework.utils;

import java.util.Comparator;

import com.cydcor.framework.model.LeadAssignment;

public class LeadAssignmentSorter implements Comparator<LeadAssignment> {
	
	private String sortOrder = "ASC";
	public LeadAssignmentSorter(String sortOrder){
		this.sortOrder = sortOrder;
	}
	
	public LeadAssignmentSorter(){
	}
	
	public int compare(LeadAssignment o1, LeadAssignment o2) {
		int retStatus=0;
		try{
			// TODO Auto-generated method stub
			int i = o1.getClli().compareTo(o2.getClli());
			int j = 0;
			if(null == o1.getLfa() && null == o2.getLfa()){
				j = 0;
			}else if(null != o1.getLfa() && null == o2.getLfa()){
				j = 1;
			}else if(null == o1.getLfa() && null != o2.getLfa()){
				j = -1;
			}else {
				j = o1.getLfa().compareTo(o2.getLfa());
			}
			
			if("ASC".equals(this.sortOrder)){
		    	if ( i < 0 ) {  
		    		retStatus = -1;  
		    	} else if ( i > 0 ) {  
		    		retStatus = 1;  
		    	} else {  
		    		if ( j < 0 ) {  
		    			retStatus = -1;  
		    		} else if ( j > 0 ) {  
		    			retStatus = 1;  
		    		} else {  
		    			retStatus = 0;  
		    		}  
		    	}
	    	}else{
	    		if ( i > 0 ) {  
	                retStatus = -1;  
	    		} else if ( i < 0 ) {  
	                retStatus = 1;  
	    		} else {  
	    			if ( j < 0 ) {  
	    				retStatus = -1;  
	    			} else if ( j > 0 ) {  
	    				retStatus = 1;  
	    			} else {  
	    				retStatus = 0;  
	    			}  
	    		}
	    	}
	    	
		}catch(Exception e){
			System.out.println("Wxception :"+e);
		}  
		return retStatus;
	}
}
