package com.cydcor.framework.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool.impl.GenericObjectPool;


public class ODSessionPool extends GenericObjectPool {
	private static final transient Log logger = LogFactory.getLog(ODSessionPool.class);
    private static ODSessionFactory factory;
    private static ODSessionPool sessionPool = null;
    private static int maxConnections = 1;
    private static int maxWaittime = 0;
    private static int minConnections = 1;
    private static long idleTime = -1;

    private ODSessionPool(ODSessionFactory inFactory) {
        super(factory);
        factory = inFactory;

        try {
            maxConnections = 10;
        } catch (Exception e) {
          logger.debug(e);
        }

        try {
            maxWaittime = 2000;
        } catch (Exception e) {
        	logger.debug(e);
        }

        try {
            minConnections = 1;
        } catch (Exception e) {
        	logger.debug(e);
        }

        try {
            idleTime = 5;
        } catch (Exception e) {
        	logger.debug(e);
        	}
    }

    /**
     * Singleton method to get the ODSessionPool instance.
     * @return
     */
    public static ODSessionPool getInstance() {
        if (sessionPool == null) {
            synchronized (ODSessionPool.class) {
                if (sessionPool == null) {
                    factory = new ODSessionFactory();
                    sessionPool = new ODSessionPool(factory);
                    sessionPool.setMaxActive(maxConnections);
                    sessionPool.setMaxIdle(maxConnections);
                    sessionPool.setMaxWait(maxWaittime);
                    sessionPool.setMinIdle(minConnections);
                    sessionPool.setTestOnBorrow(false);
                    sessionPool.setTestOnReturn(false);
                    sessionPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_GROW);
                    sessionPool.setMinEvictableIdleTimeMillis(idleTime * 60 * 1000);
                }
            }
        }

        return sessionPool;
    }

    /**
     * This method gives an active OD Session Id
     * @return session id
     * @throws Exception
     */
    public String getODSessionId() throws Exception {
        return (String) borrowObject();
    }

    /**
     * This method makes the incoming Session Id idle.
     * @return session id
     * @throws Exception
     */
    public void returnODSession(String sessionId) throws Exception {
        if (sessionId != null) {
            returnObject(sessionId);
        }
    }

    /**
     * This method destroys incoming session.
     * @return session id
     * @throws Exception
     */
    public void destroyODSession(String sessionId) {
        try {
            if (sessionId != null) {
                invalidateObject(sessionId);
            }
        } catch (Exception e) {
        	logger.debug(e);
        }
    }

    /**
     * Method which refreshes each OD session. This method has to be called
     * from a sceduler (which is set less than OD session time out)
     *
     */
    public void refreshSessions() {
        try {
            logger.debug("Starting Eviction of idle Sessions in the pool");
            sessionPool.evict();
            logger.debug("Ending Eviction of idle Sessions in the pool");
        } catch (Exception exp) {
            exp.printStackTrace();
        }
    }

    public void close() {
        try {
            if (sessionPool != null) {
                super.close();
                sessionPool = null;
            }
        } catch (Exception e) {
           logger.debug(e);
        }
    }
}
