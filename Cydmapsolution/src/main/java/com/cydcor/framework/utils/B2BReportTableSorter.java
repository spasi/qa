package com.cydcor.framework.utils;

import java.text.SimpleDateFormat;
import java.util.Comparator;

import com.cydcor.framework.model.B2BReport;

public class B2BReportTableSorter implements Comparator<B2BReport> {

	private String columnId ;
	private String sortOrder;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy"); //Changed mm to MM to fix issue with date column sorting incorrectly
	
	public B2BReportTableSorter(String columnId,String sortOrder){
		this.columnId = columnId;
		this.sortOrder = sortOrder;
	}

	public int compare(B2BReport o1, B2BReport o2) {
		int retStatus=0;
		try{
			// TODO Auto-generated method stub
			int i = o1.getSortSeq().compareTo(o2.getSortSeq());
			int j = 0;
			if(this.columnId.equals("TERRITORY")){
				j = o1.getTerritoryName().compareTo(o2.getTerritoryName());
			}if(this.columnId.equals("CLOSED_DT")){
				if(null == o1.getClosedDate() && null == o2.getClosedDate()){
					j = 0;
				}else if(null != o1.getClosedDate() && null == o2.getClosedDate()){
					j = -1;
				}else if(null == o1.getClosedDate() && null != o2.getClosedDate()){
					j = 1;
				}else {
					j = sdf.parse(o1.getClosedDate()).compareTo(sdf.parse(o2.getClosedDate()));
				}
			}if(this.columnId.equals("DTBW")){
				if(null == o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
					j = 0;
				}else if(null != o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
					j = -1;
				}else if(null == o1.getDateToBeWorked() && null != o2.getDateToBeWorked()){
					j = 1;
				}else {
					j = sdf.parse(o1.getDateToBeWorked()).compareTo(sdf.parse(o2.getDateToBeWorked()));
				}
			}else if(this.columnId.equals("TOWN")){
				if(null == o1.getTown() && null == o2.getTown()){
					j = 0;
				}else if(null != o1.getTown() && null == o2.getTown()){
					j = -1;
				}else if(null == o1.getTown() && null != o2.getTown()){
					j = 1;
				}else {
					j = o1.getTown().compareTo(o2.getTown());
				}
			}else if(this.columnId.equals("DISTANCE")){
				if(null == o1.getDistance() && null == o2.getDistance()){
					j = 0;
				}else if(null != o1.getDistance() && null == o2.getDistance()){
					if("ASC".equals(this.sortOrder)){j = -1;}else{j = 1;}
				}else if(null == o1.getDistance() && null != o2.getDistance()){
					if("ASC".equals(this.sortOrder)){j = 1;}else{j = -1;}
				}else {
					j = new Double(o1.getDistance().replaceAll(",", "")).compareTo(new Double(o2.getDistance().replaceAll(",", "")));
				}
			}else if(this.columnId.equals("TOTAL")){
				if(null == o1.getTotal() && null == o2.getTotal()){
					j = 0;
				}else if(null != o1.getTotal() && null == o2.getTotal()){
					j = -1;
				}else if(null == o1.getTotal() && null != o2.getTotal()){
					j = 1;
				}else {
					j = o1.getTotal().compareTo(o2.getTotal());
				}
			}else if(this.columnId.equals("SHARED_BY")){
				if(null == o1.getSharedBy() && null == o2.getSharedBy()){
					j = 0;
				}else if(null != o1.getSharedBy() && null == o2.getSharedBy()){
					j = -1;
				}else if(null == o1.getSharedBy() && null != o2.getSharedBy()){
					j = 1;
				}else {
					j = o1.getSharedBy().compareTo(o2.getSharedBy());
				}
			}else if(this.columnId.equals("NUMBER_OF_SHARED_LEADS")){
				if(null == o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
					j = 0;
				}else if(null != o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
					j = -1;
				}else if(null == o1.getNumOfSharedLeads() && null != o2.getNumOfSharedLeads()){
					j = 1;
				}else {
					j = o1.getNumOfSharedLeads().compareTo(o2.getNumOfSharedLeads());
				}
			}else if(this.columnId.equals("FIOS_DATA")){
				if(null == o1.getFiosData() && null == o2.getFiosData()){
					j = 0;
				}else if(null != o1.getFiosData() && null == o2.getFiosData()){
					j = -1;
				}else if(null == o1.getFiosData() && null != o2.getFiosData()){
					j = 1;
				}else {
					j = o1.getFiosData().compareTo(o2.getFiosData());
				}
			}else if(this.columnId.equals("FIOS_MIGRATION")){
				if(null == o1.getFiosMigration() && null == o2.getFiosMigration()){
					j = 0;
				}else if(null != o1.getFiosMigration() && null == o2.getFiosMigration()){
					j = -1;
				}else if(null == o1.getFiosMigration() && null != o2.getFiosMigration()){
					j = 1;
				}else {
					j = o1.getFiosMigration().compareTo(o2.getFiosMigration());
				}
			}else if(this.columnId.equals("FIOS_TV")){
				if(null == o1.getFiosTV() && null == o2.getFiosTV()){
					j = 0;
				}else if(null != o1.getFiosTV() && null == o2.getFiosTV()){
					j = -1;
				}else if(null == o1.getFiosTV() && null != o2.getFiosTV()){
					j = 1;
				}else {
					j = o1.getFiosTV().compareTo(o2.getFiosTV());
				}
			}else if(this.columnId.equals("WINBACK")){
				if(null == o1.getWinback() && null == o2.getWinback()){
					j = 0;
				}else if(null != o1.getWinback() && null == o2.getWinback()){
					j = -1;
				}else if(null == o1.getWinback() && null != o2.getWinback()){
					j = 1;
				}else {
					j = o1.getWinback().compareTo(o2.getWinback());
				}
			}else if(this.columnId.equals("PROSPECT")){
				if(null == o1.getProspect() && null == o2.getProspect()){
					j = 0;
				}else if(null != o1.getProspect() && null == o2.getProspect()){
					j = -1;
				}else if(null == o1.getProspect() && null != o2.getProspect()){
					j = 1;
				}else {
					j = o1.getProspect().compareTo(o2.getProspect());
				}
			}else if(this.columnId.equals("VOICEONLY")){
				if(null == o1.getVoiceOnly() && null == o2.getVoiceOnly()){
					j = 0;
				}else if(null != o1.getVoiceOnly() && null == o2.getVoiceOnly()){
					j = -1;
				}else if(null == o1.getVoiceOnly() && null != o2.getVoiceOnly()){
					j = 1;
				}else {
					j = o1.getVoiceOnly().compareTo(o2.getVoiceOnly());
				}
			}else if(this.columnId.equals("DIFFINCOUNT")){
				if(null == o1.getDiffInCount() && null == o2.getDiffInCount()){
					j = 0;
				}else if(null != o1.getDiffInCount() && null == o2.getDiffInCount()){
					j = -1;
				}else if(null == o1.getDiffInCount() && null != o2.getDiffInCount()){
					j = 1;
				}else {
					j = o1.getDiffInCount().compareTo(o2.getDiffInCount());
				}
			}else if(this.columnId.equals("HSI")){
				if(null == o1.getHsi() && null == o2.getHsi()){
					j = 0;
				}else if(null != o1.getHsi() && null == o2.getHsi()){
					j = -1;
				}else if(null == o1.getHsi() && null != o2.getHsi()){
					j = 1;
				}else {
					j = o1.getHsi().compareTo(o2.getHsi());
				}
			}else if(this.columnId.equals("TDMSDB3")){
				if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
					j = 0;
				}else if(null != o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
					j = -1;
				}else if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null != o2.getTotalMaxSpeedDnLoadBelow3()){
					j = 1;
				}else {
					j = o1.getTotalMaxSpeedDnLoadBelow3().compareTo(o2.getTotalMaxSpeedDnLoadBelow3());
				}
			}else if(this.columnId.equals("TDMSDA3")){
				if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
					j = 0;
				}else if(null != o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
					j = -1;
				}else if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null != o2.getTotalMaxSpeedDnLoadAbove3()){
					j = 1;
				}else {
					j = o1.getTotalMaxSpeedDnLoadAbove3().compareTo(o2.getTotalMaxSpeedDnLoadAbove3());
				}
			}
		
			if("ASC".equals(this.sortOrder)){
	    		if ( i < 0 ) {  
	    			retStatus = -1;  
	    		} else if ( i > 0 ) {  
	    			retStatus = 1;  
	    		} else {  
	    			if ( j < 0 ) {  
	    				retStatus = -1;  
	    			} else if ( j > 0 ) {  
	    				retStatus = 1;  
	    			} else {  
	    				retStatus = 0;  
	    			}  
	    		}
	    	}else{
	    		if ( i < 0 ) {  
	                retStatus = -1;  
	    		} else if ( i > 0 ) {  
	                retStatus = 1;  
	    		} else {  
	    			if ( j > 0 ) {  
	    				retStatus = -1;  
	    			} else if ( j < 0 ) {  
	    				retStatus = 1;  
	    			} else {  
	    				retStatus = 0;  
	    			}  
	    		}
	    	}
		}catch(Exception e){
			System.out.println("Wxception :"+e);
		}  
		return retStatus;
	}
}


