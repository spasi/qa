/**
 * 
 */
package com.cydcor.framework.utils;

import java.awt.Color;
import java.util.Random;

/**
 * @author ashwin
 * 
 */
public class RandomColor {

	public static String[] colorList = { "5f0000FF", "5f00CD66", "5f33FF33",
			"5f551A8B", "5f5E2605", "5f6495ED", "5f6B6B6B", "5f7FFF00",
			"5f872657", "5fA0522D", "5fA62A2A", "5fBF3EFF", "5fCD1076",
			"5fEEEE00", "5fC73F17" };

	private Random rand;

	/**
	 * Constructor for objects of class RandomColor initializes the random
	 * number generator
	 */
	public RandomColor() {
		rand = new Random();
	}

	/**
	 * randomColor returns a pseudorandom Color
	 * 
	 * @return a pseudorandom Color
	 */
	public Color randomColor() {
		return (new Color(rand.nextInt(256), rand.nextInt(256), rand
				.nextInt(256)));
	}

	/**
	 * randomGray returns a pseudorandom gray Color
	 * 
	 * @return a pseudorandom Color
	 */
	public Color randomGray() {
		int intensity = rand.nextInt(256);
		return (new Color(intensity, intensity, intensity));
	}

	/**
	 * @return
	 */
	public String randomHex() {
		Color color = new Color(rand.nextInt(256), rand.nextInt(256), rand
				.nextInt(256));
		String hex = Integer.toHexString(color.getRGB());
		hex = "5f" + hex.substring(2, hex.length());

		return colorList[rand.nextInt(colorList.length)];
		// return hex;
	}
}
