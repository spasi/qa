package com.cydcor.framework.utils;

import java.text.SimpleDateFormat;
import java.util.Comparator;

import com.cydcor.framework.model.VerizonFiosReport;

public class VerizonFiosReportTableSorter implements Comparator<VerizonFiosReport> {

	private String columnId ;
	private String sortOrder;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); //Changed mm to MM to fix issue with date column sorting incorrectly
	
	public VerizonFiosReportTableSorter(String columnId,String sortOrder){
		this.columnId = columnId;
		this.sortOrder = sortOrder;
	}

	public int compare(VerizonFiosReport o1, VerizonFiosReport o2) {
		int retStatus=0;
		try{
			int i = o1.getSortSeq().compareTo(o2.getSortSeq());
			int j = 0;
			if(this.columnId.equals("territory")){
				j = o1.getTerritoryName().compareTo(o2.getTerritoryName());
				System.out.println(o1.getTerritoryName()+","+o2.getTerritoryName());
			}if(this.columnId.equals("date_last_worked")){
				if(null == o1.getDatelastWorked() && null == o2.getDatelastWorked()){
					j = 0;
				}else if(null != o1.getDatelastWorked() && null == o2.getDatelastWorked()){
					j = -1;
				}else if(null == o1.getDatelastWorked() && null != o2.getDatelastWorked()){
					j = 1;
				}else {
					j = o1.getDatelastWorked().compareTo(o2.getDatelastWorked());
				}
			}if(this.columnId.equals("date_to_be_worked")){
				if(null == o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
					j = 0;
				}else if(null != o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
					j = -1;
				}else if(null == o1.getDateToBeWorked() && null != o2.getDateToBeWorked()){
					j = 1;
				}else {
					j = o1.getDateToBeWorked().compareTo(o2.getDateToBeWorked());
				}
			}else if(this.columnId.equals("total")){
				if(null == o1.getTotal() && null == o2.getTotal()){
					j = 0;
				}else if(null != o1.getTotal() && null == o2.getTotal()){
					j = -1;
				}else if(null == o1.getTotal() && null != o2.getTotal()){
					j = 1;
				}else {
					j = o1.getTotal().compareTo(o2.getTotal());
				}
			}else if(this.columnId.equals("total_avaialbe")){
				if(null == o1.getTotalAvailable() && null == o2.getTotalAvailable()){
					j = 0;
				}else if(null != o1.getTotalAvailable() && null == o2.getTotalAvailable()){
					j = -1;
				}else if(null == o1.getTotalAvailable() && null != o2.getTotalAvailable()){
					j = 1;
				}else {
					j = o1.getTotalAvailable().compareTo(o2.getTotalAvailable());
				}
			}else if(this.columnId.equals("leads_30_and_under_days")){
				if(null == o1.getLeadsThirtyDaysAndUnder() && null == o2.getLeadsThirtyDaysAndUnder()){
					j = 0;
				}else if(null != o1.getLeadsThirtyDaysAndUnder() && null == o2.getLeadsThirtyDaysAndUnder()){
					j = -1;
				}else if(null == o1.getLeadsThirtyDaysAndUnder() && null != o2.getLeadsThirtyDaysAndUnder()){
					j = 1;
				}else {
					j = o1.getLeadsThirtyDaysAndUnder().compareTo(o2.getLeadsThirtyDaysAndUnder());
				}
			}else if(this.columnId.equals("leads_between_31_90_days")){
				if(null == o1.getLeadsBetweenThirtyOneAndNinetyDays() && null == o2.getLeadsBetweenThirtyOneAndNinetyDays()){
					j = 0;
				}else if(null != o1.getLeadsBetweenThirtyOneAndNinetyDays() && null == o2.getLeadsBetweenThirtyOneAndNinetyDays()){
					j = -1;
				}else if(null == o1.getLeadsBetweenThirtyOneAndNinetyDays() && null != o2.getLeadsBetweenThirtyOneAndNinetyDays()){
					j = 1;
				}else {
					j = o1.getLeadsBetweenThirtyOneAndNinetyDays().compareTo(o2.getLeadsBetweenThirtyOneAndNinetyDays());
				}
			}else if(this.columnId.equals("newly_lit_30_days_or_less")){
				if(null == o1.getPercentageNewlyLitThirtyDaysOrLess() && null == o2.getPercentageNewlyLitThirtyDaysOrLess()){
					j = 0;
				}else if(null != o1.getPercentageNewlyLitThirtyDaysOrLess() && null == o2.getPercentageNewlyLitThirtyDaysOrLess()){
					j = -1;
				}else if(null == o1.getPercentageNewlyLitThirtyDaysOrLess() && null != o2.getPercentageNewlyLitThirtyDaysOrLess()){
					j = 1;
				}else {
					j = o1.getPercentageNewlyLitThirtyDaysOrLess().compareTo(o2.getPercentageNewlyLitThirtyDaysOrLess());
				}
			}else if(this.columnId.equals("newly_lit_between_31_90_days")){
				if(null == o1.getPercentageNewlyLitBetweenThirtyOneAndNinetyDays() && null == o2.getPercentageNewlyLitBetweenThirtyOneAndNinetyDays()){
					j = 0;
				}else if(null != o1.getPercentageNewlyLitBetweenThirtyOneAndNinetyDays() && null == o2.getPercentageNewlyLitBetweenThirtyOneAndNinetyDays()){
					j = -1;
				}else if(null == o1.getPercentageNewlyLitBetweenThirtyOneAndNinetyDays() && null != o2.getPercentageNewlyLitBetweenThirtyOneAndNinetyDays()){
					j = 1;
				}else {
					j = o1.getPercentageNewlyLitBetweenThirtyOneAndNinetyDays().compareTo(o2.getPercentageNewlyLitBetweenThirtyOneAndNinetyDays());
				}
			}else if(this.columnId.equals("mdu_mdu_y")){
				if(null == o1.getMduMduY() && null == o2.getMduMduY()){
					j = 0;
				}else if(null != o1.getMduMduY() && null == o2.getMduMduY()){
					j = -1;
				}else if(null == o1.getMduMduY() && null != o2.getMduMduY()){
					j = 1;
				}else {
					j = o1.getMduMduY().compareTo(o2.getMduMduY());
				}
			}else if(this.columnId.equals("open_season_mdu_y")){
				if(null == o1.getOpenSeasonMduY() && null == o2.getOpenSeasonMduY()){
					j = 0;
				}else if(null != o1.getOpenSeasonMduY() && null == o2.getOpenSeasonMduY()){
					j = -1;
				}else if(null == o1.getOpenSeasonMduY() && null != o2.getOpenSeasonMduY()){
					j = 1;
				}else {
					j = o1.getOpenSeasonMduY().compareTo(o2.getOpenSeasonMduY());
				}
			}else if(this.columnId.equals("disabled_mdu")){
				if(null == o1.getDisabledMdu() && null == o2.getDisabledMdu()){
					j = 0;
				}else if(null != o1.getDisabledMdu() && null == o2.getDisabledMdu()){
					j = -1;
				}else if(null == o1.getDisabledMdu() && null != o2.getDisabledMdu()){
					j = 1;
				}else {
					j = o1.getDisabledMdu().compareTo(o2.getDisabledMdu());
				}
			}else if(this.columnId.equals("dsl_migration")){
				if(null == o1.getDslMigration() && null == o2.getDslMigration()){
					j = 0;
				}else if(null != o1.getDslMigration() && null == o2.getDslMigration()){
					j = -1;
				}else if(null == o1.getDslMigration() && null != o2.getDslMigration()){
					j = 1;
				}else {
					j = o1.getDslMigration().compareTo(o2.getDslMigration());
				}
			}else if(this.columnId.equals("total_iont")){
				if(null == o1.getTotalIont() && null == o2.getTotalIont()){
					j = 0;
				}else if(null != o1.getTotalIont() && null == o2.getTotalIont()){
					j = -1;
				}else if(null == o1.getTotalIont() && null != o2.getTotalIont()){
					j = 1;
				}else {
					j = o1.getTotalIont().compareTo(o2.getTotalIont());
				}
			}else if(this.columnId.equals("iont_different_customers")){
				if(null == o1.getIontDifferentCustomers() && null == o2.getIontDifferentCustomers()){
					j = 0;
				}else if(null != o1.getIontDifferentCustomers() && null == o2.getIontDifferentCustomers()){
					j = -1;
				}else if(null == o1.getIontDifferentCustomers() && null != o2.getIontDifferentCustomers()){
					j = 1;
				}else {
					j = o1.getIontDifferentCustomers().compareTo(o2.getIontDifferentCustomers());
				}
			}else if(this.columnId.equals("iont_same_customer")){
				if(null == o1.getIontSameCustomer() && null == o2.getIontSameCustomer()){
					j = 0;
				}else if(null != o1.getIontSameCustomer() && null == o2.getIontSameCustomer()){
					j = -1;
				}else if(null == o1.getIontSameCustomer() && null != o2.getIontSameCustomer()){
					j = 1;
				}else {
					j = o1.getIontSameCustomer().compareTo(o2.getIontSameCustomer());
				}
			}else if(this.columnId.equals("iont_disconnect_date_under_2_years")){
				if(null == o1.getDisconnectDateUnderTwoYears() && null == o2.getDisconnectDateUnderTwoYears()){
					j = 0;
				}else if(null != o1.getDisconnectDateUnderTwoYears() && null == o2.getDisconnectDateUnderTwoYears()){
					j = -1;
				}else if(null == o1.getDisconnectDateUnderTwoYears() && null != o2.getDisconnectDateUnderTwoYears()){
					j = 1;
				}else {
					j = o1.getDisconnectDateUnderTwoYears().compareTo(o2.getDisconnectDateUnderTwoYears());
				}
			}else if(this.columnId.equals("data_eligible")){
				if(null == o1.getDataEligible() && null == o2.getDataEligible()){
					j = 0;
				}else if(null != o1.getDataEligible() && null == o2.getDataEligible()){
					j = -1;
				}else if(null == o1.getDataEligible() && null != o2.getDataEligible()){
					j = 1;
				}else {
					j = o1.getDataEligible().compareTo(o2.getDataEligible());
				}
			}else if(this.columnId.equals("voice_and_data")){
				if(null == o1.getVoiceAndData() && null == o2.getVoiceAndData()){
					j = 0;
				}else if(null != o1.getVoiceAndData() && null == o2.getVoiceAndData()){
					j = -1;
				}else if(null == o1.getVoiceAndData() && null != o2.getVoiceAndData()){
					j = 1;
				}else {
					j = o1.getVoiceAndData().compareTo(o2.getVoiceAndData());
				}
			}else if(this.columnId.equals("voice_data_tv")){
				if(null == o1.getVoiceDataTv() && null == o2.getVoiceDataTv()){
					j = 0;
				}else if(null != o1.getVoiceDataTv() && null == o2.getVoiceDataTv()){
					j = -1;
				}else if(null == o1.getVoiceDataTv() && null != o2.getVoiceDataTv()){
					j = 1;
				}else {
					j = o1.getVoiceDataTv().compareTo(o2.getVoiceDataTv());
				}
			}else if(this.columnId.equals("tv_penetration_rate")){
				if(null == o1.getTvPenetrationRate() && null == o2.getTvPenetrationRate()){
					j = 0;
				}else if(null != o1.getTvPenetrationRate() && null == o2.getTvPenetrationRate()){
					j = -1;
				}else if(null == o1.getTvPenetrationRate() && null != o2.getTvPenetrationRate()){
					j = 1;
				}else {
					j = o1.getTvPenetrationRate().compareTo(o2.getTvPenetrationRate());
				}
			}else if(this.columnId.equals("promo")){
				if(null == o1.getPromo() && null == o2.getPromo()){
					j = 0;
				}else if(null != o1.getPromo() && null == o2.getPromo()){
					j = -1;
				}else if(null == o1.getPromo() && null != o2.getPromo()){
					j = 1;
				}else {
//					j = o1.getPromo().compareTo(o2.getPromo());
					j = 0; //TODO FIXME
				}
			}
			//if(this.columnId.equals("DTBW")){
//				if(null == o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
//					j = 0;
//				}else if(null != o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
//					j = -1;
//				}else if(null == o1.getDateToBeWorked() && null != o2.getDateToBeWorked()){
//					j = 1;
//				}else {
//					j = sdf.parse(o1.getDateToBeWorked()).compareTo(sdf.parse(o2.getDateToBeWorked()));
//				}
//			}else if(this.columnId.equals("TOWN")){
//				if(null == o1.getTown() && null == o2.getTown()){
//					j = 0;
//				}else if(null != o1.getTown() && null == o2.getTown()){
//					j = -1;
//				}else if(null == o1.getTown() && null != o2.getTown()){
//					j = 1;
//				}else {
//					j = o1.getTown().compareTo(o2.getTown());
//				}
//			}else if(this.columnId.equals("DISTANCE")){
//				if(null == o1.getDistance() && null == o2.getDistance()){
//					j = 0;
//				}else if(null != o1.getDistance() && null == o2.getDistance()){
//					if("ASC".equals(this.sortOrder)){j = -1;}else{j = 1;}
//				}else if(null == o1.getDistance() && null != o2.getDistance()){
//					if("ASC".equals(this.sortOrder)){j = 1;}else{j = -1;}
//				}else {
//					j = new Double(o1.getDistance().replaceAll(",", "")).compareTo(new Double(o2.getDistance().replaceAll(",", "")));
//				}
//			}else if(this.columnId.equals("TOTAL")){
//				if(null == o1.getTotal() && null == o2.getTotal()){
//					j = 0;
//				}else if(null != o1.getTotal() && null == o2.getTotal()){
//					j = -1;
//				}else if(null == o1.getTotal() && null != o2.getTotal()){
//					j = 1;
//				}else {
//					j = o1.getTotal().compareTo(o2.getTotal());
//				}
//			}else if(this.columnId.equals("SHARED_BY")){
//				if(null == o1.getSharedBy() && null == o2.getSharedBy()){
//					j = 0;
//				}else if(null != o1.getSharedBy() && null == o2.getSharedBy()){
//					j = -1;
//				}else if(null == o1.getSharedBy() && null != o2.getSharedBy()){
//					j = 1;
//				}else {
//					j = o1.getSharedBy().compareTo(o2.getSharedBy());
//				}
//			}else if(this.columnId.equals("NUMBER_OF_SHARED_LEADS")){
//				if(null == o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
//					j = 0;
//				}else if(null != o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
//					j = -1;
//				}else if(null == o1.getNumOfSharedLeads() && null != o2.getNumOfSharedLeads()){
//					j = 1;
//				}else {
//					j = o1.getNumOfSharedLeads().compareTo(o2.getNumOfSharedLeads());
//				}
//			}else if(this.columnId.equals("FIOS_DATA")){
//				if(null == o1.getFiosData() && null == o2.getFiosData()){
//					j = 0;
//				}else if(null != o1.getFiosData() && null == o2.getFiosData()){
//					j = -1;
//				}else if(null == o1.getFiosData() && null != o2.getFiosData()){
//					j = 1;
//				}else {
//					j = o1.getFiosData().compareTo(o2.getFiosData());
//				}
//			}else if(this.columnId.equals("FIOS_MIGRATION")){
//				if(null == o1.getFiosMigration() && null == o2.getFiosMigration()){
//					j = 0;
//				}else if(null != o1.getFiosMigration() && null == o2.getFiosMigration()){
//					j = -1;
//				}else if(null == o1.getFiosMigration() && null != o2.getFiosMigration()){
//					j = 1;
//				}else {
//					j = o1.getFiosMigration().compareTo(o2.getFiosMigration());
//				}
//			}else if(this.columnId.equals("FIOS_TV")){
//				if(null == o1.getFiosTV() && null == o2.getFiosTV()){
//					j = 0;
//				}else if(null != o1.getFiosTV() && null == o2.getFiosTV()){
//					j = -1;
//				}else if(null == o1.getFiosTV() && null != o2.getFiosTV()){
//					j = 1;
//				}else {
//					j = o1.getFiosTV().compareTo(o2.getFiosTV());
//				}
//			}else if(this.columnId.equals("WINBACK")){
//				if(null == o1.getWinback() && null == o2.getWinback()){
//					j = 0;
//				}else if(null != o1.getWinback() && null == o2.getWinback()){
//					j = -1;
//				}else if(null == o1.getWinback() && null != o2.getWinback()){
//					j = 1;
//				}else {
//					j = o1.getWinback().compareTo(o2.getWinback());
//				}
//			}else if(this.columnId.equals("PROSPECT")){
//				if(null == o1.getProspect() && null == o2.getProspect()){
//					j = 0;
//				}else if(null != o1.getProspect() && null == o2.getProspect()){
//					j = -1;
//				}else if(null == o1.getProspect() && null != o2.getProspect()){
//					j = 1;
//				}else {
//					j = o1.getProspect().compareTo(o2.getProspect());
//				}
//			}else if(this.columnId.equals("HSI")){
//				if(null == o1.getHsi() && null == o2.getHsi()){
//					j = 0;
//				}else if(null != o1.getHsi() && null == o2.getHsi()){
//					j = -1;
//				}else if(null == o1.getHsi() && null != o2.getHsi()){
//					j = 1;
//				}else {
//					j = o1.getHsi().compareTo(o2.getHsi());
//				}
//			}else if(this.columnId.equals("TDMSDB3")){
//				if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
//					j = 0;
//				}else if(null != o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
//					j = -1;
//				}else if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null != o2.getTotalMaxSpeedDnLoadBelow3()){
//					j = 1;
//				}else {
//					j = o1.getTotalMaxSpeedDnLoadBelow3().compareTo(o2.getTotalMaxSpeedDnLoadBelow3());
//				}
//			}else if(this.columnId.equals("TDMSDA3")){
//				if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
//					j = 0;
//				}else if(null != o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
//					j = -1;
//				}else if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null != o2.getTotalMaxSpeedDnLoadAbove3()){
//					j = 1;
//				}else {
//					j = o1.getTotalMaxSpeedDnLoadAbove3().compareTo(o2.getTotalMaxSpeedDnLoadAbove3());
//				}
//			}
		    System.out.println("i="+i+" j="+j);
			if("ASC".equals(this.sortOrder)){
	    		if ( i < 0 ) {  
	    			retStatus = -1;  
	    		} else if ( i > 0 ) {  
	    			retStatus = 1;  
	    		} else {  
	    			if ( j < 0 ) {  
	    				retStatus = -1;  
	    			} else if ( j > 0 ) {  
	    				retStatus = 1;  
	    			} else {  
	    				retStatus = 0;  
	    			}  
	    		}
	    	}else{
	    		if ( i < 0 ) {  
	                retStatus = -1;  
	    		} else if ( i > 0 ) {  
	                retStatus = 1;  
	    		} else {  
	    			if ( j > 0 ) {  
	    				retStatus = -1;  
	    			} else if ( j < 0 ) {  
	    				retStatus = 1;  
	    			} else {  
	    				retStatus = 0;  
	    			}  
	    		}
	    	}
		}catch(Exception e){
			System.out.println("Wxception :"+e);
			e.printStackTrace();
		}  
		System.out.println("retStatus="+retStatus);
		return retStatus;
	}
}