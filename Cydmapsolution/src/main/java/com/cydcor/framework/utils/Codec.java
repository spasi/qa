/**
 * 
 */
package com.cydcor.framework.utils;

/**
 * @author ashwin
 *
 */
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Codec {
	
	private static final transient Log logger = LogFactory.getLog(Codec.class);
  public static void main(String[] args) {
    try {
      String clearText = "Hello world";
      String encodedText="AAzGUgABlCePefhgdnbG95U9nY0Mu9Q8z2FYHeg/nO9gF5hUijG0xNJ3Np5zUoKfBenqaWkXNDGokDM28ynCOwt1kng/VqO1";

      // Base64
      //encodedText = new String(Base64.encodeBase64(clearText.getBytes()));
      logger.debug("Encoded: " + encodedText);
      logger.debug("Decoded:" 
          + new String(Base64.decodeBase64(encodedText.getBytes())));
      //    
      // output :
      //   Encoded: SGVsbG8gd29ybGQ=
      //   Decoded:Hello world      
      //
    } 
    catch (Exception e) {
      logger.debug(e);
    }
  }
}
