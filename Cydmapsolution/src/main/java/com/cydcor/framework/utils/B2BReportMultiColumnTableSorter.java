package com.cydcor.framework.utils;

import java.text.SimpleDateFormat;
import java.util.Comparator;

import com.cydcor.framework.model.B2BReport;

public class B2BReportMultiColumnTableSorter implements Comparator<B2BReport> {
	
	private String columnId1 ;
	private String sortOrder1;
	private String columnId2 ;
	private String sortOrder2;
	private String columnId3 ;
	private String sortOrder3;
	private String columnId4 ;
	private String sortOrder4;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");//Changed mm to MM to fix issue with date column sorting incorrectly
	
	public B2BReportMultiColumnTableSorter(String columnId1,String sortOrder1,
		String columnId2,String sortOrder2,String columnId3,String sortOrder3,
		String columnId4,String sortOrder4){
		this.columnId1 = columnId1;
		this.columnId2 = columnId2;
		this.columnId3 = columnId3;
		this.columnId4 = columnId4;
		this.sortOrder1 = sortOrder1;
		this.sortOrder2 = sortOrder2;
		this.sortOrder3 = sortOrder3;
		this.sortOrder4 = sortOrder4;
	}
	public int compare(B2BReport o1, B2BReport o2) {
		int retStatus=0;
		try{
			// TODO Auto-generated method stub
			int i = o1.getSortSeq().compareTo(o2.getSortSeq());
			int j = 0;
			int k = 0;
			int l = 0;
			int m = 0;
			
			if("-1" != this.columnId1){
				if(this.columnId1.equals("TERRITORY")){
					j = o1.getTerritoryName().compareTo(o2.getTerritoryName());
				}if(this.columnId1.equals("CLOSED_DT")){
					if(null == o1.getClosedDate() && null == o2.getClosedDate()){
						j = 0;
					}else if(null != o1.getClosedDate() && null == o2.getClosedDate()){
						j = -1;
					}else if(null == o1.getClosedDate() && null != o2.getClosedDate()){
						j = 1;
					}else {
						j = sdf.parse(o1.getClosedDate()).compareTo(sdf.parse(o2.getClosedDate()));
					}
				}if(this.columnId1.equals("DTBW")){
					if(null == o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
						j = 0;
					}else if(null != o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
						j = -1;
					}else if(null == o1.getDateToBeWorked() && null != o2.getDateToBeWorked()){
						j = 1;
					}else {
						j = sdf.parse(o1.getDateToBeWorked()).compareTo(sdf.parse(o2.getDateToBeWorked()));
					}
				}else if(this.columnId1.equals("TOWN")){
					if(null == o1.getTown() && null == o2.getTown()){
						j = 0;
					}else if(null != o1.getTown() && null == o2.getTown()){
						j = -1;
					}else if(null == o1.getTown() && null != o2.getTown()){
						j = 1;
					}else {
						j = o1.getTown().compareTo(o2.getTown());
					}
				}else if(this.columnId1.equals("DISTANCE")){
					if(null == o1.getDistance() && null == o2.getDistance()){
						j = 0;
					}else if(null != o1.getDistance() && null == o2.getDistance()){
						if("ASC".equals(this.sortOrder1)){j = -1;}else{j = 1;}
					}else if(null == o1.getDistance() && null != o2.getDistance()){
						if("ASC".equals(this.sortOrder1)){j = 1;}else{j = -1;}
					}else {
						j = new Double(o1.getDistance().replaceAll(",", "")).compareTo(new Double(o2.getDistance().replaceAll(",", "")));
					}
				}else if(this.columnId1.equals("TOTAL")){
					if(null == o1.getTotal() && null == o2.getTotal()){
						j = 0;
					}else if(null != o1.getTotal() && null == o2.getTotal()){
						j = -1;
					}else if(null == o1.getTotal() && null != o2.getTotal()){
						j = 1;
					}else {
						j = o1.getTotal().compareTo(o2.getTotal());
					}
				}else if(this.columnId1.equals("SHARED_BY")){
					if(null == o1.getSharedBy() && null == o2.getSharedBy()){
						j = 0;
					}else if(null != o1.getSharedBy() && null == o2.getSharedBy()){
						j = -1;
					}else if(null == o1.getSharedBy() && null != o2.getSharedBy()){
						j = 1;
					}else {
						j = o1.getSharedBy().compareTo(o2.getSharedBy());
					}
				}else if(this.columnId1.equals("NUMBER_OF_SHARED_LEADS")){
					if(null == o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
						j = 0;
					}else if(null != o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
						j = -1;
					}else if(null == o1.getNumOfSharedLeads() && null != o2.getNumOfSharedLeads()){
						j = 1;
					}else {
						j = o1.getNumOfSharedLeads().compareTo(o2.getNumOfSharedLeads());
					}
				}else if(this.columnId1.equals("FIOS_DATA")){
					if(null == o1.getFiosData() && null == o2.getFiosData()){
						j = 0;
					}else if(null != o1.getFiosData() && null == o2.getFiosData()){
						j = -1;
					}else if(null == o1.getFiosData() && null != o2.getFiosData()){
						j = 1;
					}else {
						j = o1.getFiosData().compareTo(o2.getFiosData());
					}
				}else if(this.columnId1.equals("FIOS_MIGRATION")){
					if(null == o1.getFiosMigration() && null == o2.getFiosMigration()){
						j = 0;
					}else if(null != o1.getFiosMigration() && null == o2.getFiosMigration()){
						j = -1;
					}else if(null == o1.getFiosMigration() && null != o2.getFiosMigration()){
						j = 1;
					}else {
						j = o1.getFiosMigration().compareTo(o2.getFiosMigration());
					}
				}else if(this.columnId1.equals("FIOS_TV")){
					if(null == o1.getFiosTV() && null == o2.getFiosTV()){
						j = 0;
					}else if(null != o1.getFiosTV() && null == o2.getFiosTV()){
						j = -1;
					}else if(null == o1.getFiosTV() && null != o2.getFiosTV()){
						j = 1;
					}else {
						j = o1.getFiosTV().compareTo(o2.getFiosTV());
					}
				}else if(this.columnId1.equals("WINBACK")){
					if(null == o1.getWinback() && null == o2.getWinback()){
						j = 0;
					}else if(null != o1.getWinback() && null == o2.getWinback()){
						j = -1;
					}else if(null == o1.getWinback() && null != o2.getWinback()){
						j = 1;
					}else {
						j = o1.getWinback().compareTo(o2.getWinback());
					}
				}else if(this.columnId1.equals("PROSPECT")){
					if(null == o1.getProspect() && null == o2.getProspect()){
						j = 0;
					}else if(null != o1.getProspect() && null == o2.getProspect()){
						j = -1;
					}else if(null == o1.getProspect() && null != o2.getProspect()){
						j = 1;
					}else {
						j = o1.getProspect().compareTo(o2.getProspect());
					}
				}else if(this.columnId1.equals("VOICEONLY")){
					if(null == o1.getVoiceOnly() && null == o2.getVoiceOnly()){
						j = 0;
					}else if(null != o1.getVoiceOnly() && null == o2.getVoiceOnly()){
						j = -1;
					}else if(null == o1.getVoiceOnly() && null != o2.getVoiceOnly()){
						j = 1;
					}else {
						j = o1.getVoiceOnly().compareTo(o2.getVoiceOnly());
					}
				}else if(this.columnId1.equals("DIFFINCOUNT")){
					if(null == o1.getDiffInCount() && null == o2.getDiffInCount()){
						j = 0;
					}else if(null != o1.getDiffInCount() && null == o2.getDiffInCount()){
						j = -1;
					}else if(null == o1.getDiffInCount() && null != o2.getDiffInCount()){
						j = 1;
					}else {
						j = o1.getDiffInCount().compareTo(o2.getDiffInCount());
					}
				}else if(this.columnId1.equals("HSI")){
					if(null == o1.getHsi() && null == o2.getHsi()){
						j = 0;
					}else if(null != o1.getHsi() && null == o2.getHsi()){
						j = -1;
					}else if(null == o1.getHsi() && null != o2.getHsi()){
						j = 1;
					}else {
						j = o1.getHsi().compareTo(o2.getHsi());
					}
				}else if(this.columnId1.equals("TDMSDB3")){
					if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
						j = 0;
					}else if(null != o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
						j = -1;
					}else if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null != o2.getTotalMaxSpeedDnLoadBelow3()){
						j = 1;
					}else {
						j = o1.getTotalMaxSpeedDnLoadBelow3().compareTo(o2.getTotalMaxSpeedDnLoadBelow3());
					}
				}else if(this.columnId1.equals("TDMSDA3")){
					if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
						j = 0;
					}else if(null != o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
						j = -1;
					}else if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null != o2.getTotalMaxSpeedDnLoadAbove3()){
						j = 1;
					}else {
						j = o1.getTotalMaxSpeedDnLoadAbove3().compareTo(o2.getTotalMaxSpeedDnLoadAbove3());
					}
				}
				
			}
			
			if("-1" != this.columnId2){
				if(this.columnId2.equals("TERRITORY")){
					k = o1.getTerritoryName().compareTo(o2.getTerritoryName());
				}if(this.columnId2.equals("CLOSED_DT")){
					if(null == o1.getClosedDate() && null == o2.getClosedDate()){
						k = 0;
					}else if(null != o1.getClosedDate() && null == o2.getClosedDate()){
						k = -1;
					}else if(null == o1.getClosedDate() && null != o2.getClosedDate()){
						k = 1;
					}else {
						k = sdf.parse(o1.getClosedDate()).compareTo(sdf.parse(o2.getClosedDate()));
					}
				}if(this.columnId2.equals("DTBW")){
					if(null == o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
						k = 0;
					}else if(null != o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
						k = -1;
					}else if(null == o1.getDateToBeWorked() && null != o2.getDateToBeWorked()){
						k = 1;
					}else {
						k = sdf.parse(o1.getDateToBeWorked()).compareTo(sdf.parse(o2.getDateToBeWorked()));
					}
				}else if(this.columnId2.equals("TOWN")){
					if(null == o1.getTown() && null == o2.getTown()){
						k = 0;
					}else if(null != o1.getTown() && null == o2.getTown()){
						k = -1;
					}else if(null == o1.getTown() && null != o2.getTown()){
						k = 1;
					}else {
						k = o1.getTown().compareTo(o2.getTown());
					}
				}else if(this.columnId2.equals("DISTANCE")){
					if(null == o1.getDistance() && null == o2.getDistance()){
						k = 0;
					}else if(null != o1.getDistance() && null == o2.getDistance()){
						if("ASC".equals(this.sortOrder2)){k = -1;}else{k = 1;}
					}else if(null == o1.getDistance() && null != o2.getDistance()){
						if("ASC".equals(this.sortOrder2)){k = 1;}else{k = -1;}
					}else {
						k = new Double(o1.getDistance().replaceAll(",", "")).compareTo(new Double(o2.getDistance().replaceAll(",", "")));
					}
				}else if(this.columnId2.equals("TOTAL")){
					if(null == o1.getTotal() && null == o2.getTotal()){
						k = 0;
					}else if(null != o1.getTotal() && null == o2.getTotal()){
						k = -1;
					}else if(null == o1.getTotal() && null != o2.getTotal()){
						k = 1;
					}else {
						k = o1.getTotal().compareTo(o2.getTotal());
					}
				}else if(this.columnId2.equals("SHARED_BY")){
					if(null == o1.getSharedBy() && null == o2.getSharedBy()){
						k = 0;
					}else if(null != o1.getSharedBy() && null == o2.getSharedBy()){
						k = -1;
					}else if(null == o1.getSharedBy() && null != o2.getSharedBy()){
						k = 1;
					}else {
						k = o1.getSharedBy().compareTo(o2.getSharedBy());
					}
				}else if(this.columnId2.equals("NUMBER_OF_SHARED_LEADS")){
					if(null == o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
						k = 0;
					}else if(null != o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
						k = -1;
					}else if(null == o1.getNumOfSharedLeads() && null != o2.getNumOfSharedLeads()){
						k = 1;
					}else {
						k = o1.getNumOfSharedLeads().compareTo(o2.getNumOfSharedLeads());
					}
				}else if(this.columnId2.equals("FIOS_DATA")){
					if(null == o1.getFiosData() && null == o2.getFiosData()){
						k = 0;
					}else if(null != o1.getFiosData() && null == o2.getFiosData()){
						k = -1;
					}else if(null == o1.getFiosData() && null != o2.getFiosData()){
						k = 1;
					}else {
						k = o1.getFiosData().compareTo(o2.getFiosData());
					}
				}else if(this.columnId2.equals("FIOS_MIGRATION")){
					if(null == o1.getFiosMigration() && null == o2.getFiosMigration()){
						k = 0;
					}else if(null != o1.getFiosMigration() && null == o2.getFiosMigration()){
						k = -1;
					}else if(null == o1.getFiosMigration() && null != o2.getFiosMigration()){
						k = 1;
					}else {
						k = o1.getFiosMigration().compareTo(o2.getFiosMigration());
					}
				}else if(this.columnId2.equals("FIOS_TV")){
					if(null == o1.getFiosTV() && null == o2.getFiosTV()){
						k = 0;
					}else if(null != o1.getFiosTV() && null == o2.getFiosTV()){
						k = -1;
					}else if(null == o1.getFiosTV() && null != o2.getFiosTV()){
						k = 1;
					}else {
						k = o1.getFiosTV().compareTo(o2.getFiosTV());
					}
				}else if(this.columnId2.equals("WINBACK")){
					if(null == o1.getWinback() && null == o2.getWinback()){
						k = 0;
					}else if(null != o1.getWinback() && null == o2.getWinback()){
						k = -1;
					}else if(null == o1.getWinback() && null != o2.getWinback()){
						k = 1;
					}else {
						k = o1.getWinback().compareTo(o2.getWinback());
					}
				}else if(this.columnId2.equals("PROSPECT")){
					if(null == o1.getProspect() && null == o2.getProspect()){
						k = 0;
					}else if(null != o1.getProspect() && null == o2.getProspect()){
						k = -1;
					}else if(null == o1.getProspect() && null != o2.getProspect()){
						k = 1;
					}else {
						k = o1.getProspect().compareTo(o2.getProspect());
					}
				}else if(this.columnId2.equals("VOICEONLY")){
					if(null == o1.getVoiceOnly() && null == o2.getVoiceOnly()){
						k = 0;
					}else if(null != o1.getVoiceOnly() && null == o2.getVoiceOnly()){
						k = -1;
					}else if(null == o1.getVoiceOnly() && null != o2.getVoiceOnly()){
						k = 1;
					}else {
						k = o1.getVoiceOnly().compareTo(o2.getVoiceOnly());
					}
				}else if(this.columnId2.equals("DIFFINCOUNT")){
					if(null == o1.getDiffInCount() && null == o2.getDiffInCount()){
						k = 0;
					}else if(null != o1.getDiffInCount() && null == o2.getDiffInCount()){
						k = -1;
					}else if(null == o1.getDiffInCount() && null != o2.getDiffInCount()){
						k = 1;
					}else {
						k = o1.getDiffInCount().compareTo(o2.getDiffInCount());
					}
				}else if(this.columnId2.equals("HSI")){
					if(null == o1.getHsi() && null == o2.getHsi()){
						k = 0;
					}else if(null != o1.getHsi() && null == o2.getHsi()){
						k = -1;
					}else if(null == o1.getHsi() && null != o2.getHsi()){
						k = 1;
					}else {
						k = o1.getHsi().compareTo(o2.getHsi());
					}
				}else if(this.columnId2.equals("TDMSDB3")){
					if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
						k = 0;
					}else if(null != o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
						k = -1;
					}else if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null != o2.getTotalMaxSpeedDnLoadBelow3()){
						k = 1;
					}else {
						k = o1.getTotalMaxSpeedDnLoadBelow3().compareTo(o2.getTotalMaxSpeedDnLoadBelow3());
					}
				}else if(this.columnId2.equals("TDMSDA3")){
					if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
						k = 0;
					}else if(null != o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
						k = -1;
					}else if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null != o2.getTotalMaxSpeedDnLoadAbove3()){
						k = 1;
					}else {
						k = o1.getTotalMaxSpeedDnLoadAbove3().compareTo(o2.getTotalMaxSpeedDnLoadAbove3());
					}
				}
			}
			
			if("-1" != this.columnId3){
				if(this.columnId3.equals("TERRITORY")){
					l = o1.getTerritoryName().compareTo(o2.getTerritoryName());
				}if(this.columnId3.equals("CLOSED_DT")){
					if(null == o1.getClosedDate() && null == o2.getClosedDate()){
						l = 0;
					}else if(null != o1.getClosedDate() && null == o2.getClosedDate()){
						l = -1;
					}else if(null == o1.getClosedDate() && null != o2.getClosedDate()){
						l = 1;
					}else {
						l = sdf.parse(o1.getClosedDate()).compareTo(sdf.parse(o2.getClosedDate()));
					}
				}if(this.columnId3.equals("DTBW")){
					if(null == o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
						l = 0;
					}else if(null != o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
						l = -1;
					}else if(null == o1.getDateToBeWorked() && null != o2.getDateToBeWorked()){
						l = 1;
					}else {
						l = sdf.parse(o1.getDateToBeWorked()).compareTo(sdf.parse(o2.getDateToBeWorked()));
					}
				}else if(this.columnId3.equals("TOWN")){
					if(null == o1.getTown() && null == o2.getTown()){
						l = 0;
					}else if(null != o1.getTown() && null == o2.getTown()){
						l = -1;
					}else if(null == o1.getTown() && null != o2.getTown()){
						l = 1;
					}else {
						l = o1.getTown().compareTo(o2.getTown());
					}
				}else if(this.columnId3.equals("DISTANCE")){
					if(null == o1.getDistance() && null == o2.getDistance()){
						l = 0;
					}else if(null != o1.getDistance() && null == o2.getDistance()){
						if("ASC".equals(this.sortOrder3)){l = -1;}else{l = 1;}
					}else if(null == o1.getDistance() && null != o2.getDistance()){
						if("ASC".equals(this.sortOrder3)){l = 1;}else{l = -1;}
					}else {
						l = new Double(o1.getDistance().replaceAll(",", "")).compareTo(new Double(o2.getDistance().replaceAll(",", "")));
					}
				}else if(this.columnId3.equals("TOTAL")){
					if(null == o1.getTotal() && null == o2.getTotal()){
						l = 0;
					}else if(null != o1.getTotal() && null == o2.getTotal()){
						l = -1;
					}else if(null == o1.getTotal() && null != o2.getTotal()){
						l = 1;
					}else {
						l = o1.getTotal().compareTo(o2.getTotal());
					}
				}else if(this.columnId3.equals("SHARED_BY")){
					if(null == o1.getSharedBy() && null == o2.getSharedBy()){
						l = 0;
					}else if(null != o1.getSharedBy() && null == o2.getSharedBy()){
						l = -1;
					}else if(null == o1.getSharedBy() && null != o2.getSharedBy()){
						l = 1;
					}else {
						l = o1.getSharedBy().compareTo(o2.getSharedBy());
					}
				}else if(this.columnId3.equals("NUMBER_OF_SHARED_LEADS")){
					if(null == o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
						l = 0;
					}else if(null != o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
						l = -1;
					}else if(null == o1.getNumOfSharedLeads() && null != o2.getNumOfSharedLeads()){
						l = 1;
					}else {
						l = o1.getNumOfSharedLeads().compareTo(o2.getNumOfSharedLeads());
					}
				}else if(this.columnId3.equals("FIOS_DATA")){
					if(null == o1.getFiosData() && null == o2.getFiosData()){
						l = 0;
					}else if(null != o1.getFiosData() && null == o2.getFiosData()){
						l = -1;
					}else if(null == o1.getFiosData() && null != o2.getFiosData()){
						l = 1;
					}else {
						l = o1.getFiosData().compareTo(o2.getFiosData());
					}
				}else if(this.columnId3.equals("FIOS_MIGRATION")){
					if(null == o1.getFiosMigration() && null == o2.getFiosMigration()){
						l = 0;
					}else if(null != o1.getFiosMigration() && null == o2.getFiosMigration()){
						l = -1;
					}else if(null == o1.getFiosMigration() && null != o2.getFiosMigration()){
						l = 1;
					}else {
						l = o1.getFiosMigration().compareTo(o2.getFiosMigration());
					}
				}else if(this.columnId3.equals("FIOS_TV")){
					if(null == o1.getFiosTV() && null == o2.getFiosTV()){
						l = 0;
					}else if(null != o1.getFiosTV() && null == o2.getFiosTV()){
						l = -1;
					}else if(null == o1.getFiosTV() && null != o2.getFiosTV()){
						l = 1;
					}else {
						l = o1.getFiosTV().compareTo(o2.getFiosTV());
					}
				}else if(this.columnId3.equals("WINBACK")){
					if(null == o1.getWinback() && null == o2.getWinback()){
						l = 0;
					}else if(null != o1.getWinback() && null == o2.getWinback()){
						l = -1;
					}else if(null == o1.getWinback() && null != o2.getWinback()){
						l = 1;
					}else {
						l = o1.getWinback().compareTo(o2.getWinback());
					}
				}else if(this.columnId3.equals("PROSPECT")){
					if(null == o1.getProspect() && null == o2.getProspect()){
						l = 0;
					}else if(null != o1.getProspect() && null == o2.getProspect()){
						l = -1;
					}else if(null == o1.getProspect() && null != o2.getProspect()){
						l = 1;
					}else {
						l = o1.getProspect().compareTo(o2.getProspect());
					}
				}else if(this.columnId3.equals("VOICEONLY")){
					if(null == o1.getVoiceOnly() && null == o2.getVoiceOnly()){
						l = 0;
					}else if(null != o1.getVoiceOnly() && null == o2.getVoiceOnly()){
						l = -1;
					}else if(null == o1.getVoiceOnly() && null != o2.getVoiceOnly()){
						l = 1;
					}else {
						l = o1.getVoiceOnly().compareTo(o2.getVoiceOnly());
					}
				}else if(this.columnId3.equals("DIFFINCOUNT")){
					if(null == o1.getDiffInCount() && null == o2.getDiffInCount()){
						l = 0;
					}else if(null != o1.getDiffInCount() && null == o2.getDiffInCount()){
						l = -1;
					}else if(null == o1.getDiffInCount() && null != o2.getDiffInCount()){
						l = 1;
					}else {
						l = o1.getDiffInCount().compareTo(o2.getDiffInCount());
					}
				}else if(this.columnId3.equals("HSI")){
					if(null == o1.getHsi() && null == o2.getHsi()){
						l = 0;
					}else if(null != o1.getHsi() && null == o2.getHsi()){
						l = -1;
					}else if(null == o1.getHsi() && null != o2.getHsi()){
						l = 1;
					}else {
						l = o1.getHsi().compareTo(o2.getHsi());
					}
				}else if(this.columnId3.equals("TDMSDB3")){
					if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
						l = 0;
					}else if(null != o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
						l = -1;
					}else if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null != o2.getTotalMaxSpeedDnLoadBelow3()){
						l = 1;
					}else {
						l = o1.getTotalMaxSpeedDnLoadBelow3().compareTo(o2.getTotalMaxSpeedDnLoadBelow3());
					}
				}else if(this.columnId3.equals("TDMSDA3")){
					if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
						l = 0;
					}else if(null != o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
						l = -1;
					}else if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null != o2.getTotalMaxSpeedDnLoadAbove3()){
						l = 1;
					}else {
						l = o1.getTotalMaxSpeedDnLoadAbove3().compareTo(o2.getTotalMaxSpeedDnLoadAbove3());
					}
				}
			}
			
			if("-1" != this.columnId4){
				if(this.columnId4.equals("TERRITORY")){
					m = o1.getTerritoryName().compareTo(o2.getTerritoryName());
				}if(this.columnId4.equals("CLOSED_DT")){
					if(null == o1.getClosedDate() && null == o2.getClosedDate()){
						m = 0;
					}else if(null != o1.getClosedDate() && null == o2.getClosedDate()){
						m = -1;
					}else if(null == o1.getClosedDate() && null != o2.getClosedDate()){
						m = 1;
					}else {
						m = sdf.parse(o1.getClosedDate()).compareTo(sdf.parse(o2.getClosedDate()));
					}
				}if(this.columnId4.equals("DTBW")){
					if(null == o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
						m = 0;
					}else if(null != o1.getDateToBeWorked() && null == o2.getDateToBeWorked()){
						m = -1;
					}else if(null == o1.getDateToBeWorked() && null != o2.getDateToBeWorked()){
						m = 1;
					}else {
						m = sdf.parse(o1.getDateToBeWorked()).compareTo(sdf.parse(o2.getDateToBeWorked()));
					}
				}else if(this.columnId4.equals("TOWN")){
					if(null == o1.getTown() && null == o2.getTown()){
						m = 0;
					}else if(null != o1.getTown() && null == o2.getTown()){
						m = -1;
					}else if(null == o1.getTown() && null != o2.getTown()){
						m = 1;
					}else {
						m = o1.getTown().compareTo(o2.getTown());
					}
				}else if(this.columnId4.equals("DISTANCE")){
					if(null == o1.getDistance() && null == o2.getDistance()){
						m = 0;
					}else if(null != o1.getDistance() && null == o2.getDistance()){
						if("ASC".equals(this.sortOrder4)){m = -1;}else{m = 1;}
					}else if(null == o1.getDistance() && null != o2.getDistance()){
						if("ASC".equals(this.sortOrder4)){m = 1;}else{m = -1;}
					}else {
						m = new Double(o1.getDistance().replaceAll(",", "")).compareTo(new Double(o2.getDistance().replaceAll(",", "")));
					}
				}else if(this.columnId4.equals("TOTAL")){
					if(null == o1.getTotal() && null == o2.getTotal()){
						m = 0;
					}else if(null != o1.getTotal() && null == o2.getTotal()){
						m = -1;
					}else if(null == o1.getTotal() && null != o2.getTotal()){
						m = 1;
					}else {
						m = o1.getTotal().compareTo(o2.getTotal());
					}
				}else if(this.columnId4.equals("SHARED_BY")){
					if(null == o1.getSharedBy() && null == o2.getSharedBy()){
						m = 0;
					}else if(null != o1.getSharedBy() && null == o2.getSharedBy()){
						m = -1;
					}else if(null == o1.getSharedBy() && null != o2.getSharedBy()){
						m = 1;
					}else {
						m = o1.getSharedBy().compareTo(o2.getSharedBy());
					}
				}else if(this.columnId4.equals("NUMBER_OF_SHARED_LEADS")){
					if(null == o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
						m = 0;
					}else if(null != o1.getNumOfSharedLeads() && null == o2.getNumOfSharedLeads()){
						m = -1;
					}else if(null == o1.getNumOfSharedLeads() && null != o2.getNumOfSharedLeads()){
						m = 1;
					}else {
						m = o1.getNumOfSharedLeads().compareTo(o2.getNumOfSharedLeads());
					}
				}else if(this.columnId4.equals("FIOS_DATA")){
					if(null == o1.getFiosData() && null == o2.getFiosData()){
						m = 0;
					}else if(null != o1.getFiosData() && null == o2.getFiosData()){
						m = -1;
					}else if(null == o1.getFiosData() && null != o2.getFiosData()){
						m = 1;
					}else {
						m = o1.getFiosData().compareTo(o2.getFiosData());
					}
				}else if(this.columnId4.equals("FIOS_MIGRATION")){
					if(null == o1.getFiosMigration() && null == o2.getFiosMigration()){
						m = 0;
					}else if(null != o1.getFiosMigration() && null == o2.getFiosMigration()){
						m = -1;
					}else if(null == o1.getFiosMigration() && null != o2.getFiosMigration()){
						m = 1;
					}else {
						m = o1.getFiosMigration().compareTo(o2.getFiosMigration());
					}
				}else if(this.columnId4.equals("FIOS_TV")){
					if(null == o1.getFiosTV() && null == o2.getFiosTV()){
						m = 0;
					}else if(null != o1.getFiosTV() && null == o2.getFiosTV()){
						m = -1;
					}else if(null == o1.getFiosTV() && null != o2.getFiosTV()){
						m = 1;
					}else {
						m = o1.getFiosTV().compareTo(o2.getFiosTV());
					}
				}else if(this.columnId4.equals("WINBACK")){
					if(null == o1.getWinback() && null == o2.getWinback()){
						m = 0;
					}else if(null != o1.getWinback() && null == o2.getWinback()){
						m = -1;
					}else if(null == o1.getWinback() && null != o2.getWinback()){
						m = 1;
					}else {
						m = o1.getWinback().compareTo(o2.getWinback());
					}
				}else if(this.columnId4.equals("PROSPECT")){
					if(null == o1.getProspect() && null == o2.getProspect()){
						m = 0;
					}else if(null != o1.getProspect() && null == o2.getProspect()){
						m = -1;
					}else if(null == o1.getProspect() && null != o2.getProspect()){
						m = 1;
					}else {
						m = o1.getProspect().compareTo(o2.getProspect());
					}
				}else if(this.columnId4.equals("VOICEONLY")){
					if(null == o1.getVoiceOnly() && null == o2.getVoiceOnly()){
						m = 0;
					}else if(null != o1.getVoiceOnly() && null == o2.getVoiceOnly()){
						m = -1;
					}else if(null == o1.getVoiceOnly() && null != o2.getVoiceOnly()){
						m = 1;
					}else {
						m = o1.getVoiceOnly().compareTo(o2.getVoiceOnly());
					}
				}else if(this.columnId4.equals("DIFFINCOUNT")){
					if(null == o1.getDiffInCount() && null == o2.getDiffInCount()){
						m = 0;
					}else if(null != o1.getDiffInCount() && null == o2.getDiffInCount()){
						m = -1;
					}else if(null == o1.getDiffInCount() && null != o2.getDiffInCount()){
						m = 1;
					}else {
						m = o1.getDiffInCount().compareTo(o2.getDiffInCount());
					}
				}else if(this.columnId4.equals("HSI")){
					if(null == o1.getHsi() && null == o2.getHsi()){
						m = 0;
					}else if(null != o1.getHsi() && null == o2.getHsi()){
						m = -1;
					}else if(null == o1.getHsi() && null != o2.getHsi()){
						m = 1;
					}else {
						m = o1.getHsi().compareTo(o2.getHsi());
					}
				}else if(this.columnId4.equals("TDMSDB3")){
					if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
						m = 0;
					}else if(null != o1.getTotalMaxSpeedDnLoadBelow3() && null == o2.getTotalMaxSpeedDnLoadBelow3()){
						m = -1;
					}else if(null == o1.getTotalMaxSpeedDnLoadBelow3() && null != o2.getTotalMaxSpeedDnLoadBelow3()){
						m = 1;
					}else {
						m = o1.getTotalMaxSpeedDnLoadBelow3().compareTo(o2.getTotalMaxSpeedDnLoadBelow3());
					}
				}else if(this.columnId4.equals("TDMSDA3")){
					if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
						m = 0;
					}else if(null != o1.getTotalMaxSpeedDnLoadAbove3() && null == o2.getTotalMaxSpeedDnLoadAbove3()){
						m = -1;
					}else if(null == o1.getTotalMaxSpeedDnLoadAbove3() && null != o2.getTotalMaxSpeedDnLoadAbove3()){
						m = 1;
					}else {
						m = o1.getTotalMaxSpeedDnLoadAbove3().compareTo(o2.getTotalMaxSpeedDnLoadAbove3());
					}
				}
			}
			
			if ( i < 0 ) {  
    			retStatus = -1;  
    		} else if ( i > 0 ) {  
    			retStatus = 1;  
    		} else {
    			if("ASC".equals(this.sortOrder1)){
    				if ( j < 0 ) {  
	    				retStatus = -1;  
	    			} else if ( j > 0 ) {  
	    				retStatus = 1;  
	    			} else {  
	    				if("ASC".equals(this.sortOrder2)){
	    					if ( k < 0 ) {  
	    	    				retStatus = -1;  
	    	    			} else if ( k > 0 ) {  
	    	    				retStatus = 1;  
	    	    			} else {  
	    	    				if("ASC".equals(this.sortOrder3)){
	    	    					if ( l < 0 ) {  
	    	    	    				retStatus = -1;  
	    	    	    			} else if ( l > 0 ) {  
	    	    	    				retStatus = 1;  
	    	    	    			} else {  
	    	    	    				if("ASC".equals(this.sortOrder4)){
	    	    	    					if ( m < 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m > 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}else{
	    	    	    					if ( m > 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m < 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}
	    	    	    			}
	    	    				}else{
	    	    					if ( l > 0 ) {  
	    	    	    				retStatus = -1;  
	    	    	    			} else if ( l < 0 ) {  
	    	    	    				retStatus = 1;  
	    	    	    			} else {  
	    	    	    				if("ASC".equals(this.sortOrder4)){
	    	    	    					if ( m < 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m > 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}else{
	    	    	    					if ( m > 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m < 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}  
	    	    	    			}
	    	    				}  
	    	    			}
	    				}else{
	    					if ( k > 0 ) {  
	    	    				retStatus = -1;  
	    	    			} else if ( k < 0 ) {  
	    	    				retStatus = 1;  
	    	    			} else {  
	    	    				if("ASC".equals(this.sortOrder3)){
	    	    					if ( l < 0 ) {  
	    	    	    				retStatus = -1;  
	    	    	    			} else if ( l > 0 ) {  
	    	    	    				retStatus = 1;  
	    	    	    			} else {  
	    	    	    				if("ASC".equals(this.sortOrder4)){
	    	    	    					if ( m < 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m > 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}else{
	    	    	    					if ( m > 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m < 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				} 
	    	    	    			}
	    	    				}else{
	    	    					if ( l > 0 ) {  
	    	    	    				retStatus = -1;  
	    	    	    			} else if ( l < 0 ) {  
	    	    	    				retStatus = 1;  
	    	    	    			} else {  
	    	    	    				if("ASC".equals(this.sortOrder4)){
	    	    	    					if ( m < 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m > 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}else{
	    	    	    					if ( m > 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m < 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}  
	    	    	    			}
	    	    				}  
	    	    			}
	    				}  
	    			}
    			}else{
    				if ( j > 0 ) {  
	    				retStatus = -1;  
	    			} else if ( j < 0 ) {  
	    				retStatus = 1;  
	    			} else {  
	    				if("ASC".equals(this.sortOrder2)){
	    					if ( k < 0 ) {  
	    	    				retStatus = -1;  
	    	    			} else if ( k > 0 ) {  
	    	    				retStatus = 1;  
	    	    			} else {  
	    	    				if("ASC".equals(this.sortOrder3)){
	    	    					if ( l < 0 ) {  
	    	    	    				retStatus = -1;  
	    	    	    			} else if ( l > 0 ) {  
	    	    	    				retStatus = 1;  
	    	    	    			} else {  
	    	    	    				if("ASC".equals(this.sortOrder4)){
	    	    	    					if ( m < 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m > 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}else{
	    	    	    					if ( m > 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m < 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}  
	    	    	    			}
	    	    				}else{
	    	    					if ( l > 0 ) {  
	    	    	    				retStatus = -1;  
	    	    	    			} else if ( l < 0 ) {  
	    	    	    				retStatus = 1;  
	    	    	    			} else {  
	    	    	    				if("ASC".equals(this.sortOrder4)){
	    	    	    					if ( m < 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m > 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}else{
	    	    	    					if ( m > 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m < 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}  
	    	    	    			}
	    	    				}  
	    	    			}
	    				}else{
	    					if ( k > 0 ) {  
	    	    				retStatus = -1;  
	    	    			} else if ( k < 0 ) {  
	    	    				retStatus = 1;  
	    	    			} else {  
	    	    				if("ASC".equals(this.sortOrder3)){
	    	    					if ( l < 0 ) {  
	    	    	    				retStatus = -1;  
	    	    	    			} else if ( l > 0 ) {  
	    	    	    				retStatus = 1;  
	    	    	    			} else {  
	    	    	    				if("ASC".equals(this.sortOrder4)){
	    	    	    					if ( m < 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m > 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}else{
	    	    	    					if ( m > 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m < 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}  
	    	    	    			}
	    	    				}else{
	    	    					if ( l > 0 ) {  
	    	    	    				retStatus = -1;  
	    	    	    			} else if ( l < 0 ) {  
	    	    	    				retStatus = 1;  
	    	    	    			} else {  
	    	    	    				if("ASC".equals(this.sortOrder4)){
	    	    	    					if ( m < 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m > 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}else{
	    	    	    					if ( m > 0 ) {  
	    	    	    	    				retStatus = -1;  
	    	    	    	    			} else if ( m < 0 ) {  
	    	    	    	    				retStatus = 1;  
	    	    	    	    			} else {  
	    	    	    	    				retStatus = 0;  
	    	    	    	    			}
	    	    	    				}  
	    	    	    			}
	    	    				}  
	    	    			}
	    				}  
	    			}
    			}
    		}
			
//			if("ASC".equals(this.sortOrder)){
//	    		if ( i < 0 ) {  
//	    			retStatus = -1;  
//	    		} else if ( i > 0 ) {  
//	    			retStatus = 1;  
//	    		} else {  
//	    			if ( j < 0 ) {  
//	    				retStatus = -1;  
//	    			} else if ( j > 0 ) {  
//	    				retStatus = 1;  
//	    			} else {  
//	    				retStatus = 0;  
//	    			}  
//	    		}
//	    	}else{
//	    		if ( i < 0 ) {  
//	                retStatus = -1;  
//	    		} else if ( i > 0 ) {  
//	                retStatus = 1;  
//	    		} else {  
//	    			if ( j > 0 ) {  
//	    				retStatus = -1;  
//	    			} else if ( j < 0 ) {  
//	    				retStatus = 1;  
//	    			} else {  
//	    				retStatus = 0;  
//	    			}  
//	    		}
//	    	}
		}catch(Exception e){
			System.out.println("Exception :"+e);
		}  
		return retStatus;
	}

}
