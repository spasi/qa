package com.cydcor.framework.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Spring Service Locator
 * @author ashwin
 *
 */
public class ServiceLocator implements ApplicationContextAware {
	protected final Log logger = LogFactory.getLog(getClass());
	private static ApplicationContext applicationContext = null;

	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		logger.debug("***Spring application context Acquired****");
		ServiceLocator.applicationContext = applicationContext;
	}

	/**
	 * @return
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * This method assumes that the bean created has the same name as the class.
	 * 
	 * 
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getService(Class<T> clazz) {
		if (clazz == null) {
			return null;
		}

		int servicePackageLength = clazz.getPackage().getName().length() + 1;

		StringBuffer serviceName = new StringBuffer(clazz.getName().substring(servicePackageLength));

		serviceName.setCharAt(0, Character.toLowerCase(serviceName.charAt(0)));
				
		return (T) getService(serviceName.toString());
	}

	/**
	 * @param beanName
	 * @return
	 */
	public static Object getService(String beanName) {
		if (applicationContext == null) {			
			return null;
		}

		Object o = applicationContext.getBean(beanName);
		return o;
	}
	
	
	
}

