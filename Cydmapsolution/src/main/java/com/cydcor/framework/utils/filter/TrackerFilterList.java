package com.cydcor.framework.utils.filter;

import java.util.LinkedList;
import java.util.List;

import com.cydcor.framework.model.VerizonFiosReport;



public class TrackerFilterList {
	//public enum OPERATION {BEGIN_WITH, EQUALS, NOT_EQUALS, IS_LIKE, IS_NOT_LIKE, GREATER_THAN, LESS_THAN, ON_BEFORE, ON_AFTER};
    public  List<VerizonFiosReport> filterList(List<VerizonFiosReport> originalList, String text, String columnName, String opName, boolean percentage) {
        List<VerizonFiosReport> filterList = new LinkedList<VerizonFiosReport>();
        
        	if("BEGIN_WITH".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (VerizonFiosTrackerFilter.beginsWith(object, text, columnName)){
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("IS_LIKE".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (VerizonFiosTrackerFilter.isLike(object, text,columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("NOT_LIKE".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (VerizonFiosTrackerFilter.isNotLike(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("GREATER_THAN".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (VerizonFiosTrackerFilter.isGreaterThan(object, text, columnName, percentage)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("LESS_THAN".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (VerizonFiosTrackerFilter.isLessThan(object, text, columnName, percentage)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("ON_BEFORE".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (VerizonFiosTrackerFilter.isOnOrBefore(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("ON_AFTER".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (VerizonFiosTrackerFilter.isOnOrAfter(object, text, columnName)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("EQUALS".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (VerizonFiosTrackerFilter.isEquals(object, text, columnName, percentage)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}else if("NOT_EQUALS".equals(opName)){
        		for (VerizonFiosReport object : originalList) {
            		if (VerizonFiosTrackerFilter.isNotEquals(object, text, columnName, percentage)) {
            			filterList.add(object);
            		} else {
            			continue;
            		}
            	}
        		
        	}
        	
        return filterList;
    }
}
