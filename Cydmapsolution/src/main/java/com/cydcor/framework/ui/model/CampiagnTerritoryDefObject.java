package com.cydcor.framework.ui.model;

import java.io.Serializable;

public class CampiagnTerritoryDefObject implements Serializable {

	private String iclCode;
	private String iclName;
	private String iclOwner;
	private String iclAddress;
	private String iclStatus;
	private String iclCreatedDate;

	public String getIclCode() {
		return iclCode;
	}

	public void setIclCode(String iclCode) {
		this.iclCode = iclCode;
	}

	public String getIclName() {
		return iclName;
	}

	public void setIclName(String iclName) {
		this.iclName = iclName;
	}

	public String getIclOwner() {
		return iclOwner;
	}

	public void setIclOwner(String iclOwner) {
		this.iclOwner = iclOwner;
	}

	public String getIclAddress() {
		return iclAddress;
	}

	public void setIclAddress(String iclAddress) {
		this.iclAddress = iclAddress;
	}

	public String getIclStatus() {
		return iclStatus;
	}

	public void setIclStatus(String iclStatus) {
		this.iclStatus = iclStatus;
	}

	public String getIclCreatedDate() {
		return iclCreatedDate;
	}

	public void setIclCreatedDate(String iclCreatedDate) {
		this.iclCreatedDate = iclCreatedDate;
	}

}
