/**
 * 
 */
package com.cydcor.framework.ui.model;

/**
 * @author ashwin
 * 
 */
public class RequestObject {

	private int offset = 0;

	private int pageSize = 50;

	private boolean queryForTotal = false;

	/**
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * @param offset
	 *            the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize
	 *            the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the queryForTotal
	 */
	public boolean isQueryForTotal() {
		return queryForTotal;
	}

	/**
	 * @param queryForTotal
	 *            the queryForTotal to set
	 */
	public void setQueryForTotal(boolean queryForTotal) {
		this.queryForTotal = queryForTotal;
	}

}
