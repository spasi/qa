/**
 * 
 */
package com.cydcor.framework.ui.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashwin
 * 
 */
public class UIForm {
	private String formName = "";
	private String legendName = "";
	private String formStyle = "";
	private int numberOfColumns = 3;
	private List<FormElement> elements = new ArrayList<FormElement>();
	private String formActionUrl = "";

	/**
	 * @return the formName
	 */
	public String getFormName() {
		return formName;
	}

	/**
	 * @param formName
	 *            the formName to set
	 */
	public void setFormName(String formName) {
		this.formName = formName;
	}

	/**
	 * @return the legendName
	 */
	public String getLegendName() {
		return legendName;
	}

	/**
	 * @param legendName
	 *            the legendName to set
	 */
	public void setLegendName(String legendName) {
		this.legendName = legendName;
	}

	/**
	 * @return the formStyle
	 */
	public String getFormStyle() {
		return formStyle;
	}

	/**
	 * @param formStyle
	 *            the formStyle to set
	 */
	public void setFormStyle(String formStyle) {
		this.formStyle = formStyle;
	}

	/**
	 * @return the numberOfColumns
	 */
	public int getNumberOfColumns() {
		return numberOfColumns;
	}

	/**
	 * @param numberOfColumns
	 *            the numberOfColumns to set
	 */
	public void setNumberOfColumns(int numberOfColumns) {
		this.numberOfColumns = numberOfColumns;
	}

	/**
	 * @return the elements
	 */
	public List<FormElement> getElements() {
		return elements;
	}

	/**
	 * @param elements
	 *            the elements to set
	 */
	public void setElements(List<FormElement> elements) {
		this.elements = elements;
	}

	/**
	 * @return the formActionUrl
	 */
	public String getFormActionUrl() {
		return formActionUrl;
	}

	/**
	 * @param formActionUrl
	 *            the formActionUrl to set
	 */
	public void setFormActionUrl(String formActionUrl) {
		this.formActionUrl = formActionUrl;
	}

}
