/**
 * 
 */
package com.cydcor.framework.ui.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashwin
 * 
 */
public class RicoTable {
	private String tableSelectNew = "___new___";
	private String tableSelectNone = "";
	private String click = "onClick";
	private boolean canAdd = false;
	private boolean canEdit = false;
	private boolean canDelete = false;
	private boolean ConfirmDelete = true;
	private String confirmDeleteCol = "0";
	private boolean debugFlag = false;
	private boolean prefetchBuffer = true;
	private boolean panelNamesOnTabHdr = true;
	private String highlightElem = "cursorRow";
	private int panelWidth = 450;
	private int frozenColumns = 0;
	private String menuEvent = "contextmenu";
	private int filterLocation = -1;
	private int sortCol = 0;
	private String sortDir = "ASC";
	private String panels = "['Basic Info','Ship To']";
	private List<RicoColumnSpec> ricoColumns = new ArrayList<RicoColumnSpec>();

	/**
	 * @return the tableSelectNew
	 */
	public String getTableSelectNew() {
		return tableSelectNew;
	}

	/**
	 * @param tableSelectNew
	 *            the tableSelectNew to set
	 */
	public void setTableSelectNew(String tableSelectNew) {
		this.tableSelectNew = tableSelectNew;
	}

	/**
	 * @return the tableSelectNone
	 */
	public String getTableSelectNone() {
		return tableSelectNone;
	}

	/**
	 * @param tableSelectNone
	 *            the tableSelectNone to set
	 */
	public void setTableSelectNone(String tableSelectNone) {
		this.tableSelectNone = tableSelectNone;
	}

	/**
	 * @return the click
	 */
	public String getClick() {
		return click;
	}

	/**
	 * @param click
	 *            the click to set
	 */
	public void setClick(String click) {
		this.click = click;
	}

	/**
	 * @return the canAdd
	 */
	public boolean isCanAdd() {
		return canAdd;
	}

	/**
	 * @param canAdd
	 *            the canAdd to set
	 */
	public void setCanAdd(boolean canAdd) {
		this.canAdd = canAdd;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit
	 *            the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the canDelete
	 */
	public boolean isCanDelete() {
		return canDelete;
	}

	/**
	 * @param canDelete
	 *            the canDelete to set
	 */
	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}

	/**
	 * @return the confirmDelete
	 */
	public boolean isConfirmDelete() {
		return ConfirmDelete;
	}

	/**
	 * @param confirmDelete
	 *            the confirmDelete to set
	 */
	public void setConfirmDelete(boolean confirmDelete) {
		ConfirmDelete = confirmDelete;
	}

	/**
	 * @return the confirmDeleteCol
	 */
	public String getConfirmDeleteCol() {
		return confirmDeleteCol;
	}

	/**
	 * @param confirmDeleteCol
	 *            the confirmDeleteCol to set
	 */
	public void setConfirmDeleteCol(String confirmDeleteCol) {
		this.confirmDeleteCol = confirmDeleteCol;
	}

	/**
	 * @return the debugFlag
	 */
	public boolean isDebugFlag() {
		return debugFlag;
	}

	/**
	 * @param debugFlag
	 *            the debugFlag to set
	 */
	public void setDebugFlag(boolean debugFlag) {
		this.debugFlag = debugFlag;
	}

	/**
	 * @return the prefetchBuffer
	 */
	public boolean isPrefetchBuffer() {
		return prefetchBuffer;
	}

	/**
	 * @param prefetchBuffer
	 *            the prefetchBuffer to set
	 */
	public void setPrefetchBuffer(boolean prefetchBuffer) {
		this.prefetchBuffer = prefetchBuffer;
	}

	/**
	 * @return the panelNamesOnTabHdr
	 */
	public boolean isPanelNamesOnTabHdr() {
		return panelNamesOnTabHdr;
	}

	/**
	 * @param panelNamesOnTabHdr
	 *            the panelNamesOnTabHdr to set
	 */
	public void setPanelNamesOnTabHdr(boolean panelNamesOnTabHdr) {
		this.panelNamesOnTabHdr = panelNamesOnTabHdr;
	}

	/**
	 * @return the highlightElem
	 */
	public String getHighlightElem() {
		return highlightElem;
	}

	/**
	 * @param highlightElem
	 *            the highlightElem to set
	 */
	public void setHighlightElem(String highlightElem) {
		this.highlightElem = highlightElem;
	}

	/**
	 * @return the panelWidth
	 */
	public int getPanelWidth() {
		return panelWidth;
	}

	/**
	 * @param panelWidth
	 *            the panelWidth to set
	 */
	public void setPanelWidth(int panelWidth) {
		this.panelWidth = panelWidth;
	}

	/**
	 * @return the frozenColumns
	 */
	public int getFrozenColumns() {
		return frozenColumns;
	}

	/**
	 * @param frozenColumns
	 *            the frozenColumns to set
	 */
	public void setFrozenColumns(int frozenColumns) {
		this.frozenColumns = frozenColumns;
	}

	/**
	 * @return the menuEvent
	 */
	public String getMenuEvent() {
		return menuEvent;
	}

	/**
	 * @param menuEvent
	 *            the menuEvent to set
	 */
	public void setMenuEvent(String menuEvent) {
		this.menuEvent = menuEvent;
	}

	/**
	 * @return the filterLocation
	 */
	public int getFilterLocation() {
		return filterLocation;
	}

	/**
	 * @param filterLocation
	 *            the filterLocation to set
	 */
	public void setFilterLocation(int filterLocation) {
		this.filterLocation = filterLocation;
	}

	/**
	 * @return the sortCol
	 */
	public int getSortCol() {
		return sortCol;
	}

	/**
	 * @param sortCol
	 *            the sortCol to set
	 */
	public void setSortCol(int sortCol) {
		this.sortCol = sortCol;
	}

	/**
	 * @return the sortDir
	 */
	public String getSortDir() {
		return sortDir;
	}

	/**
	 * @param sortDir
	 *            the sortDir to set
	 */
	public void setSortDir(String sortDir) {
		this.sortDir = sortDir;
	}

	/**
	 * @return the panels
	 */
	public String getPanels() {
		return panels;
	}

	/**
	 * @param panels
	 *            the panels to set
	 */
	public void setPanels(String panels) {
		this.panels = panels;
	}

	/**
	 * @return the ricoColumns
	 */
	public List<RicoColumnSpec> getRicoColumns() {
		return ricoColumns;
	}

	/**
	 * @param ricoColumns
	 *            the ricoColumns to set
	 */
	public void setRicoColumns(List<RicoColumnSpec> ricoColumns) {
		this.ricoColumns = ricoColumns;
	}

	/**
	 * @param columnSpec
	 */
	public void addRicoColumn(RicoColumnSpec columnSpec) {
		this.ricoColumns.add(columnSpec);
	}

}
