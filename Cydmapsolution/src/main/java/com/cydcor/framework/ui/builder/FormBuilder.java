/**
 * 
 */
package com.cydcor.framework.ui.builder;

import net.sf.click.MockContext;
import net.sf.click.control.Form;
import net.sf.click.control.TextField;
import net.sf.click.util.ClickUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.ui.model.UIForm;

/**
 * @author ashwin
 * 
 */
public class FormBuilder {
	private static final transient Log logger = LogFactory.getLog(FormBuilder.class);

	private static FormBuilder formBuilder = new FormBuilder();

	/**
	 *Making singleton
	 */
	private FormBuilder() {

	}

	/**
	 * @return
	 */
	public static FormBuilder getInstance() {
		return formBuilder;
	}

	public String buildForm(UIForm form) {
		// Create mock context in which to test the container.
		MockContext.initContext();
		Form x = new Form("FILTER");
		TextField field = new TextField("Test", "Test");
		x.add(field);
		logger.debug(x.toString());

		return "";
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String testString = "http://localhost:8080/OpenHealth/reporthandler?reportName=FORUM_DETAILS&timestamp=821&id=genericTable&page_size=56&offset=0&get_total=true&s0=ASC&f%5B1%5D%5Bop%5D=LIKE&f%5B1%5D%5Blen%5D=1&f%5B1%5D%5B0%5D=*c*&f%5B2%5D%5Bop%5D=LIKE&f%5B2%5D%5Blen%5D=1&f%5B2%5D%5B0%5D=*R*";
		
		testString = "http://localhost:8080/OpenHealth/reporthandler?reportName=FORUM_DETAILS&timestamp=257&id=genericTable&page_size=56&offset=0&get_total=true&s0=ASC&f%5B2%5D%5Bop%5D=LIKE&f%5B2%5D%5Blen%5D=1&f%5B2%5D%5B0%5D=*c*";
		
		testString = "http://localhost:8080/OpenHealth/reporthandler?reportName=FORUM_DETAILS&timestamp=608&id=genericTable&page_size=56&offset=0&get_total=true&s0=ASC&f%5B0%5D%5Bop%5D=LIKE&f%5B0%5D%5Blen%5D=1&f%5B0%5D%5B0%5D=*c*&f%5B1%5D%5Bop%5D=LIKE&f%5B1%5D%5Blen%5D=1&f%5B1%5D%5B0%5D=*c*";
		
		testString = "http://localhost:8080/OpenHealth/reporthandler?reportName=FORUM_DETAILS&timestamp=608&id=genericTable&page_size=56&offset=0&get_total=true&s0=ASC&f%5B0%5D%5Bop%5D=LIKE&f%5B0%5D%5Blen%5D=1&f%5B0%5D%5B0%5D=*c*&f%5B1%5D%5Bop%5D=LIKE&f%5B1%5D%5Blen%5D=1&f%5B1%5D%5B0%5D=*ca*";
		// Create mock context in which to test the container.
		MockContext.initContext();
		logger.debug(ClickUtils.decodeURL(testString));
		
		
		String filterValue = "*ca*";
		logger.debug(filterValue.substring(1,filterValue.length()-1));
	}

}
