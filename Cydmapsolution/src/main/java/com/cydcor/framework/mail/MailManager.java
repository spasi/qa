package com.cydcor.framework.mail;

import java.io.File;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.ServiceLocator;

public class MailManager {
	
	private static final transient Log logger = LogFactory
			.getLog(MailManager.class);
	
	public void sendMail(String fromEmailId,String[] toEmailId,String subject,String templateName,Map data) throws MessagingException {
		MimeMessage msg =  buildMessage(fromEmailId, toEmailId, subject, templateName, data,false,null);
		((JavaMailSender) ServiceLocator.getService("mailSender")).send(msg);
	}
	
	public void sendMailWithAttachment(String fromEmailId,String toEmailId[],String subject,String templateName,Map data,Map<String,File> attachments) throws MessagingException {
		((JavaMailSender) ServiceLocator.getService("mailSender")).send(buildMessage(fromEmailId, toEmailId, subject, templateName, data,true, attachments));
	}
	
	private MimeMessage buildMessage(String fromEmailId,String[] toEmailId,String subject,String templateName,Map data,boolean multipart,Map<String,File> attachments) throws MessagingException{
		String text = FreeMarkerEngine.getInstance().evaluateTemplate(templateName, data);
		MimeMessage msg = ((JavaMailSender) ServiceLocator.getService("mailSender")).createMimeMessage();
		// use the true flag to indicate you need a multipart message
		MimeMessageHelper helper = new MimeMessageHelper(msg, multipart);
		helper.setTo(toEmailId);
		helper.setFrom(fromEmailId);
		helper.setText(text,true);
		helper.setSubject(subject);
		if (multipart) {
			for(Map.Entry<String,File> entry:  attachments.entrySet()) {
				FileSystemResource file = new FileSystemResource(entry.getValue());
				helper.addAttachment(entry.getKey(), file);
			}
		}
		return msg ;
	}

}