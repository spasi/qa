package com.cydcor.framework.exceptions;

public class RoleNotFoundException extends GenericException {
	public RoleNotFoundException() {
		// TODO Auto-generated constructor stub
		super("Access denied: Your access to the Mapping application has not been configured. <br/><br/>Please contact your application administrator for access to the Mapping Solution.");
	}
}