package com.cydcor.framework.exceptions;

public class GenericException extends RuntimeException {
    static final long serialVersionUID = 1;

    private int count= 0;
    //	private Hashtable errorStack = new Hashtable();
    public GenericException() {
        super();
    }

    //    public Hashtable getErrors()
    //    {
    //        return errorStack;
    //    }

    //    public void setError(String errorCode, String errorMsg)
    //    {
    //    	errorStack.put(errorCode, errorMsg);
    //    }	
    public GenericException(String errorMsg) {
        super(errorMsg);
    }

    //	public TWException(String errorCode, String errorMsg)
    //	{
    //		errorStack.put(errorCode, errorMsg);
    //	}
    public GenericException(String errorMsg, Throwable cause) {
        super(errorMsg, cause);

        //        errorStack.put(errorCode, errorMsg);
    }

    public GenericException(Throwable cause,int count) {
    	super(cause);
    	setCount(count);
        //        errorStack.put(errorCode, errorMsg);
    }
    
    public GenericException(Throwable t) {
        super(t);
    }

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
}
