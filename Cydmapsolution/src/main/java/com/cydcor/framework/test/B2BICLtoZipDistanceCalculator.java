package com.cydcor.framework.test;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.B2BZipOffice;
import com.cydcor.framework.model.xml.b2breport.Distance;
import com.cydcor.framework.model.xml.b2breport.DistanceMatrixResponse;
import com.cydcor.framework.model.xml.b2breport.GeocodeResponse;
import com.cydcor.framework.model.xml.b2breport.Location;
import com.cydcor.framework.model.xml.b2breport.Row;
import com.cydcor.framework.rowmapper.B2BZipOfficeRowMapper;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.URLSigner;
import com.cydcor.framework.utils.XStreamUtils;
import com.cydcor.framework.ws.GenericWS;

public class B2BICLtoZipDistanceCalculator  extends GenericWS {
	private static final transient Log logger = LogFactory
			.getLog(B2BICLtoZipDistanceCalculator.class);
	
	/**
	 *
	 */
	public static void executeProcess() {
		
		logger.info("Firing Query for ZIP :"+QueryUtils.getQuery().get("SELECT_ZIP_OFFICE_BY_LASTUPDATE_FLAG"));
		String OneLine =QueryUtils.getQuery().get("SELECT_ZIP_OFFICE_BY_LASTUPDATE_FLAG");
		System.out.println("Test the last updated flag" + OneLine);
		
		List<B2BZipOffice> zipOfficeDetails = (List<B2BZipOffice>) getPlatformService().loadRowMapper(QueryUtils
										.getQuery().get("SELECT_ZIP_OFFICE_BY_LASTUPDATE_FLAG"),
										new B2BZipOfficeRowMapper()
										,"verizonb2b");
		
		System.out.println("Test the last updated flag zipOfficeDetails" + zipOfficeDetails);
		calculateDistance(zipOfficeDetails);
	}
	
	private static void calculateDistance(List<B2BZipOffice> listZip){
		logger.info("Number of records to calculate distance :"+listZip.size());
		String response1 = null;
		String response2 = null;
		HttpClient client = new HttpClient();
		GetMethod getMethod1, getMethod2 = null;
		GeocodeResponse gResponse = null;
		DistanceMatrixResponse dMResponse = null;
		Location loc = null;
		for(B2BZipOffice z : listZip){
			StringBuilder sBuilder1 = new StringBuilder("http://maps.googleapis.com/maps/api/geocode/xml?client=gme-cydcorinc&sensor=false&address=");
			StringBuilder sBuilder2 = new StringBuilder("http://maps.googleapis.com/maps/api/distancematrix/xml?client=gme-cydcorinc&sensor=false&units=imperial&mode='driving'");
			sBuilder1.append(z.getZip());
			
			System.out.println("sBuilder1....." + sBuilder1);
			System.out.println("sBuilder2....." + sBuilder2);
			System.out.println("sBuilder1 append ....." + sBuilder1.append(z.getZip()));
			try{
				if(null == z.getFullAddress()){
			    	logger.info("Office address is NULL for ZIP : "+z.getZip());
			    	Object[] obj = new Object[]{"ERROR",new Date(),"Office full address is null for zip",null != z.getRetryCount()?z.getRetryCount()+1:0,z.getZip(),z.getOfficeSeq()};
					getPlatformService().update(QueryUtils.getQuery().get("UPDATE_DISTANCE_ERROR_BY_ZIP_AND_OFFICE"), obj, "verizonb2b");
			    	continue;
			    }
				if(null == z.getLatitude() || null == z.getLongitude()){
					String request1 = URLSigner.getInstance().getSignedURL(sBuilder1.toString());
					getMethod1 = new GetMethod(request1);
					client.executeMethod(getMethod1);
					response1 = getMethod1.getResponseBodyAsString();
					
					System.out.println("Show the B2B response count.." + response1);
					
					gResponse = (GeocodeResponse) XStreamUtils.getObjectFromXml(GeocodeResponse.class, response1);
					System.out.println("Show the B2B gResponse count.." + gResponse);
					
					loc = gResponse.getResult().get(0).getGeometry().getLocation();
					System.out.println("Show the B2B location.." + loc);
					
				    z.setLatitude(loc.getLatitude());
				    z.setLongitude(loc.getLongitude());
				    
				    System.out.println("Latitude...." + z.getLatitude() + "Longitude....."  + z.getLongitude());
				}
				
			    sBuilder2.append("&origins="+z.getFullAddress().replace(' ', '+')+"&destinations="+z.getLatitude().trim()+","+z.getLongitude().trim());
			    
			    String request2 = URLSigner.getInstance().getSignedURL(sBuilder2.toString());
			    getMethod2 = new GetMethod(request2);
				client.executeMethod(getMethod2);
				response2 = getMethod2.getResponseBodyAsString();
				System.out.println("response2 for secound...." + response2);
				
				dMResponse = (DistanceMatrixResponse) XStreamUtils.getObjectFromXml(DistanceMatrixResponse.class, response2);
				System.out.println("dMResponse for secound...." + dMResponse);
				
				Row row = (Row) dMResponse.getRow().get(0);
				Distance distance = null;
				if("OK".equals(row.getElement().get(0).getStatus())){
					distance = row.getElement().get(0).getDistance().get(0);
					String dInt = distance.getText().split(" ")[0];//.replaceAll(",", "");
					logger.info("Text: "+distance.getText()+" Value :"+distance.getValue()); 
					Object[] obj = new Object[]{z.getLatitude(),z.getLongitude(),dInt,"PROCESSED",new Date(),z.getZip(),z.getOfficeSeq()};
					getPlatformService().update(QueryUtils.getQuery().get("UPDATE_DISTANCE_BY_ZIP_AND_OFFICE"), obj, "verizonb2b");
					logger.info("Distance updated successfully !!!");
				}else{
					logger.info("Distance Matrix API returned status: "+row.getElement().get(0).getStatus()); 
					String errorMessage = "ZERO_RESULTS".equals(row.getElement().get(0).getStatus())?"Status: ZERO_RESULTS - "+
					"No route could be found between the origin and destination":"Status: NOT_FOUND - Origin and/or destination of this pairing could not be geocoded";
			    	Object[] obj = new Object[]{z.getLatitude(),z.getLongitude(),"ERROR",new Date(),errorMessage,2,z.getZip(),z.getOfficeSeq()};
					getPlatformService().update(QueryUtils.getQuery().get("UPDATE_DISTANCE_STATUS_BY_ZIP_AND_OFFICE"), obj, "verizonb2b");	
					continue;
				}

			}catch(HttpException e){
				logger.info("HttpException : "+e.getMessage());
				Object[] obj = new Object[]{"ERROR",new Date(),e.getMessage(),null != z.getRetryCount()?z.getRetryCount()+1:0,z.getZip(),z.getOfficeSeq()};
				getPlatformService().update(QueryUtils.getQuery().get("UPDATE_DISTANCE_ERROR_BY_ZIP_AND_OFFICE"), obj, "verizonb2b");
		    	continue;
			} catch (IOException e) {
				logger.info("IOException : "+e.getMessage());
				Object[] obj = new Object[]{"ERROR",new Date(),e.getMessage(),null != z.getRetryCount()?z.getRetryCount()+1:0,z.getZip(),z.getOfficeSeq()};
				getPlatformService().update(QueryUtils.getQuery().get("UPDATE_DISTANCE_ERROR_BY_ZIP_AND_OFFICE"), obj, "verizonb2b");
		    	continue;
			}catch(Exception e){
				logger.info("Exception : "+e.getMessage());
				String errorMessage = e.getMessage()!=null?(e.getMessage().length()>500?e.getMessage().substring(0,500):e.getMessage()):"";
				if(response1!=null){
					errorMessage = response1.length()>750?errorMessage+"\n Zip geocoding response="+response1.substring(0, 750):errorMessage+
							"\n Zip geocoding response="+response1;
				}
				if(response2!=null){
					errorMessage = response2.length()>750?errorMessage+"\n Distance Matrix response="+response2.substring(0, 750):errorMessage+
							"\n Distance Matrix response="+response2;
				}
				Object[] obj = new Object[]{"ERROR",new Date(),errorMessage,null != z.getRetryCount()?z.getRetryCount()+1:0,z.getZip(),z.getOfficeSeq()};
				getPlatformService().update(QueryUtils.getQuery().get("UPDATE_DISTANCE_ERROR_BY_ZIP_AND_OFFICE"), obj, "verizonb2b");
		    	continue;
			}
		}
	}
}
