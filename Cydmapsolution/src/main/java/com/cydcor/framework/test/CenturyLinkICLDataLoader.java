package com.cydcor.framework.test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.MerOffice;
import com.cydcor.framework.model.MerOfficeInsert;
import com.cydcor.framework.model.glocation.Kml;
import com.cydcor.framework.model.glocation.Placemark;
import com.cydcor.framework.rowmapper.MerOfficeRowMapper;
import com.cydcor.framework.utils.GClientGeocoder;
import com.cydcor.framework.utils.ObjectUtils;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.ws.GenericWS;

public class CenturyLinkICLDataLoader extends GenericWS {

	private static final transient Log logger = LogFactory
			.getLog(CenturyLinkICLDataLoader.class);

	/**
	 *
	 */
	public static void executeProcess() {

		logger.info("Firing Query for ICL :"
						+ QueryUtils
								.getQuery()
								.get(
										"SELECT_MERLIN_OFFICE_DETAILS_BY_MODIFIED_DATE"));
		List<MerOffice> merOffices = (List<MerOffice>) getPlatformService()
				.loadRowMapper(
						QueryUtils
								.getQuery()
								.get(
										"SELECT_MERLIN_OFFICE_DETAILS_BY_MODIFIED_DATE"),
						new MerOfficeRowMapper()
						,"centurylink"
						);

		/*try {
			getPlatformService().deleteOfficeSeqs(merOffices);
		} catch (Exception e) {
			logger.debug(e);
		}*/
		insertOrUpdateintoIclMaster(merOffices);
	}

	/**
	 * @param merOffices
	 */
	private static void insertOrUpdateintoIclMaster(List<MerOffice> merOffices) {
		String address = "";
		String kml = null;
		String geoPoint = "";
		List<Placemark> placemarks = null;
		List<MerOfficeInsert> merOfficeInserts = new ArrayList<MerOfficeInsert>();
		MerOfficeInsert merInsert = null;
		Object[] values = null;
		int j = 0;
		logger.debug("Start Time ::" + new Date());
		logger.debug("Stared....");
		try {
			for (MerOffice merOffice : merOffices) {
				if (merOffice.getOffice_seq() == 0 || merOffice.getOffice_address1() == null) {
					continue;
				}
				merInsert = new MerOfficeInsert();
				// logger.debug("address:::"+leadInput.getFullAddr());
				address = merOffice.getOffice_address1() + ","
						+ merOffice.getOffice_city() + ","
						+ merOffice.getOffice_state() + ","
						+ merOffice.getOffice_zip();
				logger.debug("Address with diacritical marks "+address); 
				address = ObjectUtils.deAccent(address);	// TODO Modify to make geocoding work for accents
				logger.debug("Address after removing diacritical marks "+address);
				address = ObjectUtils.appendCommaForSpace(address);
				try {
					kml = GClientGeocoder.getGlocationV3(address);
				} catch (Exception e) {
					// logger.debug(e);
					logger.debug("Exception ::: FULL ADDRESS :::"
							+ merOffice.getOffice_address1());
					int officeSeq = merOffice.getOffice_seq();
					Date createdDate = new Timestamp(System.currentTimeMillis());
					Date modifiedDate = new Timestamp(System.currentTimeMillis());
					values = new Object[] { officeSeq, createdDate, 1,modifiedDate, 1, officeSeq, createdDate, 1,modifiedDate, 1 };
					getPlatformService().update(
							QueryUtils.getQuery().get("ERROR_SQL_ICL_MASTER"),
							values,"centurylink");
					continue;
				}
				/*
				Date createdDate = merOffice.getStart_date();
				Date modifiedDate = merOffice.getLast_update();

				if (createdDate != null)
					merInsert.setCreatedDate(new Timestamp(createdDate
							.getTime()));
				else
					merInsert.setCreatedDate(new Timestamp(System
							.currentTimeMillis()));

				if (modifiedDate != null)
					merInsert.setModifiedDate(new Timestamp(modifiedDate
							.getTime()));
				else
					merInsert.setModifiedDate(new Timestamp(System
							.currentTimeMillis()));
				*/
				merInsert.setOfficeSeq(merOffice.getOffice_seq());
				merInsert.setCreatedBy(1);
				merInsert.setModifiedBy(1);
				merInsert.setCreatedDate(new Timestamp((new Date()).getTime()));
				merInsert.setModifiedDate(new Timestamp((new Date()).getTime()));
				try {
				    merInsert.setFullAddr(GClientGeocoder.getGeocodedAddress(kml));
				    merInsert.setGeoLocation(GClientGeocoder.getGeocode(kml));
				    if("POINT( )".equalsIgnoreCase(merInsert.getGeoLocation()))
				    	continue;
				} catch (Exception e) {
				    logger.error("This error is from the KML: " + e);
				    continue;
				}

				merOfficeInserts.add(merInsert);
				if (merOfficeInserts.size() >= 300) {
					logger.debug("dbhit for 300 records ::: "
							+ merOfficeInserts.size());
					// hit the db
					getPlatformService().populateIclMaster(merOfficeInserts,"centurylink");
					merOfficeInserts.clear();
				}
			}

			if (merOfficeInserts.size() > 0) {
				logger.debug("dbhit less then 300 records ::: "
						+ merOfficeInserts.size());
				getPlatformService().populateIclMaster(merOfficeInserts,"centurylink");
			}
			logger.debug("End Time ::" + new Date());
		} catch (Exception e) {
			logger.debug("exception for address ::: " + address);
			logger.debug(e);
		}
	}
}
