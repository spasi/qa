package com.cydcor.framework.test;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cydcor.framework.utils.PropertyUtils;

public class PropertyUtilTest {
	private static final transient Log logger = LogFactory.getLog(PropertyUtilTest.class);
	public static void main(String[] args) throws IOException {
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "applicationContext*.xml" });
		PropertyUtils propertyUtils = (PropertyUtils) context.getBean("propertyUtils");
		logger.debug(propertyUtils.getProperty().get("Test"));

	}
}
