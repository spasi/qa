package com.cydcor.framework.model;

public class LeadAssignment extends GenericModel {
	
	private Integer rowId;
	private String office_Seq;
	private String office_Code;
	private String office_Name;
	private String clli;
	private String lfa;
	private String current_Owner;
	private String new_Owner;
	private String campaign_Seq;
	
	public Integer getRowId() {
		return rowId;
	}
	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}
	public String getOffice_Seq() {
		return office_Seq;
	}
	public void setOffice_Seq(String office_Seq) {
		this.office_Seq = office_Seq;
	}
	public String getOffice_Code() {
		return office_Code;
	}
	public void setOffice_Code(String office_Code) {
		this.office_Code = office_Code;
	}
	public String getOffice_Name() {
		return office_Name;
	}
	public void setOffice_Name(String office_Name) {
		this.office_Name = office_Name;
	}
	public String getClli() {
		return clli;
	}
	public void setClli(String clli) {
		this.clli = clli;
	}
	public String getLfa() {
		return lfa;
	}
	public void setLfa(String lfa) {
		this.lfa = lfa;
	}
	public String getCurrent_Owner() {
		return current_Owner;
	}
	public void setCurrent_Owner(String current_Owner) {
		this.current_Owner = current_Owner;
	}
	public String getNew_Owner() {
		return new_Owner;
	}
	public void setNew_Owner(String new_Owner) {
		this.new_Owner = new_Owner;
	}
	public String getCampaign_Seq() {
		return campaign_Seq;
	}
	public void setCampaign_Seq(String campaign_Seq) {
		this.campaign_Seq = campaign_Seq;
	}
}
