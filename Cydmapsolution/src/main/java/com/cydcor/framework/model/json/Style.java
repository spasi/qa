package com.cydcor.framework.model.json;

public class Style {
	private String strokeColor;
	private int strokeWeight;
	private double strokeOpacity;
	private String fillColor;
	private double fillOpacity;
	private Icon icon;
	private Opts opts;

	/**
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	/**
	 * @return the strokeColor
	 */
	public String getStrokeColor() {
		return strokeColor;
	}

	/**
	 * @param strokeColor
	 *            the strokeColor to set
	 */
	public void setStrokeColor(String strokeColor) {
		this.strokeColor = strokeColor;
	}

	/**
	 * @return the fillColor
	 */
	public String getFillColor() {
		return fillColor;
	}

	/**
	 * @param fillColor
	 *            the fillColor to set
	 */
	public void setFillColor(String fillColor) {
		this.fillColor = fillColor;
	}

	/**
	 * @param strokeWeight the strokeWeight to set
	 */
	public void setStrokeWeight(int strokeWeight) {
		this.strokeWeight = strokeWeight;
	}

	/**
	 * @return the strokeWeight
	 */
	public int getStrokeWeight() {
		return strokeWeight;
	}

	/**
	 * @return the strokeOpacity
	 */
	public double getStrokeOpacity() {
		return strokeOpacity;
	}

	/**
	 * @param strokeOpacity the strokeOpacity to set
	 */
	public void setStrokeOpacity(double strokeOpacity) {
		this.strokeOpacity = strokeOpacity;
	}

	/**
	 * @param fillOpacity the fillOpacity to set
	 */
	public void setFillOpacity(double fillOpacity) {
		this.fillOpacity = fillOpacity;
	}

	/**
	 * @return the fillOpacity
	 */
	public double getFillOpacity() {
		return fillOpacity;
	}

	/**
	 * @return the opts
	 */
	public Opts getOpts() {
		return opts;
	}

	/**
	 * @param opts the opts to set
	 */
	public void setOpts(Opts opts) {
		this.opts = opts;
	}
}
