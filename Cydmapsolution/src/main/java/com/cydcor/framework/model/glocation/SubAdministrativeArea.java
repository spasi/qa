package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("SubAdministrativeArea")
public class SubAdministrativeArea {
	private String SubAdministrativeAreaName;
	private Locality Locality;
	private Thoroughfare Thoroughfare;
	private PostalCode PostalCode;
	private DependentLocality DependentLocality;
	
	/**
	 * @return the thoroughfare
	 */
	public Thoroughfare getThoroughfare() {
		return Thoroughfare;
	}
	
	/**
	 * @param thoroughfare
	 *            the thoroughfare to set
	 */
	public void setThoroughfare(Thoroughfare thoroughfare) {
		Thoroughfare = thoroughfare;
	}
	
	/**
	 * @return the subAdministrativeAreaName
	 */
	public String getSubAdministrativeAreaName() {
		return SubAdministrativeAreaName;
	}
	
	/**
	 * @param subAdministrativeAreaName
	 *            the subAdministrativeAreaName to set
	 */
	public void setSubAdministrativeAreaName(String subAdministrativeAreaName) {
		this.SubAdministrativeAreaName = subAdministrativeAreaName;
	}
	
	/**
	 * @return the locality
	 */
	public Locality getLocality() {
		return Locality;
	}
	
	/**
	 * @param locality
	 *            the locality to set
	 */
	public void setLocality(Locality locality) {
		this.Locality = locality;
	}
	
	public PostalCode getPostalCode() {
		return PostalCode;
	}
	
	public void setPostalCode(PostalCode postalCode) {
		PostalCode = postalCode;
	}
	
	public DependentLocality getDependentLocality() {
		return DependentLocality;
	}
	
	public void setDependentLocality(DependentLocality dependentLocality) {
		DependentLocality = dependentLocality;
	}
	
	@Override
	public String toString() {
		return "SubAdministrativeArea [Locality=" + Locality + ", SubAdministrativeAreaName="
				+ SubAdministrativeAreaName + ",Thoroughfare=" + Thoroughfare + "]";
	}
	
}
