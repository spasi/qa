package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("Placemark")
public class Placemark {
	@XStreamAsAttribute
	private String id;
	private String address;
	private AddressDetails AddressDetails;
	private ExtendedData ExtendedData;
	private Point Point;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the addressDetails
	 */
	public AddressDetails getAddressDetails() {
		return AddressDetails;
	}

	/**
	 * @param addressDetails
	 *            the addressDetails to set
	 */
	public void setAddressDetails(AddressDetails addressDetails) {
		this.AddressDetails = addressDetails;
	}


	/**
	 * @return the point
	 */
	public Point getPoint() {
		return Point;
	}

	/**
	 * @param point
	 *            the point to set
	 */
	public void setPoint(Point point) {
		this.Point = point;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return the extendedData
	 */
	public ExtendedData getExtendedData() {
		return ExtendedData;
	}

	/**
	 * @param extendedData the extendedData to set
	 */
	public void setExtendedData(ExtendedData extendedData) {
		ExtendedData = extendedData;
	}

	@Override
	public String toString() {
		return "Placemark [AddressDetails=" + AddressDetails
				+ ", ExtendedData=" + ExtendedData + ", Point=" + Point
				+ ", address=" + address + ", id=" + id + "]";
	}



}
