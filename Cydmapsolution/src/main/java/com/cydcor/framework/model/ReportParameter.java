/**
 * 
 */
package com.cydcor.framework.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashwin
 * 
 */
public class ReportParameter extends BaseDataObject {

	private Long id = 0L;
	private String paramName = "";
	private String paramLabel = "";
	private String paramType = "";
	private int maxLength = 20;
	private boolean isEditable = true;
	private String defaultValue = "";
	private String refTableName = "";
	private String refKeyColumn = "";
	private String refValueColumn = "";
	private boolean isHidden = false;
	private String paramDataURL = "";
	private String paramDependents = "";
	private boolean isActive = true;
	private String reportName = "";

	public static List<String> defaultParamaters = new ArrayList<String>();

	static {
		defaultParamaters.add("offset");
		defaultParamaters.add("pageSize");
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the paramName
	 */
	public String getParamName() {
		return paramName;
	}

	/**
	 * @param paramName
	 *            the paramName to set
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	/**
	 * @return the paramLabel
	 */
	public String getParamLabel() {
		return paramLabel;
	}

	/**
	 * @param paramLabel
	 *            the paramLabel to set
	 */
	public void setParamLabel(String paramLabel) {
		this.paramLabel = paramLabel;
	}

	/**
	 * @return the paramType
	 */
	public String getParamType() {
		return paramType;
	}

	/**
	 * @param paramType
	 *            the paramType to set
	 */
	public void setParamType(String paramType) {
		this.paramType = paramType;
	}

	/**
	 * @return the maxLength
	 */
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * @param maxLength
	 *            the maxLength to set
	 */
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * @return the isEditable
	 */
	public boolean isEditable() {
		return isEditable;
	}

	/**
	 * @param isEditable
	 *            the isEditable to set
	 */
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defaultValue
	 *            the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return the refTableName
	 */
	public String getRefTableName() {
		return refTableName;
	}

	/**
	 * @param refTableName
	 *            the refTableName to set
	 */
	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

	/**
	 * @return the refKeyColumn
	 */
	public String getRefKeyColumn() {
		return refKeyColumn;
	}

	/**
	 * @param refKeyColumn
	 *            the refKeyColumn to set
	 */
	public void setRefKeyColumn(String refKeyColumn) {
		this.refKeyColumn = refKeyColumn;
	}

	/**
	 * @return the refValueColumn
	 */
	public String getRefValueColumn() {
		return refValueColumn;
	}

	/**
	 * @param refValueColumn
	 *            the refValueColumn to set
	 */
	public void setRefValueColumn(String refValueColumn) {
		this.refValueColumn = refValueColumn;
	}

	/**
	 * @return the isHidden
	 */
	public boolean isHidden() {
		return isHidden;
	}

	/**
	 * @param isHidden
	 *            the isHidden to set
	 */
	public void setHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}

	/**
	 * @return the pramDataURL
	 */
	public String getParamDataURL() {
		return paramDataURL;
	}

	/**
	 * @param pramDataURL
	 *            the pramDataURL to set
	 */
	public void setParamDataURL(String pramDataURL) {
		this.paramDataURL = pramDataURL;
	}

	/**
	 * @return the paramDependents
	 */
	public String getParamDependents() {
		return paramDependents;
	}

	/**
	 * @param paramDependents
	 *            the paramDependents to set
	 */
	public void setParamDependents(String paramDependents) {
		this.paramDependents = paramDependents;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the reportName
	 */
	public String getReportName() {
		return reportName;
	}

	/**
	 * @param reportName
	 *            the reportName to set
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

}
