package com.cydcor.framework.model.xml.b2breport;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("address_component")
public class AddressComponent {
	
	@XStreamAlias("long_name")
	private String long_name;
	@XStreamAlias("short_name")
	private String short_name;
	
	@XStreamImplicit(itemFieldName="type")
	private List<String> type;
	
	public String getLong_name() {
		return long_name;
	}
	public void setLong_name(String long_name) {
		this.long_name = long_name;
	}
	public String getShort_name() {
		return short_name;
	}
	public void setShort_name(String short_name) {
		this.short_name = short_name;
	}
	public List<String> getType() {
		return type;
	}
	public void setType(List<String> type) {
		this.type = type;
	}
}
