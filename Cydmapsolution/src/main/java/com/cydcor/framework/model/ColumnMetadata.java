/**
 * 
 */
package com.cydcor.framework.model;


/**
 * This Object Stores Metadata Information of columns in tables. Like Not
 * Null,Column Length, ColumnType
 * 
 * @author ashwin
 * 
 */
public class ColumnMetadata {
	String catalogName = "";
	String columnClassName = "";
	int columnDisplaySize = 0;
	String columnLabel = "";
	String columnName = "";
	int columnType = 1;
	String columnTypeName = "";
	int precision = 0;
	int scale;
	String schemaName = "";
	String tableName = "";
	boolean isAutoIncrement = false;
	boolean isCurrency = false;
	boolean isNullable = false;
	boolean isSigned = false;

	/**
	 * @param catalogName
	 *            the catalogName to set
	 */
	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}

	/**
	 * @param columnClassName
	 *            the columnClassName to set
	 */
	public void setColumnClassName(String columnClassName) {
		this.columnClassName = columnClassName;
	}

	/**
	 * @param columnDisplaySize
	 *            the columnDisplaySize to set
	 */
	public void setColumnDisplaySize(int columnDisplaySize) {
		this.columnDisplaySize = columnDisplaySize;
	}

	/**
	 * @param columnLabel
	 *            the columnLabel to set
	 */
	public void setColumnLabel(String columnLabel) {
		this.columnLabel = columnLabel;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * @param columnType
	 *            the columnType to set
	 */
	public void setColumnType(int columnType) {
		this.columnType = columnType;
	}

	/**
	 * @param columnTypeName
	 *            the columnTypeName to set
	 */
	public void setColumnTypeName(String columnTypeName) {
		this.columnTypeName = columnTypeName;
	}

	/**
	 * @param precision
	 *            the precision to set
	 */
	public void setPrecision(int precision) {
		this.precision = precision;
	}

	/**
	 * @param scale
	 *            the scale to set
	 */
	public void setScale(int scale) {
		this.scale = scale;
	}

	/**
	 * @param schemaName
	 *            the schemaName to set
	 */
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	/**
	 * @param tableName
	 *            the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * @param isAutoIncrement
	 *            the isAutoIncrement to set
	 */
	public void setAutoIncrement(boolean isAutoIncrement) {
		this.isAutoIncrement = isAutoIncrement;
	}

	/**
	 * @param isCurrency
	 *            the isCurrency to set
	 */
	public void setCurrency(boolean isCurrency) {
		this.isCurrency = isCurrency;
	}

	/**
	 * @param isNullable
	 *            the isNullable to set
	 */
	public void setNullable(boolean isNullable) {
		this.isNullable = isNullable;
	}

	/**
	 * @param isSigned
	 *            the isSigned to set
	 */
	public void setSigned(boolean isSigned) {
		this.isSigned = isSigned;
	}

	/**
	 * @return
	 */
	public String getCatalogName() {
		// TODO Auto-generated method stub
		return catalogName;
	}

	public String getColumnClassName() {
		// TODO Auto-generated method stub
		return columnClassName;
	}

	public int getColumnDisplaySize() {
		// TODO Auto-generated method stub
		return columnDisplaySize;
	}

	public String getColumnLabel() {
		// TODO Auto-generated method stub
		return columnLabel;
	}

	public String getColumnName() {
		// TODO Auto-generated method stub
		return columnName;
	}

	public int getColumnType() {
		// TODO Auto-generated method stub
		return columnType;
	}

	public String getColumnTypeName() {
		// TODO Auto-generated method stub
		return columnTypeName;
	}

	public int getPrecision() {
		// TODO Auto-generated method stub
		return precision;
	}

	public int getScale() {
		// TODO Auto-generated method stub
		return scale;
	}

	public String getSchemaName() {
		// TODO Auto-generated method stub
		return schemaName;
	}

	public String getTableName() {
		// TODO Auto-generated method stub
		return tableName;
	}

	public boolean isAutoIncrement() {
		// TODO Auto-generated method stub
		return isAutoIncrement;
	}

	public boolean isCurrency() {
		// TODO Auto-generated method stub
		return isCurrency;
	}

	public boolean isNullable() {
		// TODO Auto-generated method stub
		return isNullable;
	}

	public boolean isSigned() {
		// TODO Auto-generated method stub
		return isSigned;
	}

}
