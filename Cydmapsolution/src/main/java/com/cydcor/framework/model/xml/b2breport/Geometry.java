package com.cydcor.framework.model.xml.b2breport;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("geometry")
public class Geometry {
	
	private Location location;
	private String location_type;
	private Bounds bounds;
	private ViewPort viewport;
	
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String getLocation_type() {
		return location_type;
	}
	public void setLocation_type(String location_type) {
		this.location_type = location_type;
	}
	public Bounds getBounds() {
		return bounds;
	}
	public void setBounds(Bounds bounds) {
		this.bounds = bounds;
	}
	public ViewPort getViewPort() {
		return viewport;
	}
	public void setViewPort(ViewPort viewPort) {
		this.viewport = viewPort;
	}
}
