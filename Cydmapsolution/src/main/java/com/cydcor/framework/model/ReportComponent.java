/**
 * 
 */
package com.cydcor.framework.model;

import java.util.ArrayList;
import java.util.List;

import com.cydcor.framework.ui.model.RicoTable;

/**
 * All reports Extend Existing TableComponent in addition to Report Parameters
 * 
 * @author ashwin
 * 
 */
public class ReportComponent extends DBEntityComponent {
	private String reportName = "";
	private String reportDescription = "";
	private boolean isExportEnabled = true;

	private String supportedFormats = "";
	private String styleSheetName = "";
	private String templateName = "";

	private List<ReportColumn> columns = new ArrayList<ReportColumn>();
	private List<ReportParameter> parameters = new ArrayList<ReportParameter>();

	private String customFilterForm = "";
	private RicoTable table = new RicoTable();

	private List<MapObject> requestFilters = new ArrayList<MapObject>();

	/**
	 * @return the columns
	 */
	public List<ReportColumn> getColumns() {
		return columns;
	}

	/**
	 * @param columns
	 *            the columns to set
	 */
	public void setColumns(List<ReportColumn> columns) {
		this.columns = columns;
	}

	/**
	 * @param column
	 */
	public void addColumn(ReportColumn column) {
		if (!this.columns.contains(column)) {
			this.columns.add(column);
		}
	}

	/**
	 * @return the parameters
	 */
	public List<ReportParameter> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters
	 *            the parameters to set
	 */
	public void setParameters(List<ReportParameter> parameters) {
		this.parameters = parameters;
	}

	/**
	 * @param parameter
	 */
	public void addParameter(ReportParameter parameter) {
		if (!this.parameters.contains(parameter)) {
			this.parameters.add(parameter);
		}
	}

	/**
	 * @return the customFilterForm
	 */
	public String getCustomFilterForm() {
		return customFilterForm;
	}

	/**
	 * @param customFilterForm
	 *            the customFilterForm to set
	 */
	public void setCustomFilterForm(String customFilterForm) {
		this.customFilterForm = customFilterForm;
	}

	/**
	 * @return the reportName
	 */
	public String getReportName() {
		return reportName;
	}

	/**
	 * @param reportName
	 *            the reportName to set
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * @return the reportDescription
	 */
	public String getReportDescription() {
		return reportDescription;
	}

	/**
	 * @param reportDescription
	 *            the reportDescription to set
	 */
	public void setReportDescription(String reportDescription) {
		this.reportDescription = reportDescription;
	}

	/**
	 * @return the isExportEnabled
	 */
	public boolean isExportEnabled() {
		return isExportEnabled;
	}

	/**
	 * @param isExportEnabled
	 *            the isExportEnabled to set
	 */
	public void setExportEnabled(boolean isExportEnabled) {
		this.isExportEnabled = isExportEnabled;
	}

	/**
	 * @return the supportedFormats
	 */
	public String getSupportedFormats() {
		return supportedFormats;
	}

	/**
	 * @param supportedFormats
	 *            the supportedFormats to set
	 */
	public void setSupportedFormats(String supportedFormats) {
		this.supportedFormats = supportedFormats;
	}

	/**
	 * @return the styleSheetName
	 */
	public String getStyleSheetName() {
		return styleSheetName;
	}

	/**
	 * @param styleSheetName
	 *            the styleSheetName to set
	 */
	public void setStyleSheetName(String styleSheetName) {
		this.styleSheetName = styleSheetName;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName
	 *            the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return the table
	 */
	public RicoTable getTable() {
		return table;
	}

	/**
	 * @param table
	 *            the table to set
	 */
	public void setTable(RicoTable table) {
		this.table = table;
	}

	/**
	 * @return the requestFilters
	 */
	public List<MapObject> getRequestFilters() {
		return requestFilters;
	}

	/**
	 * @param requestFilters
	 *            the requestFilters to set
	 */
	public void setRequestFilters(List<MapObject> requestFilters) {
		this.requestFilters = requestFilters;
	}

	/**
	 * @param filter
	 */
	public void addRequestFilter(MapObject filter) {
		this.requestFilters.add(filter);
	}

}
