/**
 * 
 */
package com.cydcor.framework.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * @author ashwin
 * 
 */
public class TempLeadSheet {

	private String leadSheetId = "";
	private String leadSheetName = "";
	private Map<String, String> daForLeads = new HashMap<String, String>();
	private List<String> leadIds = new ArrayList<String>();
	private List<Long> rowIds = new ArrayList<Long>();
	private List<String> geoPoints = new ArrayList<String>();
	private String wireCenterName = null;
	private String currentValue = null;
	private String lastValue = null;
	private String campaignSeq = null;
	private String officeSeq = null;
	private String territoryKey = null;
	private List<String> isActives = new ArrayList<String>();
	private String coordinates;	
	private String territoryType;

	/**
	 * @return the leadSheetId
	 */
	public String getLeadSheetId() {
		return leadSheetId.replaceAll(" ", "_");
	}

	/**
	 * @param leadSheetId
	 *            the leadSheetId to set
	 */
	public void setLeadSheetId(String leadSheetId) {
		this.leadSheetId = leadSheetId;
	}

	/**
	 * @return the leadSheetName
	 */
	public String getLeadSheetName() {
		return leadSheetName;
	}

	/**
	 * @param leadSheetName
	 *            the leadSheetName to set
	 */
	public void setLeadSheetName(String leadSheetName) {
		this.leadSheetName = leadSheetName;
	}

	/**
	 * @return the daForLeads
	 */
	public Map<String, String> getDaForLeads() {
		return daForLeads;
	}

	/**
	 * @param daForLeads
	 *            the daForLeads to set
	 */
	public void setDaForLeads(Map<String, String> daForLeads) {
		this.daForLeads = daForLeads;
	}

	/**
	 * @return the leadIds
	 */
	public List<String> getLeadIds() {
		return leadIds;
	}

	/**
	 * @param leadIds
	 *            the leadIds to set
	 */
	public void setLeadIds(List<String> leadIds) {
		this.leadIds = leadIds;
	}

	public String getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(String currentValue) {
		this.currentValue = currentValue;
	}

	public String getLastValue() {
		return lastValue;
	}

	public void setLastValue(String lastValue) {
		this.lastValue = lastValue;
	}

	public List<String> getGeoPoints() {
		return geoPoints;
	}

	public void setGeoPoints(List<String> geoPoints) {
		this.geoPoints = geoPoints;
	}

	public List<Long> getRowIds() {
		return rowIds;
	}

	public void setRowIds(List<Long> rowIds) {
		this.rowIds = rowIds;
	}

	public String getOfficeSeq() {
		return officeSeq;
	}

	public void setOfficeSeq(String officeSeq) {
		this.officeSeq = officeSeq;
	}

	public String getCampaignSeq() {
		return campaignSeq;
	}

	public void setCampaignSeq(String campaignSeq) {
		this.campaignSeq = campaignSeq;
	}

	/**
	 * @return the wireCenterName
	 */
	public String getWireCenterName() {
		return wireCenterName;
	}

	/**
	 * @param wireCenterName
	 *            the wireCenterName to set
	 */
	public void setWireCenterName(String wireCenterName) {
		this.wireCenterName = wireCenterName;
	}

	public List<String> getIsActives() {
		return isActives;
	}

	public void setIsActives(List<String> isActives) {
		this.isActives = isActives;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((leadSheetId == null) ? 0 : leadSheetId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TempLeadSheet))
			return false;
		TempLeadSheet other = (TempLeadSheet) obj;
		if (leadSheetId == null) {
			if (other.leadSheetId != null)
				return false;
		} else if (!leadSheetId.equals(other.leadSheetId))
			return false;
		return true;
	}

	public String getTerritoryKey() {
		return territoryKey;
	}

	public void setTerritoryKey(String territoryKey) {
		this.territoryKey = territoryKey;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public String getCoordinatesForDB() {
		if(StringUtils.isBlank(coordinates))
			return null;
		return "POLYGON((" + coordinates + "))";
	}

	public String getCoordinatesForKML() {
		String a = this.coordinates.replaceAll(
				"\\(|\\)|POLYGON |MULTI|POINT |LINESTRING ", "").replaceAll(
				", ", "\n").replaceAll(" ", ",");
		// if (type.equalsIgnoreCase("COUNTY"))
		// a = a.substring(a.indexOf("\n"), a.lastIndexOf("\n"));
		return a;

	}

	public String getTerritoryType() {
		return territoryType;
	}

	public void setTerritoryType(String territoryType) {
		this.territoryType = territoryType;
	}
	
}
