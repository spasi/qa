package com.cydcor.framework.model.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("queries")
public class Queries {
	@XStreamImplicit
	private List<Query> query = new ArrayList<Query>();
	private Map<String, String> queryMap;

	/**
	 * @return the query
	 */
	public List<Query> getQuery() {
		return query;
	}

	/**
	 * @param query
	 *            the query to set
	 */
	public void setQuery(List<Query> query) {
		this.query = query;
	}

	/**
	 * @param queryMap
	 *            the queryMap to set
	 */
	public void setQueryMap(Map<String, String> queryMap) {
		this.queryMap = queryMap;
	}

}
