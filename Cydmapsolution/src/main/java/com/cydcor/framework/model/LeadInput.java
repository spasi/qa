package com.cydcor.framework.model;

import java.sql.Timestamp;

public class LeadInput extends GenericModel{
	
	private long rowId;
	private long campaignSeq;
	private String region;
	private String market;
	private String inputLayout;
	private String leadId;
	private String clli;
	private String da;
	private String leadType;
	private String custName;
	private String fullAddr;
	private String houseNum;
	private String strPre;
	private String strName;
	private String strType;
	private String strPost;
	private String aptNum;
	private String city;
	private String state;
	private String zip;
	private String zip4;
	private String greenStatus;
	private String creditClass;
	private String connTech;
	private String aptInd;
	private String uverseInd;
	private String dslInd;
	private String dishInd;
	private Timestamp leadDOB;
	private Timestamp leadAssigned;
	private Timestamp leadExpires;
	private String disposition;
	private String notes;
	private String leadStatus;
	private String uploadFileName;
	private String processed;
	private Timestamp processedDate;
	private String leadOwner;

	/**
	 * @return the rowId
	 */
	public long getRowId() {
		return rowId;
	}

	/**
	 * @param rowId
	 *            the rowId to set
	 */
	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	/**
	 * @return the campaignSeq
	 */
	public long getCampaignSeq() {
		return campaignSeq;
	}

	/**
	 * @param campaignSeq
	 *            the campaignSeq to set
	 */
	public void setCampaignSeq(long campaignSeq) {
		this.campaignSeq = campaignSeq;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the market
	 */
	public String getMarket() {
		return market;
	}

	/**
	 * @param market
	 *            the market to set
	 */
	public void setMarket(String market) {
		this.market = market;
	}

	/**
	 * @return the inputLayout
	 */
	public String getInputLayout() {
		return inputLayout;
	}

	/**
	 * @param inputLayout
	 *            the inputLayout to set
	 */
	public void setInputLayout(String inputLayout) {
		this.inputLayout = inputLayout;
	}

	/**
	 * @return the leadId
	 */
	public String getLeadId() {
		return leadId;
	}

	/**
	 * @param leadId
	 *            the leadId to set
	 */
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	/**
	 * @return the clli
	 */
	public String getClli() {
		return clli;
	}

	/**
	 * @param clli
	 *            the clli to set
	 */
	public void setClli(String clli) {
		this.clli = clli;
	}

	/**
	 * @return the da
	 */
	public String getDa() {
		return da;
	}

	/**
	 * @param da
	 *            the da to set
	 */
	public void setDa(String da) {
		this.da = da;
	}

	/**
	 * @return the leadType
	 */
	public String getLeadType() {
		return leadType;
	}

	/**
	 * @param leadType
	 *            the leadType to set
	 */
	public void setLeadType(String leadType) {
		this.leadType = leadType;
	}

	/**
	 * @return the custName
	 */
	public String getCustName() {
		return custName;
	}

	/**
	 * @param custName the custName to set
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}

	/**
	 * @return the fullAddr
	 */
	public String getFullAddr() {
		return fullAddr;
	}

	/**
	 * @param fullAddr
	 *            the fullAddr to set
	 */
	public void setFullAddr(String fullAddr) {
		this.fullAddr = fullAddr;
	}

	/**
	 * @return the houseNum
	 */
	public String getHouseNum() {
		return houseNum;
	}

	/**
	 * @param houseNum
	 *            the houseNum to set
	 */
	public void setHouseNum(String houseNum) {
		this.houseNum = houseNum;
	}

	/**
	 * @return the strPre
	 */
	public String getStrPre() {
		return strPre;
	}

	/**
	 * @param strPre
	 *            the strPre to set
	 */
	public void setStrPre(String strPre) {
		this.strPre = strPre;
	}

	/**
	 * @return the strName
	 */
	public String getStrName() {
		return strName;
	}

	/**
	 * @param strName
	 *            the strName to set
	 */
	public void setStrName(String strName) {
		this.strName = strName;
	}

	/**
	 * @return the strType
	 */
	public String getStrType() {
		return strType;
	}

	/**
	 * @param strType
	 *            the strType to set
	 */
	public void setStrType(String strType) {
		this.strType = strType;
	}

	/**
	 * @return the strPost
	 */
	public String getStrPost() {
		return strPost;
	}

	/**
	 * @param strPost
	 *            the strPost to set
	 */
	public void setStrPost(String strPost) {
		this.strPost = strPost;
	}

	/**
	 * @return the aptNum
	 */
	public String getAptNum() {
		return aptNum;
	}

	/**
	 * @param aptNum
	 *            the aptNum to set
	 */
	public void setAptNum(String aptNum) {
		this.aptNum = aptNum;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the zip4
	 */
	public String getZip4() {
		return zip4;
	}

	/**
	 * @param zip4
	 *            the zip4 to set
	 */
	public void setZip4(String zip4) {
		this.zip4 = zip4;
	}

	/**
	 * @return the greenStatus
	 */
	public String getGreenStatus() {
		return greenStatus;
	}

	/**
	 * @param greenStatus
	 *            the greenStatus to set
	 */
	public void setGreenStatus(String greenStatus) {
		this.greenStatus = greenStatus;
	}

	/**
	 * @return the creditClass
	 */
	public String getCreditClass() {
		return creditClass;
	}

	/**
	 * @param creditClass
	 *            the creditClass to set
	 */
	public void setCreditClass(String creditClass) {
		this.creditClass = creditClass;
	}

	/**
	 * @return the connTech
	 */
	public String getConnTech() {
		return connTech;
	}

	/**
	 * @param connTech
	 *            the connTech to set
	 */
	public void setConnTech(String connTech) {
		this.connTech = connTech;
	}

	/**
	 * @return the aptInd
	 */
	public String getAptInd() {
		return aptInd;
	}

	/**
	 * @param aptInd
	 *            the aptInd to set
	 */
	public void setAptInd(String aptInd) {
		this.aptInd = aptInd;
	}

	/**
	 * @return the uverseInd
	 */
	public String getUverseInd() {
		return uverseInd;
	}

	/**
	 * @param uverseInd
	 *            the uverseInd to set
	 */
	public void setUverseInd(String uverseInd) {
		this.uverseInd = uverseInd;
	}

	/**
	 * @return the dslInd
	 */
	public String getDslInd() {
		return dslInd;
	}

	/**
	 * @param dslInd
	 *            the dslInd to set
	 */
	public void setDslInd(String dslInd) {
		this.dslInd = dslInd;
	}

	/**
	 * @return the dishInd
	 */
	public String getDishInd() {
		return dishInd;
	}

	/**
	 * @param dishInd
	 *            the dishInd to set
	 */
	public void setDishInd(String dishInd) {
		this.dishInd = dishInd;
	}

	/**
	 * @return the leadDOB
	 */
	public Timestamp getLeadDOB() {
		return leadDOB;
	}

	/**
	 * @param leadDOB
	 *            the leadDOB to set
	 */
	public void setLeadDOB(Timestamp leadDOB) {
		this.leadDOB = leadDOB;
	}

	/**
	 * @return the leadAssigned
	 */
	public Timestamp getLeadAssigned() {
		return leadAssigned;
	}

	/**
	 * @param leadAssigned
	 *            the leadAssigned to set
	 */
	public void setLeadAssigned(Timestamp leadAssigned) {
		this.leadAssigned = leadAssigned;
	}

	/**
	 * @return the leadExpires
	 */
	public Timestamp getLeadExpires() {
		return leadExpires;
	}

	/**
	 * @param leadExpires
	 *            the leadExpires to set
	 */
	public void setLeadExpires(Timestamp leadExpires) {
		this.leadExpires = leadExpires;
	}

	/**
	 * @return the disposition
	 */
	public String getDisposition() {
		return disposition;
	}

	/**
	 * @param disposition
	 *            the disposition to set
	 */
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the leadStatus
	 */
	public String getLeadStatus() {
		return leadStatus;
	}

	/**
	 * @param leadStatus
	 *            the leadStatus to set
	 */
	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	/**
	 * @return the uploadFileName
	 */
	public String getUploadFileName() {
		return uploadFileName;
	}

	/**
	 * @param uploadFileName
	 *            the uploadFileName to set
	 */
	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}


	/**
	 * @return the processed
	 */
	public String getProcessed() {
		return processed;
	}

	/**
	 * @param processed the processed to set
	 */
	public void setProcessed(String processed) {
		this.processed = processed;
	}

	/**
	 * @return the processedDate
	 */
	public Timestamp getProcessedDate() {
		return processedDate;
	}

	/**
	 * @param processedDate
	 *            the processedDate to set
	 */
	public void setProcessedDate(Timestamp processedDate) {
		this.processedDate = processedDate;
	}

	/**
	 * @return the leadOwner
	 */
	public String getLeadOwner() {
		return leadOwner;
	}

	/**
	 * @param leadOwner
	 *            the leadOwner to set
	 */
	public void setLeadOwner(String leadOwner) {
		this.leadOwner = leadOwner;
	}

}
