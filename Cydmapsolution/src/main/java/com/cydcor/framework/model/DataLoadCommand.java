/**
 * 
 */
package com.cydcor.framework.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ashwin
 * 
 */
public class DataLoadCommand {
	private String tableName = "";
	private String orderByColumns = "";
	private String groupByColumns = "";
	private int lowerLimit = -1;
	private int upperLimit = -2;
	private int rowLimit = -1;
	private String schemaName = "";
	private Map<String, Object> filters = new HashMap<String, Object>();

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName
	 *            the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * @return the orderByColumns
	 */
	public String getOrderByColumns() {
		return orderByColumns;
	}

	/**
	 * @param orderByColumns
	 *            the orderByColumns to set
	 */
	public void setOrderByColumns(String orderByColumns) {
		this.orderByColumns = orderByColumns;
	}

	/**
	 * @return the groupByColumns
	 */
	public String getGroupByColumns() {
		return groupByColumns;
	}

	/**
	 * @param groupByColumns
	 *            the groupByColumns to set
	 */
	public void setGroupByColumns(String groupByColumns) {
		this.groupByColumns = groupByColumns;
	}

	/**
	 * @return the lowerLimit
	 */
	public int getLowerLimit() {
		return lowerLimit;
	}

	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(int lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @return the upperLimit
	 */
	public int getUpperLimit() {
		return upperLimit;
	}

	/**
	 * @param upperLimit
	 *            the upperLimit to set
	 */
	public void setUpperLimit(int upperLimit) {
		this.upperLimit = upperLimit;
	}

	/**
	 * @return the rowLimit
	 */
	public int getRowLimit() {
		return rowLimit;
	}

	/**
	 * @param rowLimit
	 *            the rowLimit to set
	 */
	public void setRowLimit(int rowLimit) {
		this.rowLimit = rowLimit;
	}

	/**
	 * @return the schemaName
	 */
	public String getSchemaName() {
		return schemaName;
	}

	/**
	 * @param schemaName
	 *            the schemaName to set
	 */
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	/**
	 * @return the filters
	 */
	public Map<String, Object> getFilters() {
		return filters;
	}

	/**
	 * @param filters
	 *            the filters to set
	 */
	public void setFilters(Map<String, Object> filters) {
		this.filters = filters;
	}

}
