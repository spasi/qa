package com.cydcor.framework.model.xml.b2breport;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("result")
public class Result {
	
	@XStreamImplicit(itemFieldName="type")
	private List<String> type;
	private String formatted_address;
	@XStreamImplicit(itemFieldName="address_component")
	private List<AddressComponent> address_component;
	private Geometry geometry;
	@XStreamImplicit(itemFieldName="postcode_locality")
	private List<String> postCodeLocality;
	private String place_id;
	
	public List<String> getType() {
		return type;
	}
	public void setType(List<String> type) {
		this.type = type;
	}
	public String getFormatted_address() {
		return formatted_address;
	}
	public void setFormatted_address(String formatted_address) {
		this.formatted_address = formatted_address;
	}
	public List<AddressComponent> getAddress_component() {
		return address_component;
	}
	public void setAddress_component(List<AddressComponent> address_component) {
		this.address_component = address_component;
	}
	public Geometry getGeometry() {
		return geometry;
	}
	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}
	public List<String> getPostCodeLocality() {
		return postCodeLocality;
	}
	public void setPostCodeLocality(List<String> postCodeLocality) {
		this.postCodeLocality = postCodeLocality;
	}
	public String getPlace_id() {
		return place_id;
	}
	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}
}
