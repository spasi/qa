package com.cydcor.framework.model;

import java.util.Date;

public class B2BReport extends GenericModel {
	
	private String office_seq;
	private String currentRep;
	private String closedDate;
	private String office_code;
	private String history;
	private String dateToBeWorked;
	private String zip;
	private String clli;
	private String town;
	private String notes;
	private String distance;
	private Integer total;
	private String sharedBy;
	private Integer numOfSharedLeads;
	private Integer fiosData;
	private Integer fiosMigration;
	private Integer fiosTV;
	private Integer winback;
	private Integer prospect;
	private Integer hsi;
	private Integer totalMaxSpeedDnLoadBelow3;
	private Integer totalMaxSpeedDnLoadAbove3;
	private Integer sortSeq;
	private String createdOn;
	private Integer rowId;
	private Integer voiceOnly;
	private String territoryType;
	private String territoryName;	
	private String city;
	private String da;    
    private Integer diffInCount;
	
	public Integer getVoiceOnly() {
		return voiceOnly;
	}
	public void setVoiceOnly(Integer voiceOnly) {
		this.voiceOnly = voiceOnly;
	}
	public String getOffice_seq() {
		return office_seq;
	}
	public void setOffice_seq(String office_seq) {
		this.office_seq = office_seq;
	}
	public String getCurrentRep() {
		return currentRep;
	}
	public void setCurrentRep(String currentRep) {
		this.currentRep = currentRep;
	}
	public String getOffice_code() {
		return office_code;
	}
	public void setOffice_code(String office_code) {
		this.office_code = office_code;
	}
	public String getHistory() {
		return history;
	}
	public void setHistory(String history) {
		this.history = history;
	}
	public String getDateToBeWorked() {
		return dateToBeWorked;
	}
	public void setDateToBeWorked(String dateToBeWorked) {
		this.dateToBeWorked = dateToBeWorked;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getClli() {
		return clli;
	}
	public void setClli(String clli) {
		if(null != clli){
			this.clli = clli;
		}else{
			this.clli = "";
		}
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getSharedBy() {
		return sharedBy;
	}
	public void setSharedBy(String sharedBy) {
		this.sharedBy = sharedBy;
	}
	public Integer getNumOfSharedLeads() {
		return numOfSharedLeads;
	}
	public void setNumOfSharedLeads(Integer numOfSharedLeads) {
		this.numOfSharedLeads = numOfSharedLeads;
	}
	public Integer getFiosData() {
		return fiosData;
	}
	public void setFiosData(Integer fiosData) {
		this.fiosData = fiosData;
	}
	public Integer getFiosMigration() {
		return fiosMigration;
	}
	public void setFiosMigration(Integer fiosMigration) {
		this.fiosMigration = fiosMigration;
	}
	public Integer getFiosTV() {
		return fiosTV;
	}
	public void setFiosTV(Integer fiosTV) {
		this.fiosTV = fiosTV;
	}
	public Integer getWinback() {
		return winback;
	}
	public void setWinback(Integer winback) {
		this.winback = winback;
	}
	public Integer getProspect() {
		return prospect;
	}
	public void setProspect(Integer prospect) {
		this.prospect = prospect;
	}
	public Integer getHsi() {
		return hsi;
	}
	public void setHsi(Integer hsi) {
		this.hsi = hsi;
	}
	public Integer getTotalMaxSpeedDnLoadBelow3() {
		return totalMaxSpeedDnLoadBelow3;
	}
	public void setTotalMaxSpeedDnLoadBelow3(Integer totalMaxSpeedDnLoadBelow3) {
		this.totalMaxSpeedDnLoadBelow3 = totalMaxSpeedDnLoadBelow3;
	}
	public Integer getTotalMaxSpeedDnLoadAbove3() {
		return totalMaxSpeedDnLoadAbove3;
	}
	public void setTotalMaxSpeedDnLoadAbove3(Integer totalMaxSpeedDnLoadAbove3) {
		this.totalMaxSpeedDnLoadAbove3 = totalMaxSpeedDnLoadAbove3;
	}
	public Integer getSortSeq() {
		return sortSeq;
	}
	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public Integer getRowId() {
		return rowId;
	}
	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}
	public String getClosedDate() {
		return closedDate;
	}
	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}
	public String getTerritoryType(String territoryType) {
		return this.territoryType;		
	}
	public void setTerritoryType(String territoryType) {
		this.territoryType = territoryType;		
	}
	public String getTerritoryName() {
		return territoryName;
	}
	public void setTerritoryName(String territoryName) {
		this.territoryName = territoryName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDa() {
		return da;
	}
	public void setDa(String da) {
		this.da = da;
	}
	public Integer getDiffInCount() {
		return diffInCount;
	}
	public void setDiffInCount(Integer diffInCount) {
		this.diffInCount = diffInCount;
	}	
		

}
