package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("AddressDetails")
public class AddressDetails {
	@XStreamAsAttribute
	@XStreamAlias("Accuracy")
	private String accuracy;
	@XStreamAsAttribute
	private String xmlns;
	private String AddressLine;
	private Country Country;
	private Thoroughfare Thoroughfare;
	private Locality Locality;
	
	
	public Locality getLocality() {
		return Locality;
	}

	public void setLocality(Locality locality) {
		Locality = locality;
	}

	/**
	 * @return the thoroughfare
	 */
	public Thoroughfare getThoroughfare() {
		return Thoroughfare;
	}
	
	/**
	 * @param thoroughfare
	 *            the thoroughfare to set
	 */
	public void setThoroughfare(Thoroughfare thoroughfare) {
		Thoroughfare = thoroughfare;
	}
	
	/**
	 * @return the accuracy
	 */
	public String getAccuracy() {
		return accuracy;
	}
	
	/**
	 * @param accuracy
	 *            the accuracy to set
	 */
	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}
	
	/**
	 * @return the country
	 */
	public Country getCountry() {
		return Country;
	}
	
	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(Country country) {
		this.Country = country;
	}
	
	/**
	 * @return the xmlns
	 */
	public String getXmlns() {
		return xmlns;
	}
	
	/**
	 * @param xmlns
	 *            the xmlns to set
	 */
	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}
	
	public String getAddressLine() {
		return AddressLine;
	}
	
	public void setAddressLine(String addressLine) {
		AddressLine = addressLine;
	}
	
	@Override
	public String toString() {
		return "AddressDetails [Country=" + Country + ", accuracy=" + accuracy + ", xmlns=" + xmlns + "]";
	}
	
}
