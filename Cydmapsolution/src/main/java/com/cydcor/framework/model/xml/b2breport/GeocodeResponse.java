package com.cydcor.framework.model.xml.b2breport;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("GeocodeResponse")
public class GeocodeResponse {
	
	private String status;
	@XStreamImplicit(itemFieldName="result")
    private List<Result> result;
	
    public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Result> getResult() {
		return result;
	}
	public void setResult(List<Result> result) {
		this.result = result;
	}
}
