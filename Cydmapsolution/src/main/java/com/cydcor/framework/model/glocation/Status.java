package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Status")
public class Status {
	private String code;
	private String request;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	public Status() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}

	/**
	 * @param request
	 *            the request to set
	 */
	public void setRequest(String request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "Status [code=" + code + ", request=" + request + "]";
	}

}
