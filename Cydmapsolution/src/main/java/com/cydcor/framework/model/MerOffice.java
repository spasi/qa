package com.cydcor.framework.model;

import java.util.Date;

public class MerOffice extends GenericModel{
	private int office_seq;
	private String office_code;
	private String office_state;
	private String office_city;
	private String office_address1;
	private String office_address2;
	private String office_zip;
	private String active;
	private String hub_manager;
	private String owner;
	private String promoting_manager;
	private String comments;
	private String email;
	private String phone;
	private String office_name;
	private String fax;
	private String cell;
	private String fed_ex;
	private Date start_date;
	private Date close_date;
	private String fein;
	private long hub_manager_seq;
	private String office_update;
	private String quarterback;
	private String hot_jobs;
	private String class_id;
	private String web_user;
	private Date last_update;
	private String last_update_by;
	private String office_country;
	private String insurance_exp_date;
	private String work_comp_exp_date;
	private String payroll_processor;
	private Date date_of_birth;
	private String address_change;
	private long consult_seq;
	private String consultant;
	private long closure_reason_seq;
	private String closed;
	private String office_legal_name;
	private String secondary_email;
	/**
	 * @return the office_seq
	 */
	public int getOffice_seq() {
		return office_seq;
	}
	/**
	 * @param officeSeq the office_seq to set
	 */
	public void setOffice_seq(int officeSeq) {
		office_seq = officeSeq;
	}
	/**
	 * @return the office_code
	 */
	public String getOffice_code() {
		return office_code;
	}
	/**
	 * @param officeCode the office_code to set
	 */
	public void setOffice_code(String officeCode) {
		office_code = officeCode;
	}
	/**
	 * @return the office_state
	 */
	public String getOffice_state() {
		return office_state;
	}
	/**
	 * @param officeState the office_state to set
	 */
	public void setOffice_state(String officeState) {
		office_state = officeState;
	}
	/**
	 * @return the office_city
	 */
	public String getOffice_city() {
		return office_city;
	}
	/**
	 * @param officeCity the office_city to set
	 */
	public void setOffice_city(String officeCity) {
		office_city = officeCity;
	}
	/**
	 * @return the office_address1
	 */
	public String getOffice_address1() {
		return office_address1;
	}
	/**
	 * @param officeAddress1 the office_address1 to set
	 */
	public void setOffice_address1(String officeAddress1) {
		office_address1 = officeAddress1;
	}
	/**
	 * @return the office_address2
	 */
	public String getOffice_address2() {
		return office_address2;
	}
	/**
	 * @param officeAddress2 the office_address2 to set
	 */
	public void setOffice_address2(String officeAddress2) {
		office_address2 = officeAddress2;
	}
	/**
	 * @return the office_zip
	 */
	public String getOffice_zip() {
		return office_zip;
	}
	/**
	 * @param officeZip the office_zip to set
	 */
	public void setOffice_zip(String officeZip) {
		office_zip = officeZip;
	}
	/**
	 * @return the active
	 */
	public String getActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}
	/**
	 * @return the hub_manager
	 */
	public String getHub_manager() {
		return hub_manager;
	}
	/**
	 * @param hubManager the hub_manager to set
	 */
	public void setHub_manager(String hubManager) {
		hub_manager = hubManager;
	}
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	/**
	 * @return the promoting_manager
	 */
	public String getPromoting_manager() {
		return promoting_manager;
	}
	/**
	 * @param promotingManager the promoting_manager to set
	 */
	public void setPromoting_manager(String promotingManager) {
		promoting_manager = promotingManager;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the office_name
	 */
	public String getOffice_name() {
		return office_name;
	}
	/**
	 * @param officeName the office_name to set
	 */
	public void setOffice_name(String officeName) {
		office_name = officeName;
	}
	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}
	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	/**
	 * @return the cell
	 */
	public String getCell() {
		return cell;
	}
	/**
	 * @param cell the cell to set
	 */
	public void setCell(String cell) {
		this.cell = cell;
	}
	/**
	 * @return the fed_ex
	 */
	public String getFed_ex() {
		return fed_ex;
	}
	/**
	 * @param fedEx the fed_ex to set
	 */
	public void setFed_ex(String fedEx) {
		fed_ex = fedEx;
	}
	/**
	 * @return the start_date
	 */
	public Date getStart_date() {
		return start_date;
	}
	/**
	 * @param startDate the start_date to set
	 */
	public void setStart_date(Date startDate) {
		start_date = startDate;
	}
	/**
	 * @return the close_date
	 */
	public Date getClose_date() {
		return close_date;
	}
	/**
	 * @param closeDate the close_date to set
	 */
	public void setClose_date(Date closeDate) {
		close_date = closeDate;
	}
	/**
	 * @return the fein
	 */
	public String getFein() {
		return fein;
	}
	/**
	 * @param fein the fein to set
	 */
	public void setFein(String fein) {
		this.fein = fein;
	}
	/**
	 * @return the hub_manager_seq
	 */
	public long getHub_manager_seq() {
		return hub_manager_seq;
	}
	/**
	 * @param hubManagerSeq the hub_manager_seq to set
	 */
	public void setHub_manager_seq(long hubManagerSeq) {
		hub_manager_seq = hubManagerSeq;
	}
	/**
	 * @return the office_update
	 */
	public String getOffice_update() {
		return office_update;
	}
	/**
	 * @param officeUpdate the office_update to set
	 */
	public void setOffice_update(String officeUpdate) {
		office_update = officeUpdate;
	}
	/**
	 * @return the quarterback
	 */
	public String getQuarterback() {
		return quarterback;
	}
	/**
	 * @param quarterback the quarterback to set
	 */
	public void setQuarterback(String quarterback) {
		this.quarterback = quarterback;
	}
	/**
	 * @return the hot_jobs
	 */
	public String getHot_jobs() {
		return hot_jobs;
	}
	/**
	 * @param hotJobs the hot_jobs to set
	 */
	public void setHot_jobs(String hotJobs) {
		hot_jobs = hotJobs;
	}
	/**
	 * @return the class_id
	 */
	public String getClass_id() {
		return class_id;
	}
	/**
	 * @param classId the class_id to set
	 */
	public void setClass_id(String classId) {
		class_id = classId;
	}
	/**
	 * @return the web_user
	 */
	public String getWeb_user() {
		return web_user;
	}
	/**
	 * @param webUser the web_user to set
	 */
	public void setWeb_user(String webUser) {
		web_user = webUser;
	}
	/**
	 * @return the last_update
	 */
	public Date getLast_update() {
		return last_update;
	}
	/**
	 * @param lastUpdate the last_update to set
	 */
	public void setLast_update(Date lastUpdate) {
		last_update = lastUpdate;
	}
	/**
	 * @return the last_update_by
	 */
	public String getLast_update_by() {
		return last_update_by;
	}
	/**
	 * @param lastUpdateBy the last_update_by to set
	 */
	public void setLast_update_by(String lastUpdateBy) {
		last_update_by = lastUpdateBy;
	}
	/**
	 * @return the office_country
	 */
	public String getOffice_country() {
		return office_country;
	}
	/**
	 * @param officeCountry the office_country to set
	 */
	public void setOffice_country(String officeCountry) {
		office_country = officeCountry;
	}
	/**
	 * @return the insurance_exp_date
	 */
	public String getInsurance_exp_date() {
		return insurance_exp_date;
	}
	/**
	 * @param insuranceExpDate the insurance_exp_date to set
	 */
	public void setInsurance_exp_date(String insuranceExpDate) {
		insurance_exp_date = insuranceExpDate;
	}
	/**
	 * @return the work_comp_exp_date
	 */
	public String getWork_comp_exp_date() {
		return work_comp_exp_date;
	}
	/**
	 * @param workCompExpDate the work_comp_exp_date to set
	 */
	public void setWork_comp_exp_date(String workCompExpDate) {
		work_comp_exp_date = workCompExpDate;
	}
	/**
	 * @return the payroll_processor
	 */
	public String getPayroll_processor() {
		return payroll_processor;
	}
	/**
	 * @param payrollProcessor the payroll_processor to set
	 */
	public void setPayroll_processor(String payrollProcessor) {
		payroll_processor = payrollProcessor;
	}
	/**
	 * @return the date_of_birth
	 */
	public Date getDate_of_birth() {
		return date_of_birth;
	}
	/**
	 * @param dateOfBirth the date_of_birth to set
	 */
	public void setDate_of_birth(Date dateOfBirth) {
		date_of_birth = dateOfBirth;
	}
	/**
	 * @return the address_change
	 */
	public String getAddress_change() {
		return address_change;
	}
	/**
	 * @param addressChange the address_change to set
	 */
	public void setAddress_change(String addressChange) {
		address_change = addressChange;
	}
	/**
	 * @return the consult_seq
	 */
	public long getConsult_seq() {
		return consult_seq;
	}
	/**
	 * @param consultSeq the consult_seq to set
	 */
	public void setConsult_seq(long consultSeq) {
		consult_seq = consultSeq;
	}
	/**
	 * @return the consultant
	 */
	public String getConsultant() {
		return consultant;
	}
	/**
	 * @param consultant the consultant to set
	 */
	public void setConsultant(String consultant) {
		this.consultant = consultant;
	}
	/**
	 * @return the closure_reason_seq
	 */
	public long getClosure_reason_seq() {
		return closure_reason_seq;
	}
	/**
	 * @param closureReasonSeq the closure_reason_seq to set
	 */
	public void setClosure_reason_seq(long closureReasonSeq) {
		closure_reason_seq = closureReasonSeq;
	}
	/**
	 * @return the closed
	 */
	public String getClosed() {
		return closed;
	}
	/**
	 * @param closed the closed to set
	 */
	public void setClosed(String closed) {
		this.closed = closed;
	}
	/**
	 * @return the office_legal_name
	 */
	public String getOffice_legal_name() {
		return office_legal_name;
	}
	/**
	 * @param officeLegalName the office_legal_name to set
	 */
	public void setOffice_legal_name(String officeLegalName) {
		office_legal_name = officeLegalName;
	}
	/**
	 * @return the secondary_email
	 */
	public String getSecondary_email() {
		return secondary_email;
	}
	/**
	 * @param secondaryEmail the secondary_email to set
	 */
	public void setSecondary_email(String secondaryEmail) {
		secondary_email = secondaryEmail;
	}
	
	
}