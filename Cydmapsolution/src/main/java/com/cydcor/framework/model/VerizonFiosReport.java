package com.cydcor.framework.model;

import java.util.Date;
import java.util.Map;

public class VerizonFiosReport {

	private String territoryType;
	
	private String CLLIName;
	
	private String territoryName;
	
	private String territoryNameFirst;
	
	private String territoryNameSecond;
	
	private String status;
	
	private Integer total;
	
	private Integer totalAvailable;
	
	private String dateToBeWorked;
	
	private String datelastWorked;
	
	private Integer leadsThirtyDaysAndUnder;
	
	private Integer leadsBetweenThirtyOneAndNinetyDays;
	
	private Double percentageNewlyLitThirtyDaysOrLess;
	
	private Double percentageNewlyLitBetweenThirtyOneAndNinetyDays;
	
	private Integer mduMduY;
	
	private Integer openSeasonMduY;
	
	private Integer disabledMdu;
	
	private Integer dslMigration;
	
	private Integer totalIont;
	
	private Integer iontDifferentCustomers;
	
	private Integer iontSameCustomer;
	
	private Integer disconnectDateUnderTwoYears;
	
	private Integer dataEligible;
	
	private Integer voiceAndData;
	
	private Integer voiceDataTv;
	
	private Integer existingVoiceCustomer;
	
//	private Integer existingVoiceCustomers;
	
	private String tvPenetrationRate;
	
	private Map<String, Integer> promo;
	
	private String history;
	
	private String notes;
	
	private Integer rowId;
	
	private Integer sortSeq;

	private Date dateToBeWorkedDate;
	
	private Date datelastWorkedDate;
	
	private String wcLatitude;
	
	private String wcLongitude;
	
    private String imageIcon = null;
    
    private Integer diffInCount;
    
	private String office_seq;
	
	private String zip;
	
	private String clli;
	
	private String city;
	
	private String lfa;
	
	private Double distance;
	
	// Code Added by WCN Sanjay
	private String CLLINameForNotes;
	
	public String getCLLINameForNotes() {
		return CLLINameForNotes;
	}

	public void setCLLINameForNotes(String CLLINameForNotes) {
		this.CLLINameForNotes = CLLINameForNotes;
	}
	
	
	public String getTerritoryName() {
		return territoryName;
	}

	public void setTerritoryName(String territoryName) {
		this.territoryName = territoryName;
	}

	public String getTerritoryNameFirst() {
		return territoryNameFirst;
	}

	public void setTerritoryNameFirst(String territoryNameFirst) {
		this.territoryNameFirst = territoryNameFirst;
	}

	public String getTerritoryNameSecond() {
		return territoryNameSecond;
	}

	public void setTerritoryNameSecond(String territoryNameSecond) {
		this.territoryNameSecond = territoryNameSecond;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getTotalAvailable() {
		return totalAvailable;
	}

	public void setTotalAvailable(Integer totalAvailable) {
		this.totalAvailable = totalAvailable;
	}

	public String getDateToBeWorked() {
		return dateToBeWorked;
	}

	public void setDateToBeWorked(String dateToBeWorked) {
		this.dateToBeWorked = dateToBeWorked;
	}

	public String getDatelastWorked() {
		return datelastWorked;
	}

	public void setDatelastWorked(String datelastWorked) {
		this.datelastWorked = datelastWorked;
	}

	public Integer getLeadsThirtyDaysAndUnder() {
		return leadsThirtyDaysAndUnder;
	}

	public void setLeadsThirtyDaysAndUnder(Integer leadsThirtyDaysAndUnder) {
		this.leadsThirtyDaysAndUnder = leadsThirtyDaysAndUnder;
	}

	public Integer getLeadsBetweenThirtyOneAndNinetyDays() {
		return leadsBetweenThirtyOneAndNinetyDays;
	}

	public void setLeadsBetweenThirtyOneAndNinetyDays(
			Integer leadsBetweenThirtyOneAndNinetyDays) {
		this.leadsBetweenThirtyOneAndNinetyDays = leadsBetweenThirtyOneAndNinetyDays;
	}

	public Double getPercentageNewlyLitThirtyDaysOrLess() {
		return percentageNewlyLitThirtyDaysOrLess;
	}

	public void setPercentageNewlyLitThirtyDaysOrLess(
			Double percentageNewlyLitThirtyDaysOrLess) {
		this.percentageNewlyLitThirtyDaysOrLess = percentageNewlyLitThirtyDaysOrLess;
	}

	public Double getPercentageNewlyLitBetweenThirtyOneAndNinetyDays() {
		return percentageNewlyLitBetweenThirtyOneAndNinetyDays;
	}

	public void setPercentageNewlyLitBetweenThirtyOneAndNinetyDays(
			Double percentageNewlyLitBetweenThirtyOneAndNinetyDays) {
		this.percentageNewlyLitBetweenThirtyOneAndNinetyDays = percentageNewlyLitBetweenThirtyOneAndNinetyDays;
	}

	public Integer getMduMduY() {
		return mduMduY;
	}

	public void setMduMduY(Integer mduMduY) {
		this.mduMduY = mduMduY;
	}

	public Integer getOpenSeasonMduY() {
		return openSeasonMduY;
	}

	public void setOpenSeasonMduY(Integer openSeasonMduY) {
		this.openSeasonMduY = openSeasonMduY;
	}

	public Integer getDisabledMdu() {
		return disabledMdu;
	}

	public void setDisabledMdu(Integer disabled) {
		this.disabledMdu = disabled;
	}

	public Integer getDslMigration() {
		return dslMigration;
	}

	public void setDslMigration(Integer dslMigration) {
		this.dslMigration = dslMigration;
	}

	public Integer getTotalIont() {
		return totalIont;
	}

	public void setTotalIont(Integer totalIont) {
		this.totalIont = totalIont;
	}

	public Integer getIontDifferentCustomers() {
		return iontDifferentCustomers;
	}

	public void setIontDifferentCustomers(Integer iontDifferentCustomers) {
		this.iontDifferentCustomers = iontDifferentCustomers;
	}

	public Integer getIontSameCustomer() {
		return iontSameCustomer;
	}

	public void setIontSameCustomer(Integer iontSameCustomer) {
		this.iontSameCustomer = iontSameCustomer;
	}

	public Integer getDisconnectDateUnderTwoYears() {
		return disconnectDateUnderTwoYears;
	}

	public void setDisconnectDateUnderTwoYears(Integer disconnectDateUnderTwoYears) {
		this.disconnectDateUnderTwoYears = disconnectDateUnderTwoYears;
	}

	public Integer getDataEligible() {
		return dataEligible;
	}

	public void setDataEligible(Integer dataEligible) {
		this.dataEligible = dataEligible;
	}

	public Integer getVoiceAndData() {
		return voiceAndData;
	}

	public void setVoiceAndData(Integer voiceAndData) {
		this.voiceAndData = voiceAndData;
	}

	public Integer getVoiceDataTv() {
		return voiceDataTv;
	}

	public void setVoiceDataTv(Integer voiceDataTv) {
		this.voiceDataTv = voiceDataTv;
	}

	public Integer getExistingVoiceCustomer() {
		return existingVoiceCustomer;
	}

	public void setExistingVoiceCustomer(Integer existingVoiceCustomer) {
		this.existingVoiceCustomer = existingVoiceCustomer;
	}

//	public Integer getExistingVoiceCustomers() {
//		return existingVoiceCustomers;
//	}
//
//	public void setExistingVoiceCustomers(Integer existingVoiceCustomers) {
//		this.existingVoiceCustomers = existingVoiceCustomers;
//	}

	public String getTvPenetrationRate() {
		return tvPenetrationRate;
	}

	public void setTvPenetrationRate(String tvPenetrationRate) {
		this.tvPenetrationRate = tvPenetrationRate;
	}

	public Map<String, Integer> getPromo() {
		return promo;
	}

	public void setPromo(Map<String, Integer> promo) {
		this.promo = promo;
	}

	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public Date getDateToBeWorkedDate() {
		return dateToBeWorkedDate;
	}

	public void setDateToBeWorkedDate(Date dateToBeWorkedDate) {
		this.dateToBeWorkedDate = dateToBeWorkedDate;
	}

	public Date getDatelastWorkedDate() {
		return datelastWorkedDate;
	}

	public void setDatelastWorkedDate(Date datelastWorkedDate) {
		this.datelastWorkedDate = datelastWorkedDate;
	}

	public String getWcLatitude() {
		return wcLatitude;
	}

	public void setWcLatitude(String wcLatitude) {
		this.wcLatitude = wcLatitude;
	}

	public String getWcLongitude() {
		return wcLongitude;
	}

	public void setWcLongitude(String wcLongitude) {
		this.wcLongitude = wcLongitude;
	}

	public String getImageIcon() {
		return imageIcon;
	}

	public void setImageIcon(String imageIcon) {
		this.imageIcon = imageIcon;
	}

	public Integer getDiffInCount() {
		return diffInCount;
	}

	public void setDiffInCount(Integer diffInCount) {
		this.diffInCount = diffInCount;
	}

	public String getOffice_seq() {
		return office_seq;
	}

	public void setOffice_seq(String office_seq) {
		this.office_seq = office_seq;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getClli() {
		return clli;
	}

	public void setClli(String clli) {
		this.clli = clli;
	}
	//Code added by sanjay FIOS tracker 30 Dec
	public String getCLLiName() {
		return CLLIName;
	}
	
	public void setCLLiName(String CLLIName) {
		this.CLLIName = CLLIName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLfa() {
		return lfa;
	}

	public void setLfa(String lfa) {
		this.lfa = lfa;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getTerritoryType() {
		return territoryType;
	}

	public void setTerritoryType(String territoryType) {
		this.territoryType = territoryType;
	}
	
}
