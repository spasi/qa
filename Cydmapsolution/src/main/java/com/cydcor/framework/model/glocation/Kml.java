package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("kml")
public class Kml {
@XStreamAsAttribute
	private String xmlns;
	private Response Response;
	private String reponseFromGoogle;
	/**
	 * @return the xmlns
	 */
	public String getXmlns() {
		return xmlns;
	}

	/**
	 * @param xmlns
	 *            the xmlns to set
	 */
	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	/**
	 * @return the response
	 */
	public Response getResponse() {
		return Response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(Response response) {
		this.Response = response;
	}

	
	/**
	 * @return the reponseFromGoogle
	 */
	public String getReponseFromGoogle() {
		return reponseFromGoogle;
	}

	/**
	 * @param reponseFromGoogle the reponseFromGoogle to set
	 */
	public void setReponseFromGoogle(String reponseFromGoogle) {
		this.reponseFromGoogle = reponseFromGoogle;
	}

	@Override
	public String toString() {
		return "Kml [Response=" + Response + ", xmlns=" + xmlns + "]";
	}

}
