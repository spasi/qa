/**
 * 
 */
package com.cydcor.framework.model;

/**
 * @author ashwin
 * 
 */
public class UserObject extends BaseDataObject {
	private Long id = 0L;
	private String userName = "";
	private String userEmail = "";
	private String defaultDateFormat = "";
	private String defaultNumberFormat = "";

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * @param userEmail
	 *            the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/**
	 * @return the defaultDateFormat
	 */
	public String getDefaultDateFormat() {
		return defaultDateFormat;
	}

	/**
	 * @param defaultDateFormat
	 *            the defaultDateFormat to set
	 */
	public void setDefaultDateFormat(String defaultDateFormat) {
		this.defaultDateFormat = defaultDateFormat;
	}

	/**
	 * @return the defaultNumberFormat
	 */
	public String getDefaultNumberFormat() {
		return defaultNumberFormat;
	}

	/**
	 * @param defaultNumberFormat
	 *            the defaultNumberFormat to set
	 */
	public void setDefaultNumberFormat(String defaultNumberFormat) {
		this.defaultNumberFormat = defaultNumberFormat;
	}

}
