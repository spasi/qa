package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("LatLonBox")
public class LatLonBox {
	@XStreamAsAttribute
	private String north;
	@XStreamAsAttribute
	private String south;
	@XStreamAsAttribute
	private String east;
	@XStreamAsAttribute
	private String west;

	/**
	 * @return the north
	 */
	public String getNorth() {
		return north;
	}

	/**
	 * @param north
	 *            the north to set
	 */
	public void setNorth(String north) {
		this.north = north;
	}

	/**
	 * @return the south
	 */
	public String getSouth() {
		return south;
	}

	/**
	 * @param south
	 *            the south to set
	 */
	public void setSouth(String south) {
		this.south = south;
	}

	/**
	 * @return the east
	 */
	public String getEast() {
		return east;
	}

	/**
	 * @param east
	 *            the east to set
	 */
	public void setEast(String east) {
		this.east = east;
	}

	/**
	 * @return the west
	 */
	public String getWest() {
		return west;
	}

	/**
	 * @param west
	 *            the west to set
	 */
	public void setWest(String west) {
		this.west = west;
	}

	@Override
	public String toString() {
		return "LatLonBox [east=" + east + ", north=" + north + ", south="
				+ south + ", west=" + west + "]";
	}
	
}
