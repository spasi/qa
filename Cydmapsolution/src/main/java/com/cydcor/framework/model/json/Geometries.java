package com.cydcor.framework.model.json;

import java.util.ArrayList;
import java.util.List;

public class Geometries {

	private List<Type> records = new ArrayList<Type>();

	/**
	 * @return the records
	 */
	public List<Type> getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(List<Type> records) {
		this.records = records;
	}
}
