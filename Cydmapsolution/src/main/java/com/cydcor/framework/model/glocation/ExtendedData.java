package com.cydcor.framework.model.glocation;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("ExtendedData")
public class ExtendedData {
	@XStreamImplicit
	private List<LatLonBox> LatLonBox=new ArrayList<LatLonBox>();

	/**
	 * @return the latLonBox
	 */
	public List<LatLonBox> getLatLonBox() {
		return LatLonBox;
	}

	/**
	 * @param latLonBox the latLonBox to set
	 */
	public void setLatLonBox(List<LatLonBox> latLonBox) {
		LatLonBox = latLonBox;
	}

	@Override
	public String toString() {
		return "ExtendedData [LatLonBox=" + LatLonBox + "]";
	}
	

}
