package com.cydcor.framework.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("kml")
public class Kml {

	private String xmlns;
	private Document document;

	/**
	 * @return the xmlns
	 */
	public String getXmlns() {
		return xmlns;
	}

	/**
	 * @param xmlns
	 *            the xmlns to set
	 */
	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	/**
	 * @return the document
	 */
	public Document getDocument() {
		return document;
	}

	/**
	 * @param document the document to set
	 */
	public void setDocument(Document document) {
		this.document = document;
	}
}
