/**
 * 
 */
package com.cydcor.framework.model;

/**
 * This class defines relations between different Tables in Database. Relations
 * are loaded when ever we load any Table MedataData
 * 
 * @author ashwin
 * 
 */
public class Relation {

	private String fromTableName = "";
	private String toTableTableName = "";
	private String relationName = "";

}
