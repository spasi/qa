package com.cydcor.framework.model.xml.b2breport;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class Row {
	
	@XStreamImplicit(itemFieldName="element")
	private List<Element> element;

	public List<Element> getElement() {
		return element;
	}

	public void setElement(List<Element> element) {
		this.element = element;
	}
}
