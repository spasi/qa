package com.cydcor.framework.model.xml.b2breport;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("viewport")
public class ViewPort {
	
	private NorthEast northeast;
	private SouthWest southwest;
	
	public NorthEast getNortheast() {
		return northeast;
	}
	public void setNortheast(NorthEast northeast) {
		this.northeast = northeast;
	}
	public SouthWest getSouthwest() {
		return southwest;
	}
	public void setSouthwest(SouthWest southwest) {
		this.southwest = southwest;
	}

}
