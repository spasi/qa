package com.cydcor.framework.model.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.utils.ObjectUtils;

public class XmlMain {
	private static final transient Log logger = LogFactory.getLog(XmlMain.class);
	public static void main(String[] args) {
		//create a kml
		Kml kml = new Kml();
		kml.setXmlns("http://earth.google.com/kml/2.0");
		
		//creating document
		Document document = addDocument();
		kml.setDocument(document);
		
		//creating styles
		addStyle(document);
		
		//creating placeholder
		addPlaceHolder(document);
		
		
		List<Class> annotationList = new ArrayList<Class>();
		annotationList.add(Kml.class);
		
		Map<Class,String> attributeMap = new HashMap<Class, String>();
		attributeMap.put(Style.class, "id");
		attributeMap.put(Kml.class, "xmlns");
		
		logger.debug(ObjectUtils.annotationBasedSerialize(kml, annotationList, attributeMap));
		//logger.debug(JSONSerializer.toJSON(kml));
	}

	/**
	 * @param document
	 */
	private static void addPlaceHolder(Document document) {
		Placemark placemark = new Placemark();
		placemark.setName("Marker 1");
		placemark.setStyleUrl("#downArrow");
		placemark.setDescription("I've been loaded from kml!");
		Point point = new Point();
		point.setCoordinates("-122.1,37.4,0");
		placemark.setPoint(point);
		document.getPlaceMarkList().add(placemark);
		
		placemark = new Placemark();
		placemark.setName("Marker 2");
		placemark.setStyleUrl("#blueDot");
		placemark.setDescription("I as well");
		point = new Point();
		point.setCoordinates("-122.0,37.4,0");
		placemark.setPoint(point);
		document.getPlaceMarkList().add(placemark);
		
		//create new placemark
		placemark = new Placemark();
		placemark.setName("hollow box");
		placemark.setStyleUrl("#examplePolyStyle");
		placemark.setDescription("I'm from kml");
		//create new polygon
		Polygon polygon = new Polygon();
		polygon.setExtrude("1");
		polygon.setAltitudeMode("relativeToGround");
		//create new outerBoundaryIs
		OuterBoundaryIs outerBoundaryIs = new OuterBoundaryIs();
		polygon.setOuterBoundaryIs(outerBoundaryIs);
		//crearte new linearring
		LinearRing linearRing = new LinearRing();
		linearRing.setCoordinates(" -122.086565,37.423425,30 "
				+ "-121.586565,37.423425,30" + "-121.586565,37.023425,30 "
				+ "-122.086565,37.023425,30 " + "-122.086565,37.423425,30 ");
		outerBoundaryIs.setLinearRing(linearRing);
		
		//create new innerBoundaryIs
		InnerBoundaryIs innerBoundaryIs = new InnerBoundaryIs();
		polygon.setInnerBoundaryIs(innerBoundaryIs);
		linearRing = new LinearRing();
		linearRing.setCoordinates("-122.366212593918,37.81897719083808,30"
				+ " -122.3654241733188,37.81929450992014,30"
				+ "  -122.3657048517827,37.81973175302663,30"
				+ "  -122.3664882465854,37.81940249291773,30"
				+ " -122.366212593918,37.81897719083808,30 ");
		innerBoundaryIs.setLinearRing(linearRing);
		//set the polygon to placemark
		placemark.setPolygon(polygon);
		document.getPlaceMarkList().add(placemark);
		
		//create new placemark
		placemark = new Placemark();
		placemark.setName("purple line");
		placemark.setStyleUrl("#examplePolyStyle");
		placemark.setDescription("I'm a line from kml	");
		//create new linestring
		LineString lineString = new LineString();
		lineString.setExtrude("1");
		lineString.setTessellate("1");
		lineString.setAltitudeMode("absolute");
		lineString.setCoordinates("-121.586565,37.523425,2357 "
				+ "-121.886565,37.523425,2357 " + "-122.086565,37.923425,2357 "
				+ "-122.086565,37.523425,2357");
		placemark.setLineString(lineString);
		document.getPlaceMarkList().add(placemark);
	}

	/**
	 * @param document
	 */
	private static void addStyle(Document document) {
		Style style = new Style();
		style.setName("testeeeddd");
		style.setId("downArrow");
		IconStyle iconStyle = new IconStyle();
		style.setIconStyle(iconStyle);
		Icon icon = new Icon();
		icon.setHref("http://maps.google.com/mapfiles/kml/pal4/icon28.png");
		iconStyle.setIcon(icon);
		document.getStyleList().add(style);
		
		style = new Style();
		style.setId("blueDot");
		iconStyle = new IconStyle();
		style.setIconStyle(iconStyle);
		icon = new Icon();
		icon.setHref("http://maps.google.com/mapfiles/ms/micons/blue-dot.png");
		iconStyle.setIcon(icon);
		document.getStyleList().add(style);
		
		style = new Style();
		style.setId("examplePolyStyle");
		PolyStyle polyStyle = new PolyStyle();
		polyStyle.setColor("aaff00cc");
		polyStyle.setColorMode("normal");
		polyStyle.setFill(1);
		polyStyle.setOutline(1);
		style.setPolyStyle(polyStyle);
		document.getStyleList().add(style);
	}

	/**
	 * @return
	 */
	private static Document addDocument() {
		Document document = new Document();
		document.setName("KML Example file");
		document.setDescription("Simple markers");
		return document;
	}
}
