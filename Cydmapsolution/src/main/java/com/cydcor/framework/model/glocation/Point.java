package com.cydcor.framework.model.glocation;

import org.apache.commons.lang.StringUtils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Point")
public class Point {
	private String coordinates;
	
	/**
	 * @return the coordinates
	 */
	public String getCoordinates() {
		return coordinates;
	}

	/**
	 * @param coordinates
	 *            the coordinates to set
	 */
	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public String getGeoPointForDb(){
		String finalPoint = getCoordinates();
		int noOfComma = StringUtils.countMatches(getCoordinates(), ",");
		if(noOfComma == 2){
			finalPoint = StringUtils.substring(finalPoint,0 ,StringUtils.lastIndexOf(finalPoint, ","));
		}
		return "POINT(" + finalPoint.replaceAll(",", " ") + ")";
	}
	@Override
	public String toString() {
		return "Point [coordinates=" + coordinates + "]";
	}

}
