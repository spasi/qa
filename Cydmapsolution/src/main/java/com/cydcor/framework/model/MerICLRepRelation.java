package com.cydcor.framework.model;

import java.sql.Timestamp;

public class MerICLRepRelation extends GenericModel {
	private long campaign_seq;
	private long office_seq;
	private long person_id;
	private long campaign_agent_code;
	private Timestamp campaign_office_relationship_start_date;
	private Timestamp campaign_office_relationship_end_date;
	private String active;
	private String process_status;
	private Timestamp process_date;
	private String error_msg;
	private String retry_count;

	/**
	 * @return the campaign_seq
	 */
	public long getCampaign_seq() {
		return campaign_seq;
	}

	/**
	 * @param campaignSeq
	 *            the campaign_seq to set
	 */
	public void setCampaign_seq(long campaignSeq) {
		campaign_seq = campaignSeq;
	}

	/**
	 * @return the office_seq
	 */
	public long getOffice_seq() {
		return office_seq;
	}

	/**
	 * @param officeSeq
	 *            the office_seq to set
	 */
	public void setOffice_seq(long officeSeq) {
		office_seq = officeSeq;
	}

	/**
	 * @return the person_id
	 */
	public long getPerson_id() {
		return person_id;
	}

	/**
	 * @param personId
	 *            the person_id to set
	 */
	public void setPerson_id(long personId) {
		person_id = personId;
	}

	/**
	 * @return the campaign_agent_code
	 */
	public long getCampaign_agent_code() {
		return campaign_agent_code;
	}

	/**
	 * @param campaignAgentCode
	 *            the campaign_agent_code to set
	 */
	public void setCampaign_agent_code(long campaignAgentCode) {
		campaign_agent_code = campaignAgentCode;
	}

	/**
	 * @return the campaign_office_relationship_start_date
	 */
	public Timestamp getCampaign_office_relationship_start_date() {
		return campaign_office_relationship_start_date;
	}

	/**
	 * @param campaignOfficeRelationshipStartDate
	 *            the campaign_office_relationship_start_date to set
	 */
	public void setCampaign_office_relationship_start_date(
			Timestamp campaignOfficeRelationshipStartDate) {
		campaign_office_relationship_start_date = campaignOfficeRelationshipStartDate;
	}

	/**
	 * @return the campaign_office_relationship_end_date
	 */
	public Timestamp getCampaign_office_relationship_end_date() {
		return campaign_office_relationship_end_date;
	}

	/**
	 * @param campaignOfficeRelationshipEndDate
	 *            the campaign_office_relationship_end_date to set
	 */
	public void setCampaign_office_relationship_end_date(
			Timestamp campaignOfficeRelationshipEndDate) {
		campaign_office_relationship_end_date = campaignOfficeRelationshipEndDate;
	}

	/**
	 * @return the active
	 */
	public String getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}

	/**
	 * @return the process_status
	 */
	public String getProcess_status() {
		return process_status;
	}

	/**
	 * @param processStatus
	 *            the process_status to set
	 */
	public void setProcess_status(String processStatus) {
		process_status = processStatus;
	}

	/**
	 * @return the process_date
	 */
	public Timestamp getProcess_date() {
		return process_date;
	}

	/**
	 * @param processDate
	 *            the process_date to set
	 */
	public void setProcess_date(Timestamp processDate) {
		process_date = processDate;
	}

	/**
	 * @return the error_msg
	 */
	public String getError_msg() {
		return error_msg;
	}

	/**
	 * @param errorMsg
	 *            the error_msg to set
	 */
	public void setError_msg(String errorMsg) {
		error_msg = errorMsg;
	}

	/**
	 * @return the retry_count
	 */
	public String getRetry_count() {
		return retry_count;
	}

	/**
	 * @param retryCount
	 *            the retry_count to set
	 */
	public void setRetry_count(String retryCount) {
		retry_count = retryCount;
	}
}