package com.cydcor.framework.model.xml.b2breport;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("southwest")
public class SouthWest {
	
	private String lat;
	private String lng;
	
	public String getLatitude() {
		return lat;
	}
	public void setLatitude(String latitude) {
		this.lat = latitude;
	}
	public String getLongitude() {
		return lng;
	}
	public void setLongitude(String longitude) {
		this.lng = longitude;
	}
}
