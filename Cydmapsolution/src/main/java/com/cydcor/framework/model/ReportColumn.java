package com.cydcor.framework.model;

/**
 * @author ashwin
 * 
 */
public class ReportColumn extends BaseDataObject {
	private Long id = 0L;
	private String columnName = "";
	private String columnLabel = "";
	private String panelName = "";
	private boolean isFilterEnabled = true;
	private boolean isSortEnabled = true;
	private boolean isDisplayable = true;
	private int displayOrder = 0;
	private String dateFormat = "";
	private String numberFormat = "";
	private int displayWidth = 30;
	private String tooltip = "";
	private boolean isReadOnly = true;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * @return the columnLabel
	 */
	public String getColumnLabel() {
		return columnLabel;
	}

	/**
	 * @param columnLabel
	 *            the columnLabel to set
	 */
	public void setColumnLabel(String columnLabel) {
		this.columnLabel = columnLabel;
	}

	/**
	 * @return the panelName
	 */
	public String getPanelName() {
		return panelName;
	}

	/**
	 * @param panelName
	 *            the panelName to set
	 */
	public void setPanelName(String panelName) {
		this.panelName = panelName;
	}

	/**
	 * @return the isFilterEnabled
	 */
	public boolean isFilterEnabled() {
		return isFilterEnabled;
	}

	/**
	 * @param isFilterEnabled
	 *            the isFilterEnabled to set
	 */
	public void setFilterEnabled(boolean isFilterEnabled) {
		this.isFilterEnabled = isFilterEnabled;
	}

	/**
	 * @return the isSortEnabled
	 */
	public boolean isSortEnabled() {
		return isSortEnabled;
	}

	/**
	 * @param isSortEnabled
	 *            the isSortEnabled to set
	 */
	public void setSortEnabled(boolean isSortEnabled) {
		this.isSortEnabled = isSortEnabled;
	}

	/**
	 * @return the isDisplayable
	 */
	public boolean isDisplayable() {
		return isDisplayable;
	}

	/**
	 * @param isDisplayable
	 *            the isDisplayable to set
	 */
	public void setDisplayable(boolean isDisplayable) {
		this.isDisplayable = isDisplayable;
	}

	/**
	 * @return the displayOrder
	 */
	public int getDisplayOrder() {
		return displayOrder;
	}

	/**
	 * @param displayOrder
	 *            the displayOrder to set
	 */
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	/**
	 * @return the dateFormat
	 */
	public String getDateFormat() {
		return dateFormat;
	}

	/**
	 * @param dateFormat
	 *            the dateFormat to set
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	/**
	 * @return the numberFormat
	 */
	public String getNumberFormat() {
		return numberFormat;
	}

	/**
	 * @param numberFormat
	 *            the numberFormat to set
	 */
	public void setNumberFormat(String numberFormat) {
		this.numberFormat = numberFormat;
	}

	/**
	 * @return the displayWidth
	 */
	public int getDisplayWidth() {
		return displayWidth;
	}

	/**
	 * @param displayWidth
	 *            the displayWidth to set
	 */
	public void setDisplayWidth(int displayWidth) {
		this.displayWidth = displayWidth;
	}

	/**
	 * @return the tooltip
	 */
	public String getTooltip() {
		return tooltip;
	}

	/**
	 * @param tooltip
	 *            the tooltip to set
	 */
	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	/**
	 * @return the isReadOnly
	 */
	public boolean isReadOnly() {
		return isReadOnly;
	}

	/**
	 * @param isReadOnly
	 *            the isReadOnly to set
	 */
	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

}
