package com.cydcor.framework.model;

import java.sql.Timestamp;

public class LeadLifeCycleDetails extends GenericModel {
	private String run_id;
	private String lead_id;
	private String cust_name;
	private String full_addr;
	private String apt_num;
	private String city;
	private String state;
	private String zip;
	private Timestamp lead_expires;
	private String notes;
	private String geo_point;
	private String validation_code;
	private String response_text;
	private long campaign_seq;
	private long office_seq;
	private String clli;
	private String da;
	private long person_id;
	private String marker_icon_image;
	private String lead_status;
	private String credit_class;
	private String green_status;
	private String conn_tech;
	private String disposition_code;
	private String disposition;
	private String is_active;
	private Timestamp created_date;
	private String created_by;
	private Timestamp modified_date;
	private String modified_by;
	private long icl_rep_seq;
	private String region;
	private String status;
	private String house_num;
	private String str_pre;
	private String str_name;
	private String str_type;
	private String str_post;
	private Timestamp lead_dob;
	private Timestamp lead_assigned;
	private Timestamp last_updated;
	private String update_crm;
	private String process_status;
	private Timestamp process_date;
	private String error_message;
	private long retry_count;

	public String getRun_id() {
		return run_id;
	}

	public void setRun_id(String run_id) {
		this.run_id = run_id;
	}

	public String getLead_id() {
		return lead_id;
	}

	public void setLead_id(String lead_id) {
		this.lead_id = lead_id;
	}

	public String getCust_name() {
		return cust_name;
	}

	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}

	public String getFull_addr() {
		return full_addr;
	}

	public void setFull_addr(String full_addr) {
		this.full_addr = full_addr;
	}

	public String getApt_num() {
		return apt_num;
	}

	public void setApt_num(String apt_num) {
		this.apt_num = apt_num;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Timestamp getLead_expires() {
		return lead_expires;
	}

	public void setLead_expires(Timestamp lead_expires) {
		this.lead_expires = lead_expires;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getGeo_point() {
		return geo_point;
	}

	public void setGeo_point(String geo_point) {
		this.geo_point = geo_point;
	}

	public String getValidation_code() {
		return validation_code;
	}

	public void setValidation_code(String validation_code) {
		this.validation_code = validation_code;
	}

	public String getResponse_text() {
		return response_text;
	}

	public void setResponse_text(String response_text) {
		this.response_text = response_text;
	}

	public long getCampaign_seq() {
		return campaign_seq;
	}

	public void setCampaign_seq(long campaign_seq) {
		this.campaign_seq = campaign_seq;
	}

	public long getOffice_seq() {
		return office_seq;
	}

	public void setOffice_seq(long office_seq) {
		this.office_seq = office_seq;
	}

	public String getClli() {
		return clli;
	}

	public void setClli(String clli) {
		this.clli = clli;
	}

	public String getDa() {
		return da;
	}

	public void setDa(String da) {
		this.da = da;
	}

	public long getPerson_id() {
		return person_id;
	}

	public void setPerson_id(long person_id) {
		this.person_id = person_id;
	}

	public String getMarker_icon_image() {
		return marker_icon_image;
	}

	public void setMarker_icon_image(String marker_icon_image) {
		this.marker_icon_image = marker_icon_image;
	}

	public String getLead_status() {
		return lead_status;
	}

	public void setLead_status(String lead_status) {
		this.lead_status = lead_status;
	}

	public String getCredit_class() {
		return credit_class;
	}

	public void setCredit_class(String credit_class) {
		this.credit_class = credit_class;
	}

	public String getGreen_status() {
		return green_status;
	}

	public void setGreen_status(String green_status) {
		this.green_status = green_status;
	}

	public String getConn_tech() {
		return conn_tech;
	}

	public void setConn_tech(String conn_tech) {
		this.conn_tech = conn_tech;
	}

	public String getDisposition_code() {
		return disposition_code;
	}

	public void setDisposition_code(String disposition_code) {
		this.disposition_code = disposition_code;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public String getIs_active() {
		return is_active;
	}

	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}

	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Timestamp getModified_date() {
		return modified_date;
	}

	public void setModified_date(Timestamp modified_date) {
		this.modified_date = modified_date;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public long getIcl_rep_seq() {
		return icl_rep_seq;
	}

	public void setIcl_rep_seq(long icl_rep_seq) {
		this.icl_rep_seq = icl_rep_seq;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHouse_num() {
		return house_num;
	}

	public void setHouse_num(String house_num) {
		this.house_num = house_num;
	}

	public String getStr_pre() {
		return str_pre;
	}

	public void setStr_pre(String str_pre) {
		this.str_pre = str_pre;
	}

	public String getStr_name() {
		return str_name;
	}

	public void setStr_name(String str_name) {
		this.str_name = str_name;
	}

	public String getStr_type() {
		return str_type;
	}

	public void setStr_type(String str_type) {
		this.str_type = str_type;
	}

	public String getStr_post() {
		return str_post;
	}

	public void setStr_post(String str_post) {
		this.str_post = str_post;
	}

	public Timestamp getLead_dob() {
		return lead_dob;
	}

	public void setLead_dob(Timestamp lead_dob) {
		this.lead_dob = lead_dob;
	}

	public Timestamp getLead_assigned() {
		return lead_assigned;
	}

	public void setLead_assigned(Timestamp lead_assigned) {
		this.lead_assigned = lead_assigned;
	}

	public Timestamp getLast_updated() {
		return last_updated;
	}

	public void setLast_updated(Timestamp last_updated) {
		this.last_updated = last_updated;
	}

	public String getUpdate_crm() {
		return update_crm;
	}

	public void setUpdate_crm(String update_crm) {
		this.update_crm = update_crm;
	}

	public String getProcess_status() {
		return process_status;
	}

	public void setProcess_status(String process_status) {
		this.process_status = process_status;
	}

	public Timestamp getProcess_date() {
		return process_date;
	}

	public void setProcess_date(Timestamp process_date) {
		this.process_date = process_date;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}

	public long getRetry_count() {
		return retry_count;
	}

	public void setRetry_count(long retry_count) {
		this.retry_count = retry_count;
	}
}