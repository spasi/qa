package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Country")
public class Country {
	
	private String CountryName;
	
	private String CountryNameCode;
	private AdministrativeArea AdministrativeArea;
	private SubAdministrativeArea SubAdministrativeArea;
	private Locality Locality;
	private Thoroughfare Thoroughfare;
	private PostalCode PostalCode;
	
	/**
	 * @return the subAdministrativeArea
	 */
	public SubAdministrativeArea getSubAdministrativeArea() {
		return SubAdministrativeArea;
	}
	
	/**
	 * @param subAdministrativeArea
	 *            the subAdministrativeArea to set
	 */
	public void setSubAdministrativeArea(SubAdministrativeArea subAdministrativeArea) {
		this.SubAdministrativeArea = subAdministrativeArea;
	}
	
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return CountryName;
	}
	
	/**
	 * @param countryName
	 *            the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.CountryName = countryName;
	}
	
	/**
	 * @return the countryNameCode
	 */
	public String getCountryNameCode() {
		return CountryNameCode;
	}
	
	/**
	 * @param countryNameCode
	 *            the countryNameCode to set
	 */
	public void setCountryNameCode(String countryNameCode) {
		this.CountryNameCode = countryNameCode;
	}
	
	/**
	 * @return the administrativeArea
	 */
	public AdministrativeArea getAdministrativeArea() {
		return AdministrativeArea;
	}
	
	/**
	 * @param administrativeArea
	 *            the administrativeArea to set
	 */
	public void setAdministrativeArea(AdministrativeArea administrativeArea) {
		this.AdministrativeArea = administrativeArea;
	}
	
	public Locality getLocality() {
		return Locality;
	}
	
	public void setLocality(Locality locality) {
		Locality = locality;
	}
	
	public Thoroughfare getThoroughfare() {
		return Thoroughfare;
	}
	
	public void setThoroughfare(Thoroughfare thoroughfare) {
		Thoroughfare = thoroughfare;
	}
	
	public PostalCode getPostalCode() {
		return PostalCode;
	}
	
	public void setPostalCode(PostalCode postalCode) {
		PostalCode = postalCode;
	}
	
	@Override
	public String toString() {
		return "Country [AdministrativeArea=" + AdministrativeArea + ", CountryName=" + CountryName
				+ ", CountryNameCode=" + CountryNameCode + "]";
	}
}