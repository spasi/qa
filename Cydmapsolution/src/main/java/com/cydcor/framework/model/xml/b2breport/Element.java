package com.cydcor.framework.model.xml.b2breport;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class Element {
	
	private String status;
	@XStreamImplicit(itemFieldName="duration")
	private List<Duration> duration;
	@XStreamImplicit(itemFieldName="distance")
	private List<Distance> distance;
	
	public List<Duration> getDuration() {
		return duration;
	}
	public void setDuration(List<Duration> duration) {
		this.duration = duration;
	}
	public List<Distance> getDistance() {
		return distance;
	}
	public void setDistance(List<Distance> distance) {
		this.distance = distance;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	} 
}
