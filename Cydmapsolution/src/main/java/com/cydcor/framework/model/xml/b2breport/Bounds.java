package com.cydcor.framework.model.xml.b2breport;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("bounds")
public class Bounds {
	
	private SouthWest southwest;
	private NorthEast northeast;
	
	public SouthWest getSouthwest() {
		return southwest;
	}
	public void setSouthwest(SouthWest southwest) {
		this.southwest = southwest;
	}
	public NorthEast getNortheast() {
		return northeast;
	}
	public void setNortheast(NorthEast northeast) {
		this.northeast = northeast;
	}

}
