/**
 * 
 */
package com.cydcor.framework.model;

import java.util.List;

/**
 * This class Holds Metadata of Query Fired and also Result returned by the
 * query
 * 
 * @author ashwin
 * 
 */
public class DatabaseResult {
	private DBEntityComponent metadata = null;
	private List<List<MapObject>> data = null;
	private String totalRows = null;
	private Integer offset = 0;

	/**
	 * @return the metadata
	 */
	public DBEntityComponent getMetadata() {
		return metadata;
	}

	/**
	 * @param metadata
	 *            the metadata to set
	 */
	public void setMetadata(DBEntityComponent metadata) {
		this.metadata = metadata;
	}

	/**
	 * @return the data
	 */
	public List<List<MapObject>> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<List<MapObject>> data) {
		this.data = data;
	}

	/**
	 * @return the totalRows
	 */
	public String getTotalRows() {
		return totalRows;
	}

	/**
	 * @param totalRows
	 *            the totalRows to set
	 */
	public void setTotalRows(String totalRows) {
		this.totalRows = totalRows;
	}

	/**
	 * @return the offset
	 */
	public Integer getOffset() {
		return offset;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(Integer offset) {
		this.offset = offset;
	}

}
