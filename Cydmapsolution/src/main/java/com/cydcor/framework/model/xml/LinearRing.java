package com.cydcor.framework.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("LinearRing")
public class LinearRing {
private String coordinates;

/**
 * @return the coordinates
 */
public String getCoordinates() {
	return coordinates;
}

/**
 * @param coordinates the coordinates to set
 */
public void setCoordinates(String coordinates) {
	this.coordinates = coordinates;
}

}
