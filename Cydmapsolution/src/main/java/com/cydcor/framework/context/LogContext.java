package com.cydcor.framework.context;

import java.util.Date;

public class LogContext {

    private String accessUser;
    private Date accessDate;
    private String ipAdress;
    private String pageName;
    private String mapType;
    private String mapModule;

    public String getAccessUser() {
	return accessUser;
    }

    public void setAccessUser(String accessUser) {
	this.accessUser = accessUser;
    }

    public Date getAccessDate() {
	return accessDate;
    }

    public void setAccessDate(Date accessDate) {
	this.accessDate = accessDate;
    }

    public String getIpAdress() {
	return ipAdress;
    }

    public void setIpAdress(String ipAdress) {
	this.ipAdress = ipAdress;
    }

    public String getPageName() {
	return pageName;
    }

    public void setPageName(String pageName) {
	this.pageName = pageName;
    }

    public String getMapType() {
	return mapType;
    }

    public void setMapType(String mapType) {
	this.mapType = mapType;
    }

    public String getMapModule() {
	return mapModule;
    }

    public void setMapModule(String mapModule) {
	this.mapModule = mapModule;
    }

}
