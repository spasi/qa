/**
 * 
 */
package com.cydcor.framework.context;

import java.sql.Connection;

import com.cydcor.framework.model.UserObject;
import com.cydcor.framework.wrapper.CydcorResponseWrapper;

/**
 * This Object is created per user request. This is used to store Per Request
 * Resources Like Connection and any user Specific Information
 * 
 * @author ashwin
 * 
 */
public class ResponseContext {
    // Base where application is deployed
    private String basePath = "";
    private Connection connection = null;
    private UserObject user = null;
    private CydcorResponseWrapper responseWrapper = null;

    /**
     * @return the connection
     */
    public Connection getConnection() {
	return connection;
    }

    /**
     * @param connection
     *            the connection to set
     */
    public void setConnection(Connection connection) {
	this.connection = connection;
    }

    /**
     * @return the user
     */
    public UserObject getUser() {
	return user;
    }

    /**
     * @param user
     *            the user to set
     */
    public void setUser(UserObject user) {
	this.user = user;
    }

    /**
     * @return the responseWrapper
     */
    public CydcorResponseWrapper getResponseWrapper() {
	return responseWrapper;
    }

    /**
     * @param responseWrapper
     *            the responseWrapper to set
     */
    public void setResponseWrapper(CydcorResponseWrapper responseWrapper) {
	this.responseWrapper = responseWrapper;
    }
}
