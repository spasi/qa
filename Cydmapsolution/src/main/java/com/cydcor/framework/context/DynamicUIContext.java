/**
 * 
 */
package com.cydcor.framework.context;

import java.util.ArrayList;
import java.util.List;

import com.cydcor.framework.clickframework.GenericUIControl;

/**
 * @author ashwin
 * 
 */
public class DynamicUIContext {
	private String pageName = "";
	private String controlName = "";
	private List<GenericUIControl> controls = new ArrayList<GenericUIControl>();
	private boolean showMap = true;

	/**
	 * @return the pageName
	 */
	public String getPageName() {
		return pageName;
	}

	/**
	 * @param pageName
	 *            the pageName to set
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	/**
	 * @return the controlName
	 */
	public String getControlName() {
		return controlName;
	}

	/**
	 * @param controlName
	 *            the controlName to set
	 */
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}

	/**
	 * @return the showMap
	 */
	public boolean isShowMap() {
		return showMap;
	}

	/**
	 * @param showMap the showMap to set
	 */
	public void setShowMap(boolean showMap) {
		this.showMap = showMap;
	}

	/**
	 * @return the controls
	 */
	public List<GenericUIControl> getControls() {
		return controls;
	}

	/**
	 * @param controls
	 *            the controls to set
	 */
	public void setControls(List<GenericUIControl> controls) {
		this.controls = controls;
	}

	/**
	 * @param control
	 */
	public void addControl(GenericUIControl control) {
		this.controls.add(control);
	}

}
