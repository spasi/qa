/**
 * 
 */
package com.cydcor.framework.context;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.integration.privilege.utils.MappingPrivilegeUtil;

/**
 * 
 * @author ashwin
 * 
 */
public class CRMContext extends HashMap<String, Object> {

	private static final transient Log logger = LogFactory.getLog(CRMContext.class);

	/*
	 * (non-Javadoc) This Returns CRM Objects in Form Of String for SQL's
	 * 
	 * @see java.util.HashMap#get(java.lang.Object)
	 */

	@Override
	public Object get(Object key) {
		if (StringUtils.equalsIgnoreCase(key.toString(), "campaignSeqs")) {
			// ${GlobalContext.crmContext.campaignSeqs
			return MappingPrivilegeUtil.getCampaignSeqsByUserId(CydcorContext.getInstance().getUserId());
		} else if (StringUtils.equalsIgnoreCase(key.toString(), "officeSeqs")) {
			return MappingPrivilegeUtil.getOfficeSeqsByUserId(CydcorContext.getInstance().getUserId());
		} else if (StringUtils.equalsIgnoreCase(key.toString(), "accessLinks")) {
			String roleNames = MappingPrivilegeUtil.getRoleNamesByUserId(CydcorContext.getInstance().getUserId());
			logger.debug("roleName:"+roleNames);
			return roleNames;
		} else if (StringUtils.equalsIgnoreCase(key.toString(), "roleNames")) {
			String roleName = MappingPrivilegeUtil.getRoleNamesByUserId(CydcorContext.getInstance().getUserId());
			logger.debug("roleName:"+roleName);
			return roleName;
		} else if (StringUtils.equalsIgnoreCase(key.toString(), "userName")) {
			String roleName = MappingPrivilegeUtil.getUserNameByUserId(CydcorContext.getInstance().getUserId());
			logger.debug("userName:"+roleName);
			return roleName;
		} else if (StringUtils.equalsIgnoreCase(key.toString(), "profileName")) {
			List<String> str=MappingPrivilegeUtil.getProfileNameByUserId(CydcorContext.getInstance().getUserId());
			if(str!=null&&str.size()>0)
			  logger.debug("profiles found::"+str.size());
			else
				logger.debug("no profiles found.");
			return str;
		} else if (StringUtils.equalsIgnoreCase(key.toString(), "officecode")) {
			String ownnerAlias = MappingPrivilegeUtil.getOfficeCodeByUserId(CydcorContext.getInstance().getUserId());
			return ownnerAlias;
		}

		return super.get(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashMap#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public Object put(String key, Object value) {
		// TODO Auto-generated method stub
		return super.put(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashMap#remove(java.lang.Object)
	 */
	@Override
	public Object remove(Object key) {
		// TODO Auto-generated method stub
		return super.remove(key);
	}

}
