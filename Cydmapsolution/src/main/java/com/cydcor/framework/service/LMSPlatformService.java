package com.cydcor.framework.service;

import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.GenericModel;

public interface LMSPlatformService extends PlatformService {
	/**
	 * @param sql
	 * @param rowMapper
	 * @return
	 */
	public List<? extends GenericModel> loadLMSRowMapper(String sql,
			RowMapper rowMapper);

	/**
	 * @param leadListIds
	 */
	public void updateLeadMaster(List<Long> leadListIds);

}
