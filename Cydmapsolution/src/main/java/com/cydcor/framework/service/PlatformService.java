/**
 * 
 */
package com.cydcor.framework.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.B2BReport;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.GenericModel;
import com.cydcor.framework.model.LeadLifeCycle;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.MerOffice;
import com.cydcor.framework.model.MerOfficeInsert;
import com.cydcor.framework.model.ReportComponent;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.model.UserObject;
import com.cydcor.framework.model.VerizonFiosReport;
import com.cydcor.framework.ui.model.RequestObject;

/**
 * All Generic Service methods are Handled here
 * 
 * @author ashwin
 * 
 */
/**
 * @author suresh.chittala
 * 
 */
public interface PlatformService {

    /**
     * @param lwbCode
     * @return null if user not there/or not authenticated
     */
    public UserObject loadUser(String lwbCode);

    /**
     * For any Query Fired it creates MetadataObject along with data returned
     * frm database and wraps it in DatabaseResult
     * 
     * @param sql
     * @return
     */
    public DatabaseResult loadResult(String sql);

    /**
     * @param reportName
     * @return
     */
    public ReportComponent loadReport(String reportName);

    /**
     * ReportComponent Here acts as a Value Object for the data sent from
     * Request
     * 
     * Set Report ID and Filter Parameters in report Component Object and pass
     * to this method and this method processes further and returns result
     * 
     * @param reportComponent
     * @return
     */
    public DatabaseResult loadData(ReportComponent reportComponent, RequestObject requestObject);

    /**
     * @param sql
     * @param rowMapper
     * @return
     */
    public List<? extends GenericModel> loadRowMapper(String sql, RowMapper rowMapper);

    public List<? extends GenericModel> loadRowMapper(String sql, RowMapper rowMapper,String clientKey);
    
    /**
     * @param query
     * @param values
     */
    public void save(String query, Object[] values);

    /**
     * @param insertSql
     * @param values
     */
    public void populateLeadInput(List<LeadLifeCycle> values);

    /**
     * @param merOfficeInserts
     */
    public void populateIclMaster(List<MerOfficeInsert> merOfficeInserts);
    public void populateIclMaster(List<MerOfficeInsert> merOfficeInserts,String clientKey);

    /**
     * @param merOffice
     */
    public void deleteOfficeSeqs(List<MerOffice> merOffice);

    /**
     * @param sql
     */
    public void execute(String sql);

    /**
     * @param sql
     * @param args
     */
    public void update(String sql, Object[] args);
    
    public void update(String sql, Object[] args,String clientKey);

    /**
     * @param sql
     * @param values
     * @return
     */
    public DatabaseResult loadResult(String sql, List values);

    /**
     * 
     * @param values
     */
    public void updateLeadLifeCycle(final List<LeadLifeCycle> values);

    // public DatabaseResult query(String string, List<String> list);

    public Long saveAndGetKey(final String sql, final Object[] args);

    /**
     * @param campaignSeq
     * @param officeSeq
     * @param templateName
     *            TODO
     * @return return template params assigned to this guy
     */
    public DatabaseResult loadTemplateParams(int campaignSeq, int officeSeq, String templateName);

    public DatabaseResult loadSystemDefaultTemplateParams(int campaignSeq, int officeSeq, String templateName);

    /**
     * @param campaignSeq
     * @param templateName
     * @return return template params assigned to this Campaign
     */
    public DatabaseResult loadCampaignTemplateParams(int campaignSeq, String templateName);

    void insertWireCenters(String[] values);

    void insertDAs(String[] values);

    public List<LeadMaster> selectLeadsByLeadSheets(String leadSheetIds);

    String saveOrMergeLeadsheets(final String tempLeadSheetIds, final String savedLeadSheetId, final boolean saveAllSheets, final boolean isAssigned,
	    final String campaignSeq, final String officeSeq);

    public void updateDispositionAndNotes(final List<String> dis, final List<String> diDescs, final List<String> notes, final List<String> rowIds,
	    final String leadSheetId, String archived);

    void updateLeadSheets(List<String[]> values);

    void updateLeadSheetNotes(List<String[]> values);
    
    void generateLeadsheetsByProximity(String campaignSeq, String iclSeq, String selectedType, String values, int maxLeads);

    void generateStreetProximityLeadSheetsByTempSheets(String leadSheet, int maxLeadsPerSheet, String officeSeq, String territorySeq);
    
    void generateStreetProximityLeadSheetsByTempSheets(String leadSheet, int maxLeadsPerSheet, String officeSeq, String territorySeq , String territoryType);

    void generateL2LProximityLeadSheetsByTempSheets(String tempLeadSheet, int maxLeadsPerSheet, String officeSeq, String territorySeq);
    
    void generateL2LProximityLeadSheetsByTempSheets(String tempLeadSheet, int maxLeadsPerSheet, String officeSeq, String territorySeq, String territoryType);

    void populateCRMPrivileges(List<RoleObject> roleObjects);

    public void renameTempLeadsheets(final List<String[]> values);

    public void updateLeadsheetPrintStatus(final List<String> values);

    /**
     * @param tempSheetId
     * @param integer
     * @param parameter
     * @param object
     */
    public void generateFiosL2LProximityLeadSheetsByTempSheets(String tempLeadSheet, int maxLeadsPerSheet, String officeSeq, String territorySeq,
	    String territoryType);
    /**
     * 
     * @param campaignSeq
     * @param officeSeq
     * @return
     */
    public List<B2BReport> getB2BTerritoryDetailsByZip(final String campaignSeq, final String officeSeq);
    
    public List getB2BHistoryByZip(final String officeSeq, final String zip);
    public List getB2BHistory(final String officeSeq, final String territoryType, final String zip, final String clli);
	public List getB2BNotesByZip(final String officeSeq, final String zip);
	public void updateNotesByZip(final String officeSeq, final String zip, final String clli, final String notes);
	public List getFiosHistoryByZip(final String officeSeq, final String zip);
	
	public List fetchLeadAssignmentList(final String campaignSeq, final String assignmentType, final String merlinCode, final String clli);
	public void saveNewCLLI(final String campaignSeq, final String assignmentType, final String merlinCode, final String clli, final String lfa, final String user);
	public void saveAssignments(final String campaignSeq, final Map<String, Map<String, HashSet>> map, final String user);
	
	public void upsertMergedOwners(final String fromOffice, final String toOffice, final String user, final String active, final String action, final String clientKey);
	
	public List<VerizonFiosReport> getFiosTrackerByTerritoryType(final String campaignSeq, final String officeSeq,final String territoryType);

	public List<B2BReport> getB2BTrackerByTerritoryType(String parameter,
			String parameter2, String territoryType);
}
