package com.cydcor.framework.service.impl;

import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.cydcor.framework.dao.LMSPlatformDAO;
import com.cydcor.framework.model.GenericModel;
import com.cydcor.framework.service.LMSPlatformService;
import com.cydcor.framework.utils.ServiceLocator;

@Service("lmsPlatformService")
public class LMSPlatformServiceImpl extends PlatformServiceImpl implements
		LMSPlatformService {

	private LMSPlatformDAO platformDAO;

	/**
	 * @return the platformDAO
	 */
	public LMSPlatformDAO getPlatformDAO() {
		if (platformDAO == null) {
			platformDAO = (LMSPlatformDAO) ServiceLocator
					.getService("lmsPlatformDAO");
		}
		return platformDAO;
	}

	/**
	 * @param platformDAO
	 *            the platformDAO to set
	 */
	public void setPlatformDAO(LMSPlatformDAO platformDAO) {
		this.platformDAO = platformDAO;
	}

	public List<? extends GenericModel> loadLMSRowMapper(String sql,
			RowMapper rowMapper) {
		return getPlatformDAO().loadLMSRowMapper(sql, rowMapper);
	}

	public void updateLeadMaster(List<Long> leadListIds) {
		getPlatformDAO().updateLeadMaster(leadListIds);

	}

}
