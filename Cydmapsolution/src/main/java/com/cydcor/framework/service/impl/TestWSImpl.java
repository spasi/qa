package com.cydcor.framework.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.cydcor.framework.service.TestWS;

public class TestWSImpl implements TestWS {
	
	private static final transient Log logger = LogFactory.getLog(TestWSImpl.class);

	private JdbcTemplate jdbcTemplate;
	
	private static final String updateQuery = "UPDATE employee2 set first_name= ? where last_name='cat'";
	private static final String selectQuery = "SELECT first_name FROM employee2 where last_name='cat'";

	public void updateName(String name) {
		try {
			getJdbcTemplate().update(updateQuery, new Object[] { name});
		} catch (Exception e) {
			logger.debug(e);
		}
	}

	public String getName() {
		try {
			return (String) getJdbcTemplate().queryForObject(selectQuery, String.class);
		} 
		catch (Exception e) {
			logger.debug(e);
		}
		return "";
	}

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	/**
	 * @param jdbcTemplate the jdbcTemplate to set
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
} // end class