package com.cydcor.integration.privilege.utils;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.crmod.model.BaseObject;

@XmlRootElement(name="CustomObject6")
@XmlAccessorType(XmlAccessType.NONE)
public class MappingPrivilegeVO extends BaseObject implements Serializable {
	private static final long serialVersionUID = 1L;

	@XmlElement(name="Name")
	private String name;
	
	@XmlElement(name="IndexedPick0")
	private String indexedPick0;
	
	@XmlElement(name="IndexedPick1")
	private String indexedPick1;
	
	@XmlElement(name="OwnerAlias")
	private String ownerAlias;
	
	@XmlElement(name="OwnerUserSignInId")
	private String ownerUserSignInId;
	
	@XmlElement(name="AccountExternalSystemId")
	private String accountExternalSystemId;
	
	@XmlElement(name="CustomObject4ExternalSystemId")
	private String customObject4ExternalSystemId;
	
	@XmlElement(name="CustomObject4Name")
	private String customObject4Name;

	@XmlElement(name="CreatedBy")
	private String createdBy;
	
	@XmlElement(name="ModifiedBy")
	private String modifiedBy;
	
	@XmlElement(name="Id")
	private String id;
	
	@XmlElement(name="OwnerFirstName")
	private String ownerFirstName;
	
	@XmlElement(name="OwnerLastName")
	private String ownerLastName;
	
	@XmlElement(name="OwnerFullName")
	private String ownerFullName;
	
	@XmlElement(name="OwnerEMailAddr")
	private String ownerEMailAddr;
	
	@XmlElement(name="OwnerIntegrationId")
	private String ownerIntegrationId;

	
	public String getCustomObject4Name() {
		return customObject4Name;
	}

	public void setCustomObject4Name(String customObject4Name) {
		this.customObject4Name = customObject4Name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIndexedPick0() {
		return indexedPick0;
	}

	public void setIndexedPick0(String indexedPick0) {
		this.indexedPick0 = indexedPick0;
	}

	public String getIndexedPick1() {
		return indexedPick1;
	}

	public void setIndexedPick1(String indexedPick1) {
		this.indexedPick1 = indexedPick1;
	}

	public String getOwnerAlias() {
		return ownerAlias;
	}

	public void setOwnerAlias(String ownerAlias) {
		this.ownerAlias = ownerAlias;
	}

	public String getOwnerUserSignInId() {
		return ownerUserSignInId;
	}

	public void setOwnerUserSignInId(String ownerUserSignInId) {
		this.ownerUserSignInId = ownerUserSignInId;
	}

	public String getAccountExternalSystemId() {
		return accountExternalSystemId;
	}

	public void setAccountExternalSystemId(String accountExternalSystemId) {
		this.accountExternalSystemId = accountExternalSystemId;
	}

	public String getCustomObject4ExternalSystemId() {
		return customObject4ExternalSystemId;
	}

	public void setCustomObject4ExternalSystemId(
			String customObject4ExternalSystemId) {
		this.customObject4ExternalSystemId = customObject4ExternalSystemId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOwnerFirstName() {
		return ownerFirstName;
	}

	public void setOwnerFirstName(String ownerFirstName) {
		this.ownerFirstName = ownerFirstName;
	}

	public String getOwnerLastName() {
		return ownerLastName;
	}

	public void setOwnerLastName(String ownerLastName) {
		this.ownerLastName = ownerLastName;
	}

	public String getOwnerFullName() {
		return ownerFullName;
	}

	public void setOwnerFullName(String ownerFullName) {
		this.ownerFullName = ownerFullName;
	}

	public String getOwnerEMailAddr() {
		return ownerEMailAddr;
	}

	public void setOwnerEMailAddr(String ownerEMailAddr) {
		this.ownerEMailAddr = ownerEMailAddr;
	}

	public String getOwnerIntegrationId() {
		return ownerIntegrationId;
	}

	public void setOwnerIntegrationId(String ownerIntegrationId) {
		this.ownerIntegrationId = ownerIntegrationId;
	}


}
