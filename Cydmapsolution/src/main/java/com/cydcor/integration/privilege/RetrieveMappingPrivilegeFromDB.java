package com.cydcor.integration.privilege;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.model.MerCampignDetails;
import com.cydcor.framework.model.MerICLDetails;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.test.SpringPage;
import com.cydcor.framework.ws.GenericWS;
import com.cydcor.ws.WSMappingInterface;

public class RetrieveMappingPrivilegeFromDB extends GenericWS implements WSMappingInterface {

    protected final Log logger = LogFactory.getLog(getClass());

    private static SpringPage springPage = null;
    String sessionId = null;

    public Map<String, Map<String, RoleObject>> call(String userId) throws Exception {
	RoleObject roleObject = null;
	Map<String, RoleObject> roleSpecificMap = null;
	Map<String, Map<String, RoleObject>> roleObjMap = new HashMap<String, Map<String, RoleObject>>();
	List<Object> inputs = new ArrayList<Object>();
	inputs.add(userId);
	//DatabaseResult result = getPlatformService().loadResult("SELECT * FROM IMS.IMS_CRM_MAPPING_PRIVILEGES WHERE LOGIN_ID=? ", inputs);
	DatabaseResult result = getPlatformService().loadResult("SELECT * FROM IMS.IMS_CRM_MAPPING_PRIVILEGES WHERE USER_ALIAS=? AND IS_ACTIVE='Y' ", inputs);
	List<List<MapObject>> data = result.getData();
	if (data.size() < 1)
	    return null;
	//roleSpecificMap = roleObjMap.get(data.get(0).get(12).getValue());
	roleSpecificMap = roleObjMap.get(data.get(0).get(2).getValue());
	if (roleSpecificMap == null) {
	    roleSpecificMap = new HashMap<String, RoleObject>();
	    //roleObjMap.put(data.get(0).get(12).getValue(), roleSpecificMap);
	    roleObjMap.put(data.get(0).get(2).getValue(), roleSpecificMap);
	}

	roleObject = roleSpecificMap.get(data.get(0).get(2).getValue());
	if (roleObject == null) {
	    roleObject = new RoleObject();
	    roleObject.setOwnerIntegrationId(data.get(0).get(0).getValue());
	    roleObject.setRoleName(data.get(0).get(1).getValue());
	    roleObject.setOwnerAlias(data.get(0).get(2).getValue());
	    roleObject.addProfileNames(data.get(0).get(3).getValue());
	    roleObject.setCreated(data.get(0).get(6).getValue());
	    roleObject.setModified(data.get(0).get(7).getValue());
	    roleObject.setOwnerFirstName(data.get(0).get(8).getValue());
	    roleObject.setOwnerLastName(data.get(0).get(9).getValue());
	    roleObject.setOwnerFullName(data.get(0).get(10).getValue());
	    roleObject.setOwnerEmailAddress(data.get(0).get(11).getValue());
	    roleObject.setOwnerUserSignInId(data.get(0).get(12).getValue());
	    roleSpecificMap.put(data.get(0).get(1).getValue(), roleObject);
	    roleObject.setMerlinICL(new ArrayList<MerICLDetails>());
	    roleObject.setMerlinCampaign(new ArrayList<MerCampignDetails>());
	}
	//MerICLDetails iclDetails = new MerICLDetails();
	//iclDetails.setOffice_seq(data.get(0).get(4).getValue());
	for (int count =0; count < data.size(); count++ )
	{
		MerICLDetails iclDetails = new MerICLDetails();
		iclDetails.setOffice_seq(data.get(count).get(5).getValue());
		roleObject.getMerlinICL().add(iclDetails);
	}
	//roleObject.getMerlinICL().add(iclDetails);

	//MerCampignDetails campaignObjs = new MerCampignDetails();
	//campaignObjs.setCampaign_seq(data.get(0).get(5).getValue());
	for (int count =0; count < data.size(); count++ )
	{
		MerCampignDetails campaignObjs = new MerCampignDetails();
		campaignObjs.setCampaign_seq(data.get(count).get(4).getValue());
		roleObject.getMerlinCampaign().add(campaignObjs);
	}
	//roleObject.getMerlinCampaign().add(campaignObjs);
	return roleObjMap;
    }
}
