/**
 * CustomObject6_BindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cydcor.ws.stubs.privillege;

public class CustomObject6_BindingImpl implements com.cydcor.ws.stubs.privillege.CustomObject6_PortType{
    public com.cydcor.ws.stubs.privillege.CustomObject6Update_Output customObject6Update(com.cydcor.ws.stubs.privillege.CustomObject6Update_Input customObject6Update_Input) throws java.rmi.RemoteException {
        return null;
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Insert_Output customObject6Insert(com.cydcor.ws.stubs.privillege.CustomObject6Insert_Input customObject6Insert_Input) throws java.rmi.RemoteException {
        return null;
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output customObject6QueryPage(com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Input customObject6QueryPage_Input) throws java.rmi.RemoteException {
        return null;
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Delete_Output customObject6Delete(com.cydcor.ws.stubs.privillege.CustomObject6Delete_Input customObject6Delete_Input) throws java.rmi.RemoteException {
        return null;
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Execute_Output customObject6Execute(com.cydcor.ws.stubs.privillege.CustomObject6Execute_Input customObject6Execute_Input) throws java.rmi.RemoteException {
        return null;
    }

}
