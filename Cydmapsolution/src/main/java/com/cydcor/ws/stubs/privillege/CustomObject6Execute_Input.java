/**
 * CustomObject6Execute_Input.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cydcor.ws.stubs.privillege;

public class CustomObject6Execute_Input  implements java.io.Serializable {
    private CustomObject6Data[] listOfCustomObject6;

    private java.lang.String LOVLanguageMode;

    private java.lang.String viewMode;

    private java.lang.String echo;

    public CustomObject6Execute_Input() {
    }

    public CustomObject6Execute_Input(
           CustomObject6Data[] listOfCustomObject6,
           java.lang.String LOVLanguageMode,
           java.lang.String viewMode,
           java.lang.String echo) {
           this.listOfCustomObject6 = listOfCustomObject6;
           this.LOVLanguageMode = LOVLanguageMode;
           this.viewMode = viewMode;
           this.echo = echo;
    }


    /**
     * Gets the listOfCustomObject6 value for this CustomObject6Execute_Input.
     * 
     * @return listOfCustomObject6
     */
    public CustomObject6Data[] getListOfCustomObject6() {
        return listOfCustomObject6;
    }


    /**
     * Sets the listOfCustomObject6 value for this CustomObject6Execute_Input.
     * 
     * @param listOfCustomObject6
     */
    public void setListOfCustomObject6(CustomObject6Data[] listOfCustomObject6) {
        this.listOfCustomObject6 = listOfCustomObject6;
    }


    /**
     * Gets the LOVLanguageMode value for this CustomObject6Execute_Input.
     * 
     * @return LOVLanguageMode
     */
    public java.lang.String getLOVLanguageMode() {
        return LOVLanguageMode;
    }


    /**
     * Sets the LOVLanguageMode value for this CustomObject6Execute_Input.
     * 
     * @param LOVLanguageMode
     */
    public void setLOVLanguageMode(java.lang.String LOVLanguageMode) {
        this.LOVLanguageMode = LOVLanguageMode;
    }


    /**
     * Gets the viewMode value for this CustomObject6Execute_Input.
     * 
     * @return viewMode
     */
    public java.lang.String getViewMode() {
        return viewMode;
    }


    /**
     * Sets the viewMode value for this CustomObject6Execute_Input.
     * 
     * @param viewMode
     */
    public void setViewMode(java.lang.String viewMode) {
        this.viewMode = viewMode;
    }


    /**
     * Gets the echo value for this CustomObject6Execute_Input.
     * 
     * @return echo
     */
    public java.lang.String getEcho() {
        return echo;
    }


    /**
     * Sets the echo value for this CustomObject6Execute_Input.
     * 
     * @param echo
     */
    public void setEcho(java.lang.String echo) {
        this.echo = echo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomObject6Execute_Input)) return false;
        CustomObject6Execute_Input other = (CustomObject6Execute_Input) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.listOfCustomObject6==null && other.getListOfCustomObject6()==null) || 
             (this.listOfCustomObject6!=null &&
              java.util.Arrays.equals(this.listOfCustomObject6, other.getListOfCustomObject6()))) &&
            ((this.LOVLanguageMode==null && other.getLOVLanguageMode()==null) || 
             (this.LOVLanguageMode!=null &&
              this.LOVLanguageMode.equals(other.getLOVLanguageMode()))) &&
            ((this.viewMode==null && other.getViewMode()==null) || 
             (this.viewMode!=null &&
              this.viewMode.equals(other.getViewMode()))) &&
            ((this.echo==null && other.getEcho()==null) || 
             (this.echo!=null &&
              this.echo.equals(other.getEcho())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getListOfCustomObject6() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListOfCustomObject6());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListOfCustomObject6(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLOVLanguageMode() != null) {
            _hashCode += getLOVLanguageMode().hashCode();
        }
        if (getViewMode() != null) {
            _hashCode += getViewMode().hashCode();
        }
        if (getEcho() != null) {
            _hashCode += getEcho().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomObject6Execute_Input.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Execute_Input"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listOfCustomObject6");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ListOfCustomObject6"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ListOfCustomObject6Data"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOVLanguageMode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "LOVLanguageMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viewMode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "ViewMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("echo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "Echo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
