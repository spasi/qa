package com.cydcor.ws.stubs.privillege;
/**
 * CustomObject6Query.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class CustomObject6Query  implements java.io.Serializable {
    private QueryType modifiedDate;

    private QueryType createdDate;

    private QueryType modifiedById;

    private QueryType createdById;

    private QueryType modId;

    private QueryType id;

    private QueryType name;

    private QueryType quickSearch1;

    private QueryType quickSearch2;

    private QueryType type;

    private QueryType currencyCode;

    private QueryType exchangeDate;

    private QueryType externalSystemId;

    private QueryType integrationId;

    private QueryType indexedBoolean0;

    private QueryType indexedCurrency0;

    private QueryType indexedDate0;

    private QueryType indexedNumber0;

    private QueryType indexedPick0;

    private QueryType indexedPick1;

    private QueryType indexedPick2;

    private QueryType indexedPick3;

    private QueryType indexedPick4;

    private QueryType accountId;

    private QueryType activityId;

    private QueryType assetId;

    private QueryType campaignId;

    private QueryType contactId;

    private QueryType customObject1Id;

    private QueryType customObject2Id;

    private QueryType customObject3Id;

    private QueryType customObject4Id;

    private QueryType customObject5Id;

    private QueryType customObject6Id;

    private QueryType customObject7Id;

    private QueryType customObject8Id;

    private QueryType customObject9Id;

    private QueryType customObject10Id;

    private QueryType customObject11Id;

    private QueryType customObject12Id;

    private QueryType customObject13Id;

    private QueryType customObject14Id;

    private QueryType customObject15Id;

    private QueryType dealerId;

    private QueryType fundId;

    private QueryType fundRequestId;

    private QueryType householdId;

    private QueryType leadId;

    private QueryType medEdId;

    private QueryType opportunityId;

    private QueryType portfolioId;

    private QueryType productId;

    private QueryType serviceRequestId;

    private QueryType solutionId;

    private QueryType vehicleId;

    private QueryType ownerId;

    private QueryType updatedByAlias;

    private QueryType updatedByEMailAddr;

    private QueryType updatedByExternalSystemId;

    private QueryType updatedByFirstName;

    private QueryType updatedByFullName;

    private QueryType updatedByIntegrationId;

    private QueryType updatedByLastName;

    private QueryType updatedByUserSignInId;

    private QueryType createdByAlias;

    private QueryType createdByEMailAddr;

    private QueryType createdByExternalSystemId;

    private QueryType createdByFirstName;

    private QueryType createdByFullName;

    private QueryType createdByIntegrationId;

    private QueryType createdByLastName;

    private QueryType createdByUserSignInId;

    private QueryType accountExternalSystemId;

    private QueryType accountIntegrationId;

    private QueryType accountLocation;

    private QueryType accountName;

    private QueryType activitySubject;

    private QueryType activityExternalSystemId;

    private QueryType activityIntegrationId;

    private QueryType assetIntegrationId;

    private QueryType assetProduct;

    private QueryType assetSerialNumber;

    private QueryType assetExternalSystemId;

    private QueryType campaignExternalSystemId;

    private QueryType campaignIntegrationId;

    private QueryType campaignName;

    private QueryType contactEmail;

    private QueryType contactExternalSystemId;

    private QueryType contactFirstName;

    private QueryType contactFullName;

    private QueryType contactIntegrationId;

    private QueryType contactLastName;

    private QueryType customObject1ExternalSystemId;

    private QueryType customObject1IntegrationId;

    private QueryType customObject1Name;

    private QueryType customObject2ExternalSystemId;

    private QueryType customObject2IntegrationId;

    private QueryType customObject2Name;

    private QueryType customObject3IntegrationId;

    private QueryType customObject3ExternalSystemId;

    private QueryType customObject3Name;

    private QueryType customObject4Name;

    private QueryType customObject4ExternalSystemId;

    private QueryType customObject4IntegrationId;

    private QueryType customObject5Name;

    private QueryType customObject5ExternalSystemId;

    private QueryType customObject5IntegrationId;

    private QueryType customObject6Name;

    private QueryType customObject6ExternalSystemId;

    private QueryType customObject6IntegrationId;

    private QueryType customObject7Name;

    private QueryType customObject7ExternalSystemId;

    private QueryType customObject7IntegrationId;

    private QueryType customObject8Name;

    private QueryType customObject8ExternalSystemId;

    private QueryType customObject8IntegrationId;

    private QueryType customObject9Name;

    private QueryType customObject9ExternalSystemId;

    private QueryType customObject9IntegrationId;

    private QueryType customObject10Name;

    private QueryType customObject10ExternalSystemId;

    private QueryType customObject10IntegrationId;

    private QueryType customObject11Name;

    private QueryType customObject11ExternalSystemId;

    private QueryType customObject11IntegrationId;

    private QueryType customObject12Name;

    private QueryType customObject12ExternalSystemId;

    private QueryType customObject12IntegrationId;

    private QueryType customObject13Name;

    private QueryType customObject13ExternalSystemId;

    private QueryType customObject13IntegrationId;

    private QueryType customObject14Name;

    private QueryType customObject14ExternalSystemId;

    private QueryType customObject14IntegrationId;

    private QueryType customObject15Name;

    private QueryType customObject15ExternalSystemId;

    private QueryType customObject15IntegrationId;

    private QueryType dealerName;

    private QueryType dealerExternalSystemId;

    private QueryType dealerIntegrationId;

    private QueryType fundName;

    private QueryType fundRequestName;

    private QueryType fundRequestExternalSystemId;

    private QueryType householdName;

    private QueryType householdIntegrationId;

    private QueryType householdExternalSystemId;

    private QueryType leadExternalSystemId;

    private QueryType leadFirstName;

    private QueryType leadFullName;

    private QueryType leadIntegrationId;

    private QueryType leadLastName;

    private QueryType medEdName;

    private QueryType medEdExternalSystemId;

    private QueryType medEdIntegrationId;

    private QueryType opportunityAccountId;

    private QueryType opportunityExternalSystemId;

    private QueryType opportunityIntegrationId;

    private QueryType opportunityName;

    private QueryType portfolioAccountNumber;

    private QueryType portfolioExternalSystemId;

    private QueryType portfolioIntegrationId;

    private QueryType productIntegrationId;

    private QueryType productName;

    private QueryType productExternalSystemId;

    private QueryType serviceRequestExternalSystemId;

    private QueryType serviceRequestIntegrationId;

    private QueryType serviceRequestSRNumber;

    private QueryType solutionExternalSystemId;

    private QueryType solutionIntegrationId;

    private QueryType solutionTitle;

    private QueryType vehicleIntegrationId;

    private QueryType vehicleVIN;

    private QueryType vehicleExternalSystemId;

    private QueryType ownerAlias;

    private QueryType ownerEMailAddr;

    private QueryType ownerExternalSystemId;

    private QueryType ownerFirstName;

    private QueryType ownerFullName;

    private QueryType ownerIntegrationId;

    private QueryType ownerLastName;

    private QueryType ownerUserSignInId;

    private QueryType createdBy;

    private QueryType modifiedBy;

    private QueryType description;

    private java.lang.String searchspec;  // attribute

    public CustomObject6Query() {
    }

    public CustomObject6Query(
           QueryType modifiedDate,
           QueryType createdDate,
           QueryType modifiedById,
           QueryType createdById,
           QueryType modId,
           QueryType id,
           QueryType name,
           QueryType quickSearch1,
           QueryType quickSearch2,
           QueryType type,
           QueryType currencyCode,
           QueryType exchangeDate,
           QueryType externalSystemId,
           QueryType integrationId,
           QueryType indexedBoolean0,
           QueryType indexedCurrency0,
           QueryType indexedDate0,
           QueryType indexedNumber0,
           QueryType indexedPick0,
           QueryType indexedPick1,
           QueryType indexedPick2,
           QueryType indexedPick3,
           QueryType indexedPick4,
           QueryType accountId,
           QueryType activityId,
           QueryType assetId,
           QueryType campaignId,
           QueryType contactId,
           QueryType customObject1Id,
           QueryType customObject2Id,
           QueryType customObject3Id,
           QueryType customObject4Id,
           QueryType customObject5Id,
           QueryType customObject6Id,
           QueryType customObject7Id,
           QueryType customObject8Id,
           QueryType customObject9Id,
           QueryType customObject10Id,
           QueryType customObject11Id,
           QueryType customObject12Id,
           QueryType customObject13Id,
           QueryType customObject14Id,
           QueryType customObject15Id,
           QueryType dealerId,
           QueryType fundId,
           QueryType fundRequestId,
           QueryType householdId,
           QueryType leadId,
           QueryType medEdId,
           QueryType opportunityId,
           QueryType portfolioId,
           QueryType productId,
           QueryType serviceRequestId,
           QueryType solutionId,
           QueryType vehicleId,
           QueryType ownerId,
           QueryType updatedByAlias,
           QueryType updatedByEMailAddr,
           QueryType updatedByExternalSystemId,
           QueryType updatedByFirstName,
           QueryType updatedByFullName,
           QueryType updatedByIntegrationId,
           QueryType updatedByLastName,
           QueryType updatedByUserSignInId,
           QueryType createdByAlias,
           QueryType createdByEMailAddr,
           QueryType createdByExternalSystemId,
           QueryType createdByFirstName,
           QueryType createdByFullName,
           QueryType createdByIntegrationId,
           QueryType createdByLastName,
           QueryType createdByUserSignInId,
           QueryType accountExternalSystemId,
           QueryType accountIntegrationId,
           QueryType accountLocation,
           QueryType accountName,
           QueryType activitySubject,
           QueryType activityExternalSystemId,
           QueryType activityIntegrationId,
           QueryType assetIntegrationId,
           QueryType assetProduct,
           QueryType assetSerialNumber,
           QueryType assetExternalSystemId,
           QueryType campaignExternalSystemId,
           QueryType campaignIntegrationId,
           QueryType campaignName,
           QueryType contactEmail,
           QueryType contactExternalSystemId,
           QueryType contactFirstName,
           QueryType contactFullName,
           QueryType contactIntegrationId,
           QueryType contactLastName,
           QueryType customObject1ExternalSystemId,
           QueryType customObject1IntegrationId,
           QueryType customObject1Name,
           QueryType customObject2ExternalSystemId,
           QueryType customObject2IntegrationId,
           QueryType customObject2Name,
           QueryType customObject3IntegrationId,
           QueryType customObject3ExternalSystemId,
           QueryType customObject3Name,
           QueryType customObject4Name,
           QueryType customObject4ExternalSystemId,
           QueryType customObject4IntegrationId,
           QueryType customObject5Name,
           QueryType customObject5ExternalSystemId,
           QueryType customObject5IntegrationId,
           QueryType customObject6Name,
           QueryType customObject6ExternalSystemId,
           QueryType customObject6IntegrationId,
           QueryType customObject7Name,
           QueryType customObject7ExternalSystemId,
           QueryType customObject7IntegrationId,
           QueryType customObject8Name,
           QueryType customObject8ExternalSystemId,
           QueryType customObject8IntegrationId,
           QueryType customObject9Name,
           QueryType customObject9ExternalSystemId,
           QueryType customObject9IntegrationId,
           QueryType customObject10Name,
           QueryType customObject10ExternalSystemId,
           QueryType customObject10IntegrationId,
           QueryType customObject11Name,
           QueryType customObject11ExternalSystemId,
           QueryType customObject11IntegrationId,
           QueryType customObject12Name,
           QueryType customObject12ExternalSystemId,
           QueryType customObject12IntegrationId,
           QueryType customObject13Name,
           QueryType customObject13ExternalSystemId,
           QueryType customObject13IntegrationId,
           QueryType customObject14Name,
           QueryType customObject14ExternalSystemId,
           QueryType customObject14IntegrationId,
           QueryType customObject15Name,
           QueryType customObject15ExternalSystemId,
           QueryType customObject15IntegrationId,
           QueryType dealerName,
           QueryType dealerExternalSystemId,
           QueryType dealerIntegrationId,
           QueryType fundName,
           QueryType fundRequestName,
           QueryType fundRequestExternalSystemId,
           QueryType householdName,
           QueryType householdIntegrationId,
           QueryType householdExternalSystemId,
           QueryType leadExternalSystemId,
           QueryType leadFirstName,
           QueryType leadFullName,
           QueryType leadIntegrationId,
           QueryType leadLastName,
           QueryType medEdName,
           QueryType medEdExternalSystemId,
           QueryType medEdIntegrationId,
           QueryType opportunityAccountId,
           QueryType opportunityExternalSystemId,
           QueryType opportunityIntegrationId,
           QueryType opportunityName,
           QueryType portfolioAccountNumber,
           QueryType portfolioExternalSystemId,
           QueryType portfolioIntegrationId,
           QueryType productIntegrationId,
           QueryType productName,
           QueryType productExternalSystemId,
           QueryType serviceRequestExternalSystemId,
           QueryType serviceRequestIntegrationId,
           QueryType serviceRequestSRNumber,
           QueryType solutionExternalSystemId,
           QueryType solutionIntegrationId,
           QueryType solutionTitle,
           QueryType vehicleIntegrationId,
           QueryType vehicleVIN,
           QueryType vehicleExternalSystemId,
           QueryType ownerAlias,
           QueryType ownerEMailAddr,
           QueryType ownerExternalSystemId,
           QueryType ownerFirstName,
           QueryType ownerFullName,
           QueryType ownerIntegrationId,
           QueryType ownerLastName,
           QueryType ownerUserSignInId,
           QueryType createdBy,
           QueryType modifiedBy,
           QueryType description,
           java.lang.String searchspec) {
           this.modifiedDate = modifiedDate;
           this.createdDate = createdDate;
           this.modifiedById = modifiedById;
           this.createdById = createdById;
           this.modId = modId;
           this.id = id;
           this.name = name;
           this.quickSearch1 = quickSearch1;
           this.quickSearch2 = quickSearch2;
           this.type = type;
           this.currencyCode = currencyCode;
           this.exchangeDate = exchangeDate;
           this.externalSystemId = externalSystemId;
           this.integrationId = integrationId;
           this.indexedBoolean0 = indexedBoolean0;
           this.indexedCurrency0 = indexedCurrency0;
           this.indexedDate0 = indexedDate0;
           this.indexedNumber0 = indexedNumber0;
           this.indexedPick0 = indexedPick0;
           this.indexedPick1 = indexedPick1;
           this.indexedPick2 = indexedPick2;
           this.indexedPick3 = indexedPick3;
           this.indexedPick4 = indexedPick4;
           this.accountId = accountId;
           this.activityId = activityId;
           this.assetId = assetId;
           this.campaignId = campaignId;
           this.contactId = contactId;
           this.customObject1Id = customObject1Id;
           this.customObject2Id = customObject2Id;
           this.customObject3Id = customObject3Id;
           this.customObject4Id = customObject4Id;
           this.customObject5Id = customObject5Id;
           this.customObject6Id = customObject6Id;
           this.customObject7Id = customObject7Id;
           this.customObject8Id = customObject8Id;
           this.customObject9Id = customObject9Id;
           this.customObject10Id = customObject10Id;
           this.customObject11Id = customObject11Id;
           this.customObject12Id = customObject12Id;
           this.customObject13Id = customObject13Id;
           this.customObject14Id = customObject14Id;
           this.customObject15Id = customObject15Id;
           this.dealerId = dealerId;
           this.fundId = fundId;
           this.fundRequestId = fundRequestId;
           this.householdId = householdId;
           this.leadId = leadId;
           this.medEdId = medEdId;
           this.opportunityId = opportunityId;
           this.portfolioId = portfolioId;
           this.productId = productId;
           this.serviceRequestId = serviceRequestId;
           this.solutionId = solutionId;
           this.vehicleId = vehicleId;
           this.ownerId = ownerId;
           this.updatedByAlias = updatedByAlias;
           this.updatedByEMailAddr = updatedByEMailAddr;
           this.updatedByExternalSystemId = updatedByExternalSystemId;
           this.updatedByFirstName = updatedByFirstName;
           this.updatedByFullName = updatedByFullName;
           this.updatedByIntegrationId = updatedByIntegrationId;
           this.updatedByLastName = updatedByLastName;
           this.updatedByUserSignInId = updatedByUserSignInId;
           this.createdByAlias = createdByAlias;
           this.createdByEMailAddr = createdByEMailAddr;
           this.createdByExternalSystemId = createdByExternalSystemId;
           this.createdByFirstName = createdByFirstName;
           this.createdByFullName = createdByFullName;
           this.createdByIntegrationId = createdByIntegrationId;
           this.createdByLastName = createdByLastName;
           this.createdByUserSignInId = createdByUserSignInId;
           this.accountExternalSystemId = accountExternalSystemId;
           this.accountIntegrationId = accountIntegrationId;
           this.accountLocation = accountLocation;
           this.accountName = accountName;
           this.activitySubject = activitySubject;
           this.activityExternalSystemId = activityExternalSystemId;
           this.activityIntegrationId = activityIntegrationId;
           this.assetIntegrationId = assetIntegrationId;
           this.assetProduct = assetProduct;
           this.assetSerialNumber = assetSerialNumber;
           this.assetExternalSystemId = assetExternalSystemId;
           this.campaignExternalSystemId = campaignExternalSystemId;
           this.campaignIntegrationId = campaignIntegrationId;
           this.campaignName = campaignName;
           this.contactEmail = contactEmail;
           this.contactExternalSystemId = contactExternalSystemId;
           this.contactFirstName = contactFirstName;
           this.contactFullName = contactFullName;
           this.contactIntegrationId = contactIntegrationId;
           this.contactLastName = contactLastName;
           this.customObject1ExternalSystemId = customObject1ExternalSystemId;
           this.customObject1IntegrationId = customObject1IntegrationId;
           this.customObject1Name = customObject1Name;
           this.customObject2ExternalSystemId = customObject2ExternalSystemId;
           this.customObject2IntegrationId = customObject2IntegrationId;
           this.customObject2Name = customObject2Name;
           this.customObject3IntegrationId = customObject3IntegrationId;
           this.customObject3ExternalSystemId = customObject3ExternalSystemId;
           this.customObject3Name = customObject3Name;
           this.customObject4Name = customObject4Name;
           this.customObject4ExternalSystemId = customObject4ExternalSystemId;
           this.customObject4IntegrationId = customObject4IntegrationId;
           this.customObject5Name = customObject5Name;
           this.customObject5ExternalSystemId = customObject5ExternalSystemId;
           this.customObject5IntegrationId = customObject5IntegrationId;
           this.customObject6Name = customObject6Name;
           this.customObject6ExternalSystemId = customObject6ExternalSystemId;
           this.customObject6IntegrationId = customObject6IntegrationId;
           this.customObject7Name = customObject7Name;
           this.customObject7ExternalSystemId = customObject7ExternalSystemId;
           this.customObject7IntegrationId = customObject7IntegrationId;
           this.customObject8Name = customObject8Name;
           this.customObject8ExternalSystemId = customObject8ExternalSystemId;
           this.customObject8IntegrationId = customObject8IntegrationId;
           this.customObject9Name = customObject9Name;
           this.customObject9ExternalSystemId = customObject9ExternalSystemId;
           this.customObject9IntegrationId = customObject9IntegrationId;
           this.customObject10Name = customObject10Name;
           this.customObject10ExternalSystemId = customObject10ExternalSystemId;
           this.customObject10IntegrationId = customObject10IntegrationId;
           this.customObject11Name = customObject11Name;
           this.customObject11ExternalSystemId = customObject11ExternalSystemId;
           this.customObject11IntegrationId = customObject11IntegrationId;
           this.customObject12Name = customObject12Name;
           this.customObject12ExternalSystemId = customObject12ExternalSystemId;
           this.customObject12IntegrationId = customObject12IntegrationId;
           this.customObject13Name = customObject13Name;
           this.customObject13ExternalSystemId = customObject13ExternalSystemId;
           this.customObject13IntegrationId = customObject13IntegrationId;
           this.customObject14Name = customObject14Name;
           this.customObject14ExternalSystemId = customObject14ExternalSystemId;
           this.customObject14IntegrationId = customObject14IntegrationId;
           this.customObject15Name = customObject15Name;
           this.customObject15ExternalSystemId = customObject15ExternalSystemId;
           this.customObject15IntegrationId = customObject15IntegrationId;
           this.dealerName = dealerName;
           this.dealerExternalSystemId = dealerExternalSystemId;
           this.dealerIntegrationId = dealerIntegrationId;
           this.fundName = fundName;
           this.fundRequestName = fundRequestName;
           this.fundRequestExternalSystemId = fundRequestExternalSystemId;
           this.householdName = householdName;
           this.householdIntegrationId = householdIntegrationId;
           this.householdExternalSystemId = householdExternalSystemId;
           this.leadExternalSystemId = leadExternalSystemId;
           this.leadFirstName = leadFirstName;
           this.leadFullName = leadFullName;
           this.leadIntegrationId = leadIntegrationId;
           this.leadLastName = leadLastName;
           this.medEdName = medEdName;
           this.medEdExternalSystemId = medEdExternalSystemId;
           this.medEdIntegrationId = medEdIntegrationId;
           this.opportunityAccountId = opportunityAccountId;
           this.opportunityExternalSystemId = opportunityExternalSystemId;
           this.opportunityIntegrationId = opportunityIntegrationId;
           this.opportunityName = opportunityName;
           this.portfolioAccountNumber = portfolioAccountNumber;
           this.portfolioExternalSystemId = portfolioExternalSystemId;
           this.portfolioIntegrationId = portfolioIntegrationId;
           this.productIntegrationId = productIntegrationId;
           this.productName = productName;
           this.productExternalSystemId = productExternalSystemId;
           this.serviceRequestExternalSystemId = serviceRequestExternalSystemId;
           this.serviceRequestIntegrationId = serviceRequestIntegrationId;
           this.serviceRequestSRNumber = serviceRequestSRNumber;
           this.solutionExternalSystemId = solutionExternalSystemId;
           this.solutionIntegrationId = solutionIntegrationId;
           this.solutionTitle = solutionTitle;
           this.vehicleIntegrationId = vehicleIntegrationId;
           this.vehicleVIN = vehicleVIN;
           this.vehicleExternalSystemId = vehicleExternalSystemId;
           this.ownerAlias = ownerAlias;
           this.ownerEMailAddr = ownerEMailAddr;
           this.ownerExternalSystemId = ownerExternalSystemId;
           this.ownerFirstName = ownerFirstName;
           this.ownerFullName = ownerFullName;
           this.ownerIntegrationId = ownerIntegrationId;
           this.ownerLastName = ownerLastName;
           this.ownerUserSignInId = ownerUserSignInId;
           this.createdBy = createdBy;
           this.modifiedBy = modifiedBy;
           this.description = description;
           this.searchspec = searchspec;
    }


    /**
     * Gets the modifiedDate value for this CustomObject6Query.
     * 
     * @return modifiedDate
     */
    public QueryType getModifiedDate() {
        return modifiedDate;
    }


    /**
     * Sets the modifiedDate value for this CustomObject6Query.
     * 
     * @param modifiedDate
     */
    public void setModifiedDate(QueryType modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    /**
     * Gets the createdDate value for this CustomObject6Query.
     * 
     * @return createdDate
     */
    public QueryType getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this CustomObject6Query.
     * 
     * @param createdDate
     */
    public void setCreatedDate(QueryType createdDate) {
        this.createdDate = createdDate;
    }


    /**
     * Gets the modifiedById value for this CustomObject6Query.
     * 
     * @return modifiedById
     */
    public QueryType getModifiedById() {
        return modifiedById;
    }


    /**
     * Sets the modifiedById value for this CustomObject6Query.
     * 
     * @param modifiedById
     */
    public void setModifiedById(QueryType modifiedById) {
        this.modifiedById = modifiedById;
    }


    /**
     * Gets the createdById value for this CustomObject6Query.
     * 
     * @return createdById
     */
    public QueryType getCreatedById() {
        return createdById;
    }


    /**
     * Sets the createdById value for this CustomObject6Query.
     * 
     * @param createdById
     */
    public void setCreatedById(QueryType createdById) {
        this.createdById = createdById;
    }


    /**
     * Gets the modId value for this CustomObject6Query.
     * 
     * @return modId
     */
    public QueryType getModId() {
        return modId;
    }


    /**
     * Sets the modId value for this CustomObject6Query.
     * 
     * @param modId
     */
    public void setModId(QueryType modId) {
        this.modId = modId;
    }


    /**
     * Gets the id value for this CustomObject6Query.
     * 
     * @return id
     */
    public QueryType getId() {
        return id;
    }


    /**
     * Sets the id value for this CustomObject6Query.
     * 
     * @param id
     */
    public void setId(QueryType id) {
        this.id = id;
    }


    /**
     * Gets the name value for this CustomObject6Query.
     * 
     * @return name
     */
    public QueryType getName() {
        return name;
    }


    /**
     * Sets the name value for this CustomObject6Query.
     * 
     * @param name
     */
    public void setName(QueryType name) {
        this.name = name;
    }


    /**
     * Gets the quickSearch1 value for this CustomObject6Query.
     * 
     * @return quickSearch1
     */
    public QueryType getQuickSearch1() {
        return quickSearch1;
    }


    /**
     * Sets the quickSearch1 value for this CustomObject6Query.
     * 
     * @param quickSearch1
     */
    public void setQuickSearch1(QueryType quickSearch1) {
        this.quickSearch1 = quickSearch1;
    }


    /**
     * Gets the quickSearch2 value for this CustomObject6Query.
     * 
     * @return quickSearch2
     */
    public QueryType getQuickSearch2() {
        return quickSearch2;
    }


    /**
     * Sets the quickSearch2 value for this CustomObject6Query.
     * 
     * @param quickSearch2
     */
    public void setQuickSearch2(QueryType quickSearch2) {
        this.quickSearch2 = quickSearch2;
    }


    /**
     * Gets the type value for this CustomObject6Query.
     * 
     * @return type
     */
    public QueryType getType() {
        return type;
    }


    /**
     * Sets the type value for this CustomObject6Query.
     * 
     * @param type
     */
    public void setType(QueryType type) {
        this.type = type;
    }


    /**
     * Gets the currencyCode value for this CustomObject6Query.
     * 
     * @return currencyCode
     */
    public QueryType getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this CustomObject6Query.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(QueryType currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the exchangeDate value for this CustomObject6Query.
     * 
     * @return exchangeDate
     */
    public QueryType getExchangeDate() {
        return exchangeDate;
    }


    /**
     * Sets the exchangeDate value for this CustomObject6Query.
     * 
     * @param exchangeDate
     */
    public void setExchangeDate(QueryType exchangeDate) {
        this.exchangeDate = exchangeDate;
    }


    /**
     * Gets the externalSystemId value for this CustomObject6Query.
     * 
     * @return externalSystemId
     */
    public QueryType getExternalSystemId() {
        return externalSystemId;
    }


    /**
     * Sets the externalSystemId value for this CustomObject6Query.
     * 
     * @param externalSystemId
     */
    public void setExternalSystemId(QueryType externalSystemId) {
        this.externalSystemId = externalSystemId;
    }


    /**
     * Gets the integrationId value for this CustomObject6Query.
     * 
     * @return integrationId
     */
    public QueryType getIntegrationId() {
        return integrationId;
    }


    /**
     * Sets the integrationId value for this CustomObject6Query.
     * 
     * @param integrationId
     */
    public void setIntegrationId(QueryType integrationId) {
        this.integrationId = integrationId;
    }


    /**
     * Gets the indexedBoolean0 value for this CustomObject6Query.
     * 
     * @return indexedBoolean0
     */
    public QueryType getIndexedBoolean0() {
        return indexedBoolean0;
    }


    /**
     * Sets the indexedBoolean0 value for this CustomObject6Query.
     * 
     * @param indexedBoolean0
     */
    public void setIndexedBoolean0(QueryType indexedBoolean0) {
        this.indexedBoolean0 = indexedBoolean0;
    }


    /**
     * Gets the indexedCurrency0 value for this CustomObject6Query.
     * 
     * @return indexedCurrency0
     */
    public QueryType getIndexedCurrency0() {
        return indexedCurrency0;
    }


    /**
     * Sets the indexedCurrency0 value for this CustomObject6Query.
     * 
     * @param indexedCurrency0
     */
    public void setIndexedCurrency0(QueryType indexedCurrency0) {
        this.indexedCurrency0 = indexedCurrency0;
    }


    /**
     * Gets the indexedDate0 value for this CustomObject6Query.
     * 
     * @return indexedDate0
     */
    public QueryType getIndexedDate0() {
        return indexedDate0;
    }


    /**
     * Sets the indexedDate0 value for this CustomObject6Query.
     * 
     * @param indexedDate0
     */
    public void setIndexedDate0(QueryType indexedDate0) {
        this.indexedDate0 = indexedDate0;
    }


    /**
     * Gets the indexedNumber0 value for this CustomObject6Query.
     * 
     * @return indexedNumber0
     */
    public QueryType getIndexedNumber0() {
        return indexedNumber0;
    }


    /**
     * Sets the indexedNumber0 value for this CustomObject6Query.
     * 
     * @param indexedNumber0
     */
    public void setIndexedNumber0(QueryType indexedNumber0) {
        this.indexedNumber0 = indexedNumber0;
    }


    /**
     * Gets the indexedPick0 value for this CustomObject6Query.
     * 
     * @return indexedPick0
     */
    public QueryType getIndexedPick0() {
        return indexedPick0;
    }


    /**
     * Sets the indexedPick0 value for this CustomObject6Query.
     * 
     * @param indexedPick0
     */
    public void setIndexedPick0(QueryType indexedPick0) {
        this.indexedPick0 = indexedPick0;
    }


    /**
     * Gets the indexedPick1 value for this CustomObject6Query.
     * 
     * @return indexedPick1
     */
    public QueryType getIndexedPick1() {
        return indexedPick1;
    }


    /**
     * Sets the indexedPick1 value for this CustomObject6Query.
     * 
     * @param indexedPick1
     */
    public void setIndexedPick1(QueryType indexedPick1) {
        this.indexedPick1 = indexedPick1;
    }


    /**
     * Gets the indexedPick2 value for this CustomObject6Query.
     * 
     * @return indexedPick2
     */
    public QueryType getIndexedPick2() {
        return indexedPick2;
    }


    /**
     * Sets the indexedPick2 value for this CustomObject6Query.
     * 
     * @param indexedPick2
     */
    public void setIndexedPick2(QueryType indexedPick2) {
        this.indexedPick2 = indexedPick2;
    }


    /**
     * Gets the indexedPick3 value for this CustomObject6Query.
     * 
     * @return indexedPick3
     */
    public QueryType getIndexedPick3() {
        return indexedPick3;
    }


    /**
     * Sets the indexedPick3 value for this CustomObject6Query.
     * 
     * @param indexedPick3
     */
    public void setIndexedPick3(QueryType indexedPick3) {
        this.indexedPick3 = indexedPick3;
    }


    /**
     * Gets the indexedPick4 value for this CustomObject6Query.
     * 
     * @return indexedPick4
     */
    public QueryType getIndexedPick4() {
        return indexedPick4;
    }


    /**
     * Sets the indexedPick4 value for this CustomObject6Query.
     * 
     * @param indexedPick4
     */
    public void setIndexedPick4(QueryType indexedPick4) {
        this.indexedPick4 = indexedPick4;
    }


    /**
     * Gets the accountId value for this CustomObject6Query.
     * 
     * @return accountId
     */
    public QueryType getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this CustomObject6Query.
     * 
     * @param accountId
     */
    public void setAccountId(QueryType accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the activityId value for this CustomObject6Query.
     * 
     * @return activityId
     */
    public QueryType getActivityId() {
        return activityId;
    }


    /**
     * Sets the activityId value for this CustomObject6Query.
     * 
     * @param activityId
     */
    public void setActivityId(QueryType activityId) {
        this.activityId = activityId;
    }


    /**
     * Gets the assetId value for this CustomObject6Query.
     * 
     * @return assetId
     */
    public QueryType getAssetId() {
        return assetId;
    }


    /**
     * Sets the assetId value for this CustomObject6Query.
     * 
     * @param assetId
     */
    public void setAssetId(QueryType assetId) {
        this.assetId = assetId;
    }


    /**
     * Gets the campaignId value for this CustomObject6Query.
     * 
     * @return campaignId
     */
    public QueryType getCampaignId() {
        return campaignId;
    }


    /**
     * Sets the campaignId value for this CustomObject6Query.
     * 
     * @param campaignId
     */
    public void setCampaignId(QueryType campaignId) {
        this.campaignId = campaignId;
    }


    /**
     * Gets the contactId value for this CustomObject6Query.
     * 
     * @return contactId
     */
    public QueryType getContactId() {
        return contactId;
    }


    /**
     * Sets the contactId value for this CustomObject6Query.
     * 
     * @param contactId
     */
    public void setContactId(QueryType contactId) {
        this.contactId = contactId;
    }


    /**
     * Gets the customObject1Id value for this CustomObject6Query.
     * 
     * @return customObject1Id
     */
    public QueryType getCustomObject1Id() {
        return customObject1Id;
    }


    /**
     * Sets the customObject1Id value for this CustomObject6Query.
     * 
     * @param customObject1Id
     */
    public void setCustomObject1Id(QueryType customObject1Id) {
        this.customObject1Id = customObject1Id;
    }


    /**
     * Gets the customObject2Id value for this CustomObject6Query.
     * 
     * @return customObject2Id
     */
    public QueryType getCustomObject2Id() {
        return customObject2Id;
    }


    /**
     * Sets the customObject2Id value for this CustomObject6Query.
     * 
     * @param customObject2Id
     */
    public void setCustomObject2Id(QueryType customObject2Id) {
        this.customObject2Id = customObject2Id;
    }


    /**
     * Gets the customObject3Id value for this CustomObject6Query.
     * 
     * @return customObject3Id
     */
    public QueryType getCustomObject3Id() {
        return customObject3Id;
    }


    /**
     * Sets the customObject3Id value for this CustomObject6Query.
     * 
     * @param customObject3Id
     */
    public void setCustomObject3Id(QueryType customObject3Id) {
        this.customObject3Id = customObject3Id;
    }


    /**
     * Gets the customObject4Id value for this CustomObject6Query.
     * 
     * @return customObject4Id
     */
    public QueryType getCustomObject4Id() {
        return customObject4Id;
    }


    /**
     * Sets the customObject4Id value for this CustomObject6Query.
     * 
     * @param customObject4Id
     */
    public void setCustomObject4Id(QueryType customObject4Id) {
        this.customObject4Id = customObject4Id;
    }


    /**
     * Gets the customObject5Id value for this CustomObject6Query.
     * 
     * @return customObject5Id
     */
    public QueryType getCustomObject5Id() {
        return customObject5Id;
    }


    /**
     * Sets the customObject5Id value for this CustomObject6Query.
     * 
     * @param customObject5Id
     */
    public void setCustomObject5Id(QueryType customObject5Id) {
        this.customObject5Id = customObject5Id;
    }


    /**
     * Gets the customObject6Id value for this CustomObject6Query.
     * 
     * @return customObject6Id
     */
    public QueryType getCustomObject6Id() {
        return customObject6Id;
    }


    /**
     * Sets the customObject6Id value for this CustomObject6Query.
     * 
     * @param customObject6Id
     */
    public void setCustomObject6Id(QueryType customObject6Id) {
        this.customObject6Id = customObject6Id;
    }


    /**
     * Gets the customObject7Id value for this CustomObject6Query.
     * 
     * @return customObject7Id
     */
    public QueryType getCustomObject7Id() {
        return customObject7Id;
    }


    /**
     * Sets the customObject7Id value for this CustomObject6Query.
     * 
     * @param customObject7Id
     */
    public void setCustomObject7Id(QueryType customObject7Id) {
        this.customObject7Id = customObject7Id;
    }


    /**
     * Gets the customObject8Id value for this CustomObject6Query.
     * 
     * @return customObject8Id
     */
    public QueryType getCustomObject8Id() {
        return customObject8Id;
    }


    /**
     * Sets the customObject8Id value for this CustomObject6Query.
     * 
     * @param customObject8Id
     */
    public void setCustomObject8Id(QueryType customObject8Id) {
        this.customObject8Id = customObject8Id;
    }


    /**
     * Gets the customObject9Id value for this CustomObject6Query.
     * 
     * @return customObject9Id
     */
    public QueryType getCustomObject9Id() {
        return customObject9Id;
    }


    /**
     * Sets the customObject9Id value for this CustomObject6Query.
     * 
     * @param customObject9Id
     */
    public void setCustomObject9Id(QueryType customObject9Id) {
        this.customObject9Id = customObject9Id;
    }


    /**
     * Gets the customObject10Id value for this CustomObject6Query.
     * 
     * @return customObject10Id
     */
    public QueryType getCustomObject10Id() {
        return customObject10Id;
    }


    /**
     * Sets the customObject10Id value for this CustomObject6Query.
     * 
     * @param customObject10Id
     */
    public void setCustomObject10Id(QueryType customObject10Id) {
        this.customObject10Id = customObject10Id;
    }


    /**
     * Gets the customObject11Id value for this CustomObject6Query.
     * 
     * @return customObject11Id
     */
    public QueryType getCustomObject11Id() {
        return customObject11Id;
    }


    /**
     * Sets the customObject11Id value for this CustomObject6Query.
     * 
     * @param customObject11Id
     */
    public void setCustomObject11Id(QueryType customObject11Id) {
        this.customObject11Id = customObject11Id;
    }


    /**
     * Gets the customObject12Id value for this CustomObject6Query.
     * 
     * @return customObject12Id
     */
    public QueryType getCustomObject12Id() {
        return customObject12Id;
    }


    /**
     * Sets the customObject12Id value for this CustomObject6Query.
     * 
     * @param customObject12Id
     */
    public void setCustomObject12Id(QueryType customObject12Id) {
        this.customObject12Id = customObject12Id;
    }


    /**
     * Gets the customObject13Id value for this CustomObject6Query.
     * 
     * @return customObject13Id
     */
    public QueryType getCustomObject13Id() {
        return customObject13Id;
    }


    /**
     * Sets the customObject13Id value for this CustomObject6Query.
     * 
     * @param customObject13Id
     */
    public void setCustomObject13Id(QueryType customObject13Id) {
        this.customObject13Id = customObject13Id;
    }


    /**
     * Gets the customObject14Id value for this CustomObject6Query.
     * 
     * @return customObject14Id
     */
    public QueryType getCustomObject14Id() {
        return customObject14Id;
    }


    /**
     * Sets the customObject14Id value for this CustomObject6Query.
     * 
     * @param customObject14Id
     */
    public void setCustomObject14Id(QueryType customObject14Id) {
        this.customObject14Id = customObject14Id;
    }


    /**
     * Gets the customObject15Id value for this CustomObject6Query.
     * 
     * @return customObject15Id
     */
    public QueryType getCustomObject15Id() {
        return customObject15Id;
    }


    /**
     * Sets the customObject15Id value for this CustomObject6Query.
     * 
     * @param customObject15Id
     */
    public void setCustomObject15Id(QueryType customObject15Id) {
        this.customObject15Id = customObject15Id;
    }


    /**
     * Gets the dealerId value for this CustomObject6Query.
     * 
     * @return dealerId
     */
    public QueryType getDealerId() {
        return dealerId;
    }


    /**
     * Sets the dealerId value for this CustomObject6Query.
     * 
     * @param dealerId
     */
    public void setDealerId(QueryType dealerId) {
        this.dealerId = dealerId;
    }


    /**
     * Gets the fundId value for this CustomObject6Query.
     * 
     * @return fundId
     */
    public QueryType getFundId() {
        return fundId;
    }


    /**
     * Sets the fundId value for this CustomObject6Query.
     * 
     * @param fundId
     */
    public void setFundId(QueryType fundId) {
        this.fundId = fundId;
    }


    /**
     * Gets the fundRequestId value for this CustomObject6Query.
     * 
     * @return fundRequestId
     */
    public QueryType getFundRequestId() {
        return fundRequestId;
    }


    /**
     * Sets the fundRequestId value for this CustomObject6Query.
     * 
     * @param fundRequestId
     */
    public void setFundRequestId(QueryType fundRequestId) {
        this.fundRequestId = fundRequestId;
    }


    /**
     * Gets the householdId value for this CustomObject6Query.
     * 
     * @return householdId
     */
    public QueryType getHouseholdId() {
        return householdId;
    }


    /**
     * Sets the householdId value for this CustomObject6Query.
     * 
     * @param householdId
     */
    public void setHouseholdId(QueryType householdId) {
        this.householdId = householdId;
    }


    /**
     * Gets the leadId value for this CustomObject6Query.
     * 
     * @return leadId
     */
    public QueryType getLeadId() {
        return leadId;
    }


    /**
     * Sets the leadId value for this CustomObject6Query.
     * 
     * @param leadId
     */
    public void setLeadId(QueryType leadId) {
        this.leadId = leadId;
    }


    /**
     * Gets the medEdId value for this CustomObject6Query.
     * 
     * @return medEdId
     */
    public QueryType getMedEdId() {
        return medEdId;
    }


    /**
     * Sets the medEdId value for this CustomObject6Query.
     * 
     * @param medEdId
     */
    public void setMedEdId(QueryType medEdId) {
        this.medEdId = medEdId;
    }


    /**
     * Gets the opportunityId value for this CustomObject6Query.
     * 
     * @return opportunityId
     */
    public QueryType getOpportunityId() {
        return opportunityId;
    }


    /**
     * Sets the opportunityId value for this CustomObject6Query.
     * 
     * @param opportunityId
     */
    public void setOpportunityId(QueryType opportunityId) {
        this.opportunityId = opportunityId;
    }


    /**
     * Gets the portfolioId value for this CustomObject6Query.
     * 
     * @return portfolioId
     */
    public QueryType getPortfolioId() {
        return portfolioId;
    }


    /**
     * Sets the portfolioId value for this CustomObject6Query.
     * 
     * @param portfolioId
     */
    public void setPortfolioId(QueryType portfolioId) {
        this.portfolioId = portfolioId;
    }


    /**
     * Gets the productId value for this CustomObject6Query.
     * 
     * @return productId
     */
    public QueryType getProductId() {
        return productId;
    }


    /**
     * Sets the productId value for this CustomObject6Query.
     * 
     * @param productId
     */
    public void setProductId(QueryType productId) {
        this.productId = productId;
    }


    /**
     * Gets the serviceRequestId value for this CustomObject6Query.
     * 
     * @return serviceRequestId
     */
    public QueryType getServiceRequestId() {
        return serviceRequestId;
    }


    /**
     * Sets the serviceRequestId value for this CustomObject6Query.
     * 
     * @param serviceRequestId
     */
    public void setServiceRequestId(QueryType serviceRequestId) {
        this.serviceRequestId = serviceRequestId;
    }


    /**
     * Gets the solutionId value for this CustomObject6Query.
     * 
     * @return solutionId
     */
    public QueryType getSolutionId() {
        return solutionId;
    }


    /**
     * Sets the solutionId value for this CustomObject6Query.
     * 
     * @param solutionId
     */
    public void setSolutionId(QueryType solutionId) {
        this.solutionId = solutionId;
    }


    /**
     * Gets the vehicleId value for this CustomObject6Query.
     * 
     * @return vehicleId
     */
    public QueryType getVehicleId() {
        return vehicleId;
    }


    /**
     * Sets the vehicleId value for this CustomObject6Query.
     * 
     * @param vehicleId
     */
    public void setVehicleId(QueryType vehicleId) {
        this.vehicleId = vehicleId;
    }


    /**
     * Gets the ownerId value for this CustomObject6Query.
     * 
     * @return ownerId
     */
    public QueryType getOwnerId() {
        return ownerId;
    }


    /**
     * Sets the ownerId value for this CustomObject6Query.
     * 
     * @param ownerId
     */
    public void setOwnerId(QueryType ownerId) {
        this.ownerId = ownerId;
    }


    /**
     * Gets the updatedByAlias value for this CustomObject6Query.
     * 
     * @return updatedByAlias
     */
    public QueryType getUpdatedByAlias() {
        return updatedByAlias;
    }


    /**
     * Sets the updatedByAlias value for this CustomObject6Query.
     * 
     * @param updatedByAlias
     */
    public void setUpdatedByAlias(QueryType updatedByAlias) {
        this.updatedByAlias = updatedByAlias;
    }


    /**
     * Gets the updatedByEMailAddr value for this CustomObject6Query.
     * 
     * @return updatedByEMailAddr
     */
    public QueryType getUpdatedByEMailAddr() {
        return updatedByEMailAddr;
    }


    /**
     * Sets the updatedByEMailAddr value for this CustomObject6Query.
     * 
     * @param updatedByEMailAddr
     */
    public void setUpdatedByEMailAddr(QueryType updatedByEMailAddr) {
        this.updatedByEMailAddr = updatedByEMailAddr;
    }


    /**
     * Gets the updatedByExternalSystemId value for this CustomObject6Query.
     * 
     * @return updatedByExternalSystemId
     */
    public QueryType getUpdatedByExternalSystemId() {
        return updatedByExternalSystemId;
    }


    /**
     * Sets the updatedByExternalSystemId value for this CustomObject6Query.
     * 
     * @param updatedByExternalSystemId
     */
    public void setUpdatedByExternalSystemId(QueryType updatedByExternalSystemId) {
        this.updatedByExternalSystemId = updatedByExternalSystemId;
    }


    /**
     * Gets the updatedByFirstName value for this CustomObject6Query.
     * 
     * @return updatedByFirstName
     */
    public QueryType getUpdatedByFirstName() {
        return updatedByFirstName;
    }


    /**
     * Sets the updatedByFirstName value for this CustomObject6Query.
     * 
     * @param updatedByFirstName
     */
    public void setUpdatedByFirstName(QueryType updatedByFirstName) {
        this.updatedByFirstName = updatedByFirstName;
    }


    /**
     * Gets the updatedByFullName value for this CustomObject6Query.
     * 
     * @return updatedByFullName
     */
    public QueryType getUpdatedByFullName() {
        return updatedByFullName;
    }


    /**
     * Sets the updatedByFullName value for this CustomObject6Query.
     * 
     * @param updatedByFullName
     */
    public void setUpdatedByFullName(QueryType updatedByFullName) {
        this.updatedByFullName = updatedByFullName;
    }


    /**
     * Gets the updatedByIntegrationId value for this CustomObject6Query.
     * 
     * @return updatedByIntegrationId
     */
    public QueryType getUpdatedByIntegrationId() {
        return updatedByIntegrationId;
    }


    /**
     * Sets the updatedByIntegrationId value for this CustomObject6Query.
     * 
     * @param updatedByIntegrationId
     */
    public void setUpdatedByIntegrationId(QueryType updatedByIntegrationId) {
        this.updatedByIntegrationId = updatedByIntegrationId;
    }


    /**
     * Gets the updatedByLastName value for this CustomObject6Query.
     * 
     * @return updatedByLastName
     */
    public QueryType getUpdatedByLastName() {
        return updatedByLastName;
    }


    /**
     * Sets the updatedByLastName value for this CustomObject6Query.
     * 
     * @param updatedByLastName
     */
    public void setUpdatedByLastName(QueryType updatedByLastName) {
        this.updatedByLastName = updatedByLastName;
    }


    /**
     * Gets the updatedByUserSignInId value for this CustomObject6Query.
     * 
     * @return updatedByUserSignInId
     */
    public QueryType getUpdatedByUserSignInId() {
        return updatedByUserSignInId;
    }


    /**
     * Sets the updatedByUserSignInId value for this CustomObject6Query.
     * 
     * @param updatedByUserSignInId
     */
    public void setUpdatedByUserSignInId(QueryType updatedByUserSignInId) {
        this.updatedByUserSignInId = updatedByUserSignInId;
    }


    /**
     * Gets the createdByAlias value for this CustomObject6Query.
     * 
     * @return createdByAlias
     */
    public QueryType getCreatedByAlias() {
        return createdByAlias;
    }


    /**
     * Sets the createdByAlias value for this CustomObject6Query.
     * 
     * @param createdByAlias
     */
    public void setCreatedByAlias(QueryType createdByAlias) {
        this.createdByAlias = createdByAlias;
    }


    /**
     * Gets the createdByEMailAddr value for this CustomObject6Query.
     * 
     * @return createdByEMailAddr
     */
    public QueryType getCreatedByEMailAddr() {
        return createdByEMailAddr;
    }


    /**
     * Sets the createdByEMailAddr value for this CustomObject6Query.
     * 
     * @param createdByEMailAddr
     */
    public void setCreatedByEMailAddr(QueryType createdByEMailAddr) {
        this.createdByEMailAddr = createdByEMailAddr;
    }


    /**
     * Gets the createdByExternalSystemId value for this CustomObject6Query.
     * 
     * @return createdByExternalSystemId
     */
    public QueryType getCreatedByExternalSystemId() {
        return createdByExternalSystemId;
    }


    /**
     * Sets the createdByExternalSystemId value for this CustomObject6Query.
     * 
     * @param createdByExternalSystemId
     */
    public void setCreatedByExternalSystemId(QueryType createdByExternalSystemId) {
        this.createdByExternalSystemId = createdByExternalSystemId;
    }


    /**
     * Gets the createdByFirstName value for this CustomObject6Query.
     * 
     * @return createdByFirstName
     */
    public QueryType getCreatedByFirstName() {
        return createdByFirstName;
    }


    /**
     * Sets the createdByFirstName value for this CustomObject6Query.
     * 
     * @param createdByFirstName
     */
    public void setCreatedByFirstName(QueryType createdByFirstName) {
        this.createdByFirstName = createdByFirstName;
    }


    /**
     * Gets the createdByFullName value for this CustomObject6Query.
     * 
     * @return createdByFullName
     */
    public QueryType getCreatedByFullName() {
        return createdByFullName;
    }


    /**
     * Sets the createdByFullName value for this CustomObject6Query.
     * 
     * @param createdByFullName
     */
    public void setCreatedByFullName(QueryType createdByFullName) {
        this.createdByFullName = createdByFullName;
    }


    /**
     * Gets the createdByIntegrationId value for this CustomObject6Query.
     * 
     * @return createdByIntegrationId
     */
    public QueryType getCreatedByIntegrationId() {
        return createdByIntegrationId;
    }


    /**
     * Sets the createdByIntegrationId value for this CustomObject6Query.
     * 
     * @param createdByIntegrationId
     */
    public void setCreatedByIntegrationId(QueryType createdByIntegrationId) {
        this.createdByIntegrationId = createdByIntegrationId;
    }


    /**
     * Gets the createdByLastName value for this CustomObject6Query.
     * 
     * @return createdByLastName
     */
    public QueryType getCreatedByLastName() {
        return createdByLastName;
    }


    /**
     * Sets the createdByLastName value for this CustomObject6Query.
     * 
     * @param createdByLastName
     */
    public void setCreatedByLastName(QueryType createdByLastName) {
        this.createdByLastName = createdByLastName;
    }


    /**
     * Gets the createdByUserSignInId value for this CustomObject6Query.
     * 
     * @return createdByUserSignInId
     */
    public QueryType getCreatedByUserSignInId() {
        return createdByUserSignInId;
    }


    /**
     * Sets the createdByUserSignInId value for this CustomObject6Query.
     * 
     * @param createdByUserSignInId
     */
    public void setCreatedByUserSignInId(QueryType createdByUserSignInId) {
        this.createdByUserSignInId = createdByUserSignInId;
    }


    /**
     * Gets the accountExternalSystemId value for this CustomObject6Query.
     * 
     * @return accountExternalSystemId
     */
    public QueryType getAccountExternalSystemId() {
        return accountExternalSystemId;
    }


    /**
     * Sets the accountExternalSystemId value for this CustomObject6Query.
     * 
     * @param accountExternalSystemId
     */
    public void setAccountExternalSystemId(QueryType accountExternalSystemId) {
        this.accountExternalSystemId = accountExternalSystemId;
    }


    /**
     * Gets the accountIntegrationId value for this CustomObject6Query.
     * 
     * @return accountIntegrationId
     */
    public QueryType getAccountIntegrationId() {
        return accountIntegrationId;
    }


    /**
     * Sets the accountIntegrationId value for this CustomObject6Query.
     * 
     * @param accountIntegrationId
     */
    public void setAccountIntegrationId(QueryType accountIntegrationId) {
        this.accountIntegrationId = accountIntegrationId;
    }


    /**
     * Gets the accountLocation value for this CustomObject6Query.
     * 
     * @return accountLocation
     */
    public QueryType getAccountLocation() {
        return accountLocation;
    }


    /**
     * Sets the accountLocation value for this CustomObject6Query.
     * 
     * @param accountLocation
     */
    public void setAccountLocation(QueryType accountLocation) {
        this.accountLocation = accountLocation;
    }


    /**
     * Gets the accountName value for this CustomObject6Query.
     * 
     * @return accountName
     */
    public QueryType getAccountName() {
        return accountName;
    }


    /**
     * Sets the accountName value for this CustomObject6Query.
     * 
     * @param accountName
     */
    public void setAccountName(QueryType accountName) {
        this.accountName = accountName;
    }


    /**
     * Gets the activitySubject value for this CustomObject6Query.
     * 
     * @return activitySubject
     */
    public QueryType getActivitySubject() {
        return activitySubject;
    }


    /**
     * Sets the activitySubject value for this CustomObject6Query.
     * 
     * @param activitySubject
     */
    public void setActivitySubject(QueryType activitySubject) {
        this.activitySubject = activitySubject;
    }


    /**
     * Gets the activityExternalSystemId value for this CustomObject6Query.
     * 
     * @return activityExternalSystemId
     */
    public QueryType getActivityExternalSystemId() {
        return activityExternalSystemId;
    }


    /**
     * Sets the activityExternalSystemId value for this CustomObject6Query.
     * 
     * @param activityExternalSystemId
     */
    public void setActivityExternalSystemId(QueryType activityExternalSystemId) {
        this.activityExternalSystemId = activityExternalSystemId;
    }


    /**
     * Gets the activityIntegrationId value for this CustomObject6Query.
     * 
     * @return activityIntegrationId
     */
    public QueryType getActivityIntegrationId() {
        return activityIntegrationId;
    }


    /**
     * Sets the activityIntegrationId value for this CustomObject6Query.
     * 
     * @param activityIntegrationId
     */
    public void setActivityIntegrationId(QueryType activityIntegrationId) {
        this.activityIntegrationId = activityIntegrationId;
    }


    /**
     * Gets the assetIntegrationId value for this CustomObject6Query.
     * 
     * @return assetIntegrationId
     */
    public QueryType getAssetIntegrationId() {
        return assetIntegrationId;
    }


    /**
     * Sets the assetIntegrationId value for this CustomObject6Query.
     * 
     * @param assetIntegrationId
     */
    public void setAssetIntegrationId(QueryType assetIntegrationId) {
        this.assetIntegrationId = assetIntegrationId;
    }


    /**
     * Gets the assetProduct value for this CustomObject6Query.
     * 
     * @return assetProduct
     */
    public QueryType getAssetProduct() {
        return assetProduct;
    }


    /**
     * Sets the assetProduct value for this CustomObject6Query.
     * 
     * @param assetProduct
     */
    public void setAssetProduct(QueryType assetProduct) {
        this.assetProduct = assetProduct;
    }


    /**
     * Gets the assetSerialNumber value for this CustomObject6Query.
     * 
     * @return assetSerialNumber
     */
    public QueryType getAssetSerialNumber() {
        return assetSerialNumber;
    }


    /**
     * Sets the assetSerialNumber value for this CustomObject6Query.
     * 
     * @param assetSerialNumber
     */
    public void setAssetSerialNumber(QueryType assetSerialNumber) {
        this.assetSerialNumber = assetSerialNumber;
    }


    /**
     * Gets the assetExternalSystemId value for this CustomObject6Query.
     * 
     * @return assetExternalSystemId
     */
    public QueryType getAssetExternalSystemId() {
        return assetExternalSystemId;
    }


    /**
     * Sets the assetExternalSystemId value for this CustomObject6Query.
     * 
     * @param assetExternalSystemId
     */
    public void setAssetExternalSystemId(QueryType assetExternalSystemId) {
        this.assetExternalSystemId = assetExternalSystemId;
    }


    /**
     * Gets the campaignExternalSystemId value for this CustomObject6Query.
     * 
     * @return campaignExternalSystemId
     */
    public QueryType getCampaignExternalSystemId() {
        return campaignExternalSystemId;
    }


    /**
     * Sets the campaignExternalSystemId value for this CustomObject6Query.
     * 
     * @param campaignExternalSystemId
     */
    public void setCampaignExternalSystemId(QueryType campaignExternalSystemId) {
        this.campaignExternalSystemId = campaignExternalSystemId;
    }


    /**
     * Gets the campaignIntegrationId value for this CustomObject6Query.
     * 
     * @return campaignIntegrationId
     */
    public QueryType getCampaignIntegrationId() {
        return campaignIntegrationId;
    }


    /**
     * Sets the campaignIntegrationId value for this CustomObject6Query.
     * 
     * @param campaignIntegrationId
     */
    public void setCampaignIntegrationId(QueryType campaignIntegrationId) {
        this.campaignIntegrationId = campaignIntegrationId;
    }


    /**
     * Gets the campaignName value for this CustomObject6Query.
     * 
     * @return campaignName
     */
    public QueryType getCampaignName() {
        return campaignName;
    }


    /**
     * Sets the campaignName value for this CustomObject6Query.
     * 
     * @param campaignName
     */
    public void setCampaignName(QueryType campaignName) {
        this.campaignName = campaignName;
    }


    /**
     * Gets the contactEmail value for this CustomObject6Query.
     * 
     * @return contactEmail
     */
    public QueryType getContactEmail() {
        return contactEmail;
    }


    /**
     * Sets the contactEmail value for this CustomObject6Query.
     * 
     * @param contactEmail
     */
    public void setContactEmail(QueryType contactEmail) {
        this.contactEmail = contactEmail;
    }


    /**
     * Gets the contactExternalSystemId value for this CustomObject6Query.
     * 
     * @return contactExternalSystemId
     */
    public QueryType getContactExternalSystemId() {
        return contactExternalSystemId;
    }


    /**
     * Sets the contactExternalSystemId value for this CustomObject6Query.
     * 
     * @param contactExternalSystemId
     */
    public void setContactExternalSystemId(QueryType contactExternalSystemId) {
        this.contactExternalSystemId = contactExternalSystemId;
    }


    /**
     * Gets the contactFirstName value for this CustomObject6Query.
     * 
     * @return contactFirstName
     */
    public QueryType getContactFirstName() {
        return contactFirstName;
    }


    /**
     * Sets the contactFirstName value for this CustomObject6Query.
     * 
     * @param contactFirstName
     */
    public void setContactFirstName(QueryType contactFirstName) {
        this.contactFirstName = contactFirstName;
    }


    /**
     * Gets the contactFullName value for this CustomObject6Query.
     * 
     * @return contactFullName
     */
    public QueryType getContactFullName() {
        return contactFullName;
    }


    /**
     * Sets the contactFullName value for this CustomObject6Query.
     * 
     * @param contactFullName
     */
    public void setContactFullName(QueryType contactFullName) {
        this.contactFullName = contactFullName;
    }


    /**
     * Gets the contactIntegrationId value for this CustomObject6Query.
     * 
     * @return contactIntegrationId
     */
    public QueryType getContactIntegrationId() {
        return contactIntegrationId;
    }


    /**
     * Sets the contactIntegrationId value for this CustomObject6Query.
     * 
     * @param contactIntegrationId
     */
    public void setContactIntegrationId(QueryType contactIntegrationId) {
        this.contactIntegrationId = contactIntegrationId;
    }


    /**
     * Gets the contactLastName value for this CustomObject6Query.
     * 
     * @return contactLastName
     */
    public QueryType getContactLastName() {
        return contactLastName;
    }


    /**
     * Sets the contactLastName value for this CustomObject6Query.
     * 
     * @param contactLastName
     */
    public void setContactLastName(QueryType contactLastName) {
        this.contactLastName = contactLastName;
    }


    /**
     * Gets the customObject1ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject1ExternalSystemId
     */
    public QueryType getCustomObject1ExternalSystemId() {
        return customObject1ExternalSystemId;
    }


    /**
     * Sets the customObject1ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject1ExternalSystemId
     */
    public void setCustomObject1ExternalSystemId(QueryType customObject1ExternalSystemId) {
        this.customObject1ExternalSystemId = customObject1ExternalSystemId;
    }


    /**
     * Gets the customObject1IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject1IntegrationId
     */
    public QueryType getCustomObject1IntegrationId() {
        return customObject1IntegrationId;
    }


    /**
     * Sets the customObject1IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject1IntegrationId
     */
    public void setCustomObject1IntegrationId(QueryType customObject1IntegrationId) {
        this.customObject1IntegrationId = customObject1IntegrationId;
    }


    /**
     * Gets the customObject1Name value for this CustomObject6Query.
     * 
     * @return customObject1Name
     */
    public QueryType getCustomObject1Name() {
        return customObject1Name;
    }


    /**
     * Sets the customObject1Name value for this CustomObject6Query.
     * 
     * @param customObject1Name
     */
    public void setCustomObject1Name(QueryType customObject1Name) {
        this.customObject1Name = customObject1Name;
    }


    /**
     * Gets the customObject2ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject2ExternalSystemId
     */
    public QueryType getCustomObject2ExternalSystemId() {
        return customObject2ExternalSystemId;
    }


    /**
     * Sets the customObject2ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject2ExternalSystemId
     */
    public void setCustomObject2ExternalSystemId(QueryType customObject2ExternalSystemId) {
        this.customObject2ExternalSystemId = customObject2ExternalSystemId;
    }


    /**
     * Gets the customObject2IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject2IntegrationId
     */
    public QueryType getCustomObject2IntegrationId() {
        return customObject2IntegrationId;
    }


    /**
     * Sets the customObject2IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject2IntegrationId
     */
    public void setCustomObject2IntegrationId(QueryType customObject2IntegrationId) {
        this.customObject2IntegrationId = customObject2IntegrationId;
    }


    /**
     * Gets the customObject2Name value for this CustomObject6Query.
     * 
     * @return customObject2Name
     */
    public QueryType getCustomObject2Name() {
        return customObject2Name;
    }


    /**
     * Sets the customObject2Name value for this CustomObject6Query.
     * 
     * @param customObject2Name
     */
    public void setCustomObject2Name(QueryType customObject2Name) {
        this.customObject2Name = customObject2Name;
    }


    /**
     * Gets the customObject3IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject3IntegrationId
     */
    public QueryType getCustomObject3IntegrationId() {
        return customObject3IntegrationId;
    }


    /**
     * Sets the customObject3IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject3IntegrationId
     */
    public void setCustomObject3IntegrationId(QueryType customObject3IntegrationId) {
        this.customObject3IntegrationId = customObject3IntegrationId;
    }


    /**
     * Gets the customObject3ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject3ExternalSystemId
     */
    public QueryType getCustomObject3ExternalSystemId() {
        return customObject3ExternalSystemId;
    }


    /**
     * Sets the customObject3ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject3ExternalSystemId
     */
    public void setCustomObject3ExternalSystemId(QueryType customObject3ExternalSystemId) {
        this.customObject3ExternalSystemId = customObject3ExternalSystemId;
    }


    /**
     * Gets the customObject3Name value for this CustomObject6Query.
     * 
     * @return customObject3Name
     */
    public QueryType getCustomObject3Name() {
        return customObject3Name;
    }


    /**
     * Sets the customObject3Name value for this CustomObject6Query.
     * 
     * @param customObject3Name
     */
    public void setCustomObject3Name(QueryType customObject3Name) {
        this.customObject3Name = customObject3Name;
    }


    /**
     * Gets the customObject4Name value for this CustomObject6Query.
     * 
     * @return customObject4Name
     */
    public QueryType getCustomObject4Name() {
        return customObject4Name;
    }


    /**
     * Sets the customObject4Name value for this CustomObject6Query.
     * 
     * @param customObject4Name
     */
    public void setCustomObject4Name(QueryType customObject4Name) {
        this.customObject4Name = customObject4Name;
    }


    /**
     * Gets the customObject4ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject4ExternalSystemId
     */
    public QueryType getCustomObject4ExternalSystemId() {
        return customObject4ExternalSystemId;
    }


    /**
     * Sets the customObject4ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject4ExternalSystemId
     */
    public void setCustomObject4ExternalSystemId(QueryType customObject4ExternalSystemId) {
        this.customObject4ExternalSystemId = customObject4ExternalSystemId;
    }


    /**
     * Gets the customObject4IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject4IntegrationId
     */
    public QueryType getCustomObject4IntegrationId() {
        return customObject4IntegrationId;
    }


    /**
     * Sets the customObject4IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject4IntegrationId
     */
    public void setCustomObject4IntegrationId(QueryType customObject4IntegrationId) {
        this.customObject4IntegrationId = customObject4IntegrationId;
    }


    /**
     * Gets the customObject5Name value for this CustomObject6Query.
     * 
     * @return customObject5Name
     */
    public QueryType getCustomObject5Name() {
        return customObject5Name;
    }


    /**
     * Sets the customObject5Name value for this CustomObject6Query.
     * 
     * @param customObject5Name
     */
    public void setCustomObject5Name(QueryType customObject5Name) {
        this.customObject5Name = customObject5Name;
    }


    /**
     * Gets the customObject5ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject5ExternalSystemId
     */
    public QueryType getCustomObject5ExternalSystemId() {
        return customObject5ExternalSystemId;
    }


    /**
     * Sets the customObject5ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject5ExternalSystemId
     */
    public void setCustomObject5ExternalSystemId(QueryType customObject5ExternalSystemId) {
        this.customObject5ExternalSystemId = customObject5ExternalSystemId;
    }


    /**
     * Gets the customObject5IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject5IntegrationId
     */
    public QueryType getCustomObject5IntegrationId() {
        return customObject5IntegrationId;
    }


    /**
     * Sets the customObject5IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject5IntegrationId
     */
    public void setCustomObject5IntegrationId(QueryType customObject5IntegrationId) {
        this.customObject5IntegrationId = customObject5IntegrationId;
    }


    /**
     * Gets the customObject6Name value for this CustomObject6Query.
     * 
     * @return customObject6Name
     */
    public QueryType getCustomObject6Name() {
        return customObject6Name;
    }


    /**
     * Sets the customObject6Name value for this CustomObject6Query.
     * 
     * @param customObject6Name
     */
    public void setCustomObject6Name(QueryType customObject6Name) {
        this.customObject6Name = customObject6Name;
    }


    /**
     * Gets the customObject6ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject6ExternalSystemId
     */
    public QueryType getCustomObject6ExternalSystemId() {
        return customObject6ExternalSystemId;
    }


    /**
     * Sets the customObject6ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject6ExternalSystemId
     */
    public void setCustomObject6ExternalSystemId(QueryType customObject6ExternalSystemId) {
        this.customObject6ExternalSystemId = customObject6ExternalSystemId;
    }


    /**
     * Gets the customObject6IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject6IntegrationId
     */
    public QueryType getCustomObject6IntegrationId() {
        return customObject6IntegrationId;
    }


    /**
     * Sets the customObject6IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject6IntegrationId
     */
    public void setCustomObject6IntegrationId(QueryType customObject6IntegrationId) {
        this.customObject6IntegrationId = customObject6IntegrationId;
    }


    /**
     * Gets the customObject7Name value for this CustomObject6Query.
     * 
     * @return customObject7Name
     */
    public QueryType getCustomObject7Name() {
        return customObject7Name;
    }


    /**
     * Sets the customObject7Name value for this CustomObject6Query.
     * 
     * @param customObject7Name
     */
    public void setCustomObject7Name(QueryType customObject7Name) {
        this.customObject7Name = customObject7Name;
    }


    /**
     * Gets the customObject7ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject7ExternalSystemId
     */
    public QueryType getCustomObject7ExternalSystemId() {
        return customObject7ExternalSystemId;
    }


    /**
     * Sets the customObject7ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject7ExternalSystemId
     */
    public void setCustomObject7ExternalSystemId(QueryType customObject7ExternalSystemId) {
        this.customObject7ExternalSystemId = customObject7ExternalSystemId;
    }


    /**
     * Gets the customObject7IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject7IntegrationId
     */
    public QueryType getCustomObject7IntegrationId() {
        return customObject7IntegrationId;
    }


    /**
     * Sets the customObject7IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject7IntegrationId
     */
    public void setCustomObject7IntegrationId(QueryType customObject7IntegrationId) {
        this.customObject7IntegrationId = customObject7IntegrationId;
    }


    /**
     * Gets the customObject8Name value for this CustomObject6Query.
     * 
     * @return customObject8Name
     */
    public QueryType getCustomObject8Name() {
        return customObject8Name;
    }


    /**
     * Sets the customObject8Name value for this CustomObject6Query.
     * 
     * @param customObject8Name
     */
    public void setCustomObject8Name(QueryType customObject8Name) {
        this.customObject8Name = customObject8Name;
    }


    /**
     * Gets the customObject8ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject8ExternalSystemId
     */
    public QueryType getCustomObject8ExternalSystemId() {
        return customObject8ExternalSystemId;
    }


    /**
     * Sets the customObject8ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject8ExternalSystemId
     */
    public void setCustomObject8ExternalSystemId(QueryType customObject8ExternalSystemId) {
        this.customObject8ExternalSystemId = customObject8ExternalSystemId;
    }


    /**
     * Gets the customObject8IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject8IntegrationId
     */
    public QueryType getCustomObject8IntegrationId() {
        return customObject8IntegrationId;
    }


    /**
     * Sets the customObject8IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject8IntegrationId
     */
    public void setCustomObject8IntegrationId(QueryType customObject8IntegrationId) {
        this.customObject8IntegrationId = customObject8IntegrationId;
    }


    /**
     * Gets the customObject9Name value for this CustomObject6Query.
     * 
     * @return customObject9Name
     */
    public QueryType getCustomObject9Name() {
        return customObject9Name;
    }


    /**
     * Sets the customObject9Name value for this CustomObject6Query.
     * 
     * @param customObject9Name
     */
    public void setCustomObject9Name(QueryType customObject9Name) {
        this.customObject9Name = customObject9Name;
    }


    /**
     * Gets the customObject9ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject9ExternalSystemId
     */
    public QueryType getCustomObject9ExternalSystemId() {
        return customObject9ExternalSystemId;
    }


    /**
     * Sets the customObject9ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject9ExternalSystemId
     */
    public void setCustomObject9ExternalSystemId(QueryType customObject9ExternalSystemId) {
        this.customObject9ExternalSystemId = customObject9ExternalSystemId;
    }


    /**
     * Gets the customObject9IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject9IntegrationId
     */
    public QueryType getCustomObject9IntegrationId() {
        return customObject9IntegrationId;
    }


    /**
     * Sets the customObject9IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject9IntegrationId
     */
    public void setCustomObject9IntegrationId(QueryType customObject9IntegrationId) {
        this.customObject9IntegrationId = customObject9IntegrationId;
    }


    /**
     * Gets the customObject10Name value for this CustomObject6Query.
     * 
     * @return customObject10Name
     */
    public QueryType getCustomObject10Name() {
        return customObject10Name;
    }


    /**
     * Sets the customObject10Name value for this CustomObject6Query.
     * 
     * @param customObject10Name
     */
    public void setCustomObject10Name(QueryType customObject10Name) {
        this.customObject10Name = customObject10Name;
    }


    /**
     * Gets the customObject10ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject10ExternalSystemId
     */
    public QueryType getCustomObject10ExternalSystemId() {
        return customObject10ExternalSystemId;
    }


    /**
     * Sets the customObject10ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject10ExternalSystemId
     */
    public void setCustomObject10ExternalSystemId(QueryType customObject10ExternalSystemId) {
        this.customObject10ExternalSystemId = customObject10ExternalSystemId;
    }


    /**
     * Gets the customObject10IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject10IntegrationId
     */
    public QueryType getCustomObject10IntegrationId() {
        return customObject10IntegrationId;
    }


    /**
     * Sets the customObject10IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject10IntegrationId
     */
    public void setCustomObject10IntegrationId(QueryType customObject10IntegrationId) {
        this.customObject10IntegrationId = customObject10IntegrationId;
    }


    /**
     * Gets the customObject11Name value for this CustomObject6Query.
     * 
     * @return customObject11Name
     */
    public QueryType getCustomObject11Name() {
        return customObject11Name;
    }


    /**
     * Sets the customObject11Name value for this CustomObject6Query.
     * 
     * @param customObject11Name
     */
    public void setCustomObject11Name(QueryType customObject11Name) {
        this.customObject11Name = customObject11Name;
    }


    /**
     * Gets the customObject11ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject11ExternalSystemId
     */
    public QueryType getCustomObject11ExternalSystemId() {
        return customObject11ExternalSystemId;
    }


    /**
     * Sets the customObject11ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject11ExternalSystemId
     */
    public void setCustomObject11ExternalSystemId(QueryType customObject11ExternalSystemId) {
        this.customObject11ExternalSystemId = customObject11ExternalSystemId;
    }


    /**
     * Gets the customObject11IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject11IntegrationId
     */
    public QueryType getCustomObject11IntegrationId() {
        return customObject11IntegrationId;
    }


    /**
     * Sets the customObject11IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject11IntegrationId
     */
    public void setCustomObject11IntegrationId(QueryType customObject11IntegrationId) {
        this.customObject11IntegrationId = customObject11IntegrationId;
    }


    /**
     * Gets the customObject12Name value for this CustomObject6Query.
     * 
     * @return customObject12Name
     */
    public QueryType getCustomObject12Name() {
        return customObject12Name;
    }


    /**
     * Sets the customObject12Name value for this CustomObject6Query.
     * 
     * @param customObject12Name
     */
    public void setCustomObject12Name(QueryType customObject12Name) {
        this.customObject12Name = customObject12Name;
    }


    /**
     * Gets the customObject12ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject12ExternalSystemId
     */
    public QueryType getCustomObject12ExternalSystemId() {
        return customObject12ExternalSystemId;
    }


    /**
     * Sets the customObject12ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject12ExternalSystemId
     */
    public void setCustomObject12ExternalSystemId(QueryType customObject12ExternalSystemId) {
        this.customObject12ExternalSystemId = customObject12ExternalSystemId;
    }


    /**
     * Gets the customObject12IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject12IntegrationId
     */
    public QueryType getCustomObject12IntegrationId() {
        return customObject12IntegrationId;
    }


    /**
     * Sets the customObject12IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject12IntegrationId
     */
    public void setCustomObject12IntegrationId(QueryType customObject12IntegrationId) {
        this.customObject12IntegrationId = customObject12IntegrationId;
    }


    /**
     * Gets the customObject13Name value for this CustomObject6Query.
     * 
     * @return customObject13Name
     */
    public QueryType getCustomObject13Name() {
        return customObject13Name;
    }


    /**
     * Sets the customObject13Name value for this CustomObject6Query.
     * 
     * @param customObject13Name
     */
    public void setCustomObject13Name(QueryType customObject13Name) {
        this.customObject13Name = customObject13Name;
    }


    /**
     * Gets the customObject13ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject13ExternalSystemId
     */
    public QueryType getCustomObject13ExternalSystemId() {
        return customObject13ExternalSystemId;
    }


    /**
     * Sets the customObject13ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject13ExternalSystemId
     */
    public void setCustomObject13ExternalSystemId(QueryType customObject13ExternalSystemId) {
        this.customObject13ExternalSystemId = customObject13ExternalSystemId;
    }


    /**
     * Gets the customObject13IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject13IntegrationId
     */
    public QueryType getCustomObject13IntegrationId() {
        return customObject13IntegrationId;
    }


    /**
     * Sets the customObject13IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject13IntegrationId
     */
    public void setCustomObject13IntegrationId(QueryType customObject13IntegrationId) {
        this.customObject13IntegrationId = customObject13IntegrationId;
    }


    /**
     * Gets the customObject14Name value for this CustomObject6Query.
     * 
     * @return customObject14Name
     */
    public QueryType getCustomObject14Name() {
        return customObject14Name;
    }


    /**
     * Sets the customObject14Name value for this CustomObject6Query.
     * 
     * @param customObject14Name
     */
    public void setCustomObject14Name(QueryType customObject14Name) {
        this.customObject14Name = customObject14Name;
    }


    /**
     * Gets the customObject14ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject14ExternalSystemId
     */
    public QueryType getCustomObject14ExternalSystemId() {
        return customObject14ExternalSystemId;
    }


    /**
     * Sets the customObject14ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject14ExternalSystemId
     */
    public void setCustomObject14ExternalSystemId(QueryType customObject14ExternalSystemId) {
        this.customObject14ExternalSystemId = customObject14ExternalSystemId;
    }


    /**
     * Gets the customObject14IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject14IntegrationId
     */
    public QueryType getCustomObject14IntegrationId() {
        return customObject14IntegrationId;
    }


    /**
     * Sets the customObject14IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject14IntegrationId
     */
    public void setCustomObject14IntegrationId(QueryType customObject14IntegrationId) {
        this.customObject14IntegrationId = customObject14IntegrationId;
    }


    /**
     * Gets the customObject15Name value for this CustomObject6Query.
     * 
     * @return customObject15Name
     */
    public QueryType getCustomObject15Name() {
        return customObject15Name;
    }


    /**
     * Sets the customObject15Name value for this CustomObject6Query.
     * 
     * @param customObject15Name
     */
    public void setCustomObject15Name(QueryType customObject15Name) {
        this.customObject15Name = customObject15Name;
    }


    /**
     * Gets the customObject15ExternalSystemId value for this CustomObject6Query.
     * 
     * @return customObject15ExternalSystemId
     */
    public QueryType getCustomObject15ExternalSystemId() {
        return customObject15ExternalSystemId;
    }


    /**
     * Sets the customObject15ExternalSystemId value for this CustomObject6Query.
     * 
     * @param customObject15ExternalSystemId
     */
    public void setCustomObject15ExternalSystemId(QueryType customObject15ExternalSystemId) {
        this.customObject15ExternalSystemId = customObject15ExternalSystemId;
    }


    /**
     * Gets the customObject15IntegrationId value for this CustomObject6Query.
     * 
     * @return customObject15IntegrationId
     */
    public QueryType getCustomObject15IntegrationId() {
        return customObject15IntegrationId;
    }


    /**
     * Sets the customObject15IntegrationId value for this CustomObject6Query.
     * 
     * @param customObject15IntegrationId
     */
    public void setCustomObject15IntegrationId(QueryType customObject15IntegrationId) {
        this.customObject15IntegrationId = customObject15IntegrationId;
    }


    /**
     * Gets the dealerName value for this CustomObject6Query.
     * 
     * @return dealerName
     */
    public QueryType getDealerName() {
        return dealerName;
    }


    /**
     * Sets the dealerName value for this CustomObject6Query.
     * 
     * @param dealerName
     */
    public void setDealerName(QueryType dealerName) {
        this.dealerName = dealerName;
    }


    /**
     * Gets the dealerExternalSystemId value for this CustomObject6Query.
     * 
     * @return dealerExternalSystemId
     */
    public QueryType getDealerExternalSystemId() {
        return dealerExternalSystemId;
    }


    /**
     * Sets the dealerExternalSystemId value for this CustomObject6Query.
     * 
     * @param dealerExternalSystemId
     */
    public void setDealerExternalSystemId(QueryType dealerExternalSystemId) {
        this.dealerExternalSystemId = dealerExternalSystemId;
    }


    /**
     * Gets the dealerIntegrationId value for this CustomObject6Query.
     * 
     * @return dealerIntegrationId
     */
    public QueryType getDealerIntegrationId() {
        return dealerIntegrationId;
    }


    /**
     * Sets the dealerIntegrationId value for this CustomObject6Query.
     * 
     * @param dealerIntegrationId
     */
    public void setDealerIntegrationId(QueryType dealerIntegrationId) {
        this.dealerIntegrationId = dealerIntegrationId;
    }


    /**
     * Gets the fundName value for this CustomObject6Query.
     * 
     * @return fundName
     */
    public QueryType getFundName() {
        return fundName;
    }


    /**
     * Sets the fundName value for this CustomObject6Query.
     * 
     * @param fundName
     */
    public void setFundName(QueryType fundName) {
        this.fundName = fundName;
    }


    /**
     * Gets the fundRequestName value for this CustomObject6Query.
     * 
     * @return fundRequestName
     */
    public QueryType getFundRequestName() {
        return fundRequestName;
    }


    /**
     * Sets the fundRequestName value for this CustomObject6Query.
     * 
     * @param fundRequestName
     */
    public void setFundRequestName(QueryType fundRequestName) {
        this.fundRequestName = fundRequestName;
    }


    /**
     * Gets the fundRequestExternalSystemId value for this CustomObject6Query.
     * 
     * @return fundRequestExternalSystemId
     */
    public QueryType getFundRequestExternalSystemId() {
        return fundRequestExternalSystemId;
    }


    /**
     * Sets the fundRequestExternalSystemId value for this CustomObject6Query.
     * 
     * @param fundRequestExternalSystemId
     */
    public void setFundRequestExternalSystemId(QueryType fundRequestExternalSystemId) {
        this.fundRequestExternalSystemId = fundRequestExternalSystemId;
    }


    /**
     * Gets the householdName value for this CustomObject6Query.
     * 
     * @return householdName
     */
    public QueryType getHouseholdName() {
        return householdName;
    }


    /**
     * Sets the householdName value for this CustomObject6Query.
     * 
     * @param householdName
     */
    public void setHouseholdName(QueryType householdName) {
        this.householdName = householdName;
    }


    /**
     * Gets the householdIntegrationId value for this CustomObject6Query.
     * 
     * @return householdIntegrationId
     */
    public QueryType getHouseholdIntegrationId() {
        return householdIntegrationId;
    }


    /**
     * Sets the householdIntegrationId value for this CustomObject6Query.
     * 
     * @param householdIntegrationId
     */
    public void setHouseholdIntegrationId(QueryType householdIntegrationId) {
        this.householdIntegrationId = householdIntegrationId;
    }


    /**
     * Gets the householdExternalSystemId value for this CustomObject6Query.
     * 
     * @return householdExternalSystemId
     */
    public QueryType getHouseholdExternalSystemId() {
        return householdExternalSystemId;
    }


    /**
     * Sets the householdExternalSystemId value for this CustomObject6Query.
     * 
     * @param householdExternalSystemId
     */
    public void setHouseholdExternalSystemId(QueryType householdExternalSystemId) {
        this.householdExternalSystemId = householdExternalSystemId;
    }


    /**
     * Gets the leadExternalSystemId value for this CustomObject6Query.
     * 
     * @return leadExternalSystemId
     */
    public QueryType getLeadExternalSystemId() {
        return leadExternalSystemId;
    }


    /**
     * Sets the leadExternalSystemId value for this CustomObject6Query.
     * 
     * @param leadExternalSystemId
     */
    public void setLeadExternalSystemId(QueryType leadExternalSystemId) {
        this.leadExternalSystemId = leadExternalSystemId;
    }


    /**
     * Gets the leadFirstName value for this CustomObject6Query.
     * 
     * @return leadFirstName
     */
    public QueryType getLeadFirstName() {
        return leadFirstName;
    }


    /**
     * Sets the leadFirstName value for this CustomObject6Query.
     * 
     * @param leadFirstName
     */
    public void setLeadFirstName(QueryType leadFirstName) {
        this.leadFirstName = leadFirstName;
    }


    /**
     * Gets the leadFullName value for this CustomObject6Query.
     * 
     * @return leadFullName
     */
    public QueryType getLeadFullName() {
        return leadFullName;
    }


    /**
     * Sets the leadFullName value for this CustomObject6Query.
     * 
     * @param leadFullName
     */
    public void setLeadFullName(QueryType leadFullName) {
        this.leadFullName = leadFullName;
    }


    /**
     * Gets the leadIntegrationId value for this CustomObject6Query.
     * 
     * @return leadIntegrationId
     */
    public QueryType getLeadIntegrationId() {
        return leadIntegrationId;
    }


    /**
     * Sets the leadIntegrationId value for this CustomObject6Query.
     * 
     * @param leadIntegrationId
     */
    public void setLeadIntegrationId(QueryType leadIntegrationId) {
        this.leadIntegrationId = leadIntegrationId;
    }


    /**
     * Gets the leadLastName value for this CustomObject6Query.
     * 
     * @return leadLastName
     */
    public QueryType getLeadLastName() {
        return leadLastName;
    }


    /**
     * Sets the leadLastName value for this CustomObject6Query.
     * 
     * @param leadLastName
     */
    public void setLeadLastName(QueryType leadLastName) {
        this.leadLastName = leadLastName;
    }


    /**
     * Gets the medEdName value for this CustomObject6Query.
     * 
     * @return medEdName
     */
    public QueryType getMedEdName() {
        return medEdName;
    }


    /**
     * Sets the medEdName value for this CustomObject6Query.
     * 
     * @param medEdName
     */
    public void setMedEdName(QueryType medEdName) {
        this.medEdName = medEdName;
    }


    /**
     * Gets the medEdExternalSystemId value for this CustomObject6Query.
     * 
     * @return medEdExternalSystemId
     */
    public QueryType getMedEdExternalSystemId() {
        return medEdExternalSystemId;
    }


    /**
     * Sets the medEdExternalSystemId value for this CustomObject6Query.
     * 
     * @param medEdExternalSystemId
     */
    public void setMedEdExternalSystemId(QueryType medEdExternalSystemId) {
        this.medEdExternalSystemId = medEdExternalSystemId;
    }


    /**
     * Gets the medEdIntegrationId value for this CustomObject6Query.
     * 
     * @return medEdIntegrationId
     */
    public QueryType getMedEdIntegrationId() {
        return medEdIntegrationId;
    }


    /**
     * Sets the medEdIntegrationId value for this CustomObject6Query.
     * 
     * @param medEdIntegrationId
     */
    public void setMedEdIntegrationId(QueryType medEdIntegrationId) {
        this.medEdIntegrationId = medEdIntegrationId;
    }


    /**
     * Gets the opportunityAccountId value for this CustomObject6Query.
     * 
     * @return opportunityAccountId
     */
    public QueryType getOpportunityAccountId() {
        return opportunityAccountId;
    }


    /**
     * Sets the opportunityAccountId value for this CustomObject6Query.
     * 
     * @param opportunityAccountId
     */
    public void setOpportunityAccountId(QueryType opportunityAccountId) {
        this.opportunityAccountId = opportunityAccountId;
    }


    /**
     * Gets the opportunityExternalSystemId value for this CustomObject6Query.
     * 
     * @return opportunityExternalSystemId
     */
    public QueryType getOpportunityExternalSystemId() {
        return opportunityExternalSystemId;
    }


    /**
     * Sets the opportunityExternalSystemId value for this CustomObject6Query.
     * 
     * @param opportunityExternalSystemId
     */
    public void setOpportunityExternalSystemId(QueryType opportunityExternalSystemId) {
        this.opportunityExternalSystemId = opportunityExternalSystemId;
    }


    /**
     * Gets the opportunityIntegrationId value for this CustomObject6Query.
     * 
     * @return opportunityIntegrationId
     */
    public QueryType getOpportunityIntegrationId() {
        return opportunityIntegrationId;
    }


    /**
     * Sets the opportunityIntegrationId value for this CustomObject6Query.
     * 
     * @param opportunityIntegrationId
     */
    public void setOpportunityIntegrationId(QueryType opportunityIntegrationId) {
        this.opportunityIntegrationId = opportunityIntegrationId;
    }


    /**
     * Gets the opportunityName value for this CustomObject6Query.
     * 
     * @return opportunityName
     */
    public QueryType getOpportunityName() {
        return opportunityName;
    }


    /**
     * Sets the opportunityName value for this CustomObject6Query.
     * 
     * @param opportunityName
     */
    public void setOpportunityName(QueryType opportunityName) {
        this.opportunityName = opportunityName;
    }


    /**
     * Gets the portfolioAccountNumber value for this CustomObject6Query.
     * 
     * @return portfolioAccountNumber
     */
    public QueryType getPortfolioAccountNumber() {
        return portfolioAccountNumber;
    }


    /**
     * Sets the portfolioAccountNumber value for this CustomObject6Query.
     * 
     * @param portfolioAccountNumber
     */
    public void setPortfolioAccountNumber(QueryType portfolioAccountNumber) {
        this.portfolioAccountNumber = portfolioAccountNumber;
    }


    /**
     * Gets the portfolioExternalSystemId value for this CustomObject6Query.
     * 
     * @return portfolioExternalSystemId
     */
    public QueryType getPortfolioExternalSystemId() {
        return portfolioExternalSystemId;
    }


    /**
     * Sets the portfolioExternalSystemId value for this CustomObject6Query.
     * 
     * @param portfolioExternalSystemId
     */
    public void setPortfolioExternalSystemId(QueryType portfolioExternalSystemId) {
        this.portfolioExternalSystemId = portfolioExternalSystemId;
    }


    /**
     * Gets the portfolioIntegrationId value for this CustomObject6Query.
     * 
     * @return portfolioIntegrationId
     */
    public QueryType getPortfolioIntegrationId() {
        return portfolioIntegrationId;
    }


    /**
     * Sets the portfolioIntegrationId value for this CustomObject6Query.
     * 
     * @param portfolioIntegrationId
     */
    public void setPortfolioIntegrationId(QueryType portfolioIntegrationId) {
        this.portfolioIntegrationId = portfolioIntegrationId;
    }


    /**
     * Gets the productIntegrationId value for this CustomObject6Query.
     * 
     * @return productIntegrationId
     */
    public QueryType getProductIntegrationId() {
        return productIntegrationId;
    }


    /**
     * Sets the productIntegrationId value for this CustomObject6Query.
     * 
     * @param productIntegrationId
     */
    public void setProductIntegrationId(QueryType productIntegrationId) {
        this.productIntegrationId = productIntegrationId;
    }


    /**
     * Gets the productName value for this CustomObject6Query.
     * 
     * @return productName
     */
    public QueryType getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this CustomObject6Query.
     * 
     * @param productName
     */
    public void setProductName(QueryType productName) {
        this.productName = productName;
    }


    /**
     * Gets the productExternalSystemId value for this CustomObject6Query.
     * 
     * @return productExternalSystemId
     */
    public QueryType getProductExternalSystemId() {
        return productExternalSystemId;
    }


    /**
     * Sets the productExternalSystemId value for this CustomObject6Query.
     * 
     * @param productExternalSystemId
     */
    public void setProductExternalSystemId(QueryType productExternalSystemId) {
        this.productExternalSystemId = productExternalSystemId;
    }


    /**
     * Gets the serviceRequestExternalSystemId value for this CustomObject6Query.
     * 
     * @return serviceRequestExternalSystemId
     */
    public QueryType getServiceRequestExternalSystemId() {
        return serviceRequestExternalSystemId;
    }


    /**
     * Sets the serviceRequestExternalSystemId value for this CustomObject6Query.
     * 
     * @param serviceRequestExternalSystemId
     */
    public void setServiceRequestExternalSystemId(QueryType serviceRequestExternalSystemId) {
        this.serviceRequestExternalSystemId = serviceRequestExternalSystemId;
    }


    /**
     * Gets the serviceRequestIntegrationId value for this CustomObject6Query.
     * 
     * @return serviceRequestIntegrationId
     */
    public QueryType getServiceRequestIntegrationId() {
        return serviceRequestIntegrationId;
    }


    /**
     * Sets the serviceRequestIntegrationId value for this CustomObject6Query.
     * 
     * @param serviceRequestIntegrationId
     */
    public void setServiceRequestIntegrationId(QueryType serviceRequestIntegrationId) {
        this.serviceRequestIntegrationId = serviceRequestIntegrationId;
    }


    /**
     * Gets the serviceRequestSRNumber value for this CustomObject6Query.
     * 
     * @return serviceRequestSRNumber
     */
    public QueryType getServiceRequestSRNumber() {
        return serviceRequestSRNumber;
    }


    /**
     * Sets the serviceRequestSRNumber value for this CustomObject6Query.
     * 
     * @param serviceRequestSRNumber
     */
    public void setServiceRequestSRNumber(QueryType serviceRequestSRNumber) {
        this.serviceRequestSRNumber = serviceRequestSRNumber;
    }


    /**
     * Gets the solutionExternalSystemId value for this CustomObject6Query.
     * 
     * @return solutionExternalSystemId
     */
    public QueryType getSolutionExternalSystemId() {
        return solutionExternalSystemId;
    }


    /**
     * Sets the solutionExternalSystemId value for this CustomObject6Query.
     * 
     * @param solutionExternalSystemId
     */
    public void setSolutionExternalSystemId(QueryType solutionExternalSystemId) {
        this.solutionExternalSystemId = solutionExternalSystemId;
    }


    /**
     * Gets the solutionIntegrationId value for this CustomObject6Query.
     * 
     * @return solutionIntegrationId
     */
    public QueryType getSolutionIntegrationId() {
        return solutionIntegrationId;
    }


    /**
     * Sets the solutionIntegrationId value for this CustomObject6Query.
     * 
     * @param solutionIntegrationId
     */
    public void setSolutionIntegrationId(QueryType solutionIntegrationId) {
        this.solutionIntegrationId = solutionIntegrationId;
    }


    /**
     * Gets the solutionTitle value for this CustomObject6Query.
     * 
     * @return solutionTitle
     */
    public QueryType getSolutionTitle() {
        return solutionTitle;
    }


    /**
     * Sets the solutionTitle value for this CustomObject6Query.
     * 
     * @param solutionTitle
     */
    public void setSolutionTitle(QueryType solutionTitle) {
        this.solutionTitle = solutionTitle;
    }


    /**
     * Gets the vehicleIntegrationId value for this CustomObject6Query.
     * 
     * @return vehicleIntegrationId
     */
    public QueryType getVehicleIntegrationId() {
        return vehicleIntegrationId;
    }


    /**
     * Sets the vehicleIntegrationId value for this CustomObject6Query.
     * 
     * @param vehicleIntegrationId
     */
    public void setVehicleIntegrationId(QueryType vehicleIntegrationId) {
        this.vehicleIntegrationId = vehicleIntegrationId;
    }


    /**
     * Gets the vehicleVIN value for this CustomObject6Query.
     * 
     * @return vehicleVIN
     */
    public QueryType getVehicleVIN() {
        return vehicleVIN;
    }


    /**
     * Sets the vehicleVIN value for this CustomObject6Query.
     * 
     * @param vehicleVIN
     */
    public void setVehicleVIN(QueryType vehicleVIN) {
        this.vehicleVIN = vehicleVIN;
    }


    /**
     * Gets the vehicleExternalSystemId value for this CustomObject6Query.
     * 
     * @return vehicleExternalSystemId
     */
    public QueryType getVehicleExternalSystemId() {
        return vehicleExternalSystemId;
    }


    /**
     * Sets the vehicleExternalSystemId value for this CustomObject6Query.
     * 
     * @param vehicleExternalSystemId
     */
    public void setVehicleExternalSystemId(QueryType vehicleExternalSystemId) {
        this.vehicleExternalSystemId = vehicleExternalSystemId;
    }


    /**
     * Gets the ownerAlias value for this CustomObject6Query.
     * 
     * @return ownerAlias
     */
    public QueryType getOwnerAlias() {
        return ownerAlias;
    }


    /**
     * Sets the ownerAlias value for this CustomObject6Query.
     * 
     * @param ownerAlias
     */
    public void setOwnerAlias(QueryType ownerAlias) {
        this.ownerAlias = ownerAlias;
    }


    /**
     * Gets the ownerEMailAddr value for this CustomObject6Query.
     * 
     * @return ownerEMailAddr
     */
    public QueryType getOwnerEMailAddr() {
        return ownerEMailAddr;
    }


    /**
     * Sets the ownerEMailAddr value for this CustomObject6Query.
     * 
     * @param ownerEMailAddr
     */
    public void setOwnerEMailAddr(QueryType ownerEMailAddr) {
        this.ownerEMailAddr = ownerEMailAddr;
    }


    /**
     * Gets the ownerExternalSystemId value for this CustomObject6Query.
     * 
     * @return ownerExternalSystemId
     */
    public QueryType getOwnerExternalSystemId() {
        return ownerExternalSystemId;
    }


    /**
     * Sets the ownerExternalSystemId value for this CustomObject6Query.
     * 
     * @param ownerExternalSystemId
     */
    public void setOwnerExternalSystemId(QueryType ownerExternalSystemId) {
        this.ownerExternalSystemId = ownerExternalSystemId;
    }


    /**
     * Gets the ownerFirstName value for this CustomObject6Query.
     * 
     * @return ownerFirstName
     */
    public QueryType getOwnerFirstName() {
        return ownerFirstName;
    }


    /**
     * Sets the ownerFirstName value for this CustomObject6Query.
     * 
     * @param ownerFirstName
     */
    public void setOwnerFirstName(QueryType ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }


    /**
     * Gets the ownerFullName value for this CustomObject6Query.
     * 
     * @return ownerFullName
     */
    public QueryType getOwnerFullName() {
        return ownerFullName;
    }


    /**
     * Sets the ownerFullName value for this CustomObject6Query.
     * 
     * @param ownerFullName
     */
    public void setOwnerFullName(QueryType ownerFullName) {
        this.ownerFullName = ownerFullName;
    }


    /**
     * Gets the ownerIntegrationId value for this CustomObject6Query.
     * 
     * @return ownerIntegrationId
     */
    public QueryType getOwnerIntegrationId() {
        return ownerIntegrationId;
    }


    /**
     * Sets the ownerIntegrationId value for this CustomObject6Query.
     * 
     * @param ownerIntegrationId
     */
    public void setOwnerIntegrationId(QueryType ownerIntegrationId) {
        this.ownerIntegrationId = ownerIntegrationId;
    }


    /**
     * Gets the ownerLastName value for this CustomObject6Query.
     * 
     * @return ownerLastName
     */
    public QueryType getOwnerLastName() {
        return ownerLastName;
    }


    /**
     * Sets the ownerLastName value for this CustomObject6Query.
     * 
     * @param ownerLastName
     */
    public void setOwnerLastName(QueryType ownerLastName) {
        this.ownerLastName = ownerLastName;
    }


    /**
     * Gets the ownerUserSignInId value for this CustomObject6Query.
     * 
     * @return ownerUserSignInId
     */
    public QueryType getOwnerUserSignInId() {
        return ownerUserSignInId;
    }


    /**
     * Sets the ownerUserSignInId value for this CustomObject6Query.
     * 
     * @param ownerUserSignInId
     */
    public void setOwnerUserSignInId(QueryType ownerUserSignInId) {
        this.ownerUserSignInId = ownerUserSignInId;
    }


    /**
     * Gets the createdBy value for this CustomObject6Query.
     * 
     * @return createdBy
     */
    public QueryType getCreatedBy() {
        return createdBy;
    }


    /**
     * Sets the createdBy value for this CustomObject6Query.
     * 
     * @param createdBy
     */
    public void setCreatedBy(QueryType createdBy) {
        this.createdBy = createdBy;
    }


    /**
     * Gets the modifiedBy value for this CustomObject6Query.
     * 
     * @return modifiedBy
     */
    public QueryType getModifiedBy() {
        return modifiedBy;
    }


    /**
     * Sets the modifiedBy value for this CustomObject6Query.
     * 
     * @param modifiedBy
     */
    public void setModifiedBy(QueryType modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    /**
     * Gets the description value for this CustomObject6Query.
     * 
     * @return description
     */
    public QueryType getDescription() {
        return description;
    }


    /**
     * Sets the description value for this CustomObject6Query.
     * 
     * @param description
     */
    public void setDescription(QueryType description) {
        this.description = description;
    }


    /**
     * Gets the searchspec value for this CustomObject6Query.
     * 
     * @return searchspec
     */
    public java.lang.String getSearchspec() {
        return searchspec;
    }


    /**
     * Sets the searchspec value for this CustomObject6Query.
     * 
     * @param searchspec
     */
    public void setSearchspec(java.lang.String searchspec) {
        this.searchspec = searchspec;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomObject6Query)) return false;
        CustomObject6Query other = (CustomObject6Query) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.modifiedDate==null && other.getModifiedDate()==null) || 
             (this.modifiedDate!=null &&
              this.modifiedDate.equals(other.getModifiedDate()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate()))) &&
            ((this.modifiedById==null && other.getModifiedById()==null) || 
             (this.modifiedById!=null &&
              this.modifiedById.equals(other.getModifiedById()))) &&
            ((this.createdById==null && other.getCreatedById()==null) || 
             (this.createdById!=null &&
              this.createdById.equals(other.getCreatedById()))) &&
            ((this.modId==null && other.getModId()==null) || 
             (this.modId!=null &&
              this.modId.equals(other.getModId()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.quickSearch1==null && other.getQuickSearch1()==null) || 
             (this.quickSearch1!=null &&
              this.quickSearch1.equals(other.getQuickSearch1()))) &&
            ((this.quickSearch2==null && other.getQuickSearch2()==null) || 
             (this.quickSearch2!=null &&
              this.quickSearch2.equals(other.getQuickSearch2()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.exchangeDate==null && other.getExchangeDate()==null) || 
             (this.exchangeDate!=null &&
              this.exchangeDate.equals(other.getExchangeDate()))) &&
            ((this.externalSystemId==null && other.getExternalSystemId()==null) || 
             (this.externalSystemId!=null &&
              this.externalSystemId.equals(other.getExternalSystemId()))) &&
            ((this.integrationId==null && other.getIntegrationId()==null) || 
             (this.integrationId!=null &&
              this.integrationId.equals(other.getIntegrationId()))) &&
            ((this.indexedBoolean0==null && other.getIndexedBoolean0()==null) || 
             (this.indexedBoolean0!=null &&
              this.indexedBoolean0.equals(other.getIndexedBoolean0()))) &&
            ((this.indexedCurrency0==null && other.getIndexedCurrency0()==null) || 
             (this.indexedCurrency0!=null &&
              this.indexedCurrency0.equals(other.getIndexedCurrency0()))) &&
            ((this.indexedDate0==null && other.getIndexedDate0()==null) || 
             (this.indexedDate0!=null &&
              this.indexedDate0.equals(other.getIndexedDate0()))) &&
            ((this.indexedNumber0==null && other.getIndexedNumber0()==null) || 
             (this.indexedNumber0!=null &&
              this.indexedNumber0.equals(other.getIndexedNumber0()))) &&
            ((this.indexedPick0==null && other.getIndexedPick0()==null) || 
             (this.indexedPick0!=null &&
              this.indexedPick0.equals(other.getIndexedPick0()))) &&
            ((this.indexedPick1==null && other.getIndexedPick1()==null) || 
             (this.indexedPick1!=null &&
              this.indexedPick1.equals(other.getIndexedPick1()))) &&
            ((this.indexedPick2==null && other.getIndexedPick2()==null) || 
             (this.indexedPick2!=null &&
              this.indexedPick2.equals(other.getIndexedPick2()))) &&
            ((this.indexedPick3==null && other.getIndexedPick3()==null) || 
             (this.indexedPick3!=null &&
              this.indexedPick3.equals(other.getIndexedPick3()))) &&
            ((this.indexedPick4==null && other.getIndexedPick4()==null) || 
             (this.indexedPick4!=null &&
              this.indexedPick4.equals(other.getIndexedPick4()))) &&
            ((this.accountId==null && other.getAccountId()==null) || 
             (this.accountId!=null &&
              this.accountId.equals(other.getAccountId()))) &&
            ((this.activityId==null && other.getActivityId()==null) || 
             (this.activityId!=null &&
              this.activityId.equals(other.getActivityId()))) &&
            ((this.assetId==null && other.getAssetId()==null) || 
             (this.assetId!=null &&
              this.assetId.equals(other.getAssetId()))) &&
            ((this.campaignId==null && other.getCampaignId()==null) || 
             (this.campaignId!=null &&
              this.campaignId.equals(other.getCampaignId()))) &&
            ((this.contactId==null && other.getContactId()==null) || 
             (this.contactId!=null &&
              this.contactId.equals(other.getContactId()))) &&
            ((this.customObject1Id==null && other.getCustomObject1Id()==null) || 
             (this.customObject1Id!=null &&
              this.customObject1Id.equals(other.getCustomObject1Id()))) &&
            ((this.customObject2Id==null && other.getCustomObject2Id()==null) || 
             (this.customObject2Id!=null &&
              this.customObject2Id.equals(other.getCustomObject2Id()))) &&
            ((this.customObject3Id==null && other.getCustomObject3Id()==null) || 
             (this.customObject3Id!=null &&
              this.customObject3Id.equals(other.getCustomObject3Id()))) &&
            ((this.customObject4Id==null && other.getCustomObject4Id()==null) || 
             (this.customObject4Id!=null &&
              this.customObject4Id.equals(other.getCustomObject4Id()))) &&
            ((this.customObject5Id==null && other.getCustomObject5Id()==null) || 
             (this.customObject5Id!=null &&
              this.customObject5Id.equals(other.getCustomObject5Id()))) &&
            ((this.customObject6Id==null && other.getCustomObject6Id()==null) || 
             (this.customObject6Id!=null &&
              this.customObject6Id.equals(other.getCustomObject6Id()))) &&
            ((this.customObject7Id==null && other.getCustomObject7Id()==null) || 
             (this.customObject7Id!=null &&
              this.customObject7Id.equals(other.getCustomObject7Id()))) &&
            ((this.customObject8Id==null && other.getCustomObject8Id()==null) || 
             (this.customObject8Id!=null &&
              this.customObject8Id.equals(other.getCustomObject8Id()))) &&
            ((this.customObject9Id==null && other.getCustomObject9Id()==null) || 
             (this.customObject9Id!=null &&
              this.customObject9Id.equals(other.getCustomObject9Id()))) &&
            ((this.customObject10Id==null && other.getCustomObject10Id()==null) || 
             (this.customObject10Id!=null &&
              this.customObject10Id.equals(other.getCustomObject10Id()))) &&
            ((this.customObject11Id==null && other.getCustomObject11Id()==null) || 
             (this.customObject11Id!=null &&
              this.customObject11Id.equals(other.getCustomObject11Id()))) &&
            ((this.customObject12Id==null && other.getCustomObject12Id()==null) || 
             (this.customObject12Id!=null &&
              this.customObject12Id.equals(other.getCustomObject12Id()))) &&
            ((this.customObject13Id==null && other.getCustomObject13Id()==null) || 
             (this.customObject13Id!=null &&
              this.customObject13Id.equals(other.getCustomObject13Id()))) &&
            ((this.customObject14Id==null && other.getCustomObject14Id()==null) || 
             (this.customObject14Id!=null &&
              this.customObject14Id.equals(other.getCustomObject14Id()))) &&
            ((this.customObject15Id==null && other.getCustomObject15Id()==null) || 
             (this.customObject15Id!=null &&
              this.customObject15Id.equals(other.getCustomObject15Id()))) &&
            ((this.dealerId==null && other.getDealerId()==null) || 
             (this.dealerId!=null &&
              this.dealerId.equals(other.getDealerId()))) &&
            ((this.fundId==null && other.getFundId()==null) || 
             (this.fundId!=null &&
              this.fundId.equals(other.getFundId()))) &&
            ((this.fundRequestId==null && other.getFundRequestId()==null) || 
             (this.fundRequestId!=null &&
              this.fundRequestId.equals(other.getFundRequestId()))) &&
            ((this.householdId==null && other.getHouseholdId()==null) || 
             (this.householdId!=null &&
              this.householdId.equals(other.getHouseholdId()))) &&
            ((this.leadId==null && other.getLeadId()==null) || 
             (this.leadId!=null &&
              this.leadId.equals(other.getLeadId()))) &&
            ((this.medEdId==null && other.getMedEdId()==null) || 
             (this.medEdId!=null &&
              this.medEdId.equals(other.getMedEdId()))) &&
            ((this.opportunityId==null && other.getOpportunityId()==null) || 
             (this.opportunityId!=null &&
              this.opportunityId.equals(other.getOpportunityId()))) &&
            ((this.portfolioId==null && other.getPortfolioId()==null) || 
             (this.portfolioId!=null &&
              this.portfolioId.equals(other.getPortfolioId()))) &&
            ((this.productId==null && other.getProductId()==null) || 
             (this.productId!=null &&
              this.productId.equals(other.getProductId()))) &&
            ((this.serviceRequestId==null && other.getServiceRequestId()==null) || 
             (this.serviceRequestId!=null &&
              this.serviceRequestId.equals(other.getServiceRequestId()))) &&
            ((this.solutionId==null && other.getSolutionId()==null) || 
             (this.solutionId!=null &&
              this.solutionId.equals(other.getSolutionId()))) &&
            ((this.vehicleId==null && other.getVehicleId()==null) || 
             (this.vehicleId!=null &&
              this.vehicleId.equals(other.getVehicleId()))) &&
            ((this.ownerId==null && other.getOwnerId()==null) || 
             (this.ownerId!=null &&
              this.ownerId.equals(other.getOwnerId()))) &&
            ((this.updatedByAlias==null && other.getUpdatedByAlias()==null) || 
             (this.updatedByAlias!=null &&
              this.updatedByAlias.equals(other.getUpdatedByAlias()))) &&
            ((this.updatedByEMailAddr==null && other.getUpdatedByEMailAddr()==null) || 
             (this.updatedByEMailAddr!=null &&
              this.updatedByEMailAddr.equals(other.getUpdatedByEMailAddr()))) &&
            ((this.updatedByExternalSystemId==null && other.getUpdatedByExternalSystemId()==null) || 
             (this.updatedByExternalSystemId!=null &&
              this.updatedByExternalSystemId.equals(other.getUpdatedByExternalSystemId()))) &&
            ((this.updatedByFirstName==null && other.getUpdatedByFirstName()==null) || 
             (this.updatedByFirstName!=null &&
              this.updatedByFirstName.equals(other.getUpdatedByFirstName()))) &&
            ((this.updatedByFullName==null && other.getUpdatedByFullName()==null) || 
             (this.updatedByFullName!=null &&
              this.updatedByFullName.equals(other.getUpdatedByFullName()))) &&
            ((this.updatedByIntegrationId==null && other.getUpdatedByIntegrationId()==null) || 
             (this.updatedByIntegrationId!=null &&
              this.updatedByIntegrationId.equals(other.getUpdatedByIntegrationId()))) &&
            ((this.updatedByLastName==null && other.getUpdatedByLastName()==null) || 
             (this.updatedByLastName!=null &&
              this.updatedByLastName.equals(other.getUpdatedByLastName()))) &&
            ((this.updatedByUserSignInId==null && other.getUpdatedByUserSignInId()==null) || 
             (this.updatedByUserSignInId!=null &&
              this.updatedByUserSignInId.equals(other.getUpdatedByUserSignInId()))) &&
            ((this.createdByAlias==null && other.getCreatedByAlias()==null) || 
             (this.createdByAlias!=null &&
              this.createdByAlias.equals(other.getCreatedByAlias()))) &&
            ((this.createdByEMailAddr==null && other.getCreatedByEMailAddr()==null) || 
             (this.createdByEMailAddr!=null &&
              this.createdByEMailAddr.equals(other.getCreatedByEMailAddr()))) &&
            ((this.createdByExternalSystemId==null && other.getCreatedByExternalSystemId()==null) || 
             (this.createdByExternalSystemId!=null &&
              this.createdByExternalSystemId.equals(other.getCreatedByExternalSystemId()))) &&
            ((this.createdByFirstName==null && other.getCreatedByFirstName()==null) || 
             (this.createdByFirstName!=null &&
              this.createdByFirstName.equals(other.getCreatedByFirstName()))) &&
            ((this.createdByFullName==null && other.getCreatedByFullName()==null) || 
             (this.createdByFullName!=null &&
              this.createdByFullName.equals(other.getCreatedByFullName()))) &&
            ((this.createdByIntegrationId==null && other.getCreatedByIntegrationId()==null) || 
             (this.createdByIntegrationId!=null &&
              this.createdByIntegrationId.equals(other.getCreatedByIntegrationId()))) &&
            ((this.createdByLastName==null && other.getCreatedByLastName()==null) || 
             (this.createdByLastName!=null &&
              this.createdByLastName.equals(other.getCreatedByLastName()))) &&
            ((this.createdByUserSignInId==null && other.getCreatedByUserSignInId()==null) || 
             (this.createdByUserSignInId!=null &&
              this.createdByUserSignInId.equals(other.getCreatedByUserSignInId()))) &&
            ((this.accountExternalSystemId==null && other.getAccountExternalSystemId()==null) || 
             (this.accountExternalSystemId!=null &&
              this.accountExternalSystemId.equals(other.getAccountExternalSystemId()))) &&
            ((this.accountIntegrationId==null && other.getAccountIntegrationId()==null) || 
             (this.accountIntegrationId!=null &&
              this.accountIntegrationId.equals(other.getAccountIntegrationId()))) &&
            ((this.accountLocation==null && other.getAccountLocation()==null) || 
             (this.accountLocation!=null &&
              this.accountLocation.equals(other.getAccountLocation()))) &&
            ((this.accountName==null && other.getAccountName()==null) || 
             (this.accountName!=null &&
              this.accountName.equals(other.getAccountName()))) &&
            ((this.activitySubject==null && other.getActivitySubject()==null) || 
             (this.activitySubject!=null &&
              this.activitySubject.equals(other.getActivitySubject()))) &&
            ((this.activityExternalSystemId==null && other.getActivityExternalSystemId()==null) || 
             (this.activityExternalSystemId!=null &&
              this.activityExternalSystemId.equals(other.getActivityExternalSystemId()))) &&
            ((this.activityIntegrationId==null && other.getActivityIntegrationId()==null) || 
             (this.activityIntegrationId!=null &&
              this.activityIntegrationId.equals(other.getActivityIntegrationId()))) &&
            ((this.assetIntegrationId==null && other.getAssetIntegrationId()==null) || 
             (this.assetIntegrationId!=null &&
              this.assetIntegrationId.equals(other.getAssetIntegrationId()))) &&
            ((this.assetProduct==null && other.getAssetProduct()==null) || 
             (this.assetProduct!=null &&
              this.assetProduct.equals(other.getAssetProduct()))) &&
            ((this.assetSerialNumber==null && other.getAssetSerialNumber()==null) || 
             (this.assetSerialNumber!=null &&
              this.assetSerialNumber.equals(other.getAssetSerialNumber()))) &&
            ((this.assetExternalSystemId==null && other.getAssetExternalSystemId()==null) || 
             (this.assetExternalSystemId!=null &&
              this.assetExternalSystemId.equals(other.getAssetExternalSystemId()))) &&
            ((this.campaignExternalSystemId==null && other.getCampaignExternalSystemId()==null) || 
             (this.campaignExternalSystemId!=null &&
              this.campaignExternalSystemId.equals(other.getCampaignExternalSystemId()))) &&
            ((this.campaignIntegrationId==null && other.getCampaignIntegrationId()==null) || 
             (this.campaignIntegrationId!=null &&
              this.campaignIntegrationId.equals(other.getCampaignIntegrationId()))) &&
            ((this.campaignName==null && other.getCampaignName()==null) || 
             (this.campaignName!=null &&
              this.campaignName.equals(other.getCampaignName()))) &&
            ((this.contactEmail==null && other.getContactEmail()==null) || 
             (this.contactEmail!=null &&
              this.contactEmail.equals(other.getContactEmail()))) &&
            ((this.contactExternalSystemId==null && other.getContactExternalSystemId()==null) || 
             (this.contactExternalSystemId!=null &&
              this.contactExternalSystemId.equals(other.getContactExternalSystemId()))) &&
            ((this.contactFirstName==null && other.getContactFirstName()==null) || 
             (this.contactFirstName!=null &&
              this.contactFirstName.equals(other.getContactFirstName()))) &&
            ((this.contactFullName==null && other.getContactFullName()==null) || 
             (this.contactFullName!=null &&
              this.contactFullName.equals(other.getContactFullName()))) &&
            ((this.contactIntegrationId==null && other.getContactIntegrationId()==null) || 
             (this.contactIntegrationId!=null &&
              this.contactIntegrationId.equals(other.getContactIntegrationId()))) &&
            ((this.contactLastName==null && other.getContactLastName()==null) || 
             (this.contactLastName!=null &&
              this.contactLastName.equals(other.getContactLastName()))) &&
            ((this.customObject1ExternalSystemId==null && other.getCustomObject1ExternalSystemId()==null) || 
             (this.customObject1ExternalSystemId!=null &&
              this.customObject1ExternalSystemId.equals(other.getCustomObject1ExternalSystemId()))) &&
            ((this.customObject1IntegrationId==null && other.getCustomObject1IntegrationId()==null) || 
             (this.customObject1IntegrationId!=null &&
              this.customObject1IntegrationId.equals(other.getCustomObject1IntegrationId()))) &&
            ((this.customObject1Name==null && other.getCustomObject1Name()==null) || 
             (this.customObject1Name!=null &&
              this.customObject1Name.equals(other.getCustomObject1Name()))) &&
            ((this.customObject2ExternalSystemId==null && other.getCustomObject2ExternalSystemId()==null) || 
             (this.customObject2ExternalSystemId!=null &&
              this.customObject2ExternalSystemId.equals(other.getCustomObject2ExternalSystemId()))) &&
            ((this.customObject2IntegrationId==null && other.getCustomObject2IntegrationId()==null) || 
             (this.customObject2IntegrationId!=null &&
              this.customObject2IntegrationId.equals(other.getCustomObject2IntegrationId()))) &&
            ((this.customObject2Name==null && other.getCustomObject2Name()==null) || 
             (this.customObject2Name!=null &&
              this.customObject2Name.equals(other.getCustomObject2Name()))) &&
            ((this.customObject3IntegrationId==null && other.getCustomObject3IntegrationId()==null) || 
             (this.customObject3IntegrationId!=null &&
              this.customObject3IntegrationId.equals(other.getCustomObject3IntegrationId()))) &&
            ((this.customObject3ExternalSystemId==null && other.getCustomObject3ExternalSystemId()==null) || 
             (this.customObject3ExternalSystemId!=null &&
              this.customObject3ExternalSystemId.equals(other.getCustomObject3ExternalSystemId()))) &&
            ((this.customObject3Name==null && other.getCustomObject3Name()==null) || 
             (this.customObject3Name!=null &&
              this.customObject3Name.equals(other.getCustomObject3Name()))) &&
            ((this.customObject4Name==null && other.getCustomObject4Name()==null) || 
             (this.customObject4Name!=null &&
              this.customObject4Name.equals(other.getCustomObject4Name()))) &&
            ((this.customObject4ExternalSystemId==null && other.getCustomObject4ExternalSystemId()==null) || 
             (this.customObject4ExternalSystemId!=null &&
              this.customObject4ExternalSystemId.equals(other.getCustomObject4ExternalSystemId()))) &&
            ((this.customObject4IntegrationId==null && other.getCustomObject4IntegrationId()==null) || 
             (this.customObject4IntegrationId!=null &&
              this.customObject4IntegrationId.equals(other.getCustomObject4IntegrationId()))) &&
            ((this.customObject5Name==null && other.getCustomObject5Name()==null) || 
             (this.customObject5Name!=null &&
              this.customObject5Name.equals(other.getCustomObject5Name()))) &&
            ((this.customObject5ExternalSystemId==null && other.getCustomObject5ExternalSystemId()==null) || 
             (this.customObject5ExternalSystemId!=null &&
              this.customObject5ExternalSystemId.equals(other.getCustomObject5ExternalSystemId()))) &&
            ((this.customObject5IntegrationId==null && other.getCustomObject5IntegrationId()==null) || 
             (this.customObject5IntegrationId!=null &&
              this.customObject5IntegrationId.equals(other.getCustomObject5IntegrationId()))) &&
            ((this.customObject6Name==null && other.getCustomObject6Name()==null) || 
             (this.customObject6Name!=null &&
              this.customObject6Name.equals(other.getCustomObject6Name()))) &&
            ((this.customObject6ExternalSystemId==null && other.getCustomObject6ExternalSystemId()==null) || 
             (this.customObject6ExternalSystemId!=null &&
              this.customObject6ExternalSystemId.equals(other.getCustomObject6ExternalSystemId()))) &&
            ((this.customObject6IntegrationId==null && other.getCustomObject6IntegrationId()==null) || 
             (this.customObject6IntegrationId!=null &&
              this.customObject6IntegrationId.equals(other.getCustomObject6IntegrationId()))) &&
            ((this.customObject7Name==null && other.getCustomObject7Name()==null) || 
             (this.customObject7Name!=null &&
              this.customObject7Name.equals(other.getCustomObject7Name()))) &&
            ((this.customObject7ExternalSystemId==null && other.getCustomObject7ExternalSystemId()==null) || 
             (this.customObject7ExternalSystemId!=null &&
              this.customObject7ExternalSystemId.equals(other.getCustomObject7ExternalSystemId()))) &&
            ((this.customObject7IntegrationId==null && other.getCustomObject7IntegrationId()==null) || 
             (this.customObject7IntegrationId!=null &&
              this.customObject7IntegrationId.equals(other.getCustomObject7IntegrationId()))) &&
            ((this.customObject8Name==null && other.getCustomObject8Name()==null) || 
             (this.customObject8Name!=null &&
              this.customObject8Name.equals(other.getCustomObject8Name()))) &&
            ((this.customObject8ExternalSystemId==null && other.getCustomObject8ExternalSystemId()==null) || 
             (this.customObject8ExternalSystemId!=null &&
              this.customObject8ExternalSystemId.equals(other.getCustomObject8ExternalSystemId()))) &&
            ((this.customObject8IntegrationId==null && other.getCustomObject8IntegrationId()==null) || 
             (this.customObject8IntegrationId!=null &&
              this.customObject8IntegrationId.equals(other.getCustomObject8IntegrationId()))) &&
            ((this.customObject9Name==null && other.getCustomObject9Name()==null) || 
             (this.customObject9Name!=null &&
              this.customObject9Name.equals(other.getCustomObject9Name()))) &&
            ((this.customObject9ExternalSystemId==null && other.getCustomObject9ExternalSystemId()==null) || 
             (this.customObject9ExternalSystemId!=null &&
              this.customObject9ExternalSystemId.equals(other.getCustomObject9ExternalSystemId()))) &&
            ((this.customObject9IntegrationId==null && other.getCustomObject9IntegrationId()==null) || 
             (this.customObject9IntegrationId!=null &&
              this.customObject9IntegrationId.equals(other.getCustomObject9IntegrationId()))) &&
            ((this.customObject10Name==null && other.getCustomObject10Name()==null) || 
             (this.customObject10Name!=null &&
              this.customObject10Name.equals(other.getCustomObject10Name()))) &&
            ((this.customObject10ExternalSystemId==null && other.getCustomObject10ExternalSystemId()==null) || 
             (this.customObject10ExternalSystemId!=null &&
              this.customObject10ExternalSystemId.equals(other.getCustomObject10ExternalSystemId()))) &&
            ((this.customObject10IntegrationId==null && other.getCustomObject10IntegrationId()==null) || 
             (this.customObject10IntegrationId!=null &&
              this.customObject10IntegrationId.equals(other.getCustomObject10IntegrationId()))) &&
            ((this.customObject11Name==null && other.getCustomObject11Name()==null) || 
             (this.customObject11Name!=null &&
              this.customObject11Name.equals(other.getCustomObject11Name()))) &&
            ((this.customObject11ExternalSystemId==null && other.getCustomObject11ExternalSystemId()==null) || 
             (this.customObject11ExternalSystemId!=null &&
              this.customObject11ExternalSystemId.equals(other.getCustomObject11ExternalSystemId()))) &&
            ((this.customObject11IntegrationId==null && other.getCustomObject11IntegrationId()==null) || 
             (this.customObject11IntegrationId!=null &&
              this.customObject11IntegrationId.equals(other.getCustomObject11IntegrationId()))) &&
            ((this.customObject12Name==null && other.getCustomObject12Name()==null) || 
             (this.customObject12Name!=null &&
              this.customObject12Name.equals(other.getCustomObject12Name()))) &&
            ((this.customObject12ExternalSystemId==null && other.getCustomObject12ExternalSystemId()==null) || 
             (this.customObject12ExternalSystemId!=null &&
              this.customObject12ExternalSystemId.equals(other.getCustomObject12ExternalSystemId()))) &&
            ((this.customObject12IntegrationId==null && other.getCustomObject12IntegrationId()==null) || 
             (this.customObject12IntegrationId!=null &&
              this.customObject12IntegrationId.equals(other.getCustomObject12IntegrationId()))) &&
            ((this.customObject13Name==null && other.getCustomObject13Name()==null) || 
             (this.customObject13Name!=null &&
              this.customObject13Name.equals(other.getCustomObject13Name()))) &&
            ((this.customObject13ExternalSystemId==null && other.getCustomObject13ExternalSystemId()==null) || 
             (this.customObject13ExternalSystemId!=null &&
              this.customObject13ExternalSystemId.equals(other.getCustomObject13ExternalSystemId()))) &&
            ((this.customObject13IntegrationId==null && other.getCustomObject13IntegrationId()==null) || 
             (this.customObject13IntegrationId!=null &&
              this.customObject13IntegrationId.equals(other.getCustomObject13IntegrationId()))) &&
            ((this.customObject14Name==null && other.getCustomObject14Name()==null) || 
             (this.customObject14Name!=null &&
              this.customObject14Name.equals(other.getCustomObject14Name()))) &&
            ((this.customObject14ExternalSystemId==null && other.getCustomObject14ExternalSystemId()==null) || 
             (this.customObject14ExternalSystemId!=null &&
              this.customObject14ExternalSystemId.equals(other.getCustomObject14ExternalSystemId()))) &&
            ((this.customObject14IntegrationId==null && other.getCustomObject14IntegrationId()==null) || 
             (this.customObject14IntegrationId!=null &&
              this.customObject14IntegrationId.equals(other.getCustomObject14IntegrationId()))) &&
            ((this.customObject15Name==null && other.getCustomObject15Name()==null) || 
             (this.customObject15Name!=null &&
              this.customObject15Name.equals(other.getCustomObject15Name()))) &&
            ((this.customObject15ExternalSystemId==null && other.getCustomObject15ExternalSystemId()==null) || 
             (this.customObject15ExternalSystemId!=null &&
              this.customObject15ExternalSystemId.equals(other.getCustomObject15ExternalSystemId()))) &&
            ((this.customObject15IntegrationId==null && other.getCustomObject15IntegrationId()==null) || 
             (this.customObject15IntegrationId!=null &&
              this.customObject15IntegrationId.equals(other.getCustomObject15IntegrationId()))) &&
            ((this.dealerName==null && other.getDealerName()==null) || 
             (this.dealerName!=null &&
              this.dealerName.equals(other.getDealerName()))) &&
            ((this.dealerExternalSystemId==null && other.getDealerExternalSystemId()==null) || 
             (this.dealerExternalSystemId!=null &&
              this.dealerExternalSystemId.equals(other.getDealerExternalSystemId()))) &&
            ((this.dealerIntegrationId==null && other.getDealerIntegrationId()==null) || 
             (this.dealerIntegrationId!=null &&
              this.dealerIntegrationId.equals(other.getDealerIntegrationId()))) &&
            ((this.fundName==null && other.getFundName()==null) || 
             (this.fundName!=null &&
              this.fundName.equals(other.getFundName()))) &&
            ((this.fundRequestName==null && other.getFundRequestName()==null) || 
             (this.fundRequestName!=null &&
              this.fundRequestName.equals(other.getFundRequestName()))) &&
            ((this.fundRequestExternalSystemId==null && other.getFundRequestExternalSystemId()==null) || 
             (this.fundRequestExternalSystemId!=null &&
              this.fundRequestExternalSystemId.equals(other.getFundRequestExternalSystemId()))) &&
            ((this.householdName==null && other.getHouseholdName()==null) || 
             (this.householdName!=null &&
              this.householdName.equals(other.getHouseholdName()))) &&
            ((this.householdIntegrationId==null && other.getHouseholdIntegrationId()==null) || 
             (this.householdIntegrationId!=null &&
              this.householdIntegrationId.equals(other.getHouseholdIntegrationId()))) &&
            ((this.householdExternalSystemId==null && other.getHouseholdExternalSystemId()==null) || 
             (this.householdExternalSystemId!=null &&
              this.householdExternalSystemId.equals(other.getHouseholdExternalSystemId()))) &&
            ((this.leadExternalSystemId==null && other.getLeadExternalSystemId()==null) || 
             (this.leadExternalSystemId!=null &&
              this.leadExternalSystemId.equals(other.getLeadExternalSystemId()))) &&
            ((this.leadFirstName==null && other.getLeadFirstName()==null) || 
             (this.leadFirstName!=null &&
              this.leadFirstName.equals(other.getLeadFirstName()))) &&
            ((this.leadFullName==null && other.getLeadFullName()==null) || 
             (this.leadFullName!=null &&
              this.leadFullName.equals(other.getLeadFullName()))) &&
            ((this.leadIntegrationId==null && other.getLeadIntegrationId()==null) || 
             (this.leadIntegrationId!=null &&
              this.leadIntegrationId.equals(other.getLeadIntegrationId()))) &&
            ((this.leadLastName==null && other.getLeadLastName()==null) || 
             (this.leadLastName!=null &&
              this.leadLastName.equals(other.getLeadLastName()))) &&
            ((this.medEdName==null && other.getMedEdName()==null) || 
             (this.medEdName!=null &&
              this.medEdName.equals(other.getMedEdName()))) &&
            ((this.medEdExternalSystemId==null && other.getMedEdExternalSystemId()==null) || 
             (this.medEdExternalSystemId!=null &&
              this.medEdExternalSystemId.equals(other.getMedEdExternalSystemId()))) &&
            ((this.medEdIntegrationId==null && other.getMedEdIntegrationId()==null) || 
             (this.medEdIntegrationId!=null &&
              this.medEdIntegrationId.equals(other.getMedEdIntegrationId()))) &&
            ((this.opportunityAccountId==null && other.getOpportunityAccountId()==null) || 
             (this.opportunityAccountId!=null &&
              this.opportunityAccountId.equals(other.getOpportunityAccountId()))) &&
            ((this.opportunityExternalSystemId==null && other.getOpportunityExternalSystemId()==null) || 
             (this.opportunityExternalSystemId!=null &&
              this.opportunityExternalSystemId.equals(other.getOpportunityExternalSystemId()))) &&
            ((this.opportunityIntegrationId==null && other.getOpportunityIntegrationId()==null) || 
             (this.opportunityIntegrationId!=null &&
              this.opportunityIntegrationId.equals(other.getOpportunityIntegrationId()))) &&
            ((this.opportunityName==null && other.getOpportunityName()==null) || 
             (this.opportunityName!=null &&
              this.opportunityName.equals(other.getOpportunityName()))) &&
            ((this.portfolioAccountNumber==null && other.getPortfolioAccountNumber()==null) || 
             (this.portfolioAccountNumber!=null &&
              this.portfolioAccountNumber.equals(other.getPortfolioAccountNumber()))) &&
            ((this.portfolioExternalSystemId==null && other.getPortfolioExternalSystemId()==null) || 
             (this.portfolioExternalSystemId!=null &&
              this.portfolioExternalSystemId.equals(other.getPortfolioExternalSystemId()))) &&
            ((this.portfolioIntegrationId==null && other.getPortfolioIntegrationId()==null) || 
             (this.portfolioIntegrationId!=null &&
              this.portfolioIntegrationId.equals(other.getPortfolioIntegrationId()))) &&
            ((this.productIntegrationId==null && other.getProductIntegrationId()==null) || 
             (this.productIntegrationId!=null &&
              this.productIntegrationId.equals(other.getProductIntegrationId()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.productExternalSystemId==null && other.getProductExternalSystemId()==null) || 
             (this.productExternalSystemId!=null &&
              this.productExternalSystemId.equals(other.getProductExternalSystemId()))) &&
            ((this.serviceRequestExternalSystemId==null && other.getServiceRequestExternalSystemId()==null) || 
             (this.serviceRequestExternalSystemId!=null &&
              this.serviceRequestExternalSystemId.equals(other.getServiceRequestExternalSystemId()))) &&
            ((this.serviceRequestIntegrationId==null && other.getServiceRequestIntegrationId()==null) || 
             (this.serviceRequestIntegrationId!=null &&
              this.serviceRequestIntegrationId.equals(other.getServiceRequestIntegrationId()))) &&
            ((this.serviceRequestSRNumber==null && other.getServiceRequestSRNumber()==null) || 
             (this.serviceRequestSRNumber!=null &&
              this.serviceRequestSRNumber.equals(other.getServiceRequestSRNumber()))) &&
            ((this.solutionExternalSystemId==null && other.getSolutionExternalSystemId()==null) || 
             (this.solutionExternalSystemId!=null &&
              this.solutionExternalSystemId.equals(other.getSolutionExternalSystemId()))) &&
            ((this.solutionIntegrationId==null && other.getSolutionIntegrationId()==null) || 
             (this.solutionIntegrationId!=null &&
              this.solutionIntegrationId.equals(other.getSolutionIntegrationId()))) &&
            ((this.solutionTitle==null && other.getSolutionTitle()==null) || 
             (this.solutionTitle!=null &&
              this.solutionTitle.equals(other.getSolutionTitle()))) &&
            ((this.vehicleIntegrationId==null && other.getVehicleIntegrationId()==null) || 
             (this.vehicleIntegrationId!=null &&
              this.vehicleIntegrationId.equals(other.getVehicleIntegrationId()))) &&
            ((this.vehicleVIN==null && other.getVehicleVIN()==null) || 
             (this.vehicleVIN!=null &&
              this.vehicleVIN.equals(other.getVehicleVIN()))) &&
            ((this.vehicleExternalSystemId==null && other.getVehicleExternalSystemId()==null) || 
             (this.vehicleExternalSystemId!=null &&
              this.vehicleExternalSystemId.equals(other.getVehicleExternalSystemId()))) &&
            ((this.ownerAlias==null && other.getOwnerAlias()==null) || 
             (this.ownerAlias!=null &&
              this.ownerAlias.equals(other.getOwnerAlias()))) &&
            ((this.ownerEMailAddr==null && other.getOwnerEMailAddr()==null) || 
             (this.ownerEMailAddr!=null &&
              this.ownerEMailAddr.equals(other.getOwnerEMailAddr()))) &&
            ((this.ownerExternalSystemId==null && other.getOwnerExternalSystemId()==null) || 
             (this.ownerExternalSystemId!=null &&
              this.ownerExternalSystemId.equals(other.getOwnerExternalSystemId()))) &&
            ((this.ownerFirstName==null && other.getOwnerFirstName()==null) || 
             (this.ownerFirstName!=null &&
              this.ownerFirstName.equals(other.getOwnerFirstName()))) &&
            ((this.ownerFullName==null && other.getOwnerFullName()==null) || 
             (this.ownerFullName!=null &&
              this.ownerFullName.equals(other.getOwnerFullName()))) &&
            ((this.ownerIntegrationId==null && other.getOwnerIntegrationId()==null) || 
             (this.ownerIntegrationId!=null &&
              this.ownerIntegrationId.equals(other.getOwnerIntegrationId()))) &&
            ((this.ownerLastName==null && other.getOwnerLastName()==null) || 
             (this.ownerLastName!=null &&
              this.ownerLastName.equals(other.getOwnerLastName()))) &&
            ((this.ownerUserSignInId==null && other.getOwnerUserSignInId()==null) || 
             (this.ownerUserSignInId!=null &&
              this.ownerUserSignInId.equals(other.getOwnerUserSignInId()))) &&
            ((this.createdBy==null && other.getCreatedBy()==null) || 
             (this.createdBy!=null &&
              this.createdBy.equals(other.getCreatedBy()))) &&
            ((this.modifiedBy==null && other.getModifiedBy()==null) || 
             (this.modifiedBy!=null &&
              this.modifiedBy.equals(other.getModifiedBy()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.searchspec==null && other.getSearchspec()==null) || 
             (this.searchspec!=null &&
              this.searchspec.equals(other.getSearchspec())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getModifiedDate() != null) {
            _hashCode += getModifiedDate().hashCode();
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        if (getModifiedById() != null) {
            _hashCode += getModifiedById().hashCode();
        }
        if (getCreatedById() != null) {
            _hashCode += getCreatedById().hashCode();
        }
        if (getModId() != null) {
            _hashCode += getModId().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getQuickSearch1() != null) {
            _hashCode += getQuickSearch1().hashCode();
        }
        if (getQuickSearch2() != null) {
            _hashCode += getQuickSearch2().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getExchangeDate() != null) {
            _hashCode += getExchangeDate().hashCode();
        }
        if (getExternalSystemId() != null) {
            _hashCode += getExternalSystemId().hashCode();
        }
        if (getIntegrationId() != null) {
            _hashCode += getIntegrationId().hashCode();
        }
        if (getIndexedBoolean0() != null) {
            _hashCode += getIndexedBoolean0().hashCode();
        }
        if (getIndexedCurrency0() != null) {
            _hashCode += getIndexedCurrency0().hashCode();
        }
        if (getIndexedDate0() != null) {
            _hashCode += getIndexedDate0().hashCode();
        }
        if (getIndexedNumber0() != null) {
            _hashCode += getIndexedNumber0().hashCode();
        }
        if (getIndexedPick0() != null) {
            _hashCode += getIndexedPick0().hashCode();
        }
        if (getIndexedPick1() != null) {
            _hashCode += getIndexedPick1().hashCode();
        }
        if (getIndexedPick2() != null) {
            _hashCode += getIndexedPick2().hashCode();
        }
        if (getIndexedPick3() != null) {
            _hashCode += getIndexedPick3().hashCode();
        }
        if (getIndexedPick4() != null) {
            _hashCode += getIndexedPick4().hashCode();
        }
        if (getAccountId() != null) {
            _hashCode += getAccountId().hashCode();
        }
        if (getActivityId() != null) {
            _hashCode += getActivityId().hashCode();
        }
        if (getAssetId() != null) {
            _hashCode += getAssetId().hashCode();
        }
        if (getCampaignId() != null) {
            _hashCode += getCampaignId().hashCode();
        }
        if (getContactId() != null) {
            _hashCode += getContactId().hashCode();
        }
        if (getCustomObject1Id() != null) {
            _hashCode += getCustomObject1Id().hashCode();
        }
        if (getCustomObject2Id() != null) {
            _hashCode += getCustomObject2Id().hashCode();
        }
        if (getCustomObject3Id() != null) {
            _hashCode += getCustomObject3Id().hashCode();
        }
        if (getCustomObject4Id() != null) {
            _hashCode += getCustomObject4Id().hashCode();
        }
        if (getCustomObject5Id() != null) {
            _hashCode += getCustomObject5Id().hashCode();
        }
        if (getCustomObject6Id() != null) {
            _hashCode += getCustomObject6Id().hashCode();
        }
        if (getCustomObject7Id() != null) {
            _hashCode += getCustomObject7Id().hashCode();
        }
        if (getCustomObject8Id() != null) {
            _hashCode += getCustomObject8Id().hashCode();
        }
        if (getCustomObject9Id() != null) {
            _hashCode += getCustomObject9Id().hashCode();
        }
        if (getCustomObject10Id() != null) {
            _hashCode += getCustomObject10Id().hashCode();
        }
        if (getCustomObject11Id() != null) {
            _hashCode += getCustomObject11Id().hashCode();
        }
        if (getCustomObject12Id() != null) {
            _hashCode += getCustomObject12Id().hashCode();
        }
        if (getCustomObject13Id() != null) {
            _hashCode += getCustomObject13Id().hashCode();
        }
        if (getCustomObject14Id() != null) {
            _hashCode += getCustomObject14Id().hashCode();
        }
        if (getCustomObject15Id() != null) {
            _hashCode += getCustomObject15Id().hashCode();
        }
        if (getDealerId() != null) {
            _hashCode += getDealerId().hashCode();
        }
        if (getFundId() != null) {
            _hashCode += getFundId().hashCode();
        }
        if (getFundRequestId() != null) {
            _hashCode += getFundRequestId().hashCode();
        }
        if (getHouseholdId() != null) {
            _hashCode += getHouseholdId().hashCode();
        }
        if (getLeadId() != null) {
            _hashCode += getLeadId().hashCode();
        }
        if (getMedEdId() != null) {
            _hashCode += getMedEdId().hashCode();
        }
        if (getOpportunityId() != null) {
            _hashCode += getOpportunityId().hashCode();
        }
        if (getPortfolioId() != null) {
            _hashCode += getPortfolioId().hashCode();
        }
        if (getProductId() != null) {
            _hashCode += getProductId().hashCode();
        }
        if (getServiceRequestId() != null) {
            _hashCode += getServiceRequestId().hashCode();
        }
        if (getSolutionId() != null) {
            _hashCode += getSolutionId().hashCode();
        }
        if (getVehicleId() != null) {
            _hashCode += getVehicleId().hashCode();
        }
        if (getOwnerId() != null) {
            _hashCode += getOwnerId().hashCode();
        }
        if (getUpdatedByAlias() != null) {
            _hashCode += getUpdatedByAlias().hashCode();
        }
        if (getUpdatedByEMailAddr() != null) {
            _hashCode += getUpdatedByEMailAddr().hashCode();
        }
        if (getUpdatedByExternalSystemId() != null) {
            _hashCode += getUpdatedByExternalSystemId().hashCode();
        }
        if (getUpdatedByFirstName() != null) {
            _hashCode += getUpdatedByFirstName().hashCode();
        }
        if (getUpdatedByFullName() != null) {
            _hashCode += getUpdatedByFullName().hashCode();
        }
        if (getUpdatedByIntegrationId() != null) {
            _hashCode += getUpdatedByIntegrationId().hashCode();
        }
        if (getUpdatedByLastName() != null) {
            _hashCode += getUpdatedByLastName().hashCode();
        }
        if (getUpdatedByUserSignInId() != null) {
            _hashCode += getUpdatedByUserSignInId().hashCode();
        }
        if (getCreatedByAlias() != null) {
            _hashCode += getCreatedByAlias().hashCode();
        }
        if (getCreatedByEMailAddr() != null) {
            _hashCode += getCreatedByEMailAddr().hashCode();
        }
        if (getCreatedByExternalSystemId() != null) {
            _hashCode += getCreatedByExternalSystemId().hashCode();
        }
        if (getCreatedByFirstName() != null) {
            _hashCode += getCreatedByFirstName().hashCode();
        }
        if (getCreatedByFullName() != null) {
            _hashCode += getCreatedByFullName().hashCode();
        }
        if (getCreatedByIntegrationId() != null) {
            _hashCode += getCreatedByIntegrationId().hashCode();
        }
        if (getCreatedByLastName() != null) {
            _hashCode += getCreatedByLastName().hashCode();
        }
        if (getCreatedByUserSignInId() != null) {
            _hashCode += getCreatedByUserSignInId().hashCode();
        }
        if (getAccountExternalSystemId() != null) {
            _hashCode += getAccountExternalSystemId().hashCode();
        }
        if (getAccountIntegrationId() != null) {
            _hashCode += getAccountIntegrationId().hashCode();
        }
        if (getAccountLocation() != null) {
            _hashCode += getAccountLocation().hashCode();
        }
        if (getAccountName() != null) {
            _hashCode += getAccountName().hashCode();
        }
        if (getActivitySubject() != null) {
            _hashCode += getActivitySubject().hashCode();
        }
        if (getActivityExternalSystemId() != null) {
            _hashCode += getActivityExternalSystemId().hashCode();
        }
        if (getActivityIntegrationId() != null) {
            _hashCode += getActivityIntegrationId().hashCode();
        }
        if (getAssetIntegrationId() != null) {
            _hashCode += getAssetIntegrationId().hashCode();
        }
        if (getAssetProduct() != null) {
            _hashCode += getAssetProduct().hashCode();
        }
        if (getAssetSerialNumber() != null) {
            _hashCode += getAssetSerialNumber().hashCode();
        }
        if (getAssetExternalSystemId() != null) {
            _hashCode += getAssetExternalSystemId().hashCode();
        }
        if (getCampaignExternalSystemId() != null) {
            _hashCode += getCampaignExternalSystemId().hashCode();
        }
        if (getCampaignIntegrationId() != null) {
            _hashCode += getCampaignIntegrationId().hashCode();
        }
        if (getCampaignName() != null) {
            _hashCode += getCampaignName().hashCode();
        }
        if (getContactEmail() != null) {
            _hashCode += getContactEmail().hashCode();
        }
        if (getContactExternalSystemId() != null) {
            _hashCode += getContactExternalSystemId().hashCode();
        }
        if (getContactFirstName() != null) {
            _hashCode += getContactFirstName().hashCode();
        }
        if (getContactFullName() != null) {
            _hashCode += getContactFullName().hashCode();
        }
        if (getContactIntegrationId() != null) {
            _hashCode += getContactIntegrationId().hashCode();
        }
        if (getContactLastName() != null) {
            _hashCode += getContactLastName().hashCode();
        }
        if (getCustomObject1ExternalSystemId() != null) {
            _hashCode += getCustomObject1ExternalSystemId().hashCode();
        }
        if (getCustomObject1IntegrationId() != null) {
            _hashCode += getCustomObject1IntegrationId().hashCode();
        }
        if (getCustomObject1Name() != null) {
            _hashCode += getCustomObject1Name().hashCode();
        }
        if (getCustomObject2ExternalSystemId() != null) {
            _hashCode += getCustomObject2ExternalSystemId().hashCode();
        }
        if (getCustomObject2IntegrationId() != null) {
            _hashCode += getCustomObject2IntegrationId().hashCode();
        }
        if (getCustomObject2Name() != null) {
            _hashCode += getCustomObject2Name().hashCode();
        }
        if (getCustomObject3IntegrationId() != null) {
            _hashCode += getCustomObject3IntegrationId().hashCode();
        }
        if (getCustomObject3ExternalSystemId() != null) {
            _hashCode += getCustomObject3ExternalSystemId().hashCode();
        }
        if (getCustomObject3Name() != null) {
            _hashCode += getCustomObject3Name().hashCode();
        }
        if (getCustomObject4Name() != null) {
            _hashCode += getCustomObject4Name().hashCode();
        }
        if (getCustomObject4ExternalSystemId() != null) {
            _hashCode += getCustomObject4ExternalSystemId().hashCode();
        }
        if (getCustomObject4IntegrationId() != null) {
            _hashCode += getCustomObject4IntegrationId().hashCode();
        }
        if (getCustomObject5Name() != null) {
            _hashCode += getCustomObject5Name().hashCode();
        }
        if (getCustomObject5ExternalSystemId() != null) {
            _hashCode += getCustomObject5ExternalSystemId().hashCode();
        }
        if (getCustomObject5IntegrationId() != null) {
            _hashCode += getCustomObject5IntegrationId().hashCode();
        }
        if (getCustomObject6Name() != null) {
            _hashCode += getCustomObject6Name().hashCode();
        }
        if (getCustomObject6ExternalSystemId() != null) {
            _hashCode += getCustomObject6ExternalSystemId().hashCode();
        }
        if (getCustomObject6IntegrationId() != null) {
            _hashCode += getCustomObject6IntegrationId().hashCode();
        }
        if (getCustomObject7Name() != null) {
            _hashCode += getCustomObject7Name().hashCode();
        }
        if (getCustomObject7ExternalSystemId() != null) {
            _hashCode += getCustomObject7ExternalSystemId().hashCode();
        }
        if (getCustomObject7IntegrationId() != null) {
            _hashCode += getCustomObject7IntegrationId().hashCode();
        }
        if (getCustomObject8Name() != null) {
            _hashCode += getCustomObject8Name().hashCode();
        }
        if (getCustomObject8ExternalSystemId() != null) {
            _hashCode += getCustomObject8ExternalSystemId().hashCode();
        }
        if (getCustomObject8IntegrationId() != null) {
            _hashCode += getCustomObject8IntegrationId().hashCode();
        }
        if (getCustomObject9Name() != null) {
            _hashCode += getCustomObject9Name().hashCode();
        }
        if (getCustomObject9ExternalSystemId() != null) {
            _hashCode += getCustomObject9ExternalSystemId().hashCode();
        }
        if (getCustomObject9IntegrationId() != null) {
            _hashCode += getCustomObject9IntegrationId().hashCode();
        }
        if (getCustomObject10Name() != null) {
            _hashCode += getCustomObject10Name().hashCode();
        }
        if (getCustomObject10ExternalSystemId() != null) {
            _hashCode += getCustomObject10ExternalSystemId().hashCode();
        }
        if (getCustomObject10IntegrationId() != null) {
            _hashCode += getCustomObject10IntegrationId().hashCode();
        }
        if (getCustomObject11Name() != null) {
            _hashCode += getCustomObject11Name().hashCode();
        }
        if (getCustomObject11ExternalSystemId() != null) {
            _hashCode += getCustomObject11ExternalSystemId().hashCode();
        }
        if (getCustomObject11IntegrationId() != null) {
            _hashCode += getCustomObject11IntegrationId().hashCode();
        }
        if (getCustomObject12Name() != null) {
            _hashCode += getCustomObject12Name().hashCode();
        }
        if (getCustomObject12ExternalSystemId() != null) {
            _hashCode += getCustomObject12ExternalSystemId().hashCode();
        }
        if (getCustomObject12IntegrationId() != null) {
            _hashCode += getCustomObject12IntegrationId().hashCode();
        }
        if (getCustomObject13Name() != null) {
            _hashCode += getCustomObject13Name().hashCode();
        }
        if (getCustomObject13ExternalSystemId() != null) {
            _hashCode += getCustomObject13ExternalSystemId().hashCode();
        }
        if (getCustomObject13IntegrationId() != null) {
            _hashCode += getCustomObject13IntegrationId().hashCode();
        }
        if (getCustomObject14Name() != null) {
            _hashCode += getCustomObject14Name().hashCode();
        }
        if (getCustomObject14ExternalSystemId() != null) {
            _hashCode += getCustomObject14ExternalSystemId().hashCode();
        }
        if (getCustomObject14IntegrationId() != null) {
            _hashCode += getCustomObject14IntegrationId().hashCode();
        }
        if (getCustomObject15Name() != null) {
            _hashCode += getCustomObject15Name().hashCode();
        }
        if (getCustomObject15ExternalSystemId() != null) {
            _hashCode += getCustomObject15ExternalSystemId().hashCode();
        }
        if (getCustomObject15IntegrationId() != null) {
            _hashCode += getCustomObject15IntegrationId().hashCode();
        }
        if (getDealerName() != null) {
            _hashCode += getDealerName().hashCode();
        }
        if (getDealerExternalSystemId() != null) {
            _hashCode += getDealerExternalSystemId().hashCode();
        }
        if (getDealerIntegrationId() != null) {
            _hashCode += getDealerIntegrationId().hashCode();
        }
        if (getFundName() != null) {
            _hashCode += getFundName().hashCode();
        }
        if (getFundRequestName() != null) {
            _hashCode += getFundRequestName().hashCode();
        }
        if (getFundRequestExternalSystemId() != null) {
            _hashCode += getFundRequestExternalSystemId().hashCode();
        }
        if (getHouseholdName() != null) {
            _hashCode += getHouseholdName().hashCode();
        }
        if (getHouseholdIntegrationId() != null) {
            _hashCode += getHouseholdIntegrationId().hashCode();
        }
        if (getHouseholdExternalSystemId() != null) {
            _hashCode += getHouseholdExternalSystemId().hashCode();
        }
        if (getLeadExternalSystemId() != null) {
            _hashCode += getLeadExternalSystemId().hashCode();
        }
        if (getLeadFirstName() != null) {
            _hashCode += getLeadFirstName().hashCode();
        }
        if (getLeadFullName() != null) {
            _hashCode += getLeadFullName().hashCode();
        }
        if (getLeadIntegrationId() != null) {
            _hashCode += getLeadIntegrationId().hashCode();
        }
        if (getLeadLastName() != null) {
            _hashCode += getLeadLastName().hashCode();
        }
        if (getMedEdName() != null) {
            _hashCode += getMedEdName().hashCode();
        }
        if (getMedEdExternalSystemId() != null) {
            _hashCode += getMedEdExternalSystemId().hashCode();
        }
        if (getMedEdIntegrationId() != null) {
            _hashCode += getMedEdIntegrationId().hashCode();
        }
        if (getOpportunityAccountId() != null) {
            _hashCode += getOpportunityAccountId().hashCode();
        }
        if (getOpportunityExternalSystemId() != null) {
            _hashCode += getOpportunityExternalSystemId().hashCode();
        }
        if (getOpportunityIntegrationId() != null) {
            _hashCode += getOpportunityIntegrationId().hashCode();
        }
        if (getOpportunityName() != null) {
            _hashCode += getOpportunityName().hashCode();
        }
        if (getPortfolioAccountNumber() != null) {
            _hashCode += getPortfolioAccountNumber().hashCode();
        }
        if (getPortfolioExternalSystemId() != null) {
            _hashCode += getPortfolioExternalSystemId().hashCode();
        }
        if (getPortfolioIntegrationId() != null) {
            _hashCode += getPortfolioIntegrationId().hashCode();
        }
        if (getProductIntegrationId() != null) {
            _hashCode += getProductIntegrationId().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getProductExternalSystemId() != null) {
            _hashCode += getProductExternalSystemId().hashCode();
        }
        if (getServiceRequestExternalSystemId() != null) {
            _hashCode += getServiceRequestExternalSystemId().hashCode();
        }
        if (getServiceRequestIntegrationId() != null) {
            _hashCode += getServiceRequestIntegrationId().hashCode();
        }
        if (getServiceRequestSRNumber() != null) {
            _hashCode += getServiceRequestSRNumber().hashCode();
        }
        if (getSolutionExternalSystemId() != null) {
            _hashCode += getSolutionExternalSystemId().hashCode();
        }
        if (getSolutionIntegrationId() != null) {
            _hashCode += getSolutionIntegrationId().hashCode();
        }
        if (getSolutionTitle() != null) {
            _hashCode += getSolutionTitle().hashCode();
        }
        if (getVehicleIntegrationId() != null) {
            _hashCode += getVehicleIntegrationId().hashCode();
        }
        if (getVehicleVIN() != null) {
            _hashCode += getVehicleVIN().hashCode();
        }
        if (getVehicleExternalSystemId() != null) {
            _hashCode += getVehicleExternalSystemId().hashCode();
        }
        if (getOwnerAlias() != null) {
            _hashCode += getOwnerAlias().hashCode();
        }
        if (getOwnerEMailAddr() != null) {
            _hashCode += getOwnerEMailAddr().hashCode();
        }
        if (getOwnerExternalSystemId() != null) {
            _hashCode += getOwnerExternalSystemId().hashCode();
        }
        if (getOwnerFirstName() != null) {
            _hashCode += getOwnerFirstName().hashCode();
        }
        if (getOwnerFullName() != null) {
            _hashCode += getOwnerFullName().hashCode();
        }
        if (getOwnerIntegrationId() != null) {
            _hashCode += getOwnerIntegrationId().hashCode();
        }
        if (getOwnerLastName() != null) {
            _hashCode += getOwnerLastName().hashCode();
        }
        if (getOwnerUserSignInId() != null) {
            _hashCode += getOwnerUserSignInId().hashCode();
        }
        if (getCreatedBy() != null) {
            _hashCode += getCreatedBy().hashCode();
        }
        if (getModifiedBy() != null) {
            _hashCode += getModifiedBy().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getSearchspec() != null) {
            _hashCode += getSearchspec().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomObject6Query.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject6Query"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("searchspec");
        attrField.setXmlName(new javax.xml.namespace.QName("", "searchspec"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ModifiedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedById");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ModifiedById"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdById");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedById"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ModId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quickSearch1");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "QuickSearch1"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quickSearch2");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "QuickSearch2"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exchangeDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ExchangeDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("integrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedBoolean0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IndexedBoolean0"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedCurrency0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IndexedCurrency0"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedDate0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IndexedDate0"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedNumber0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IndexedNumber0"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IndexedPick0"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick1");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IndexedPick1"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick2");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IndexedPick2"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick3");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IndexedPick3"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick4");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "IndexedPick4"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activityId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ActivityId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AssetId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campaignId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CampaignId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ContactId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject1Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject1Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject2Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject2Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject3Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject3Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject4Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject4Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject5Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject5Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject6Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject6Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject7Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject7Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject8Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject8Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject9Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject9Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject10Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject10Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject11Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject11Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject12Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject12Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject13Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject13Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject14Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject14Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject15Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject15Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "DealerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "FundId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundRequestId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "FundRequestId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("householdId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "HouseholdId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "LeadId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medEdId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "MedEdId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OpportunityId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portfolioId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "PortfolioId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceRequestId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ServiceRequestId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solutionId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "SolutionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "VehicleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OwnerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByAlias");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "UpdatedByAlias"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByEMailAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "UpdatedByEMailAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "UpdatedByExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "UpdatedByFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "UpdatedByFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "UpdatedByIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "UpdatedByLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByUserSignInId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "UpdatedByUserSignInId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByAlias");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedByAlias"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByEMailAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedByEMailAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedByExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedByFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedByFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedByIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedByLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByUserSignInId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedByUserSignInId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AccountExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AccountIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AccountLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AccountName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activitySubject");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ActivitySubject"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activityExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ActivityExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activityIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ActivityIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AssetIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetProduct");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AssetProduct"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetSerialNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AssetSerialNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "AssetExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campaignExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CampaignExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campaignIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CampaignIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campaignName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CampaignName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ContactEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ContactExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ContactFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ContactFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ContactIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ContactLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject1ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject1ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject1IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject1IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject1Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject1Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject2ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject2ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject2IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject2IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject2Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject2Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject3IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject3IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject3ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject3ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject3Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject3Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject4Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject4Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject4ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject4ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject4IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject4IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject5Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject5Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject5ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject5ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject5IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject5IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject6Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject6Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject6ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject6ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject6IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject6IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject7Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject7Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject7ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject7ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject7IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject7IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject8Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject8Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject8ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject8ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject8IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject8IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject9Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject9Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject9ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject9ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject9IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject9IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject10Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject10Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject10ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject10ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject10IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject10IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject11Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject11Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject11ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject11ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject11IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject11IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject12Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject12Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject12ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject12ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject12IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject12IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject13Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject13Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject13ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject13ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject13IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject13IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject14Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject14Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject14ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject14ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject14IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject14IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject15Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject15Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject15ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject15ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject15IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject15IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "DealerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "DealerExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "DealerIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "FundName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundRequestName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "FundRequestName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundRequestExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "FundRequestExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("householdName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "HouseholdName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("householdIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "HouseholdIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("householdExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "HouseholdExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "LeadExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "LeadFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "LeadFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "LeadIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "LeadLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medEdName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "MedEdName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medEdExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "MedEdExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medEdIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "MedEdIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityAccountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OpportunityAccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OpportunityExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OpportunityIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OpportunityName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portfolioAccountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "PortfolioAccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portfolioExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "PortfolioExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portfolioIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "PortfolioIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ProductIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ProductName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ProductExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceRequestExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ServiceRequestExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceRequestIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ServiceRequestIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceRequestSRNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ServiceRequestSRNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solutionExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "SolutionExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solutionIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "SolutionIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solutionTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "SolutionTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "VehicleIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleVIN");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "VehicleVIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "VehicleExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerAlias");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OwnerAlias"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerEMailAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OwnerEMailAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OwnerExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OwnerFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OwnerFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OwnerIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OwnerLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerUserSignInId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "OwnerUserSignInId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdBy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CreatedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ModifiedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
