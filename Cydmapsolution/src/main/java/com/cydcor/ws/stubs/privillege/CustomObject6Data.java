package com.cydcor.ws.stubs.privillege;
/**
 * CustomObject6Data.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class CustomObject6Data  implements java.io.Serializable {
    private java.util.Calendar modifiedDate;

    private java.util.Calendar createdDate;

    private java.lang.String modifiedById;

    private java.lang.String createdById;

    private java.lang.Integer modId;

    private java.lang.String id;

    private java.lang.String name;

    private java.lang.String quickSearch1;

    private java.lang.String quickSearch2;

    private java.lang.String type;

    private java.lang.String currencyCode;

    private java.util.Date exchangeDate;

    private java.lang.String externalSystemId;

    private java.lang.String integrationId;

    private java.lang.Boolean indexedBoolean0;

    private java.math.BigDecimal indexedCurrency0;

    private java.util.Calendar indexedDate0;

    private java.math.BigDecimal indexedNumber0;

    private java.lang.String indexedPick0;

    private java.lang.String indexedPick1;

    private java.lang.String indexedPick2;

    private java.lang.String indexedPick3;

    private java.lang.String indexedPick4;

    private java.lang.String accountId;

    private java.lang.String activityId;

    private java.lang.String assetId;

    private java.lang.String campaignId;

    private java.lang.String contactId;

    private java.lang.String customObject1Id;

    private java.lang.String customObject2Id;

    private java.lang.String customObject3Id;

    private java.lang.String customObject4Id;

    private java.lang.String customObject5Id;

    private java.lang.String customObject6Id;

    private java.lang.String customObject7Id;

    private java.lang.String customObject8Id;

    private java.lang.String customObject9Id;

    private java.lang.String customObject10Id;

    private java.lang.String customObject11Id;

    private java.lang.String customObject12Id;

    private java.lang.String customObject13Id;

    private java.lang.String customObject14Id;

    private java.lang.String customObject15Id;

    private java.lang.String dealerId;

    private java.lang.String fundId;

    private java.lang.String fundRequestId;

    private java.lang.String householdId;

    private java.lang.String leadId;

    private java.lang.String medEdId;

    private java.lang.String opportunityId;

    private java.lang.String portfolioId;

    private java.lang.String productId;

    private java.lang.String serviceRequestId;

    private java.lang.String solutionId;

    private java.lang.String vehicleId;

    private java.lang.String ownerId;

    private java.lang.String updatedByAlias;

    private java.lang.String updatedByEMailAddr;

    private java.lang.String updatedByExternalSystemId;

    private java.lang.String updatedByFirstName;

    private java.lang.String updatedByFullName;

    private java.lang.String updatedByIntegrationId;

    private java.lang.String updatedByLastName;

    private java.lang.String updatedByUserSignInId;

    private java.lang.String createdByAlias;

    private java.lang.String createdByEMailAddr;

    private java.lang.String createdByExternalSystemId;

    private java.lang.String createdByFirstName;

    private java.lang.String createdByFullName;

    private java.lang.String createdByIntegrationId;

    private java.lang.String createdByLastName;

    private java.lang.String createdByUserSignInId;

    private java.lang.String accountExternalSystemId;

    private java.lang.String accountIntegrationId;

    private java.lang.String accountLocation;

    private java.lang.String accountName;

    private java.lang.String activitySubject;

    private java.lang.String activityExternalSystemId;

    private java.lang.String activityIntegrationId;

    private java.lang.String assetIntegrationId;

    private java.lang.String assetProduct;

    private java.lang.String assetSerialNumber;

    private java.lang.String assetExternalSystemId;

    private java.lang.String campaignExternalSystemId;

    private java.lang.String campaignIntegrationId;

    private java.lang.String campaignName;

    private java.lang.String contactEmail;

    private java.lang.String contactExternalSystemId;

    private java.lang.String contactFirstName;

    private java.lang.String contactFullName;

    private java.lang.String contactIntegrationId;

    private java.lang.String contactLastName;

    private java.lang.String customObject1ExternalSystemId;

    private java.lang.String customObject1IntegrationId;

    private java.lang.String customObject1Name;

    private java.lang.String customObject2ExternalSystemId;

    private java.lang.String customObject2IntegrationId;

    private java.lang.String customObject2Name;

    private java.lang.String customObject3IntegrationId;

    private java.lang.String customObject3ExternalSystemId;

    private java.lang.String customObject3Name;

    private java.lang.String customObject4Name;

    private java.lang.String customObject4ExternalSystemId;

    private java.lang.String customObject4IntegrationId;

    private java.lang.String customObject5Name;

    private java.lang.String customObject5ExternalSystemId;

    private java.lang.String customObject5IntegrationId;

    private java.lang.String customObject6Name;

    private java.lang.String customObject6ExternalSystemId;

    private java.lang.String customObject6IntegrationId;

    private java.lang.String customObject7Name;

    private java.lang.String customObject7ExternalSystemId;

    private java.lang.String customObject7IntegrationId;

    private java.lang.String customObject8Name;

    private java.lang.String customObject8ExternalSystemId;

    private java.lang.String customObject8IntegrationId;

    private java.lang.String customObject9Name;

    private java.lang.String customObject9ExternalSystemId;

    private java.lang.String customObject9IntegrationId;

    private java.lang.String customObject10Name;

    private java.lang.String customObject10ExternalSystemId;

    private java.lang.String customObject10IntegrationId;

    private java.lang.String customObject11Name;

    private java.lang.String customObject11ExternalSystemId;

    private java.lang.String customObject11IntegrationId;

    private java.lang.String customObject12Name;

    private java.lang.String customObject12ExternalSystemId;

    private java.lang.String customObject12IntegrationId;

    private java.lang.String customObject13Name;

    private java.lang.String customObject13ExternalSystemId;

    private java.lang.String customObject13IntegrationId;

    private java.lang.String customObject14Name;

    private java.lang.String customObject14ExternalSystemId;

    private java.lang.String customObject14IntegrationId;

    private java.lang.String customObject15Name;

    private java.lang.String customObject15ExternalSystemId;

    private java.lang.String customObject15IntegrationId;

    private java.lang.String dealerName;

    private java.lang.String dealerExternalSystemId;

    private java.lang.String dealerIntegrationId;

    private java.lang.String fundName;

    private java.lang.String fundRequestName;

    private java.lang.String fundRequestExternalSystemId;

    private java.lang.String householdName;

    private java.lang.String householdIntegrationId;

    private java.lang.String householdExternalSystemId;

    private java.lang.String leadExternalSystemId;

    private java.lang.String leadFirstName;

    private java.lang.String leadFullName;

    private java.lang.String leadIntegrationId;

    private java.lang.String leadLastName;

    private java.lang.String medEdName;

    private java.lang.String medEdExternalSystemId;

    private java.lang.String medEdIntegrationId;

    private java.lang.String opportunityAccountId;

    private java.lang.String opportunityExternalSystemId;

    private java.lang.String opportunityIntegrationId;

    private java.lang.String opportunityName;

    private java.lang.String portfolioAccountNumber;

    private java.lang.String portfolioExternalSystemId;

    private java.lang.String portfolioIntegrationId;

    private java.lang.String productIntegrationId;

    private java.lang.String productName;

    private java.lang.String productExternalSystemId;

    private java.lang.String serviceRequestExternalSystemId;

    private java.lang.String serviceRequestIntegrationId;

    private java.lang.String serviceRequestSRNumber;

    private java.lang.String solutionExternalSystemId;

    private java.lang.String solutionIntegrationId;

    private java.lang.String solutionTitle;

    private java.lang.String vehicleIntegrationId;

    private java.lang.String vehicleVIN;

    private java.lang.String vehicleExternalSystemId;

    private java.lang.String ownerAlias;

    private java.lang.String ownerEMailAddr;

    private java.lang.String ownerExternalSystemId;

    private java.lang.String ownerFirstName;

    private java.lang.String ownerFullName;

    private java.lang.String ownerIntegrationId;

    private java.lang.String ownerLastName;

    private java.lang.String ownerUserSignInId;

    private java.lang.String createdBy;

    private java.lang.String modifiedBy;

    private java.lang.String description;

    private java.lang.String operation;  // attribute

    public CustomObject6Data() {
    }

    public CustomObject6Data(
           java.util.Calendar modifiedDate,
           java.util.Calendar createdDate,
           java.lang.String modifiedById,
           java.lang.String createdById,
           java.lang.Integer modId,
           java.lang.String id,
           java.lang.String name,
           java.lang.String quickSearch1,
           java.lang.String quickSearch2,
           java.lang.String type,
           java.lang.String currencyCode,
           java.util.Date exchangeDate,
           java.lang.String externalSystemId,
           java.lang.String integrationId,
           java.lang.Boolean indexedBoolean0,
           java.math.BigDecimal indexedCurrency0,
           java.util.Calendar indexedDate0,
           java.math.BigDecimal indexedNumber0,
           java.lang.String indexedPick0,
           java.lang.String indexedPick1,
           java.lang.String indexedPick2,
           java.lang.String indexedPick3,
           java.lang.String indexedPick4,
           java.lang.String accountId,
           java.lang.String activityId,
           java.lang.String assetId,
           java.lang.String campaignId,
           java.lang.String contactId,
           java.lang.String customObject1Id,
           java.lang.String customObject2Id,
           java.lang.String customObject3Id,
           java.lang.String customObject4Id,
           java.lang.String customObject5Id,
           java.lang.String customObject6Id,
           java.lang.String customObject7Id,
           java.lang.String customObject8Id,
           java.lang.String customObject9Id,
           java.lang.String customObject10Id,
           java.lang.String customObject11Id,
           java.lang.String customObject12Id,
           java.lang.String customObject13Id,
           java.lang.String customObject14Id,
           java.lang.String customObject15Id,
           java.lang.String dealerId,
           java.lang.String fundId,
           java.lang.String fundRequestId,
           java.lang.String householdId,
           java.lang.String leadId,
           java.lang.String medEdId,
           java.lang.String opportunityId,
           java.lang.String portfolioId,
           java.lang.String productId,
           java.lang.String serviceRequestId,
           java.lang.String solutionId,
           java.lang.String vehicleId,
           java.lang.String ownerId,
           java.lang.String updatedByAlias,
           java.lang.String updatedByEMailAddr,
           java.lang.String updatedByExternalSystemId,
           java.lang.String updatedByFirstName,
           java.lang.String updatedByFullName,
           java.lang.String updatedByIntegrationId,
           java.lang.String updatedByLastName,
           java.lang.String updatedByUserSignInId,
           java.lang.String createdByAlias,
           java.lang.String createdByEMailAddr,
           java.lang.String createdByExternalSystemId,
           java.lang.String createdByFirstName,
           java.lang.String createdByFullName,
           java.lang.String createdByIntegrationId,
           java.lang.String createdByLastName,
           java.lang.String createdByUserSignInId,
           java.lang.String accountExternalSystemId,
           java.lang.String accountIntegrationId,
           java.lang.String accountLocation,
           java.lang.String accountName,
           java.lang.String activitySubject,
           java.lang.String activityExternalSystemId,
           java.lang.String activityIntegrationId,
           java.lang.String assetIntegrationId,
           java.lang.String assetProduct,
           java.lang.String assetSerialNumber,
           java.lang.String assetExternalSystemId,
           java.lang.String campaignExternalSystemId,
           java.lang.String campaignIntegrationId,
           java.lang.String campaignName,
           java.lang.String contactEmail,
           java.lang.String contactExternalSystemId,
           java.lang.String contactFirstName,
           java.lang.String contactFullName,
           java.lang.String contactIntegrationId,
           java.lang.String contactLastName,
           java.lang.String customObject1ExternalSystemId,
           java.lang.String customObject1IntegrationId,
           java.lang.String customObject1Name,
           java.lang.String customObject2ExternalSystemId,
           java.lang.String customObject2IntegrationId,
           java.lang.String customObject2Name,
           java.lang.String customObject3IntegrationId,
           java.lang.String customObject3ExternalSystemId,
           java.lang.String customObject3Name,
           java.lang.String customObject4Name,
           java.lang.String customObject4ExternalSystemId,
           java.lang.String customObject4IntegrationId,
           java.lang.String customObject5Name,
           java.lang.String customObject5ExternalSystemId,
           java.lang.String customObject5IntegrationId,
           java.lang.String customObject6Name,
           java.lang.String customObject6ExternalSystemId,
           java.lang.String customObject6IntegrationId,
           java.lang.String customObject7Name,
           java.lang.String customObject7ExternalSystemId,
           java.lang.String customObject7IntegrationId,
           java.lang.String customObject8Name,
           java.lang.String customObject8ExternalSystemId,
           java.lang.String customObject8IntegrationId,
           java.lang.String customObject9Name,
           java.lang.String customObject9ExternalSystemId,
           java.lang.String customObject9IntegrationId,
           java.lang.String customObject10Name,
           java.lang.String customObject10ExternalSystemId,
           java.lang.String customObject10IntegrationId,
           java.lang.String customObject11Name,
           java.lang.String customObject11ExternalSystemId,
           java.lang.String customObject11IntegrationId,
           java.lang.String customObject12Name,
           java.lang.String customObject12ExternalSystemId,
           java.lang.String customObject12IntegrationId,
           java.lang.String customObject13Name,
           java.lang.String customObject13ExternalSystemId,
           java.lang.String customObject13IntegrationId,
           java.lang.String customObject14Name,
           java.lang.String customObject14ExternalSystemId,
           java.lang.String customObject14IntegrationId,
           java.lang.String customObject15Name,
           java.lang.String customObject15ExternalSystemId,
           java.lang.String customObject15IntegrationId,
           java.lang.String dealerName,
           java.lang.String dealerExternalSystemId,
           java.lang.String dealerIntegrationId,
           java.lang.String fundName,
           java.lang.String fundRequestName,
           java.lang.String fundRequestExternalSystemId,
           java.lang.String householdName,
           java.lang.String householdIntegrationId,
           java.lang.String householdExternalSystemId,
           java.lang.String leadExternalSystemId,
           java.lang.String leadFirstName,
           java.lang.String leadFullName,
           java.lang.String leadIntegrationId,
           java.lang.String leadLastName,
           java.lang.String medEdName,
           java.lang.String medEdExternalSystemId,
           java.lang.String medEdIntegrationId,
           java.lang.String opportunityAccountId,
           java.lang.String opportunityExternalSystemId,
           java.lang.String opportunityIntegrationId,
           java.lang.String opportunityName,
           java.lang.String portfolioAccountNumber,
           java.lang.String portfolioExternalSystemId,
           java.lang.String portfolioIntegrationId,
           java.lang.String productIntegrationId,
           java.lang.String productName,
           java.lang.String productExternalSystemId,
           java.lang.String serviceRequestExternalSystemId,
           java.lang.String serviceRequestIntegrationId,
           java.lang.String serviceRequestSRNumber,
           java.lang.String solutionExternalSystemId,
           java.lang.String solutionIntegrationId,
           java.lang.String solutionTitle,
           java.lang.String vehicleIntegrationId,
           java.lang.String vehicleVIN,
           java.lang.String vehicleExternalSystemId,
           java.lang.String ownerAlias,
           java.lang.String ownerEMailAddr,
           java.lang.String ownerExternalSystemId,
           java.lang.String ownerFirstName,
           java.lang.String ownerFullName,
           java.lang.String ownerIntegrationId,
           java.lang.String ownerLastName,
           java.lang.String ownerUserSignInId,
           java.lang.String createdBy,
           java.lang.String modifiedBy,
           java.lang.String description,
           java.lang.String operation) {
           this.modifiedDate = modifiedDate;
           this.createdDate = createdDate;
           this.modifiedById = modifiedById;
           this.createdById = createdById;
           this.modId = modId;
           this.id = id;
           this.name = name;
           this.quickSearch1 = quickSearch1;
           this.quickSearch2 = quickSearch2;
           this.type = type;
           this.currencyCode = currencyCode;
           this.exchangeDate = exchangeDate;
           this.externalSystemId = externalSystemId;
           this.integrationId = integrationId;
           this.indexedBoolean0 = indexedBoolean0;
           this.indexedCurrency0 = indexedCurrency0;
           this.indexedDate0 = indexedDate0;
           this.indexedNumber0 = indexedNumber0;
           this.indexedPick0 = indexedPick0;
           this.indexedPick1 = indexedPick1;
           this.indexedPick2 = indexedPick2;
           this.indexedPick3 = indexedPick3;
           this.indexedPick4 = indexedPick4;
           this.accountId = accountId;
           this.activityId = activityId;
           this.assetId = assetId;
           this.campaignId = campaignId;
           this.contactId = contactId;
           this.customObject1Id = customObject1Id;
           this.customObject2Id = customObject2Id;
           this.customObject3Id = customObject3Id;
           this.customObject4Id = customObject4Id;
           this.customObject5Id = customObject5Id;
           this.customObject6Id = customObject6Id;
           this.customObject7Id = customObject7Id;
           this.customObject8Id = customObject8Id;
           this.customObject9Id = customObject9Id;
           this.customObject10Id = customObject10Id;
           this.customObject11Id = customObject11Id;
           this.customObject12Id = customObject12Id;
           this.customObject13Id = customObject13Id;
           this.customObject14Id = customObject14Id;
           this.customObject15Id = customObject15Id;
           this.dealerId = dealerId;
           this.fundId = fundId;
           this.fundRequestId = fundRequestId;
           this.householdId = householdId;
           this.leadId = leadId;
           this.medEdId = medEdId;
           this.opportunityId = opportunityId;
           this.portfolioId = portfolioId;
           this.productId = productId;
           this.serviceRequestId = serviceRequestId;
           this.solutionId = solutionId;
           this.vehicleId = vehicleId;
           this.ownerId = ownerId;
           this.updatedByAlias = updatedByAlias;
           this.updatedByEMailAddr = updatedByEMailAddr;
           this.updatedByExternalSystemId = updatedByExternalSystemId;
           this.updatedByFirstName = updatedByFirstName;
           this.updatedByFullName = updatedByFullName;
           this.updatedByIntegrationId = updatedByIntegrationId;
           this.updatedByLastName = updatedByLastName;
           this.updatedByUserSignInId = updatedByUserSignInId;
           this.createdByAlias = createdByAlias;
           this.createdByEMailAddr = createdByEMailAddr;
           this.createdByExternalSystemId = createdByExternalSystemId;
           this.createdByFirstName = createdByFirstName;
           this.createdByFullName = createdByFullName;
           this.createdByIntegrationId = createdByIntegrationId;
           this.createdByLastName = createdByLastName;
           this.createdByUserSignInId = createdByUserSignInId;
           this.accountExternalSystemId = accountExternalSystemId;
           this.accountIntegrationId = accountIntegrationId;
           this.accountLocation = accountLocation;
           this.accountName = accountName;
           this.activitySubject = activitySubject;
           this.activityExternalSystemId = activityExternalSystemId;
           this.activityIntegrationId = activityIntegrationId;
           this.assetIntegrationId = assetIntegrationId;
           this.assetProduct = assetProduct;
           this.assetSerialNumber = assetSerialNumber;
           this.assetExternalSystemId = assetExternalSystemId;
           this.campaignExternalSystemId = campaignExternalSystemId;
           this.campaignIntegrationId = campaignIntegrationId;
           this.campaignName = campaignName;
           this.contactEmail = contactEmail;
           this.contactExternalSystemId = contactExternalSystemId;
           this.contactFirstName = contactFirstName;
           this.contactFullName = contactFullName;
           this.contactIntegrationId = contactIntegrationId;
           this.contactLastName = contactLastName;
           this.customObject1ExternalSystemId = customObject1ExternalSystemId;
           this.customObject1IntegrationId = customObject1IntegrationId;
           this.customObject1Name = customObject1Name;
           this.customObject2ExternalSystemId = customObject2ExternalSystemId;
           this.customObject2IntegrationId = customObject2IntegrationId;
           this.customObject2Name = customObject2Name;
           this.customObject3IntegrationId = customObject3IntegrationId;
           this.customObject3ExternalSystemId = customObject3ExternalSystemId;
           this.customObject3Name = customObject3Name;
           this.customObject4Name = customObject4Name;
           this.customObject4ExternalSystemId = customObject4ExternalSystemId;
           this.customObject4IntegrationId = customObject4IntegrationId;
           this.customObject5Name = customObject5Name;
           this.customObject5ExternalSystemId = customObject5ExternalSystemId;
           this.customObject5IntegrationId = customObject5IntegrationId;
           this.customObject6Name = customObject6Name;
           this.customObject6ExternalSystemId = customObject6ExternalSystemId;
           this.customObject6IntegrationId = customObject6IntegrationId;
           this.customObject7Name = customObject7Name;
           this.customObject7ExternalSystemId = customObject7ExternalSystemId;
           this.customObject7IntegrationId = customObject7IntegrationId;
           this.customObject8Name = customObject8Name;
           this.customObject8ExternalSystemId = customObject8ExternalSystemId;
           this.customObject8IntegrationId = customObject8IntegrationId;
           this.customObject9Name = customObject9Name;
           this.customObject9ExternalSystemId = customObject9ExternalSystemId;
           this.customObject9IntegrationId = customObject9IntegrationId;
           this.customObject10Name = customObject10Name;
           this.customObject10ExternalSystemId = customObject10ExternalSystemId;
           this.customObject10IntegrationId = customObject10IntegrationId;
           this.customObject11Name = customObject11Name;
           this.customObject11ExternalSystemId = customObject11ExternalSystemId;
           this.customObject11IntegrationId = customObject11IntegrationId;
           this.customObject12Name = customObject12Name;
           this.customObject12ExternalSystemId = customObject12ExternalSystemId;
           this.customObject12IntegrationId = customObject12IntegrationId;
           this.customObject13Name = customObject13Name;
           this.customObject13ExternalSystemId = customObject13ExternalSystemId;
           this.customObject13IntegrationId = customObject13IntegrationId;
           this.customObject14Name = customObject14Name;
           this.customObject14ExternalSystemId = customObject14ExternalSystemId;
           this.customObject14IntegrationId = customObject14IntegrationId;
           this.customObject15Name = customObject15Name;
           this.customObject15ExternalSystemId = customObject15ExternalSystemId;
           this.customObject15IntegrationId = customObject15IntegrationId;
           this.dealerName = dealerName;
           this.dealerExternalSystemId = dealerExternalSystemId;
           this.dealerIntegrationId = dealerIntegrationId;
           this.fundName = fundName;
           this.fundRequestName = fundRequestName;
           this.fundRequestExternalSystemId = fundRequestExternalSystemId;
           this.householdName = householdName;
           this.householdIntegrationId = householdIntegrationId;
           this.householdExternalSystemId = householdExternalSystemId;
           this.leadExternalSystemId = leadExternalSystemId;
           this.leadFirstName = leadFirstName;
           this.leadFullName = leadFullName;
           this.leadIntegrationId = leadIntegrationId;
           this.leadLastName = leadLastName;
           this.medEdName = medEdName;
           this.medEdExternalSystemId = medEdExternalSystemId;
           this.medEdIntegrationId = medEdIntegrationId;
           this.opportunityAccountId = opportunityAccountId;
           this.opportunityExternalSystemId = opportunityExternalSystemId;
           this.opportunityIntegrationId = opportunityIntegrationId;
           this.opportunityName = opportunityName;
           this.portfolioAccountNumber = portfolioAccountNumber;
           this.portfolioExternalSystemId = portfolioExternalSystemId;
           this.portfolioIntegrationId = portfolioIntegrationId;
           this.productIntegrationId = productIntegrationId;
           this.productName = productName;
           this.productExternalSystemId = productExternalSystemId;
           this.serviceRequestExternalSystemId = serviceRequestExternalSystemId;
           this.serviceRequestIntegrationId = serviceRequestIntegrationId;
           this.serviceRequestSRNumber = serviceRequestSRNumber;
           this.solutionExternalSystemId = solutionExternalSystemId;
           this.solutionIntegrationId = solutionIntegrationId;
           this.solutionTitle = solutionTitle;
           this.vehicleIntegrationId = vehicleIntegrationId;
           this.vehicleVIN = vehicleVIN;
           this.vehicleExternalSystemId = vehicleExternalSystemId;
           this.ownerAlias = ownerAlias;
           this.ownerEMailAddr = ownerEMailAddr;
           this.ownerExternalSystemId = ownerExternalSystemId;
           this.ownerFirstName = ownerFirstName;
           this.ownerFullName = ownerFullName;
           this.ownerIntegrationId = ownerIntegrationId;
           this.ownerLastName = ownerLastName;
           this.ownerUserSignInId = ownerUserSignInId;
           this.createdBy = createdBy;
           this.modifiedBy = modifiedBy;
           this.description = description;
           this.operation = operation;
    }


    /**
     * Gets the modifiedDate value for this CustomObject6Data.
     * 
     * @return modifiedDate
     */
    public java.util.Calendar getModifiedDate() {
        return modifiedDate;
    }


    /**
     * Sets the modifiedDate value for this CustomObject6Data.
     * 
     * @param modifiedDate
     */
    public void setModifiedDate(java.util.Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    /**
     * Gets the createdDate value for this CustomObject6Data.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this CustomObject6Data.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }


    /**
     * Gets the modifiedById value for this CustomObject6Data.
     * 
     * @return modifiedById
     */
    public java.lang.String getModifiedById() {
        return modifiedById;
    }


    /**
     * Sets the modifiedById value for this CustomObject6Data.
     * 
     * @param modifiedById
     */
    public void setModifiedById(java.lang.String modifiedById) {
        this.modifiedById = modifiedById;
    }


    /**
     * Gets the createdById value for this CustomObject6Data.
     * 
     * @return createdById
     */
    public java.lang.String getCreatedById() {
        return createdById;
    }


    /**
     * Sets the createdById value for this CustomObject6Data.
     * 
     * @param createdById
     */
    public void setCreatedById(java.lang.String createdById) {
        this.createdById = createdById;
    }


    /**
     * Gets the modId value for this CustomObject6Data.
     * 
     * @return modId
     */
    public java.lang.Integer getModId() {
        return modId;
    }


    /**
     * Sets the modId value for this CustomObject6Data.
     * 
     * @param modId
     */
    public void setModId(java.lang.Integer modId) {
        this.modId = modId;
    }


    /**
     * Gets the id value for this CustomObject6Data.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this CustomObject6Data.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the name value for this CustomObject6Data.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this CustomObject6Data.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the quickSearch1 value for this CustomObject6Data.
     * 
     * @return quickSearch1
     */
    public java.lang.String getQuickSearch1() {
        return quickSearch1;
    }


    /**
     * Sets the quickSearch1 value for this CustomObject6Data.
     * 
     * @param quickSearch1
     */
    public void setQuickSearch1(java.lang.String quickSearch1) {
        this.quickSearch1 = quickSearch1;
    }


    /**
     * Gets the quickSearch2 value for this CustomObject6Data.
     * 
     * @return quickSearch2
     */
    public java.lang.String getQuickSearch2() {
        return quickSearch2;
    }


    /**
     * Sets the quickSearch2 value for this CustomObject6Data.
     * 
     * @param quickSearch2
     */
    public void setQuickSearch2(java.lang.String quickSearch2) {
        this.quickSearch2 = quickSearch2;
    }


    /**
     * Gets the type value for this CustomObject6Data.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this CustomObject6Data.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the currencyCode value for this CustomObject6Data.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this CustomObject6Data.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the exchangeDate value for this CustomObject6Data.
     * 
     * @return exchangeDate
     */
    public java.util.Date getExchangeDate() {
        return exchangeDate;
    }


    /**
     * Sets the exchangeDate value for this CustomObject6Data.
     * 
     * @param exchangeDate
     */
    public void setExchangeDate(java.util.Date exchangeDate) {
        this.exchangeDate = exchangeDate;
    }


    /**
     * Gets the externalSystemId value for this CustomObject6Data.
     * 
     * @return externalSystemId
     */
    public java.lang.String getExternalSystemId() {
        return externalSystemId;
    }


    /**
     * Sets the externalSystemId value for this CustomObject6Data.
     * 
     * @param externalSystemId
     */
    public void setExternalSystemId(java.lang.String externalSystemId) {
        this.externalSystemId = externalSystemId;
    }


    /**
     * Gets the integrationId value for this CustomObject6Data.
     * 
     * @return integrationId
     */
    public java.lang.String getIntegrationId() {
        return integrationId;
    }


    /**
     * Sets the integrationId value for this CustomObject6Data.
     * 
     * @param integrationId
     */
    public void setIntegrationId(java.lang.String integrationId) {
        this.integrationId = integrationId;
    }


    /**
     * Gets the indexedBoolean0 value for this CustomObject6Data.
     * 
     * @return indexedBoolean0
     */
    public java.lang.Boolean getIndexedBoolean0() {
        return indexedBoolean0;
    }


    /**
     * Sets the indexedBoolean0 value for this CustomObject6Data.
     * 
     * @param indexedBoolean0
     */
    public void setIndexedBoolean0(java.lang.Boolean indexedBoolean0) {
        this.indexedBoolean0 = indexedBoolean0;
    }


    /**
     * Gets the indexedCurrency0 value for this CustomObject6Data.
     * 
     * @return indexedCurrency0
     */
    public java.math.BigDecimal getIndexedCurrency0() {
        return indexedCurrency0;
    }


    /**
     * Sets the indexedCurrency0 value for this CustomObject6Data.
     * 
     * @param indexedCurrency0
     */
    public void setIndexedCurrency0(java.math.BigDecimal indexedCurrency0) {
        this.indexedCurrency0 = indexedCurrency0;
    }


    /**
     * Gets the indexedDate0 value for this CustomObject6Data.
     * 
     * @return indexedDate0
     */
    public java.util.Calendar getIndexedDate0() {
        return indexedDate0;
    }


    /**
     * Sets the indexedDate0 value for this CustomObject6Data.
     * 
     * @param indexedDate0
     */
    public void setIndexedDate0(java.util.Calendar indexedDate0) {
        this.indexedDate0 = indexedDate0;
    }


    /**
     * Gets the indexedNumber0 value for this CustomObject6Data.
     * 
     * @return indexedNumber0
     */
    public java.math.BigDecimal getIndexedNumber0() {
        return indexedNumber0;
    }


    /**
     * Sets the indexedNumber0 value for this CustomObject6Data.
     * 
     * @param indexedNumber0
     */
    public void setIndexedNumber0(java.math.BigDecimal indexedNumber0) {
        this.indexedNumber0 = indexedNumber0;
    }


    /**
     * Gets the indexedPick0 value for this CustomObject6Data.
     * 
     * @return indexedPick0
     */
    public java.lang.String getIndexedPick0() {
        return indexedPick0;
    }


    /**
     * Sets the indexedPick0 value for this CustomObject6Data.
     * 
     * @param indexedPick0
     */
    public void setIndexedPick0(java.lang.String indexedPick0) {
        this.indexedPick0 = indexedPick0;
    }


    /**
     * Gets the indexedPick1 value for this CustomObject6Data.
     * 
     * @return indexedPick1
     */
    public java.lang.String getIndexedPick1() {
        return indexedPick1;
    }


    /**
     * Sets the indexedPick1 value for this CustomObject6Data.
     * 
     * @param indexedPick1
     */
    public void setIndexedPick1(java.lang.String indexedPick1) {
        this.indexedPick1 = indexedPick1;
    }


    /**
     * Gets the indexedPick2 value for this CustomObject6Data.
     * 
     * @return indexedPick2
     */
    public java.lang.String getIndexedPick2() {
        return indexedPick2;
    }


    /**
     * Sets the indexedPick2 value for this CustomObject6Data.
     * 
     * @param indexedPick2
     */
    public void setIndexedPick2(java.lang.String indexedPick2) {
        this.indexedPick2 = indexedPick2;
    }


    /**
     * Gets the indexedPick3 value for this CustomObject6Data.
     * 
     * @return indexedPick3
     */
    public java.lang.String getIndexedPick3() {
        return indexedPick3;
    }


    /**
     * Sets the indexedPick3 value for this CustomObject6Data.
     * 
     * @param indexedPick3
     */
    public void setIndexedPick3(java.lang.String indexedPick3) {
        this.indexedPick3 = indexedPick3;
    }


    /**
     * Gets the indexedPick4 value for this CustomObject6Data.
     * 
     * @return indexedPick4
     */
    public java.lang.String getIndexedPick4() {
        return indexedPick4;
    }


    /**
     * Sets the indexedPick4 value for this CustomObject6Data.
     * 
     * @param indexedPick4
     */
    public void setIndexedPick4(java.lang.String indexedPick4) {
        this.indexedPick4 = indexedPick4;
    }


    /**
     * Gets the accountId value for this CustomObject6Data.
     * 
     * @return accountId
     */
    public java.lang.String getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this CustomObject6Data.
     * 
     * @param accountId
     */
    public void setAccountId(java.lang.String accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the activityId value for this CustomObject6Data.
     * 
     * @return activityId
     */
    public java.lang.String getActivityId() {
        return activityId;
    }


    /**
     * Sets the activityId value for this CustomObject6Data.
     * 
     * @param activityId
     */
    public void setActivityId(java.lang.String activityId) {
        this.activityId = activityId;
    }


    /**
     * Gets the assetId value for this CustomObject6Data.
     * 
     * @return assetId
     */
    public java.lang.String getAssetId() {
        return assetId;
    }


    /**
     * Sets the assetId value for this CustomObject6Data.
     * 
     * @param assetId
     */
    public void setAssetId(java.lang.String assetId) {
        this.assetId = assetId;
    }


    /**
     * Gets the campaignId value for this CustomObject6Data.
     * 
     * @return campaignId
     */
    public java.lang.String getCampaignId() {
        return campaignId;
    }


    /**
     * Sets the campaignId value for this CustomObject6Data.
     * 
     * @param campaignId
     */
    public void setCampaignId(java.lang.String campaignId) {
        this.campaignId = campaignId;
    }


    /**
     * Gets the contactId value for this CustomObject6Data.
     * 
     * @return contactId
     */
    public java.lang.String getContactId() {
        return contactId;
    }


    /**
     * Sets the contactId value for this CustomObject6Data.
     * 
     * @param contactId
     */
    public void setContactId(java.lang.String contactId) {
        this.contactId = contactId;
    }


    /**
     * Gets the customObject1Id value for this CustomObject6Data.
     * 
     * @return customObject1Id
     */
    public java.lang.String getCustomObject1Id() {
        return customObject1Id;
    }


    /**
     * Sets the customObject1Id value for this CustomObject6Data.
     * 
     * @param customObject1Id
     */
    public void setCustomObject1Id(java.lang.String customObject1Id) {
        this.customObject1Id = customObject1Id;
    }


    /**
     * Gets the customObject2Id value for this CustomObject6Data.
     * 
     * @return customObject2Id
     */
    public java.lang.String getCustomObject2Id() {
        return customObject2Id;
    }


    /**
     * Sets the customObject2Id value for this CustomObject6Data.
     * 
     * @param customObject2Id
     */
    public void setCustomObject2Id(java.lang.String customObject2Id) {
        this.customObject2Id = customObject2Id;
    }


    /**
     * Gets the customObject3Id value for this CustomObject6Data.
     * 
     * @return customObject3Id
     */
    public java.lang.String getCustomObject3Id() {
        return customObject3Id;
    }


    /**
     * Sets the customObject3Id value for this CustomObject6Data.
     * 
     * @param customObject3Id
     */
    public void setCustomObject3Id(java.lang.String customObject3Id) {
        this.customObject3Id = customObject3Id;
    }


    /**
     * Gets the customObject4Id value for this CustomObject6Data.
     * 
     * @return customObject4Id
     */
    public java.lang.String getCustomObject4Id() {
        return customObject4Id;
    }


    /**
     * Sets the customObject4Id value for this CustomObject6Data.
     * 
     * @param customObject4Id
     */
    public void setCustomObject4Id(java.lang.String customObject4Id) {
        this.customObject4Id = customObject4Id;
    }


    /**
     * Gets the customObject5Id value for this CustomObject6Data.
     * 
     * @return customObject5Id
     */
    public java.lang.String getCustomObject5Id() {
        return customObject5Id;
    }


    /**
     * Sets the customObject5Id value for this CustomObject6Data.
     * 
     * @param customObject5Id
     */
    public void setCustomObject5Id(java.lang.String customObject5Id) {
        this.customObject5Id = customObject5Id;
    }


    /**
     * Gets the customObject6Id value for this CustomObject6Data.
     * 
     * @return customObject6Id
     */
    public java.lang.String getCustomObject6Id() {
        return customObject6Id;
    }


    /**
     * Sets the customObject6Id value for this CustomObject6Data.
     * 
     * @param customObject6Id
     */
    public void setCustomObject6Id(java.lang.String customObject6Id) {
        this.customObject6Id = customObject6Id;
    }


    /**
     * Gets the customObject7Id value for this CustomObject6Data.
     * 
     * @return customObject7Id
     */
    public java.lang.String getCustomObject7Id() {
        return customObject7Id;
    }


    /**
     * Sets the customObject7Id value for this CustomObject6Data.
     * 
     * @param customObject7Id
     */
    public void setCustomObject7Id(java.lang.String customObject7Id) {
        this.customObject7Id = customObject7Id;
    }


    /**
     * Gets the customObject8Id value for this CustomObject6Data.
     * 
     * @return customObject8Id
     */
    public java.lang.String getCustomObject8Id() {
        return customObject8Id;
    }


    /**
     * Sets the customObject8Id value for this CustomObject6Data.
     * 
     * @param customObject8Id
     */
    public void setCustomObject8Id(java.lang.String customObject8Id) {
        this.customObject8Id = customObject8Id;
    }


    /**
     * Gets the customObject9Id value for this CustomObject6Data.
     * 
     * @return customObject9Id
     */
    public java.lang.String getCustomObject9Id() {
        return customObject9Id;
    }


    /**
     * Sets the customObject9Id value for this CustomObject6Data.
     * 
     * @param customObject9Id
     */
    public void setCustomObject9Id(java.lang.String customObject9Id) {
        this.customObject9Id = customObject9Id;
    }


    /**
     * Gets the customObject10Id value for this CustomObject6Data.
     * 
     * @return customObject10Id
     */
    public java.lang.String getCustomObject10Id() {
        return customObject10Id;
    }


    /**
     * Sets the customObject10Id value for this CustomObject6Data.
     * 
     * @param customObject10Id
     */
    public void setCustomObject10Id(java.lang.String customObject10Id) {
        this.customObject10Id = customObject10Id;
    }


    /**
     * Gets the customObject11Id value for this CustomObject6Data.
     * 
     * @return customObject11Id
     */
    public java.lang.String getCustomObject11Id() {
        return customObject11Id;
    }


    /**
     * Sets the customObject11Id value for this CustomObject6Data.
     * 
     * @param customObject11Id
     */
    public void setCustomObject11Id(java.lang.String customObject11Id) {
        this.customObject11Id = customObject11Id;
    }


    /**
     * Gets the customObject12Id value for this CustomObject6Data.
     * 
     * @return customObject12Id
     */
    public java.lang.String getCustomObject12Id() {
        return customObject12Id;
    }


    /**
     * Sets the customObject12Id value for this CustomObject6Data.
     * 
     * @param customObject12Id
     */
    public void setCustomObject12Id(java.lang.String customObject12Id) {
        this.customObject12Id = customObject12Id;
    }


    /**
     * Gets the customObject13Id value for this CustomObject6Data.
     * 
     * @return customObject13Id
     */
    public java.lang.String getCustomObject13Id() {
        return customObject13Id;
    }


    /**
     * Sets the customObject13Id value for this CustomObject6Data.
     * 
     * @param customObject13Id
     */
    public void setCustomObject13Id(java.lang.String customObject13Id) {
        this.customObject13Id = customObject13Id;
    }


    /**
     * Gets the customObject14Id value for this CustomObject6Data.
     * 
     * @return customObject14Id
     */
    public java.lang.String getCustomObject14Id() {
        return customObject14Id;
    }


    /**
     * Sets the customObject14Id value for this CustomObject6Data.
     * 
     * @param customObject14Id
     */
    public void setCustomObject14Id(java.lang.String customObject14Id) {
        this.customObject14Id = customObject14Id;
    }


    /**
     * Gets the customObject15Id value for this CustomObject6Data.
     * 
     * @return customObject15Id
     */
    public java.lang.String getCustomObject15Id() {
        return customObject15Id;
    }


    /**
     * Sets the customObject15Id value for this CustomObject6Data.
     * 
     * @param customObject15Id
     */
    public void setCustomObject15Id(java.lang.String customObject15Id) {
        this.customObject15Id = customObject15Id;
    }


    /**
     * Gets the dealerId value for this CustomObject6Data.
     * 
     * @return dealerId
     */
    public java.lang.String getDealerId() {
        return dealerId;
    }


    /**
     * Sets the dealerId value for this CustomObject6Data.
     * 
     * @param dealerId
     */
    public void setDealerId(java.lang.String dealerId) {
        this.dealerId = dealerId;
    }


    /**
     * Gets the fundId value for this CustomObject6Data.
     * 
     * @return fundId
     */
    public java.lang.String getFundId() {
        return fundId;
    }


    /**
     * Sets the fundId value for this CustomObject6Data.
     * 
     * @param fundId
     */
    public void setFundId(java.lang.String fundId) {
        this.fundId = fundId;
    }


    /**
     * Gets the fundRequestId value for this CustomObject6Data.
     * 
     * @return fundRequestId
     */
    public java.lang.String getFundRequestId() {
        return fundRequestId;
    }


    /**
     * Sets the fundRequestId value for this CustomObject6Data.
     * 
     * @param fundRequestId
     */
    public void setFundRequestId(java.lang.String fundRequestId) {
        this.fundRequestId = fundRequestId;
    }


    /**
     * Gets the householdId value for this CustomObject6Data.
     * 
     * @return householdId
     */
    public java.lang.String getHouseholdId() {
        return householdId;
    }


    /**
     * Sets the householdId value for this CustomObject6Data.
     * 
     * @param householdId
     */
    public void setHouseholdId(java.lang.String householdId) {
        this.householdId = householdId;
    }


    /**
     * Gets the leadId value for this CustomObject6Data.
     * 
     * @return leadId
     */
    public java.lang.String getLeadId() {
        return leadId;
    }


    /**
     * Sets the leadId value for this CustomObject6Data.
     * 
     * @param leadId
     */
    public void setLeadId(java.lang.String leadId) {
        this.leadId = leadId;
    }


    /**
     * Gets the medEdId value for this CustomObject6Data.
     * 
     * @return medEdId
     */
    public java.lang.String getMedEdId() {
        return medEdId;
    }


    /**
     * Sets the medEdId value for this CustomObject6Data.
     * 
     * @param medEdId
     */
    public void setMedEdId(java.lang.String medEdId) {
        this.medEdId = medEdId;
    }


    /**
     * Gets the opportunityId value for this CustomObject6Data.
     * 
     * @return opportunityId
     */
    public java.lang.String getOpportunityId() {
        return opportunityId;
    }


    /**
     * Sets the opportunityId value for this CustomObject6Data.
     * 
     * @param opportunityId
     */
    public void setOpportunityId(java.lang.String opportunityId) {
        this.opportunityId = opportunityId;
    }


    /**
     * Gets the portfolioId value for this CustomObject6Data.
     * 
     * @return portfolioId
     */
    public java.lang.String getPortfolioId() {
        return portfolioId;
    }


    /**
     * Sets the portfolioId value for this CustomObject6Data.
     * 
     * @param portfolioId
     */
    public void setPortfolioId(java.lang.String portfolioId) {
        this.portfolioId = portfolioId;
    }


    /**
     * Gets the productId value for this CustomObject6Data.
     * 
     * @return productId
     */
    public java.lang.String getProductId() {
        return productId;
    }


    /**
     * Sets the productId value for this CustomObject6Data.
     * 
     * @param productId
     */
    public void setProductId(java.lang.String productId) {
        this.productId = productId;
    }


    /**
     * Gets the serviceRequestId value for this CustomObject6Data.
     * 
     * @return serviceRequestId
     */
    public java.lang.String getServiceRequestId() {
        return serviceRequestId;
    }


    /**
     * Sets the serviceRequestId value for this CustomObject6Data.
     * 
     * @param serviceRequestId
     */
    public void setServiceRequestId(java.lang.String serviceRequestId) {
        this.serviceRequestId = serviceRequestId;
    }


    /**
     * Gets the solutionId value for this CustomObject6Data.
     * 
     * @return solutionId
     */
    public java.lang.String getSolutionId() {
        return solutionId;
    }


    /**
     * Sets the solutionId value for this CustomObject6Data.
     * 
     * @param solutionId
     */
    public void setSolutionId(java.lang.String solutionId) {
        this.solutionId = solutionId;
    }


    /**
     * Gets the vehicleId value for this CustomObject6Data.
     * 
     * @return vehicleId
     */
    public java.lang.String getVehicleId() {
        return vehicleId;
    }


    /**
     * Sets the vehicleId value for this CustomObject6Data.
     * 
     * @param vehicleId
     */
    public void setVehicleId(java.lang.String vehicleId) {
        this.vehicleId = vehicleId;
    }


    /**
     * Gets the ownerId value for this CustomObject6Data.
     * 
     * @return ownerId
     */
    public java.lang.String getOwnerId() {
        return ownerId;
    }


    /**
     * Sets the ownerId value for this CustomObject6Data.
     * 
     * @param ownerId
     */
    public void setOwnerId(java.lang.String ownerId) {
        this.ownerId = ownerId;
    }


    /**
     * Gets the updatedByAlias value for this CustomObject6Data.
     * 
     * @return updatedByAlias
     */
    public java.lang.String getUpdatedByAlias() {
        return updatedByAlias;
    }


    /**
     * Sets the updatedByAlias value for this CustomObject6Data.
     * 
     * @param updatedByAlias
     */
    public void setUpdatedByAlias(java.lang.String updatedByAlias) {
        this.updatedByAlias = updatedByAlias;
    }


    /**
     * Gets the updatedByEMailAddr value for this CustomObject6Data.
     * 
     * @return updatedByEMailAddr
     */
    public java.lang.String getUpdatedByEMailAddr() {
        return updatedByEMailAddr;
    }


    /**
     * Sets the updatedByEMailAddr value for this CustomObject6Data.
     * 
     * @param updatedByEMailAddr
     */
    public void setUpdatedByEMailAddr(java.lang.String updatedByEMailAddr) {
        this.updatedByEMailAddr = updatedByEMailAddr;
    }


    /**
     * Gets the updatedByExternalSystemId value for this CustomObject6Data.
     * 
     * @return updatedByExternalSystemId
     */
    public java.lang.String getUpdatedByExternalSystemId() {
        return updatedByExternalSystemId;
    }


    /**
     * Sets the updatedByExternalSystemId value for this CustomObject6Data.
     * 
     * @param updatedByExternalSystemId
     */
    public void setUpdatedByExternalSystemId(java.lang.String updatedByExternalSystemId) {
        this.updatedByExternalSystemId = updatedByExternalSystemId;
    }


    /**
     * Gets the updatedByFirstName value for this CustomObject6Data.
     * 
     * @return updatedByFirstName
     */
    public java.lang.String getUpdatedByFirstName() {
        return updatedByFirstName;
    }


    /**
     * Sets the updatedByFirstName value for this CustomObject6Data.
     * 
     * @param updatedByFirstName
     */
    public void setUpdatedByFirstName(java.lang.String updatedByFirstName) {
        this.updatedByFirstName = updatedByFirstName;
    }


    /**
     * Gets the updatedByFullName value for this CustomObject6Data.
     * 
     * @return updatedByFullName
     */
    public java.lang.String getUpdatedByFullName() {
        return updatedByFullName;
    }


    /**
     * Sets the updatedByFullName value for this CustomObject6Data.
     * 
     * @param updatedByFullName
     */
    public void setUpdatedByFullName(java.lang.String updatedByFullName) {
        this.updatedByFullName = updatedByFullName;
    }


    /**
     * Gets the updatedByIntegrationId value for this CustomObject6Data.
     * 
     * @return updatedByIntegrationId
     */
    public java.lang.String getUpdatedByIntegrationId() {
        return updatedByIntegrationId;
    }


    /**
     * Sets the updatedByIntegrationId value for this CustomObject6Data.
     * 
     * @param updatedByIntegrationId
     */
    public void setUpdatedByIntegrationId(java.lang.String updatedByIntegrationId) {
        this.updatedByIntegrationId = updatedByIntegrationId;
    }


    /**
     * Gets the updatedByLastName value for this CustomObject6Data.
     * 
     * @return updatedByLastName
     */
    public java.lang.String getUpdatedByLastName() {
        return updatedByLastName;
    }


    /**
     * Sets the updatedByLastName value for this CustomObject6Data.
     * 
     * @param updatedByLastName
     */
    public void setUpdatedByLastName(java.lang.String updatedByLastName) {
        this.updatedByLastName = updatedByLastName;
    }


    /**
     * Gets the updatedByUserSignInId value for this CustomObject6Data.
     * 
     * @return updatedByUserSignInId
     */
    public java.lang.String getUpdatedByUserSignInId() {
        return updatedByUserSignInId;
    }


    /**
     * Sets the updatedByUserSignInId value for this CustomObject6Data.
     * 
     * @param updatedByUserSignInId
     */
    public void setUpdatedByUserSignInId(java.lang.String updatedByUserSignInId) {
        this.updatedByUserSignInId = updatedByUserSignInId;
    }


    /**
     * Gets the createdByAlias value for this CustomObject6Data.
     * 
     * @return createdByAlias
     */
    public java.lang.String getCreatedByAlias() {
        return createdByAlias;
    }


    /**
     * Sets the createdByAlias value for this CustomObject6Data.
     * 
     * @param createdByAlias
     */
    public void setCreatedByAlias(java.lang.String createdByAlias) {
        this.createdByAlias = createdByAlias;
    }


    /**
     * Gets the createdByEMailAddr value for this CustomObject6Data.
     * 
     * @return createdByEMailAddr
     */
    public java.lang.String getCreatedByEMailAddr() {
        return createdByEMailAddr;
    }


    /**
     * Sets the createdByEMailAddr value for this CustomObject6Data.
     * 
     * @param createdByEMailAddr
     */
    public void setCreatedByEMailAddr(java.lang.String createdByEMailAddr) {
        this.createdByEMailAddr = createdByEMailAddr;
    }


    /**
     * Gets the createdByExternalSystemId value for this CustomObject6Data.
     * 
     * @return createdByExternalSystemId
     */
    public java.lang.String getCreatedByExternalSystemId() {
        return createdByExternalSystemId;
    }


    /**
     * Sets the createdByExternalSystemId value for this CustomObject6Data.
     * 
     * @param createdByExternalSystemId
     */
    public void setCreatedByExternalSystemId(java.lang.String createdByExternalSystemId) {
        this.createdByExternalSystemId = createdByExternalSystemId;
    }


    /**
     * Gets the createdByFirstName value for this CustomObject6Data.
     * 
     * @return createdByFirstName
     */
    public java.lang.String getCreatedByFirstName() {
        return createdByFirstName;
    }


    /**
     * Sets the createdByFirstName value for this CustomObject6Data.
     * 
     * @param createdByFirstName
     */
    public void setCreatedByFirstName(java.lang.String createdByFirstName) {
        this.createdByFirstName = createdByFirstName;
    }


    /**
     * Gets the createdByFullName value for this CustomObject6Data.
     * 
     * @return createdByFullName
     */
    public java.lang.String getCreatedByFullName() {
        return createdByFullName;
    }


    /**
     * Sets the createdByFullName value for this CustomObject6Data.
     * 
     * @param createdByFullName
     */
    public void setCreatedByFullName(java.lang.String createdByFullName) {
        this.createdByFullName = createdByFullName;
    }


    /**
     * Gets the createdByIntegrationId value for this CustomObject6Data.
     * 
     * @return createdByIntegrationId
     */
    public java.lang.String getCreatedByIntegrationId() {
        return createdByIntegrationId;
    }


    /**
     * Sets the createdByIntegrationId value for this CustomObject6Data.
     * 
     * @param createdByIntegrationId
     */
    public void setCreatedByIntegrationId(java.lang.String createdByIntegrationId) {
        this.createdByIntegrationId = createdByIntegrationId;
    }


    /**
     * Gets the createdByLastName value for this CustomObject6Data.
     * 
     * @return createdByLastName
     */
    public java.lang.String getCreatedByLastName() {
        return createdByLastName;
    }


    /**
     * Sets the createdByLastName value for this CustomObject6Data.
     * 
     * @param createdByLastName
     */
    public void setCreatedByLastName(java.lang.String createdByLastName) {
        this.createdByLastName = createdByLastName;
    }


    /**
     * Gets the createdByUserSignInId value for this CustomObject6Data.
     * 
     * @return createdByUserSignInId
     */
    public java.lang.String getCreatedByUserSignInId() {
        return createdByUserSignInId;
    }


    /**
     * Sets the createdByUserSignInId value for this CustomObject6Data.
     * 
     * @param createdByUserSignInId
     */
    public void setCreatedByUserSignInId(java.lang.String createdByUserSignInId) {
        this.createdByUserSignInId = createdByUserSignInId;
    }


    /**
     * Gets the accountExternalSystemId value for this CustomObject6Data.
     * 
     * @return accountExternalSystemId
     */
    public java.lang.String getAccountExternalSystemId() {
        return accountExternalSystemId;
    }


    /**
     * Sets the accountExternalSystemId value for this CustomObject6Data.
     * 
     * @param accountExternalSystemId
     */
    public void setAccountExternalSystemId(java.lang.String accountExternalSystemId) {
        this.accountExternalSystemId = accountExternalSystemId;
    }


    /**
     * Gets the accountIntegrationId value for this CustomObject6Data.
     * 
     * @return accountIntegrationId
     */
    public java.lang.String getAccountIntegrationId() {
        return accountIntegrationId;
    }


    /**
     * Sets the accountIntegrationId value for this CustomObject6Data.
     * 
     * @param accountIntegrationId
     */
    public void setAccountIntegrationId(java.lang.String accountIntegrationId) {
        this.accountIntegrationId = accountIntegrationId;
    }


    /**
     * Gets the accountLocation value for this CustomObject6Data.
     * 
     * @return accountLocation
     */
    public java.lang.String getAccountLocation() {
        return accountLocation;
    }


    /**
     * Sets the accountLocation value for this CustomObject6Data.
     * 
     * @param accountLocation
     */
    public void setAccountLocation(java.lang.String accountLocation) {
        this.accountLocation = accountLocation;
    }


    /**
     * Gets the accountName value for this CustomObject6Data.
     * 
     * @return accountName
     */
    public java.lang.String getAccountName() {
        return accountName;
    }


    /**
     * Sets the accountName value for this CustomObject6Data.
     * 
     * @param accountName
     */
    public void setAccountName(java.lang.String accountName) {
        this.accountName = accountName;
    }


    /**
     * Gets the activitySubject value for this CustomObject6Data.
     * 
     * @return activitySubject
     */
    public java.lang.String getActivitySubject() {
        return activitySubject;
    }


    /**
     * Sets the activitySubject value for this CustomObject6Data.
     * 
     * @param activitySubject
     */
    public void setActivitySubject(java.lang.String activitySubject) {
        this.activitySubject = activitySubject;
    }


    /**
     * Gets the activityExternalSystemId value for this CustomObject6Data.
     * 
     * @return activityExternalSystemId
     */
    public java.lang.String getActivityExternalSystemId() {
        return activityExternalSystemId;
    }


    /**
     * Sets the activityExternalSystemId value for this CustomObject6Data.
     * 
     * @param activityExternalSystemId
     */
    public void setActivityExternalSystemId(java.lang.String activityExternalSystemId) {
        this.activityExternalSystemId = activityExternalSystemId;
    }


    /**
     * Gets the activityIntegrationId value for this CustomObject6Data.
     * 
     * @return activityIntegrationId
     */
    public java.lang.String getActivityIntegrationId() {
        return activityIntegrationId;
    }


    /**
     * Sets the activityIntegrationId value for this CustomObject6Data.
     * 
     * @param activityIntegrationId
     */
    public void setActivityIntegrationId(java.lang.String activityIntegrationId) {
        this.activityIntegrationId = activityIntegrationId;
    }


    /**
     * Gets the assetIntegrationId value for this CustomObject6Data.
     * 
     * @return assetIntegrationId
     */
    public java.lang.String getAssetIntegrationId() {
        return assetIntegrationId;
    }


    /**
     * Sets the assetIntegrationId value for this CustomObject6Data.
     * 
     * @param assetIntegrationId
     */
    public void setAssetIntegrationId(java.lang.String assetIntegrationId) {
        this.assetIntegrationId = assetIntegrationId;
    }


    /**
     * Gets the assetProduct value for this CustomObject6Data.
     * 
     * @return assetProduct
     */
    public java.lang.String getAssetProduct() {
        return assetProduct;
    }


    /**
     * Sets the assetProduct value for this CustomObject6Data.
     * 
     * @param assetProduct
     */
    public void setAssetProduct(java.lang.String assetProduct) {
        this.assetProduct = assetProduct;
    }


    /**
     * Gets the assetSerialNumber value for this CustomObject6Data.
     * 
     * @return assetSerialNumber
     */
    public java.lang.String getAssetSerialNumber() {
        return assetSerialNumber;
    }


    /**
     * Sets the assetSerialNumber value for this CustomObject6Data.
     * 
     * @param assetSerialNumber
     */
    public void setAssetSerialNumber(java.lang.String assetSerialNumber) {
        this.assetSerialNumber = assetSerialNumber;
    }


    /**
     * Gets the assetExternalSystemId value for this CustomObject6Data.
     * 
     * @return assetExternalSystemId
     */
    public java.lang.String getAssetExternalSystemId() {
        return assetExternalSystemId;
    }


    /**
     * Sets the assetExternalSystemId value for this CustomObject6Data.
     * 
     * @param assetExternalSystemId
     */
    public void setAssetExternalSystemId(java.lang.String assetExternalSystemId) {
        this.assetExternalSystemId = assetExternalSystemId;
    }


    /**
     * Gets the campaignExternalSystemId value for this CustomObject6Data.
     * 
     * @return campaignExternalSystemId
     */
    public java.lang.String getCampaignExternalSystemId() {
        return campaignExternalSystemId;
    }


    /**
     * Sets the campaignExternalSystemId value for this CustomObject6Data.
     * 
     * @param campaignExternalSystemId
     */
    public void setCampaignExternalSystemId(java.lang.String campaignExternalSystemId) {
        this.campaignExternalSystemId = campaignExternalSystemId;
    }


    /**
     * Gets the campaignIntegrationId value for this CustomObject6Data.
     * 
     * @return campaignIntegrationId
     */
    public java.lang.String getCampaignIntegrationId() {
        return campaignIntegrationId;
    }


    /**
     * Sets the campaignIntegrationId value for this CustomObject6Data.
     * 
     * @param campaignIntegrationId
     */
    public void setCampaignIntegrationId(java.lang.String campaignIntegrationId) {
        this.campaignIntegrationId = campaignIntegrationId;
    }


    /**
     * Gets the campaignName value for this CustomObject6Data.
     * 
     * @return campaignName
     */
    public java.lang.String getCampaignName() {
        return campaignName;
    }


    /**
     * Sets the campaignName value for this CustomObject6Data.
     * 
     * @param campaignName
     */
    public void setCampaignName(java.lang.String campaignName) {
        this.campaignName = campaignName;
    }


    /**
     * Gets the contactEmail value for this CustomObject6Data.
     * 
     * @return contactEmail
     */
    public java.lang.String getContactEmail() {
        return contactEmail;
    }


    /**
     * Sets the contactEmail value for this CustomObject6Data.
     * 
     * @param contactEmail
     */
    public void setContactEmail(java.lang.String contactEmail) {
        this.contactEmail = contactEmail;
    }


    /**
     * Gets the contactExternalSystemId value for this CustomObject6Data.
     * 
     * @return contactExternalSystemId
     */
    public java.lang.String getContactExternalSystemId() {
        return contactExternalSystemId;
    }


    /**
     * Sets the contactExternalSystemId value for this CustomObject6Data.
     * 
     * @param contactExternalSystemId
     */
    public void setContactExternalSystemId(java.lang.String contactExternalSystemId) {
        this.contactExternalSystemId = contactExternalSystemId;
    }


    /**
     * Gets the contactFirstName value for this CustomObject6Data.
     * 
     * @return contactFirstName
     */
    public java.lang.String getContactFirstName() {
        return contactFirstName;
    }


    /**
     * Sets the contactFirstName value for this CustomObject6Data.
     * 
     * @param contactFirstName
     */
    public void setContactFirstName(java.lang.String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }


    /**
     * Gets the contactFullName value for this CustomObject6Data.
     * 
     * @return contactFullName
     */
    public java.lang.String getContactFullName() {
        return contactFullName;
    }


    /**
     * Sets the contactFullName value for this CustomObject6Data.
     * 
     * @param contactFullName
     */
    public void setContactFullName(java.lang.String contactFullName) {
        this.contactFullName = contactFullName;
    }


    /**
     * Gets the contactIntegrationId value for this CustomObject6Data.
     * 
     * @return contactIntegrationId
     */
    public java.lang.String getContactIntegrationId() {
        return contactIntegrationId;
    }


    /**
     * Sets the contactIntegrationId value for this CustomObject6Data.
     * 
     * @param contactIntegrationId
     */
    public void setContactIntegrationId(java.lang.String contactIntegrationId) {
        this.contactIntegrationId = contactIntegrationId;
    }


    /**
     * Gets the contactLastName value for this CustomObject6Data.
     * 
     * @return contactLastName
     */
    public java.lang.String getContactLastName() {
        return contactLastName;
    }


    /**
     * Sets the contactLastName value for this CustomObject6Data.
     * 
     * @param contactLastName
     */
    public void setContactLastName(java.lang.String contactLastName) {
        this.contactLastName = contactLastName;
    }


    /**
     * Gets the customObject1ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject1ExternalSystemId
     */
    public java.lang.String getCustomObject1ExternalSystemId() {
        return customObject1ExternalSystemId;
    }


    /**
     * Sets the customObject1ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject1ExternalSystemId
     */
    public void setCustomObject1ExternalSystemId(java.lang.String customObject1ExternalSystemId) {
        this.customObject1ExternalSystemId = customObject1ExternalSystemId;
    }


    /**
     * Gets the customObject1IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject1IntegrationId
     */
    public java.lang.String getCustomObject1IntegrationId() {
        return customObject1IntegrationId;
    }


    /**
     * Sets the customObject1IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject1IntegrationId
     */
    public void setCustomObject1IntegrationId(java.lang.String customObject1IntegrationId) {
        this.customObject1IntegrationId = customObject1IntegrationId;
    }


    /**
     * Gets the customObject1Name value for this CustomObject6Data.
     * 
     * @return customObject1Name
     */
    public java.lang.String getCustomObject1Name() {
        return customObject1Name;
    }


    /**
     * Sets the customObject1Name value for this CustomObject6Data.
     * 
     * @param customObject1Name
     */
    public void setCustomObject1Name(java.lang.String customObject1Name) {
        this.customObject1Name = customObject1Name;
    }


    /**
     * Gets the customObject2ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject2ExternalSystemId
     */
    public java.lang.String getCustomObject2ExternalSystemId() {
        return customObject2ExternalSystemId;
    }


    /**
     * Sets the customObject2ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject2ExternalSystemId
     */
    public void setCustomObject2ExternalSystemId(java.lang.String customObject2ExternalSystemId) {
        this.customObject2ExternalSystemId = customObject2ExternalSystemId;
    }


    /**
     * Gets the customObject2IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject2IntegrationId
     */
    public java.lang.String getCustomObject2IntegrationId() {
        return customObject2IntegrationId;
    }


    /**
     * Sets the customObject2IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject2IntegrationId
     */
    public void setCustomObject2IntegrationId(java.lang.String customObject2IntegrationId) {
        this.customObject2IntegrationId = customObject2IntegrationId;
    }


    /**
     * Gets the customObject2Name value for this CustomObject6Data.
     * 
     * @return customObject2Name
     */
    public java.lang.String getCustomObject2Name() {
        return customObject2Name;
    }


    /**
     * Sets the customObject2Name value for this CustomObject6Data.
     * 
     * @param customObject2Name
     */
    public void setCustomObject2Name(java.lang.String customObject2Name) {
        this.customObject2Name = customObject2Name;
    }


    /**
     * Gets the customObject3IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject3IntegrationId
     */
    public java.lang.String getCustomObject3IntegrationId() {
        return customObject3IntegrationId;
    }


    /**
     * Sets the customObject3IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject3IntegrationId
     */
    public void setCustomObject3IntegrationId(java.lang.String customObject3IntegrationId) {
        this.customObject3IntegrationId = customObject3IntegrationId;
    }


    /**
     * Gets the customObject3ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject3ExternalSystemId
     */
    public java.lang.String getCustomObject3ExternalSystemId() {
        return customObject3ExternalSystemId;
    }


    /**
     * Sets the customObject3ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject3ExternalSystemId
     */
    public void setCustomObject3ExternalSystemId(java.lang.String customObject3ExternalSystemId) {
        this.customObject3ExternalSystemId = customObject3ExternalSystemId;
    }


    /**
     * Gets the customObject3Name value for this CustomObject6Data.
     * 
     * @return customObject3Name
     */
    public java.lang.String getCustomObject3Name() {
        return customObject3Name;
    }


    /**
     * Sets the customObject3Name value for this CustomObject6Data.
     * 
     * @param customObject3Name
     */
    public void setCustomObject3Name(java.lang.String customObject3Name) {
        this.customObject3Name = customObject3Name;
    }


    /**
     * Gets the customObject4Name value for this CustomObject6Data.
     * 
     * @return customObject4Name
     */
    public java.lang.String getCustomObject4Name() {
        return customObject4Name;
    }


    /**
     * Sets the customObject4Name value for this CustomObject6Data.
     * 
     * @param customObject4Name
     */
    public void setCustomObject4Name(java.lang.String customObject4Name) {
        this.customObject4Name = customObject4Name;
    }


    /**
     * Gets the customObject4ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject4ExternalSystemId
     */
    public java.lang.String getCustomObject4ExternalSystemId() {
        return customObject4ExternalSystemId;
    }


    /**
     * Sets the customObject4ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject4ExternalSystemId
     */
    public void setCustomObject4ExternalSystemId(java.lang.String customObject4ExternalSystemId) {
        this.customObject4ExternalSystemId = customObject4ExternalSystemId;
    }


    /**
     * Gets the customObject4IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject4IntegrationId
     */
    public java.lang.String getCustomObject4IntegrationId() {
        return customObject4IntegrationId;
    }


    /**
     * Sets the customObject4IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject4IntegrationId
     */
    public void setCustomObject4IntegrationId(java.lang.String customObject4IntegrationId) {
        this.customObject4IntegrationId = customObject4IntegrationId;
    }


    /**
     * Gets the customObject5Name value for this CustomObject6Data.
     * 
     * @return customObject5Name
     */
    public java.lang.String getCustomObject5Name() {
        return customObject5Name;
    }


    /**
     * Sets the customObject5Name value for this CustomObject6Data.
     * 
     * @param customObject5Name
     */
    public void setCustomObject5Name(java.lang.String customObject5Name) {
        this.customObject5Name = customObject5Name;
    }


    /**
     * Gets the customObject5ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject5ExternalSystemId
     */
    public java.lang.String getCustomObject5ExternalSystemId() {
        return customObject5ExternalSystemId;
    }


    /**
     * Sets the customObject5ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject5ExternalSystemId
     */
    public void setCustomObject5ExternalSystemId(java.lang.String customObject5ExternalSystemId) {
        this.customObject5ExternalSystemId = customObject5ExternalSystemId;
    }


    /**
     * Gets the customObject5IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject5IntegrationId
     */
    public java.lang.String getCustomObject5IntegrationId() {
        return customObject5IntegrationId;
    }


    /**
     * Sets the customObject5IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject5IntegrationId
     */
    public void setCustomObject5IntegrationId(java.lang.String customObject5IntegrationId) {
        this.customObject5IntegrationId = customObject5IntegrationId;
    }


    /**
     * Gets the customObject6Name value for this CustomObject6Data.
     * 
     * @return customObject6Name
     */
    public java.lang.String getCustomObject6Name() {
        return customObject6Name;
    }


    /**
     * Sets the customObject6Name value for this CustomObject6Data.
     * 
     * @param customObject6Name
     */
    public void setCustomObject6Name(java.lang.String customObject6Name) {
        this.customObject6Name = customObject6Name;
    }


    /**
     * Gets the customObject6ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject6ExternalSystemId
     */
    public java.lang.String getCustomObject6ExternalSystemId() {
        return customObject6ExternalSystemId;
    }


    /**
     * Sets the customObject6ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject6ExternalSystemId
     */
    public void setCustomObject6ExternalSystemId(java.lang.String customObject6ExternalSystemId) {
        this.customObject6ExternalSystemId = customObject6ExternalSystemId;
    }


    /**
     * Gets the customObject6IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject6IntegrationId
     */
    public java.lang.String getCustomObject6IntegrationId() {
        return customObject6IntegrationId;
    }


    /**
     * Sets the customObject6IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject6IntegrationId
     */
    public void setCustomObject6IntegrationId(java.lang.String customObject6IntegrationId) {
        this.customObject6IntegrationId = customObject6IntegrationId;
    }


    /**
     * Gets the customObject7Name value for this CustomObject6Data.
     * 
     * @return customObject7Name
     */
    public java.lang.String getCustomObject7Name() {
        return customObject7Name;
    }


    /**
     * Sets the customObject7Name value for this CustomObject6Data.
     * 
     * @param customObject7Name
     */
    public void setCustomObject7Name(java.lang.String customObject7Name) {
        this.customObject7Name = customObject7Name;
    }


    /**
     * Gets the customObject7ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject7ExternalSystemId
     */
    public java.lang.String getCustomObject7ExternalSystemId() {
        return customObject7ExternalSystemId;
    }


    /**
     * Sets the customObject7ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject7ExternalSystemId
     */
    public void setCustomObject7ExternalSystemId(java.lang.String customObject7ExternalSystemId) {
        this.customObject7ExternalSystemId = customObject7ExternalSystemId;
    }


    /**
     * Gets the customObject7IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject7IntegrationId
     */
    public java.lang.String getCustomObject7IntegrationId() {
        return customObject7IntegrationId;
    }


    /**
     * Sets the customObject7IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject7IntegrationId
     */
    public void setCustomObject7IntegrationId(java.lang.String customObject7IntegrationId) {
        this.customObject7IntegrationId = customObject7IntegrationId;
    }


    /**
     * Gets the customObject8Name value for this CustomObject6Data.
     * 
     * @return customObject8Name
     */
    public java.lang.String getCustomObject8Name() {
        return customObject8Name;
    }


    /**
     * Sets the customObject8Name value for this CustomObject6Data.
     * 
     * @param customObject8Name
     */
    public void setCustomObject8Name(java.lang.String customObject8Name) {
        this.customObject8Name = customObject8Name;
    }


    /**
     * Gets the customObject8ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject8ExternalSystemId
     */
    public java.lang.String getCustomObject8ExternalSystemId() {
        return customObject8ExternalSystemId;
    }


    /**
     * Sets the customObject8ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject8ExternalSystemId
     */
    public void setCustomObject8ExternalSystemId(java.lang.String customObject8ExternalSystemId) {
        this.customObject8ExternalSystemId = customObject8ExternalSystemId;
    }


    /**
     * Gets the customObject8IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject8IntegrationId
     */
    public java.lang.String getCustomObject8IntegrationId() {
        return customObject8IntegrationId;
    }


    /**
     * Sets the customObject8IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject8IntegrationId
     */
    public void setCustomObject8IntegrationId(java.lang.String customObject8IntegrationId) {
        this.customObject8IntegrationId = customObject8IntegrationId;
    }


    /**
     * Gets the customObject9Name value for this CustomObject6Data.
     * 
     * @return customObject9Name
     */
    public java.lang.String getCustomObject9Name() {
        return customObject9Name;
    }


    /**
     * Sets the customObject9Name value for this CustomObject6Data.
     * 
     * @param customObject9Name
     */
    public void setCustomObject9Name(java.lang.String customObject9Name) {
        this.customObject9Name = customObject9Name;
    }


    /**
     * Gets the customObject9ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject9ExternalSystemId
     */
    public java.lang.String getCustomObject9ExternalSystemId() {
        return customObject9ExternalSystemId;
    }


    /**
     * Sets the customObject9ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject9ExternalSystemId
     */
    public void setCustomObject9ExternalSystemId(java.lang.String customObject9ExternalSystemId) {
        this.customObject9ExternalSystemId = customObject9ExternalSystemId;
    }


    /**
     * Gets the customObject9IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject9IntegrationId
     */
    public java.lang.String getCustomObject9IntegrationId() {
        return customObject9IntegrationId;
    }


    /**
     * Sets the customObject9IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject9IntegrationId
     */
    public void setCustomObject9IntegrationId(java.lang.String customObject9IntegrationId) {
        this.customObject9IntegrationId = customObject9IntegrationId;
    }


    /**
     * Gets the customObject10Name value for this CustomObject6Data.
     * 
     * @return customObject10Name
     */
    public java.lang.String getCustomObject10Name() {
        return customObject10Name;
    }


    /**
     * Sets the customObject10Name value for this CustomObject6Data.
     * 
     * @param customObject10Name
     */
    public void setCustomObject10Name(java.lang.String customObject10Name) {
        this.customObject10Name = customObject10Name;
    }


    /**
     * Gets the customObject10ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject10ExternalSystemId
     */
    public java.lang.String getCustomObject10ExternalSystemId() {
        return customObject10ExternalSystemId;
    }


    /**
     * Sets the customObject10ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject10ExternalSystemId
     */
    public void setCustomObject10ExternalSystemId(java.lang.String customObject10ExternalSystemId) {
        this.customObject10ExternalSystemId = customObject10ExternalSystemId;
    }


    /**
     * Gets the customObject10IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject10IntegrationId
     */
    public java.lang.String getCustomObject10IntegrationId() {
        return customObject10IntegrationId;
    }


    /**
     * Sets the customObject10IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject10IntegrationId
     */
    public void setCustomObject10IntegrationId(java.lang.String customObject10IntegrationId) {
        this.customObject10IntegrationId = customObject10IntegrationId;
    }


    /**
     * Gets the customObject11Name value for this CustomObject6Data.
     * 
     * @return customObject11Name
     */
    public java.lang.String getCustomObject11Name() {
        return customObject11Name;
    }


    /**
     * Sets the customObject11Name value for this CustomObject6Data.
     * 
     * @param customObject11Name
     */
    public void setCustomObject11Name(java.lang.String customObject11Name) {
        this.customObject11Name = customObject11Name;
    }


    /**
     * Gets the customObject11ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject11ExternalSystemId
     */
    public java.lang.String getCustomObject11ExternalSystemId() {
        return customObject11ExternalSystemId;
    }


    /**
     * Sets the customObject11ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject11ExternalSystemId
     */
    public void setCustomObject11ExternalSystemId(java.lang.String customObject11ExternalSystemId) {
        this.customObject11ExternalSystemId = customObject11ExternalSystemId;
    }


    /**
     * Gets the customObject11IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject11IntegrationId
     */
    public java.lang.String getCustomObject11IntegrationId() {
        return customObject11IntegrationId;
    }


    /**
     * Sets the customObject11IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject11IntegrationId
     */
    public void setCustomObject11IntegrationId(java.lang.String customObject11IntegrationId) {
        this.customObject11IntegrationId = customObject11IntegrationId;
    }


    /**
     * Gets the customObject12Name value for this CustomObject6Data.
     * 
     * @return customObject12Name
     */
    public java.lang.String getCustomObject12Name() {
        return customObject12Name;
    }


    /**
     * Sets the customObject12Name value for this CustomObject6Data.
     * 
     * @param customObject12Name
     */
    public void setCustomObject12Name(java.lang.String customObject12Name) {
        this.customObject12Name = customObject12Name;
    }


    /**
     * Gets the customObject12ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject12ExternalSystemId
     */
    public java.lang.String getCustomObject12ExternalSystemId() {
        return customObject12ExternalSystemId;
    }


    /**
     * Sets the customObject12ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject12ExternalSystemId
     */
    public void setCustomObject12ExternalSystemId(java.lang.String customObject12ExternalSystemId) {
        this.customObject12ExternalSystemId = customObject12ExternalSystemId;
    }


    /**
     * Gets the customObject12IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject12IntegrationId
     */
    public java.lang.String getCustomObject12IntegrationId() {
        return customObject12IntegrationId;
    }


    /**
     * Sets the customObject12IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject12IntegrationId
     */
    public void setCustomObject12IntegrationId(java.lang.String customObject12IntegrationId) {
        this.customObject12IntegrationId = customObject12IntegrationId;
    }


    /**
     * Gets the customObject13Name value for this CustomObject6Data.
     * 
     * @return customObject13Name
     */
    public java.lang.String getCustomObject13Name() {
        return customObject13Name;
    }


    /**
     * Sets the customObject13Name value for this CustomObject6Data.
     * 
     * @param customObject13Name
     */
    public void setCustomObject13Name(java.lang.String customObject13Name) {
        this.customObject13Name = customObject13Name;
    }


    /**
     * Gets the customObject13ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject13ExternalSystemId
     */
    public java.lang.String getCustomObject13ExternalSystemId() {
        return customObject13ExternalSystemId;
    }


    /**
     * Sets the customObject13ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject13ExternalSystemId
     */
    public void setCustomObject13ExternalSystemId(java.lang.String customObject13ExternalSystemId) {
        this.customObject13ExternalSystemId = customObject13ExternalSystemId;
    }


    /**
     * Gets the customObject13IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject13IntegrationId
     */
    public java.lang.String getCustomObject13IntegrationId() {
        return customObject13IntegrationId;
    }


    /**
     * Sets the customObject13IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject13IntegrationId
     */
    public void setCustomObject13IntegrationId(java.lang.String customObject13IntegrationId) {
        this.customObject13IntegrationId = customObject13IntegrationId;
    }


    /**
     * Gets the customObject14Name value for this CustomObject6Data.
     * 
     * @return customObject14Name
     */
    public java.lang.String getCustomObject14Name() {
        return customObject14Name;
    }


    /**
     * Sets the customObject14Name value for this CustomObject6Data.
     * 
     * @param customObject14Name
     */
    public void setCustomObject14Name(java.lang.String customObject14Name) {
        this.customObject14Name = customObject14Name;
    }


    /**
     * Gets the customObject14ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject14ExternalSystemId
     */
    public java.lang.String getCustomObject14ExternalSystemId() {
        return customObject14ExternalSystemId;
    }


    /**
     * Sets the customObject14ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject14ExternalSystemId
     */
    public void setCustomObject14ExternalSystemId(java.lang.String customObject14ExternalSystemId) {
        this.customObject14ExternalSystemId = customObject14ExternalSystemId;
    }


    /**
     * Gets the customObject14IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject14IntegrationId
     */
    public java.lang.String getCustomObject14IntegrationId() {
        return customObject14IntegrationId;
    }


    /**
     * Sets the customObject14IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject14IntegrationId
     */
    public void setCustomObject14IntegrationId(java.lang.String customObject14IntegrationId) {
        this.customObject14IntegrationId = customObject14IntegrationId;
    }


    /**
     * Gets the customObject15Name value for this CustomObject6Data.
     * 
     * @return customObject15Name
     */
    public java.lang.String getCustomObject15Name() {
        return customObject15Name;
    }


    /**
     * Sets the customObject15Name value for this CustomObject6Data.
     * 
     * @param customObject15Name
     */
    public void setCustomObject15Name(java.lang.String customObject15Name) {
        this.customObject15Name = customObject15Name;
    }


    /**
     * Gets the customObject15ExternalSystemId value for this CustomObject6Data.
     * 
     * @return customObject15ExternalSystemId
     */
    public java.lang.String getCustomObject15ExternalSystemId() {
        return customObject15ExternalSystemId;
    }


    /**
     * Sets the customObject15ExternalSystemId value for this CustomObject6Data.
     * 
     * @param customObject15ExternalSystemId
     */
    public void setCustomObject15ExternalSystemId(java.lang.String customObject15ExternalSystemId) {
        this.customObject15ExternalSystemId = customObject15ExternalSystemId;
    }


    /**
     * Gets the customObject15IntegrationId value for this CustomObject6Data.
     * 
     * @return customObject15IntegrationId
     */
    public java.lang.String getCustomObject15IntegrationId() {
        return customObject15IntegrationId;
    }


    /**
     * Sets the customObject15IntegrationId value for this CustomObject6Data.
     * 
     * @param customObject15IntegrationId
     */
    public void setCustomObject15IntegrationId(java.lang.String customObject15IntegrationId) {
        this.customObject15IntegrationId = customObject15IntegrationId;
    }


    /**
     * Gets the dealerName value for this CustomObject6Data.
     * 
     * @return dealerName
     */
    public java.lang.String getDealerName() {
        return dealerName;
    }


    /**
     * Sets the dealerName value for this CustomObject6Data.
     * 
     * @param dealerName
     */
    public void setDealerName(java.lang.String dealerName) {
        this.dealerName = dealerName;
    }


    /**
     * Gets the dealerExternalSystemId value for this CustomObject6Data.
     * 
     * @return dealerExternalSystemId
     */
    public java.lang.String getDealerExternalSystemId() {
        return dealerExternalSystemId;
    }


    /**
     * Sets the dealerExternalSystemId value for this CustomObject6Data.
     * 
     * @param dealerExternalSystemId
     */
    public void setDealerExternalSystemId(java.lang.String dealerExternalSystemId) {
        this.dealerExternalSystemId = dealerExternalSystemId;
    }


    /**
     * Gets the dealerIntegrationId value for this CustomObject6Data.
     * 
     * @return dealerIntegrationId
     */
    public java.lang.String getDealerIntegrationId() {
        return dealerIntegrationId;
    }


    /**
     * Sets the dealerIntegrationId value for this CustomObject6Data.
     * 
     * @param dealerIntegrationId
     */
    public void setDealerIntegrationId(java.lang.String dealerIntegrationId) {
        this.dealerIntegrationId = dealerIntegrationId;
    }


    /**
     * Gets the fundName value for this CustomObject6Data.
     * 
     * @return fundName
     */
    public java.lang.String getFundName() {
        return fundName;
    }


    /**
     * Sets the fundName value for this CustomObject6Data.
     * 
     * @param fundName
     */
    public void setFundName(java.lang.String fundName) {
        this.fundName = fundName;
    }


    /**
     * Gets the fundRequestName value for this CustomObject6Data.
     * 
     * @return fundRequestName
     */
    public java.lang.String getFundRequestName() {
        return fundRequestName;
    }


    /**
     * Sets the fundRequestName value for this CustomObject6Data.
     * 
     * @param fundRequestName
     */
    public void setFundRequestName(java.lang.String fundRequestName) {
        this.fundRequestName = fundRequestName;
    }


    /**
     * Gets the fundRequestExternalSystemId value for this CustomObject6Data.
     * 
     * @return fundRequestExternalSystemId
     */
    public java.lang.String getFundRequestExternalSystemId() {
        return fundRequestExternalSystemId;
    }


    /**
     * Sets the fundRequestExternalSystemId value for this CustomObject6Data.
     * 
     * @param fundRequestExternalSystemId
     */
    public void setFundRequestExternalSystemId(java.lang.String fundRequestExternalSystemId) {
        this.fundRequestExternalSystemId = fundRequestExternalSystemId;
    }


    /**
     * Gets the householdName value for this CustomObject6Data.
     * 
     * @return householdName
     */
    public java.lang.String getHouseholdName() {
        return householdName;
    }


    /**
     * Sets the householdName value for this CustomObject6Data.
     * 
     * @param householdName
     */
    public void setHouseholdName(java.lang.String householdName) {
        this.householdName = householdName;
    }


    /**
     * Gets the householdIntegrationId value for this CustomObject6Data.
     * 
     * @return householdIntegrationId
     */
    public java.lang.String getHouseholdIntegrationId() {
        return householdIntegrationId;
    }


    /**
     * Sets the householdIntegrationId value for this CustomObject6Data.
     * 
     * @param householdIntegrationId
     */
    public void setHouseholdIntegrationId(java.lang.String householdIntegrationId) {
        this.householdIntegrationId = householdIntegrationId;
    }


    /**
     * Gets the householdExternalSystemId value for this CustomObject6Data.
     * 
     * @return householdExternalSystemId
     */
    public java.lang.String getHouseholdExternalSystemId() {
        return householdExternalSystemId;
    }


    /**
     * Sets the householdExternalSystemId value for this CustomObject6Data.
     * 
     * @param householdExternalSystemId
     */
    public void setHouseholdExternalSystemId(java.lang.String householdExternalSystemId) {
        this.householdExternalSystemId = householdExternalSystemId;
    }


    /**
     * Gets the leadExternalSystemId value for this CustomObject6Data.
     * 
     * @return leadExternalSystemId
     */
    public java.lang.String getLeadExternalSystemId() {
        return leadExternalSystemId;
    }


    /**
     * Sets the leadExternalSystemId value for this CustomObject6Data.
     * 
     * @param leadExternalSystemId
     */
    public void setLeadExternalSystemId(java.lang.String leadExternalSystemId) {
        this.leadExternalSystemId = leadExternalSystemId;
    }


    /**
     * Gets the leadFirstName value for this CustomObject6Data.
     * 
     * @return leadFirstName
     */
    public java.lang.String getLeadFirstName() {
        return leadFirstName;
    }


    /**
     * Sets the leadFirstName value for this CustomObject6Data.
     * 
     * @param leadFirstName
     */
    public void setLeadFirstName(java.lang.String leadFirstName) {
        this.leadFirstName = leadFirstName;
    }


    /**
     * Gets the leadFullName value for this CustomObject6Data.
     * 
     * @return leadFullName
     */
    public java.lang.String getLeadFullName() {
        return leadFullName;
    }


    /**
     * Sets the leadFullName value for this CustomObject6Data.
     * 
     * @param leadFullName
     */
    public void setLeadFullName(java.lang.String leadFullName) {
        this.leadFullName = leadFullName;
    }


    /**
     * Gets the leadIntegrationId value for this CustomObject6Data.
     * 
     * @return leadIntegrationId
     */
    public java.lang.String getLeadIntegrationId() {
        return leadIntegrationId;
    }


    /**
     * Sets the leadIntegrationId value for this CustomObject6Data.
     * 
     * @param leadIntegrationId
     */
    public void setLeadIntegrationId(java.lang.String leadIntegrationId) {
        this.leadIntegrationId = leadIntegrationId;
    }


    /**
     * Gets the leadLastName value for this CustomObject6Data.
     * 
     * @return leadLastName
     */
    public java.lang.String getLeadLastName() {
        return leadLastName;
    }


    /**
     * Sets the leadLastName value for this CustomObject6Data.
     * 
     * @param leadLastName
     */
    public void setLeadLastName(java.lang.String leadLastName) {
        this.leadLastName = leadLastName;
    }


    /**
     * Gets the medEdName value for this CustomObject6Data.
     * 
     * @return medEdName
     */
    public java.lang.String getMedEdName() {
        return medEdName;
    }


    /**
     * Sets the medEdName value for this CustomObject6Data.
     * 
     * @param medEdName
     */
    public void setMedEdName(java.lang.String medEdName) {
        this.medEdName = medEdName;
    }


    /**
     * Gets the medEdExternalSystemId value for this CustomObject6Data.
     * 
     * @return medEdExternalSystemId
     */
    public java.lang.String getMedEdExternalSystemId() {
        return medEdExternalSystemId;
    }


    /**
     * Sets the medEdExternalSystemId value for this CustomObject6Data.
     * 
     * @param medEdExternalSystemId
     */
    public void setMedEdExternalSystemId(java.lang.String medEdExternalSystemId) {
        this.medEdExternalSystemId = medEdExternalSystemId;
    }


    /**
     * Gets the medEdIntegrationId value for this CustomObject6Data.
     * 
     * @return medEdIntegrationId
     */
    public java.lang.String getMedEdIntegrationId() {
        return medEdIntegrationId;
    }


    /**
     * Sets the medEdIntegrationId value for this CustomObject6Data.
     * 
     * @param medEdIntegrationId
     */
    public void setMedEdIntegrationId(java.lang.String medEdIntegrationId) {
        this.medEdIntegrationId = medEdIntegrationId;
    }


    /**
     * Gets the opportunityAccountId value for this CustomObject6Data.
     * 
     * @return opportunityAccountId
     */
    public java.lang.String getOpportunityAccountId() {
        return opportunityAccountId;
    }


    /**
     * Sets the opportunityAccountId value for this CustomObject6Data.
     * 
     * @param opportunityAccountId
     */
    public void setOpportunityAccountId(java.lang.String opportunityAccountId) {
        this.opportunityAccountId = opportunityAccountId;
    }


    /**
     * Gets the opportunityExternalSystemId value for this CustomObject6Data.
     * 
     * @return opportunityExternalSystemId
     */
    public java.lang.String getOpportunityExternalSystemId() {
        return opportunityExternalSystemId;
    }


    /**
     * Sets the opportunityExternalSystemId value for this CustomObject6Data.
     * 
     * @param opportunityExternalSystemId
     */
    public void setOpportunityExternalSystemId(java.lang.String opportunityExternalSystemId) {
        this.opportunityExternalSystemId = opportunityExternalSystemId;
    }


    /**
     * Gets the opportunityIntegrationId value for this CustomObject6Data.
     * 
     * @return opportunityIntegrationId
     */
    public java.lang.String getOpportunityIntegrationId() {
        return opportunityIntegrationId;
    }


    /**
     * Sets the opportunityIntegrationId value for this CustomObject6Data.
     * 
     * @param opportunityIntegrationId
     */
    public void setOpportunityIntegrationId(java.lang.String opportunityIntegrationId) {
        this.opportunityIntegrationId = opportunityIntegrationId;
    }


    /**
     * Gets the opportunityName value for this CustomObject6Data.
     * 
     * @return opportunityName
     */
    public java.lang.String getOpportunityName() {
        return opportunityName;
    }


    /**
     * Sets the opportunityName value for this CustomObject6Data.
     * 
     * @param opportunityName
     */
    public void setOpportunityName(java.lang.String opportunityName) {
        this.opportunityName = opportunityName;
    }


    /**
     * Gets the portfolioAccountNumber value for this CustomObject6Data.
     * 
     * @return portfolioAccountNumber
     */
    public java.lang.String getPortfolioAccountNumber() {
        return portfolioAccountNumber;
    }


    /**
     * Sets the portfolioAccountNumber value for this CustomObject6Data.
     * 
     * @param portfolioAccountNumber
     */
    public void setPortfolioAccountNumber(java.lang.String portfolioAccountNumber) {
        this.portfolioAccountNumber = portfolioAccountNumber;
    }


    /**
     * Gets the portfolioExternalSystemId value for this CustomObject6Data.
     * 
     * @return portfolioExternalSystemId
     */
    public java.lang.String getPortfolioExternalSystemId() {
        return portfolioExternalSystemId;
    }


    /**
     * Sets the portfolioExternalSystemId value for this CustomObject6Data.
     * 
     * @param portfolioExternalSystemId
     */
    public void setPortfolioExternalSystemId(java.lang.String portfolioExternalSystemId) {
        this.portfolioExternalSystemId = portfolioExternalSystemId;
    }


    /**
     * Gets the portfolioIntegrationId value for this CustomObject6Data.
     * 
     * @return portfolioIntegrationId
     */
    public java.lang.String getPortfolioIntegrationId() {
        return portfolioIntegrationId;
    }


    /**
     * Sets the portfolioIntegrationId value for this CustomObject6Data.
     * 
     * @param portfolioIntegrationId
     */
    public void setPortfolioIntegrationId(java.lang.String portfolioIntegrationId) {
        this.portfolioIntegrationId = portfolioIntegrationId;
    }


    /**
     * Gets the productIntegrationId value for this CustomObject6Data.
     * 
     * @return productIntegrationId
     */
    public java.lang.String getProductIntegrationId() {
        return productIntegrationId;
    }


    /**
     * Sets the productIntegrationId value for this CustomObject6Data.
     * 
     * @param productIntegrationId
     */
    public void setProductIntegrationId(java.lang.String productIntegrationId) {
        this.productIntegrationId = productIntegrationId;
    }


    /**
     * Gets the productName value for this CustomObject6Data.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this CustomObject6Data.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the productExternalSystemId value for this CustomObject6Data.
     * 
     * @return productExternalSystemId
     */
    public java.lang.String getProductExternalSystemId() {
        return productExternalSystemId;
    }


    /**
     * Sets the productExternalSystemId value for this CustomObject6Data.
     * 
     * @param productExternalSystemId
     */
    public void setProductExternalSystemId(java.lang.String productExternalSystemId) {
        this.productExternalSystemId = productExternalSystemId;
    }


    /**
     * Gets the serviceRequestExternalSystemId value for this CustomObject6Data.
     * 
     * @return serviceRequestExternalSystemId
     */
    public java.lang.String getServiceRequestExternalSystemId() {
        return serviceRequestExternalSystemId;
    }


    /**
     * Sets the serviceRequestExternalSystemId value for this CustomObject6Data.
     * 
     * @param serviceRequestExternalSystemId
     */
    public void setServiceRequestExternalSystemId(java.lang.String serviceRequestExternalSystemId) {
        this.serviceRequestExternalSystemId = serviceRequestExternalSystemId;
    }


    /**
     * Gets the serviceRequestIntegrationId value for this CustomObject6Data.
     * 
     * @return serviceRequestIntegrationId
     */
    public java.lang.String getServiceRequestIntegrationId() {
        return serviceRequestIntegrationId;
    }


    /**
     * Sets the serviceRequestIntegrationId value for this CustomObject6Data.
     * 
     * @param serviceRequestIntegrationId
     */
    public void setServiceRequestIntegrationId(java.lang.String serviceRequestIntegrationId) {
        this.serviceRequestIntegrationId = serviceRequestIntegrationId;
    }


    /**
     * Gets the serviceRequestSRNumber value for this CustomObject6Data.
     * 
     * @return serviceRequestSRNumber
     */
    public java.lang.String getServiceRequestSRNumber() {
        return serviceRequestSRNumber;
    }


    /**
     * Sets the serviceRequestSRNumber value for this CustomObject6Data.
     * 
     * @param serviceRequestSRNumber
     */
    public void setServiceRequestSRNumber(java.lang.String serviceRequestSRNumber) {
        this.serviceRequestSRNumber = serviceRequestSRNumber;
    }


    /**
     * Gets the solutionExternalSystemId value for this CustomObject6Data.
     * 
     * @return solutionExternalSystemId
     */
    public java.lang.String getSolutionExternalSystemId() {
        return solutionExternalSystemId;
    }


    /**
     * Sets the solutionExternalSystemId value for this CustomObject6Data.
     * 
     * @param solutionExternalSystemId
     */
    public void setSolutionExternalSystemId(java.lang.String solutionExternalSystemId) {
        this.solutionExternalSystemId = solutionExternalSystemId;
    }


    /**
     * Gets the solutionIntegrationId value for this CustomObject6Data.
     * 
     * @return solutionIntegrationId
     */
    public java.lang.String getSolutionIntegrationId() {
        return solutionIntegrationId;
    }


    /**
     * Sets the solutionIntegrationId value for this CustomObject6Data.
     * 
     * @param solutionIntegrationId
     */
    public void setSolutionIntegrationId(java.lang.String solutionIntegrationId) {
        this.solutionIntegrationId = solutionIntegrationId;
    }


    /**
     * Gets the solutionTitle value for this CustomObject6Data.
     * 
     * @return solutionTitle
     */
    public java.lang.String getSolutionTitle() {
        return solutionTitle;
    }


    /**
     * Sets the solutionTitle value for this CustomObject6Data.
     * 
     * @param solutionTitle
     */
    public void setSolutionTitle(java.lang.String solutionTitle) {
        this.solutionTitle = solutionTitle;
    }


    /**
     * Gets the vehicleIntegrationId value for this CustomObject6Data.
     * 
     * @return vehicleIntegrationId
     */
    public java.lang.String getVehicleIntegrationId() {
        return vehicleIntegrationId;
    }


    /**
     * Sets the vehicleIntegrationId value for this CustomObject6Data.
     * 
     * @param vehicleIntegrationId
     */
    public void setVehicleIntegrationId(java.lang.String vehicleIntegrationId) {
        this.vehicleIntegrationId = vehicleIntegrationId;
    }


    /**
     * Gets the vehicleVIN value for this CustomObject6Data.
     * 
     * @return vehicleVIN
     */
    public java.lang.String getVehicleVIN() {
        return vehicleVIN;
    }


    /**
     * Sets the vehicleVIN value for this CustomObject6Data.
     * 
     * @param vehicleVIN
     */
    public void setVehicleVIN(java.lang.String vehicleVIN) {
        this.vehicleVIN = vehicleVIN;
    }


    /**
     * Gets the vehicleExternalSystemId value for this CustomObject6Data.
     * 
     * @return vehicleExternalSystemId
     */
    public java.lang.String getVehicleExternalSystemId() {
        return vehicleExternalSystemId;
    }


    /**
     * Sets the vehicleExternalSystemId value for this CustomObject6Data.
     * 
     * @param vehicleExternalSystemId
     */
    public void setVehicleExternalSystemId(java.lang.String vehicleExternalSystemId) {
        this.vehicleExternalSystemId = vehicleExternalSystemId;
    }


    /**
     * Gets the ownerAlias value for this CustomObject6Data.
     * 
     * @return ownerAlias
     */
    public java.lang.String getOwnerAlias() {
        return ownerAlias;
    }


    /**
     * Sets the ownerAlias value for this CustomObject6Data.
     * 
     * @param ownerAlias
     */
    public void setOwnerAlias(java.lang.String ownerAlias) {
        this.ownerAlias = ownerAlias;
    }


    /**
     * Gets the ownerEMailAddr value for this CustomObject6Data.
     * 
     * @return ownerEMailAddr
     */
    public java.lang.String getOwnerEMailAddr() {
        return ownerEMailAddr;
    }


    /**
     * Sets the ownerEMailAddr value for this CustomObject6Data.
     * 
     * @param ownerEMailAddr
     */
    public void setOwnerEMailAddr(java.lang.String ownerEMailAddr) {
        this.ownerEMailAddr = ownerEMailAddr;
    }


    /**
     * Gets the ownerExternalSystemId value for this CustomObject6Data.
     * 
     * @return ownerExternalSystemId
     */
    public java.lang.String getOwnerExternalSystemId() {
        return ownerExternalSystemId;
    }


    /**
     * Sets the ownerExternalSystemId value for this CustomObject6Data.
     * 
     * @param ownerExternalSystemId
     */
    public void setOwnerExternalSystemId(java.lang.String ownerExternalSystemId) {
        this.ownerExternalSystemId = ownerExternalSystemId;
    }


    /**
     * Gets the ownerFirstName value for this CustomObject6Data.
     * 
     * @return ownerFirstName
     */
    public java.lang.String getOwnerFirstName() {
        return ownerFirstName;
    }


    /**
     * Sets the ownerFirstName value for this CustomObject6Data.
     * 
     * @param ownerFirstName
     */
    public void setOwnerFirstName(java.lang.String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }


    /**
     * Gets the ownerFullName value for this CustomObject6Data.
     * 
     * @return ownerFullName
     */
    public java.lang.String getOwnerFullName() {
        return ownerFullName;
    }


    /**
     * Sets the ownerFullName value for this CustomObject6Data.
     * 
     * @param ownerFullName
     */
    public void setOwnerFullName(java.lang.String ownerFullName) {
        this.ownerFullName = ownerFullName;
    }


    /**
     * Gets the ownerIntegrationId value for this CustomObject6Data.
     * 
     * @return ownerIntegrationId
     */
    public java.lang.String getOwnerIntegrationId() {
        return ownerIntegrationId;
    }


    /**
     * Sets the ownerIntegrationId value for this CustomObject6Data.
     * 
     * @param ownerIntegrationId
     */
    public void setOwnerIntegrationId(java.lang.String ownerIntegrationId) {
        this.ownerIntegrationId = ownerIntegrationId;
    }


    /**
     * Gets the ownerLastName value for this CustomObject6Data.
     * 
     * @return ownerLastName
     */
    public java.lang.String getOwnerLastName() {
        return ownerLastName;
    }


    /**
     * Sets the ownerLastName value for this CustomObject6Data.
     * 
     * @param ownerLastName
     */
    public void setOwnerLastName(java.lang.String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }


    /**
     * Gets the ownerUserSignInId value for this CustomObject6Data.
     * 
     * @return ownerUserSignInId
     */
    public java.lang.String getOwnerUserSignInId() {
        return ownerUserSignInId;
    }


    /**
     * Sets the ownerUserSignInId value for this CustomObject6Data.
     * 
     * @param ownerUserSignInId
     */
    public void setOwnerUserSignInId(java.lang.String ownerUserSignInId) {
        this.ownerUserSignInId = ownerUserSignInId;
    }


    /**
     * Gets the createdBy value for this CustomObject6Data.
     * 
     * @return createdBy
     */
    public java.lang.String getCreatedBy() {
        return createdBy;
    }


    /**
     * Sets the createdBy value for this CustomObject6Data.
     * 
     * @param createdBy
     */
    public void setCreatedBy(java.lang.String createdBy) {
        this.createdBy = createdBy;
    }


    /**
     * Gets the modifiedBy value for this CustomObject6Data.
     * 
     * @return modifiedBy
     */
    public java.lang.String getModifiedBy() {
        return modifiedBy;
    }


    /**
     * Sets the modifiedBy value for this CustomObject6Data.
     * 
     * @param modifiedBy
     */
    public void setModifiedBy(java.lang.String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    /**
     * Gets the description value for this CustomObject6Data.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this CustomObject6Data.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the operation value for this CustomObject6Data.
     * 
     * @return operation
     */
    public java.lang.String getOperation() {
        return operation;
    }


    /**
     * Sets the operation value for this CustomObject6Data.
     * 
     * @param operation
     */
    public void setOperation(java.lang.String operation) {
        this.operation = operation;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomObject6Data)) return false;
        CustomObject6Data other = (CustomObject6Data) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.modifiedDate==null && other.getModifiedDate()==null) || 
             (this.modifiedDate!=null &&
              this.modifiedDate.equals(other.getModifiedDate()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate()))) &&
            ((this.modifiedById==null && other.getModifiedById()==null) || 
             (this.modifiedById!=null &&
              this.modifiedById.equals(other.getModifiedById()))) &&
            ((this.createdById==null && other.getCreatedById()==null) || 
             (this.createdById!=null &&
              this.createdById.equals(other.getCreatedById()))) &&
            ((this.modId==null && other.getModId()==null) || 
             (this.modId!=null &&
              this.modId.equals(other.getModId()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.quickSearch1==null && other.getQuickSearch1()==null) || 
             (this.quickSearch1!=null &&
              this.quickSearch1.equals(other.getQuickSearch1()))) &&
            ((this.quickSearch2==null && other.getQuickSearch2()==null) || 
             (this.quickSearch2!=null &&
              this.quickSearch2.equals(other.getQuickSearch2()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.exchangeDate==null && other.getExchangeDate()==null) || 
             (this.exchangeDate!=null &&
              this.exchangeDate.equals(other.getExchangeDate()))) &&
            ((this.externalSystemId==null && other.getExternalSystemId()==null) || 
             (this.externalSystemId!=null &&
              this.externalSystemId.equals(other.getExternalSystemId()))) &&
            ((this.integrationId==null && other.getIntegrationId()==null) || 
             (this.integrationId!=null &&
              this.integrationId.equals(other.getIntegrationId()))) &&
            ((this.indexedBoolean0==null && other.getIndexedBoolean0()==null) || 
             (this.indexedBoolean0!=null &&
              this.indexedBoolean0.equals(other.getIndexedBoolean0()))) &&
            ((this.indexedCurrency0==null && other.getIndexedCurrency0()==null) || 
             (this.indexedCurrency0!=null &&
              this.indexedCurrency0.equals(other.getIndexedCurrency0()))) &&
            ((this.indexedDate0==null && other.getIndexedDate0()==null) || 
             (this.indexedDate0!=null &&
              this.indexedDate0.equals(other.getIndexedDate0()))) &&
            ((this.indexedNumber0==null && other.getIndexedNumber0()==null) || 
             (this.indexedNumber0!=null &&
              this.indexedNumber0.equals(other.getIndexedNumber0()))) &&
            ((this.indexedPick0==null && other.getIndexedPick0()==null) || 
             (this.indexedPick0!=null &&
              this.indexedPick0.equals(other.getIndexedPick0()))) &&
            ((this.indexedPick1==null && other.getIndexedPick1()==null) || 
             (this.indexedPick1!=null &&
              this.indexedPick1.equals(other.getIndexedPick1()))) &&
            ((this.indexedPick2==null && other.getIndexedPick2()==null) || 
             (this.indexedPick2!=null &&
              this.indexedPick2.equals(other.getIndexedPick2()))) &&
            ((this.indexedPick3==null && other.getIndexedPick3()==null) || 
             (this.indexedPick3!=null &&
              this.indexedPick3.equals(other.getIndexedPick3()))) &&
            ((this.indexedPick4==null && other.getIndexedPick4()==null) || 
             (this.indexedPick4!=null &&
              this.indexedPick4.equals(other.getIndexedPick4()))) &&
            ((this.accountId==null && other.getAccountId()==null) || 
             (this.accountId!=null &&
              this.accountId.equals(other.getAccountId()))) &&
            ((this.activityId==null && other.getActivityId()==null) || 
             (this.activityId!=null &&
              this.activityId.equals(other.getActivityId()))) &&
            ((this.assetId==null && other.getAssetId()==null) || 
             (this.assetId!=null &&
              this.assetId.equals(other.getAssetId()))) &&
            ((this.campaignId==null && other.getCampaignId()==null) || 
             (this.campaignId!=null &&
              this.campaignId.equals(other.getCampaignId()))) &&
            ((this.contactId==null && other.getContactId()==null) || 
             (this.contactId!=null &&
              this.contactId.equals(other.getContactId()))) &&
            ((this.customObject1Id==null && other.getCustomObject1Id()==null) || 
             (this.customObject1Id!=null &&
              this.customObject1Id.equals(other.getCustomObject1Id()))) &&
            ((this.customObject2Id==null && other.getCustomObject2Id()==null) || 
             (this.customObject2Id!=null &&
              this.customObject2Id.equals(other.getCustomObject2Id()))) &&
            ((this.customObject3Id==null && other.getCustomObject3Id()==null) || 
             (this.customObject3Id!=null &&
              this.customObject3Id.equals(other.getCustomObject3Id()))) &&
            ((this.customObject4Id==null && other.getCustomObject4Id()==null) || 
             (this.customObject4Id!=null &&
              this.customObject4Id.equals(other.getCustomObject4Id()))) &&
            ((this.customObject5Id==null && other.getCustomObject5Id()==null) || 
             (this.customObject5Id!=null &&
              this.customObject5Id.equals(other.getCustomObject5Id()))) &&
            ((this.customObject6Id==null && other.getCustomObject6Id()==null) || 
             (this.customObject6Id!=null &&
              this.customObject6Id.equals(other.getCustomObject6Id()))) &&
            ((this.customObject7Id==null && other.getCustomObject7Id()==null) || 
             (this.customObject7Id!=null &&
              this.customObject7Id.equals(other.getCustomObject7Id()))) &&
            ((this.customObject8Id==null && other.getCustomObject8Id()==null) || 
             (this.customObject8Id!=null &&
              this.customObject8Id.equals(other.getCustomObject8Id()))) &&
            ((this.customObject9Id==null && other.getCustomObject9Id()==null) || 
             (this.customObject9Id!=null &&
              this.customObject9Id.equals(other.getCustomObject9Id()))) &&
            ((this.customObject10Id==null && other.getCustomObject10Id()==null) || 
             (this.customObject10Id!=null &&
              this.customObject10Id.equals(other.getCustomObject10Id()))) &&
            ((this.customObject11Id==null && other.getCustomObject11Id()==null) || 
             (this.customObject11Id!=null &&
              this.customObject11Id.equals(other.getCustomObject11Id()))) &&
            ((this.customObject12Id==null && other.getCustomObject12Id()==null) || 
             (this.customObject12Id!=null &&
              this.customObject12Id.equals(other.getCustomObject12Id()))) &&
            ((this.customObject13Id==null && other.getCustomObject13Id()==null) || 
             (this.customObject13Id!=null &&
              this.customObject13Id.equals(other.getCustomObject13Id()))) &&
            ((this.customObject14Id==null && other.getCustomObject14Id()==null) || 
             (this.customObject14Id!=null &&
              this.customObject14Id.equals(other.getCustomObject14Id()))) &&
            ((this.customObject15Id==null && other.getCustomObject15Id()==null) || 
             (this.customObject15Id!=null &&
              this.customObject15Id.equals(other.getCustomObject15Id()))) &&
            ((this.dealerId==null && other.getDealerId()==null) || 
             (this.dealerId!=null &&
              this.dealerId.equals(other.getDealerId()))) &&
            ((this.fundId==null && other.getFundId()==null) || 
             (this.fundId!=null &&
              this.fundId.equals(other.getFundId()))) &&
            ((this.fundRequestId==null && other.getFundRequestId()==null) || 
             (this.fundRequestId!=null &&
              this.fundRequestId.equals(other.getFundRequestId()))) &&
            ((this.householdId==null && other.getHouseholdId()==null) || 
             (this.householdId!=null &&
              this.householdId.equals(other.getHouseholdId()))) &&
            ((this.leadId==null && other.getLeadId()==null) || 
             (this.leadId!=null &&
              this.leadId.equals(other.getLeadId()))) &&
            ((this.medEdId==null && other.getMedEdId()==null) || 
             (this.medEdId!=null &&
              this.medEdId.equals(other.getMedEdId()))) &&
            ((this.opportunityId==null && other.getOpportunityId()==null) || 
             (this.opportunityId!=null &&
              this.opportunityId.equals(other.getOpportunityId()))) &&
            ((this.portfolioId==null && other.getPortfolioId()==null) || 
             (this.portfolioId!=null &&
              this.portfolioId.equals(other.getPortfolioId()))) &&
            ((this.productId==null && other.getProductId()==null) || 
             (this.productId!=null &&
              this.productId.equals(other.getProductId()))) &&
            ((this.serviceRequestId==null && other.getServiceRequestId()==null) || 
             (this.serviceRequestId!=null &&
              this.serviceRequestId.equals(other.getServiceRequestId()))) &&
            ((this.solutionId==null && other.getSolutionId()==null) || 
             (this.solutionId!=null &&
              this.solutionId.equals(other.getSolutionId()))) &&
            ((this.vehicleId==null && other.getVehicleId()==null) || 
             (this.vehicleId!=null &&
              this.vehicleId.equals(other.getVehicleId()))) &&
            ((this.ownerId==null && other.getOwnerId()==null) || 
             (this.ownerId!=null &&
              this.ownerId.equals(other.getOwnerId()))) &&
            ((this.updatedByAlias==null && other.getUpdatedByAlias()==null) || 
             (this.updatedByAlias!=null &&
              this.updatedByAlias.equals(other.getUpdatedByAlias()))) &&
            ((this.updatedByEMailAddr==null && other.getUpdatedByEMailAddr()==null) || 
             (this.updatedByEMailAddr!=null &&
              this.updatedByEMailAddr.equals(other.getUpdatedByEMailAddr()))) &&
            ((this.updatedByExternalSystemId==null && other.getUpdatedByExternalSystemId()==null) || 
             (this.updatedByExternalSystemId!=null &&
              this.updatedByExternalSystemId.equals(other.getUpdatedByExternalSystemId()))) &&
            ((this.updatedByFirstName==null && other.getUpdatedByFirstName()==null) || 
             (this.updatedByFirstName!=null &&
              this.updatedByFirstName.equals(other.getUpdatedByFirstName()))) &&
            ((this.updatedByFullName==null && other.getUpdatedByFullName()==null) || 
             (this.updatedByFullName!=null &&
              this.updatedByFullName.equals(other.getUpdatedByFullName()))) &&
            ((this.updatedByIntegrationId==null && other.getUpdatedByIntegrationId()==null) || 
             (this.updatedByIntegrationId!=null &&
              this.updatedByIntegrationId.equals(other.getUpdatedByIntegrationId()))) &&
            ((this.updatedByLastName==null && other.getUpdatedByLastName()==null) || 
             (this.updatedByLastName!=null &&
              this.updatedByLastName.equals(other.getUpdatedByLastName()))) &&
            ((this.updatedByUserSignInId==null && other.getUpdatedByUserSignInId()==null) || 
             (this.updatedByUserSignInId!=null &&
              this.updatedByUserSignInId.equals(other.getUpdatedByUserSignInId()))) &&
            ((this.createdByAlias==null && other.getCreatedByAlias()==null) || 
             (this.createdByAlias!=null &&
              this.createdByAlias.equals(other.getCreatedByAlias()))) &&
            ((this.createdByEMailAddr==null && other.getCreatedByEMailAddr()==null) || 
             (this.createdByEMailAddr!=null &&
              this.createdByEMailAddr.equals(other.getCreatedByEMailAddr()))) &&
            ((this.createdByExternalSystemId==null && other.getCreatedByExternalSystemId()==null) || 
             (this.createdByExternalSystemId!=null &&
              this.createdByExternalSystemId.equals(other.getCreatedByExternalSystemId()))) &&
            ((this.createdByFirstName==null && other.getCreatedByFirstName()==null) || 
             (this.createdByFirstName!=null &&
              this.createdByFirstName.equals(other.getCreatedByFirstName()))) &&
            ((this.createdByFullName==null && other.getCreatedByFullName()==null) || 
             (this.createdByFullName!=null &&
              this.createdByFullName.equals(other.getCreatedByFullName()))) &&
            ((this.createdByIntegrationId==null && other.getCreatedByIntegrationId()==null) || 
             (this.createdByIntegrationId!=null &&
              this.createdByIntegrationId.equals(other.getCreatedByIntegrationId()))) &&
            ((this.createdByLastName==null && other.getCreatedByLastName()==null) || 
             (this.createdByLastName!=null &&
              this.createdByLastName.equals(other.getCreatedByLastName()))) &&
            ((this.createdByUserSignInId==null && other.getCreatedByUserSignInId()==null) || 
             (this.createdByUserSignInId!=null &&
              this.createdByUserSignInId.equals(other.getCreatedByUserSignInId()))) &&
            ((this.accountExternalSystemId==null && other.getAccountExternalSystemId()==null) || 
             (this.accountExternalSystemId!=null &&
              this.accountExternalSystemId.equals(other.getAccountExternalSystemId()))) &&
            ((this.accountIntegrationId==null && other.getAccountIntegrationId()==null) || 
             (this.accountIntegrationId!=null &&
              this.accountIntegrationId.equals(other.getAccountIntegrationId()))) &&
            ((this.accountLocation==null && other.getAccountLocation()==null) || 
             (this.accountLocation!=null &&
              this.accountLocation.equals(other.getAccountLocation()))) &&
            ((this.accountName==null && other.getAccountName()==null) || 
             (this.accountName!=null &&
              this.accountName.equals(other.getAccountName()))) &&
            ((this.activitySubject==null && other.getActivitySubject()==null) || 
             (this.activitySubject!=null &&
              this.activitySubject.equals(other.getActivitySubject()))) &&
            ((this.activityExternalSystemId==null && other.getActivityExternalSystemId()==null) || 
             (this.activityExternalSystemId!=null &&
              this.activityExternalSystemId.equals(other.getActivityExternalSystemId()))) &&
            ((this.activityIntegrationId==null && other.getActivityIntegrationId()==null) || 
             (this.activityIntegrationId!=null &&
              this.activityIntegrationId.equals(other.getActivityIntegrationId()))) &&
            ((this.assetIntegrationId==null && other.getAssetIntegrationId()==null) || 
             (this.assetIntegrationId!=null &&
              this.assetIntegrationId.equals(other.getAssetIntegrationId()))) &&
            ((this.assetProduct==null && other.getAssetProduct()==null) || 
             (this.assetProduct!=null &&
              this.assetProduct.equals(other.getAssetProduct()))) &&
            ((this.assetSerialNumber==null && other.getAssetSerialNumber()==null) || 
             (this.assetSerialNumber!=null &&
              this.assetSerialNumber.equals(other.getAssetSerialNumber()))) &&
            ((this.assetExternalSystemId==null && other.getAssetExternalSystemId()==null) || 
             (this.assetExternalSystemId!=null &&
              this.assetExternalSystemId.equals(other.getAssetExternalSystemId()))) &&
            ((this.campaignExternalSystemId==null && other.getCampaignExternalSystemId()==null) || 
             (this.campaignExternalSystemId!=null &&
              this.campaignExternalSystemId.equals(other.getCampaignExternalSystemId()))) &&
            ((this.campaignIntegrationId==null && other.getCampaignIntegrationId()==null) || 
             (this.campaignIntegrationId!=null &&
              this.campaignIntegrationId.equals(other.getCampaignIntegrationId()))) &&
            ((this.campaignName==null && other.getCampaignName()==null) || 
             (this.campaignName!=null &&
              this.campaignName.equals(other.getCampaignName()))) &&
            ((this.contactEmail==null && other.getContactEmail()==null) || 
             (this.contactEmail!=null &&
              this.contactEmail.equals(other.getContactEmail()))) &&
            ((this.contactExternalSystemId==null && other.getContactExternalSystemId()==null) || 
             (this.contactExternalSystemId!=null &&
              this.contactExternalSystemId.equals(other.getContactExternalSystemId()))) &&
            ((this.contactFirstName==null && other.getContactFirstName()==null) || 
             (this.contactFirstName!=null &&
              this.contactFirstName.equals(other.getContactFirstName()))) &&
            ((this.contactFullName==null && other.getContactFullName()==null) || 
             (this.contactFullName!=null &&
              this.contactFullName.equals(other.getContactFullName()))) &&
            ((this.contactIntegrationId==null && other.getContactIntegrationId()==null) || 
             (this.contactIntegrationId!=null &&
              this.contactIntegrationId.equals(other.getContactIntegrationId()))) &&
            ((this.contactLastName==null && other.getContactLastName()==null) || 
             (this.contactLastName!=null &&
              this.contactLastName.equals(other.getContactLastName()))) &&
            ((this.customObject1ExternalSystemId==null && other.getCustomObject1ExternalSystemId()==null) || 
             (this.customObject1ExternalSystemId!=null &&
              this.customObject1ExternalSystemId.equals(other.getCustomObject1ExternalSystemId()))) &&
            ((this.customObject1IntegrationId==null && other.getCustomObject1IntegrationId()==null) || 
             (this.customObject1IntegrationId!=null &&
              this.customObject1IntegrationId.equals(other.getCustomObject1IntegrationId()))) &&
            ((this.customObject1Name==null && other.getCustomObject1Name()==null) || 
             (this.customObject1Name!=null &&
              this.customObject1Name.equals(other.getCustomObject1Name()))) &&
            ((this.customObject2ExternalSystemId==null && other.getCustomObject2ExternalSystemId()==null) || 
             (this.customObject2ExternalSystemId!=null &&
              this.customObject2ExternalSystemId.equals(other.getCustomObject2ExternalSystemId()))) &&
            ((this.customObject2IntegrationId==null && other.getCustomObject2IntegrationId()==null) || 
             (this.customObject2IntegrationId!=null &&
              this.customObject2IntegrationId.equals(other.getCustomObject2IntegrationId()))) &&
            ((this.customObject2Name==null && other.getCustomObject2Name()==null) || 
             (this.customObject2Name!=null &&
              this.customObject2Name.equals(other.getCustomObject2Name()))) &&
            ((this.customObject3IntegrationId==null && other.getCustomObject3IntegrationId()==null) || 
             (this.customObject3IntegrationId!=null &&
              this.customObject3IntegrationId.equals(other.getCustomObject3IntegrationId()))) &&
            ((this.customObject3ExternalSystemId==null && other.getCustomObject3ExternalSystemId()==null) || 
             (this.customObject3ExternalSystemId!=null &&
              this.customObject3ExternalSystemId.equals(other.getCustomObject3ExternalSystemId()))) &&
            ((this.customObject3Name==null && other.getCustomObject3Name()==null) || 
             (this.customObject3Name!=null &&
              this.customObject3Name.equals(other.getCustomObject3Name()))) &&
            ((this.customObject4Name==null && other.getCustomObject4Name()==null) || 
             (this.customObject4Name!=null &&
              this.customObject4Name.equals(other.getCustomObject4Name()))) &&
            ((this.customObject4ExternalSystemId==null && other.getCustomObject4ExternalSystemId()==null) || 
             (this.customObject4ExternalSystemId!=null &&
              this.customObject4ExternalSystemId.equals(other.getCustomObject4ExternalSystemId()))) &&
            ((this.customObject4IntegrationId==null && other.getCustomObject4IntegrationId()==null) || 
             (this.customObject4IntegrationId!=null &&
              this.customObject4IntegrationId.equals(other.getCustomObject4IntegrationId()))) &&
            ((this.customObject5Name==null && other.getCustomObject5Name()==null) || 
             (this.customObject5Name!=null &&
              this.customObject5Name.equals(other.getCustomObject5Name()))) &&
            ((this.customObject5ExternalSystemId==null && other.getCustomObject5ExternalSystemId()==null) || 
             (this.customObject5ExternalSystemId!=null &&
              this.customObject5ExternalSystemId.equals(other.getCustomObject5ExternalSystemId()))) &&
            ((this.customObject5IntegrationId==null && other.getCustomObject5IntegrationId()==null) || 
             (this.customObject5IntegrationId!=null &&
              this.customObject5IntegrationId.equals(other.getCustomObject5IntegrationId()))) &&
            ((this.customObject6Name==null && other.getCustomObject6Name()==null) || 
             (this.customObject6Name!=null &&
              this.customObject6Name.equals(other.getCustomObject6Name()))) &&
            ((this.customObject6ExternalSystemId==null && other.getCustomObject6ExternalSystemId()==null) || 
             (this.customObject6ExternalSystemId!=null &&
              this.customObject6ExternalSystemId.equals(other.getCustomObject6ExternalSystemId()))) &&
            ((this.customObject6IntegrationId==null && other.getCustomObject6IntegrationId()==null) || 
             (this.customObject6IntegrationId!=null &&
              this.customObject6IntegrationId.equals(other.getCustomObject6IntegrationId()))) &&
            ((this.customObject7Name==null && other.getCustomObject7Name()==null) || 
             (this.customObject7Name!=null &&
              this.customObject7Name.equals(other.getCustomObject7Name()))) &&
            ((this.customObject7ExternalSystemId==null && other.getCustomObject7ExternalSystemId()==null) || 
             (this.customObject7ExternalSystemId!=null &&
              this.customObject7ExternalSystemId.equals(other.getCustomObject7ExternalSystemId()))) &&
            ((this.customObject7IntegrationId==null && other.getCustomObject7IntegrationId()==null) || 
             (this.customObject7IntegrationId!=null &&
              this.customObject7IntegrationId.equals(other.getCustomObject7IntegrationId()))) &&
            ((this.customObject8Name==null && other.getCustomObject8Name()==null) || 
             (this.customObject8Name!=null &&
              this.customObject8Name.equals(other.getCustomObject8Name()))) &&
            ((this.customObject8ExternalSystemId==null && other.getCustomObject8ExternalSystemId()==null) || 
             (this.customObject8ExternalSystemId!=null &&
              this.customObject8ExternalSystemId.equals(other.getCustomObject8ExternalSystemId()))) &&
            ((this.customObject8IntegrationId==null && other.getCustomObject8IntegrationId()==null) || 
             (this.customObject8IntegrationId!=null &&
              this.customObject8IntegrationId.equals(other.getCustomObject8IntegrationId()))) &&
            ((this.customObject9Name==null && other.getCustomObject9Name()==null) || 
             (this.customObject9Name!=null &&
              this.customObject9Name.equals(other.getCustomObject9Name()))) &&
            ((this.customObject9ExternalSystemId==null && other.getCustomObject9ExternalSystemId()==null) || 
             (this.customObject9ExternalSystemId!=null &&
              this.customObject9ExternalSystemId.equals(other.getCustomObject9ExternalSystemId()))) &&
            ((this.customObject9IntegrationId==null && other.getCustomObject9IntegrationId()==null) || 
             (this.customObject9IntegrationId!=null &&
              this.customObject9IntegrationId.equals(other.getCustomObject9IntegrationId()))) &&
            ((this.customObject10Name==null && other.getCustomObject10Name()==null) || 
             (this.customObject10Name!=null &&
              this.customObject10Name.equals(other.getCustomObject10Name()))) &&
            ((this.customObject10ExternalSystemId==null && other.getCustomObject10ExternalSystemId()==null) || 
             (this.customObject10ExternalSystemId!=null &&
              this.customObject10ExternalSystemId.equals(other.getCustomObject10ExternalSystemId()))) &&
            ((this.customObject10IntegrationId==null && other.getCustomObject10IntegrationId()==null) || 
             (this.customObject10IntegrationId!=null &&
              this.customObject10IntegrationId.equals(other.getCustomObject10IntegrationId()))) &&
            ((this.customObject11Name==null && other.getCustomObject11Name()==null) || 
             (this.customObject11Name!=null &&
              this.customObject11Name.equals(other.getCustomObject11Name()))) &&
            ((this.customObject11ExternalSystemId==null && other.getCustomObject11ExternalSystemId()==null) || 
             (this.customObject11ExternalSystemId!=null &&
              this.customObject11ExternalSystemId.equals(other.getCustomObject11ExternalSystemId()))) &&
            ((this.customObject11IntegrationId==null && other.getCustomObject11IntegrationId()==null) || 
             (this.customObject11IntegrationId!=null &&
              this.customObject11IntegrationId.equals(other.getCustomObject11IntegrationId()))) &&
            ((this.customObject12Name==null && other.getCustomObject12Name()==null) || 
             (this.customObject12Name!=null &&
              this.customObject12Name.equals(other.getCustomObject12Name()))) &&
            ((this.customObject12ExternalSystemId==null && other.getCustomObject12ExternalSystemId()==null) || 
             (this.customObject12ExternalSystemId!=null &&
              this.customObject12ExternalSystemId.equals(other.getCustomObject12ExternalSystemId()))) &&
            ((this.customObject12IntegrationId==null && other.getCustomObject12IntegrationId()==null) || 
             (this.customObject12IntegrationId!=null &&
              this.customObject12IntegrationId.equals(other.getCustomObject12IntegrationId()))) &&
            ((this.customObject13Name==null && other.getCustomObject13Name()==null) || 
             (this.customObject13Name!=null &&
              this.customObject13Name.equals(other.getCustomObject13Name()))) &&
            ((this.customObject13ExternalSystemId==null && other.getCustomObject13ExternalSystemId()==null) || 
             (this.customObject13ExternalSystemId!=null &&
              this.customObject13ExternalSystemId.equals(other.getCustomObject13ExternalSystemId()))) &&
            ((this.customObject13IntegrationId==null && other.getCustomObject13IntegrationId()==null) || 
             (this.customObject13IntegrationId!=null &&
              this.customObject13IntegrationId.equals(other.getCustomObject13IntegrationId()))) &&
            ((this.customObject14Name==null && other.getCustomObject14Name()==null) || 
             (this.customObject14Name!=null &&
              this.customObject14Name.equals(other.getCustomObject14Name()))) &&
            ((this.customObject14ExternalSystemId==null && other.getCustomObject14ExternalSystemId()==null) || 
             (this.customObject14ExternalSystemId!=null &&
              this.customObject14ExternalSystemId.equals(other.getCustomObject14ExternalSystemId()))) &&
            ((this.customObject14IntegrationId==null && other.getCustomObject14IntegrationId()==null) || 
             (this.customObject14IntegrationId!=null &&
              this.customObject14IntegrationId.equals(other.getCustomObject14IntegrationId()))) &&
            ((this.customObject15Name==null && other.getCustomObject15Name()==null) || 
             (this.customObject15Name!=null &&
              this.customObject15Name.equals(other.getCustomObject15Name()))) &&
            ((this.customObject15ExternalSystemId==null && other.getCustomObject15ExternalSystemId()==null) || 
             (this.customObject15ExternalSystemId!=null &&
              this.customObject15ExternalSystemId.equals(other.getCustomObject15ExternalSystemId()))) &&
            ((this.customObject15IntegrationId==null && other.getCustomObject15IntegrationId()==null) || 
             (this.customObject15IntegrationId!=null &&
              this.customObject15IntegrationId.equals(other.getCustomObject15IntegrationId()))) &&
            ((this.dealerName==null && other.getDealerName()==null) || 
             (this.dealerName!=null &&
              this.dealerName.equals(other.getDealerName()))) &&
            ((this.dealerExternalSystemId==null && other.getDealerExternalSystemId()==null) || 
             (this.dealerExternalSystemId!=null &&
              this.dealerExternalSystemId.equals(other.getDealerExternalSystemId()))) &&
            ((this.dealerIntegrationId==null && other.getDealerIntegrationId()==null) || 
             (this.dealerIntegrationId!=null &&
              this.dealerIntegrationId.equals(other.getDealerIntegrationId()))) &&
            ((this.fundName==null && other.getFundName()==null) || 
             (this.fundName!=null &&
              this.fundName.equals(other.getFundName()))) &&
            ((this.fundRequestName==null && other.getFundRequestName()==null) || 
             (this.fundRequestName!=null &&
              this.fundRequestName.equals(other.getFundRequestName()))) &&
            ((this.fundRequestExternalSystemId==null && other.getFundRequestExternalSystemId()==null) || 
             (this.fundRequestExternalSystemId!=null &&
              this.fundRequestExternalSystemId.equals(other.getFundRequestExternalSystemId()))) &&
            ((this.householdName==null && other.getHouseholdName()==null) || 
             (this.householdName!=null &&
              this.householdName.equals(other.getHouseholdName()))) &&
            ((this.householdIntegrationId==null && other.getHouseholdIntegrationId()==null) || 
             (this.householdIntegrationId!=null &&
              this.householdIntegrationId.equals(other.getHouseholdIntegrationId()))) &&
            ((this.householdExternalSystemId==null && other.getHouseholdExternalSystemId()==null) || 
             (this.householdExternalSystemId!=null &&
              this.householdExternalSystemId.equals(other.getHouseholdExternalSystemId()))) &&
            ((this.leadExternalSystemId==null && other.getLeadExternalSystemId()==null) || 
             (this.leadExternalSystemId!=null &&
              this.leadExternalSystemId.equals(other.getLeadExternalSystemId()))) &&
            ((this.leadFirstName==null && other.getLeadFirstName()==null) || 
             (this.leadFirstName!=null &&
              this.leadFirstName.equals(other.getLeadFirstName()))) &&
            ((this.leadFullName==null && other.getLeadFullName()==null) || 
             (this.leadFullName!=null &&
              this.leadFullName.equals(other.getLeadFullName()))) &&
            ((this.leadIntegrationId==null && other.getLeadIntegrationId()==null) || 
             (this.leadIntegrationId!=null &&
              this.leadIntegrationId.equals(other.getLeadIntegrationId()))) &&
            ((this.leadLastName==null && other.getLeadLastName()==null) || 
             (this.leadLastName!=null &&
              this.leadLastName.equals(other.getLeadLastName()))) &&
            ((this.medEdName==null && other.getMedEdName()==null) || 
             (this.medEdName!=null &&
              this.medEdName.equals(other.getMedEdName()))) &&
            ((this.medEdExternalSystemId==null && other.getMedEdExternalSystemId()==null) || 
             (this.medEdExternalSystemId!=null &&
              this.medEdExternalSystemId.equals(other.getMedEdExternalSystemId()))) &&
            ((this.medEdIntegrationId==null && other.getMedEdIntegrationId()==null) || 
             (this.medEdIntegrationId!=null &&
              this.medEdIntegrationId.equals(other.getMedEdIntegrationId()))) &&
            ((this.opportunityAccountId==null && other.getOpportunityAccountId()==null) || 
             (this.opportunityAccountId!=null &&
              this.opportunityAccountId.equals(other.getOpportunityAccountId()))) &&
            ((this.opportunityExternalSystemId==null && other.getOpportunityExternalSystemId()==null) || 
             (this.opportunityExternalSystemId!=null &&
              this.opportunityExternalSystemId.equals(other.getOpportunityExternalSystemId()))) &&
            ((this.opportunityIntegrationId==null && other.getOpportunityIntegrationId()==null) || 
             (this.opportunityIntegrationId!=null &&
              this.opportunityIntegrationId.equals(other.getOpportunityIntegrationId()))) &&
            ((this.opportunityName==null && other.getOpportunityName()==null) || 
             (this.opportunityName!=null &&
              this.opportunityName.equals(other.getOpportunityName()))) &&
            ((this.portfolioAccountNumber==null && other.getPortfolioAccountNumber()==null) || 
             (this.portfolioAccountNumber!=null &&
              this.portfolioAccountNumber.equals(other.getPortfolioAccountNumber()))) &&
            ((this.portfolioExternalSystemId==null && other.getPortfolioExternalSystemId()==null) || 
             (this.portfolioExternalSystemId!=null &&
              this.portfolioExternalSystemId.equals(other.getPortfolioExternalSystemId()))) &&
            ((this.portfolioIntegrationId==null && other.getPortfolioIntegrationId()==null) || 
             (this.portfolioIntegrationId!=null &&
              this.portfolioIntegrationId.equals(other.getPortfolioIntegrationId()))) &&
            ((this.productIntegrationId==null && other.getProductIntegrationId()==null) || 
             (this.productIntegrationId!=null &&
              this.productIntegrationId.equals(other.getProductIntegrationId()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.productExternalSystemId==null && other.getProductExternalSystemId()==null) || 
             (this.productExternalSystemId!=null &&
              this.productExternalSystemId.equals(other.getProductExternalSystemId()))) &&
            ((this.serviceRequestExternalSystemId==null && other.getServiceRequestExternalSystemId()==null) || 
             (this.serviceRequestExternalSystemId!=null &&
              this.serviceRequestExternalSystemId.equals(other.getServiceRequestExternalSystemId()))) &&
            ((this.serviceRequestIntegrationId==null && other.getServiceRequestIntegrationId()==null) || 
             (this.serviceRequestIntegrationId!=null &&
              this.serviceRequestIntegrationId.equals(other.getServiceRequestIntegrationId()))) &&
            ((this.serviceRequestSRNumber==null && other.getServiceRequestSRNumber()==null) || 
             (this.serviceRequestSRNumber!=null &&
              this.serviceRequestSRNumber.equals(other.getServiceRequestSRNumber()))) &&
            ((this.solutionExternalSystemId==null && other.getSolutionExternalSystemId()==null) || 
             (this.solutionExternalSystemId!=null &&
              this.solutionExternalSystemId.equals(other.getSolutionExternalSystemId()))) &&
            ((this.solutionIntegrationId==null && other.getSolutionIntegrationId()==null) || 
             (this.solutionIntegrationId!=null &&
              this.solutionIntegrationId.equals(other.getSolutionIntegrationId()))) &&
            ((this.solutionTitle==null && other.getSolutionTitle()==null) || 
             (this.solutionTitle!=null &&
              this.solutionTitle.equals(other.getSolutionTitle()))) &&
            ((this.vehicleIntegrationId==null && other.getVehicleIntegrationId()==null) || 
             (this.vehicleIntegrationId!=null &&
              this.vehicleIntegrationId.equals(other.getVehicleIntegrationId()))) &&
            ((this.vehicleVIN==null && other.getVehicleVIN()==null) || 
             (this.vehicleVIN!=null &&
              this.vehicleVIN.equals(other.getVehicleVIN()))) &&
            ((this.vehicleExternalSystemId==null && other.getVehicleExternalSystemId()==null) || 
             (this.vehicleExternalSystemId!=null &&
              this.vehicleExternalSystemId.equals(other.getVehicleExternalSystemId()))) &&
            ((this.ownerAlias==null && other.getOwnerAlias()==null) || 
             (this.ownerAlias!=null &&
              this.ownerAlias.equals(other.getOwnerAlias()))) &&
            ((this.ownerEMailAddr==null && other.getOwnerEMailAddr()==null) || 
             (this.ownerEMailAddr!=null &&
              this.ownerEMailAddr.equals(other.getOwnerEMailAddr()))) &&
            ((this.ownerExternalSystemId==null && other.getOwnerExternalSystemId()==null) || 
             (this.ownerExternalSystemId!=null &&
              this.ownerExternalSystemId.equals(other.getOwnerExternalSystemId()))) &&
            ((this.ownerFirstName==null && other.getOwnerFirstName()==null) || 
             (this.ownerFirstName!=null &&
              this.ownerFirstName.equals(other.getOwnerFirstName()))) &&
            ((this.ownerFullName==null && other.getOwnerFullName()==null) || 
             (this.ownerFullName!=null &&
              this.ownerFullName.equals(other.getOwnerFullName()))) &&
            ((this.ownerIntegrationId==null && other.getOwnerIntegrationId()==null) || 
             (this.ownerIntegrationId!=null &&
              this.ownerIntegrationId.equals(other.getOwnerIntegrationId()))) &&
            ((this.ownerLastName==null && other.getOwnerLastName()==null) || 
             (this.ownerLastName!=null &&
              this.ownerLastName.equals(other.getOwnerLastName()))) &&
            ((this.ownerUserSignInId==null && other.getOwnerUserSignInId()==null) || 
             (this.ownerUserSignInId!=null &&
              this.ownerUserSignInId.equals(other.getOwnerUserSignInId()))) &&
            ((this.createdBy==null && other.getCreatedBy()==null) || 
             (this.createdBy!=null &&
              this.createdBy.equals(other.getCreatedBy()))) &&
            ((this.modifiedBy==null && other.getModifiedBy()==null) || 
             (this.modifiedBy!=null &&
              this.modifiedBy.equals(other.getModifiedBy()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.operation==null && other.getOperation()==null) || 
             (this.operation!=null &&
              this.operation.equals(other.getOperation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getModifiedDate() != null) {
            _hashCode += getModifiedDate().hashCode();
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        if (getModifiedById() != null) {
            _hashCode += getModifiedById().hashCode();
        }
        if (getCreatedById() != null) {
            _hashCode += getCreatedById().hashCode();
        }
        if (getModId() != null) {
            _hashCode += getModId().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getQuickSearch1() != null) {
            _hashCode += getQuickSearch1().hashCode();
        }
        if (getQuickSearch2() != null) {
            _hashCode += getQuickSearch2().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getExchangeDate() != null) {
            _hashCode += getExchangeDate().hashCode();
        }
        if (getExternalSystemId() != null) {
            _hashCode += getExternalSystemId().hashCode();
        }
        if (getIntegrationId() != null) {
            _hashCode += getIntegrationId().hashCode();
        }
        if (getIndexedBoolean0() != null) {
            _hashCode += getIndexedBoolean0().hashCode();
        }
        if (getIndexedCurrency0() != null) {
            _hashCode += getIndexedCurrency0().hashCode();
        }
        if (getIndexedDate0() != null) {
            _hashCode += getIndexedDate0().hashCode();
        }
        if (getIndexedNumber0() != null) {
            _hashCode += getIndexedNumber0().hashCode();
        }
        if (getIndexedPick0() != null) {
            _hashCode += getIndexedPick0().hashCode();
        }
        if (getIndexedPick1() != null) {
            _hashCode += getIndexedPick1().hashCode();
        }
        if (getIndexedPick2() != null) {
            _hashCode += getIndexedPick2().hashCode();
        }
        if (getIndexedPick3() != null) {
            _hashCode += getIndexedPick3().hashCode();
        }
        if (getIndexedPick4() != null) {
            _hashCode += getIndexedPick4().hashCode();
        }
        if (getAccountId() != null) {
            _hashCode += getAccountId().hashCode();
        }
        if (getActivityId() != null) {
            _hashCode += getActivityId().hashCode();
        }
        if (getAssetId() != null) {
            _hashCode += getAssetId().hashCode();
        }
        if (getCampaignId() != null) {
            _hashCode += getCampaignId().hashCode();
        }
        if (getContactId() != null) {
            _hashCode += getContactId().hashCode();
        }
        if (getCustomObject1Id() != null) {
            _hashCode += getCustomObject1Id().hashCode();
        }
        if (getCustomObject2Id() != null) {
            _hashCode += getCustomObject2Id().hashCode();
        }
        if (getCustomObject3Id() != null) {
            _hashCode += getCustomObject3Id().hashCode();
        }
        if (getCustomObject4Id() != null) {
            _hashCode += getCustomObject4Id().hashCode();
        }
        if (getCustomObject5Id() != null) {
            _hashCode += getCustomObject5Id().hashCode();
        }
        if (getCustomObject6Id() != null) {
            _hashCode += getCustomObject6Id().hashCode();
        }
        if (getCustomObject7Id() != null) {
            _hashCode += getCustomObject7Id().hashCode();
        }
        if (getCustomObject8Id() != null) {
            _hashCode += getCustomObject8Id().hashCode();
        }
        if (getCustomObject9Id() != null) {
            _hashCode += getCustomObject9Id().hashCode();
        }
        if (getCustomObject10Id() != null) {
            _hashCode += getCustomObject10Id().hashCode();
        }
        if (getCustomObject11Id() != null) {
            _hashCode += getCustomObject11Id().hashCode();
        }
        if (getCustomObject12Id() != null) {
            _hashCode += getCustomObject12Id().hashCode();
        }
        if (getCustomObject13Id() != null) {
            _hashCode += getCustomObject13Id().hashCode();
        }
        if (getCustomObject14Id() != null) {
            _hashCode += getCustomObject14Id().hashCode();
        }
        if (getCustomObject15Id() != null) {
            _hashCode += getCustomObject15Id().hashCode();
        }
        if (getDealerId() != null) {
            _hashCode += getDealerId().hashCode();
        }
        if (getFundId() != null) {
            _hashCode += getFundId().hashCode();
        }
        if (getFundRequestId() != null) {
            _hashCode += getFundRequestId().hashCode();
        }
        if (getHouseholdId() != null) {
            _hashCode += getHouseholdId().hashCode();
        }
        if (getLeadId() != null) {
            _hashCode += getLeadId().hashCode();
        }
        if (getMedEdId() != null) {
            _hashCode += getMedEdId().hashCode();
        }
        if (getOpportunityId() != null) {
            _hashCode += getOpportunityId().hashCode();
        }
        if (getPortfolioId() != null) {
            _hashCode += getPortfolioId().hashCode();
        }
        if (getProductId() != null) {
            _hashCode += getProductId().hashCode();
        }
        if (getServiceRequestId() != null) {
            _hashCode += getServiceRequestId().hashCode();
        }
        if (getSolutionId() != null) {
            _hashCode += getSolutionId().hashCode();
        }
        if (getVehicleId() != null) {
            _hashCode += getVehicleId().hashCode();
        }
        if (getOwnerId() != null) {
            _hashCode += getOwnerId().hashCode();
        }
        if (getUpdatedByAlias() != null) {
            _hashCode += getUpdatedByAlias().hashCode();
        }
        if (getUpdatedByEMailAddr() != null) {
            _hashCode += getUpdatedByEMailAddr().hashCode();
        }
        if (getUpdatedByExternalSystemId() != null) {
            _hashCode += getUpdatedByExternalSystemId().hashCode();
        }
        if (getUpdatedByFirstName() != null) {
            _hashCode += getUpdatedByFirstName().hashCode();
        }
        if (getUpdatedByFullName() != null) {
            _hashCode += getUpdatedByFullName().hashCode();
        }
        if (getUpdatedByIntegrationId() != null) {
            _hashCode += getUpdatedByIntegrationId().hashCode();
        }
        if (getUpdatedByLastName() != null) {
            _hashCode += getUpdatedByLastName().hashCode();
        }
        if (getUpdatedByUserSignInId() != null) {
            _hashCode += getUpdatedByUserSignInId().hashCode();
        }
        if (getCreatedByAlias() != null) {
            _hashCode += getCreatedByAlias().hashCode();
        }
        if (getCreatedByEMailAddr() != null) {
            _hashCode += getCreatedByEMailAddr().hashCode();
        }
        if (getCreatedByExternalSystemId() != null) {
            _hashCode += getCreatedByExternalSystemId().hashCode();
        }
        if (getCreatedByFirstName() != null) {
            _hashCode += getCreatedByFirstName().hashCode();
        }
        if (getCreatedByFullName() != null) {
            _hashCode += getCreatedByFullName().hashCode();
        }
        if (getCreatedByIntegrationId() != null) {
            _hashCode += getCreatedByIntegrationId().hashCode();
        }
        if (getCreatedByLastName() != null) {
            _hashCode += getCreatedByLastName().hashCode();
        }
        if (getCreatedByUserSignInId() != null) {
            _hashCode += getCreatedByUserSignInId().hashCode();
        }
        if (getAccountExternalSystemId() != null) {
            _hashCode += getAccountExternalSystemId().hashCode();
        }
        if (getAccountIntegrationId() != null) {
            _hashCode += getAccountIntegrationId().hashCode();
        }
        if (getAccountLocation() != null) {
            _hashCode += getAccountLocation().hashCode();
        }
        if (getAccountName() != null) {
            _hashCode += getAccountName().hashCode();
        }
        if (getActivitySubject() != null) {
            _hashCode += getActivitySubject().hashCode();
        }
        if (getActivityExternalSystemId() != null) {
            _hashCode += getActivityExternalSystemId().hashCode();
        }
        if (getActivityIntegrationId() != null) {
            _hashCode += getActivityIntegrationId().hashCode();
        }
        if (getAssetIntegrationId() != null) {
            _hashCode += getAssetIntegrationId().hashCode();
        }
        if (getAssetProduct() != null) {
            _hashCode += getAssetProduct().hashCode();
        }
        if (getAssetSerialNumber() != null) {
            _hashCode += getAssetSerialNumber().hashCode();
        }
        if (getAssetExternalSystemId() != null) {
            _hashCode += getAssetExternalSystemId().hashCode();
        }
        if (getCampaignExternalSystemId() != null) {
            _hashCode += getCampaignExternalSystemId().hashCode();
        }
        if (getCampaignIntegrationId() != null) {
            _hashCode += getCampaignIntegrationId().hashCode();
        }
        if (getCampaignName() != null) {
            _hashCode += getCampaignName().hashCode();
        }
        if (getContactEmail() != null) {
            _hashCode += getContactEmail().hashCode();
        }
        if (getContactExternalSystemId() != null) {
            _hashCode += getContactExternalSystemId().hashCode();
        }
        if (getContactFirstName() != null) {
            _hashCode += getContactFirstName().hashCode();
        }
        if (getContactFullName() != null) {
            _hashCode += getContactFullName().hashCode();
        }
        if (getContactIntegrationId() != null) {
            _hashCode += getContactIntegrationId().hashCode();
        }
        if (getContactLastName() != null) {
            _hashCode += getContactLastName().hashCode();
        }
        if (getCustomObject1ExternalSystemId() != null) {
            _hashCode += getCustomObject1ExternalSystemId().hashCode();
        }
        if (getCustomObject1IntegrationId() != null) {
            _hashCode += getCustomObject1IntegrationId().hashCode();
        }
        if (getCustomObject1Name() != null) {
            _hashCode += getCustomObject1Name().hashCode();
        }
        if (getCustomObject2ExternalSystemId() != null) {
            _hashCode += getCustomObject2ExternalSystemId().hashCode();
        }
        if (getCustomObject2IntegrationId() != null) {
            _hashCode += getCustomObject2IntegrationId().hashCode();
        }
        if (getCustomObject2Name() != null) {
            _hashCode += getCustomObject2Name().hashCode();
        }
        if (getCustomObject3IntegrationId() != null) {
            _hashCode += getCustomObject3IntegrationId().hashCode();
        }
        if (getCustomObject3ExternalSystemId() != null) {
            _hashCode += getCustomObject3ExternalSystemId().hashCode();
        }
        if (getCustomObject3Name() != null) {
            _hashCode += getCustomObject3Name().hashCode();
        }
        if (getCustomObject4Name() != null) {
            _hashCode += getCustomObject4Name().hashCode();
        }
        if (getCustomObject4ExternalSystemId() != null) {
            _hashCode += getCustomObject4ExternalSystemId().hashCode();
        }
        if (getCustomObject4IntegrationId() != null) {
            _hashCode += getCustomObject4IntegrationId().hashCode();
        }
        if (getCustomObject5Name() != null) {
            _hashCode += getCustomObject5Name().hashCode();
        }
        if (getCustomObject5ExternalSystemId() != null) {
            _hashCode += getCustomObject5ExternalSystemId().hashCode();
        }
        if (getCustomObject5IntegrationId() != null) {
            _hashCode += getCustomObject5IntegrationId().hashCode();
        }
        if (getCustomObject6Name() != null) {
            _hashCode += getCustomObject6Name().hashCode();
        }
        if (getCustomObject6ExternalSystemId() != null) {
            _hashCode += getCustomObject6ExternalSystemId().hashCode();
        }
        if (getCustomObject6IntegrationId() != null) {
            _hashCode += getCustomObject6IntegrationId().hashCode();
        }
        if (getCustomObject7Name() != null) {
            _hashCode += getCustomObject7Name().hashCode();
        }
        if (getCustomObject7ExternalSystemId() != null) {
            _hashCode += getCustomObject7ExternalSystemId().hashCode();
        }
        if (getCustomObject7IntegrationId() != null) {
            _hashCode += getCustomObject7IntegrationId().hashCode();
        }
        if (getCustomObject8Name() != null) {
            _hashCode += getCustomObject8Name().hashCode();
        }
        if (getCustomObject8ExternalSystemId() != null) {
            _hashCode += getCustomObject8ExternalSystemId().hashCode();
        }
        if (getCustomObject8IntegrationId() != null) {
            _hashCode += getCustomObject8IntegrationId().hashCode();
        }
        if (getCustomObject9Name() != null) {
            _hashCode += getCustomObject9Name().hashCode();
        }
        if (getCustomObject9ExternalSystemId() != null) {
            _hashCode += getCustomObject9ExternalSystemId().hashCode();
        }
        if (getCustomObject9IntegrationId() != null) {
            _hashCode += getCustomObject9IntegrationId().hashCode();
        }
        if (getCustomObject10Name() != null) {
            _hashCode += getCustomObject10Name().hashCode();
        }
        if (getCustomObject10ExternalSystemId() != null) {
            _hashCode += getCustomObject10ExternalSystemId().hashCode();
        }
        if (getCustomObject10IntegrationId() != null) {
            _hashCode += getCustomObject10IntegrationId().hashCode();
        }
        if (getCustomObject11Name() != null) {
            _hashCode += getCustomObject11Name().hashCode();
        }
        if (getCustomObject11ExternalSystemId() != null) {
            _hashCode += getCustomObject11ExternalSystemId().hashCode();
        }
        if (getCustomObject11IntegrationId() != null) {
            _hashCode += getCustomObject11IntegrationId().hashCode();
        }
        if (getCustomObject12Name() != null) {
            _hashCode += getCustomObject12Name().hashCode();
        }
        if (getCustomObject12ExternalSystemId() != null) {
            _hashCode += getCustomObject12ExternalSystemId().hashCode();
        }
        if (getCustomObject12IntegrationId() != null) {
            _hashCode += getCustomObject12IntegrationId().hashCode();
        }
        if (getCustomObject13Name() != null) {
            _hashCode += getCustomObject13Name().hashCode();
        }
        if (getCustomObject13ExternalSystemId() != null) {
            _hashCode += getCustomObject13ExternalSystemId().hashCode();
        }
        if (getCustomObject13IntegrationId() != null) {
            _hashCode += getCustomObject13IntegrationId().hashCode();
        }
        if (getCustomObject14Name() != null) {
            _hashCode += getCustomObject14Name().hashCode();
        }
        if (getCustomObject14ExternalSystemId() != null) {
            _hashCode += getCustomObject14ExternalSystemId().hashCode();
        }
        if (getCustomObject14IntegrationId() != null) {
            _hashCode += getCustomObject14IntegrationId().hashCode();
        }
        if (getCustomObject15Name() != null) {
            _hashCode += getCustomObject15Name().hashCode();
        }
        if (getCustomObject15ExternalSystemId() != null) {
            _hashCode += getCustomObject15ExternalSystemId().hashCode();
        }
        if (getCustomObject15IntegrationId() != null) {
            _hashCode += getCustomObject15IntegrationId().hashCode();
        }
        if (getDealerName() != null) {
            _hashCode += getDealerName().hashCode();
        }
        if (getDealerExternalSystemId() != null) {
            _hashCode += getDealerExternalSystemId().hashCode();
        }
        if (getDealerIntegrationId() != null) {
            _hashCode += getDealerIntegrationId().hashCode();
        }
        if (getFundName() != null) {
            _hashCode += getFundName().hashCode();
        }
        if (getFundRequestName() != null) {
            _hashCode += getFundRequestName().hashCode();
        }
        if (getFundRequestExternalSystemId() != null) {
            _hashCode += getFundRequestExternalSystemId().hashCode();
        }
        if (getHouseholdName() != null) {
            _hashCode += getHouseholdName().hashCode();
        }
        if (getHouseholdIntegrationId() != null) {
            _hashCode += getHouseholdIntegrationId().hashCode();
        }
        if (getHouseholdExternalSystemId() != null) {
            _hashCode += getHouseholdExternalSystemId().hashCode();
        }
        if (getLeadExternalSystemId() != null) {
            _hashCode += getLeadExternalSystemId().hashCode();
        }
        if (getLeadFirstName() != null) {
            _hashCode += getLeadFirstName().hashCode();
        }
        if (getLeadFullName() != null) {
            _hashCode += getLeadFullName().hashCode();
        }
        if (getLeadIntegrationId() != null) {
            _hashCode += getLeadIntegrationId().hashCode();
        }
        if (getLeadLastName() != null) {
            _hashCode += getLeadLastName().hashCode();
        }
        if (getMedEdName() != null) {
            _hashCode += getMedEdName().hashCode();
        }
        if (getMedEdExternalSystemId() != null) {
            _hashCode += getMedEdExternalSystemId().hashCode();
        }
        if (getMedEdIntegrationId() != null) {
            _hashCode += getMedEdIntegrationId().hashCode();
        }
        if (getOpportunityAccountId() != null) {
            _hashCode += getOpportunityAccountId().hashCode();
        }
        if (getOpportunityExternalSystemId() != null) {
            _hashCode += getOpportunityExternalSystemId().hashCode();
        }
        if (getOpportunityIntegrationId() != null) {
            _hashCode += getOpportunityIntegrationId().hashCode();
        }
        if (getOpportunityName() != null) {
            _hashCode += getOpportunityName().hashCode();
        }
        if (getPortfolioAccountNumber() != null) {
            _hashCode += getPortfolioAccountNumber().hashCode();
        }
        if (getPortfolioExternalSystemId() != null) {
            _hashCode += getPortfolioExternalSystemId().hashCode();
        }
        if (getPortfolioIntegrationId() != null) {
            _hashCode += getPortfolioIntegrationId().hashCode();
        }
        if (getProductIntegrationId() != null) {
            _hashCode += getProductIntegrationId().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getProductExternalSystemId() != null) {
            _hashCode += getProductExternalSystemId().hashCode();
        }
        if (getServiceRequestExternalSystemId() != null) {
            _hashCode += getServiceRequestExternalSystemId().hashCode();
        }
        if (getServiceRequestIntegrationId() != null) {
            _hashCode += getServiceRequestIntegrationId().hashCode();
        }
        if (getServiceRequestSRNumber() != null) {
            _hashCode += getServiceRequestSRNumber().hashCode();
        }
        if (getSolutionExternalSystemId() != null) {
            _hashCode += getSolutionExternalSystemId().hashCode();
        }
        if (getSolutionIntegrationId() != null) {
            _hashCode += getSolutionIntegrationId().hashCode();
        }
        if (getSolutionTitle() != null) {
            _hashCode += getSolutionTitle().hashCode();
        }
        if (getVehicleIntegrationId() != null) {
            _hashCode += getVehicleIntegrationId().hashCode();
        }
        if (getVehicleVIN() != null) {
            _hashCode += getVehicleVIN().hashCode();
        }
        if (getVehicleExternalSystemId() != null) {
            _hashCode += getVehicleExternalSystemId().hashCode();
        }
        if (getOwnerAlias() != null) {
            _hashCode += getOwnerAlias().hashCode();
        }
        if (getOwnerEMailAddr() != null) {
            _hashCode += getOwnerEMailAddr().hashCode();
        }
        if (getOwnerExternalSystemId() != null) {
            _hashCode += getOwnerExternalSystemId().hashCode();
        }
        if (getOwnerFirstName() != null) {
            _hashCode += getOwnerFirstName().hashCode();
        }
        if (getOwnerFullName() != null) {
            _hashCode += getOwnerFullName().hashCode();
        }
        if (getOwnerIntegrationId() != null) {
            _hashCode += getOwnerIntegrationId().hashCode();
        }
        if (getOwnerLastName() != null) {
            _hashCode += getOwnerLastName().hashCode();
        }
        if (getOwnerUserSignInId() != null) {
            _hashCode += getOwnerUserSignInId().hashCode();
        }
        if (getCreatedBy() != null) {
            _hashCode += getCreatedBy().hashCode();
        }
        if (getModifiedBy() != null) {
            _hashCode += getModifiedBy().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getOperation() != null) {
            _hashCode += getOperation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomObject6Data.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject6Data"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("operation");
        attrField.setXmlName(new javax.xml.namespace.QName("", "operation"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ModifiedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedById");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ModifiedById"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdById");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedById"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ModId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quickSearch1");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "QuickSearch1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quickSearch2");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "QuickSearch2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exchangeDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ExchangeDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("integrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedBoolean0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IndexedBoolean0"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedCurrency0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IndexedCurrency0"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedDate0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IndexedDate0"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedNumber0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IndexedNumber0"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick0");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IndexedPick0"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick1");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IndexedPick1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick2");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IndexedPick2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick3");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IndexedPick3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indexedPick4");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "IndexedPick4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activityId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ActivityId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AssetId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campaignId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CampaignId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ContactId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject1Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject1Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject2Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject2Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject3Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject3Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject4Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject4Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject5Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject5Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject6Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject6Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject7Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject7Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject8Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject8Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject9Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject9Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject10Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject10Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject11Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject11Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject12Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject12Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject13Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject13Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject14Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject14Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject15Id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject15Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "DealerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "FundId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundRequestId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "FundRequestId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("householdId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "HouseholdId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "LeadId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medEdId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "MedEdId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OpportunityId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portfolioId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "PortfolioId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceRequestId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ServiceRequestId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solutionId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "SolutionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "VehicleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OwnerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByAlias");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "UpdatedByAlias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByEMailAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "UpdatedByEMailAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "UpdatedByExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "UpdatedByFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "UpdatedByFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "UpdatedByIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "UpdatedByLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedByUserSignInId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "UpdatedByUserSignInId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByAlias");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedByAlias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByEMailAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedByEMailAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedByExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedByFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedByFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedByIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedByLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdByUserSignInId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedByUserSignInId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AccountExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AccountIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AccountLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AccountName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activitySubject");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ActivitySubject"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activityExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ActivityExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activityIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ActivityIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AssetIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetProduct");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AssetProduct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetSerialNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AssetSerialNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "AssetExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campaignExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CampaignExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campaignIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CampaignIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campaignName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CampaignName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ContactEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ContactExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ContactFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ContactFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ContactIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ContactLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject1ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject1ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject1IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject1IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject1Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject1Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject2ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject2ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject2IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject2IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject2Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject2Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject3IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject3IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject3ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject3ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject3Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject3Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject4Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject4Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject4ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject4ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject4IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject4IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject5Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject5Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject5ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject5ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject5IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject5IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject6Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject6Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject6ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject6ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject6IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject6IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject7Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject7Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject7ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject7ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject7IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject7IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject8Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject8Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject8ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject8ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject8IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject8IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject9Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject9Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject9ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject9ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject9IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject9IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject10Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject10Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject10ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject10ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject10IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject10IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject11Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject11Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject11ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject11ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject11IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject11IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject12Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject12Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject12ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject12ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject12IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject12IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject13Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject13Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject13ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject13ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject13IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject13IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject14Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject14Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject14ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject14ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject14IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject14IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject15Name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject15Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject15ExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject15ExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject15IntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject15IntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "DealerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "DealerExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dealerIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "DealerIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "FundName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundRequestName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "FundRequestName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundRequestExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "FundRequestExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("householdName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "HouseholdName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("householdIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "HouseholdIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("householdExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "HouseholdExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "LeadExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "LeadFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "LeadFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "LeadIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "LeadLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medEdName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "MedEdName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medEdExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "MedEdExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medEdIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "MedEdIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityAccountId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OpportunityAccountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OpportunityExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OpportunityIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opportunityName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OpportunityName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portfolioAccountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "PortfolioAccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portfolioExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "PortfolioExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portfolioIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "PortfolioIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ProductIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ProductName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ProductExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceRequestExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ServiceRequestExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceRequestIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ServiceRequestIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceRequestSRNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ServiceRequestSRNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solutionExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "SolutionExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solutionIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "SolutionIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solutionTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "SolutionTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "VehicleIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleVIN");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "VehicleVIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "VehicleExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerAlias");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OwnerAlias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerEMailAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OwnerEMailAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerExternalSystemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OwnerExternalSystemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OwnerFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerFullName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OwnerFullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OwnerIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OwnerLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerUserSignInId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "OwnerUserSignInId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdBy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CreatedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ModifiedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
