/**
 * CustomObject6_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cydcor.ws.stubs.privillege;

public class CustomObject6_ServiceLocator extends org.apache.axis.client.Service implements com.cydcor.ws.stubs.privillege.CustomObject6_Service {

    public CustomObject6_ServiceLocator() {
    }


    public CustomObject6_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CustomObject6_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CustomObject6
    private java.lang.String CustomObject6_address = "https://secure-ausomxjpa.crmondemand.com/Services/Integration";

    public java.lang.String getCustomObject6Address() {
        return CustomObject6_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CustomObject6WSDDServiceName = "CustomObject6";

    public java.lang.String getCustomObject6WSDDServiceName() {
        return CustomObject6WSDDServiceName;
    }

    public void setCustomObject6WSDDServiceName(java.lang.String name) {
        CustomObject6WSDDServiceName = name;
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6_PortType getCustomObject6() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CustomObject6_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCustomObject6(endpoint);
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6_PortType getCustomObject6(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.cydcor.ws.stubs.privillege.CustomObject6_BindingStub _stub = new com.cydcor.ws.stubs.privillege.CustomObject6_BindingStub(portAddress, this);
            _stub.setPortName(getCustomObject6WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCustomObject6EndpointAddress(java.lang.String address) {
        CustomObject6_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.cydcor.ws.stubs.privillege.CustomObject6_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.cydcor.ws.stubs.privillege.CustomObject6_BindingStub _stub = new com.cydcor.ws.stubs.privillege.CustomObject6_BindingStub(new java.net.URL(CustomObject6_address), this);
                _stub.setPortName(getCustomObject6WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CustomObject6".equals(inputPortName)) {
            return getCustomObject6();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CustomObject6".equals(portName)) {
            setCustomObject6EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
