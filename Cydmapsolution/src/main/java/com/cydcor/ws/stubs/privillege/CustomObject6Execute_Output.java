/**
 * CustomObject6Execute_Output.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cydcor.ws.stubs.privillege;

public class CustomObject6Execute_Output  implements java.io.Serializable {
    private CustomObject6Data[] listOfCustomObject6;

    public CustomObject6Execute_Output() {
    }

    public CustomObject6Execute_Output(
           CustomObject6Data[] listOfCustomObject6) {
           this.listOfCustomObject6 = listOfCustomObject6;
    }


    /**
     * Gets the listOfCustomObject6 value for this CustomObject6Execute_Output.
     * 
     * @return listOfCustomObject6
     */
    public CustomObject6Data[] getListOfCustomObject6() {
        return listOfCustomObject6;
    }


    /**
     * Sets the listOfCustomObject6 value for this CustomObject6Execute_Output.
     * 
     * @param listOfCustomObject6
     */
    public void setListOfCustomObject6(CustomObject6Data[] listOfCustomObject6) {
        this.listOfCustomObject6 = listOfCustomObject6;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomObject6Execute_Output)) return false;
        CustomObject6Execute_Output other = (CustomObject6Execute_Output) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.listOfCustomObject6==null && other.getListOfCustomObject6()==null) || 
             (this.listOfCustomObject6!=null &&
              java.util.Arrays.equals(this.listOfCustomObject6, other.getListOfCustomObject6())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getListOfCustomObject6() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListOfCustomObject6());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListOfCustomObject6(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomObject6Execute_Output.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Execute_Output"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listOfCustomObject6");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ListOfCustomObject6"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ListOfCustomObject6Data"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
