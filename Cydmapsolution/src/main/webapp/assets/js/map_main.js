/**
 * Main Map Script
 */

var drawnPolygonsArray = [];
var lastSelectedPolygon =null;

$(document).ready(function() {
});

/**
 * To create Maps Controls
 * 
 * @return
 */
function createMapControls() {
	drawingManager = new google.maps.drawing.DrawingManager({
		drawingControl : true,
		drawingControlOptions : {
			position : google.maps.ControlPosition.TOP_LEFT,
			drawingModes : [ google.maps.drawing.OverlayType.POLYGON ]
		},
		polygonOptions : {
			fillColor : '#0000FF',
			strokeColor : '#0000FF',
			strokeWeight : 0,
			fillOpacity : 0.45,
			zIndex : 1,
			editable : true,
			title : "CUSTOM"
		}
	});
	google.maps.event.addListener(drawingManager, 'overlaycomplete',
			function(e) {
				if (e.type != google.maps.drawing.OverlayType.MARKER) {
					var newShape = e.overlay;					
					newShape.type = e.type;
					 newShape.setEditable(false);
					 if(lastSelectedPolygon!=null) lastSelectedPolygon.setEditable(false);
					google.maps.event.addListener(newShape, 'click',
							function(e) {  if(lastSelectedPolygon!=null) lastSelectedPolygon.setEditable(false); newShape.setEditable(true);lastSelectedPolygon= newShape; });
					drawnPolygonsArray.push(newShape);
//					2013147 - Add borders on polygons
					drawnPolygons.push(newShape);
					updatePoints(newShape);
					google.maps.event.addListener(newShape.getPath(), 'set_at',
							function(newShape) {
								updateAllPolygonsLeads();
							});
					google.maps.event.addListener(newShape.getPath(),
							'remove_at', function() {
								updateAllPolygonsLeads();
							});
					google.maps.event.addListener(newShape.getPath(),
							'insert_at', function(newShape) {
								updateAllPolygonsLeads();
							});

				}
			});

}

/**
 * To add Controls to Map
 * 
 * @return
 */
function addControls() {
	if (drawingManager == undefined)
		createMapControls();
	if (enableDrawing)
		drawingManager.setMap(map);
	markerManager = new MarkerManager(map);
}

/**
 * This method is called when ever a user comes to system for the first time
 * 
 * @return
 */
function loadMap() {
	showMapDirections == "false";
	//alert('in loadMap');
	//alert(mapLoaded);
	if (mapLoaded == "false" && mapRequired == "true") {
		//alert('initializing new map');
		var myOptions = {
			zoom : 12,
			center : new google.maps.LatLng(37.88, -122.442626),
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("map_canvas"),
				myOptions);
		logMap();
		//alert('new map initialized');
		// map.setUIToDefault();
		// map.addControl(new GLargeMapControl());
		// map.addControl(new GHierarchicalMapTypeControl());
		// map.addMapType(G_PHYSICAL_MAP);
		// map.setMapTypeId(google.maps.MapTypeId.SATELLITE );
		// map.enableScrollWheelZoom();
		mapLoaded = "true";

	}
}

google.maps.event.addDomListener(window, "unload", function() {
	// GUnload();
});

/**
 * 
 * @param textBox
 * @return
 */
function getCounty(textBox) {
	var countyName = textBox.value;

	geometryControls.loadData({
		type : "kml",
		url : "mapServlet?action=load&cName=" + countyName + "&clientKey="
				+ clientKey
	});
}

/**
 * Loads KML response from defined URL and Displays in map
 * 
 * @param url
 * @return
 */
function loadKML(url, postData) {

	loadControl(url, {}, handleKmlDataResponse);
}

function markersLoaded(data) {
	var markers = eval('(' + data + ')').kml.document.placemark;
	loadMarkers(markers);
}

/**
 * Clears Markers from the map
 * 
 * @return
 */
function clearMarkers() {
	try {
		if (markerManager)
			markerManager.clearMarkers();
	} catch (e) {
	}
	try {
		if (markerCluster && markerCluster != null) {
			markerCluster.clearMarkers();
			markersForCluster = [];
		}
	} catch (e) {
	}

}

/**
 * Clear Polygons from the Map
 * 
 * @return
 */
function clearPolygons() {
	try {
		if (drawnPolygonsArray) {
			for ( var i = 0; i < drawnPolygonsArray.length; i++) {
				var d = drawnPolygonsArray[i];
				d.setMap(null);
			}
			drawnPolygonsArray = [];
		}
		if (loadedPolygonsArray) {
			for ( var i = 0; i < loadedPolygonsArray.length; i++) {
				var p = loadedPolygonsArray[i];
				p.setMap(null);
			}
			loadedPolygonsArray = [];
		}
//		2013147 - Add borders on polygons
		if(drawnPolygons){
			for ( var i = 0; i < drawnPolygons.length; i++) {
				var d = drawnPolygons[i];
				d.setMap(null);
			}
			drawnPolygons = [];
		}

		PolygonLeadsArray = [];
        while(polygonLabelMarkers.length) {
        	polygonLabelMarkers.pop().setMap(null);
        }
	} catch (e) {
	}
}

/**
 * Clear Polylines from the Map
 * 
 * @return
 */
function clearPolylines() {
	try {
		if (polylineControl)
			polylineControl.clearPolylines();
	} catch (e) {
	}
}

function clearMap() {
	try {
		clearPolygons();
		// clearPolylines();
		clearMarkers();
	} catch (e) {
	}
}

function clearMarkersCluster() {
	try {
		if (markerCluster && markerCluster != null) {
			markerCluster.clearMarkers();
			markersForCluster = [];
		}
	} catch (e) {
	}

}
function clearMapWithClusterMarkers() {
	try {
		clearPolygons();
		// clearPolylines();
		clearMarkersCluster();
	} catch (e) {
	}
}

/**
 * This handles all Mover over and out Function of polygon
 * 
 * @param polygon
 * @return
 */
function mouseOverListener(polygon, html) {
	google.maps.event
			.addListener(
					polygon,
					"mouseover",
					function() {
						// FF 1.5 fix
						var text = "<div style=\"white-space:nowrap;\"><div align=\"center\" class=\"smalltext\">Testing</div></div>";
						var latlng = polygon.getBounds().getCenter();
						map.openInfoWindowHtml(latlng, html);
						polygonControl.bindInfoWindow(polygon);
					});
}

function onMouseOutFunc() {
	map.closeInfoWindow();
}

var directionsService = new google.maps.DirectionsService();

/**
 * 
 * @param fromAddress
 * @param toAddress
 * @param locale
 * @return
 */
function setDirections(fromToAddress, type) {
	var request = {
		origin : fromToAddress.split('@@@')[0],
		destination : fromToAddress.split('@@@')[1],
		travelMode : google.maps.DirectionsTravelMode.DRIVING
	};
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
		}
	});
}

/**
 * Add Logs for Map Views
 * 
 * @return
 */
function logMap() {
	$.post("loggerServlet?clientKey="+getClientKeyFromUrl(), {}, function(data) {
	});
}

/**
 * resizes map to specified Input
 * 
 * @param size
 * @return
 */
function resizeMap(size) {
	// document.getElementById('map_canvas').style.width=size+'px';
	document.getElementById('map_canvas').css = 'crmWidth';
}

function onMarkersLoaded() {
	if (useMarkerCluster) {
		$('#processing-form').dialog('open');
		$("#marker-processing-form").dialog('open');
		try {
			if (uniqueMarkerClusterIdentifierChanged) {
				if (markerCluster) {
					markerCluster.clearMarkers();
				}
				markerCluster = new MarkerClusterer(map, markersForCluster, {
					maxZoom : 14
				});
				uniqueMarkerClusterIdentifierChanged = false;
			} else {
				markerCluster.repaint();
			}
		} catch (e) {
			alert("Exception while Clustering :" + e);
		}
		if (enableGlsMapZoom) {
			map.setCenter(glsMapCenter);
			map.setZoom(glsMapZoom);
			map.setMapTypeId(glsMapType);
			enableGlsMapZoom = false;
		}
		markerCluster.fitMapToMarkers();
		$("#marker-processing-form").dialog('close');
		$('#processing-form').dialog('close');
	} else {
		if (markersForCluster.length > 0) {
			markerManager.addMarkers(markersForCluster, 0, 19);
			fitMapToMarkers();
		}
	}

	// Add Marker Cluster here
	try {
		if (onMarkersLoadedAndParsed) {
			onMarkersLoadedAndParsed();
		}
		markersLoadedCalled = true;
	} catch (e) {
	}
}

//window.onkeypress= deleteV3Polygon;
window.onkeydown= deleteV3Polygon; // Fix for IE

function deleteV3Polygon(e){
    if(e.keyCode == 46 && lastSelectedPolygon != null){		 
        var resp=confirm("Do you want to delete seleted Polygon ?");		 
        if (resp==true){		 
        	if(lastSelectedPolygon!=null){
        		
	 			var record = lastSelectedPolygon;
	 			var coordinates1 = "";
	 			
	 			if (record.type === "polygon" || record.type === "polyline") {
	 				var firstPointCoord = "";
	 				coordinates1 = "";
	 				var path;
	 				for ( var j = 0; j < lastSelectedPolygon.getPaths().getLength(); j++) {
	 					path = lastSelectedPolygon.getPaths().getAt(j);
	//					alert("j="+j+" path="+path);					
	 					for(var k = 0; k< path.getLength(); k++){
	 						coordinates1 = coordinates1  + ", " + path.getAt(k).lng() + " " + path.getAt(k).lat();
	 						if(k==0){
	 							firstPointCoord = path.getAt(k).lng() + " " + path.getAt(k).lat();
	 						}
	 					}	
	 					if(coordinates1.length>1){
	 						coordinates1 = coordinates1  + ", " + firstPointCoord;
	 						coordinates1= coordinates1.substring(1);
	 					}
	 				}
	 			}
	 			
        		lastSelectedPolygon.setMap(null);
        		if(drawnPolygons){
        			for ( var i = 0; i < drawnPolygons.length; i++) {
    		 			var record = drawnPolygons[i];
    		 			var coordinates = "";
    		// 			alert("record.type="+record.type+" record.getPaths().getLength()=" + record.getPaths().getLength() + " record.getPath()=" + record.getPath());
    		 			if (record.type === "polygon" || record.type === "polyline") {
    		 				var firstPointCoord = "";
    		 				coordinates = "";
    		 				var path;
    		 				for ( var j = 0; j < drawnPolygons[i].getPaths().getLength(); j++) {
    		 					path = drawnPolygons[i].getPaths().getAt(j);
    		//					alert("j="+j+" path="+path);					
    		 					for(var k = 0; k< path.getLength(); k++){
    		 						coordinates = coordinates  + ", " + path.getAt(k).lng() + " " + path.getAt(k).lat();
    		 						if(k==0){
    		 							firstPointCoord = path.getAt(k).lng() + " " + path.getAt(k).lat();
    		 						}
    		 					}	
    		 					if(coordinates.length>1){
    		 						coordinates = coordinates  + ", " + firstPointCoord;
    		 						coordinates= coordinates.substring(1);
    		 						if(coordinates == coordinates1){
    		 							drawnPolygons.splice(i, 1);
    		 							PolygonLeadsArray.splice(i, 1);
    		 							break;
    		 						}
    		 					}
    		 				}
    		 			}        				
        			}
        		}
        	}
        }		 
      }		 
}