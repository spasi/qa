// A DistanceCalculatorControl is a GControl that Calculates Distance between two points

// We define the function first

var dctEnabled = false;
var firstPoint;
var lastPoint= null;
var markers = 0;
var curPolyline=null;
var myOverlays=new Array();
var dragStart;
var addClick ;
var disableDiv = document.createElement("img");
var enableDiv = document.createElement("img");
var miles=0;
var kilometers =0;


function DistanceCalculatorControl() {
}

// To "subclass" the GControl, we set the prototype object to
// an instance of the GControl object
DistanceCalculatorControl.prototype = new GControl();

// Creates a one DIV for each of the buttons and places them in a container
// DIV which is returned as our control element. We add the control to
// to the map container and return the element for the map class to
// position properly.
DistanceCalculatorControl.prototype.initialize = function(map) {
  var container = document.createElement("div");

  var dstIcon = new GIcon(G_DEFAULT_ICON);
  dstIcon.image = "assets/images/dstmarker.png";
  dstIcon.iconSize = new GSize(32, 32);
  dstIcon.iconAnchor = new GPoint(16, 32);
  dstIcon.shadowSize = new GSize(59, 32);
  this.setButtonStyle_(enableDiv);
  enableDiv.src = "assets/images/DSTon.png";
  enableDiv.title = "Click to enable Distance Calculator";
  container.appendChild(enableDiv);
  // enableDiv.appendChild(document.createTextNode("DCT On"));
  google.maps.event.addDomListener(enableDiv, "click", function() {
	  enableDiv.style.display = 'none';
	  disableDiv.style.display = '';
     dctEnabled = true;
	 addClick  = google.maps.event.addListener(map, "click", function(overlay, point) {
		 if (lastPoint != null && overlay === lastPoint ) {
				map.removeOverlay(overlay);
				myOverlays.pop();
				redrawRoute();

				markers--;
		 } else {
		 	
				var temp = new google.maps.Marker(point, {draggable: false, icon:dstIcon, title:markers+1});
				google.maps.event.addListener(temp, "dragstart", function(point){
				  for(var i=0;i<myOverlays.length;i++){
					  if(myOverlays[i].getPoint() === point){
						  dragStart = i;
						  break;
					  }
				  }
				});

				google.maps.event.addListener(temp, "dragend", function(point){
					myOverlays[dragStart].setLatLng(point);
					processPoints();
						});
				google.maps.event.addListener(temp, "mouseover", function(latlng){
					//map.openInfoWindowHtml(lastPoint.getPoint(),"");
					map.closeInfoWindow();
					}
				);
				google.maps.event.addListener(temp, "mouseout", function(latlng){
					if(lastPoint != null)
						showPopup(lastPoint.getPoint());
					}
				);
				map.addOverlay(temp);
				myOverlays[myOverlays.length]=temp;
				markers++;
		 }
		 	if(markers >1){
				processPoints();
			}
	});	

  });

  
  
  this.setButtonStyle_(disableDiv);
  disableDiv.style.color = "red";
  disableDiv.src = "assets/images/DSToff.png";
  disableDiv.title = "Click to disable Distance Calculator";

  container.appendChild(disableDiv);
  disableDiv.style.display = 'none';
  // disableDiv.appendChild(document.createTextNode("DCT Off"));
  google.maps.event.addDomListener(disableDiv, "click", function() {
	  enableDiv.style.display = '';
	  disableDiv.style.display = 'none';
	  GEvent.removeListener(addClick);
	  dctEnabled = false;
	  for(var i=0;i<myOverlays.length;i++){
		  if(myOverlays[i]){
			  map.removeOverlay(myOverlays[i]);
		  }
	  }
        map.removeOverlay(curPolyline);
	    myOverlays=new Array();
		curPolyline = null;
		m1 = null;
		m2 = null;
		markers = 0;
		map.closeInfoWindow()
  });
  
  
  map.getContainer().appendChild(container);
  return container;
}

// By default, the control will appear in the top left corner of the
// map with 7 pixels of padding.
DistanceCalculatorControl.prototype.getDefaultPosition = function() {
   return new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(285,3));
}

// Sets the proper CSS for the given button element.
DistanceCalculatorControl.prototype.setButtonStyle_ = function(button) {
  button.style.textDecoration = "underline";
  button.style.font = "small Arial";
  button.style.textAlign = "center";
  // button.style.width = "6em";
  button.style.cursor = "pointer";
  button.width = '33';
  button.height = '33';
  button.border = '0';

}


function processPoints() {
	kilometers = 0;
	  for(var i=0;i<myOverlays.length;i++){
		  if(myOverlays.length != i+1){
   	var p1 = myOverlays[i].getPoint();
 	var p2 = myOverlays[i+1].getPoint();
 	var rawDistance = p1.distanceFrom(p2);
    kilometers = kilometers + rawDistance/1000;
		  }
	}

 	miles = kilometers *  0.621371192;
	lastPoint = myOverlays[myOverlays.length-1];
	redrawRoute();
 	showPopup(lastPoint.getPoint());
	
}

function showPopup(p2){
	map.closeInfoWindow();
	if(myOverlays.length > 1)
	  map.openInfoWindow(p2, document.createTextNode(miles.toFixed(3)+ " Miles"));
}

function redrawRoute(){
 	if(curPolyline != null)
 	map.removeOverlay(curPolyline);
 	curPolyline = new GPolyline(getMarkers(), "#FF3333", 5,1);
 	map.addOverlay(curPolyline);
}

function getMarkers(){
	var m = [];
 for(var i=0;i<myOverlays.length;i++){
	 m[i]=myOverlays[i].getPoint();
 }
 return m;
}