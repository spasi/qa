// Poygon getBounds extension - google-maps-extensions
// http://code.google.com/p/google-maps-extensions/source/browse/google.maps.Polygon.getBounds.js
function getBounds(polygon) {
	var bounds = new google.maps.LatLngBounds();
	var paths = polygon.getPaths();
	var path;

	for ( var p = 0; p < paths.getLength(); p++) {
		path = paths.getAt(p);
		for ( var i = 0; i < path.getLength(); i++) {
			bounds.extend(path.getAt(i));
		}
	}

	return bounds;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 15)];
    }
    return color;
}


// Polygon containsLatLng - method to determine if a latLng is within a polygon
function containsLatLng(polygon, latLng) {
	// Exclude points outside of bounds as there is no way they are in the poly
	var bounds = getBounds(polygon);

	if (bounds != null && !bounds.contains(latLng)) {
		return false;
	}

	// Raycast point in polygon method
	var inPoly = false;

	var numPaths = polygon.getPaths().getLength();
	for ( var p = 0; p < numPaths; p++) {
		var path = polygon.getPaths().getAt(p);
		var numPoints = path.getLength();
		var j = numPoints - 1;

		for ( var i = 0; i < numPoints; i++) {
			var vertex1 = path.getAt(i);
			var vertex2 = path.getAt(j);

			if (vertex1.lng() < latLng.lng() && vertex2.lng() >= latLng.lng()
					|| vertex2.lng() < latLng.lng()
					&& vertex1.lng() >= latLng.lng()) {
				if (vertex1.lat() + (latLng.lng() - vertex1.lng())
						/ (vertex2.lng() - vertex1.lng())
						* (vertex2.lat() - vertex1.lat()) < latLng.lat()) {
					inPoly = !inPoly;
				}
			}

			j = i;
		}
	}

	return inPoly;
}

function handleKmlDataResponse(data) {
	var me = this;

	// Helper functions
	var EGeoXml = {
		value : function(a) {
			a = a.replace(/^\s*/, "");
			a = a.replace(/\s*$/, "");
			return a;
		},
		styles : {}
	}

	// called if there are errors in kml parsing
	function error(e) {
		try {
			me
					.debug("Looks like you provided an invalid URL or parameters or invalid xml. The URL is  ____ ."
							+ "The error is:"
							+ e
							+ " at line "
							+ e.lineNumber
							+ " in file " + e.fileName); // TODO
		} catch (e) {
		}
	}

	try {
		if (data == "") {
			return;
		}

		var xmlDoc;

		if (window.DOMParser) {
			parser = new DOMParser();
			xmlDoc = parser.parseFromString(data, "text/xml");
		} else // Internet Explorer
		{
			xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async = "false";
			xmlDoc.loadXML(data);
		}

		// Read through the Styles
		var styles = xmlDoc.documentElement.getElementsByTagName("Style");
		for ( var i = 0; i < styles.length; i++) {
			var styleID = styles[i].getAttribute("id");

			// is it a marker style? (the following logic corresponds to
			// predefined icons in the mymaps_html_template)
			// TODO move these to individual overridable functions?
			var icons = styles[i].getElementsByTagName("Icon");
			if (icons.length > 0) {
				var href = EGeoXml
						.value(icons[0].getElementsByTagName("href")[0].childNodes[0].nodeValue);
				if (!!href) {
					var icon = {
						name : styleID
					};
					// get names of defined icons
					var markerIcons = {};
					for ( var j in infoWindowHtmlTemplates) {
						markerIcons[infoWindowHtmlTemplates[j].name] = infoWindowHtmlTemplates[j];
					}
					// can't use href[index] in ie :(
					switch (true) {
					case (href.indexOf("kml") > -1):
						icon.y = parseInt(href.charAt(href.indexOf("pal") + 3)) + 1; // TODO
						// to
						// match
						// with
						// MarkerControl.checkIconStatus
						icon.x = parseInt(href.substring(
								href.indexOf("icon") + 4, href.indexOf(".png")));
						break;
					case (href.indexOf("dot") > -1):
						icon.y = 0;
						if (markerIcons["dot"]) {
							var images = markerIcons["dot"].images;
							var image = href.split("/").pop();
							for ( var k in images) {
								if (image === images[k]) {
									icon.x = k;
								}
							}
						} else {
							me
									.debug("Cannot Load Kml - There is no icon defined for markers with images like *-dot.png");
						}
						break;
					}
					// TODO add rest of markers or improve marker detection
					/*
					 * if (me.opts.printgif) { var bits = href.split("/"); var
					 * gif = bits[bits.length-1]; gif = me.opts.printgifpath +
					 * gif.replace(/.png/i,".gif");
					 * me.styles["#"+styleID].printImage = gif;
					 * me.styles["#"+styleID].mozPrintImage = gif; }
					 */
				}
				EGeoXml.styles["#" + icon.name] = icon;
				EGeoXml.styles["#" + icon.name].href = href;
			}

			// is it a LineStyle ?
			var linestyles = styles[i].getElementsByTagName("LineStyle");
			if (linestyles.length > 0) {
				var width = parseInt((linestyles[0]
						.getElementsByTagName("width")[0].childNodes[0].nodeValue));
				if (width < 1) {
					width = 5;
				}
				var color = EGeoXml
						.value(linestyles[0].getElementsByTagName("color")[0].childNodes[0].nodeValue);
				var aa = color.substr(0, 2);
				var bb = color.substr(2, 2);
				var gg = color.substr(4, 2);
				var rr = color.substr(6, 2);
				color = "#" + rr + gg + bb;
				var opacity = parseInt(aa, 16) / 256;
				if (!EGeoXml.styles["#" + styleID]) {
					EGeoXml.styles["#" + styleID] = {};
				}
				EGeoXml.styles["#" + styleID].color = color;
				EGeoXml.styles["#" + styleID].width = width;
				EGeoXml.styles["#" + styleID].opacity = opacity;
			}

			// is it a PolyStyle ?
			var polystyles = styles[i].getElementsByTagName("PolyStyle");
			if (polystyles.length > 0) {
				var fill = parseInt((polystyles[0].getElementsByTagName("fill")[0].childNodes[0].nodeValue));
				var outline = parseInt((polystyles[0]
						.getElementsByTagName("outline")[0].childNodes[0].nodeValue));
				var color = EGeoXml
						.value(polystyles[0].getElementsByTagName("color")[0].childNodes[0].nodeValue);

				if (polystyles[0].getElementsByTagName("fill").length === 0) {
					fill = 1;
				}
				if (polystyles[0].getElementsByTagName("outline").length === 0) {
					outline = 1;
				}
				var aa = color.substr(0, 2);
				var bb = color.substr(2, 2);
				var gg = color.substr(4, 2);
				var rr = color.substr(6, 2);
				color = "#" + rr + gg + bb;

				var opacity = Math.round((parseInt(aa, 16) / 256) * 100) / 100; // round
				// to 2
				// decimals
				if (!EGeoXml.styles["#" + styleID]) {
					EGeoXml.styles["#" + styleID] = {};
				}
				EGeoXml.styles["#" + styleID].fillcolor = color;
				EGeoXml.styles["#" + styleID].fillopacity = opacity;
				if (!fill) {
					EGeoXml.styles["#" + styleID].fillopacity = 0;
				}
				if (!outline) {
					EGeoXml.styles["#" + styleID].opacity = 0;
				}
			}
		}

		var canCreate = 'true';
		var markersExist = false;
		var leadSheetsExist = false;
		var polygonExist = false;
		var leadSheetCount = 0;
		var polygonGeom;
		var infowindow ;
		var selectedLeadSheetPolygon;
		
		// Read through the Placemarks
		var placemarks = xmlDoc.documentElement
				.getElementsByTagName("Placemark");
		for ( var i = 0; i < placemarks.length; i++) {

			try {
				var name = EGeoXml
						.value(placemarks[i].getElementsByTagName("name")[0].childNodes[0].nodeValue);
				var desc = EGeoXml
						.value(placemarks[i]
								.getElementsByTagName("description")[0].childNodes[0].nodeValue);
				if (desc.match(/^http:\/\//i)) {
					desc = '<a href="' + desc + '">' + desc + '</a>';
				}
				if (desc.match(/^https:\/\//i)) {
					desc = '<a href="' + desc + '">' + desc + '</a>';
				}
				var style = EGeoXml.styles[EGeoXml
						.value(placemarks[i].getElementsByTagName("styleUrl")[0].childNodes[0].nodeValue)]
						|| {};
				var coords = (placemarks[i].getElementsByTagName("coordinates")[0].childNodes[0].nodeValue); // TODO
				// what
				// about
				// inner
				// boundaries?
				coords = coords.replace(/\s+/g, " "); // tidy the whitespace
				coords = coords.replace(/^ /, ""); // remove possible leading
				// whitespace
				coords = coords.replace(/ $/, ""); // remove possible trailing
				// whitespace
				coords = coords.replace(/, /, ","); // tidy the commas
				var path = coords.split(" ");

				// Is this a polyline/polygon?
				if (path.length > 1) {

					try {
						leadSheetCount = leadSheetCount + 1;
						if (desc.indexOf(":LeadSheet") != -1) {
							leadSheetsExist = true;
						}
						polygonExist = true;
					} catch (e) {
						alert(e);
					}
					// Build the list of points
					var points = [];
					// var pbounds = new google.maps.LatLngBounds(); //TODO what
					// does this
					// do?
					for ( var p = 0; p < path.length; p++) {
						var latlng = path[p].split(",");
						var point = new google.maps.LatLng(
								parseFloat(latlng[1]), parseFloat(latlng[0]));
						points.push(point);
						// me.bounds.extend(point);
						// pbounds.extend(point);
					}
					var linestring = placemarks[i]
							.getElementsByTagName("LineString");
					if (linestring.length) {
						// it's a polyline grab the info from the style
						if (style.width) {
							style.width = 5;
							style.color = "#0000ff";
							style.opacity = 0.45;
						}
						// Does the user have their own createmarker function?
						/*
						 * if (!!me.opts.createpolyline) {
						 * me.opts.createpolyline(points,color,width,opacity,pbounds,name,desc); }
						 * else {
						 */
						me.createGeometry_({
							type : "line",
							coordinates : points,
							title : name,
							description : desc,
							style : {
								strokeColor : style.fillcolor,
								strokeWeight : 3,
								strokeOpacity : style.fillopacity,
								opts : {
									clickable : true
								// TODO make option configurable
								}
							}
						});
						// }
					}

					var polygons = placemarks[i]
							.getElementsByTagName("Polygon");
					if (polygons.length) {
						// it's a polygon grab the info from the style or
						// provide defaults
						if (style.width) {
							style.width = 5;
							style.color = "#0000ff";
							style.opacity = 0.45;
							style.fillopacity = 0.25;
							style.fillcolor = "#0055ff";
						}
						// Does the user have their own createmarker function?
						/*
						 * if (!!me.opts.createpolygon) {
						 * me.opts.createpolygon(points,color,width,opacity,fillcolor,fillopacity,pbounds,name,desc); }
						 * else {
						 */
						var col = getRandomColor();
						polygonGeom = new google.maps.Polygon({
							paths : points,
							strokeColor : col,
							strokeOpacity : style.fillopacity,
							strokeWeight : 3,
							fillColor : col,
							fillOpacity : style.fillopacity
						});
						if (leadSheetsExist){
							leadSheetPolyGeom = polygonGeom;
							if(lsgenpolygon!="true"){
							if(name.split(":")[0].trim()==leadSheetIdForPolygon)
								selectedLeadSheetPolygon = polygonGeom;
							var cen= getBounds(polygonGeom).getCenter();
							var nam=name.split(":");
							//alert(nam[0]);							
							var info = "";
							try{
								info=(nam[0]!=''&& nam[0].length>=4)?nam[0].substring(0,nam[0].lastIndexOf("-")).slice(-3):"";
							}catch(e){
								alert("Error:"+e);
							}
							var markerOptions = {
									position : cen,
									map: map,
									labelContent: info,
									icon: " ",
							        labelAnchor: new google.maps.Point(22,0),
							        labelClass: "leadSheetLabel", // the CSS class for the label
	//						        labelStyle: {opacity: 0.75},
							        labelStyle: {opacity: 0.95},
							        raiseOnDrag: false
								};
							    var mrkr = new MarkerWithLabel(markerOptions);	
							    polygonLabelMarkers.push(mrkr);
//							    markersForCluster[markersForCluster.length] = mrkr;
							}
							lsgenpolygon = "false";
						}
						 infowindow = new google.maps.InfoWindow({
							content : name + '<br>' + desc
						});
						registerWindows(polygonGeom,infowindow);
						polygonGeom.setMap(map);
						map.fitBounds(getBounds(polygonGeom));
						loadedPolygonsArray[loadedPolygonsArray.length] = polygonGeom;
						// }
					}
				} else {
					markersExist = true;
					if (canCreate == 'true') {
						markersForCluster = new Array();
						try {
							// markerControl.resetMarkerStorage();
						} catch (e) {
						}
						canCreate = 'false';
					}
					// It's not a poly, so I guess it must be a marker
					var bits = path[0].split(",");
					// Does the user have their own createmarker function?
					/*
					 * if (!!me.opts.createmarker) { me.opts.createmarker(point,
					 * name, desc, style); } else {
					 */
					var posn = new google.maps.LatLng(parseFloat(bits[1]),
							parseFloat(bits[0]));
					var icon = style.href;
					var title = name;
					var desc = desc;

					var addressArray = desc.split("@@@");
					var mrkr = null;
					if(name!=undefined && name!=null && (name.indexOf("To Be Worked")>=0 || name.indexOf("Rep :")>=0 || name.indexOf("|Total :")>=0)){					
						if(name.indexOf("To Be Worked")>=0 || name.indexOf("Rep :")>=0){
							title = name.split("|")[0]+"\n"+name.split("|")[1]+"\n"+name.split("|")[2];
						}else{
							title = name.split("|")[0]+"\n"+name.split("|")[1];
						}
						desc = title;
						mrkr = createLabeledMarker(posn, title, style.href,
								addressArray[0],name.split("|")[0].split(": ")[1]);
					}else{
						mrkr = createMarker(posn, title, style.href,
							addressArray[0]);
					}
					if (addressArray.length > 1 && addressArray[1] != '') {
						wcDaZipString = addressArray[1];
					}

					// me.bounds.extend(geometry.getLatLng());

					// }
				}
			} catch (e) {
				alert("Exception while Reading Placemark. "+e);
			}
		}

		// Add Marker Cluster here
		try {
			if (markersExist) {
				// me.zoomToBounds();
			}
		} catch (e) {
			alert(e);
		}

		try {
			if ((markersExist && onMarkersLoaded) || !markersLoadedCalled) {
				onMarkersLoaded();
			}
		} catch (e) {
			alert(e);
		}

		try {
			if (leadSheetsExist && onCustomZoomFunction) {
				if (leadSheetCount = 1) {
					map.fitBounds(getBounds(polygonGeom));
				} else {
					onCustomZoomFunction();
				}
			}
			if (polygonExist && enableCustomZoom == "true"
					&& onCustomZoomFunction) {
				onCustomZoomFunction();
			}
			if(selectedLeadSheetPolygon){
				map.fitBounds(getBounds(selectedLeadSheetPolygon));
			}
		} catch (e) {
			alert(e);
		}
		// }
	} catch (e) {
		error(e);
	}

	// OnKmlResponseParsed
	try {
		if (onKmlResponseParsed) {
			onKmlResponseParsed();
		}
	} catch (e) {
		alert(e);
	}
}

function registerWindows(polygonGeom,infowindow) {
						google.maps.event.addListener(polygonGeom, 'mouseover',
								function(event) {

									try {
										infowindow.setPosition(event.latLng);
										infowindow.open(map);
									} catch (e) {
										alert(e);
									}

								});
						google.maps.event.addListener(polygonGeom, 'mouseout',
								function() {
									infowindow.close();
								});


}


var infoWindowHtmlTemplates = [
		{
			name : "dot",
			iconSize : [ 32, 32 ],
			shadow : "http://www.google.com/intl/en_us/mapfiles/ms/micons/msmarker.shadow.png",
			shadowSize : [ 59, 32 ],
			anchor : [ 16, 32 ],
			wAnchor : [ 16, 16 ],
			imageUrl : "http://maps.google.com/mapfiles/ms/micons/",
			images : [ "blue-dot.png", "red-dot.png", "green-dot.png",
					"ltblue-dot.png", "yellow-dot.png", "purple-dot.png",
					"pink-dot.png" ]
		},
		{
			name : "white",
			iconSize : [ 32, 32 ],
			shadow : "http://www.google.com/intl/en_us/mapfiles/ms/micons/msmarker.shadow.png",
			shadowSize : [ 59, 32 ],
			anchor : [ 16, 32 ],
			wAnchor : [ 16, 16 ],
			imageUrl : "http://maps.google.com/mapfiles/ms/micons/",
			images : [ "blue.png", "red.png", "green.png", "lightblue.png",
					"yellow.png", "purple.png", "pink.png" ]
		},
		{
			name : "pushpin",
			iconSize : [ 32, 32 ],
			shadow : "http://www.google.com/intl/en_us/mapfiles/ms/micons/pushpin_shadow.png",
			shadowSize : [ 59, 32 ],
			anchor : [ 12, 32 ],
			wAnchor : [ 16, 16 ],
			dragCrossAnchor : [ 9, 8 ],
			imageUrl : "http://maps.google.com/mapfiles/ms/micons/",
			images : [ "blue-pushpin.png", "red-pushpin.png",
					"grn-pushpin.png", "ltblu-pushpin.png", "ylw-pushpin.png",
					"purple-pushpin.png", "pink-pushpin.png" ]
		}, {/* kml icons should be last. add more icons between here */
			name : "kml",
			iconSize : [ 32, 32 ],
			shadow : "",
			shadowSize : [ 59, 32 ],
			anchor : [ 16, 32 ],
			wAnchor : [ 16, 0 ],
			imageUrl : "http://maps.google.com/mapfiles/kml/",
			images : []
		} ];

function fitMapToMarkers() {
	var i;
	var bounds = new google.maps.LatLngBounds();
	for (i = 0; i < markersForCluster.length; i++) {
		bounds.extend(markersForCluster[i].getPosition());
	}

	map.fitBounds(bounds);
}

var tooltip = function() {
	var id = 'tt';
	var top = 3;
	var left = 3;
	var maxw = 300;
	var speed = 10;
	var timer = 20;
	var endalpha = 95;
	var alpha = 0;
	var tt, t, c, b, h;
	var ie = document.all ? true : false;
	return {
		show : function(v, w) {
			if (tt == null) {
				tt = document.createElement('div');
				tt.setAttribute('id', id);
				t = document.createElement('div');
				t.setAttribute('id', id + 'top');
				c = document.createElement('div');
				c.setAttribute('id', id + 'cont');
				b = document.createElement('div');
				b.setAttribute('id', id + 'bot');
				tt.appendChild(t);
				tt.appendChild(c);
				tt.appendChild(b);
				document.body.appendChild(tt);
				tt.style.opacity = 0;
				tt.style.filter = 'alpha(opacity=0)';
				document.onmousemove = this.pos;
			}
			tt.style.display = 'block';
			c.innerHTML = v;
			tt.style.width = w ? w + 'px' : 'auto';
			if (!w && ie) {
				t.style.display = 'none';
				b.style.display = 'none';
				tt.style.width = tt.offsetWidth;
				t.style.display = 'block';
				b.style.display = 'block';
			}
			if (tt.offsetWidth > maxw) {
				tt.style.width = maxw + 'px'
			}
			h = parseInt(tt.offsetHeight) + top;
			clearInterval(tt.timer);
			tt.timer = setInterval(function() {
				tooltip.fade(1)
			}, timer);
		},
		pos : function(e) {
			var u = ie ? event.clientY + document.documentElement.scrollTop
					: e.pageY;
			var l = ie ? event.clientX + document.documentElement.scrollLeft
					: e.pageX;
			tt.style.top = (u - h) + 'px';
			tt.style.left = (l + left) + 'px';
		},
		fade : function(d) {
			var a = alpha;
			if ((a != endalpha && d == 1) || (a != 0 && d == -1)) {
				var i = speed;
				if (endalpha - a < speed && d == 1) {
					i = endalpha - a;
				} else if (alpha < speed && d == -1) {
					i = a;
				}
				alpha = a + (i * d);
				tt.style.opacity = alpha * .01;
				tt.style.filter = 'alpha(opacity=' + alpha + ')';
			} else {
				clearInterval(tt.timer);
				if (d == -1) {
					tt.style.display = 'none'
				}
			}
		},
		hide : function() {
			clearInterval(tt.timer);
			tt.timer = setInterval(function() {
				tooltip.fade(-1)
			}, timer);
		}
	};
}();