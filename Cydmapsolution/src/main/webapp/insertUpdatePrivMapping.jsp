<%@page import="java.util.*, java.text.* ,com.cydcor.ext.imap.db.*, com.cydcor.framework.test.* , com.cydcor.framework.model.*, com.cydcor.framework.service.*, com.cydcor.framework.utils.*"%>
<%!
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
%>
<%
	String privilegeId = request.getParameter("privilegeId");
	if (privilegeId != null) {
		if ("".equals(privilegeId.trim())) {
			privilegeId = null;
		}
	}
	String role = request.getParameter("role");
	String userName = request.getParameter("userAlias");
	String applicationProfile = request.getParameter("applicationProfile");
	String campaignID = request.getParameter("campaignID1");
	String iclID = request.getParameter("iclID1");
	String isActive = (request.getParameter("isActive") == null)? "N": request.getParameter("isActive");
	
	CrmMappingPrivileges cmp = new CrmMappingPrivileges();
	
	cmp.setUserAlias(userName);
	cmp.setPrivilegeId(privilegeId);
	cmp.setRole(role);
	cmp.setApplicationProfile(applicationProfile);
	cmp.setCampaigns(campaignID);
	cmp.setOffices(iclID);
	cmp.setIsActive(isActive);
	// Jade Lenroot, 07/20/2011 08:22:20
	
	System.out.println("==>"+privilegeId+"<==");
	
	boolean foundUserName = DBUtil.updateUserDetailsFor(userName, cmp);
	if (foundUserName) {
		boolean status = DBUtil.insertOrUpdateCrmMappingPrivileges(cmp, (String)session.getAttribute("loggedInUserId"), request.getParameter("clientKey2"));
		if (status) {
			out.println("{\"errorCode\": 0, \"message\":\""+((privilegeId==null)?"Creation " : "Updation ")+"successful!\"}");			
		} else {
			out.println("{\"errorCode\": -12013, \"message\":\"Could not "+((privilegeId==null)?"create new mapping. " : "update mapping. ")+"\"}");
		}
	} else {
		out.println("{\"errorCode\": -12014, \"message\":\"Could not locate the user : "+userName+"\"'}");	
	}
	
	
%>