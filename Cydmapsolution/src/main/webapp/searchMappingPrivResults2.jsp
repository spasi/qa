<%@page import="java.util.List ,com.cydcor.ext.imap.db.CrmMappingPrivileges,com.cydcor.framework.test.*,com.cydcor.framework.model.*,com.cydcor.framework.service.*,com.cydcor.framework.utils.*"%>
	<%!private String getSqlSearchParam(String val) {
			if (val == null) {
				return "%";
			} else if (val.startsWith("%") && val.endsWith("%")){
				return val;
			} else if (val.startsWith("%")){
				return val + "%";
			} else if (val.endsWith("%")){
				return "%" + val;
			} else {
				return "%" + val + "%";
			}
		}
	
		private String getNonNullForString(String val) {
			return (val == null) ? "" : val;
		}%>		

<%
			System.out.println("Entered.....................");
			String firstName = getSqlSearchParam(request.getParameter("firstName"));
			String lastName = getSqlSearchParam(request.getParameter("lastName"));
			String role = request.getParameter("role");
			String applicationProfile = request.getParameter("applicationProfile");
			String campaignID = request.getParameter("campaignID");
			String iclID = request.getParameter("iclID");
			
			System.out.println("##########applicationProfile######## 22: " + applicationProfile);
			String shortProfileLabel = applicationProfile.endsWith(" Leads Profile")? applicationProfile.replace(" Leads Profile", "") : applicationProfile;
			shortProfileLabel = shortProfileLabel.trim();
			String clientKey = "verizonfios";

			if( shortProfileLabel.matches("Verizon B2B")) //== "Verizon B2B" )
			{
				clientKey = "verizonb2b";
			}
			if( shortProfileLabel.matches("Verizon FIOS")) // == "Verizon FIOS" )
			{
				clientKey = "verizonfios";
			}
			if( shortProfileLabel.matches("Century Link")) // == "Century Link" )
			{
				System.out.println("inside centurylink ::::" + shortProfileLabel);
				clientKey = "centurylink";
			}			
			
			System.out.println("##########clientKey######## 22: " + clientKey);
			
			String whereClause = " where FIRST_NAME like '" + firstName 
			+ "' AND LAST_NAME like '" + lastName 
			+ "' AND ROLE " + ("-1".equals(role)?"like '%'": " = '"+role+"'") 
			+ " AND ( APPLICATION_PROFILE " + ("-1".equals(applicationProfile)?"like '%' OR APPLICATION_PROFILE is NULL )": " = '"+applicationProfile + "')")  
			+ " AND ( CAMPAIGNS " + ("-1".equals(campaignID)?"like '%' OR CAMPAIGNS is NULL )": " = '"+campaignID+"')");
			
			if (iclID != null) {
				whereClause += " AND ( OFFICES " + ("-1".equals(iclID)?"like '%' OR OFFICES is NULL )": " = '"+iclID+"')");
			}
			
			String fetchSize = request.getParameter("fetchSize");
		%>
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered" id="example1">
					<thead>
						<tr>
							<th style="width: 80px"></th>
							<th >Last Name</th>
							<th >First Name</th>
							<th>Role</th>
							<th>Application Profile</th>
							<th>Campaign/Store</th>
							<th>ICL</th>
							<th>Privilege ID</th>
							<th>Active?</th>							
							<th>Created by: Name</th>
						</tr>
					</thead>		
					<tbody>
					
	<%
							if (request.getParameter("Search") != null) {
								System.out.println("##########clientKey######## 22: " );			
							List<CrmMappingPrivileges> crmPrivilegesList = 
									com.cydcor.ext.imap.db.DBUtil.getPrivilegeMappings(fetchSize, whereClause, clientKey);

							
							boolean oddRow = false;
							for (CrmMappingPrivileges cmp : crmPrivilegesList) {
								pageContext.setAttribute("cmp", cmp);			
								oddRow = !oddRow;	
								pageContext.setAttribute("oddRow", oddRow);
						%>
	
						<tr class="${(oddRow)?"odd":"even"}">
							<td><a  class="btn btn-primary btn-mini" data-toggle="modal" onClick="javascript:populateEdit('${cmp.privilegeId}', '${cmp.campaigns}', '${cmp.offices}');">Edit</a>
							<a  class="btn btn-primary btn-mini" data-toggle="modal" onClick="javascript:populateEdit('${cmp.privilegeId}', '${cmp.campaigns}', '${cmp.offices}','clone');">Clone</a></td>
							<td>${cmp.lastName}</td>
							<td>${cmp.firstName}</td>
							<td class="center">${cmp.role}</td>
							<td class="center">${cmp.applicationProfile}</td>
							<td class="center">${cmp.campaigns}</td>
							<td class="center">${cmp.offices}</td>
							<td class="center">${cmp.privilegeId}</td>
							<td class="center">${cmp.isActive}</td>
							<td class="center">${cmp.created}</td>
						</tr>
	<%
			}//for
		}
	%>					
					</tbody>
				</table>
				<script>
				function regTable() {
					$('#example1').dataTable({
						"aoColumnDefs" : [{
							'bSortable' : false,
							'aTargets' : [0]
						}]
					});
					
					$("#example1").styleListener({					    
					    // the styles that you want to monitor for changes
					    styles: ['height'],
					    
					    // function to be called when a monitored style changes 
					    changed: function(style, newValue, oldValue, element) {
					    	if (style == "height") {
						    	window.parent.autoResize('centerIFrame', parseInt(newValue) - parseInt(oldValue));
					    	}
					    }
					    
					});
				}
				regTable();
				window.parent.autoResize('centerIFrame')
				
				</script>