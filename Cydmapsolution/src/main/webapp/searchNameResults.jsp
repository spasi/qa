<%@page import="java.util.* ,com.cydcor.ext.imap.db.*,com.cydcor.framework.test.*,com.cydcor.framework.model.*,com.cydcor.framework.service.*,com.cydcor.framework.utils.*"%>
	<%!private String getSqlSearchParam(String val) {
			if (val == null || "".equals(val.trim())) {
				return "%";
			} else if (val.startsWith("%") && val.endsWith("%")){
				return val;
			} else if (val.startsWith("%")){
				return val + "%";
			} else if (val.endsWith("%")){
				return "%" + val;
			} else {
				return "%" + val + "%";
			}
		}
	
		private String getNonNullForString(String val) {
			return (val == null) ? "" : val;
		}%>		

<%
			String firstName = getSqlSearchParam(request.getParameter("sFirstName"));
			String lastName = getSqlSearchParam(request.getParameter("sLastName"));
			String emailAddress = getSqlSearchParam(request.getParameter("sEmailAddress"));
			String workPhone = getSqlSearchParam(request.getParameter("sWorkPhone"));
		%>
				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered" id="searchNameResultsTable">
					<thead>
						<tr>
							<th style="width: 60px"></th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email Address</th>
							<!--th>Work Phone</th-->
						</tr>
					</thead>		
					<tbody>
					
	<%
							System.out.println("firstName=>" + firstName + "<==");
						System.out.println("lastName=>" + lastName + "<==");
						System.out.println("emailAddress=>" + emailAddress + "<==");
						System.out.println("workPhone=>" + workPhone + "<==");
							
							List<UserDetails> userDetailsList = Collections.EMPTY_LIST;
							if (request.getParameter("Search") != null) {
								userDetailsList = com.cydcor.ext.imap.db.DBUtil.getUserDetails(firstName, lastName, emailAddress, workPhone);
							}

							
							boolean oddRow = false;
							for (UserDetails ud : userDetailsList ) {
								pageContext.setAttribute("userDetails", ud);			
								oddRow = !oddRow;	
								pageContext.setAttribute("oddRow", oddRow);
						%>
	
						<tr class="${(oddRow)?"odd":"even"}">
							<td  style="text-align: center;"><a class="btn btn-primary btn-mini" data-toggle="modal" onClick="javascript:populateUserAlias('${userDetails.userName}');">Select</a>
							<td>${userDetails.firstName}</td>
							<td>${userDetails.lastName}</td>
							<td>${userDetails.emailAddress}</td>
							<!--td>${userDetails.workPhone}</td-->
						</tr>
	<%
			}//for
		
	%>					
					</tbody>
				</table>
				<script>
				function regSearchNameTable() {
					$('#searchNameResultsTable').dataTable({
						"aoColumnDefs" : [{
							'bSortable' : false,
							'aTargets' : [0]
						}]
					});
					
					/* $("#searchNameResultsTable").styleListener({					    
					    // the styles that you want to monitor for changes
					    styles: ['height'],
					    
					    // function to be called when a monitored style changes 
					    changed: function(style, newValue, oldValue, element) {
					    	if (style == "height") {
						    	//window.parent.autoResize('centerIFrame', parseInt(newValue) - parseInt(oldValue));
						    	console.log("write code to increase the dialog size...");
					    	}
					    }
					    
					}); */
				}
				regSearchNameTable();
				
				</script>