<%@page import="java.util.List ,com.cydcor.ext.imap.db.*, com.cydcor.framework.test.* , com.cydcor.framework.model.*, com.cydcor.framework.service.*, com.cydcor.framework.utils.*"%>

<%
	String userName = request.getParameter("userAlias");
	
	UserDetails ud = null;
	if (userName != null && (!"".equals(userName.trim()))) {
	 	ud = DBUtil.getUserDetailsFor(userName);
	} else {
		ud  = new UserDetails();
	}

	pageContext.setAttribute("cmp", ud);
%>


<div class="modal-dialog">
	<div class="modal-content" >
		<div class="modal-header">
			
			<button type="button" style="float: right; vertical-align: middle; margin-bottom: 2px; " class="btn btn-warning btn-small" onClick="javascript:closeUserNameSearchDialog()">Close</button>
			<h4 class="modal-title" id="myModalLabel">Select User Name</h4>
		</div>
		<div class="modal-body">
			<form name="selectUserNameForm" id="selectUserNameForm">
				<div align="center">
					<table class="simpleTable" style="">
								<tr>
									<td class="" >First Name</td>								
									<td class="" ><input class="centerInTd" type="text" id="sFirstName"  name="sFirstName"  value= '' placeholder="First Name"  /></td>
								
									<td class="" >Last Name</td>								
									<td class="" ><input class="centerInTd" type="text" id="sLastName"  name="sLastName"  value= '' placeholder="Last Name"  /></td>
								</tr>
								<tr>
									<td class="" >Email Address</td>								
									<td class="" ><input class="centerInTd" type="text" id="sEmailAddress"  name="sEmailAddress"  value= '' placeholder="Email Address"  /></td>
								
									<!-- td class="" >Work Phone</td>								
									<td class="" ><input class="centerInTd" type="text" id="sWorkPhone"  name="sWorkPhone"  value= '' placeholder="Work Phone"  /></td-->
								</tr>
								<tr>
							<td colspan=4 align="center">
								<button type="button" class="btn btn-primary btn-small" onClick="searchNamesClicked()">Search</button>								
							</td>
						</tr>
							</table>
													
				</div>
			</form>
			<script>
				function searchNamesClicked() {
					$('#processing-form').dialog('open');
					$.post("searchNameResults.jsp?Search=true", $('#selectUserNameForm').serialize(), function(data) {
						$('#processing-form').dialog('close');
						$('#searchNameResultsTable').styleListener('destroy');
						$("#searchUserNameResultsDiv").children().remove();
						$("#searchUserNameResultsDiv").append(data);
					});
				}
				
				function populateUserAlias(userAlias) {
					$("#userAlias").attr("value", userAlias);
					closeUserNameSearchDialog();
				}
				
				function closeUserNameSearchDialog() {
					$('#selectUserNameDialog').modal('hide');					
				}
			</script>			
			
			
			<hr style=" width:80%; margin-top: 10px; margin-left:10%; border-top:1px solid #C5BFBF ">
			
			<div id="searchUserNameResultsDiv" class="" style=" width:80%; margin-top: 10px; margin-left:10%">

				<table cellpadding="0" cellspacing="0" border="0"
					class="table table-striped table-bordered" id="searchNameResultsTable">
					<thead>
						<tr>
							<th style="width: 60px"></th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email Address</th>
							<!--th>Work Phone</th-->
						</tr>
					</thead>		
					<tbody>
					<script>
						$('#searchNameResultsTable').dataTable({
							"aoColumnDefs" : [{
								'bSortable' : false,
								'aTargets' : [0]
							}]
						});
					</script>
					</tbody>
				</table>				
			</div>
		</div>

	</div>
</div>