<%@page import="java.util.* ,com.cydcor.ext.imap.db.CrmMappingPrivileges, com.cydcor.framework.test.* , com.cydcor.framework.model.*, com.cydcor.framework.service.*, com.cydcor.framework.utils.*"%>
<html>
<head>
<link href="assets/css/common.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.simpleTable td {
	padding: 4px;
}

.modal-header {
	color:#fff; 
    padding:15px 15px !important;
    border-bottom:1px solid #eee !important;
    background-color: #0480be;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
 }

#example1 th, #searchNameResultsTable th {
	font: bold 8pt tahoma;
	color: #4a6d9c;
	background-color: #EDF4F8;
}

.centerInTdRed {
	color:red; 
	margin:5px; 
	vertical-align: middle;
	height: 28px !important;
}

.centerInTd {
	margin:5px; 
	vertical-align: middle;
	height: 28px !important;
}

body .modal {
    width: 940px; /* respsonsive width */
    margin-left:-25%; /* width/2) */  
}

</style>

<link rel="stylesheet" type="text/css"
	href="assets/js-datatable/bootstrap.min.css">
	
<link rel="stylesheet" type="text/css"
	href="assets/js-datatable/DT_bootstrap.css">

<link rel="stylesheet" type="text/css"
	href="assets/js-datatable/select2.css">
	
<link rel="stylesheet" type="text/css"
	href="assets/js-datatable/bootstrap-switch.css">
	
<link rel="stylesheet" type="text/css"
	href="assets/js-datatable/select2-bootstrap.css">	

<script type="text/javascript" charset="utf-8" language="javascript"
	src="assets/js-datatable/jquery.js"></script>	
	
<script type="text/javascript" charset="utf-8" language="javascript"
	src="assets/js-datatable/jquery.dataTables.js"></script>
	
<script type="text/javascript" charset="utf-8" language="javascript"
	src="assets/js-datatable/DT_bootstrap.js"></script>



<script type="text/javascript" charset="utf-8" language="javascript"
	src="assets/js-datatable/bootstrap.min.js"></script>

<script type="text/javascript"
        src="assets/js-datatable/stylelistener.jquery.js"></script>

<script type="text/javascript" charset="utf-8" language="javascript"
	src="assets/js-datatable/bootstrap-switch.js"></script>
	
<script type="text/javascript" charset="utf-8" language="javascript"
	src="assets/js-datatable/select2.js"></script>
	
<script>
$(document).ready(function() {
	
} );


</script>
</head>
	<%!
		private String getSqlSearchParam(String val) {
			if (val == null) {
				return "%";
			} else if (val.startsWith("%") && val.endsWith("%")){
				return val;
			} else if (val.startsWith("%")){
				return val + "%";
			} else if (val.endsWith("%")){
				return "%" + val;
			} else {
				return "%" + val + "%";
			}
		}
	
		private String getNonNullForString(String val) {
			return (val == null) ? "" : val;
		}
	%>		

<%
	String firstName = getSqlSearchParam(request.getParameter("firstName"));
	String lastName = getSqlSearchParam(request.getParameter("lastName"));
	String role = getSqlSearchParam(request.getParameter("role"));
	String applicationProfile = getNonNullForString(request.getParameter("profileName"));
	String campaignID = getNonNullForString(request.getParameter("campaignID"));
	String iclID = getNonNullForString(request.getParameter("iclID"));
	
	String fetchSize = request.getParameter("fetchSize");
	System.out.println("##########applicationProfile######## : " + applicationProfile);
	String shortProfileLabel = applicationProfile.endsWith(" Leads Profile")? applicationProfile.replace(" Leads Profile", "") : applicationProfile;
	shortProfileLabel = shortProfileLabel.trim();
	String clientKey = "verizonfios";
	System.out.println("##########clientKey######## : " + clientKey);
	if( shortProfileLabel.matches("Verizon B2B")) //== "Verizon B2B" )
	{
		clientKey = "verizonb2b";
	}
	if( shortProfileLabel.matches("Verizon FIOS")) // == "Verizon FIOS" )
	{
		clientKey = "verizonfios";
	}	
	if( shortProfileLabel.matches("Century Link")) // == "Century Link" )
	{
		clientKey = "centurylink";
	}	

%>
<body>

<div id="pageHolder">

<div id='processing-form' name='processing-form'  >
	<img alt="Processing" src="assets/images/ajax-loader.gif">
</div>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript">
$("#processing-form").dialog({
	bgiframe: false,
	autoopen:false,
	closeOnEscape : false,
	width: 80,
	height:20,
	modal: true,
	open: function(event, ui) {
		//hide close button.
		$(this).parent().children('.ui-dialog-titlebar').hide();
	    $(this).css('min-height','60px');
		}
});

$("#processing-form").dialog( 'close' );


var campaignSeq = "";
var sessionCampaignSeq = "";
var sessionOfficeSeq = "";
var url = "uiController";
var userId = '${param.userId}'
var ssoToken = '${param.ssoToken}'

function loadICLSelector() {
	campaignSeq = document.getElementById("campaignID").options[document.getElementById("campaignID").selectedIndex].value;
	sessionCampaignSeq = campaignSeq;
	var params = {
		pageName :"ICLLeadSheetHistory",
		controlName :"iclSelector",
		campaignSeq :campaignSeq
	}
	
	loadControl(url, params, iclSelectorLoaded);
}
function iclSelectorLoaded(data) {
	$("#iclSelector").children().remove();
	$("#iclSelector").append(data);
	
	sessionOfficeSeq = document.getElementById('iclID').options[document.getElementById('iclID').selectedIndex].value;
	
	$("#iclSelector > table").attr( "align", "left" );
	//$("#iclSelector > table > tbody > tr > td").css( "padding-right", "70px" );
	
	$("#iclID").attr("onchange", "");
	
	<%= "".equals(iclID)? "": "$('#iclID').val('"+iclID+"')" %>

	//sortedColumns = new Array();
	//loadCriteriaComponents();									
	// $('#iclID').select2();
	$('#iclID').css("width", "400px");
	$('#iclID').select2({
		placeholder: "Select a ICL",
		allowClear: true
	});
}

function defaultCallback(data) {
	$('#processing-form').dialog('close');
}

function loadControl(url, parameters, callback, dataType) {
	if (url != undefined && url != null) {
		if (url.indexOf("?") == -1) {
			url = url + "?userId=" + userId + "&ssoToken=" + ssoToken;
		} else {
			url = url + "&userId=" + userId + "&ssoToken=" + ssoToken;
		}
	}

	try {
		var mydate = new Date();
		url = url + "&uniqueTimestamp=" + mydate.getFullYear()
				+ mydate.getMonth() + mydate.getDay() + mydate.getHours()
				+ mydate.getSeconds() + mydate.getUTCMilliseconds();
	} catch (e) {
	}

	try {
		if (sessionCampaignSeq != "") {
			url = url + "&sessionCampaignSeq=" + sessionCampaignSeq;
		}
	} catch (e) {
	}

	try {
		if (sessionOfficeSeq != "") {
			url = url + "&sessionOfficeSeq=" + sessionOfficeSeq;
		}
	} catch (e) {
	}
	try {
		if (sessionOfficeSeq != "") {
			url = url + "&fromPortal=true";
		}
	} catch (e) {
	}

	$('#processing-form').dialog('open');
	if (callback) {
		$.post(url, parameters, function(data) {
			$('#processing-form').dialog('close');
			eval(callback(data));
		});
	} else {
		$.post(url, parameters, defaultCallback);
	}
}

function populateCampaignDiv() {
	params = {
		pageName : "LeadSheetGeneration",
		controlName: "campaignSelector",
		loadTemplate : "false",
		clientKey : "att"
	}
	loadControl(url, params, appendCampaignToPage);
}

function appendCampaignToPage(data){
	$("#campaignDiv").children().remove();
	$("#campaignDiv").append(data);
	
	$("#campaignDiv > table").attr( "align", "left" );	
	
	$($("#campaignDiv > table > tbody tr > td")[0]).attr( "class", "" );
	$($("#campaignDiv > table > tbody tr > td")[1]).attr( "class", "" );
	
	
	
	//$($("#campaignDiv > table > tbody tr > td")[0]).css( "padding-right", "70px" );
	
	$("#campaignID").attr("onchange", "");
	<%= "".equals(campaignID)? "": "$('#campaignID').val('"+campaignID+"')" %>	
	$("#campaignID").attr("onchange", "javascript:loadICLSelector();");
	
	
	$('#campaignID').removeClass("inputControl");
	
	
	$('#campaignID').css("width", "400px");
	$('#campaignID').select2({
		placeholder: "Select a Campaign",
		allowClear: true
	}); 
}

$(document).ready(function(){
	populateCampaignDiv();
	setTimeout(function () {		
		loadICLSelector();		
	}, 450);
	window.parent.autoResize('centerIFrame');
	
});
</script> 

	<form id="searchForm" name="searchForm" method="post">
		<div align="center" >
			<table class="" style="margin: 10px; " >
				<tr>
					<td class="modal-header" ><h5 class="modal-title">Search By:</h5></td>

				</tr>
				<tr>
					<td>
						<table class="simpleTable" style=" padding: 20px; background-color: #fcfcfc;  border-collapse: initial;">
							<tr>
								<td>Filter by Owner:</td>
								<td><input type="text" name="firstName"
									class="centerInTd" placeholder="First Name" value="${param.firstName}" /> <input
									type="text" name="lastName" class="centerInTd"
									placeholder="Last Name " value="${param.lastName}"/></td>
							</tr>
							<tr>
								<td>Filter by Role:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td><select id="role" name="role">
										<option ${ (empty param.role) ? "selected": (param.role == "-1")? "selected": ""} value="-1">-- select --</option>
										<option ${ (param.role == "Application Administrator")? "selected": ""} value="Application Administrator">Application Administrator</option>
										<option ${ (param.role == "Campaign Manager")? "selected": ""} value="Campaign Manager">Campaign Manager</option>
										<option ${ (param.role == "Cydcor Executive")? "selected": ""} value="Cydcor Executive">Cydcor Executive</option>
										<option ${ (param.role == "ICL Owner")? "selected": ""} value="ICL Owner">ICL Owner</option>
										<option ${ (param.role == "Dispositions Manager")? "selected": ""} value="Dispositions Manager">Dispositions Manager</option>
								</select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Application Profile:&nbsp;&nbsp;&nbsp; 
								
								<% 
									String profileName = request.getQueryString().substring(request.getQueryString().indexOf("&profileName=") + "&profileName=".length());
									profileName = profileName.replace("+", " ");
								%>
								<input type="hidden" name="applicationProfile" value="<%=profileName%>" /><%=profileName%>
								<!--  select id="applicationProfile" name="applicationProfile" name="applicationProfile">
										<option ${ (empty param.applicationProfile) ? "selected": (param.applicationProfile == "-1")? "selected": ""} value="-1">-- select --</option>
										<option ${ (param.applicationProfile == "AT&T Leads Profile")? "selected": ""} value="AT&T Leads Profile">AT&T Leads Profile</option>
										<option ${ (param.applicationProfile == "Verizon FIOS Leads Profile")? "selected": ""} value="Verizon FIOS Leads Profile">Verizon FIOS Leads Profile</option>
										<option ${ (param.applicationProfile == "Verizon B2B Leads Profile")? "selected": ""} value="Verizon B2B Leads Profile">Verizon B2B Leads Profile</option>
										<option ${ (param.applicationProfile == "Century Link Leads Profile")? "selected": ""} value="Century Link Leads Profile">Century Link Leads Profile</option>
										<option ${ (param.applicationProfile == "Century L Prism Leads Profile")? "selected": ""} value="Century L Prism Leads Profile">Century L Prism Leads Profile</option>
								</select-->
								</td>
							</tr>

							<tr>
								<td colspan=2 align="left">
								
								<%
								
								//DatabaseResult dbResult = ServiceLocator.getService(PlatformService.class).loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));
								//System.out.println("+++> \n \n " + dbResult.getData());
								
								
								%>	
								<div id="campaignDiv"></div>	
									
									</td>
							</tr>
							<tr>
								<td colspan=2>
								<div id="iclSelector"></div></td>
							</tr>
							<tr>
								<td>
									<input type="hidden"  name="fetchSize" value="all" />
								</td>
								<%--td>Fetch :</td>
								<td><select name="fetchSize">
										<option ${ (empty param.fetchSize) ? "selected": (param.fetchSize == "50")? "selected": ""} value="50">only top 50 matches</option>
										<option ${ (param.fetchSize == "100")? "selected": ""}  value="100">only top 100 matches</option>
										<option ${ (param.fetchSize == "1000")? "selected": ""}  value="1000">only top 1000 matches</option>
										<option ${ (param.fetchSize == "all")? "selected": ""}  value="all">all matching rows</option>
								</select></td--%>
							</tr>
							<tr>
								<td colspan=2 align="center">
									<button type="button" class="btn btn-primary btn-small" onClick="searchClicked()">Search</button>&nbsp;&nbsp;&nbsp;&nbsp;
									<button type="button" class="btn btn-primary btn-small" onClick="populateEdit()">Add New Privilege</button>
									
									</td>
							</tr>
						</table>
						
						<script>
							function searchClicked() {
								$('#processing-form').dialog('open');
								$.post("searchMappingPrivResults.jsp?Search=true", $('#searchForm').serialize(), function(data) {
									$('#processing-form').dialog('close');
									$('#example1').styleListener('destroy');
									$("#searchResultsDiv").children().remove();
									$("#searchResultsDiv").append(data);
								});
							}
						</script>
				</td>
				</tr>
			</table>
			<div id="searchResultsDiv" class="" style="width:1024px; margin-top: 10px">



			</div>
		</div>
	</form>
	
	<div id="createMappingDialog" class="modal fade createMappingDialog large" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	
	</div>

</div>
  <script>
  var profileN = "<%=profileName%>"; 

		function populateEdit(privilegeId, campaignId, iclId,actionType) {
			
			if (!privilegeId) {
				privilegeId = "";
			}
			if (!campaignId) {
				campaignId = "";
			}
			if (!iclId) {
				iclId = "";
			}
			if (!actionType) {
				actionType = "";
			}
			
			$('#processing-form').dialog('open');
			$.get("createUpdatePrivilegeMappings.jsp?actionType="+actionType + "&privilegeId="+privilegeId + "&campaignID="+campaignId+"&iclID="+iclId+"&ProfileName="+profileN, function(data) {
				$('#processing-form').dialog('close');
				$("#createMappingDialog").children().remove();
				$("#createMappingDialog").append(data);
			});
			$('#createMappingDialog').modal('show');
		}	        	
		
  
  </script>
</body>
</html>
