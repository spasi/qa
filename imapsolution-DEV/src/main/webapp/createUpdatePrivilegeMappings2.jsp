<%@page import="java.util.* ,com.cydcor.ext.imap.db.*, com.cydcor.framework.test.* , com.cydcor.framework.model.*, com.cydcor.framework.service.*, com.cydcor.framework.utils.*"%>

<%
	String privilegeId = request.getParameter("privilegeId");
	String campaignID = request.getParameter("campaignID");
	String iclID = request.getParameter("iclID");
	String profileName2 = request.getQueryString().substring(request.getQueryString().indexOf("&ProfileName=") + "&ProfileName=".length());
	System.out.println(profileName2);
	profileName2 = profileName2.replace("%20", " ");
	System.out.println(profileName2);
	
	Properties customProps = new Properties();
	System.out.println("props resource = " + this.getClass().getResourceAsStream("/CustomProps.properties") );
	customProps.load(this.getClass().getResourceAsStream("/CustomProps.properties"));
	
	
	CrmMappingPrivileges cmp = null;
	if (privilegeId != null && (!privilegeId.trim().isEmpty()) ) {
	 	cmp = DBUtil.getCrmMappingPrivilegesForId(request.getParameter("privilegeId"),customProps.getProperty("defaultServer") );
	 	campaignID = cmp.getCampaigns();
	 	iclID = cmp.getOffices();
	} else {
		cmp = new CrmMappingPrivileges();
		cmp.setApplicationProfile(profileName2);
	}
	if (cmp != null) {
		pageContext.setAttribute("cmp", cmp);
 	}
%>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" style="float: right; vertical-align: middle; margin-bottom: 2px;" data-dismiss="modal" class="btn btn-warning btn-small" >Close</button>
			<h4 class="modal-title" id="myModalLabel">Create/Modify Privileges</h4>
		</div>
		<div class="modal-body">
			<form name="saveForm" id="saveForm">
			<table >
				<tr>
					<td>
						<table >
							<tr>
								<td class="fl centerInTdRed" >Privilege ID*</td>
								
								<td class="centerInTd" ><input class="centerInTd" type="text" id="privilegeId"  name="privilegeId"  readonly="readonly" value= '${ (  param.actionType == "clone") ? "" :  cmp.privilegeId }' placeholder="To Be Generated"  /></td>
							</tr>
							<tr>
								<td class="fl centerInTdRed" >Role*</td>
								<td><select class="centerInTd" name="role" >
										<option ${ (empty cmp.role) ? "selected": (cmp.role == "-1")? "selected": ""} value="-1">-- select --</option>
										<option ${ (cmp.role == "Application Administrator")? "selected": ""} value="Application Administrator">Application Administrator</option>
										<option ${ (cmp.role == "Campaign Manager")? "selected": ""} value="Campaign Manager">Campaign Manager</option>
										<option ${ (cmp.role == "Cydcor Executive")? "selected": ""} value="Cydcor Executive">Cydcor Executive</option>
										<option ${ (cmp.role == "ICL Owner")? "selected": ""} value="ICL Owner">ICL Owner</option>
										<option ${ (cmp.role == "Dispositions Manager")? "selected": ""} value="Dispositions Manager">Dispositions Manager</option>
								</select></td>
							</tr>
							<tr>
								<td class="fl centerInTdRed">User Name*</td>
								<td class="fv centerInTd" >
								<div class="controls">
									<div class="input-append">
										<input type="text" class="centerInTd" id="userAlias" name="userAlias" readonly value="${cmp.userAlias}"/>
										<span style="margin-top: 4px; margin-left: -5px" class="add-on"><a href="javascript:selectUserNameClicked()"><i class="icon-search"></i></a></span>
									</div>	
									</div>
								</td>
								
							</tr>
							<tr>
							<td class="fl centerInTdRed" >Application Profile*</td>
							<td>
<!--  							${cmp.applicationProfile}
							<input type="hidden" name="applicationProfile" value="${cmp.applicationProfile}" /> -->
								<select id="applicationProfilePopup" class="centerInTd" name="applicationProfile">
										<option ${ (empty cmp.applicationProfile) ? "selected": (cmp.applicationProfile == "-1")? "selected": ""} value="-1">-- select --</option>
 										<option ${ (cmp.applicationProfile == "AT&T Leads Profile")? "selected": ""} value="AT&T Leads Profile">AT&T Leads Profile</option> 
										<option ${ (cmp.applicationProfile == "Verizon FIOS Leads Profile")? "selected": ""} value="Verizon FIOS Leads Profile">Verizon FIOS Leads Profile</option>
										<option ${ (cmp.applicationProfile == "Verizon B2B Leads Profile")? "selected": ""} value="Verizon B2B Leads Profile">Verizon B2B Leads Profile</option>
										<option ${ (cmp.applicationProfile == "Century Link Leads Profile")? "selected": ""} value="Century Link Leads Profile">Century Link Leads Profile</option>
<%-- 										<option ${ (cmp.applicationProfile == "Century L Prism Leads Profile")? "selected": ""} value="Century L Prism Leads Profile">Century L Prism Leads Profile</option> --%>
								</select>
							</td>
							</tr>
						</table>
					</td>
					<td style="width:50px;">&nbsp;</td>
					<td>
						<table>
							<tr>
								<td>
									<div id="campaignDiv1" style="margin:5px;">									
									</div>									
								</td>
							</tr>
							<tr>
								<td>
									<div id="iclSelector1" style="margin:5px;">									
									</div>
								</td>								
							</tr>
							<tr>
								<td>
									<div id="createdBy" style="margin:10px;">	
									Created By :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${cmp.created}									
									</div>
								</td>								
							</tr>
							<tr>
								<td>
									<div id="modifiedBy" style="margin:10px;">
										Modified By :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${cmp.modified}									
									</div>
								</td>								
							</tr>							
							<tr>
								<td>
									<div id="unknown2" style="margin:5px;">	
										Active : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" data-size="small" id="isActive" name="isActive" value="Y" ${(empty cmp.isActive)?" checked ": (cmp.isActive=='Y')?" checked " : ""}/>	
									</div>
								</td>								
							</tr>
							<tr>
								<td>
									<div id="unknown1" style="margin:5px;">
										&nbsp;									
									</div>
								</td>								
							</tr>
						</table>
					</td>	
				</tr>
			</table>
			</form>
			<script>
			
			function saveClicked() {
				$('#processing-form').dialog('open');
				$.post("insertUpdatePrivMapping.jsp", $('#saveForm').serialize(), function(data) {
					$('#processing-form').dialog('close');
					
					var jsonObj = JSON.parse(data.trim());
					if (jsonObj.errorCode == 0) {
						$("#dbResponseBox").removeClass("alert-error");
						$("#dbResponseBox").addClass("alert-success");
						$("#dbResponseBox").html(jsonObj.message);
						$("#dbResponseBox").show();
						searchClicked();
						setTimeout(function() {
							$("#dbResponseBox").hide();
							$('#createMappingDialog').modal('hide');
						}, 1500);
					} else {
						$("#dbResponseBox").html(jsonObj.message);
						$("#dbResponseBox").show();
						$("#dbResponseBox").removeClass("alert-success");
						$("#dbResponseBox").addClass("alert-error");
					}
					
					
					
					
				});
			}
			
      		function popuplateCampaigns2() {
      			params = {
      					pageName : "LeadSheetGeneration",
      					controlName: "campaignSelector",
      					loadTemplate : "false"
      			}
      			loadControl(url, params, appendCampaignToPage2);
      			
	      		$('#ss').select2({
	      			placeholder: "Type User Name",
	      			allowClear: true
	      		});
	      		
	      	}
      		$('#isActive').bootstrapSwitch();
      		popuplateCampaigns2();
//       		setTimeout(function () {		
//       			loadICLSelector2();		
//       		}, 400);
      		
      		function appendCampaignToPage2(data){
      			$("#campaignDiv1").children().remove();
      			$("#campaignDiv1").append(data);
      			
      			$("#campaignDiv1 > table").attr( "align", "left" );	
      			
      			$($("#campaignDiv1 > table > tbody tr > td")[0]).attr( "class", "" );
      			$($("#campaignDiv1 > table > tbody tr > td")[1]).attr( "class", "" );
      			
      			
      			$($("#campaignDiv1 > table > tbody tr > td")[0]).css( "padding-right", "10px" );
      			
      			$("#campaignDiv1 > table > tbody tr > td > select").attr("id", "campaignID1");
      			$("#campaignDiv1 > table > tbody tr > td > select").attr("name", "campaignID1");
      			
      			$("#campaignID1").attr("onchange", "");
      				
      			$("#campaignID1").attr("onchange", "javascript:loadICLSelector2();");
      			
      			
      			$('#campaignID1').removeClass("inputControl");
      			
      			
      			$('#campaignID1').css("width", "400px");
      			
      			$('#campaignID1').select2({
      				placeholder: "Select a Campaign",
      				allowClear: true
      			}).select2('val', '${cmp.campaigns}');
      			
      			//<%= (campaignID  == null)? "": "$('#campaignID1').val('"+campaignID+"')" %>
          		setTimeout(function () {		
          			loadICLSelector2();		
          		}, 400);
      		}
      		
      		function loadICLSelector2() {
      			var campaignSeq2 = document.getElementById("campaignID1").options[document.getElementById("campaignID1").selectedIndex].value;
      			//sessionCampaignSeq = document.getElementById("campaignID").value;
      			var params = {
      				pageName :"ICLLeadSheetHistory",
      				controlName :"iclSelector",
      				campaignSeq :campaignSeq2
      			}
      			
      			loadControl(url, params, iclSelectorLoaded2);
      		}
      		
      		function iclSelectorLoaded2(data) {
      			$("#iclSelector1").children().remove();
      			$("#iclSelector1").append(data);
      			
      			//sessionOfficeSeq = document.getElementById('iclID').value;
      			
      			$("#iclSelector1 > table").attr( "align", "left" );
      			$("#iclSelector1 > table > tbody > tr > td").css( "padding-right", "7px" );
      			
      			$("#iclSelector1 > table > tbody > tr > td > select").attr("id", "iclID1");
      			$("#iclSelector1 > table > tbody > tr > td > select").attr("name", "iclID1");
      			
      			$("#iclID1").attr("onchange", "");
      			
      			//<%= "".equals(iclID)? "": "$('#iclID1').val('"+iclID+"')" %>

      			
      			$('#iclID1').css("width", "400px");
      			$('#iclID1').select2({
      				placeholder: "Select a ICL",
      				allowClear: true
      			}).select2('val', '${cmp.offices}');
      		}
      		
      		</script>
		</div>
		<div class="modal-footer">
			<span id="dbResponseBox" class="alert alert-success" style="text-align:right; margin-right:300px !important; padding: 2px 10px !important; margin: 0px !important; display:none" ></span>
			<button type="button" class="btn btn-default btn-small" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary btn-small" onClick="javascript:saveClicked()">Save</button>
		</div>
	</div>
</div>



	<div id="selectUserNameDialog" 
		class="modal fade selectUserNameDialog large" 
		role="dialog" aria-labelledby="myLargeModalLabel" 
		aria-hidden="true" 
		>
	
	</div>

  <script>

		function selectUserNameClicked() {
			$('#processing-form').dialog('open');
			$.get("selectUserName.jsp?userName="+$('#userAlias'), function(data) {
				$('#processing-form').dialog('close');
				$("#selectUserNameDialog").children().remove();
				$("#selectUserNameDialog").append(data);
			});
			$('#selectUserNameDialog').css("min-height", "500px");
			$('#selectUserNameDialog').css("min-width", "10%");
			$('#selectUserNameDialog').modal('show');
			
		}	        	
		
  
  </script>