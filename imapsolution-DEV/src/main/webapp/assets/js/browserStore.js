BrowserStore = {
	get : function(id) {
		return document.getElementById(id);
	},

	load : function(key,func) {
		BrowserStore.store.get(key, func);
	},

	save : function(key, value) {
		BrowserStore.store.set(key, value);
	},

	init : function() {
		// create new persistent store
	BrowserStore.store = new Persist.Store('clientStore', {
		swf_path : '../swf/persist.swf'
	});
	// BrowserStore.get('type').innerHTML = Persist.type;
}
};
