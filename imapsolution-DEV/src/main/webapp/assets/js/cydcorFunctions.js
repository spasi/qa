	/**

	 * Default Callback function for all Request
	 * 
	 * @return
	 */
	function defaultCallback(data) {
		$('#processing-form').dialog('close');
	}

	/**
	 * This is generic Function Called by all Guys to Load Components
	 * 
	 * @return
	 */

	function loadControl(url, parameters, callback,progressBarIdx ,dataType) {
		if(progressBarIdx == undefined)
			progressBarIdx = 1;
		if($('#processing-form'+progressBarIdx).dialog('isOpen')) {
			loadControl(url, parameters, callback,progressBarIdx+1);
			return;
		} else {
			$('#processing-form'+progressBarIdx).dialog('open');
		}

		if (url != undefined && url != null) {
			if (url.indexOf("?") == -1) {
				url = url + "?userId=" + userId + "&ssoToken=" + ssoToken;
			} else {
				url = url + "&userId=" + userId + "&ssoToken=" + ssoToken;
			}
		}

		try {
			var mydate = new Date();
			url = url + "&uniqueTimestamp=" + mydate.getFullYear()
					+ mydate.getMonth() + mydate.getDay() + mydate.getHours()
					+ mydate.getSeconds() + mydate.getUTCMilliseconds();
		} catch (e) {
		}

		try {
			if (sessionCampaignSeq != "") {
				url = url + "&sessionCampaignSeq=" + sessionCampaignSeq;
			}
		} catch (e) {
		}

		try {
			if (sessionOfficeSeq != "") {
				url = url + "&sessionOfficeSeq=" + sessionOfficeSeq;
			}
		} catch (e) {
		}
	
		if (callback) {
			$.post(url, parameters, function(data) {
				eval(callback(data));
				$('#processing-form'+progressBarIdx).dialog('close');				
			});
		} else {
			$.post(url, parameters, defaultCallback);
		}
	}

	// For Paginated Table
	function loadPaginatedTable(url, parameters, callback, pageNumber,progressBarIdx) {
		if(progressBarIdx == undefined)
			progressBarIdx = 1;
		if($('#marker-processing-form'+progressBarIdx).dialog('isOpen')) {
			loadPaginatedTable(url, parameters, callback, pageNumber,progressBarIdx+1);
			return;
		} else {
			$('#marker-processing-form'+progressBarIdx).dialog('open');
		}

		if (url != undefined && url != null) {
			if (url.indexOf("?") == -1) {
				url = url + "?userId=" + userId + "&ssoToken=" + ssoToken;
			} else {
				url = url + "&userId=" + userId + "&ssoToken=" + ssoToken;
			}
		}

		if (pageNumber != undefined && pageNumber != null && pageNumber != "") {
			url = url + "&pageNumber=" + pageNumber;
		}else{
			try{
				var controlName = parameters.controlName;
				if(controlName){
					var presentPageNumber = eval(controlName+"PageNumber");
					if(presentPageNumber){
						parameters.pageNumber = presentPageNumber;
					}
				}
			}catch(e){
			}
		}

		try {
			var mydate = new Date();
			url = url + "&uniqueTimestamp=" + mydate.getFullYear()
					+ mydate.getMonth() + mydate.getDay() + mydate.getHours()
					+ mydate.getSeconds() + mydate.getUTCMilliseconds();
		} catch (e) {
		}

		try {
			if (sessionCampaignSeq != "") {
				url = url + "&sessionCampaignSeq=" + sessionCampaignSeq;
			}
		} catch (e) {
		}

		try {
			if (sessionOfficeSeq != "") {
				url = url + "&sessionOfficeSeq=" + sessionOfficeSeq;
			}
		} catch (e) {
		}
		

		

		if (callback) {
			$.post(url, parameters, function(data) {
				eval(callback(data));
				$('#marker-processing-form'+progressBarIdx).dialog('close');				
			});
		} else {
			$.post(url, parameters, defaultCallback);
		}
	}

	/*
	 * 
	 * For Modifiying the processing message to any custom message and to set width
	 * and height
	 * 
	 */
	function changeProcessingModalWindow(message, width, height) {

		if (message) {
			try {
				$("#processing-message").innerHTML = message;
			} catch (e) {
			}
		}
		if (width) {
			try {
				$("#processing-form").dialog('option', "width", width);
			} catch (e) {
			}
		}
		if (height) {
			try {
				$("#processing-form").dialog('option', "height", height);
			} catch (e) {
			}
		}
	}

	function openModal(campaignSeq, officeSeq) {
		try {
			if (territoryID) {
				if (territoryID != "" && territoryID > 0) {
				} else {
					alert("Please Save Parent Territory Before you Create New Child Territory");
					return;
				}
			}
		} catch (e) {
		}

		var params = {
			pageName : "TerritoryManagement",
			loadTemplate : "false",
			campaignSeq : campaignSeq,
			officeSeq : officeSeq
		};
		var url = "uiController";
		$('#territory-form').dialog('open');
		loadControl(url, params, onTerritoryLoaded);
	}

	// This is called when ever territory Modal is loaded
	function onTerritoryLoaded(data) {
		$("#territory-form-message").children().remove();
		$("#territory-form-message").append(data);
	}

	function showMap() {
		clearMap();
		try {
//			polygonControl.tooltip.off();
		} catch (e) {
		}
		var tempMapHolder = $('#tempMapHolder');
		$('#MapHolder').children().appendTo(tempMapHolder);
		$('#map_canvas').show();
		$('#tempMapHolder').show();
		enablePolygonDrawing = false;
		disablePolygonMessage = "";
	}

	function hideMap() {
		useMarkerCluster = true;
		var tempMapHolder = $('#MapHolder');
		$('#tempMapHolder').children().appendTo(tempMapHolder);
		$('#MapHolder').hide();
		if(drawingManager != undefined)
			drawingManager.setMap(null);
}

	function onPageLoaded(data) {
		onMarkersLoadedAndParsed = function() {
		};

		onKmlResponseParsed = function () {
		};
		
		 onCustomZoomFunction  = function () {
			}; 
			
		useMarkerCluster = false;
		
		try{
			geometryControls.resetBounds();
		}catch(e){
		}
		appendToPage(data);
	}

	function loadICLTerritories() {
		$.post("Controller?pageName=campaign/TerritoryDefTables.html", {
			action : "loadTerritoryData",
			officeSeq : officeSeq
		}, function(data) {
			$("#data").remove();
			$("#def2").append(data);

		});
		clearPolygons();
		loadKML("mapServlet?action=loadPolygons&officeSeq=" + officeSeq);
	}

	function loadCampiagnICLData(selectBox) {

		var campaignSeq = selectBox.value;
		$.post("Controller?pageName=campaign/ICLTables.html", {
			action : "loadCampiagnICLData",
			iclSeq : campaignSeq
		}, function(data) {
			$("#data").remove();
			$("#def").append(data);

		});
		clearMarkers();
		loadKML("mapServlet?action=loadPoints&cSeq=" + campaignSeq);

	}
	function loadAssignedLeads(selectBox) {
		var campaignSeq = selectBox.value;
		alert(campaignSeq);
		$.post("Controller?pageName=campaign/LeadAssignTables.html", {
			action : "loadAssignedLeads",
			iclSeq : campaignSeq
		}, function(data) {
			alert(data);
			$("#data").remove();
			$("#def3").append(data);

		});
	}

	function loadTerritories() {

		$.post("Controller?pageName=campaign/TerritoryDefTables.html", {
			action : "loadTerritories"

		}, function(data) {
			$("#data").remove();
			$("#def2").append(data);

		});
	}

	function getUrlParamvalue(paramName) {
		paramName = paramName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + paramName + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if (results == null)
			return "";
		else
			return results[1];
	}

	function displayCounties() {
		var stateName = document.getElementById("states").value;
		$.post("Controller?pageName=campaign/counties.xml", {
			action : "displayCounties",
			officeSeq : officeSeq,
			showStates : true,
			stateName : stateName
		}, function(data) {
			$("#counties").remove();
			$("#countyHolder").append(data);

		});
	}

	function SaveCounties() {
		var countyKey = document.getElementById("counties").value;
		$.post("Controller?pageName=campaign/TerritoryDefTables.html", {
			action : "saveIclTerritory",
			officeSeq : officeSeq,
			countyKey : countyKey
		}, function(data) {
			parent.opener.loadICLTerritories();
			window.close();

		});
	}

	var params = {
		pageName : "Homepage",
		loadTemplate : "false"
	}
	var url = "uiController";

	// All the page load Functions reside here

	function resetGLSVariables(){
		 selectedUploadFiles = new Array();
		 generatedPageNumber = '';
		 savedPageNumber ='';
		 enableGlsMapZoom = false;
		 uploadedFilesPageNumber = '';
		 glsMapZoom ='';
		 glsMapCenter='' ;
		 glsMapType ='';
	}

	function gotoHomePage() {
	resetGLSVariables();	
		params = {
			pageName : "HomePage",
			loadTemplate : "false"
		}
		url = "uiController";
		hideMap();
		loadControl(url, params, onPageLoaded);
	}

	function gotoCampaignTerritory() {
		hideMap();
		params = {
			pageName : "ICLAdmin",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	function goToICLManagement() {
		hideMap();
		params = {
			pageName : "RepManagement",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	function goToICLLeadSheetHistory() {
		hideMap();
		params = {
			pageName : "ICLLeadSheetHistory",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	function goToCampaignLeadAssignment() {
		hideMap();
		params = {
			pageName : "ICLTerritoryManagement",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	function goToLeadAssignment() {
		hideMap();
		params = {
			pageName : "ICLTemplateCreation",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	function goToICLRepManagement() {
		hideMap();
		params = {
			pageName : "RepManagement",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	// Call back function when Campaign is loaded
	function goToCampaignAdmin() {
		hideMap();
		params = {
			pageName : "CampaignAdmin",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}
	// Call back function when View Lead Sheet button is pressed
	function goToLeadSheetAdmin(leadSheetid, type, readOnlyUser) {

		enableGlsMapZoom = true;
		if(typeof leadSheetSavedtablePageNumber != 'undefined' )
			savedPageNumber = leadSheetSavedtablePageNumber;
		if(typeof availableLeadsDataPageNumber != 'undefined' )
			uploadedFilesPageNumber = availableLeadsDataPageNumber;
		if(typeof leadSheetGeneratedtablePageNumber != 'undefined' )
			generatedPageNumber = leadSheetGeneratedtablePageNumber;
		if(typeof leadSheetAssignmentHistoryTablePageNumber != 'undefined' )
			historyPageNumber = leadSheetAssignmentHistoryTablePageNumber;
		if(typeof map != 'undefined' ){
			glsMapCenter = map.getCenter();
			// Map Zoom Level
			glsMapZoom = map.getZoom();
			// Map Type
            glsMapType = map.getMapTypeId();
        }
		var k=0;
		selectedUploadFiles = new Array();
		for (i = 0; i < document.getElementsByName('selectedFiles').length; i++){
			  if(document.getElementsByName('selectedFiles')[i].checked){
				  selectedUploadFiles[k] = document.getElementsByName('selectedFiles')[i].value;
				  k++
			  }
		}

		var leadSheetID = leadSheetid;
		var url = "uiController";
		if (type == "Saved") {
			var leadSheetURL = url
					+ "?pageName=LeadSheet&disableBackLinks=true&leadSheetId="
					+ leadSheetID + "&popup=true&type=" + type+"&readOnlyUser="+readOnlyUser+"&campaignSeq="+sessionCampaignSeq+"&officeSeq="+sessionOfficeSeq;
			window
					.open(
							leadSheetURL,
							"Leadsheet",
							"height=850,width=1150,status=yes,toolbar=no,menubar=no,scrollbars=yes,location=no");
		} else {
			onMarkersLoadedAndParsed = function() {
			};

			onKmlResponseParsed = function () {
			};
			params = {
				pageName : "LeadSheet",
				leadSheetId : leadSheetID,
				campaignSeq : sessionCampaignSeq,
				officeSeq : sessionOfficeSeq,
				loadTemplate : "false",
				type : type
			}
			hideMap();
			loadControl(url, params, onPageLoaded);
		}
	}

	function goToLeadSheetGeneration() {
		hideMap();
		params = {
			pageName : "LeadSheetGeneration",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	function goToGenericRuleTemplate() {
		hideMap();
		params = {
			pageName : "GenericRuleTemplate",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	function gotoICLTerritoryMgmtB2B(territoryID) {
		var url = "uiController";
		params = {
			pageName : "ICLTerritoryManagementB2B",
			controlName : "territoryDefSelector",
			territoryID : territoryID
		}
		loadControl(url, params, ICLTerritoryMgmtB2BLoaded);
	}
	/**
	 * 
	 * @return
	 */
	function getInputTagValuesAsParams() {

		var paramString = "";

		$('input').each(function() {
			var type = this.type;
			var tag = this.tagName.toLowerCase(); // normalize case
				// it's ok to reset the value attr of text inputs,
				// password inputs, and textareas
				if (type == 'hidden' || type == 'text') {
					paramString = paramString + "&" + this.name + "=" + this.value;
					// alert(params.lenth);
				}

				// checkboxes and radios need to have their checked state cleared
				// but should not have their 'value' changed
				else if (type == 'checkbox' || type == 'radio') {
					paramString = paramString + "&" + this.name + "="
							+ this.checked;
					// alert(params.lenth);
				}

			});

		return paramString;
	}
	function goToDefaultTerritoryViewSetUp() {
		hideMap();
		params = {
			pageName : "DefaultTerritoryViewSetup",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	function disablePolygonDrawing() {
        PolygonLeadsArray = new Array();
        enablePolygonDrawing = false;
	}

	function startPolygonDrawing() {
		enablePolygonDrawing = true;
	}

	function goToDispositionManagement() {
		hideMap();
		params = {
			pageName : "DispositionManagement",
			loadTemplate : "false"
		}
		loadControl(url, params, onPageLoaded);
	}

	function deleteConfirmation(message, callBackFunction) {
		var answer = false;
		if (message) {
			message = message;
		} else {
			message = "Are you sure you want to delete?"
		}

		answer = confirm(message);

		if (answer) {
			eval(callBackFunction);
			//alert("Done!!!");
		}

	}


	String.prototype.replaceAll = function(stringToFind,stringToReplace){
		var temp = this;
		var index = temp.indexOf(stringToFind);
			while(index != -1){
				temp = temp.replace(stringToFind,stringToReplace);
				index = temp.indexOf(stringToFind);
			}
			return temp;
	}
	function getSortingColumn(tableName){

		for(var i =0;i<sortedColumns.length;i++){
			if(sortedColumns[i].indexOf(tableName) != -1){
				return sortedColumns[i].replace(tableName+'_','');
			}
		}
		return '';
		
	}

	function sortColumn(column,tableName,sortedColumns){
		if(column!=null){
			var index = 0;
			var found = false;
			for(var i =0;i<sortedColumns.length;i++){
				if(sortedColumns[i].indexOf(tableName) != -1){
					found = true;
					index = i;
					break;
				}
			}
			if(!found || sortedColumns.length==0){
				if(sortedColumns.length<1){
					sortedColumns[0] = column.id+':ASC';
					index = 0;
				}else {
					sortedColumns[sortedColumns.length] = column.id+':ASC';
					index = sortedColumns.length;	
				}
			}
			var name = sortedColumns[index].replace(tableName+'_','');
			var columnName = name.split(':')[0];
			var returnVal ='';
			var currentColumn = column.id.replace(tableName+'_','');
			var order = 'ASC';
			var columnChanged = false;
			if( currentColumn != columnName){
				columnChanged = true;
			}
			if(!found || !(currentColumn==columnName)){
				returnVal = currentColumn+' '+order+":"+columnChanged;
			}else{
				if(name.split(':')[1] == 'ASC'){
					order = 'DESC';
				}else if(name.split(':')[1] == 'DESC'){
					order = 'ASC';
				}else{
					order = 'ASC';
				}
				returnVal = currentColumn+' '+order+":"+columnChanged;
			}
			sortedColumns[index] = column.id+':'+order;
			return returnVal;
		}
	}


	function setSortOrderImage(tableName,sortedColumns){
		var spanId ='';
		var index =-1;
		for(var i =0;i<sortedColumns.length;i++){
			if(sortedColumns[i].indexOf(tableName) != -1){
				 index = i;
				 break;
			}
		}
		var error = false;
		try {
			eval(tableName+'sortingColumn'); 
		}catch(e) {
			error = true;
		}
		if(!error) {
		if(index == -1){
			index = sortedColumns.length;
			sortedColumns[index] = tableName+'_'+(eval(tableName+'sortingColumn')).replace(' ',':');
		}
		var img = '';
		if(sortedColumns[index].indexOf(":ASC") != -1){
			img = "assets/images/sort_asc.png" 
		}else {
			img = "assets/images/sort_desc.png"
		}
		 spanId = 'span_'+sortedColumns[index].split(':')[0];
		$('#'+spanId).attr('innerHTML','&nbsp;<img src="'+img+'"/>');
		}
	}

	function goToLeadManagement() {
			params = {
				pageName : "LeadManagement",
				loadTemplate : "false"
			}
			url = "uiController";
			hideMap();
			loadControl(url, params, onPageLoaded);
		}

	function goToReport1(){
			params = {
				pageName : "Report",
				loadTemplate : "false",
				pageDefination : "Report1Def.html"
					
			}
			url = "uiController";
			hideMap();
			loadControl(url, params, onPageLoaded);
		}

	function goToReport2(){
		params = {
			pageName : "Report",
			loadTemplate : "false",
			pageDefination : "Report2Def.html"
				
		}
		url = "uiController";
		hideMap();
		loadControl(url, params, onPageLoaded);
	}

	function goToReport3(){
		params = {
			pageName : "Report",
			loadTemplate : "false",
			pageDefination : "Report3Def.html"
				
		}
		url = "uiController";
		hideMap();
		loadControl(url, params, onPageLoaded);
	}
	
	function goToExportDispositions() {
		params = {
				pageName : "ExportDispositionLeads",
				loadTemplate : "false",
				pageDefination : "ExportDispositionLeadsDef.html"
					
			}
			url = "uiController";
			hideMap();
			loadControl(url, params, onPageLoaded);

	}
	
	function cancelEditLeadSheet(){
		$('#mask, .window').hide();
		$('#modalWindow').children().remove();
	}
	
	function openModalWindow(rowId,isHistPage) {
		//Get the A tag
		var id = '#modalWindow';
	
		//Get the screen height and width
		var maskHeight = $(document).height()-21;
		var maskWidth = $(window).width();
	
		//Set height and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		var url = "uiController";

		var params = {
				pageName :"LeadSheetGeneration",
				controlName :"getModalWindow",
				officeSeq : document.getElementById("iclID").value
			};

		$.post(url, params, function(data) {
			$(id).children().remove();
			$(id).append(data);

			 var today = new Date();
				
				var sDate = today.getDate()+'';
				var sMonth = (today.getMonth()+1)+'';
				var sYear = today.getFullYear();
	
		   		if(sDate.length < 2){
					sDate = '0'+sDate;
				}
				if(sMonth.length < 2){
					sMonth = '0'+sMonth;
				}
				
				var aD = $('#td_REP_ASSIGNED_DATE_'+rowId).html();
				if($('#td_REP_ASSIGNED_DATE_'+rowId).html() == null || $('#td_REP_ASSIGNED_DATE_'+rowId).html() == '')
					aD = sMonth+'/'+sDate+'/'+sYear;

			$('#rowId').html(rowId);
			$('#editSheetName').html($('#td_LEADSHEET_NAME_'+rowId).html());
			$('#editCreatedDate').html($('#td_CREATED_DATE_'+rowId).html());
			$('#editNol').html($('#td_NUMBER_OF_LEADS_'+rowId).html());
			$('#editAssignedDate').html(aD);
			if(isHistPage)
				$('#editRepAssigned').html($('#td_REP_NAME_'+rowId).html());
			$('#editStatus').html($('#td_STATUS_'+rowId).html());
			$("#editReturnedDate").val($('#td_rd_'+rowId).html());
			$("#editSales").val($('#td_s_'+rowId).html());
			$("#editDM").val($('#td_dm_'+rowId).html());
			$("#editDK").val($('#td_dk_'+rowId).html());
			$("#editNotes").html($('#td_nts_'+rowId).html());
			if(isHistPage){
				$("#tr_editDK").hide();
			}else{
				$("#tr_editStatus").hide();
			}

 });
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = document.documentElement.scrollTop;
		var winW = document.body.clientWidth;
		//Set the popup window to center
		$(id).css('top',  winH+$(id).height()/2);
		$(id).css('left', winW/4+$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(2000);
		$("#editRepAssigned").focus(); 
	}

	//if close button is clicked
	function modalWindow1() {
		$('#mask, .window').hide();
	}


    function bindEscape(){
        jQuery(document).bind('keydown', 'esc',function (evt){
        	 evt.stopPropagation( );  
        	 evt.preventDefault( );
        	return false;
        });
    }
    
    function unBindEscape(){
        jQuery(document).unbind('keydown', 'esc');
    }
    
    function setSelectTagOptionByText(id,value){
    	var options = document.getElementById(id);
    	if(options){
    		for(var i=0;i<options.length;i++){
    			var option = options[i];
    			if(option.text == value){
    				option.selected = true;
    				break;
    			}
    		}
    	}
    	return;
    }