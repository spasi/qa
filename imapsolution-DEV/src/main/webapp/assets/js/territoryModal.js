/*
 * @author Ashwin
 */

function activateModalDialog() {
	$('#territory-form input.territory, #territory-form a.territory').click(function (e) {
		e.preventDefault();
		// load the territory form using ajax
		
		var params = {
			pageName :"TerritoryManagement",
			loadTemplate : "false"
		}
		var url = "uiController";
		loadControl(url, params,onTerritoryLoaded);
			
		
	});

	// preload images
	var img = ['cancel.png', 'form_bottom.gif', 'form_top.gif', 'loading.gif', 'send.png'];
	$(img).each(function () {
		var i = new Image();
		i.src = 'assets/images/' + this;
	});
};

var territory = {
	message: null,
	open: function (dialog) {
		// add padding to the buttons in firefox/mozilla
		if ($.browser.mozilla) {
			$('#territory-container .territory-button').css({
				'padding-bottom': '2px'
			});
		}
		// input field font size
		if ($.browser.safari) {
			$('#territory-container .territory-input').css({
				'font-size': '.9em'
			});
		}

		// dynamically determine height
		var h = 180;


		var title = $('#territory-container .territory-title').html();
		$('#territory-container .territory-title').html('Loading...');
		dialog.overlay.fadeIn(200, function () {
			dialog.container.fadeIn(200, function () {
				dialog.data.fadeIn(200, function () {
					$('#territory-container .territory-content').animate({
						height: h
					}, function () {
						$('#territory-container .territory-title').html(title);
						$('#territory-container form').fadeIn(200, function () {
							$('#territory-container #territory-name').focus();

							// fix png's for IE 6
							if ($.browser.msie && $.browser.version < 7) {
								$('#territory-container .territory-button').each(function () {
									if ($(this).css('backgroundImage').match(/^url[("']+(.*\.png)[)"']+$/i)) {
										var src = RegExp.$1;
										$(this).css({
											backgroundImage: 'none',
											filter: 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' +  src + '", sizingMethod="crop")'
										});
									}
								});
							}
						});
					});
				});
			});
		});
	},
	show: function (dialog) {
		$('#territory-container .territory-send').click(function (e) {
			e.preventDefault();
			// validate form
			if (territory.validate()) {
				var msg = $('#territory-container .territory-message');
				msg.fadeOut(function () {
					msg.removeClass('territory-error').empty();
				});
				$('#territory-container .territory-title').html('Sending...');
				$('#territory-container form').fadeOut(200);
				$('#territory-container .territory-content').animate({
					height: '80px'
				}, function () {
					$('#territory-container .territory-loading').fadeIn(200, function () {
						mapRepTerritory();
					});
				});
			}
			else {
				if ($('#territory-container .territory-message:visible').length > 0) {
					var msg = $('#territory-container .territory-message div');
					msg.fadeOut(200, function () {
						msg.empty();
						territory.showError();
						msg.fadeIn(200);
					});
				}
				else {
					$('#territory-container .territory-message').animate({
						height: '30px'
					}, territory.showError);
				}
				
			}
		});
	},
	close: function (dialog) {
		$('#territory-container .territory-message').fadeOut();
		$('#territory-container .territory-title').html('Goodbye...');
		$('#territory-container form').fadeOut(200);
		$('#territory-container .territory-content').animate({
			height: 40
		}, function () {
			dialog.data.fadeOut(200, function () {
				dialog.container.fadeOut(200, function () {
					dialog.overlay.fadeOut(200, function () {
						try{
							$.modal.close();
						}catch(e){
							
						}
					});
				});
			});
		});
	},
	error: function (xhr) {
		alert(xhr.statusText);
	},
	validate: function () {
		return true;
	},
	showError: function () {
		$('#territory-container .territory-message')
			.html($('<div class="territory-error"></div>').append(territory.message))
			.fadeIn(200);
	}
};