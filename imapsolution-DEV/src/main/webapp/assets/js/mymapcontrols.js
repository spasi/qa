function deletePolygon(type, tName) {

	try {
		if (!territoryID) {
			territoryID = -2;
		}
	} catch (e) {
		territoryID = -2;
	}
	
	if (territoryID) {
		if (territoryID != "" && territoryID > 0) {
		} else {
			alert("Please Delete it from edit territory details page");
			return;
		}
	}

	if (type === "polygon" || type === "polyline") {
		$.post("mapServlet", {
			action : "delete",
			territoryName : tName,
			territoryID : territoryID
		}, function(data) {
			alert("Successfully Deleted");
			refreshTerritory();
		});
	}
}

function savePolygon(record) {
	if (territoryPageName != "ICL_TERRITORY") {
		return;
	}

	if (!territoryID) {
		territoryID = -2;
	}

	if (territoryID) {
		if (territoryID != "" && territoryID > 0) {
		} else {
			try{
				alert("Please Save Parent Territory Before you save custom Polygon");
				$("#territory-form").dialog( 'close' );
				return;
			}catch(e){
			}
		}
	}

	// return;
	if (record.type === "polygon" || record.type === "polyline") {
		var recordJSON = {};
		recordJSON.type = record.type;
		recordJSON.coordinates = [];
		var coordinates = '';
		// determine geometry type, and copy geometry appropriately
		var vertex;
		for ( var i = 0; i < record.geometry.getVertexCount(); i++) {
			vertex = record.geometry.getVertex(i);
			coordinates = coordinates + vertex.lng() + " " + vertex.lat() + ",";
			recordJSON.coordinates.push( {
				lat : vertex.lat(),
				lng : vertex.lng()
			});
		}

		// add title and description
		recordJSON.title = record.title[0];
		recordJSON.description = record.description[0];
		recordJSON.style = {};
		recordJSON.style.line = {};
		if (record.type === "polygon") {
			recordJSON.style.color = record.geometry.savedStyle.fillColor
					.replace('#', '');
			recordJSON.style.fillOpacity = record.geometry.savedStyle.fillOpacity;
			recordJSON.style.outline = record.geometry.outline;
		}
		// recordJSON.style.fill = record.geometry.fill.replace('#', '');

		recordJSON.style.line.color = record.geometry.savedStyle.strokeColor
				.replace('#', '');
		recordJSON.style.line.width = record.geometry.savedStyle.strokeWeight;
		recordJSON.style.line.opacity = record.geometry.savedStyle.strokeOpacity;
		var url = "mapServlet?action=save&title=" + recordJSON.title + "&desc="
				+ recordJSON.description + "&styleColor="
				+ recordJSON.style.color + "&styleFill="
				+ recordJSON.style.fill + "&styleOutline="
				+ recordJSON.style.outline + "&styleFillOpacity="
				+ recordJSON.style.fillOpacity + "&styleLineColor="
				+ recordJSON.style.line.color + "&styleLineWidth="
				+ recordJSON.style.line.width + "&styleLineOpacity="
				+ recordJSON.style.line.opacity + "&officeSeq=" + officeSeq;

		if (!campaignSeq) {
			campaignSeq = "";
		}

		if (!officeSeq) {
			officeSeq = "";
		}

		if (!territoryFor) {
			territoryFor = "";
		}

		$.post(url, {
			action : "save",
			campaignSeq : campaignSeq,
			officeSeq : officeSeq,
			territoryFor : territoryFor,
			coordinates : coordinates,
			territoryPageName : territoryPageName,
			territoryID : territoryID
		}, function(data) {
			alert(data);
			
			try{
				refreshTerritory();
			}catch(e){
			}
			
			try{
				refreshChildTerritories();
			}catch(e){
			}
		});
	}

}


function resetLeadCount(){
	
	for(var i=0;i<polygonControl.storage.length;i++){
		try{
			var polygon = polygonControl.storage[i].geometry;
			if(polygon && polygon != null){
				var title = polygonControl.storage[i].title[0];
				if(title== ""){
					updatePoints(polygon);
				}
			}
		}catch(e){
		}
	}
}


// Update all markers with in poly status
function updatePoints(polygon) {
	
	
//	try{
//		var title = polygonControl.storage[polygon.index].title[0];
//		var desc = polygonControl.storage[polygon.index].description[0];
//		desc = desc.replace("TerritoryType :", "");
//		polygonControl.tooltip.on(desc+" : "+title);
//		if(desc.length > 0){
//			return;
//		}
//	}catch(e){
//		// No leadsheet name defined
//	}	
		
	var iconIncluded = 'assets/images/mapicons/man.png';
	var iconExcluded = 'assets/images/mapicons/red-dot.png';
	// me.storage[marker.index].title
	var leadCount = 0;
	var leads = '';
	if(markerCluster){
	var allClusters = markerCluster.getClusters();
	
	// Iterate over all Marker Clusters
	for(var i=0;i<allClusters.length;i++){
		//var uiRepresentation = allClusters[i].getUIRepresentation();
		try{
//			if(!uiRepresentation && uiRepresentation == null){
				var clusterMarkers = allClusters[i].getMarkers();
				
				for(var j=0;j<clusterMarkers.length;j++){
					var pointMarker = clusterMarkers[j].getPosition();
					var inPolyMarker = containsLatLng(polygon, pointMarker);
		
					if (inPolyMarker) {
						leads = leads + ',' + clusterMarkers[j].getTitle()+":POINT ("+clusterMarkers[j].getPosition().lng()+" "+clusterMarkers[j].getPosition().lat()+")";
						leadCount = leadCount + 1;
					}
				}				
//			}else{
//				var point = uiRepresentation.getLatLng();
//				var inPoly = polygon.containsLatLng(point);
//
//				if (inPoly) {
//					var clusterMarkers = allClusters[i].getMarkers();
//					
//					for(var j=0;j<clusterMarkers.length;j++){
//						var isOverLayed = false;
//						
//						try{
//							isOverLayed = clusterMarkers[j].isOverLayed;
//						}catch(e){
//						}
//						
//						if(!isOverLayed){
//							leads = leads + ',' + clusterMarkers[j].marker.getTitle()+":POINT ("+clusterMarkers[j].marker.getLatLng().lng()+" "+clusterMarkers[j].marker.getLatLng().lat()+")";
//							leadCount = leadCount + 1;
//						}
//						try{
//							if(!clusterMarkers[j].marker.isHidden()){}
//								//clusterMarkers[j].setImage(iconIncluded);
//						}catch(e){
//						}	
//							
//					}
//				}
//			}
		}catch(e){
			continue;
		}
	}
	
	// Now count Left over markers
	
//	var leftOverMarkers = markerCluster.getLeftOverMarkers();
//	
//	for ( var i = 0; i < leftOverMarkers.length; i++) {
//		var marker = leftOverMarkers[i];
//		var point = marker.getLatLng();
//		var inPoly = polygon.containsLatLng(point);
//
//		if (inPoly) {
//			leads = leads + ',' + leftOverMarkers[i].getTitle()+":POINT ("+leftOverMarkers[i].getLatLng().lng()+" "+leftOverMarkers[i].getLatLng().lat()+")";
//			leadCount = leadCount + 1;
//		}
//
//	}	
//	

	// alert("Update Points Called with Lead COunt :"+leadCount);
//	try {
//		polygonControl.tooltip.tooltipContainer.innerHTML = "Lead Count:"
//				+ leadCount;
//		polygonControl.tooltip.on("Lead Count:" + leadCount);
//	} catch (e) {
//	}


	PolygonLeadsArray[PolygonLeadsArray.length] = leads.substring(1, leads.length);
	// alert("Leads :"+JSON.stringify(PolygonLeadsArray));
	}
		var infowindow = new google.maps.InfoWindow({
	    content: '<div style="white-space:nowrap">'+"Lead Count:" + (leads.length>2?leads.substring(1, leads.length).split(",").length:0)+'</div>'
	});

	google.maps.event.addListener(polygon, 'mouseover', function(event) {
		try{
		  tooltip.show('Test tip');
		  infowindow.setPosition(event.latLng);
		  infowindow.open(map);
		}catch(e){
			
		}
		});
	google.maps.event.addListener(polygon, 'mouseout', function(event) {
		try{
			tooltip.hide();
		  infowindow.close();
		}catch(e){
			
		}

		});

}

function updateAllPolygonsLeads(){
	PolygonLeadsArray = [];
	for(var i=0; i< drawnPolygonsArray.length;i++){
		var p =  drawnPolygonsArray[i];
		updatePoints(p);
	}
}


function showLeadCount(polygon){
	// alert("Update Points Called with Lead COunt :"+leadCount);
	// If no Leads Found
	try{
		var title = polygonControl.storage[polygon.index].title[0];
		var desc = polygonControl.storage[polygon.index].description[0];
		if(desc.length > 0){
			desc = desc.replace("TerritoryType:", "");
			polygonControl.tooltip.on(desc+" : "+title);
		}else{
			try {
				if(PolygonLeadsArray[polygon.index] == ""){
					polygonControl.tooltip.tooltipContainer.innerHTML = "Lead Count:0";
					polygonControl.tooltip.on("Lead Count:0");
				}else{
					polygonControl.tooltip.tooltipContainer.innerHTML = "Lead Count:"
						+ PolygonLeadsArray[polygon.index].split(",").length;
					polygonControl.tooltip.on("Lead Count:" + PolygonLeadsArray[polygon.index].split(",").length);
				}

			} catch (e) {
				polygonControl.tooltip.off();		
			}			
		}
		
	}catch(e1){
		// No leadsheet name defined
		polygonControl.tooltip.off();
	}
	


	
}
