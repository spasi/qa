var lwbCode = "";

/**
 * This function is a callback provided to browser store to get called when data
 * is loaded. This is called only once in page on page load
 * 
 * @param success
 * @param value
 * @return
 */
function onLWBCodeLoad(success, value) {
	if (success) {
		lwbCode = value;
		// alert(lwbCode);
		if (lwbCode == null || lwbCode == '') {
			alert("You are not logged in!! Redirecting to login Page");
			window.location.href = getBaseUrlByParse(window.location.href);
		} else {
			$(document).ready(function() {
				// On Page load Iterate over all Links and add lwbCode so that
					// all Request after login have the code with them
					$("a").each(function() {
						var href = $(this).attr("href");
						if (href != undefined) {
							var questonIndex = href.indexOf("?");
							if (questonIndex != -1) {
								href = href + "&lwbCode=test";
							} else {
								href = href + "?lwbCode=test";
							}
							$(this).attr("href", href);
						}

					});

				});
		}
	} else {
		alert("You are not logged in!! Redirecting to login Page");
		window.location.href = getBaseUrlByParse(window.location.href);
	}
}

/*
 * This function parses the URL on the window.location and then returns if page
 * is login Page or not of it
 */
function isLoginPage(url) {
	/*
	 * http://localhost:9090/linkwithweb-webapp/Controller?pageLink=login.html
	 */
	try {
		var urlArray = url.split("/");
		if (urlArray[3] == 'login.jsp' || urlArray[4] == 'login.jsp') {
			return true;
		}
	} catch (e) {
		return false;
	}

	return false;
}

/*
 * This function parses the URL on the window.location and then returns the base
 * of it
 */
function getBaseUrlByParse(url) {
	/*
	 * http://localhost:9090/linkwithweb-webapp/Controller?pageLink=login.html
	 * Find the word between 2nd and 3rd slash. If it is linkwithweb then base
	 * url is constructed till this one. Else base URL is till 2nd slash
	 */
	var baseUrl = "http://";
	var urlArray = url.split("/");
	baseUrl = baseUrl + urlArray[2] + "/" + urlArray[3] + "/login.jsp";

	return baseUrl;
}

BrowserStore.init();

// IF it is login page then you dont need to redirect to login again else check
// for lwbCode and if code dosen't exist redirect
if (isLoginPage(window.location.href)) {
	// alert("Not Login Page");
} else {
	BrowserStore.load("lwbCode", onLWBCodeLoad);
}
// BrowserStore.save("test", "ashwin");
// BrowserStore.load("test", onDataLoad);

