package com.cydcor.integration.privilege;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.MerCampignDetails;
import com.cydcor.framework.model.MerICLDetails;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.ws.GenericWS;
import com.cydcor.integration.privilege.utils.MappingPrivilegeVO;
import com.cydcor.ws.WSMappingInterface;
import com.cydcor.ws.stubs.privillege.CustomObject6Data;
import com.cydcor.ws.stubs.privillege.CustomObject6Query;
import com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Input;
import com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output;
import com.cydcor.ws.stubs.privillege.CustomObject6_ServiceLocator;
import com.cydcor.ws.stubs.privillege.ListOfCustomObject6Query;
import com.cydcor.ws.stubs.privillege.QueryType;
import com.serene.crmod.service.impl.CRMODServiceDaoImpl;

public class RetrieveMappingPrivilegeFromCRM extends GenericWS implements WSMappingInterface {

	protected final Log logger = LogFactory.getLog(getClass());

	@SuppressWarnings("unchecked")
	public Map<String, Map<String, RoleObject>> call(String userId) throws Exception {
		RoleObject roleObject = null;
		Map<String, RoleObject> roleSpecificMap = null;
		Map<String, Map<String, RoleObject>> roleObjMap = new HashMap<String, Map<String, RoleObject>>();
	
		CRMODServiceDaoImpl crmodServiceDao = new CRMODServiceDaoImpl();
		List<MappingPrivilegeVO> listCustomObject6Data = new ArrayList<MappingPrivilegeVO>();
		MappingPrivilegeVO customObject6VO = new MappingPrivilegeVO();
		
		customObject6VO.setPageSize(100);
		customObject6VO.setStartRowNum(0);
		//customObject6VO.setSearchSpec("[OwnerUserSignInId]='"+userId+"'");
		customObject6VO.setOwnerUserSignInId("='"+userId+"'");

		try {
			listCustomObject6Data = crmodServiceDao.query(customObject6VO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for (MappingPrivilegeVO object : listCustomObject6Data) {

			roleSpecificMap = roleObjMap.get(object.getOwnerUserSignInId());
			if (roleSpecificMap == null) {
				roleSpecificMap = new HashMap<String, RoleObject>();
				roleObjMap.put(object.getOwnerUserSignInId().toString(), roleSpecificMap);
			}

			roleObject = roleSpecificMap.get(object.getIndexedPick0());
			if (roleObject == null) {
				roleObject = new RoleObject();
				roleObject.setOwnerUserSignInId(object.getOwnerUserSignInId());
				roleObject.setOwnerFirstName(object.getOwnerFirstName());
				roleObject.setOwnerLastName(object.getOwnerLastName());
				roleObject.setOwnerFullName(object.getOwnerFullName());
				roleObject.setOwnerEmailAddress(object.getOwnerEMailAddr());
				roleObject.setOwnerIntegrationId(object.getOwnerIntegrationId());
				roleObject.setOwnerAlias(object.getOwnerAlias());
				roleObject.setRoleName(object.getIndexedPick0());
				roleObject.setMerlinICL(new ArrayList<MerICLDetails>());
				roleObject.setMerlinCampaign(new ArrayList<MerCampignDetails>());
				roleSpecificMap.put(object.getIndexedPick0(), roleObject);
			}
			if (roleObject != null)
				roleObject.addProfileNames(object.getIndexedPick1());
			
			MerICLDetails iclDetails = new MerICLDetails();
			iclDetails.setOffice_seq(object.getCustomObject4ExternalSystemId());
			roleObject.getMerlinICL().add(iclDetails);

			MerCampignDetails campaignObjs = new MerCampignDetails();
			campaignObjs.setCampaign_seq(object.getAccountExternalSystemId());
			roleObject.getMerlinCampaign().add(campaignObjs);
		}
		logger.info("END TIME OF RetrieveMappingPrivilege ::" + new Date(System.currentTimeMillis()));
		logger.debug("Returning roleObject Map instance");
		
		return roleObjMap;
	}
	public List<CustomObject6Data> searchByUserId(CustomObject6_ServiceLocator locator, List<CustomObject6Data> listCustomObjectData, int pageNum, String userId)
	throws RemoteException, ServiceException {

		// logger.debug("Retrieving all Privileges");

		CustomObject6QueryPage_Input input = new CustomObject6QueryPage_Input();
		ListOfCustomObject6Query listOfCustomObject6 = new ListOfCustomObject6Query();
		CustomObject6Query customObject6 = new CustomObject6Query();
		CustomObject6Data[] customObject6Data = null;

		// Query types by :
		// id,OwnerUserSignInId,indexPicklist0,AccountExternalUniqueID and
		// CustomObject4ExternalUniqueID

		// Privilege ID
		QueryType queryType = null;
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setName(queryType);

		// Role
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setIndexedPick0(queryType);

		// Application Profile
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setIndexedPick1(queryType);

		// User Name
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerAlias(queryType);

		// user id ( to compare with id received in url)
		queryType = new QueryType();
		queryType.set_value("='" + userId + "'");
		customObject6.setOwnerUserSignInId(queryType);

		// Campaign/Store Id
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setAccountExternalSystemId(queryType);

		// ICL id
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setCustomObject4ExternalSystemId(queryType);

		// Created By
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setCreatedBy(queryType);

		// Modified By
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setModifiedBy(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setId(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerFirstName(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerLastName(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerFullName(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerEMailAddr(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerIntegrationId(queryType);

		listOfCustomObject6.setCustomObject6(customObject6);
		listOfCustomObject6.setRecordcountneeded(true);
		listOfCustomObject6.setPagesize(new BigInteger("100"));
		listOfCustomObject6.setStartrownum(new BigInteger("" + pageNum));
		input.setListOfCustomObject6(listOfCustomObject6);

		CustomObject6QueryPage_Output output = locator.getCustomObject6().customObject6QueryPage(input);

		if (output.getListOfCustomObject6().length < 100) {

			customObject6Data = output.getListOfCustomObject6();
			for (CustomObject6Data customObject : customObject6Data) {
				listCustomObjectData.add(customObject);
			}
			return listCustomObjectData;

		} else {

			customObject6Data = output.getListOfCustomObject6();
			for (CustomObject6Data customObject : customObject6Data) {
				listCustomObjectData.add(customObject);
			}
			searchByUserId(locator, listCustomObjectData, pageNum + 100, userId);
		}
		// logger.debug("Returning paginated records");
		return listCustomObjectData;
	}

	public List<CustomObject6Data> getAllPrivileges(CustomObject6_ServiceLocator locator, List<CustomObject6Data> listCustomObjectData, int pageNum, String userId)
	throws RemoteException, ServiceException {

		// logger.debug("Retrieving all Privileges");

		CustomObject6QueryPage_Input input = new CustomObject6QueryPage_Input();
		ListOfCustomObject6Query listOfCustomObject6 = new ListOfCustomObject6Query();
		CustomObject6Query customObject6 = new CustomObject6Query();
		CustomObject6Data[] customObject6Data = null;

		// Query types by :
		// id,OwnerUserSignInId,indexPicklist0,AccountExternalUniqueID and
		// CustomObject4ExternalUniqueID

		// Privilege ID
		QueryType queryType = null;
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setName(queryType);

		// Role
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setIndexedPick0(queryType);

		// Application Profile
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setIndexedPick1(queryType);

		// User Name
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerAlias(queryType);

		// user id ( to compare with id received in url)
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerUserSignInId(queryType);

		// Campaign/Store Id
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setAccountExternalSystemId(queryType);

		// ICL id
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setCustomObject4ExternalSystemId(queryType);

		// Created By
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setCreatedBy(queryType);

		// Modified By
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setModifiedBy(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setId(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerFirstName(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerLastName(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerFullName(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerEMailAddr(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerIntegrationId(queryType);

		listOfCustomObject6.setCustomObject6(customObject6);
		listOfCustomObject6.setRecordcountneeded(true);
		listOfCustomObject6.setPagesize(new BigInteger("100"));
		listOfCustomObject6.setStartrownum(new BigInteger("" + pageNum));
		input.setListOfCustomObject6(listOfCustomObject6);

		CustomObject6QueryPage_Output output = locator.getCustomObject6().customObject6QueryPage(input);

		if (output.getListOfCustomObject6().length < 100) {

			customObject6Data = output.getListOfCustomObject6();
			for (CustomObject6Data customObject : customObject6Data) {
				listCustomObjectData.add(customObject);
			}
			return listCustomObjectData;
		} else {
			customObject6Data = output.getListOfCustomObject6();
			for (CustomObject6Data customObject : customObject6Data) {
				listCustomObjectData.add(customObject);
			}
			getAllPrivileges(locator, listCustomObjectData, pageNum + 100, userId);
		}
		// logger.debug("Returning paginated records");
		return listCustomObjectData;
	}    
	
}
