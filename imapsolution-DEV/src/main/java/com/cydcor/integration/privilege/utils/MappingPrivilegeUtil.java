package com.cydcor.integration.privilege.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.exceptions.GenericException;
import com.cydcor.framework.exceptions.RoleNotFoundException;
import com.cydcor.framework.model.MerCampignDetails;
import com.cydcor.framework.model.MerICLDetails;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.PropertyUtils;
import com.cydcor.framework.utils.RetryableRunner;

/**
 * @author ashwin
 * 
 */
public class MappingPrivilegeUtil {
    private static final transient Log logger = LogFactory.getLog(MappingPrivilegeUtil.class);

    /**
     * @param userId
     * @return
     */
    public static Map<String, Map<String, RoleObject>> getRoleObject(String userId) {
	// Storing retrieved roleObject into Session
	Map<String, Map<String, RoleObject>> roleObjMap = (Map) CydcorContext.getInstance().getCache().get("roleObjMap_" + userId);

	// Check if roleObject exists in session or not
	if (roleObjMap == null || roleObjMap.isEmpty()) {
	    if (StringUtils.equalsIgnoreCase(CydcorUtils.getProperty("crmmode"), "true")) {

		try {
		    String url = (String) PropertyUtils.getInstance().getProperty().get("wsLocation");
		    if (!validateSSOToken(url, CydcorContext.getInstance().getSsoToken(), CydcorContext.getInstance().getUserId())) {
			logger.info("Invalid Validating SSO Token :");
			throw new RoleNotFoundException();
		    }
		} catch (Exception e) {
		    // TODO Auto-generated catch block
		    logger.info("Error While Validating SSO Token :", e);
		    throw new RoleNotFoundException();
		}
	    }
	    RetryableRunner retryableRunner = new RetryableRunner();
	    try {
		retryableRunner.run(new RoleRetreiver(userId));
	    } catch (GenericException e) {
		logger.debug("Error while retrying for role :", e);
	    }
	    roleObjMap = (Map) CydcorContext.getInstance().getCache().get("roleObjMap_" + userId);
	} else if (!roleObjMap.containsKey("roleObjMap_" + userId)) {

	    RetryableRunner retryableRunner = new RetryableRunner();
	    try {
		retryableRunner.run(new RoleRetreiver(userId));
	    } catch (GenericException e) {
		logger.debug("Error while retrying for role :", e);
	    }
	    roleObjMap = (Map) CydcorContext.getInstance().getCache().get("roleObjMap_" + userId);
	}

	if (roleObjMap == null || roleObjMap.isEmpty()) {
	    throw new RoleNotFoundException();
	}
	return roleObjMap;
    }

    /**
     * @param url
     * @param ssoToken
     * @param userName
     * @return
     * @throws Exception
     */
    public static boolean validateSSOToken(String url, String ssoToken, String userName) throws Exception {
	HttpURLConnection wsConnection = null;
	StringBuffer sb = new StringBuffer("");
	try {
	    String ssoTokenValidateURL = url.substring(0, url.lastIndexOf("/") + 1) + "SSOTokenValidate?odSsoToken=" + URLEncoder.encode(ssoToken, "UTF-8");
	    logger.debug("SSO Token validate URL : " + ssoTokenValidateURL);

	    URL wsURL = new URL(ssoTokenValidateURL);
	    // create an HTTPS connection to the OnDemand webservices
	    wsConnection = (HttpURLConnection) wsURL.openConnection();
	    // we don't want any caching to occur
	    wsConnection.setUseCaches(false);
	    // set some http headers to indicate the username and password we
	    // are using to logon
	    wsConnection.setRequestMethod("GET");

	    wsConnection.connect();
	    if (wsConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
		return false;
	    } else {
		BufferedReader br = new BufferedReader(new InputStreamReader(wsConnection.getInputStream()));
		String data = "";
		while (true) {
		    data = br.readLine();
		    if (data == null || "".equals(data)) {
			break;
		    }
		    sb.append(data);
		}
	    }
	} catch (UnsupportedEncodingException e) {
	    logger.error("Failed encoding ssotoken", e);
	    throw new Exception("Failed encoding ssotoken", e);
	} catch (MalformedURLException e) {
	    logger.error("Error while logging off from OD Session", e);
	    throw new Exception(e.getMessage(), e);
	} catch (ProtocolException e) {
	    logger.error("ProtocolException while logging off from OD Session", e);
	    throw new Exception("ProtocolException while logging off from OD Session", e);
	} catch (IOException e) {
	    logger.error("IOException while logging off from OD Session", e);
	    throw new Exception("IOException while logging off from OD Session", e);
	} finally {
	    try {
		if (wsConnection != null) {
		    wsConnection.disconnect();
		    wsConnection = null;
		}
	    } catch (Exception e) {
		logger.error("IOException while logging off from OD Session", e);
		throw new Exception("Exception while disconnecting from Oracle OnDemand", e);
	    }
	}
	logger.debug("data from OD :" + sb.toString().length());
	return sb.toString().trim().equalsIgnoreCase(userName.trim());
    }

    public static String getOfficeSeqsByUserId(String userId) {

	List officeSeqList = new ArrayList();
	Map<String, Map<String, RoleObject>> roleObjMap = getRoleObject(userId);

	Set entries = roleObjMap.entrySet();
	Iterator it = entries.iterator();

	while (it.hasNext()) {
	    Map.Entry roleObjEntry = (Entry) it.next();
	    Set innerMapEntries = ((Map<String, Map<String, RoleObject>>) roleObjEntry.getValue()).entrySet();
	    Iterator it2 = innerMapEntries.iterator();
	    while (it2.hasNext()) {
		Map.Entry innerEntry = (Entry) it2.next();
		logger.debug("in getOfficeSeqsByUserId : Key:" + innerEntry.getKey() + "--Value:---" + innerEntry.getValue());
		RoleObject roleOb = (RoleObject) innerEntry.getValue();
		officeSeqList.addAll(roleOb.getMerlinICL());
	    }
	}
	String officeSeqs = "";
	// appending officeSeqs into a string object with comma separated values
	for (Iterator iterator = officeSeqList.iterator(); iterator.hasNext();) {
	    MerICLDetails type = (MerICLDetails) iterator.next();
	    officeSeqs = officeSeqs + type.getOffice_seq() + ",";
	}
	// returning comma separated officeSeqs
	try {
	    return officeSeqs.substring(0, officeSeqs.length() - 1).trim();
	} catch (Exception e) {
	    return "";
	}
    }

    public static String getCampaignSeqsByUserId(String userId) {

	List campaignSeqList = new ArrayList();
	Map<String, Map<String, RoleObject>> roleObjMap = getRoleObject(userId);

	Set entries = roleObjMap.entrySet();
	Iterator it = entries.iterator();

	while (it.hasNext()) {
	    Map.Entry roleObjEntry = (Entry) it.next();
	    Set innerMapEntries = ((Map<String, Map<String, RoleObject>>) roleObjEntry.getValue()).entrySet();
	    Iterator it2 = innerMapEntries.iterator();
	    while (it2.hasNext()) {
		Map.Entry innerEntry = (Entry) it2.next();
		logger.debug("in getCampaignSeqsByUserId :  Key" + innerEntry.getKey() + "---Value---" + innerEntry.getValue());
		RoleObject roleOb = (RoleObject) innerEntry.getValue();
		campaignSeqList.addAll(roleOb.getMerlinCampaign());
	    }
	}
	String campaignSeqs = "";
	if (campaignSeqList != null && campaignSeqList.size() > 0) {
	    // appending campaignSeqs into a string object with comma separated
	    // values
	    for (Iterator iterator = campaignSeqList.iterator(); iterator.hasNext();) {
		MerCampignDetails type = (MerCampignDetails) iterator.next();
		campaignSeqs = campaignSeqs + type.getCampaign_seq() + ",";
	    }
	    // returning comma separated campaignSeqs
	    return campaignSeqs.substring(0, campaignSeqs.length() - 1).trim();
	} else {
	    return "";
	}
    }

    public static String getRoleNamesByUserId(String userId) {

	List roleNameList = new ArrayList();
	Map<String, Map<String, RoleObject>> roleObjMap = getRoleObject(userId);

	Set entries = roleObjMap.entrySet();
	Iterator it = entries.iterator();

	while (it.hasNext()) {
	    Map.Entry roleObjEntry = (Entry) it.next();
	    Set innerMapEntries = ((Map<String, Map<String, RoleObject>>) roleObjEntry.getValue()).entrySet();
	    Iterator it2 = innerMapEntries.iterator();
	    while (it2.hasNext()) {
		Map.Entry innerEntry = (Entry) it2.next();
		logger.debug("in getRoleNamesByUserId : Key " + innerEntry.getKey() + "---Value---" + innerEntry.getValue());
		RoleObject roleOb = (RoleObject) innerEntry.getValue();
		roleNameList.add(roleOb.getRoleName());
	    }
	}
	String roleName = null;
	// appending officeSeqs into a string object with comma separated values
	for (Iterator iterator = roleNameList.iterator(); iterator.hasNext();) {
	    String name = (String) iterator.next();
	    if (name.equalsIgnoreCase("Application Administrator")) {
		roleName = name;
		break;
	    } else if (name.equalsIgnoreCase("Cydcor Executive")) {
		roleName = name;
		break;
	    } else if (name.equalsIgnoreCase("Campaign Manager")) {
		roleName = name;
		break;
	    } else if (name.equalsIgnoreCase("ICL Owner")) {
		roleName = name;
		break;
	    } else if (name.equalsIgnoreCase("Dispositions Manager")) {
		roleName = name;
		break;
	    }
	}
	return roleName;
	/*
	 * String commaSeparatedroleNames = roleNames; // returning comma
	 * separated officeSeqs return
	 * commaSeparatedroleNames.replace(commaSeparatedroleNames
	 * .charAt(commaSeparatedroleNames.length() - 1), ' ') .trim();
	 */
    }

    public static String getUserNameByUserId(String userId) {
	String userName = "";

	List roleNameList = new ArrayList();
	Map<String, Map<String, RoleObject>> roleObjMap = getRoleObject(userId);

	Set entries = roleObjMap.entrySet();
	Iterator it = entries.iterator();

	while (it.hasNext()) {
	    Map.Entry roleObjEntry = (Entry) it.next();
	    Set innerMapEntries = ((Map<String, Map<String, RoleObject>>) roleObjEntry.getValue()).entrySet();
	    Iterator it2 = innerMapEntries.iterator();
	    while (it2.hasNext()) {
		Map.Entry innerEntry = (Entry) it2.next();
		logger.debug("in getUserNameByUserId : Key " + innerEntry.getKey() + "---Value---" + innerEntry.getValue());
		RoleObject roleOb = (RoleObject) innerEntry.getValue();
		userName += roleOb.getOwnerFirstName() + " " + roleOb.getOwnerLastName();
	    }
	}
	return userName;

    }
}
