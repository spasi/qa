package com.cydcor.integration.privilege;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.MerCampignDetails;
import com.cydcor.framework.model.MerICLDetails;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.test.SpringPage;
import com.cydcor.framework.utils.ODSessionPool;
import com.cydcor.framework.utils.PropertyUtils;
import com.cydcor.framework.ws.GenericWS;
import com.cydcor.ws.WSMappingInterface;
import com.cydcor.ws.stubs.privillege.CustomObject6Data;
import com.cydcor.ws.stubs.privillege.CustomObject6Query;
import com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Input;
import com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output;
import com.cydcor.ws.stubs.privillege.CustomObject6_ServiceLocator;
import com.cydcor.ws.stubs.privillege.ListOfCustomObject6Query;
import com.cydcor.ws.stubs.privillege.QueryType;

public class RetrieveMappingPrivilege extends GenericWS implements WSMappingInterface {

	protected final Log logger = LogFactory.getLog(getClass());

	private static SpringPage springPage = null;
	String sessionId = null;

	public Map<String, Map<String, RoleObject>> call(String userId) throws Exception {

		RoleObject roleObject = null;
		Map<String, RoleObject> roleSpecificMap = null;
		Map<String, Map<String, RoleObject>> roleObjMap = new HashMap<String, Map<String, RoleObject>>();
		List<CustomObject6Data> listCustomObjectData = new ArrayList<CustomObject6Data>();

		CustomObject6_ServiceLocator locator = new CustomObject6_ServiceLocator();
		// springPage = new SpringPage();
		CustomObject6Data[] customObject6Data = null;
		sessionId = ODSessionPool.getInstance().getODSessionId();
		logger.debug(sessionId);
		locator.setCustomObject6EndpointAddress(PropertyUtils.getInstance().getProperty().get("wsLocation")
				+ ";jsessionid=" + sessionId);
		logger.info("START TIME OF RetrieveMappingPrivillege ::" + new Date(System.currentTimeMillis()));

		listCustomObjectData = search(locator, listCustomObjectData, 0, userId);

		// Loop to retrieve roleObject with all OFFICES & CAMPAIGNS assigned to
		// this user

		for (CustomObject6Data object : listCustomObjectData) {

			roleSpecificMap = roleObjMap.get(object.getOwnerUserSignInId());
			if (roleSpecificMap == null) {
				roleSpecificMap = new HashMap<String, RoleObject>();
				roleObjMap.put(object.getOwnerUserSignInId().toString(), roleSpecificMap);
			}

			roleObject = roleSpecificMap.get(object.getIndexedPick0());
			if (roleObject == null) {
				roleObject = new RoleObject();
				roleObject.setOwnerUserSignInId(object.getOwnerUserSignInId());
				roleObject.setOwnerFirstName(object.getOwnerFirstName());
				roleObject.setOwnerLastName(object.getOwnerLastName());
				roleObject.setOwnerFullName(object.getOwnerFullName());
				roleObject.setOwnerEmailAddress(object.getOwnerEMailAddr());
				roleObject.setOwnerIntegrationId(object.getOwnerIntegrationId());
				roleObject.setOwnerAlias(object.getOwnerAlias());
				roleObject.setRoleName(object.getIndexedPick0());
				roleObject.setMerlinICL(new ArrayList<MerICLDetails>());
				roleObject.setMerlinCampaign(new ArrayList<MerCampignDetails>());
				roleSpecificMap.put(object.getIndexedPick0(), roleObject);
			}
			if (roleObject != null)
				roleObject.addProfileNames(object.getIndexedPick1());
			MerICLDetails iclDetails = new MerICLDetails();
			iclDetails.setOffice_seq(object.getCustomObject4ExternalSystemId());
			roleObject.getMerlinICL().add(iclDetails);

			MerCampignDetails campaignObjs = new MerCampignDetails();
			campaignObjs.setCampaign_seq(object.getAccountExternalSystemId());
			roleObject.getMerlinCampaign().add(campaignObjs);
		}
		logger.info("END TIME OF RetrieveMappingPrivillege ::" + new Date(System.currentTimeMillis()));
		// To return to session to pool
		ODSessionPool.getInstance().returnODSession(sessionId);
		logger.debug("ODSession returned");
		logger.debug("Returning roleObject Map instance");
		return roleObjMap;
	}

	private List<CustomObject6Data> search(CustomObject6_ServiceLocator locator,
			List<CustomObject6Data> listCustomObjectData, int pageNum, String userId) throws RemoteException,
			ServiceException {

		// logger.debug("Retrieving all Privileges");

		CustomObject6QueryPage_Input input = new CustomObject6QueryPage_Input();
		ListOfCustomObject6Query listOfCustomObject6 = new ListOfCustomObject6Query();
		CustomObject6Query customObject6 = new CustomObject6Query();
		CustomObject6Data[] customObject6Data = null;

		// Query types by :
		// id,OwnerUserSignInId,indexPicklist0,AccountExternalUniqueID and
		// CustomObject4ExternalUniqueID
		QueryType queryType = null;
		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setId(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setAccountExternalSystemId(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerFirstName(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerLastName(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerFullName(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerEMailAddr(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerIntegrationId(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setOwnerAlias(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setIndexedPick0(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setIndexedPick1(queryType);

		queryType = new QueryType();
		queryType.set_value("");
		customObject6.setCustomObject4ExternalSystemId(queryType);

		queryType = new QueryType();
		queryType.set_value("='" + userId + "'");
		customObject6.setOwnerUserSignInId(queryType);

		listOfCustomObject6.setCustomObject6(customObject6);
		listOfCustomObject6.setRecordcountneeded(true);
		listOfCustomObject6.setPagesize(new BigInteger("100"));
		listOfCustomObject6.setStartrownum(new BigInteger("" + pageNum));

		input.setListOfCustomObject6(listOfCustomObject6);

		CustomObject6QueryPage_Output output = locator.getCustomObject6().customObject6QueryPage(input);

		if (output.getListOfCustomObject6().length < 100) {

			customObject6Data = output.getListOfCustomObject6();
			for (CustomObject6Data customObject : customObject6Data) {
				listCustomObjectData.add(customObject);
			}
			return listCustomObjectData;

		} else {
			listCustomObjectData = new ArrayList<CustomObject6Data>();
			customObject6Data = output.getListOfCustomObject6();
			for (CustomObject6Data customObject : customObject6Data) {
				listCustomObjectData.add(customObject);
			}
			search(locator, listCustomObjectData, pageNum + 100, userId);
		}
		// logger.debug("Retruning paginated records");
		return listCustomObjectData;
	}
}
