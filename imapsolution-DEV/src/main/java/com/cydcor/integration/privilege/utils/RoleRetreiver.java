/**
 * 
 */
package com.cydcor.integration.privilege.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.exceptions.RoleNotFoundException;
import com.cydcor.framework.model.MerCampignDetails;
import com.cydcor.framework.model.MerICLDetails;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.utils.RetryableRunner;
import com.cydcor.framework.utils.WSRetryable;
import com.cydcor.integration.privilege.RetrieveMappingPrivilegeFromCRM;
import com.cydcor.integration.privilege.RetrieveMappingPrivilegeFromDB;
import com.cydcor.ws.WSMappingInterface;
import com.serene.crmod.service.impl.CRMODServiceDaoImpl;

/**
 * @author ashwin
 * 
 */
public class RoleRetreiver implements WSRetryable {
	private String userId;
	private static final transient Log logger = LogFactory.getLog(RoleRetreiver.class);

	/**
	 * @param userId
	 */
	public RoleRetreiver(String userId) {
		this.userId = userId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.utils.WSRetryable#run(com.cydcor.framework.utils
	 * .RetryableRunner)
	 */
	public void run(RetryableRunner r) throws Exception {
		Map<String, Map<String, RoleObject>> roleObjMap = (Map) CydcorContext.getInstance().getCache().get("roleObjMap_" + userId);
		if (roleObjMap.containsKey(userId.toUpperCase()))
			return;
		
		WSMappingInterface wsInterface = null;
		
			
		/*wsInterface = new RetrieveMappingPrivilegeFromDB();

		try {
			roleObjMap = wsInterface.call(userId);
		} catch (Exception e) {
			logger.debug("Error While Retrieving Mapping Privilege From DB", e);
			// e.printStackTrace();
			throw e;
		}*/
		
		
		if (roleObjMap == null || roleObjMap.isEmpty()) {
			wsInterface = new RetrieveMappingPrivilegeFromCRM();

			try {
		
				RoleObject roleObject = null;
				Map<String, RoleObject> roleSpecificMap = null;
				//Map<String, Map<String, RoleObject>> roleObjMap = new HashMap<String, Map<String, RoleObject>>();
			
		
				roleSpecificMap = roleObjMap.get("CYDCOR/MAPPINGADMIN");
				if (roleSpecificMap == null) {
					roleSpecificMap = new HashMap<String, RoleObject>();
					roleObjMap.put("CYDCOR/MAPPINGADMIN", roleSpecificMap);
				}
		
				roleObject = roleSpecificMap.get("Application Administrator");
				if (roleObject == null) {
					roleObject = new RoleObject();
					roleObject.setOwnerUserSignInId("CYDCOR/MAPPINGADMIN");
		//				roleObject.setOwnerFirstName(object.getOwnerFirstName());
		//				roleObject.setOwnerLastName(object.getOwnerLastName());
		//				roleObject.setOwnerFullName(object.getOwnerFullName());
		//				roleObject.setOwnerEmailAddress(object.getOwnerEMailAddr());
		//				roleObject.setOwnerIntegrationId(object.getOwnerIntegrationId());
		//				roleObject.setOwnerAlias(object.getOwnerAlias());
					roleObject.setRoleName("Application Administrator");
					roleObject.setMerlinICL(new ArrayList<MerICLDetails>());
					roleObject.setMerlinCampaign(new ArrayList<MerCampignDetails>());
					roleSpecificMap.put("Application Administrator", roleObject);
				}
				if (roleObject != null)
					roleObject.addProfileNames("Verizon FIOS Leads Profile");
				
				MerICLDetails iclDetails = new MerICLDetails();
		//			iclDetails.setOffice_seq(object.getCustomObject4ExternalSystemId());
		//			iclDetails.setOffice_code(object.getCustomObject4Name());
				roleObject.getMerlinICL().add(iclDetails);
		
				MerCampignDetails campaignObjs = new MerCampignDetails();
		//			campaignObjs.setCampaign_seq(object.getAccountExternalSystemId());
				roleObject.getMerlinCampaign().add(campaignObjs);
	
			} catch (Exception e) {
				logger.debug("Error While Retreiving Mapping Privilege From CRM", e);
				// e.printStackTrace();
				throw e;
			}
		
		}
		
		if (roleObjMap == null || roleObjMap.isEmpty()) {
			throw new RoleNotFoundException();
		}

		CydcorContext.getInstance().getCache().put("roleObjMap_" + userId, roleObjMap);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cydcor.framework.utils.Retryable#reInitForRetry()
	 */
	public void reInitForRetry() {
		// TODO Auto-generated method stub

	}

}
