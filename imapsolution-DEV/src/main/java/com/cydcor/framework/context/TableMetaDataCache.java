/**
 * 
 */
package com.cydcor.framework.context;

import java.util.HashMap;
import java.util.Map;

import com.cydcor.framework.model.DBEntityComponent;

/**
 * @author ashwin
 * 
 */
/**
 * @author ashwin
 *
 */
public class TableMetaDataCache {

	private static TableMetaDataCache metaDataCache = new TableMetaDataCache();

	private static Map<String, DBEntityComponent> entityMetaDataCache = new HashMap<String, DBEntityComponent>();

	private TableMetaDataCache() {
	}

	public static TableMetaDataCache getInstance() {
		return metaDataCache;
	}

	/**
	 * @param entityName
	 * @return
	 */
	public DBEntityComponent getMetaData(String entityName) {
		return entityMetaDataCache.get(entityName);
	}

	/**
	 * @param entityName
	 * @param metaData
	 */
	public void storeMetaData(String entityName, DBEntityComponent metaData) {
		entityMetaDataCache.remove(entityName);
		entityMetaDataCache.put(entityName, metaData);
	}

}
