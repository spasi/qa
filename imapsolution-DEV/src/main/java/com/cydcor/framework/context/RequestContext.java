/**
 * 
 */
package com.cydcor.framework.context;

import java.sql.Connection;

import com.cydcor.framework.model.UserObject;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

/**
 * This Object is created per user request. This is used to store Per Request
 * Resources Like Connection and any user Specific Information
 * 
 * @author ashwin
 * 
 */
public class RequestContext {
	// Base where application is deployed
	private String basePath = "";
	private Connection connection = null;
	private UserObject user = null;
	private CydcorRequestWrapper requestWrapper = null;

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection
	 *            the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @return the user
	 */
	public UserObject getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(UserObject user) {
		this.user = user;
	}

	/**
	 * @return the requestWrapper
	 */
	public CydcorRequestWrapper getRequestWrapper() {
		return requestWrapper;
	}

	/**
	 * @param requestWrapper
	 *            the requestWrapper to set
	 */
	public void setRequestWrapper(CydcorRequestWrapper requestWrapper) {
		this.requestWrapper = requestWrapper;
	}

}
