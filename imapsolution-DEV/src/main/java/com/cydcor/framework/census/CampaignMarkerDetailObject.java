package com.cydcor.framework.census;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cydcor.framework.model.LeadMaster;

public class CampaignMarkerDetailObject {
	private Integer campaignKey;
	private Integer officeKey;
	private String Name;
	private List<ICLMaster> offices = new ArrayList<ICLMaster>();
	private List<LeadMaster> leads = new ArrayList<LeadMaster>();
	private List<Polygon> territories = new ArrayList<Polygon>();

	private Map<String, String> markerMap = new HashMap<String, String>();
	private Set<MarkerImage> markerList = new HashSet<MarkerImage>();

	public Integer getCampaignKey() {
		return campaignKey;
	}

	public void setCampaignKey(Integer campaignKey) {
		this.campaignKey = campaignKey;
	}

	public Integer getOfficeKey() {
		return officeKey;
	}

	public void setOfficeKey(Integer officeKey) {
		this.officeKey = officeKey;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public List<ICLMaster> getOffices() {
		return offices;
	}

	public void setOffices(List<ICLMaster> offices) {
		this.offices = offices;
	}

	public List<LeadMaster> getLeads() {
		return leads;
	}

	public void setLeads(List<LeadMaster> leads) {
		this.leads = leads;
	}

	public List<Polygon> getTerritories() {
		return territories;
	}

	public void setTerritories(List<Polygon> territories) {
		this.territories = territories;
	}

	@Override
	public String toString() {
		return "Campaign [Name=" + Name + ", campaignKey=" + campaignKey
				+ ", leads=" + leads + ", officeKey=" + officeKey
				+ ", offices=" + offices + ", territories=" + territories + "]";
	}

	/**
	 * @return the markerMap
	 */
	public Map<String, String> getMarkerMap() {
		return markerMap;
	}

	/**
	 * @param markerMap
	 *            the markerMap to set
	 */
	public void setMarkerMap(Map<String, String> markerMap) {
		this.markerMap = markerMap;
	}

	/**
	 * @return the markerList
	 */
	public Set<MarkerImage> getMarkerList() {
		return markerList;
	}

	/**
	 * @param markerList
	 *            the markerList to set
	 */
	public void setMarkerList(Set<MarkerImage> markerList) {
		this.markerList = markerList;
	}

}
