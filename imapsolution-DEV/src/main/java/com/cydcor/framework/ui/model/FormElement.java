/**
 * 
 */
package com.cydcor.framework.ui.model;

/**
 * @author ashwin
 * 
 */
public class FormElement {
	private String name = "";
	private String label = "";
	private String type = "";

	public static interface ElementTypes {
		public static String TEXT_FIELD = "TEXT_FIELD";
		public static String TEXT_AREA = "TEXT_AREA";
		public static String DATETIME = "DATETIME";
		public static String DATE = "DATE";
		public static String LOV = "LOV";
		public static String CHECKBOX = "CHECKBOX";
		public static String FILE_UPLOAD = "FILE_UPLOAD";
		public static String BUTTON = "BUTTON";
		public static String SUBMIT = "SUBMIT";
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
