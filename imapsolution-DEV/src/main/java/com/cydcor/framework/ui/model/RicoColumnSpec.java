/**
 * 
 */
package com.cydcor.framework.ui.model;

/**
 * @author ashwin
 * 
 */
public class RicoColumnSpec {
	private String fieldName = "genericTable_0";
	private int panelIdx = 0;
	private boolean readOnly = false;
	private String colName = "OrderID";
	private String hdg = "Order ID";
	private String entryType = "B";
	private String colData = "<auto>";
	private int width = 50;
	private boolean nullable = false;
	private boolean writeable = false;
	private boolean key = true;
	private Integer length = null;
	private String selectCtl = null;
	private boolean insertOnly = false;
	private boolean filterUI = false;
	// THis defines column type (Date..etc)
	private String type = null;
	// Any custom columntypes to be defined like checkbox/hyperlink
	private String control = "";

	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName
	 *            the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * @return the panelIdx
	 */
	public int getPanelIdx() {
		return panelIdx;
	}

	/**
	 * @param panelIdx
	 *            the panelIdx to set
	 */
	public void setPanelIdx(int panelIdx) {
		this.panelIdx = panelIdx;
	}

	/**
	 * @return the readOnly
	 */
	public boolean isReadOnly() {
		return readOnly;
	}

	/**
	 * @param readOnly
	 *            the readOnly to set
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * @return the colName
	 */
	public String getColName() {
		return colName;
	}

	/**
	 * @param colName
	 *            the colName to set
	 */
	public void setColName(String colName) {
		this.colName = colName;
	}

	/**
	 * @return the hdg
	 */
	public String getHdg() {
		return hdg;
	}

	/**
	 * @param hdg
	 *            the hdg to set
	 */
	public void setHdg(String hdg) {
		this.hdg = hdg;
	}

	/**
	 * @return the entryType
	 */
	public String getEntryType() {
		return entryType;
	}

	/**
	 * @param entryType
	 *            the entryType to set
	 */
	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}

	/**
	 * @return the colData
	 */
	public String getColData() {
		return colData;
	}

	/**
	 * @param colData
	 *            the colData to set
	 */
	public void setColData(String colData) {
		this.colData = colData;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the isNullable
	 */
	public boolean isNullable() {
		return nullable;
	}

	/**
	 * @param isNullable
	 *            the isNullable to set
	 */
	public void setNullable(boolean isNullable) {
		this.nullable = isNullable;
	}

	/**
	 * @return the writeable
	 */
	public boolean isWriteable() {
		return writeable;
	}

	/**
	 * @param writeable
	 *            the writeable to set
	 */
	public void setWriteable(boolean writeable) {
		this.writeable = writeable;
	}

	/**
	 * @return the isKey
	 */
	public boolean isKey() {
		return key;
	}

	/**
	 * @param isKey
	 *            the isKey to set
	 */
	public void setKey(boolean isKey) {
		this.key = isKey;
	}

	/**
	 * @return the length
	 */
	public Integer getLength() {
		return length;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public void setLength(Integer length) {
		this.length = length;
	}

	/**
	 * @return the selectCtl
	 */
	public String getSelectCtl() {
		return selectCtl;
	}

	/**
	 * @param selectCtl
	 *            the selectCtl to set
	 */
	public void setSelectCtl(String selectCtl) {
		this.selectCtl = selectCtl;
	}

	/**
	 * @return the insertOnly
	 */
	public boolean isInsertOnly() {
		return insertOnly;
	}

	/**
	 * @param insertOnly
	 *            the insertOnly to set
	 */
	public void setInsertOnly(boolean insertOnly) {
		this.insertOnly = insertOnly;
	}

	/**
	 * @return the filterUI
	 */
	public boolean isFilterUI() {
		return filterUI;
	}

	/**
	 * @param filterUI
	 *            the filterUI to set
	 */
	public void setFilterUI(boolean filterUI) {
		this.filterUI = filterUI;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the control
	 */
	public String getControl() {
		return control;
	}

	/**
	 * @param control the control to set
	 */
	public void setControl(String control) {
		this.control = control;
	}

}
