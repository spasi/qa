/**
 * 
 */
package com.cydcor.framework.ui.model.converter;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.ReportComponent;
import com.cydcor.framework.ui.model.RicoColumnSpec;
import com.cydcor.framework.ui.model.RicoTable;
import com.cydcor.framework.utils.FreeMarkerEngine;

/**
 * @author ashwin
 *
 */
public class RicoTableConverter {
	private static final transient Log logger = LogFactory.getLog(RicoTableConverter.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Map paramMap = new HashMap();
		
		
		ReportComponent component = new ReportComponent(); 
		
		RicoTable ricoTable = new RicoTable();
		
		RicoColumnSpec columnSpec = new RicoColumnSpec();
		
		ricoTable.addRicoColumn(columnSpec);
		
		
		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_1");
		columnSpec.setPanelIdx(0);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("CustomerID");
		columnSpec.setHdg("Customer");
		columnSpec.setEntryType("CL");
		columnSpec.setColData("");
		columnSpec.setSelectCtl("CustomerTree");
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(160);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(5);
		ricoTable.addRicoColumn(columnSpec);
		
		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_2");
		columnSpec.setPanelIdx(0);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("EmployeeID");
		columnSpec.setHdg("Sales Person");
		columnSpec.setEntryType("SL");
		columnSpec.setColData("");
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(140);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(11);
		ricoTable.addRicoColumn(columnSpec);
		
		
		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_3");
		columnSpec.setPanelIdx(0);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("OrderDate");
		columnSpec.setHdg("Order Date");
		columnSpec.setEntryType("D");
		columnSpec.setColData("2009-11-09");
		columnSpec.setType("date");
		columnSpec.setSelectCtl("Cal");
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(90);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(null);
		ricoTable.addRicoColumn(columnSpec);	

		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_4");
		columnSpec.setPanelIdx(0);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("RequiredDate");
		columnSpec.setHdg("Required Date");
		columnSpec.setEntryType("D");
		columnSpec.setColData("2009-11-09");
		columnSpec.setType("date");
		columnSpec.setSelectCtl("Cal");
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(90);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(null);
		ricoTable.addRicoColumn(columnSpec);
		
		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_5");
		columnSpec.setPanelIdx(0);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("Calc_5");
		columnSpec.setHdg("Net Sale");
		columnSpec.setEntryType(null);
		columnSpec.setColData(null);
		columnSpec.setType(null);
		columnSpec.setSelectCtl(null);
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(80);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(null);
		ricoTable.addRicoColumn(columnSpec);	

		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_6");
		columnSpec.setPanelIdx(1);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("ShipName");
		columnSpec.setHdg("Name");
		columnSpec.setEntryType(null);
		columnSpec.setColData("");
		columnSpec.setType(null);
		columnSpec.setSelectCtl(null);
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(140);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(40);
		ricoTable.addRicoColumn(columnSpec);		


		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_7");
		columnSpec.setPanelIdx(1);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("ShipAddress");
		columnSpec.setHdg("Address");
		columnSpec.setEntryType("B");
		columnSpec.setColData("");
		columnSpec.setType(null);
		columnSpec.setSelectCtl(null);
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(140);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(60);
		ricoTable.addRicoColumn(columnSpec);


		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_8");
		columnSpec.setPanelIdx(1);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("ShipCity");
		columnSpec.setHdg("City");
		columnSpec.setEntryType("B");
		columnSpec.setColData("");
		columnSpec.setType(null);
		columnSpec.setSelectCtl(null);
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(120);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(15);
		ricoTable.addRicoColumn(columnSpec);				


		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_9");
		columnSpec.setPanelIdx(1);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("ShipRegion");
		columnSpec.setHdg("Region");
		columnSpec.setEntryType("T");
		columnSpec.setColData("");
		columnSpec.setType(null);
		columnSpec.setSelectCtl(null);
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(60);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(15);
		ricoTable.addRicoColumn(columnSpec);						


		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_10");
		columnSpec.setPanelIdx(1);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("ShipPostalCode");
		columnSpec.setHdg("Postal Code");
		columnSpec.setEntryType("T");
		columnSpec.setColData("");
		columnSpec.setType(null);
		columnSpec.setSelectCtl(null);
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(100);
		columnSpec.setFilterUI(true);
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(10);
		ricoTable.addRicoColumn(columnSpec);						


		columnSpec = new RicoColumnSpec();
		columnSpec.setFieldName("genericTable_11");
		columnSpec.setPanelIdx(1);
		columnSpec.setReadOnly(false);
		columnSpec.setColName("ShipCountry");
		columnSpec.setHdg("Country");
		columnSpec.setEntryType("N");
		columnSpec.setColData("");
		columnSpec.setType(null);
		columnSpec.setSelectCtl(null);
		columnSpec.setInsertOnly(true);
		columnSpec.setWidth(100);
		columnSpec.setFilterUI(true);
		columnSpec.setControl("new Rico.TableColumn.link(\\'http://en.wikipedia.org/wiki/{11}\\',\\'_blank\\')");
		columnSpec.setNullable(true);
		columnSpec.setWriteable(true);
		columnSpec.setKey(false);
		columnSpec.setLength(15);
		ricoTable.addRicoColumn(columnSpec);						
		
		
		component.setTable(ricoTable);
		
		paramMap.put("report", component);
		
		FreeMarkerEngine.getInstance().changeFTLDirectory("src\\main\\resources\\ftl");
		String ricoTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("RicoTableDef.html",paramMap);
		
		logger.debug(ricoTemplate);
	}

}
