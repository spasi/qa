package com.cydcor.framework.report.impl;

import java.io.OutputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cydcor.framework.report.Exporter;

public class ExportContext {
	private String reportName = "";
	private String reportType = Exporter.PDF;
	private String reportDescription = "";
	private List<Object> sqlParameters = new ArrayList<Object>();;
	private Map<String, Object> reportParameters = new HashMap<String, Object>();
	private String sqlString = "";
	private OutputStream exportStream = null;
	private Connection sqlConnecion = null;
	private boolean showMapInReport = false;

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportDescription() {
		return reportDescription;
	}

	public void setReportDescription(String reportDescription) {
		this.reportDescription = reportDescription;
	}

	public String getSqlString() {
		return sqlString;
	}

	public void setSqlString(String sqlString) {
		this.sqlString = sqlString;
	}

	public OutputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(OutputStream exportStream) {
		this.exportStream = exportStream;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public Connection getSqlConnecion() {
		return sqlConnecion;
	}

	public void setSqlConnecion(Connection sqlConnecion) {
		this.sqlConnecion = sqlConnecion;
	}

	public Map<String, Object> getReportParameters() {
		return reportParameters;
	}

	public void setReportParameters(Map<String, Object> reportParameters) {
		this.reportParameters = reportParameters;
	}

	public List<Object> getSqlParameters() {
		return sqlParameters;
	}

	public void setSqlParameters(List<Object> sqlParameters) {
		this.sqlParameters = sqlParameters;
	}

	/**
	 * @return the showMapInReport
	 */
	public boolean isShowMapInReport() {
		return showMapInReport;
	}

	/**
	 * @param showMapInReport the showMapInReport to set
	 */
	public void setShowMapInReport(boolean showMapInReport) {
		this.showMapInReport = showMapInReport;
	}

}
