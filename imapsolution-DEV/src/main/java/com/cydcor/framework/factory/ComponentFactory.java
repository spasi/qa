/**
 * 
 */
package com.cydcor.framework.factory;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.component.ComponentCreator;
import com.cydcor.framework.component.TemplateCreator;
import com.cydcor.framework.context.DynamicUIContext;

/**
 * To Create any Generic Components through out the Application
 * 
 * @author ashwin
 * 
 */
public class ComponentFactory {

	private static ComponentFactory componentFactory = new ComponentFactory();

	private ComponentFactory() {
	}

	/**
	 * @return
	 */
	public static ComponentFactory getInstance() {
		return componentFactory;
	}

	/**
	 * @param uiContext
	 * @return
	 */
	public GenericUIControl getComponent(DynamicUIContext uiContext) {
		GenericUIControl genericUIControl = null;
		if (StringUtils.equalsIgnoreCase(uiContext.getControlName(),
				"LoadTemplate")) {
			ComponentCreator componentCreator = new TemplateCreator();
			genericUIControl = componentCreator.getComponent(uiContext);
		}else{
			
		}
		return genericUIControl;
	}

}
