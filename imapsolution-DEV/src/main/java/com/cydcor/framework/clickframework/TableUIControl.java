/**
 * 
 */
package com.cydcor.framework.clickframework;

import java.util.ArrayList;
import java.util.List;

import net.sf.click.util.HtmlStringBuffer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.QueryUtils;

/**
 * @author ashwin
 * 
 */
public class TableUIControl extends GenericUIControl {
    private static final transient Log logger = LogFactory.getLog(TableUIControl.class);

    private String queryKeyName = "";

    private String queryString = "";

    private String columnName = "";

    private String tableName = "";

    private String javaScriptMethodName = "";

    // SQL That has to be run
    private String tableSQL = "";

    // Total Number of records in Table
    private int numberOfRecords = 0;

    // Total Number of rows that has to be shown on page
    private int rowsPerPage = 5;

    // Position from which we have to start getting records
    private int offset = 0;

    private int pageNumber = 1;

    private List filterParamList = new ArrayList();

    private boolean enableFirstLastLinks = true;

    /**
     * @return the queryKeyName
     */
    public String getQueryKeyName() {
	return queryKeyName;
    }

    /**
     * @param queryKeyName
     *            the queryKeyName to set
     */
    public void setQueryKeyName(String queryKeyName) {
	this.queryKeyName = queryKeyName;
    }

    /**
     * @return the tableSQL
     */
    public String getTableSQL() {
	return tableSQL;
    }

    /**
     * @param tableSQL
     *            the tableSQL to set
     */
    public void setTableSQL(String tableSQL) {
	this.tableSQL = tableSQL;
    }

    /**
     * @return the numberOfRecords
     */
    public int getNumberOfRecords() {
	return numberOfRecords;
    }

    /**
     * @param numberOfRecords
     *            the numberOfRecords to set
     */
    public void setNumberOfRecords(int numberOfRecords) {
	this.numberOfRecords = numberOfRecords;
    }

    /**
     * @return the rowsPerPage
     */
    public int getRowsPerPage() {
	return rowsPerPage;
    }

    /**
     * @param rowsPerPage
     *            the rowsPerPage to set
     */
    public void setRowsPerPage(int rowsPerPage) {
	this.rowsPerPage = rowsPerPage;
    }

    /**
     * @return the offset
     */
    public int getOffset() {
	return offset;
    }

    /**
     * @param offset
     *            the offset to set
     */
    public void setOffset(int offset) {
	this.offset = offset;
    }

    /**
     * @return the pageNumber
     */
    public int getPageNumber() {
	return pageNumber;
    }

    /**
     * @param pageNumber
     *            the pageNumber to set
     */
    public void setPageNumber(int pageNumber) {
	this.pageNumber = pageNumber;
    }

    /**
     * @return the filterParamList
     */
    public List getFilterParamList() {
	return filterParamList;
    }

    /**
     * @param filterParamList
     *            the filterParamList to set
     */
    public void setFilterParamList(List filterParamList) {
	this.filterParamList = filterParamList;
    }

    public String getColumnName() {
	return columnName;
    }

    public void setColumnName(String columnName) {
	this.columnName = columnName.trim();
    }

    public String getJavaScriptMethodName() {
	return javaScriptMethodName;
    }

    public void setJavaScriptMethodName(String javaScriptMethodName) {
	this.javaScriptMethodName = javaScriptMethodName;
    }

    @Override
    public void render(HtmlStringBuffer buffer) {

	try {
	    pageNumber = Integer.parseInt(CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("pageNumber"));
	} catch (Exception  e) 
	{
	    pageNumber = 1;
	    //System.out.println(e);
	}
	try {
	    if (StringUtils.isNotBlank(CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("sortingColumn"))) {
		String sortingColumn = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("sortingColumn");
		if(sortingColumn.equalsIgnoreCase("CLLI_DA:ASC"))
		{
			sortingColumn = "CLLI:ASC";
		}
		else if(sortingColumn.equalsIgnoreCase("CLLI_DA:DESC"))
		{
			sortingColumn = "CLLI:DESC";
		}
		if (!sortingColumn.contains(" ")) {
		    setColumnName(sortingColumn.replace(":", " "));
		} else {
		    setColumnName(sortingColumn.split(":")[0]);
		}
		if (new Boolean(sortingColumn.split(":")[1])) {
		    pageNumber = 1;
		}
	    }
	} catch (Exception e) {
	}

	// offset = pageNumber * rowsPerPage;

	int lastIndex = pageNumber * rowsPerPage;
	boolean enableNext = true;

	int startIndex = lastIndex - rowsPerPage;
	String filterQuery = null;
	if (StringUtils.isBlank(getQueryString())) {
	    filterQuery = QueryUtils.getQuery().get(getQueryKeyName());
	} else {
	    filterQuery = getQueryString();
	}

	int noOfRecs = 0;
	try {
	    noOfRecs = getPlatformService().loadResult(filterQuery, getFilterParamList()).getData().size();
	} catch (Exception e) {
	    logger.debug(e);
	}
	if (noOfRecs == 0) {
	    startIndex = 0;
	    lastIndex = 0;
	    enableNext = false;
	} else {
	    if (pageNumber * rowsPerPage >= noOfRecs) {
		lastIndex = noOfRecs;
		startIndex = ((pageNumber - 1) * rowsPerPage) + 1;
		enableNext = false;
	    } else {
		startIndex = ((pageNumber - 1) * rowsPerPage) + 1;
		lastIndex = pageNumber * rowsPerPage;
	    }
	}
	// Adding Default Order
	if (!(getColumnName().contains(" ASC") == true || getColumnName().contains(" DESC") == true)) {
	    setColumnName(getColumnName() + " ASC");
	}
		
	tableSQL = "select * from (select *,ROW_NUMBER() OVER (ORDER BY " + getColumnName() + ") AS rowNumber from (" + filterQuery
		+ ")as  X ) as Y where rowNumber >= " + startIndex + " and rowNumber <= " + lastIndex;

	DatabaseResult dbResult = new DatabaseResult();
	int presentNumberOfRecords = 0;
	try {
	    dbResult = getPlatformService().loadResult(tableSQL, getFilterParamList());
	    presentNumberOfRecords = dbResult.getData().size();
	} catch (Exception e) {
	    logger.debug(e);
	}
	this.addParameter("result", dbResult);
	if (!dbResult.getData().isEmpty() && dbResult.getData().size() > 0)
	    this.addParameter("columnCount", dbResult.getData().get(0).size());
	this.addParameter("customTable", getTemplateName());
	this.addParameter("startIndex", startIndex);
	this.addParameter("noOfRecs", presentNumberOfRecords);
	this.addParameter("lastIndex", lastIndex);
	this.addParameter("totalRecords", noOfRecs);
	this.addParameter("javaScriptMethodName", javaScriptMethodName);
	this.addParameter("sortingColumn", getColumnName().split(",")[0]);
	this.addParameter("pageNumber", pageNumber);
	this.addParameter("tableName", tableName);
	if (StringUtils.isBlank(tableName)) {
	    this.addParameter("tableName", queryKeyName);
	}
	this.addParameter("prevPageNumber", pageNumber - 1);
	this.addParameter("nextPageNumber", pageNumber + 1);
	this.addParameter("enableNext", enableNext);
	if (isEnableFirstLastLinks()) {
	    this.addParameter("firstPageNumber", 1);
	    this.addParameter("lastPageNumber", noOfRecs / rowsPerPage + (noOfRecs % rowsPerPage > 0 ? 1 : 0));
	}
	buffer.append(FreeMarkerEngine.getInstance().evaluateTemplate("GenericTableTemplate.html", getParameters()));
    }

    public String getQueryString() {
	return queryString;
    }

    /**
     * To Set native query String
     * 
     * @param queryString
     */
    public void setQueryString(String queryString) {
	this.queryString = queryString;
    }

    public boolean isEnableFirstLastLinks() {
	return enableFirstLastLinks;
    }

    public void setEnableFirstLastLinks(boolean enableFirstLastLinks) {
	this.enableFirstLastLinks = enableFirstLastLinks;
    }

    public String getTableName() {
	return tableName;
    }

    public void setTableName(String tableName) {
	this.tableName = tableName;
    }

}
