/**
 * 
 */
package com.cydcor.framework.clickframework;

import java.util.HashMap;
import java.util.Map;

import net.sf.click.control.AbstractControl;
import net.sf.click.util.HtmlStringBuffer;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * This renders all controls of page Individually
 * 
 * @author ashwin
 * 
 */
public class GenericUIControl extends AbstractControl {
	private String controlName = "";
	private String templateName = "";
	private String responseText = "";
	private Map parameters = new HashMap();

	private String dataSource = "IMS";

	/**
	 * @return the dataSource
	 */
	public String getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource
	 *            the dataSource to set
	 */
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * @return
	 */
	protected PlatformService getPlatformService() {
		if (StringUtils.equalsIgnoreCase(dataSource, "LMS")) {
			return (PlatformService) ServiceLocator
					.getService("lmsPlatformService");
		} else {
			return (PlatformService) ServiceLocator
					.getService("platformService");
		}
	}

	/**
	 * @return the controlName
	 */
	public String getControlName() {
		return controlName;
	}

	/**
	 * @param controlName
	 *            the controlName to set
	 */
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName
	 *            the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return the parameters
	 */
	public Map getParameters() {
		return parameters;
	}

	/**
	 * @return the responseText
	 */
	public String getResponseText() {
		return responseText;
	}

	/**
	 * @param responseText
	 *            the responseText to set
	 */
	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}

	/**
	 * @param parameters
	 *            the parameters to set
	 */
	public void setParameters(Map parameters) {
		this.parameters = parameters;
	}

	/**
	 * @param key
	 * @param value
	 */
	public void addParameter(String key, Object value) {
		this.parameters.put(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.click.control.AbstractControl#getTag()
	 */
	@Override
	public String getTag() {
		// TODO Auto-generated method stub
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seenet.sf.click.control.AbstractControl#render(net.sf.click.util.
	 * HtmlStringBuffer)
	 */
	@Override
	public void render(HtmlStringBuffer buffer) {
		if (StringUtils.isBlank(templateName)) {
			buffer.append(responseText);
		} else {
			buffer.append(FreeMarkerEngine.getInstance().evaluateTemplate(
					templateName, parameters));
		}
	}

}
