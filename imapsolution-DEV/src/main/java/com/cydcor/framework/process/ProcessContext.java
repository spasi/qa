/**
 * 
 */
package com.cydcor.framework.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ashwin
 * 
 */
public class ProcessContext {
	private String processTaskName = "";
	private Map<String, Object> parameters = new HashMap<String, Object>();
	private Map<String, Object> paramMaps = new HashMap<String, Object>();

	/**
	 * @return the processTaskName
	 */
	public String getProcessTaskName() {
		return processTaskName;
	}

	/**
	 * @param processTaskName
	 *            the processTaskName to set
	 */
	public void setProcessTaskName(String processTaskName) {
		this.processTaskName = processTaskName;
	}

	public Map<String, Object> getParamMaps() {
		return paramMaps;
	}

	public void setParamMaps(Map<String, Object> paramMaps) {
		this.paramMaps = paramMaps;
	}

	private List<String> taskList = new ArrayList<String>();

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

	public void addParameter(String key, Object value) {
		this.parameters.put(key, value);
	}

	/**
	 * @return the taskList
	 */
	public List<String> getTaskList() {
		return taskList;
	}

	/**
	 * @param taskList
	 *            the taskList to set
	 */
	public void setTaskList(List<String> taskList) {
		this.taskList = taskList;
	}

	public void addTask(String task) {
		this.taskList.add(task);
	}

}
