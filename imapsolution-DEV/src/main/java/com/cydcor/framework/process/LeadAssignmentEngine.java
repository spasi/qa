package com.cydcor.framework.process;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import bsh.EvalError;
import bsh.Interpreter;

import com.cydcor.framework.task.Task;
import com.cydcor.framework.task.TaskContext;
import com.cydcor.framework.utils.CydcorUtils;

/**
 * Handles all Lead Assignment Tasks
 * 
 * @author ashwin
 * 
 */
public class LeadAssignmentEngine extends AbstractProcessEngine {
	private static final transient Log logger = LogFactory
			.getLog(LeadAssignmentEngine.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.process.AbstractProcessEngine#startProcess(com.cydcor
	 * .framework.process.ProcessContext)
	 */
	@Override
	public void startProcess(ProcessContext context) {
		// TODO Auto-generated method stub
		if (taskList == null || taskList.isEmpty()) {
			return;
		}

		super.startProcess(context);
		for (Task task : taskList) {
			TaskContext taskContext = new TaskContext();
			taskContext.setProcessContext(context);
			task.execute(taskContext);
		}

		taskList.clear();

		for (String taskName : context.getTaskList()) {
			Task task = null;
			task = createDynamicTask(task, taskName);
			addTask(task);
		}
		context.getTaskList().clear();
		startProcess(context);

	}

	/**
	 * @param task
	 * @return
	 */
	private static Task createDynamicTask(Task task, String template) {
		String templatePath = CydcorUtils.class.getClassLoader().getResource(
				"ftl/tasks/" + template).getPath();

		try {
			templatePath = templatePath.replaceAll("%20", " ");
			task = (Task) new Interpreter().source(templatePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.debug(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug(e);
		} catch (EvalError e) {
			// TODO Auto-generated catch block
			logger.debug(e);
		}
		return task;
	}

}
