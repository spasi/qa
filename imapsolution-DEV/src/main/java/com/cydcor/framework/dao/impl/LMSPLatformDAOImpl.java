package com.cydcor.framework.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.cydcor.framework.dao.LMSPlatformDAO;
import com.cydcor.framework.model.GenericModel;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * @author ashwin
 * 
 */
@Repository(value="lmsPlatformDAO")
public class LMSPLatformDAOImpl extends PLatformDAOImpl implements
		LMSPlatformDAO {
	private static final transient Log logger = LogFactory
			.getLog(LMSPLatformDAOImpl.class);

	private JdbcTemplate jdbcTemplate;

	/**
	 * @return the lmsJDBCTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		if (jdbcTemplate == null) {
			jdbcTemplate = (JdbcTemplate) ServiceLocator
					.getService("lmsJDBCTemplate");
		}
		return jdbcTemplate;
	}

	/**
	 * @param lmsJDBCTemplate
	 *            the lmsJDBCTemplate to set
	 */
	public void getJdbcTemplate(JdbcTemplate lmsJDBCTemplate) {
		this.jdbcTemplate = lmsJDBCTemplate;
	}

	public List<? extends GenericModel> loadLMSRowMapper(String sql,
			RowMapper rowMapper) {
		return getJdbcTemplate().query(sql, rowMapper);
	}

	public void updateLeadMaster(final List<Long> leadListIds) {
		logger.debug("UPDATE_LMS_LEADID_STATUS::: "
				+ QueryUtils.getQuery().get("UPDATE_LMS_LEADID_STATUS"));
		getJdbcTemplate().batchUpdate(
				QueryUtils.getQuery().get("UPDATE_LMS_LEADID_STATUS"),
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						populateLeadMaster(ps, leadListIds.get(i));

					}

					public int getBatchSize() {
						return leadListIds.size();
					}
				});
	}

	private void populateLeadMaster(PreparedStatement ps, Long leadId)
			throws SQLException {
		ps.setLong(1, leadId);
	}

}
