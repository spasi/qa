/**
 * 
 */
package com.cydcor.framework.dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.GenericModel;
import com.cydcor.framework.model.LeadLifeCycle;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.MerOffice;
import com.cydcor.framework.model.MerOfficeInsert;
import com.cydcor.framework.model.ReportComponent;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.model.Template;
import com.cydcor.framework.model.UserObject;

/**
 * Generic DAO which is used to load all Generic Table References
 * 
 * @author ashwin
 * 
 */

public interface PlatformDAO {
    /**
     * @param jdbcTemplate
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate);

    /**
     * @param lwbCode
     * @return
     */
    public UserObject loadUser(String lwbCode);

    /**
     * @param sql
     * @return
     */
    public DatabaseResult loadResult(String sql);

    /**
     * @param reportName
     * @return
     */
    public ReportComponent loadReport(String reportName);

    /**
     * Count Number of Rows Returned by this Query
     * 
     * @param sql
     * @return
     */
    public int countRows(String sql);

    /**
     * @param sql
     * @param rowMapper
     * @return
     */
    public List<? extends GenericModel> loadRowMapper(String sql, RowMapper rowMapper);

    /**
     * @param query
     * @param values
     */
    public void save(String query, Object[] values);

    /**
     * @param insertSql
     * @param values
     */
    public void populateLeadInput(List<LeadLifeCycle> values);

    /**
     * @param merOfficeInserts
     */
    public void populateIclMaster(List<MerOfficeInsert> merOfficeInserts);

    /**
     * @param sql
     */
    public void execute(String sql);

    /**
     * @param sql
     * @param args
     */
    public void update(String sql, Object[] args);

    /**
     * @param sql
     * @param values
     * @return
     */
    public DatabaseResult loadResult(String sql, List values);

    /**
     * @param template
     */
    public void loadTemplate(Template template);

    /**
     * 
     * @param values
     */
    public void updateLeadLifeCycle(final List<LeadLifeCycle> values);

    public void deleteOfficeSeqs(List<MerOffice> merOffice);

    public Long saveAndGetKey(final String sql, final Object[] args);

    void insertWireCenters(String[] values);

    void insertDAs(String[] values);

    List<LeadMaster> selectLeadsByLeadSheets(String leadSheetIds);

    String saveOrMergeLeadsheets(final String tempLeadSheetIds, final String savedLeadSheetId,
	    final boolean saveAllSheets, final boolean isAssigned, final String campaignSeq, final String officeSeq);

    public void updateDispositionAndNotes(final List<String> dis, final List<String> diDescs, final List<String> notes,
	    final List<String> rowIds, final String leadSheetId, String archived);

    public void populateCRMPrivileges(List<RoleObject> roleObjects);

    public void updateLeadsheetPrintStatus(List<String> values);

    void updateLeadSheets(List<String[]> values);
    
    public void upsertATTOwner(final String cyCode, final String merlinCode, final String sfOwnerId, final String region, final String user, final String active, final String action);
    
    public void upsertMergedOwners(final String fromOffice, final String toOffice, final String user, final String active, final String action);
}
