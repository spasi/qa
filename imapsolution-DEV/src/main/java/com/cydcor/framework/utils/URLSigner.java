package com.cydcor.framework.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.cydcor.framework.test.SpringPage;

/**
 * 
 * 
 * Static Maps API
 * 
 * The Google Static Maps API allows you to construct and retrieve map images
 * using parameters passed through its URL service. Full documentation of the
 * Google Static Maps V2 API is available at
 * http://code.google.com/apis/maps/documentation/staticmaps/. Static Maps API
 * requests using a Premier client ID have higher query limits.
 * 
 * To embed a map image in a webpage using your Google Maps API Premier client
 * ID, append a client parameter containing your client ID to the Static Map
 * URL, also passing a sensor parameter value.
 * http://maps.google.com/maps/api/staticmap
 * ?center=40.714728,-73.998672&zoom=12&
 * size=400x400&client=gme-yourclientid&sensor=false&signature=SIGNATURE
 * 
 * Note that you do not need to pass an API key. Premier users must now sign
 * their Static Map URL requests. (See URL Authentication for more information.)
 * 
 * Google Maps API Premier customers can also make requests to the Static Maps
 * API over https. In order to use the Static Maps API over https your client ID
 * must first be enabled for HTTPS access by Google Enterprise Support.
 * 
 * To embed a map image into a page retrieved over a secure https connection,
 * use the alternative hostname in your request URL as shown below:
 * https://maps-
 * api-ssl.google.com/maps/api/staticmap?center=40.714728,-73.998672
 * &zoom=12&size=400x400&client=gme-yourclientid&sensor=true_or_false
 * 
 * Note: Static Maps API Requests over https using a client ID that has not been
 * enabled for https will be rejected with a Status 400 "Bad Request" error.
 * 
 * @author ashwin
 * 
 */
public class URLSigner {

	// Note: Generally, you should store your private key someplace safe
	// and read them into your code

	private static SpringPage page = new SpringPage();
	private static String keyString = CydcorUtils.getProperty("PrivateKey");

	// The URL shown in these examples must be already
	// URL-encoded. In practice, you will likely have code
	// which assembles your URL from user or web service input
	// and plugs those values into its parameters.
	private static String urlString = "http://maps.google.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=12&size=400x400&client="
			+ CydcorUtils.getProperty("ClientID") + "&sensor=false";

	// This variable stores the binary key, which is computed from the string
	// (Base64) key
	private static byte[] key;

	private static URLSigner urlSigner = new URLSigner();

	public static void main(String[] args) throws IOException,
			InvalidKeyException, NoSuchAlgorithmException, URISyntaxException {

		// Convert the string to a URL so we can parse it
		URL url = new URL(urlString);

		String request = URLSigner.getInstance().signRequest(url.getPath(),
				url.getQuery());

		System.out.println("Signed URL :" + url.getProtocol() + "://" + url.getHost() + request);
		
		System.out.println(URLSigner.getInstance().getSignedURL("http://maps.google.com/maps/api/geocode/xml?address=314,East,Highland,Mall,Blvd,Suite,411&sensor=false&client=gme-cydcorinc"));
	}

	/**
	 * @return
	 */
	public static URLSigner getInstance() {
		return urlSigner;
	}

	/**
	 * @return
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public String getSignedURL(String requestURL) throws MalformedURLException,
			InvalidKeyException, NoSuchAlgorithmException,
			UnsupportedEncodingException, URISyntaxException {
		// Convert the string to a URL so we can parse it
		URL url = new URL(requestURL);

		String request = URLSigner.getInstance().signRequest(url.getPath(),
				url.getQuery());

		String signedURL = url.getProtocol() + "://" + url.getHost() + request;

		return signedURL;
	}

	private URLSigner() {
		try {
			new URLSigner(keyString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private URLSigner(String keyString) throws IOException {
		// Convert the key from 'web safe' base 64 to binary
		keyString = keyString.replace('-', '+');
		keyString = keyString.replace('_', '/');
		System.out.println("Key: " + keyString);
		this.key = Base64.decode(keyString);
	}

	public String signRequest(String path, String query)
			throws NoSuchAlgorithmException, InvalidKeyException,
			UnsupportedEncodingException, URISyntaxException {

		// Retrieve the proper URL components to sign
		String resource = path + '?' + query;

		// Get an HMAC-SHA1 signing key from the raw key bytes
		SecretKeySpec sha1Key = new SecretKeySpec(key, "HmacSHA1");

		// Get an HMAC-SHA1 Mac instance and initialize it with the HMAC-SHA1
		// key
		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(sha1Key);

		// compute the binary signature for the request
		byte[] sigBytes = mac.doFinal(resource.getBytes());

		// base 64 encode the binary signature
		String signature = Base64.encodeBytes(sigBytes);

		// convert the signature to 'web safe' base 64
		signature = signature.replace('+', '-');
		signature = signature.replace('/', '_');

		return resource + "&signature=" + signature;
	}
}
