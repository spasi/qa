package com.cydcor.framework.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.model.query.Queries;
import com.cydcor.framework.model.query.Query;

public class QueryUtils {
	private static final transient Log logger = LogFactory
			.getLog(QueryUtils.class);

	private static String xml = null;
	private static Map<String, String> queryMap = new HashMap<String, String>();
	private static QueryUtils queryUtils = new QueryUtils();

	private static QueryUtils getInstance() {
		return queryUtils;
	}

	private QueryUtils() {
		try {
			String xmlPath = CydcorContext.getInstance().getRequestContext()
					.getRequestWrapper().getRealPath("/")
					+ "/WEB-INF/classes/queries/queries.xml";
			logger.debug("Trying to Load Queries from :" + xmlPath);
			init(xmlPath);
		} catch (Exception e) {
			try {
				String xmlPath = CydcorContext.getInstance().getBasePath()
						+ "/WEB-INF/classes/queries/queries.xml";
				logger
						.debug("Trying to Load Queries from as it failed in previous step :"
								+ xmlPath);
				init(xmlPath);
			} catch (Exception e1) {
				reload();
			}
		}
	}

	private void init(String url) {
		xml = FileUtils.readFile(url);
		Queries queries = (Queries) XStreamUtils.getObjectFromXml(
				Queries.class, xml);
		getQueryMap(queries.getQuery());
	}

	// reload queries again
	private void reload() {
		try {
			xml = FileUtils.readFile(CydcorUtils.class.getClassLoader()
					.getResourceAsStream("classes/queries/queries.xml"));
			logger
					.debug("Trying to Load Queries from as it failed in last try :classes/queries/queries.xml");
		} catch (Exception e) {
			// TODO: handle exception
			xml = FileUtils.readFile(CydcorUtils.class.getClassLoader()
					.getResourceAsStream("queries/queries.xml"));
			logger
					.debug("Trying to Load Queries from as it failed in last try :queries/queries.xml");
		}
		Queries queries = (Queries) XStreamUtils.getObjectFromXml(
				Queries.class, xml);
		getQueryMap(queries.getQuery());
	}

	/**
	 * @return the queryMap
	 */
	private static Map<String, String> getQueryMap(List<Query> query) {
		queryMap.clear();
		for (Query quer : query) {
			queryMap.put(quer.getKey().trim(), quer.getValue().trim());
		}
		return queryMap;
	}

	public static Map<String, String> getQuery() {
		return queryMap;
	}

	public static void main(String[] args) {
		logger.debug(getQuery().keySet());
	}
}
