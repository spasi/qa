package com.cydcor.framework.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import net.sf.json.JSONObject;
import net.sf.json.util.JSONTokener;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.cydcor.framework.model.xml.v3.directions.Distance;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

public class PostMethodExample {

  public static void main(String args[]) {

    HttpClient client = new HttpClient();
    //client.getParams().setParameter("http.useragent", "Test Client");

    BufferedReader br = null;

    //PostMethod method = new PostMethod("http://search.yahoo.com/search");
    //http://maps.googleapis.com/maps/api/distancematrix/json?origins=Vancouver+BC|Seattle&destinations=San+Francisco|Victoria+BC&mode=bicycling&language=fr-FR&sensor=false
    PostMethod method = new PostMethod("http://maps.googleapis.com/maps/api/distancematrix/json?origins=Vancouver&destinations=San+Francisco&mode=driving&language=fr-FR&sensor=false");
   // method.addParameter("p", "\"java2s\"");

    try{
      int returnCode = client.executeMethod(method);

      if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
        System.err.println("The Post method is not implemented by this URI");
        // still consume the response body
        method.getResponseBodyAsString();
      } else {
    	  String bString = method.getResponseBodyAsString();
    	  JSONObject jobj = JSONObject.fromObject(bString);
    	  jobj.getJSONArray("rows");
//    	  for(int i=0;)
    	  System.out.println("distance="+jobj.getJSONArray("rows"));
//    	 
//	  		XStream xstream = new XStream(new JettisonMappedXmlDriver());
//	  		xstream.alias("distance", Distance.class);
//	  		Distance distance = (Distance)xstream.fromXML(bString);
//	  		System.out.println(distance.getText());
//	  		System.out.println(distance.getValue());
    	  
    	  JSONTokener json = new JSONTokener(bString);
    	  br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
    	  
    	  String readLine;
    	  while(((readLine = br.readLine()) != null)) {
    		  System.err.println(readLine);
    	  }
      }
    } catch (Exception e) {
      System.err.println(e);
    } finally {
      method.releaseConnection();
      if(br != null) try { br.close(); } catch (Exception fe) {}
    }

  }
}

