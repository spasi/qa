/**
 * 
 */
package com.cydcor.framework.utils;

import java.awt.Color;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.census.CampaignMarkerDetailObject;
import com.cydcor.framework.census.MarkerImage;
import com.cydcor.framework.context.LogContext;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.service.PlatformService;

/**
 * @author ashwin
 * 
 */
public class CydcorUtils {
    private static final transient Log logger = LogFactory.getLog(CydcorUtils.class);
    private static Map<Integer, String> opacityMap = new HashMap<Integer, String>();
    private static PropertyUtils propertyUtils = (PropertyUtils) ServiceLocator.getService("propertyUtils");
    private static Map<String, Integer> generateSheets = new HashMap<String, Integer>();
    private static Map<String, Map> generateSheetsWithMap = new HashMap<String, Map>();
    private static Boolean isAllLeadsProcessed = false;
    static {
	opacityMap.put(0, "00");
	opacityMap.put(1, "11");
	opacityMap.put(2, "22");
	opacityMap.put(3, "33");
	opacityMap.put(4, "44");
	opacityMap.put(5, "55");
	opacityMap.put(6, "66");
	opacityMap.put(7, "77");
	opacityMap.put(8, "88");
	opacityMap.put(9, "99");
	opacityMap.put(10, "aa");
	opacityMap.put(11, "bb");
	opacityMap.put(12, "cc");
	opacityMap.put(13, "dd");
	opacityMap.put(14, "ee");
	opacityMap.put(15, "ff");
    }

    /**
     * @param rgbvalue
     * @return
     */
    public static String rgbToHexCode(String rgbvalue) {
	return rgbToHexCode(rgbvalue, 0);
    }

    /**
     * @param rgbvalue
     * @return
     */
    public static String rgbToHexCode(String rgbvalue, float opacity) {
	int opacityInt = Math.round(opacity * 16);

	String hexaValue = null;
	if (StringUtils.isBlank(rgbvalue))
	    return "0000FF";
	if (rgbvalue.contains("rgb")) {
	    rgbvalue = rgbvalue.replaceAll("rgb|\\(|\\)", "");
	    String[] values = rgbvalue.split(",");
	    Color color = new Color(Integer.parseInt(values[0].trim()), Integer.parseInt(values[1].trim()), Integer.parseInt(values[2]
		    .trim()));
	    hexaValue = Integer.toHexString(color.getRGB());
	} else {
	    hexaValue = rgbvalue;
	}

	String opacityString = opacityMap.get(opacityInt);
	if (hexaValue.length() == 8) {
	    // hexaValue = opacityString+ hexaValue.substring(2,
	    // hexaValue.length());
	} else {
	    hexaValue = opacityString + hexaValue;
	}
	return hexaValue;
    }

    public static void log(LogContext context) {

	getPlatformService().update(
		"insert into IMS.IMS_MAP_LOG values(?,?,?,?,?,?)",
		new Object[] { context.getAccessUser(), context.getAccessDate(), context.getIpAdress(), context.getPageName(),
			context.getMapType(), context.getMapModule() });
    }

    private static PlatformService getPlatformService() {
	return ServiceLocator.getService(PlatformService.class);
    }

    /**
     * @param fullAddr
     * @return
     */
    public static String sanitizeAddresses(String fullAddr) {
	String address = fullAddr;
	if (StringUtils.contains(address, " - ")) {
	    address = StringUtils.replace(address, " - ", ",");
	} else if (StringUtils.contains(address, "-")) {
	    address = StringUtils.replace(address, "-", ",");
	}

	if (StringUtils.contains(address, "#")) {
	    address = StringUtils.replace(address, "#", ",");
	}

	address = StringUtils.replace(address, ",,", " ");
	address = StringUtils.replace(address, ",", " ");
	return address.trim();
    }

    /**
     * @param key
     * @return
     */
    public static String getProperty(String key) {
	try {
	    if (propertyUtils.getProperty().containsKey(key))
		return propertyUtils.getProperty().get(key).toString();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    logger.debug(e);
	    return "";
	}
	return "";
    }

    public static void increaseCount(String key, int value) {
	if (!generateSheets.containsKey(key)) {
	    generateSheets.put(key, 0);
	}
	generateSheets.put(key, generateSheets.get(key) + value);
    }

    public static int getCount(String key) {
	if (!generateSheets.containsKey(key)) {
	    return 0;
	}
	return generateSheets.get(key);

    }

    public static void resetCount() {
	generateSheets.clear();
	generateSheetsWithMap.clear();
    }

    /**
     * @param label
     * @param uniqueKey
     * @param value
     */
    public static void increaseCount(String label, String uniqueKey, int value) {
	if (!generateSheets.containsKey(label)) {
	    generateSheetsWithMap.put(label, new HashMap<String, Integer>());
	}

	Map<String, Integer> keyMap = generateSheetsWithMap.get(label);

	if (!keyMap.containsKey(uniqueKey)) {
	    keyMap.put(uniqueKey, 0);
	}

	generateSheetsWithMap.put(label, keyMap);
    }

    /**
     * @param label
     * @param uniqueKey
     * @return
     */
    public static int getCount(String label, String uniqueKey) {
	if (!generateSheetsWithMap.containsKey(label)) {
	    return 0;
	}

	Map<String, Integer> keyMap = generateSheetsWithMap.get(label);
	if (keyMap == null) {
	    return 0;
	}

	return keyMap.size();

    }

    public static boolean isAllLeadsProcessed() {
	return isAllLeadsProcessed;
    }

    public static void setAllLeadsProcessed(boolean isDone) {
	isAllLeadsProcessed = isDone;
    }

    public static boolean isEqualInt(int x, int y) {
	return x == y;
    }

    public static void testNumberFormat() {
	// The 0 symbol shows a digit or 0 if no digit present
	NumberFormat formatter = new DecimalFormat("000000");
	String s = formatter.format(1231233424); // -001235
	// notice that the number was rounded up

	// The # symbol shows a digit or nothing if no digit present
	formatter = new DecimalFormat("#####################");
	s = formatter.format(1231233424); // -1235
	//System.out.println(s);

	s = formatter.format(0); // 0
	formatter = new DecimalFormat("##00");
	s = formatter.format(0); // 00
	//System.out.println(s);
	// The . symbol indicates the decimal point
	formatter = new DecimalFormat(".00");
	s = formatter.format(-.567); // -.57
	formatter = new DecimalFormat("0.00");
	s = formatter.format(-.567); // -0.57
	formatter = new DecimalFormat("#.#");
	s = formatter.format(-1234.567); // -1234.6
	formatter = new DecimalFormat("#.######");
	s = formatter.format(-1234.567); // -1234.567
	formatter = new DecimalFormat(".######");
	s = formatter.format(-1234.567); // -1234.567
	formatter = new DecimalFormat("#.000000");
	s = formatter.format(-1234.567); // -1234.567000

	// The , symbol is used to group numbers
	formatter = new DecimalFormat("#,###,###");
	s = formatter.format(-1234.567); // -1,235
	s = formatter.format(-1234567.890); // -1,234,568

	// The ; symbol is used to specify an alternate pattern for negative
	// values
	formatter = new DecimalFormat("#;(#)");
	s = formatter.format(-1234.567); // (1235)

	// The ' symbol is used to quote literal symbols
	formatter = new DecimalFormat("'#'#");
	s = formatter.format(-1234.567); // -#1235
	formatter = new DecimalFormat("'abc'#");
	s = formatter.format(-1234.567); // -abc1235
    }

    public static void systemNumberFormat() {
	// Print out a number using the localized number, integer, currency,
	// and percent format for each locale
	Locale[] locales = NumberFormat.getAvailableLocales();
	double myNumber = -1234.56;
	NumberFormat form;
	for (int j = 0; j < 4; ++j) {
	    //System.out.println("FORMAT");
	    for (int i = 0; i < locales.length; ++i) {
		if (locales[i].getCountry().length() == 0) {
		    continue; // Skip language-only locales
		}
		System.out.print(locales[i].getDisplayName());
		switch (j) {
		case 0:
		    form = NumberFormat.getInstance(locales[i]);
		    break;
		case 1:
		    form = NumberFormat.getIntegerInstance(locales[i]);
		    break;
		case 2:
		    form = NumberFormat.getCurrencyInstance(locales[i]);
		    break;
		default:
		    form = NumberFormat.getPercentInstance(locales[i]);
		    break;
		}
		if (form instanceof DecimalFormat) {
		    System.out.print(": " + ((DecimalFormat) form).toPattern());
		}
		System.out.print(" -> " + form.format(myNumber));
		try {
		    System.out.println(" -> " + form.parse(form.format(myNumber)));
		} catch (ParseException e) {
		}
	    }
	}
    }

    public static void main(String[] args) {
	testNumberFormat();
    }

    public static String getKMLforLeadSheets(String leadSheetIds) {
	List<LeadMaster> leads = getPlatformService().selectLeadsByLeadSheets(leadSheetIds);
	if (leads == null)
	    return null;
	CampaignMarkerDetailObject markerDetailObject = new CampaignMarkerDetailObject();
	for (LeadMaster lead : leads) {
	    if (StringUtils.isBlank(lead.getImageIcon())) {
		lead.setImageIcon("redDot");
	    } else {
		try {
		    String imageIcon = lead.getImageIcon();
		    int lastIndexOfSlash = imageIcon.lastIndexOf("/");
		    int lastIndexOfDot = imageIcon.lastIndexOf(".");
		    String imageName = imageIcon.substring(lastIndexOfSlash + 1, lastIndexOfDot);
		    MarkerImage markerImage = new MarkerImage();
		    markerImage.setStyleId(imageName);
		    markerImage.setImageUrl(imageIcon);

		    markerDetailObject.getMarkerList().add(markerImage);
		    lead.setImageIcon(imageName);
		} catch (Exception e) {
		    lead.setImageIcon("redDot");
		}
	    }
	}

	markerDetailObject.setLeads(leads);
	Map<String, Object> params = new HashMap<String, Object>();
	params.put("leads", markerDetailObject);
	return FreeMarkerEngine.getInstance().evaluateTemplate("Geometry.xml", params);

    }

}
