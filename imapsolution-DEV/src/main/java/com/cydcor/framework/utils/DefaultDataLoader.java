/**
 * 
 */
package com.cydcor.framework.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.DBEntityComponent;
import com.cydcor.framework.model.DataLoadCommand;

/**
 * @author ashwin
 * 
 */
public class DefaultDataLoader extends DataLoader {
	private static final transient Log logger = LogFactory.getLog(DefaultDataLoader.class);
	/*
	 * (non-Javadoc)
	 * 
	 * @seecom.linkwithweb.healthcare.utils.DataLoader#getData(com.linkwithweb.
	 * healthcare.model.TableComponent)
	 */
	@Override
	public List<List<Object>> getData(DBEntityComponent component) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.linkwithweb.healthcare.utils.DataLoader#loadTableMetadata(com.linkwithweb
	 * .healthcare.model.DataLoadCommand)
	 */
	@Override
	public DBEntityComponent loadTableMetadata(DataLoadCommand dataLoadCommand) {
		// TODO Auto-generated method stub
		DBEntityComponent component = new DBEntityComponent();

		StringBuffer query = new StringBuffer();

		// First Build Query
		buildQuery(query, dataLoadCommand);

		// Now fire Query and Build TableComponet Out of it
		ResultSet resultSet = null;
		// Get Connection and Get Result Set

		try {
			//resultSet.get
			while (resultSet.next()) {
				// Get Details of Column
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.debug(e);
		}

		return component;
	}

	/**
	 * @param query
	 */
	private void buildQuery(StringBuffer query, DataLoadCommand dataLoadCommand) {
		// TODO Auto-generated method stub
		buildSelectClause(query, dataLoadCommand);
		buildFilterClause(query, dataLoadCommand);
		buildGroupByClause(query, dataLoadCommand);
		buildGroupByClause(query, dataLoadCommand);
		buildFilterClause(query, dataLoadCommand);
	}

	private void buildGroupByClause(StringBuffer query,
			DataLoadCommand dataLoadCommand) {
		// TODO Auto-generated method stub

	}

	private void buildFilterClause(StringBuffer query,
			DataLoadCommand dataLoadCommand) {
		// TODO Auto-generated method stub

	}

	private void buildSelectClause(StringBuffer query,
			DataLoadCommand dataLoadCommand) {
		// TODO Auto-generated method stub

	}

}
