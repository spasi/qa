package com.cydcor.framework.utils;

import java.util.HashMap;
import java.util.Map;

public class TemplateRules {

    private static Map<String, String> rules = new HashMap<String, String>();

    static {
	rules.put("BadCredit", "F");
	rules.put("UnderCompetetiorContract", "C");
	rules.put("FailedAddressValidation", "A");
	rules.put("FCasePreviously", "");
	rules.put("LanguagePreference", "L");
	rules.put("Visited", "");
	rules.put("VisitedInLast6Months", "");
	rules.put("Dispositioned", "");
	rules.put("HomePhoneService", "");
	rules.put("HasInternetService", "");
	rules.put("CellPhoneService", "");
	rules.put("NumMonthsFromDateOfOrigin", "");
    }

    public static boolean containsKey(String keyName) {
	return rules.containsKey(keyName);
    }
    
    public static String getValue(String keyName) {
	return rules.get(keyName);
    }

}
