package com.cydcor.framework.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	private static SimpleDateFormat dateFormat = null;
	private static SimpleDateFormat dateFormat1 = null;
	static {
		dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat1 = new SimpleDateFormat("MM/dd/yy");
	 	
	}

	public static Calendar getCalenderDateForCRM(String value) throws ParseException {
		Date date = null;
		Calendar cal = Calendar.getInstance();
		date = dateFormat.parse(value);
		cal.setTime(date);
		return cal;
	}
	public static Calendar getCalenderDate(String value) throws ParseException {
		Date date = null;
		Calendar cal = Calendar.getInstance();
		date = dateFormat1.parse(value);
		cal.setTime(date);
		return cal;
	}
	
	public static Calendar getCalenderDateForCRM(Timestamp value) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(value);
		return cal;
	}
	
}
