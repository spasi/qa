package com.cydcor.framework.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.service.PlatformService;
import com.mysql.jdbc.StringUtils;

public class CodeUtils {

	private static final transient Log logger = LogFactory.getLog(CodeUtils.class);
	private static List<MapObject> dispositList = new ArrayList<MapObject>();
	private static MapObject dispositionMap = null;
	protected static PlatformService getPlatformService() {
		return ServiceLocator.getService(PlatformService.class);
	}

	static {
		if (dispositList.size() == 0) {
			dispositionMap = new MapObject();
			dispositionMap.setKey("NONE");
			dispositionMap.setValue("NONE");
			dispositList.add(dispositionMap);

			DatabaseResult result = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_DISPOSITION_LIST"));

			for (List<MapObject> obj : result.getData()) {
				dispositionMap = new MapObject();
				dispositionMap.setKey(obj.get(0).getValue());
				dispositionMap.setValue(obj.get(0).getValue() + " - " + obj.get(1).getValue());
				dispositionMap.setActualValue(obj.get(1).getValue());
				dispositList.add(dispositionMap);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		generatePojoClass(" com.cydcor.framework.model", "MerICLDetails", "MERLIN.OFFICES");
		generateRowMapper("com.cydcor.framework.rowmapper", "MerICLDetailsRowMapper", "MerICLDetails", "MERLIN.OFFICES");
	}

	/**
	 * @throws IOException
	 */
	private static void generatePojoClass(String packageName, String pojoClassName, String tableName) throws IOException {

		String sql = "select * from " + tableName + " where 1=2";
		Statement st = null;
		String dbtype = "";
		String columnName = "";
		String privates = "private";
		FileOutputStream fos = null;
		File file = null;
		try {
			file = new File("c://temp//" + pojoClassName + ".java");
			fos = new FileOutputStream(file);
			st = ConnectionUtil.getStatement();
			ResultSet rs = st.executeQuery(sql);
			int columnCount = rs.getMetaData().getColumnCount();

			StringBuilder sb = new StringBuilder("package " + packageName + ";");
			sb.append("\n");
			sb.append("public class " + pojoClassName + " extends GenericModel{");
			sb.append("\n");

			for (int i = 1; i <= columnCount; i++) {
				dbtype = rs.getMetaData().getColumnTypeName(i);
				columnName = rs.getMetaData().getColumnLabel(i);
				if (StringUtils.startsWithIgnoreCase(dbtype, "int")) {
					sb.append(privates + " " + " long " + columnName.toLowerCase() + ";");
					sb.append("\n");
				} else if (StringUtils.startsWithIgnoreCase(dbtype, "DATE")) {
					sb.append(privates + " " + " Timestamp " + columnName.toLowerCase() + ";");
					sb.append("\n");
				} else {
					sb.append(privates + " " + " String " + columnName.toLowerCase() + ";");
					sb.append("\n");
				}
				// logger.debug(rs.getMetaData().getColumnName(i));
			}
			sb.append("}");
			fos.write(sb.toString().getBytes());
		} catch (Exception e) {
			logger.debug(e);
		} finally {

			logger.debug("File with name " + pojoClassName + ".java written to " + file.getAbsolutePath());
			fos.close();
			ConnectionUtil.closeConnection();
		}
	}

	/**
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 */
	private static void generateRowMapper(String packageName, String className, String modelObjectName, String tableName) throws FileNotFoundException,
			SQLException, IOException {
		Statement st = null;
		String instance = Character.toLowerCase(modelObjectName.charAt(0)) + modelObjectName.substring(1, modelObjectName.length());

		String columnName = "";
		String dbColumnType = "";
		String firstLetterToupper = "";
		FileOutputStream fos = null;
		File file = null;
		try {
			file = new File("c://temp//" + className + ".java");
			fos = new FileOutputStream(file);
			StringBuilder sb = new StringBuilder("package " + packageName + ";");
			sb.append("\n");
			sb.append("public class " + className + " implements RowMapper {");
			sb.append("\n");
			sb.append("public Object mapRow(ResultSet rs, int rowNum) throws SQLException {");
			sb.append("\n");
			sb.append(modelObjectName + " " + instance + " = ");
			sb.append(" new " + modelObjectName + "();");
			sb.append("\n");

			String sql = "select * from " + tableName + " where 1=2";
			st = ConnectionUtil.getStatement();
			ResultSet rs = st.executeQuery(sql);
			int columnCount = rs.getMetaData().getColumnCount();

			for (int i = 1; i <= columnCount; i++) {
				columnName = rs.getMetaData().getColumnLabel(i);
				dbColumnType = rs.getMetaData().getColumnTypeName(i);
				firstLetterToupper = Character.toUpperCase(columnName.charAt(0)) + columnName.substring(1, columnName.length()).toLowerCase();
				if (StringUtils.startsWithIgnoreCase(dbColumnType, "int")) {
					sb.append(instance + "." + "set" + firstLetterToupper + "(rs.getLong(\"" + columnName + "\"));");
					sb.append("\n");
				} else if (dbColumnType.equalsIgnoreCase("DATETIME")) {
					sb.append(instance + "." + "set" + firstLetterToupper + "(rs.getTimestamp(\"" + columnName + "\"));");
					sb.append("\n");
				} else if (dbColumnType.equalsIgnoreCase("DATE")) {
					sb.append(instance + "." + "set" + firstLetterToupper + "(rs.getDate(\"" + columnName + "\"));");
					sb.append("\n");
				} else {
					sb.append(instance + "." + "set" + firstLetterToupper + "(rs.getString(\"" + columnName + "\"));");
					sb.append("\n");
				}
			}
			sb.append("return " + instance + ";");
			sb.append("\n");

			sb.append("}");
			sb.append("\n");
			sb.append("}");
			fos.write(sb.toString().getBytes());
		} catch (Exception e) {
			logger.debug(e);
		} finally {
			logger.debug("File with name " + className + ".java written to " + file.getAbsolutePath());
			fos.close();
			ConnectionUtil.closeConnection();
		}

	}

	public static String getDispositionDescription(String key) {
		String val = "";
		// List<MapObject> dispositList = null;
		Iterator<MapObject> iterator = dispositList.iterator();

		while (iterator.hasNext()) {
			MapObject mapObject = (MapObject) iterator.next();
			if (org.apache.commons.lang.StringUtils.equalsIgnoreCase(key, mapObject.getKey())) {
				val = (String)mapObject.getActualValue();
				break;
			}
		}
		return val;
	}

	public static List<MapObject> getDispositionMap() {
		return dispositList;
	}

}