/**
 *
 */
package com.cydcor.framework.utils;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.axis.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.exceptions.GenericException;

/**
 * @author agkrishnan This is a simple utility that manages exception handling
 *         and retries for SQL code and WS Call It expects callable classes to
 *         implement a sub-interface of the Retryable Interface. Additionally,
 *         the class expects callee-s to register objects like OD Session
 *         strings, SQL Connections, Statements and result sets with it for
 *         automatic closure.
 * 
 *         This class does not provide additional hooks for transaction
 *         management. Some overhead may occur in cases where multiple OD
 *         Sessions are used by the callee and registered with this class for
 *         cleanup (Upon a WS exception on any call, this class destroys all OD
 *         instances registered with it).
 */
public class RetryableRunner {
	protected final Log log = LogFactory.getLog(getClass());
	private List<Connection> connList;
	private List<ResultSet> resultsList;
	private List<Statement> stmtList;
	private List<String> odSessList;
	private int maxRetryCount = 0;

	public RetryableRunner() {
		connList = new ArrayList<Connection>();
		resultsList = new ArrayList<ResultSet>();
		stmtList = new ArrayList<Statement>();
		odSessList = new ArrayList<String>();
	}

	public RetryableRunner(int retryCount) {
		connList = new ArrayList<Connection>();
		resultsList = new ArrayList<ResultSet>();
		stmtList = new ArrayList<Statement>();
		odSessList = new ArrayList<String>();

		if (retryCount > 0)
			maxRetryCount = retryCount;
	}

	public void registerConnectionToClose(Connection conn) {
		if (conn != null) {
			connList.add(conn);
		}
	}

	public void registerStatementToClose(Statement stmt) {
		if (stmt != null) {
			stmtList.add(stmt);
		}
	}

	public void registerResultSetToClose(ResultSet rs) {
		if (rs != null) {
			resultsList.add(rs);
		}
	}

	public void registerODSessionToClose(String conn) {
		if (conn != null) {
			odSessList.add(conn);
		}
	}

	public void run(Retryable item) throws GenericException {
		if ((item == null) || !(item instanceof Retryable)) {
			throw new GenericException(
					"Invalid use of Retryable Runner. Paramater of type Retryable cannot be null and must be sub-interface of Retryable.");
		}

		if (item instanceof WSRetryable) {
			runWS((WSRetryable) item);
		}

		if (item instanceof SQLRetryable) {
			runSQL((SQLRetryable) item);
		}
	}

	protected void runSQL(SQLRetryable item) throws GenericException {
		int retryCount = 0;
		boolean hasException;

		do {
			hasException = false;

			try {
				item.run(this);

				return;
			} catch (SQLException sqlex) {
				log.error("Error while executing sql ", sqlex);

				hasException = true;
			} catch (GenericException te) {
				log.error("Fatal exception, will not retry", te);
				hasException = true;
				throw te;
			} catch (Exception e) {
				log.error("Generic Exception while Querying the DB", e);
				hasException = true;
				if (retryCount == maxRetryCount - 1) {
					// throw new StarterkitException(e);
				}

			} finally {
				retryCount++;

				if (hasException && (retryCount < maxRetryCount)) {
					// sleep for some time to avoid temporary errors
					// CommonUtil.sleepWOException();
					// clear before the retry;
					item.reInitForRetry();
				}
			}
		} while (hasException && (retryCount < maxRetryCount));

		throw new GenericException(
				"Unable to complete SQL call after repeated attempts.");
	}

	protected void cleanUpForSQLRun(boolean hasException) {
		Iterator<ResultSet> results = resultsList.iterator();

		for (int i = 0; results.hasNext(); i++) {
			try {
				ResultSet result = results.next();

				if (result != null) {
					result.close();
				}
			} catch (Exception ex) {
				log.error("Error in closing Resultset", ex);
			}

			results.remove();
		}

		Iterator<Statement> statements = stmtList.iterator();

		for (; statements.hasNext();) {
			Statement statement = statements.next();

			try {
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				log.error("Error in closing statement", e);
			}

			statements.remove();
		}

		Iterator<Connection> connections = connList.iterator();

		for (; connections.hasNext();) {
			Connection connection = connections.next();

			try {
				if (connection != null) {
					if (connection.getAutoCommit() == false && hasException) {
						try {
							connection.rollback();
						} catch (RuntimeException e) {
							log
									.error(
											"Runtime Exception while rolling back transactions",
											e);
						}
						connection.setAutoCommit(true);// incase the Retryable
														// instance used some
														// form of transaction

					}
					connection.close();
				}
			} catch (Exception e) {
				log.error("Error in closing connection", e);
			}

			connections.remove();
		}
	}

	protected void cleanUpForWSRun(boolean hasException) {
		Iterator<String> sessions = odSessList.iterator();

		for (; sessions.hasNext();) {
			try {
				String odSession = sessions.next();

				if ((odSession != null) && hasException) {
					ODSessionPool.getInstance().destroyODSession(odSession);
				} else {
					ODSessionPool.getInstance().returnObject(odSession);
				}
			} catch (Exception ex) {
				log.error("Error in closing OD Sessions", ex);
			}

			sessions.remove();
		}
	}

	protected void runWS(WSRetryable item) throws GenericException {
		int retryCount = 0;
		boolean hasException;
		Throwable lastException;

		do {
			hasException = false;

			try {
				item.run(this);

				return;
			} catch (AxisFault af) {
				hasException = true;
				log.error("An Error occurred while trying a WS Call", af);
				lastException = af;
			} catch (RemoteException rEx) {
				hasException = true;
				log.error("An Error occurred while trying a WS Call", rEx);
				lastException = rEx;
			} catch (GenericException te) {
				log.error("FATAL Exception, will not retry", te);
				hasException = true;
				throw te;
			} catch (Exception e) {
				// We've decided to retry in this block as well since we want to
				// retry even if an OD session is not available.
				log.error("Generic Exception while trying a WS call", e);
				hasException = true;
				lastException = e;
			} finally {
				cleanUpForWSRun(hasException);
				retryCount++;

				/*
				 * if (hasException && (retryCount <
				 * StarterkitConstants.OD_RETRY_COUNT)) { //sleep for some time
				 * to avoid temporary errors CommonUtil.sleepWOException();
				 * 
				 * //clear before the retry; item.reInitForRetry(); }
				 */
			}
			// TODO SHOULD CHANGE 2
		} while (hasException && (retryCount < 2));

		if (lastException == null || lastException.getMessage() == null
				|| "".equals(lastException.getMessage())) {
			throw new GenericException(
					"Unable to complete Web Service call after repeated attempts.");
		} else {
			throw new GenericException(lastException, retryCount);
		}

	}
}
