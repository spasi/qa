package com.cydcor.framework.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool.PoolableObjectFactory;


public class ODSessionFactory implements PoolableObjectFactory {
	private static final transient Log logger = LogFactory.getLog(ODSessionFactory.class);
    private static final String JSESSIONID = "JSESSIONID=";
    private static final String PATHSTR = "; path=/Services";
    private String wsLocation = null;
    private String userName = null;
    private String password = null;

    public ODSessionFactory() {
        super();
        // Get the login parameters from property file
        try {
			wsLocation = PropertyUtils.getInstance().getProperty().getProperty("wsLocation");
			userName = PropertyUtils.getInstance().getProperty().getProperty("userName");
		    password = PropertyUtils.getInstance().getProperty().getProperty("password");
		} catch (IOException e) {
			logger.debug(e);
		}
       
    }

    /**
     * Activate method is empty as to avoid the calling the OnDemand again
     * after the session is created.
     * borrowObject() in the GenericObjectPool will call makeObject() and
     * then activateObject().
     * keepSessionAlive() is the new method added which does the session activation.
     */
    public void activateObject(Object arg0) throws Exception {
    }

    /**
     * This method logs out the given Session Id in On Demand.
     * @param sessionId
     * @throws Exception
     */
    public void destroyObject(Object sessionId) throws Exception {
        URL wsURL = new URL(wsLocation + "?command=logoff");
        HttpURLConnection wsConnection = null;
        logger.debug("Destroying OD Session with Id" + sessionId);

        try {
            String sessionString = (String) sessionId;
            // create an HTTPS connection to the OnDemand webservices
            wsConnection = (HttpURLConnection) wsURL.openConnection();
            // we don't want any caching to occur
            wsConnection.setUseCaches(false);

            // let it know which session we're logging off of
            wsConnection.setRequestProperty("Cookie", "JSESSIONID=" + sessionString);
            wsConnection.setRequestMethod("GET");
            wsConnection.setConnectTimeout(30000);
            wsConnection.setReadTimeout(30000);
            
            // see if we got a successful response
            if (wsConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // if you care that a logoff was successful, do that code here
                // showResponseHttpHeaders(wsConnection);
            }
        } catch (Exception e) {
            logger.debug(e);
        } finally {
            try {
                if (wsConnection != null) {
                    wsConnection.disconnect();
                    wsConnection = null;
                }
            } catch (Exception e) {
                logger.debug(e);
            }
        }
    }

    /**
     * This method creates a new OnDemand session and returns the
     * session id for the same.
     * @throws Exception
     */
    public Object makeObject() throws Exception {
        String sessionString = "FAIL";
        URL wsURL = new URL(wsLocation + "?command=login");
        HttpURLConnection wsConnection = null;

        try {
            // create an HTTPS connection to the OnDemand webservices
            wsConnection = (HttpURLConnection) wsURL.openConnection();
            // we don't want any caching to occur
            wsConnection.setUseCaches(false);
            // set some http headers to indicate the username and password we are using to logon
            wsConnection.setRequestProperty("UserName", userName);
            wsConnection.setRequestProperty("Password", password);
            wsConnection.setRequestMethod("GET");
            wsConnection.setConnectTimeout(30000);
            wsConnection.setReadTimeout(30000);
            wsConnection.connect();

            // see if we got a successful response
            if (wsConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // get the session id from the cookie setting
                if (wsConnection.getHeaderFields().containsKey("Set-Cookie")) {
                    //sessionString = wsConnection.getHeaderField("Set-Cookie");   
                    sessionString = getCookieFromHeaders(wsConnection);
                    if( !sessionString.equals("FAIL") ) {
                        String trimmedString = null;
                        int k = sessionString.indexOf(JSESSIONID);
                        if (k >= 0) {
                            trimmedString = sessionString.substring(k + JSESSIONID.length());
                        }
                        if (trimmedString != null) {
                            trimmedString = trimmedString.substring(0,trimmedString.indexOf(PATHSTR) - 1);
                            sessionString = trimmedString;
                        }
                    }
                }
            }
            if(sessionString.equals("FAIL")) {
                //throw new SUException(PropFileUtil.getValue(SUConstants.OD_CONNECTION_FAILED_MSG));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (wsConnection != null) {
                    wsConnection.disconnect();
                    wsConnection = null;
                }
            } catch (Exception e) {
              logger.debug(e);
            }
        }   
        return sessionString;
    }

    /**
     * No usage as of now.
     */
    public void passivateObject(Object arg0) throws Exception {
    
    }

    /**
     * No usage as of now.
     */
    public boolean validateObject(Object arg0) {
        // Since we refresh session and don't use the pool for this purpose,
        // validate will always return true
        return true;
    }

    /*
     * given a successful logon response, extract the session cookie information
     * from the response HTTP headers
     *
     * @param wsConnection successfully connected connection to OnDemand web services
     * @return the session cookie string from the OnDemand WS session or FAIL if not found*
     */
    private static String getCookieFromHeaders(HttpURLConnection wsConnection) throws SocketTimeoutException{
        // debug code - display all the returned headers
        String headerName;
        String headerValue = "FAIL";
        int count = wsConnection.getHeaderFields().size();
        for (int i = 0;i<count; i++) {
            headerName = wsConnection.getHeaderFieldKey(i);
            if ((headerName != null) && headerName.equals("Set-Cookie")) {
                // found the Set-Cookie header (code assumes only one cookie is being set)
                headerValue = wsConnection.getHeaderField(i);
                break;
            }
        }
        // return the header value (FAIL string for not found)
        return headerValue;
    }
}
