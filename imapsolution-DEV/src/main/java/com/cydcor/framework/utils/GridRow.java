package com.cydcor.framework.utils;

import java.util.ArrayList;
import java.util.List;

public class GridRow {
	private Object id;
	private List<String> cell;
	
	public Object getId() {
		return id;
	}
	
	public void setId(Object id) {
		this.id = id;
	}
	
	public List<String> getCell() {
		if (cell == null) {
			cell = new ArrayList<String>();
		}
		return cell;
	}
	
	public void setCell(List<String> cell) {
		this.cell = cell;
		
	}
	
}
