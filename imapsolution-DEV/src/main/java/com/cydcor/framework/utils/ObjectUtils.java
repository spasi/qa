/**
 * 
 */
package com.cydcor.framework.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.thoughtworks.xstream.XStream;

/**
 * @author Ashwin
 * 
 */
public class ObjectUtils {
	private static String TEST_STRING = "TEST";
	private static double TEST_DOUBLE = 1.0;

	/**
	 * @param object
	 * @return
	 */
	public static String serialize(Object object, Map<String, Class> aliases,
			Map<String, Class> implicitCollections) {
		String xmlForm = "";
		XStream xstream = new XStream();

		// Add aliases here
		if (aliases != null) {
			Set entries = aliases.entrySet();
			Iterator entryIter = entries.iterator();
			while (entryIter.hasNext()) {
				Map.Entry entry = (Map.Entry) entryIter.next();
				xstream
						.alias((String) entry.getKey(), (Class) entry
								.getValue());
			}
		}

		// Add Implicit Collections here
		if (implicitCollections != null) {
			Set entries = implicitCollections.entrySet();
			Iterator entryIter = entries.iterator();
			while (entryIter.hasNext()) {
				Map.Entry entry = (Map.Entry) entryIter.next();
				xstream.addImplicitCollection((Class) entry.getValue(),
						(String) entry.getKey());
			}
		}

		xmlForm = xstream.toXML(object);
		return xmlForm;
	}

	/**
	 * @param object
	 * @return
	 */
	public static Object deSerialize(String xml, Map<String, Class> aliases,
			Map<String, Class> implicitCollections) {
		Object object = null;
		XStream xstream = new XStream();

		// Add aliases here
		if (aliases != null) {
			Set entries = aliases.entrySet();
			Iterator entryIter = entries.iterator();
			while (entryIter.hasNext()) {
				Map.Entry entry = (Map.Entry) entryIter.next();
				xstream
						.alias((String) entry.getKey(), (Class) entry
								.getValue());
			}
		}

		// Add Implicit Collections here
		if (implicitCollections != null) {
			Set entries = implicitCollections.entrySet();
			Iterator entryIter = entries.iterator();
			while (entryIter.hasNext()) {
				Map.Entry entry = (Map.Entry) entryIter.next();
				xstream.addImplicitCollection((Class) entry.getValue(),
						(String) entry.getKey());
			}
		}

		object = xstream.fromXML(xml);
		return object;
	}

	public static Object annotationBasedSerialize(Object obj,List<Class> annotionClassList,Map<Class,String> attributeMap){
		XStream xstream = new XStream();
		
		//iterate through the class and add them to processAnnotation
		/**
		 * Ex: <com.cydcor.framework.model.Kml></<com.cydcor.framework.model.Kml> converted to <kml></kml> */
		for (Class clazz : annotionClassList) {
			xstream.processAnnotations(clazz);
		}
		
		//iterate through map and set the attributes
		/**
		 * Ex : <Style><id>"downArrow"</id></style> will be converted to
		 * <Style id="downArrow"></style>
		 * */
		Set<Class> classes = attributeMap.keySet();
		for (Class key : classes) {
			xstream.useAttributeFor(key, attributeMap.get(key));
		}
		return xstream.toXML(obj);
	}
	
	/**
	 * For Testing Purposes
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * KPISValueObject kpisValueObject = new KPISValueObject();
		 * 
		 * KPIValueObject kpiValueObject = new KPIValueObject();
		 * kpiValueObject.setName(TEST_STRING);
		 * kpiValueObject.setTimeslice(TEST_STRING);
		 * kpiValueObject.setType(TEST_STRING);
		 * kpiValueObject.setImpact(TEST_DOUBLE);
		 * kpiValueObject.setThreshold(TEST_DOUBLE);
		 * 
		 * KPIValueSeriesObject series = new KPIValueSeriesObject(); //
		 * series.setBenchmark(TEST_DOUBLE); series.setLabel(TEST_STRING);
		 * series.setKpi(TEST_DOUBLE); series.setTarget(TEST_DOUBLE);
		 * 
		 * KPIValuesObject kpiValuesObject = new KPIValuesObject();
		 * kpiValuesObject.addSeries(series);
		 * 
		 * series = new KPIValueSeriesObject(); //
		 * series.setBenchmark(TEST_DOUBLE); series.setLabel(TEST_STRING + "");
		 * series.setKpi(TEST_DOUBLE + 1); series.setTarget(TEST_DOUBLE + 1);
		 * 
		 * kpiValuesObject.addSeries(series);
		 * 
		 * series = new KPIValueSeriesObject(); //
		 * series.setBenchmark(TEST_DOUBLE); series.setLabel(TEST_STRING + "2");
		 * series.setKpi(TEST_DOUBLE + 2); series.setTarget(TEST_DOUBLE + 2);
		 * 
		 * kpiValuesObject.addSeries(series);
		 * 
		 * 
		 * kpiValueObject.setValues(kpiValuesObject);
		 * 
		 * kpisValueObject.addKpi(kpiValueObject);
		 * 
		 * // Adding Aliases Map<String, Class> aliases = new HashMap<String,
		 * Class>(); aliases.put("kpis", KPISValueObject.class);
		 * aliases.put("kpi", com.futurity.valueobject.KPIValueObject.class);
		 * aliases.put("values", KPIValuesObject.class); aliases.put("series",
		 * KPIValueSeriesObject.class);
		 * 
		 * // Adding implicitCollections Map<String, Class> implicitCollections
		 * = new HashMap<String, Class>(); implicitCollections.put("kpi",
		 * KPISValueObject.class); implicitCollections.put("series",
		 * KPIValuesObject.class);
		 * 
		 * logger.debug(serialize(kpisValueObject, aliases,
		 * implicitCollections));
		 * 
		 * printSampleKPIEntryObject();
		 */


	}
	
	public static String appendCommaForSpace(String fullAddr) {
		String address = fullAddr;
		if (StringUtils.contains(address, " - ")) {
			address = StringUtils.replace(address, " - ", ",");
		} /*else if (StringUtils.contains(address, "-")) {
			address = StringUtils.replace(address, "-", ",");
		}*/

		if (StringUtils.contains(address, "#")) {
			address = StringUtils.replace(address, "#", "+");
		}

		address = StringUtils.replace(address, " ", "+");
		return address;
	}

}
