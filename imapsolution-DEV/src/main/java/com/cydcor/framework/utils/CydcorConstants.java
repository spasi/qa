/**
 * 
 */
package com.cydcor.framework.utils;

/**
 * @author ashwin
 * 
 */
public interface CydcorConstants {

	/**
	 * @author ashwin
	 * 
	 */
	public static class DataConstants {
		public static String DEFAULT_EVALUATION_STRING = "-98761";
	}
	
	public static class ProcessConstants {
		public static String ERROR = "ERROR";
		public static String PROCESSED = "PROCESSED";
	}

}
