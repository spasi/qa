package com.cydcor.framework.utils;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class PropertyUtils extends PropertyPlaceholderConfigurer {
	private Properties mergedProperties;

	public static PropertyUtils getInstance(){
		return (PropertyUtils) ServiceLocator.getService("propertyUtils");
	}
	
	public Properties getProperty() throws IOException {
		if (mergedProperties == null) {
				mergedProperties = mergeProperties();
		}
		return mergedProperties;
	}
}
