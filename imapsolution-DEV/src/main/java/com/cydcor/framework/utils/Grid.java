package com.cydcor.framework.utils;

import java.util.ArrayList;
import java.util.List;

public class Grid {
	private Integer page;
	private Integer total;
	private Integer records;
	private List<GridRow> rows;
	
	public Integer getPage() {
		return page;
	}
	
	public void setPage(Integer page) {
		this.page = page;
	}
	
	public Integer getTotal() {
		return total;
	}
	
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	public Integer getRecords() {
		return records;
	}
	
	public void setRecords(Integer records) {
		this.records = records;
	}
	
	public List<GridRow> getRows() {
		if (rows == null) {
			rows = new ArrayList<GridRow>();
		}
		return rows;
	}
	
	public void setRows(List<GridRow> rows) {
		this.rows = rows;
	}
	
}
