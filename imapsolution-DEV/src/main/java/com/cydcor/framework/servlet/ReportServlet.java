package com.cydcor.framework.servlet;

import groovyjarjarbackport.java.util.Arrays;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.report.Exporter;
import com.cydcor.framework.report.impl.ExportContext;
import com.cydcor.framework.report.impl.ExporterImpl;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * Hello world!
 * s
 */
public class ReportServlet extends HttpServlet {

    /** <!-- Sanjay added -->
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unchecked")
    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

	Exporter exporter = new ExporterImpl();
	ExportContext context = new ExportContext();
	// System.out.println(req.getParameter("reportType"));

	Enumeration<String> paramNames = req.getParameterNames();
	while (paramNames.hasMoreElements()) {
	    String param = paramNames.nextElement();
	    context.getReportParameters().put(param, req.getParameter(param));
	}
	context.setReportDescription("Report desc");
	context.setReportName(req.getParameter("leadSheetId"));
	context.getReportParameters().put("titleImage", new ClassPathResource("cydcor_logo.gif").getPath());
	context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	context.getReportParameters().put("columnWidths", CydcorUtils.getProperty(req.getParameter("queryString") + "_WIDTHS"));
	// context.getReportParameters().put("subTitle",
	// "Leads under this Lead Sheet");
	if (StringUtils.isNotBlank(CydcorUtils.getProperty(req.getParameter("queryString")))) {
	    context.getReportParameters().put("ignorableColumns", Arrays.asList(CydcorUtils.getProperty(req.getParameter("queryString")).split(",")));
	}
	context.getReportParameters().put("showPointColorInReport", CydcorUtils.getProperty("showPointColorInReport"));
	context.getReportParameters().put("leadsPerMap", CydcorUtils.getProperty("leadsPerMap"));
	context.getReportParameters().put("rowsPerPage", CydcorUtils.getProperty("rowsPerPage"));
	try {
	    context.setSqlConnecion(((JdbcTemplate) ServiceLocator.getService("jdbcTemplate")).getDataSource().getConnection());
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	String sql = QueryUtils.getQuery().get(req.getParameter("queryString"));
	
	
	
	if ("ICL_REP_LEADLIST_BY_LEADSHEET_ID".equalsIgnoreCase(req.getParameter("queryString"))) {
	    context.getSqlParameters().add(req.getParameter("leadSheetId"));

	} else if ("ICL_REP_LEAD_SHEET_PRINT".equalsIgnoreCase(req.getParameter("queryString"))) {
	    if ("true".equalsIgnoreCase(req.getParameter("showMapInReport")))
		context.setShowMapInReport(true);
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    // context.getSqlParameters().add(req.getParameter("leadSheetId"));
	    context.getSqlParameters().add(req.getParameter("leadSheetId"));
	    context.getSqlParameters().add(req.getParameter("leadSheetId"));
	    if (Exporter.PDF.equalsIgnoreCase(req.getParameter("reportType")))
		context.getSqlParameters().add(req.getParameter("leadSheetId"));
	    // sql = sql.replaceAll(":leadSheetId", "'" +
	    // req.getParameter("leadSheetId") + "'");
	    // context.getReportParameters().put("enableMapContent", "true");
	} 
 else if ("ICL_REP_LEADLIST_FOR_EXPORT_XLS".equalsIgnoreCase(req.getParameter("queryString"))) {
	    sql = QueryUtils.getQuery().get(req.getParameter("queryString"));
	    String archSql = QueryUtils.getQuery().get("ICL_REP_ARCHIVED_LEADLIST_FOR_EXPORT_XLS");
	    String lengthSql = QueryUtils.getQuery().get("ICL_REP_LEADLIST_FOR_EXPORT_XLS_LENGTHS");
	    String archLengthSql = QueryUtils.getQuery().get("ICL_REP_ARCHIVED_LEADLIST_FOR_EXPORT_XLS_LENGTHS");
	    List archInputs = new ArrayList();
	    context.getReportParameters().put("AT&T_HEADERS", true);
	    context.getSqlParameters().add(req.getParameter("dispositionStDate"));
	    context.getSqlParameters().add(req.getParameter("dispositionEndDate"));
	    archInputs.add(req.getParameter("dispositionStDate"));
	    archInputs.add(req.getParameter("dispositionEndDate"));
	    if (!StringUtils.equalsIgnoreCase("-1", req.getParameter("campaignId"))) {
		sql = sql + " AND LF.CAMPAIGN_SEQ = ? ";
		lengthSql = lengthSql + " AND LF.CAMPAIGN_SEQ = ? ";
		archSql = archSql + " AND LFA.CAMPAIGN_SEQ = ? ";
		archLengthSql = archLengthSql + " AND LFA.CAMPAIGN_SEQ = ? ";
		context.getSqlParameters().add(req.getParameter("campaignId"));
		archInputs.add(req.getParameter("campaignId"));
	    }
	    if (!StringUtils.equalsIgnoreCase("-1", req.getParameter("officeId"))) {
		sql = sql + " AND LF.OFFICE_SEQ = ? ";
		lengthSql = lengthSql + " AND LF.OFFICE_SEQ = ? ";
		archSql = archSql + " AND LFA.OFFICE_SEQ = ? ";
		archLengthSql = archLengthSql + " AND LFA.OFFICE_SEQ = ? ";
		context.getSqlParameters().add(req.getParameter("officeId"));
		archInputs.add(req.getParameter("officeId"));
	    }
	    sql = sql + " UNION ALL " + archSql;
	    context.setSqlString(sql);
	    context.setReportName("AT&T Format Template");
	    context.getSqlParameters().addAll(archInputs);
	    lengthSql = lengthSql + " UNION ALL " + archLengthSql;
	    String[] columnLengths = new String[4]; // 2013136 ashish : Added Lead_Owner as Owner Code column 
	    DatabaseResult databaseResult = getPlatformService().loadResult(lengthSql, context.getSqlParameters());
	    for (int i = 0; i < databaseResult.getData().get(0).size(); i++) {
		if (databaseResult.getData().get(0).get(i).getValue() != null)
		    columnLengths[i] = databaseResult.getData().get(0).get(i).getValue();
		else
		    columnLengths[i] = databaseResult.getData().get(1).get(i).getValue();
		    if(columnLengths[i] != null && Integer.valueOf(columnLengths[i])<10){
		    	columnLengths[i] = "10";
		    }
	    }
	    context.getReportParameters().put("columnLengths", columnLengths);
	} 
	else if ("SELECT_LEADSHEETS".equalsIgnoreCase(req.getParameter("queryString"))) {
	    sql = QueryUtils.getQuery().get(req.getParameter("queryString"));
	    String archivedSheetsSql = QueryUtils.getQuery().get("SELECT_CLOSED_LEADSHEETS");
	    List<Object> archivedSylheetsInputs = new ArrayList<Object>();
	    archivedSylheetsInputs.add(req.getParameter("startDate"));
	    archivedSylheetsInputs.add(req.getParameter("endDate"));
	    context.getSqlParameters().add(req.getParameter("startDate"));
	    context.getSqlParameters().add(req.getParameter("endDate"));

	    if (StringUtils.isNotBlank(req.getParameter("campaignSeq"))) {
		sql = sql + " " + QueryUtils.getQuery().get("ADD_CAMPAIGN_SEQ");
		context.getSqlParameters().add(new Long(req.getParameter("campaignSeq")));
		archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_CAMPAIGN_SEQ");
		archivedSylheetsInputs.add(new Long(req.getParameter("campaignSeq")));
	    }

	    if (StringUtils.isNotBlank(req.getParameter("officeSeq"))) {
		sql = sql + " " + QueryUtils.getQuery().get("ADD_OFFICE_SEQ");
		context.getSqlParameters().add(new Long(req.getParameter("officeSeq")));
		archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_OFFICE_SEQ");
		archivedSylheetsInputs.add(new Long(req.getParameter("officeSeq")));
	    }

	    if (StringUtils.isNotBlank(req.getParameter("repId"))) {
		if (!req.getParameter("repId").equalsIgnoreCase("UA")) {
		    sql = sql + " " + QueryUtils.getQuery().get("ADD_PERSON_ID");
		    context.getSqlParameters().add(new Long(req.getParameter("repId")));
		    archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_PERSON_ID");
		    archivedSylheetsInputs.add(new Long(req.getParameter("repId")));
		} else {
		    sql = sql + " " + QueryUtils.getQuery().get("ADD_UN_ASSGN_PERSON_ID");
		    archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_UN_ASSGN_PERSON_ID");
		}
	    }

	    if (StringUtils.isNotBlank(req.getParameter("status"))) {
		sql = sql + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_STATUS");
		context.getSqlParameters().add(req.getParameter("status"));
		archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_STATUS");
		archivedSylheetsInputs.add(req.getParameter("status"));
	    }

	    String leadSheetID = req.getParameter("leadSheetID");
	    if (leadSheetID != null && "%".equalsIgnoreCase(leadSheetID.trim()))
		leadSheetID = null;
	    if (StringUtils.isNotBlank(leadSheetID)) {
		sql = sql + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_NAME");
		context.getSqlParameters().add(leadSheetID.trim());
		archivedSheetsSql = archivedSheetsSql + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_NAME");
		archivedSylheetsInputs.add(leadSheetID.trim());
	    }
	    String[] columnLengths = CydcorUtils.getProperty(req.getParameter("queryString") + "_WIDTHS").replaceAll(" ", "").split(",");
	    context.getReportParameters().put("columnLengths", columnLengths);
	    sql = sql + " UNION ALL " + archivedSheetsSql;
	    context.getSqlParameters().addAll(archivedSylheetsInputs);
	}
	
	

	//code changes by sanjay 9/4/2016
	else if ("SELECT_IMS_DASHBOARD_BY_RUNID".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("runId"));
	} 
	
	else if ("SELECT_IMS_DASHBOARD_BY_DOWNLOAD".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	} 
	
	else if ("SELECT_IMS_DASHBOARD_BY_DOWNLOAD_LEAD_OWNER".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	    context.getSqlParameters().add(req.getParameter("LEAD_OWNER"));
	} 
	else if ("SELECT_IMS_DASHBOARD_BY_DOWNLOAD_CAMPAIGN_SEQ".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	    context.getSqlParameters().add(req.getParameter("CAMPAIGN_SEQ"));
	} 
	else if ("SELECT_IMS_DASHBOARD_BY_DOWNLOAD_ALL".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	    context.getSqlParameters().add(req.getParameter("CAMPAIGN_SEQ"));
	    context.getSqlParameters().add(req.getParameter("LEAD_OWNER"));
	} 
	
	
	else if ("SELECT_IMS_DASHBOARD_BY_CLLIDA".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("DA"));
	    context.getSqlParameters().add(req.getParameter("CLLI"));
	} 
	
	else if ("SELECT_IMS_HISTORY_BY_DOWNLOAD".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	}
	
	else if ("SELECT_IMS_HISTORY_BY_DOWNLOAD_LEAD_OWNER".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	    context.getSqlParameters().add(req.getParameter("LEAD_OWNER"));
	}
	else if ("SELECT_IMS_HISTORY_BY_DOWNLOAD_CAMPAIGN_SEQ".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	    context.getSqlParameters().add(req.getParameter("CAMPAIGN_SEQ"));
	}
	else if ("SELECT_IMS_HISTORY_BY_DOWNLOAD_ALL".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	    context.getSqlParameters().add(req.getParameter("CAMPAIGN_SEQ"));
	    context.getSqlParameters().add(req.getParameter("LEAD_OWNER"));
	}
	
	else if ("SELECT_IMS_LeadApproval_report".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("DA"));
	    context.getSqlParameters().add(req.getParameter("CLLI"));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	    
	}
	
	else if ("SELECT_IMS_LeadOwner_report".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("OWNER"));
	    context.getSqlParameters().add(req.getParameter("LEAD_ASSIGNED"));
	    context.getSqlParameters().add(req.getParameter("uploadedStartDate"));
	    context.getSqlParameters().add(req.getParameter("uploadedEndDate"));
	    
	}
	
	else if ("SELECT_IMS_New_LeadOwner_report".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("OWNER"));
	    context.getSqlParameters().add(req.getParameter("CSF_CURRENT_ICL_OWNERNAME"));
	    context.getSqlParameters().add(req.getParameter("LEAD_ASSIGNED"));
	    context.getSqlParameters().add(req.getParameter("CSF_PREV_ASSIGNEDDATE"));
	 	    
	}
	
	else if ("SELECT_LEAD_APPROVAL_BY_DOWNLOAD".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("LEAD_ASSIGNED"));
	    context.getSqlParameters().add(req.getParameter("uploadEndDate"));
	}
	
	else if ("SELECT_LEAD_OWNER_DOWNLOAD".equalsIgnoreCase(req.getParameter("queryString"))) 
	{
	    context.setSqlString(QueryUtils.getQuery().get(req.getParameter("queryString")));
	    context.getSqlParameters().add(req.getParameter("LEAD_ASSIGNED"));
	    context.getSqlParameters().add(req.getParameter("uploadEndDate"));
	}
	
	//code changes by sanjay 9/4/2016
	
	context.setSqlString(sql);
	OutputStream outputStream = res.getOutputStream();
	context.setExportStream(outputStream);

	if (Exporter.PDF.equalsIgnoreCase(req.getParameter("reportType"))) {
	    context.setReportType(Exporter.PDF);
	    res.setContentType("application/pdf");
	    if ("ICL_REP_LEAD_SHEET_PRINT".equalsIgnoreCase(req.getParameter("queryString"))
		    || "ICL_REP_LEADLIST_BY_LEADSHEET_ID".equalsIgnoreCase(req.getParameter("queryString"))) {
		List inputs = new ArrayList();
		String[] leadSheetsIds = null;
		boolean leadsExist = false;
		if ("ICL_REP_LEADLIST_BY_LEADSHEET_ID".equalsIgnoreCase(req.getParameter("queryString"))) {
		    leadsExist = true;
		} else {
		    if (StringUtils.isNotBlank(req.getParameter("leadSheets"))) {
			leadSheetsIds = (req.getParameter("leadSheets")).split(",");

		    } else {
			leadSheetsIds = new String[1];
			leadSheetsIds[0] = req.getParameter("leadSheetId");
		    }

		    for (String sheet : leadSheetsIds) {
			inputs.clear();
			inputs.add(sheet);
			inputs.add(sheet);
			if (getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_LEAD_SHEET_PRINT"), inputs).getData().size() > 0) {
			    leadsExist = true;
			    break;
			}
		    }
		}
		if (leadsExist) {
		    res.setHeader("Content-Disposition", "inline; filename=" + req.getParameter("reportName") + "." + req.getParameter("reportType"));
		    exporter.buildPDFReport(context);
		} else {
		    res.setContentType("text/html");
		    outputStream.write("No Leads availabe to export. All leads are expired/dispositioned".getBytes());
		}

	    } else {
		res.setHeader("Content-Disposition", "attachment; filename=" + req.getParameter("reportName") + "." + req.getParameter("reportType"));
		exporter.export(context);
	    }
	} else if (Exporter.XLS.equalsIgnoreCase(req.getParameter("reportType"))) {
	    res.setHeader("Content-Disposition", "attachment; filename=" + req.getParameter("reportName") + "." + req.getParameter("reportType"));
	    res.setContentType("application/vnd.ms-excel");
	    context.setReportType(Exporter.XLS);
	    exporter.export(context);
	}

    }

    protected PlatformService getPlatformService() {
	return ServiceLocator.getService(PlatformService.class);
    }

}
