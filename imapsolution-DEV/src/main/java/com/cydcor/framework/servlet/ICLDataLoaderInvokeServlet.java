package com.cydcor.framework.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cydcor.framework.test.ICLDataLoader;

public class ICLDataLoaderInvokeServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
	if ("true".equals(arg0.getParameter("loadNow"))) {
	    ICLDataLoader.executeProcess();
	} else {
	    arg1.setContentType("text/html;charset=UTF-8");
	    arg1.getWriter().write(
		    "	<script src='assets/js/jquery-1.3.2.min.js' type='text/javascript'>\n</script><script src='assets/js/jquery-ui-1.7.2.custom.min.js' type='text/javascript'></script>" +
		    "	\n<script type='text/javascript'>\n $.post('iclLoaderServlet?loadNow=true',{},function(data){document.getElementById('result').innerHTML ='Job Finished.';}); \n </script>\n" +
		    "<title>ICL Data Loader</title><h1>ICL Data Loading Started.</h1><br>Last Run was on " + ICLDataLoader.LAST_RUN_DATE + ".<br><span id='result' style='color:green;font-size:20px'>Running..</span><br><a href='javascript:close()'>Close</a>");
	}
    }
}
