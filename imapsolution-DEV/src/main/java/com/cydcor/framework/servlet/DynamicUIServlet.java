package com.cydcor.framework.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.click.util.HtmlStringBuffer;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.controller.Controller;
import com.cydcor.framework.controller.ControllerFactory;
import com.cydcor.framework.exceptions.RoleNotFoundException;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.FreeMarkerEngine;

/**
 * Servlet implementation class DynamicUIServlet
 */
public class DynamicUIServlet extends HttpServlet {
	private static final transient Log logger = LogFactory
			.getLog(DynamicUIServlet.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DynamicUIServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String pageName = request.getParameter("pageName");
		String controlName = request.getParameter("controlName");
		String loadTemplate = request.getParameter("loadTemplate");
		String userId = request.getParameter("userId");
		String ssoToken = request.getParameter("ssoToken");

		response.setContentType("text/html");

		HtmlStringBuffer responseString = new HtmlStringBuffer();
		DynamicUIContext uiContext = new DynamicUIContext();
		uiContext.setPageName(pageName);
		uiContext.setControlName(controlName);

		Controller controller = ControllerFactory.getInstance().getController(
				pageName);

		boolean hasRole = false;
		if (controller != null) {
			if (StringUtils.isBlank(loadTemplate)
					&& StringUtils.isBlank(controlName)) {
				CydcorContext.getInstance().getCache().put(
						"roleObjMap_" + userId, new HashMap());
			}

			controller.execute(uiContext);
			for (GenericUIControl uiControl : uiContext.getControls()) {
				uiControl.render(responseString);
			}

			// If loadTemplate is not defined as false Page is loaded with
			// template
			if (StringUtils.isBlank(loadTemplate)
					&& StringUtils.isBlank(controlName)) {
				Map paramMap = new HashMap();
				paramMap.put("customPage", responseString.toString());
				paramMap.put("userId", userId);
				paramMap.put("ssoToken", ssoToken);
				List<ListOrderedMap> roleNamesList;
				if (StringUtils.equalsIgnoreCase(CydcorUtils
						.getProperty("crmmode"), "true")) {
					// Add CRM Code here
					try {
						roleNamesList = DBUtil.getAccessLinksByRoleName();
						if (roleNamesList != null && !roleNamesList.isEmpty()
								&& roleNamesList.size() > 0) {
							addRoleMappings(paramMap, roleNamesList);
							addUserNameAndRole(paramMap);
							hasRole = true;
						}
					} catch (Exception e) {
						logger.error(e.getMessage());
					}

				} else {
					// Add CRM Code here
					try {
						roleNamesList = DBUtil.getAllAccessLinks();
						addRoleMappings(paramMap, roleNamesList);
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				}

				if (!uiContext.isShowMap()) {
					paramMap.put("pageName", "PopupWindow");
				} else {
					paramMap.put("pageName", "TotalWindow");
				}
				paramMap.put("GoogleMapURL", CydcorUtils
						.getProperty("GoogleMapURL"));

				String ricoTemplate = FreeMarkerEngine.getInstance()
						.evaluateTemplate("template/GenericTemplate.html",
								paramMap);
				response.getWriter().append(ricoTemplate);
			} else {
				response.getWriter().append(responseString.toString());
			}

		} else {
			if (StringUtils.isBlank(loadTemplate)) {
				CydcorContext.getInstance().getCache().put(
						"roleObjMap_" + userId, new HashMap());
			}

			Map paramMap = new HashMap();
			paramMap.put("userId", userId);
			paramMap.put("ssoToken", ssoToken);

			List<ListOrderedMap> roleNamesList;
			if (StringUtils.equalsIgnoreCase(
					CydcorUtils.getProperty("crmmode"), "true")) {
				// Add CRM Code here
				try {
					roleNamesList = DBUtil.getAccessLinksByRoleName();
					if (roleNamesList != null && !roleNamesList.isEmpty()
							&& roleNamesList.size() > 0) {
						addRoleMappings(paramMap, roleNamesList);
						// add UserName and UserRole into paramMap
						addUserNameAndRole(paramMap);
						hasRole = true;
					} else {
						paramMap
								.put(
										"message",
										"Your Role is not yet given access to the Application. Kindly contact your Administrator");
					}
				} catch (RoleNotFoundException e) {
					paramMap.put("message", e.getMessage() + " - " + userId);
					logger.error(e.getMessage() + " - " + userId);
				} catch (Exception e) {
					paramMap.put("message", e.getMessage() + " - " + userId);
					logger.error(e.getMessage() + "-" + userId);
				}
			} else {
				// Add CRM Code here
				try {
					roleNamesList = DBUtil.getAllAccessLinks();
					addRoleMappings(paramMap, roleNamesList);
					addUserNameAndRole(paramMap);
					hasRole = true;
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}

			// If there is no Controller Just Render the Page
			if (StringUtils.isBlank(loadTemplate)) {

				paramMap.put("GoogleMapURL", CydcorUtils
						.getProperty("GoogleMapURL"));
				paramMap.put("pageName", pageName);

				if (!hasRole) {
					paramMap.put("customFile", "ErrorPage.html");
				} else {
					paramMap.put("customFile", pageName + ".html");
				}

				String ricoTemplate = FreeMarkerEngine.getInstance()
						.evaluateTemplate("template/GenericTemplate.html",
								paramMap);
				response.getWriter().append(ricoTemplate);
			} else {
				// Render Page wihtout Template
				paramMap.put("pageName", "TotalWindow");
				if (StringUtils.equalsIgnoreCase(CydcorUtils
						.getProperty("crmmode"), "true")) {
					// Add CRM Code here
					try {
						roleNamesList = DBUtil.getAccessLinksByRoleName();
						if (roleNamesList != null && !roleNamesList.isEmpty()
								&& roleNamesList.size() > 0) {
							addRoleMappings(paramMap, roleNamesList);
							addUserNameAndRole(paramMap);
							hasRole = true;
						}
					} catch (RoleNotFoundException e) {
						paramMap
								.put("message", e.getMessage() + " - " + userId);
						logger.error(e.getMessage() + "-" + userId);
					} catch (Exception e) {
						logger.error(e.getMessage() + "-" + userId);
						paramMap.put("message", e.getMessage());
					}

				} else {
					// Add CRM Code here
					try {
						roleNamesList = DBUtil.getAllAccessLinks();
						addRoleMappings(paramMap, roleNamesList);
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				}
				String ricoTemplate = "";

				if (!hasRole) {
					ricoTemplate = FreeMarkerEngine.getInstance()
							.evaluateTemplate("ErrorPage.html", paramMap);
				} else {
					ricoTemplate = FreeMarkerEngine.getInstance()
							.evaluateTemplate(pageName + ".html", paramMap);
				}
				response.getWriter().append(ricoTemplate);
			}
		}
		response.flushBuffer();
	}

	private void addUserNameAndRole(Map paramMap) {
		String userRoleFreeMarkerString = "${GlobalContext.crmContext.accessLinks}";
		String userRole = null;
		try {
			userRole = FreeMarkerEngine.getInstance().evaluateString(
					userRoleFreeMarkerString);
			paramMap.put("userRole", userRole);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		String userNameFreeMarkerString = "${GlobalContext.crmContext.userName}";
		String userName = null;
		try {
			userName = FreeMarkerEngine.getInstance().evaluateString(
					userNameFreeMarkerString);
			paramMap.put("userName", userName);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	private void addRoleMappings(Map paramMap,
			List<ListOrderedMap> roleNamesList) {
		for (Map map : roleNamesList) {
			if (StringUtils.isNotBlank((String) map.get("LINK_NAME"))) {
				paramMap.put(map.get("LINK_NAME"), true);
			}
		}
	}

}
