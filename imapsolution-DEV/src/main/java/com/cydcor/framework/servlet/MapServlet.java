package com.cydcor.framework.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import com.cydcor.framework.census.CampaignMarkerDetailObject;
import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.census.MarkerImage;
import com.cydcor.framework.census.Polygon;
import com.cydcor.framework.census.Territory;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;

public class MapServlet extends HttpServlet {
	private static final transient Log logger = LogFactory.getLog(MapServlet.class);
	/**
	 * Handles Geometries on map
	 * 
	 */
	private PlatformService platformService = null;

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		
		/*
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator.getService("jdbcTemplate");
		PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator.getService("txManager");
		DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (action.equalsIgnoreCase("save")) {
			savePolygons(req, resp, dbUtil);
		} else if (action.equalsIgnoreCase("loadPolygons") || action.equalsIgnoreCase("loadPolylines")) {
			loadBoundaries(req, resp, action, dbUtil, paramMap);
		} else if (action.equalsIgnoreCase("delete")) {
			deletePolygons(req, resp, dbUtil);
		} else if (action.equalsIgnoreCase("loadPoints")) {
			loadMarkers(req, resp, dbUtil, paramMap);
		} else if (action.equals("loadCampaign")) {
			if (StringUtils.isNotBlank(req.getParameter("campaignSeq"))) {
				List<LeadMaster> leads = dbUtil.selectLeadsByCampaign(req.getParameter("campaignSeq"));
				if (leads != null && leads.size() > 0) {
					CampaignMarkerDetailObject campaign = new CampaignMarkerDetailObject();
					campaign.setLeads(leads);
					Set<String> offices = new HashSet<String>();
					for (LeadMaster master : leads)
						offices.add("'" + master.getOfficeSeq() + "'");
					List<Polygon> polygons = dbUtil.selectICLTerritories(org.springframework.util.StringUtils.arrayToCommaDelimitedString(offices.toArray()));
					if (polygons != null && polygons.size() > 0) {
						campaign.setTerritories(polygons);
					}
					paramMap.put("leads", campaign);
					String polygonTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("Geometry.xml", paramMap);
					resp.getWriter().append(polygonTemplate);
					resp.flushBuffer();
				}
			}
		} 
		
		else if (StringUtils.equalsIgnoreCase("loadDirectionsInMap", action)) 
		{
			loadDirections(req, resp);
		} 
		else if (StringUtils.equalsIgnoreCase("loadLeadsInMap", action)) 
		{
			loadLeadsForRep(req, resp, paramMap);
		}*/
		
		
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator.getService("jdbcTemplate");
		PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator.getService("txManager");
		DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		if (action.equalsIgnoreCase("save")) {
			
			savePolygons(req, resp, dbUtil);
		} 
		else if (action.equalsIgnoreCase("loadPolygons") || action.equalsIgnoreCase("loadPolylines")) {
			loadBoundaries(req, resp, action, dbUtil, paramMap);
		} 
		else if (action.equalsIgnoreCase("delete")) {
			deletePolygons(req, resp, dbUtil);
		} else if (action.equalsIgnoreCase("loadPoints")) {
			loadMarkers(req, resp, dbUtil, paramMap);
		} else if (action.equals("loadCampaign")) {
			if (StringUtils.isNotBlank(req.getParameter("campaignSeq"))) {
				logger.debug("campaignSeq not blank.");
				List<LeadMaster> leads = dbUtil.selectLeadsByCampaign(req.getParameter("campaignSeq"));
				if (leads != null && leads.size() > 0) {
					CampaignMarkerDetailObject campaign = new CampaignMarkerDetailObject();
					campaign.setLeads(leads);
					Set<String> offices = new HashSet<String>();
					for (LeadMaster master : leads)
						offices.add("'" + master.getOfficeSeq() + "'");
					List<Polygon> polygons = dbUtil.selectICLTerritories(org.springframework.util.StringUtils
							.arrayToCommaDelimitedString(offices.toArray()));
					if (polygons != null && polygons.size() > 0) {
						campaign.setTerritories(polygons);
					}
					paramMap.put("leads", campaign);
					String polygonTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("Geometry.xml", paramMap);
					resp.getWriter().append(polygonTemplate);
					resp.flushBuffer();
				}
			}
		} else if (StringUtils.equalsIgnoreCase("loadDirectionsInMap", action)) {
			loadDirections(req, resp);
		} else if (StringUtils.equalsIgnoreCase("loadLeadsInMap", action)) {
			loadLeadsForRep(req, resp, paramMap);
		}
	}

	private void loadBoundariesByLeadSheets(HttpServletRequest req, HttpServletResponse resp, DBUtil dbUtil) throws IOException {

		if (StringUtils.isNotBlank(req.getParameter("campaignSeq")) && StringUtils.isNotBlank(req.getParameter("officeSeq"))) {

			List<Polygon> coordinates = dbUtil.getBoundariesForLeadSheets(req.getParameter("campaignSeq"), req.getParameter("officeSeq"));
			if (coordinates != null && coordinates.size() > 0) {
				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("leadSheetPolygons", true);
				paramMap.put("coordinates", coordinates);
				String leadsTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("Geometry.xml", paramMap);
				resp.getWriter().append(leadsTemplate);
				resp.flushBuffer();
			}
		}

	}

	/**
	 * @param req
	 * @param resp
	 * @param paramMap
	 * @throws IOException
	 */
	private void loadLeadsForRep(HttpServletRequest req, HttpServletResponse resp, Map<String, Object> paramMap) throws IOException {
		List irInputs = new ArrayList();
		irInputs.add(req.getParameter("leadSheetId"));
		// irInputs.add(req.getParameter("repId"));
		irInputs.add(req.getParameter("leadSheetId"));
		DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_LEAD_ADDRESSES"), irInputs);
		List<LeadMaster> leads = new ArrayList<LeadMaster>();
		for (List<MapObject> row : dbResult.getData()) {
			LeadMaster master = new LeadMaster();
			master.setCustName(row.get(1).getValue());
			master.setLeadId(row.get(0).getValue());
			master.setAddress(row.get(2).getValue());
			master.setFullAddr(row.get(2).getValue());
			master.setGeoPoint(row.get(3).getValue());
			master.setRowId(Long.parseLong(row.get(4).getValue()));
			master.setDisposition(row.get(5).getValue());
			master.setDispositionalImage(row.get(7).getValue());
			master.setDispositionCode(row.get(8).getValue());
			master.setImageIcon(row.get(9).getValue());
			if (StringUtils.isNotBlank(master.getGeoPoint()))
				leads.add(master);
		}
		List<String> dispImgs = new ArrayList<String>();
		if (StringUtils.isNotBlank(req.getParameter("leadSheetDispositionalImages"))) {
			for (String str : req.getParameter("leadSheetDispositionalImages").split("~")) {
				dispImgs.add(str.trim());
			}
		}
		CampaignMarkerDetailObject campaign = new CampaignMarkerDetailObject();
		for (LeadMaster lead : leads) {
			if (dispImgs.size() < 1 && StringUtils.isBlank(lead.getImageIcon())) {
				lead.setImageIcon("redDot");
			} else {
				// Assign Lead Image ICon Based on Rule Template here
				try {
					String imageName = null;
					if (dispImgs.size() > 0) {
						lead.setImageIcon("redDot");
					}
					String imageIcon = lead.getImageIcon();
					if (dispImgs != null && dispImgs.contains(lead.getDispositionalImage()))
						imageIcon = lead.getDispositionalImage();
					int lastIndexOfSlash = imageIcon.lastIndexOf("/");
					int lastIndexOfDot = imageIcon.lastIndexOf(".");
					imageName = imageIcon.substring(lastIndexOfSlash + 1, lastIndexOfDot);
					MarkerImage markerImage = new MarkerImage();
					markerImage.setStyleId(imageName);
					markerImage.setImageUrl(imageIcon);

					campaign.getMarkerList().add(markerImage);
					lead.setImageIcon(imageName);

				} catch (Exception e) {
					lead.setImageIcon("redDot");
				}
			}
		}

		campaign.setLeads(leads);
		paramMap.put("leads", campaign);
		String leadsTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("Geometry.xml", paramMap);
		resp.getWriter().append(leadsTemplate);
		resp.flushBuffer();
	}

	/**
	 * @param req
	 * @param resp
	 * @param action
	 * @param dbUtil
	 * @param paramMap
	 * @throws IOException
	 */
	private void loadBoundaries(HttpServletRequest req, HttpServletResponse resp, String action, DBUtil dbUtil, Map<String, Object> paramMap)
			throws IOException {
		List<Polygon> polygons = null;

		if (StringUtils.isNotBlank(req.getParameter("territoryFor")) && StringUtils.equalsIgnoreCase("ICL", req.getParameter("territoryFor"))
				&& StringUtils.isNotBlank(req.getParameter("campaignSeq")) && StringUtils.isNotBlank(req.getParameter("officeSeq"))) {

			// For ICLAdmin Page and Campaing Admin Page
			polygons = dbUtil.selectCampaignTerritories(req.getParameter("campaignSeq"), req.getParameter("officeSeq").replaceAll("'", ""));

		} else if (StringUtils.isNotBlank(req.getParameter("campaignSeq")) && StringUtils.isNotBlank(req.getParameter("officeSeq"))) {
			if (StringUtils.isNotBlank(req.getParameter("defaultView"))) {

				polygons = dbUtil.getBoundariesForWCandDA(req.getParameter("campaignSeq"),
						req.getParameter("officeSeq"), req.getParameter("defaultView"),
						req.getParameter("wcDaZipString"));

			} else if ("true".equals(req.getParameter("loadByLeadSheets"))) {

				if (StringUtils.isBlank(req.getParameter("leadSheetId"))) {
					polygons = dbUtil.getBoundariesForLeadSheets(req.getParameter("campaignSeq"), req.getParameter("officeSeq"));
				} else {
					polygons = dbUtil.getBoundariesForLeadSheets(req.getParameter("campaignSeq"), req.getParameter("officeSeq"), req
							.getParameter("leadSheetId"));
				}

			} else {
				polygons = dbUtil.getBoundariesByCampaignAndOffice(req.getParameter("campaignSeq"), req.getParameter("officeSeq"));
			}
		}

		if (StringUtils.isNotBlank(req.getParameter("terrytoryIds")) && !StringUtils.equalsIgnoreCase("undefined", req.getParameter("terrytoryIds"))) {
			polygons = dbUtil.getTerritoriesByIds(req.getParameter("terrytoryIds"));
		} else if (StringUtils.isNotBlank(req.getParameter("parentTerritoryIds"))
				&& !StringUtils.equalsIgnoreCase("undefined", req.getParameter("parentTerritoryIds"))) {
			polygons = dbUtil.getTerritoriesByParentIds(req.getParameter("parentTerritoryIds"));
		}

		if (polygons != null && polygons.size() > 0) {
			CampaignMarkerDetailObject campaign = new CampaignMarkerDetailObject();
			for (Iterator<Polygon> iterator = polygons.iterator(); iterator.hasNext();) {
				Polygon polygon = iterator.next();
				if (polygon.getCoordinates().contains("LINESTRING")) {
					try {
						iterator.remove();
					} catch (Exception e) {
						// TODO: handle exception
					}
				} else {

					StringTokenizer stringTokenizer = new StringTokenizer(polygon.getCoordinates(), ",");
					if (stringTokenizer.countTokens() < 3) {
						try {
							iterator.remove();
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				}

			}
			campaign.setTerritories(polygons);
			if (action.equalsIgnoreCase("loadPolylines"))
				paramMap.put("polyLines", campaign);
			else
				paramMap.put("polygons", campaign);
			String polygonTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("Geometry.xml", paramMap);
			resp.getWriter().append(polygonTemplate);
			resp.flushBuffer();

		}
	}

	/**
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	private void loadDirections(HttpServletRequest req, HttpServletResponse resp) throws IOException 
	{
		List irInputs = new ArrayList();
	    
		irInputs.add(req.getParameter("firstLeadRowId"));
		irInputs.add(req.getParameter("firstLeadRowId"));
		irInputs.add(req.getParameter("firstLeadRowId"));
		
			DatabaseResult dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_FIRST_LEAD_ADDRESSES"), irInputs);
			String leadAdds = "";
			//Code modified for AT&T to fix direction issue
			if(null == dbResult || dbResult.getData().size() == 0){
				com.cydcor.framework.test.ICLDataLoader.executeICLLoader(req.getParameter("firstLeadRowId"));
				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("ICL_REP_FIRST_LEAD_ADDRESSES"), irInputs);
			}
			for (List<MapObject> row : dbResult.getData()) {
		    leadAdds = row.get(1).getValue();
		    leadAdds = leadAdds + "@@@" + row.get(0).getValue();
				break;
			}
			resp.getWriter().append(leadAdds);
			resp.flushBuffer();
		
	}
	
	/**
	 * @param req
	 * @param resp
	 * @param dbUtil
	 * @param paramMap
	 * @throws IOException
	 */
	private void loadMarkers(HttpServletRequest req, HttpServletResponse resp, DBUtil dbUtil, Map<String, Object> paramMap) throws IOException {

		// pageName=leadSheetGeneration
		if (StringUtils.isNotBlank(req.getParameter("pageName")) && StringUtils.equalsIgnoreCase("leadSheetGeneration", req.getParameter("pageName"))) {
			// &disableAssignedLeads=true
			if (StringUtils.isNotBlank(req.getParameter("disableAssignedLeads"))
					&& StringUtils.equalsIgnoreCase("true", req.getParameter("disableAssignedLeads"))) {
				// System.out.println("About to get Leads");
				// logger.info("About to get Leads");
				getLeadsMarkers(req, resp, dbUtil, paramMap, false);

			}

		} else if (StringUtils.isNotBlank(req.getParameter("campaignSeq")) && StringUtils.isNotBlank(req.getParameter("officeSeq"))) {
			getLeadsMarkers(req, resp, dbUtil, paramMap, true);

		} else if ((StringUtils.isNotBlank(req.getParameter("cSeq")) && !StringUtils.equalsIgnoreCase(req.getParameter("cSeq"), "undefined"))
				|| (StringUtils.isNotBlank(req.getParameter("officeSeq")) && !StringUtils.equalsIgnoreCase(req.getParameter("officeSeq"), "undefined"))) {

			// You can get officeSeq or
			// You can get cSeq

			CampaignMarkerDetailObject campaign = null;
			if (StringUtils.isNotBlank(req.getParameter("cSeq"))) {
				campaign = dbUtil.getCampaignOffices(req.getParameter("cSeq"));
				paramMap.put("offices", campaign);
			}
			if (StringUtils.isNotBlank(req.getParameter("officeSeq"))) {
				campaign = new CampaignMarkerDetailObject();
				campaign.setLeads(dbUtil.selectLeadsByICL(req.getParameter("officeSeq")));
				paramMap.put("leads", campaign);
			}
			if (campaign != null) {
				String pointsTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("Geometry.xml", paramMap);
				resp.getWriter().append(pointsTemplate);
				resp.flushBuffer();
			}
		}
	}

	/**
	 * @param req
	 * @param resp
	 * @param dbUtil
	 * @param paramMap
	 * @param showAssignedLeads
	 *            TODO
	 * @throws IOException
	 */
	private void getLeadsMarkers(HttpServletRequest req, HttpServletResponse resp, DBUtil dbUtil, Map<String, Object> paramMap, boolean showAssignedLeads)
			throws IOException {
		CampaignMarkerDetailObject campaign = new CampaignMarkerDetailObject();
		// logger.info("Trying to Hit DB For Leads");
		// System.out.println("Trying to Hit DB For Leads");
		List<String> dispImgs = new ArrayList<String>();
		if (StringUtils.isNotBlank(req.getParameter("dispositionalImages"))) {
			for (String str : req.getParameter("dispositionalImages").split("~")) {
				dispImgs.add(str.trim());
			}
		}

		ServletOutputStream outputStream = resp.getOutputStream();
		String pointsTemplate = "";
		try {
			paramMap.clear();
			paramMap.put("header", true);
			pointsTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("StreamGeometry.xml", paramMap);
		} catch (Exception e) {
			e.printStackTrace();
		}

		outputStream.print(pointsTemplate);
		dbUtil.selectLeadsByCampaignAndICL(req.getParameter("campaignSeq"), req.getParameter("officeSeq"), req.getParameter("selectedFiles"), outputStream,
				dispImgs, showAssignedLeads,req.getParameter("useWirecenters"));
		try {
			paramMap.clear();
			paramMap.put("footer", true);
			pointsTemplate = FreeMarkerEngine.getInstance().evaluateTemplate("StreamGeometry.xml", paramMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		outputStream.print(pointsTemplate);
		resp.flushBuffer();
	}

	/**
	 * @param req
	 * @param resp
	 * @param dbUtil
	 * @throws IOException
	 */
	private void deletePolygons(HttpServletRequest req, HttpServletResponse resp, DBUtil dbUtil) throws IOException {
		if (StringUtils.isNotBlank(req.getParameter("territoryID")) && StringUtils.isNotBlank(req.getParameter("territoryName")))
			dbUtil.deleteCountyDetails(req.getParameter("territoryID"), req.getParameter("territoryName"), req.getParameter("territoryMapSeq"));
	}

	/**
	 * @param req
	 * @param resp
	 * @param dbUtil
	 * @throws IOException
	 */
	private void savePolygons(HttpServletRequest req, HttpServletResponse resp, DBUtil dbUtil) throws IOException {
		if ("ICLLEAD".equalsIgnoreCase(req.getParameter("territoryFor"))) {
			String campignSeq = req.getParameter("campaignSeq");
			String officeSeq = req.getParameter("officeSeq");
			Territory territory = new Territory();
			territory.setName(req.getParameter("title"));

			if (StringUtils.isNotBlank(officeSeq))
				territory.setOfficeKey(Integer.parseInt(officeSeq));
			Date now = new Date();
			territory.setCreatedDate(now);
			territory.setCreatedUser(""/* req.getUserPrincipal().getName() */);
			territory.setModifiedDate(now);
			territory.setModifiedUser(""/* req.getUserPrincipal().getName() */);
			Polygon polygon = new Polygon();
			String coordinates = req.getParameter("coordinates");
			polygon.setCoordinates(coordinates.substring(0, coordinates.length() - 1));
			if (!req.getParameter("styleColor").equalsIgnoreCase("undefined"))
				polygon.setColor(req.getParameter("styleColor"));
			// polygon.setFill(new
			// Boolean(req.getParameter("styleFill").trim()));
			polygon.setOutline(new Boolean(req.getParameter("styleFillOpacity").trim()));
			if (!req.getParameter("styleColor").equalsIgnoreCase("undefined"))
				polygon.setOpacity(req.getParameter("styleFillOpacity"));
			polygon.setLineColor(req.getParameter("styleLineColor"));
			polygon.setLineOpacity(req.getParameter("styleLineOpacity"));
			polygon.setCreatedDate(now);
			polygon.setCreatedUser(""/* req.getUserPrincipal().getName() */);
			polygon.setModifiedDate(now);
			polygon.setModifiedUser(""/* req.getUserPrincipal().getName() */);

			if (StringUtils.isNotBlank(req.getParameter("styleLineWidth")))
				polygon.setLineWidth(Integer.parseInt(req.getParameter("styleLineWidth")));
			List<Object> list = new ArrayList<Object>();
			DatabaseResult databaseResult;
			Long leadTerritoryDefSeq = null;

			if (req.getParameter("desc") != null && !req.getParameter("desc").startsWith("TerritoryType")) {
				if (StringUtils.isNotBlank(req.getParameter("territoryID"))) {
					leadTerritoryDefSeq = new Long(req.getParameter("territoryID"));
					list.add(req.getParameter("title"));
					list.add(leadTerritoryDefSeq);
					databaseResult = getPlatformService().loadResult(QueryUtils.getQuery().get("IS_CUSTOM_POLYGON_EXISTS_NAME_DEF_SEQ"), list);
					if (databaseResult.getData().size() > 0) {
						resp.getWriter().append("Polygon already exist!!!\n Try with other name");
						return;
					}
				} else {
					list.clear();
					list.add(new Integer(campignSeq));
					list.add(new Integer(officeSeq));
					list.add("CUSTOM_POLYGON");
					databaseResult = getPlatformService().loadResult(QueryUtils.getQuery().get("IS_POLYGON_EXISTS_ICLLEAD_DEF"), list);

					if (null != databaseResult.getData() && databaseResult.getData().size() > 0) {
						leadTerritoryDefSeq = (Long) databaseResult.getData().get(0).get(0).getActualValue();
					}
				}
				territory.setType("CUSTOM");
				territory.setDescription("TerritoryType:" + territory.getType());

				int territoryKey = dbUtil.insertTerritory(territory);
				territory.setKey(territoryKey);
				polygon.getTerritory().setKey(territoryKey);
				polygon.setTerritory(territory);
				List<Polygon> polygons = new ArrayList<Polygon>();
				polygons.add(polygon);
				dbUtil.insertPolygons(polygons);
				if (leadTerritoryDefSeq == null) {
					list.clear();
					list.add("CUSTOM_POLYGON");
					list.add(territory.getDescription());
					list.add(Boolean.TRUE);
					list.add(0);
					list.add(0);
					list.add(null);
					list.add(new Integer(campignSeq));
					list.add(new Integer(officeSeq));
					list.add(null);
					leadTerritoryDefSeq = getPlatformService().saveAndGetKey(QueryUtils.getQuery().get("INSERT_IMS_ICL_LEAD_TERRITORY_DEF_PARENT_TERRITORY"),
							list.toArray());
				}
				getPlatformService().update(QueryUtils.getQuery().get("INSERT_IMS_ICLLEAD_TERRITORY_MAP"), new Object[]{leadTerritoryDefSeq, territoryKey});
				// dbUtil.insertIclTerritory(territory);
				resp.getWriter().append("Created Successfully!");
			} else {
				list.clear();
				list.add(new Integer(campignSeq));
				list.add(new Integer(officeSeq));
				list.add(territory.getName());
				list.add(req.getParameter("desc").split(":")[1]);
				databaseResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TERRITORY_KEY_ON_CAMP_OFFICE"), list);
				int territoryKey = (Integer) databaseResult.getData().get(0).get(0).getActualValue();
				territory.setType(req.getParameter("desc").split(":")[1]);
				territory.setDescription("TerritoryType:" + territory.getType());
				territory.setKey(territoryKey);
				polygon.getTerritory().setKey(territoryKey);
				polygon.setTerritory(territory);
				dbUtil.updatePolygon(polygon);
				resp.getWriter().append("Updated Successfully!");
			}
		}
	}

	public PlatformService getPlatformService() {
		return ServiceLocator.getService(PlatformService.class);
	}

	public void setPlatformService(PlatformService platformService) {
		this.platformService = platformService;
	}
}
