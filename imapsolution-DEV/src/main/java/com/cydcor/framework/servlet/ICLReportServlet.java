package com.cydcor.framework.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.report.Exporter;
import com.cydcor.framework.report.impl.ExportContext;
import com.cydcor.framework.report.impl.ExporterImpl;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.utils.Grid;
import com.cydcor.framework.utils.GridRow;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;

public class ICLReportServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (StringUtils.isBlank(req.getParameter("campaignId"))) {
			return;
		}
		List inputs = new ArrayList();
		inputs.add(req.getParameter("campaignId"));
		inputs.add(req.getParameter("officeId"));
		if (req.getParameter("queryId").equalsIgnoreCase("ICL_LEADSHEET_REPORT2")) {
			inputs.add(req.getParameter("startDate"));
			inputs.add(req.getParameter("endDate"));
		}
		String filters = "";
		JSONObject jsonOB = null;
		try {

			if (StringUtils.isNotBlank(req.getParameter("filters"))) {
				jsonOB = JSONObject.fromObject(JSONSerializer.toJSON(req.getParameter("filters")));
				JSONArray rules = (JSONArray) jsonOB.get("rules");
				for (int i = 0; i < rules.size(); i++) {
					JSONObject rule = (JSONObject) rules.get(i);
					if ("PERSON_ID".equalsIgnoreCase(rule.getString("field")))
						filters = filters + " AND " + rule.getString("field") + " IN (SELECT PERSON_ID FROM MERLIN.AGENTS WHERE FIRST_NAME+LAST_NAME LIKE '%"
								+ rule.getString("data") + "%')";
					else
						filters = filters + " AND " + rule.getString("field") + " LIKE '%" + rule.getString("data") + "%'";
				}

			}
			String sql = QueryUtils.getQuery().get(req.getParameter("queryId"));
			int grpIndex = sql.lastIndexOf("? GROUP BY");
			if (grpIndex == -1)
				sql = sql + filters;
			else
				sql = sql.substring(0, grpIndex + 1) + filters + sql.substring(grpIndex + 1);
			int count = getPlatformService().loadResult(sql, inputs).getData().size();
			if (StringUtils.isBlank(req.getParameter("oper"))) {
				Float limit = new Float(req.getParameter("rows"));
				int page = new Integer(req.getParameter("page"));
				int startIdx = 0;
				int endIdx = 0;
				if (page == 1) {
					startIdx = 1;
					endIdx = limit.intValue();
				} else {
					startIdx = (page - 1 == 0 ? 1 : page - 1) * limit.intValue();
					endIdx = startIdx + limit.intValue();
				}
				DatabaseResult result = getPlatformService().loadResult(
						"select * from (select *,ROW_NUMBER() OVER (ORDER BY " + req.getParameter("sidx") + " " + req.getParameter("sord")
								+ ") AS rowNumber from (" + sql + ")as  X ) as Y where rowNumber >= " + startIdx + " and rowNumber <= " + endIdx, inputs);

				int totalPages = 0;
				if (count > 0) {
					float countf = count;
					totalPages = (int) Math.ceil((countf / limit));
				} else {
					totalPages = 0;
				}

				GridRow row;
				List<GridRow> rows = new ArrayList<GridRow>();
				String val = "";
				for (List<MapObject> cObj : result.getData()) {
					row = new GridRow();
					row.setId(cObj.get(0).getValue());
					List list = new ArrayList();
					if (req.getParameter("queryId").equalsIgnoreCase("ICL_LEADSHEET_REPORT3")) {
						boolean boldRequired = false;
						if (cObj.get(1).getValue().equalsIgnoreCase("TOTAL")) {
							boldRequired = true;
						}
						for (MapObject obj : cObj) {
							val = obj.getValue() != null ? obj.getValue().replaceAll("\n", " ") : obj.getValue();
							if (boldRequired) {
								val = "<b>" + val + "</b>";
							}
							list.add(val);
							row.setCell(list);
						}
					} else {
						for (MapObject obj : cObj) {
							list.add(obj.getValue() != null ? obj.getValue().replaceAll("\n", " ") : obj.getValue());
							row.setCell(list);
						}
					}
					rows.add(row);
				}
				Grid grid = new Grid();
				grid.setPage(new Integer(req.getParameter("page")));
				grid.setRows(rows);
				grid.setTotal(totalPages);
				grid.setRecords(count);

				jsonOB = JSONObject.fromObject(JSONSerializer.toJSON(grid));
				resp.setContentType("application/json");
				resp.getWriter().write(jsonOB.toString());
			} else {
				Exporter exporter = new ExporterImpl();
				ExportContext context = new ExportContext();
				context.setExportStream(resp.getOutputStream());
				context.setSqlConnecion(((JdbcTemplate) ServiceLocator.getService("jdbcTemplate")).getDataSource().getConnection());
				context.setSqlString(sql + "ORDER BY " + req.getParameter("sidx"));
				context.getSqlParameters().add(req.getParameter("campaignId"));
				context.getSqlParameters().add(req.getParameter("officeId"));
				context.setReportName(req.getParameter("reportName"));
				context.setReportType(req.getParameter("type"));
				if (req.getParameter("type").equalsIgnoreCase(Exporter.XLS))
					resp.setContentType("application/vnd.ms-excel");
				else if (req.getParameter("type").equalsIgnoreCase(Exporter.PDF))
					resp.setContentType("application/pdf");
				resp.setHeader("Content-Disposition", "attachment; filename=" + req.getParameter("reportName") + "." + req.getParameter("type"));
				exporter.export(context);
				resp.flushBuffer();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected PlatformService getPlatformService() {
		return ServiceLocator.getService(PlatformService.class);
	}
}
