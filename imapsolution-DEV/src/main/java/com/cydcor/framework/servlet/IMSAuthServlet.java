package com.cydcor.framework.servlet;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cydcor.ext.imap.db.DBUtil;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.exceptions.RoleNotFoundException;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.model.MerCampignDetails;
import com.cydcor.framework.model.MerICLDetails;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.utils.ServiceLocator;
import com.cydcor.integration.privilege.RetrieveMappingPrivilegeFromDB;
import com.cydcor.ws.WSMappingInterface;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class IMSAuthServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public static JdbcTemplate getJdbcTemplate() {
		if (jdbcTemplate == null) {
			jdbcTemplate = (JdbcTemplate) ServiceLocator
					.getService("JdbcTemplate");
			;
		}
		return jdbcTemplate;
	}

	public static JdbcTemplate getSecurityJdbcTemplate() {
		if (jdbcTemplate == null) {
			jdbcTemplate = (JdbcTemplate) ServiceLocator
					.getService("securityJdbcTemplate");
			;
		}
		return jdbcTemplate;
	}
	
	private static JdbcTemplate jdbcTemplate;

	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getSession().setAttribute("loggedInUserId",
				request.getParameter("userId"));
		//TODO: read XML from DB

		try {
			//Load properties file
			//Properties dbProperty = loadProperties("/db.properties");
			
			
			
			String authkey = request.getParameter("RequestID");
			String securityXML = request.getParameter("securityXML");
			String userIDFromURL = request.getParameter("userId");
			
			String xml = null;
			Properties custProperty = loadProperties("/CustomProps.properties");
			String logouturl = custProperty.getProperty("logoutUrl");
			
			if( !StringUtils.isBlank(authkey)) {
				String securityDB  = custProperty.getProperty("SecurityDBName");
				System.out.println("securityDB ::::" + securityDB);
				try {
				xml = (String) getSecurityJdbcTemplate().queryForObject("SELECT * FROM [" + securityDB + "].[dbo].RequestDetails WHERE RequestID =  '" + authkey + "'", new RowMapper() {
					@Override
					public Object mapRow(ResultSet rs, int rowNumber)
							throws SQLException {				
						return rs.getString("RequestValue");
					}
					
				});
				} catch(Exception e )
				{
					System.out.println("Exception in getting request Value ::::" + e.getMessage());
					response.sendRedirect(logouturl);
					return;
				}
				
				boolean isSessionValid = CheckForSessionValidity(xml, userIDFromURL);
				if(!isSessionValid)
				{
					System.out.println("Invalid Session :::: Logout" );
					response.sendRedirect(logouturl);
					return;
				}
			}
			if( !StringUtils.isBlank(securityXML)) {
				String str = java.net.URLDecoder.decode(securityXML);
				System.out.println(str);
				boolean isSessionValid = CheckForSessionValidity(str, userIDFromURL);
				if(!isSessionValid)
				{

					response.sendRedirect(logouturl);
					return;
				}
			}
			
			if(StringUtils.isBlank(securityXML)&& StringUtils.isBlank(authkey) )
			{
				response.sendRedirect(logouturl);
				return;
			}
			
			String internalLink = request.getParameter("InternalLink");
			String url = "";	

			//Get properties
			String defaultServer = custProperty.getProperty("defaultServer");
			List<String> profileNamesList = DBUtil.getDistinctApplicationProfiles(request.getParameter("userId"), defaultServer); // Lakshmi -- Hard wring the initial DB name to get the privilages
			
			String defaultServer2 = defaultServer;
			System.out.println("defaultServer2 <!!!!>:" + defaultServer2);
			if( profileNamesList.size()>0 )
			{
				defaultServer2 = profileNamesList.get(0);
				String shortProfileLabel = defaultServer2.endsWith(" Leads Profile")? defaultServer2.replace(" Leads Profile", "") : defaultServer2;
				shortProfileLabel = shortProfileLabel.trim();
				defaultServer2 = "verizonfios";
				if( shortProfileLabel.matches("Verizon B2B")) //== "Verizon B2B" )
				{
					defaultServer2 = "verizonb2b";
				}
				if( shortProfileLabel.matches("Verizon FIOS")) // == "Verizon FIOS" )
				{
					System.out.println("inside verizon ::::" + shortProfileLabel);
					defaultServer2 = "verizonfios";
				}			
				if( shortProfileLabel.matches("AT&T")) 
				{
					System.out.println("inside att ::::" + shortProfileLabel);
					defaultServer2 = "att";
				}				
				if( shortProfileLabel.matches("Century Link")) 
				{
					System.out.println("inside centurylink ::::" + shortProfileLabel);
					defaultServer2 = "centurylink";
				}		
				url = custProperty.getProperty(shortProfileLabel);
			}

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();


			
			//System.out.println("xml = > " + xml);
			String roleXML = xml;
			//Map imsMap = generateParamMap(roleXML);
//			request.getSession(true).setAttribute("getAccessLinks",
//					getAccessLinks((List) imsMap.get("rightsList")));
//			updateRoleCache(imsMap);
			
			String iframeURL;
			if (StringUtils.isBlank(internalLink)) {
				iframeURL = "/" + url + "uiController?pageName=HomePage&userId="
						+ request.getParameter("userId") + "&clientKey=" + defaultServer2;
			} else {
				iframeURL = "/" + url + "uiController?pageName=" + internalLink
						+ "&userId=" + request.getParameter("userId")
						+ "&clientKey=" + defaultServer2;
			}
			iframeURL = "/page1.jsp";
			System.out.println("iframeURL : " + iframeURL);
			RequestDispatcher rd_header = getServletContext()
					.getRequestDispatcher(
							"/header.jsp?firstName=" + request.getParameter("userId") //imsMap.get("firstName")
									+ "&lastName=" + ""); //imsMap.get("lastName"));
			RequestDispatcher rd_footer = getServletContext()
					.getRequestDispatcher("/footer.jsp");

			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/Home.jsp");

			rd_header.include(request, response);
			rd_footer.include(request, response);

		} catch (Exception e) {
			System.out
					.println("excepiton in imsauthservlet: " + e.getMessage());
			e.printStackTrace();
		}

	}

//	public Connection getSQLConnection() throws Exception {
//
//		//Load properties file
//		Properties dbProperty = loadProperties("/db.properties");
//		//Get properties
//		String url = dbProperty.getProperty("url");
//		String username = dbProperty.getProperty("username");
//		String password = dbProperty.getProperty("password");
//
//		//Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
//		Class clazz = Class.forName("net.sourceforge.jtds.jdbc.Driver");
//
//		System.out.println("clazz  = " + clazz);
//
//		//Create connection
//		Connection connection = DriverManager.getConnection(url, username,
//				password);
//
//		//Return
//		return connection;
//	}

	public Connection getSQLConnection2() throws Exception {

		//Load properties file
		//Properties dbProperty = loadProperties("/db.properties");
		Properties custProperty = loadProperties("/CustomProps.properties");
		//Get properties
		String url = custProperty.getProperty("lionurl");
		String username = custProperty.getProperty("lionusername");
		String password = custProperty.getProperty("lionpassword");

		//Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
		Class.forName("net.sourceforge.jtds.jdbc.Driver");
		//Create connection
		Connection connection = DriverManager.getConnection(url, username,
				password);

		//Return
		return connection;
	}

	public Properties loadProperties(String filename) throws IOException {
		// create properties object
		Properties properties = new Properties();
		// Load properties from class loader
		properties.load(IMSAuthServlet.class.getResourceAsStream(filename));
		// Return
		return properties;
	}

	//overrdiing roleobject
	public Map<String, Map<String, RoleObject>> updateRoleCache(Map roleMap)
			throws Exception {
		// Storing retrieved roleObject into Session

		String userId = (String) roleMap.get("userId");
		Map<String, Map<String, RoleObject>> roleObjMap = (Map) CydcorContext
				.getInstance().getCache().get("roleObjMap_" + userId);

		try {
			if (roleObjMap == null || roleObjMap.isEmpty()) {
				CydcorContext
						.getInstance()
						.getCache()
						.put("roleObjMap_" + userId,
								generateRoleObjectMap2(roleMap));
			} else if (!roleObjMap.containsKey("roleObjMap_" + userId)) {
				CydcorContext
						.getInstance()
						.getCache()
						.put("roleObjMap_" + userId,
								generateRoleObjectMap2(roleMap));

			}

			roleObjMap = (Map) CydcorContext.getInstance().getCache()
					.get("roleObjMap_" + userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roleObjMap;

	}

	public Map<String, Map<String, RoleObject>> generateRoleObjectMap(
			Map roleMap) throws Exception {

		RoleObject roleObject = null;
		Map<String, RoleObject> roleSpecificMap = null;
		Map<String, Map<String, RoleObject>> roleObjMap = new HashMap<String, Map<String, RoleObject>>();

		String roleId = (String) roleMap.get("roleId");
		String userId = (String) roleMap.get("userId");
		roleSpecificMap = new HashMap<String, RoleObject>();
		roleObjMap.put(userId, roleSpecificMap);

		roleObject = new RoleObject();

		roleObject.setOwnerUserSignInId(userId);
		roleObject.setRoleName(roleId);
		roleSpecificMap.put(roleId, roleObject);

		return roleObjMap;
	}

	public Map<String, Map<String, RoleObject>> generateRoleObjectMap2(
			Map roleMap) throws Exception {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		String xml = null;

		String roleId = (String) roleMap.get("roleId");
		String userId = (String) roleMap.get("userId");
		//System.out.println("userId: " + userId);
		String sql = "SELECT * FROM IMS.IMS_CRM_MAPPING_PRIVILEGES WHERE LOGIN_ID=? ";
		//System.out.println("sql: " + sql);
		connection = getSQLConnection2();
		//System.out.println("Connection established" );
		statement = connection.prepareStatement(sql);
		statement.setString(1, userId);
		//System.out.println("sql: " + statement.toString());
		result = statement.executeQuery();
		//System.out.println("After execute query" );

		RoleObject roleObject = null;
		Map<String, RoleObject> roleSpecificMap = null;
		Map<String, Map<String, RoleObject>> roleObjMap = new HashMap<String, Map<String, RoleObject>>();

		if (result.next()) {
			//System.out.println("After data size");
			roleSpecificMap = new HashMap<String, RoleObject>();
			roleObjMap.put(userId.toUpperCase(), roleSpecificMap);

			//roleObject = roleSpecificMap.get(data.get(0).get(2).getValue());
			if (roleObject == null) {
				roleObject = new RoleObject();
				//roleObject.setOwnerIntegrationId(data.get(0).get(0).getValue());
				roleObject.setRoleName(result.getString("ROLE"));
				roleObject.setOwnerAlias(result.getString("USER_ALIAS"));
				//System.out.println("profile name: " + result.getString(4));
				roleObject.addProfileNames(result.getString(4));
				roleObject.setCreated(result.getString(7));
				roleObject.setModified(result.getString(8));
				roleObject.setOwnerFirstName(result.getString(9));
				roleObject.setOwnerLastName(result.getString(10));
				roleObject.setOwnerFullName(result.getString(11));
				roleObject.setOwnerEmailAddress(result.getString(12));
				roleObject.setOwnerUserSignInId(result.getString(13));
				roleSpecificMap.put(userId.toUpperCase(), roleObject);
				roleObject.setMerlinICL(new ArrayList<MerICLDetails>());
				roleObject.setMerlinCampaign(new ArrayList<MerCampignDetails>());
				
				MerICLDetails iclDetails = new MerICLDetails();
				System.out.println("setOffice_seq : " + result.getString(6));
				iclDetails.setOffice_seq(result.getString(6));
				roleObject.getMerlinICL().add(iclDetails);

				MerCampignDetails campaignObjs = new MerCampignDetails();
				//System.out.println("setCampaign_seq : "+ result.getString(5));
				campaignObjs.setCampaign_seq(result.getString(5));
				roleObject.getMerlinCampaign().add(campaignObjs);
			}
			MerICLDetails iclDetails = new MerICLDetails();
			System.out.println("setOffice_seq : " + result.getString(6));
			iclDetails.setOffice_seq(result.getString(6));
			roleObject.getMerlinICL().add(iclDetails);

			MerCampignDetails campaignObjs = new MerCampignDetails();
			//System.out.println("setCampaign_seq : "+ result.getString(5));
			campaignObjs.setCampaign_seq(result.getString(5));
			roleObject.getMerlinCampaign().add(campaignObjs);
			//System.out.println("Out : "+ result.getString(1));
		}
		//		roleSpecificMap = new HashMap<String, RoleObject>();
		//		roleObjMap.put(userId,roleSpecificMap);
		//		
		//		roleObject = new RoleObject();
		//				
		//		roleObject.setOwnerUserSignInId(userId);
		//		roleObject.setRoleName(roleId);
		//		roleSpecificMap.put(roleId, roleObject);

		return roleObjMap;
	}

	private boolean CheckForSessionValidity(String json, String userIDFromURL)throws Exception {
	
		boolean retVal = false;
		try {
			
			JSONObject obj = new JSONObject(json);
			JSONObject userInfoJson = obj.getJSONObject("UserInformation");
			JSONObject sessionDetailsJson = obj.getJSONObject("SessionDetails");
			java.util.Date dateNow = new java.util.Date();
			
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			Date loginDate = formatter.parse((String)sessionDetailsJson.get("LoginTime"));
			Date expiryDate = formatter.parse((String)sessionDetailsJson.get("ExpiryTime"));
			System.out.println("loginDate : "+ loginDate.toString());
			System.out.println("expiryDate : "+ expiryDate.toString());
			java.util.Date currentDate = new java.util.Date();
			
			if( currentDate.compareTo(loginDate) > 0 &&  currentDate.compareTo(expiryDate)< 0 )
			{
				retVal = true;
			}
			
			if( retVal == true)
			{
				System.out.println("userIDFromDB : "+ (String) userInfoJson.get("UserName"));
				String userIDFromPortal = (String) userInfoJson.get("UserName");
				System.out.println("userIDFromPortal : "+ userIDFromPortal);
				if(!userIDFromPortal.matches(userIDFromURL))
				{
					retVal = false;
				}
				
			}
			


		
		} catch (Exception e) {
			System.out.println("excpeiton in generating map from json"
					+ e.getMessage());
		}
		
		return retVal;
	}
	
	//reading roles from json
	private Map generateParamMap(String json) throws Exception {

		System.out.println("json: " + json);
		Map paramMap = new HashMap();
		List<String> rightsList = new ArrayList<String>();
		System.out.println("before try ");

		try {
			JSONObject obj = new JSONObject(json);
			JSONObject userInfoJson = obj.getJSONObject("UserInformation");
			JSONObject rightsInfoJson = obj.getJSONObject("RightsInformation");
			JSONObject sessionDetailsJson = obj.getJSONObject("SessionDetails");
			JSONArray rightsArray = new JSONArray();

			paramMap.put("firstName", userInfoJson.get("FirstName"));
			paramMap.put("lastName", userInfoJson.get("LastName"));
			paramMap.put("userId", userInfoJson.get("UserName"));
			paramMap.put("roleId", rightsInfoJson.get("RoleID"));

			rightsArray = rightsInfoJson.getJSONArray("Rights");

			for (int i = 0; i < rightsArray.length(); i++) {
				JSONObject right = (JSONObject) rightsArray.get(i);
				rightsList.add((String) right.get("RightName"));
			}

			paramMap.put("rightsList", rightsList);
		} catch (Exception e) {
			System.out.println("excpeiton in generating map from json"
					+ e.getMessage());
		}

		return paramMap;

	}

	//reading roles from xml 
	private Map generateParamMapFromXML(String XML) throws Exception {
		Map paramMap = new HashMap();
		try {
			List<String> rightsList = new ArrayList<String>();
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = factory.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(XML.getBytes());
			Document doc = dBuilder.parse(is);
			NodeList nodesList = doc
					.getElementsByTagName("SecurityPermissions");
			NodeList roleList = null;
			NodeList userinfo = null;
			RoleObject roleObject = new RoleObject();
			NodeList nodeList = null;
			for (int temp = 0; temp < nodesList.getLength(); temp++) {
				Node nNode = nodesList.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					nodeList = eElement.getChildNodes();

				}
			}

			for (int temp = 0; temp < nodeList.getLength(); temp++) {

				Node nNode = nodeList.item(temp);

				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					String name = eElement.getNodeName();

					if ("RightsInformation".equalsIgnoreCase(name)) {
						roleList = eElement.getChildNodes();
						paramMap.put("roleId", eElement.getAttribute("RoleID"));
					}

					if ("userinformation".equalsIgnoreCase(name))
						userinfo = eElement.getChildNodes();

				}
			}

			if (userinfo != null) {

				for (int temp = 0; temp < userinfo.getLength(); temp++) {

					Node nNode = userinfo.item(temp);

					//System.out.println("\nCurrent Element :" + nNode.getNodeName());

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;
						String name = eElement.getNodeName();
						//System.out.println("name:"+name);
						if ("firstname".equalsIgnoreCase(name)) {
							paramMap.put("firstName", nNode.getTextContent());
						}

						if ("lastname".equalsIgnoreCase(name)) {
							paramMap.put("lastName", nNode.getTextContent());
						}

						if ("userid".equalsIgnoreCase(name)) {
							paramMap.put("userId", nNode.getTextContent());
						}

					}
				}
			}

			if (roleList != null) {

				for (int temp = 0; temp < roleList.getLength(); temp++) {

					Node nNode = roleList.item(temp);

					//System.out.println("\nCurrent Element :" + nNode.getNodeName());

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;
						String name = eElement.getNodeName();
						//System.out.println("name:"+name);
						if ("right".equalsIgnoreCase(name)) {
							String rightName = eElement.getAttribute("name");
							rightsList.add(rightName);

							//paramMap.put(roleName, true);
						}

					}
				}

				paramMap.put("rightsList", rightsList);

			}

		} catch (Exception e) {
			//System.out.println("e::"+e.getMessage());
			e.printStackTrace();
		}
		//System.out.println("roMap::::::::::::"+paramMap);

		return paramMap;
	}

	public static List<ListOrderedMap> getAccessLinks(List rightsList)
			throws Exception {
		List<ListOrderedMap> list = new ArrayList<ListOrderedMap>();
		for (Object right : rightsList) {
			String sRight = (String) right;
			ListOrderedMap map = new ListOrderedMap();
			map.put("LINK_NAME", sRight);
			list.add(map);
		}
		return list;
	}

}
