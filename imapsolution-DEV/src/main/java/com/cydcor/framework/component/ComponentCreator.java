/**
 * 
 */
package com.cydcor.framework.component;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.DynamicUIContext;

/**
 * @author ashwin
 * 
 */
public interface ComponentCreator {

	/**
	 * @param uiContext
	 * @return
	 */
	public GenericUIControl getComponent(DynamicUIContext uiContext);

}
