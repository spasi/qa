package com.cydcor.framework.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.integration.privilege.utils.MappingPrivilegeUtil;

public class MappingPrivilegeTest {
	private static final transient Log logger = LogFactory
			.getLog(MappingPrivilegeTest.class);

	public static void main(String[] args) {
		
		//System.out.println(""+MappingPrivilegeUtil.getOfficeSeqsByUserId("JCOONROD"));
		
		//System.out.println(MappingPrivilegeUtil.getCampaignSeqsByUserId("CYDCOR/JCOONROD"));
		try {
			System.out.println(MappingPrivilegeUtil.getOfficeSeqsByUserId("CYDCOR/JCOONROD"));
			System.out.println(MappingPrivilegeUtil.getCampaignSeqsByUserId("CYDCOR/JCOONROD"));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		
	/*	try {
			WSMappingInterface wsInterface = new RetrieveMappingPrivilege();
			Map<String, RoleObject> roleObj = wsInterface.call();
			System.out.println("No Of Privileges :"+roleObj.size());
			System.out.println("Owner Role Name :"+roleObj.get("CYDCOR/JCOONROD").getRoleName());
			System.out.println("Owner First Name :"+roleObj.get("CYDCOR/JCOONROD").getOwnerFirstName());
			System.out.println("Owner Last Name :"+roleObj.get("CYDCOR/JCOONROD").getOwnerLastName());
			System.out.println("Owner Full Name :"+roleObj.get("CYDCOR/JCOONROD").getOwnerFullName());
			System.out.println("Owner Integration ID :"+roleObj.get("CYDCOR/JCOONROD").getOwnerIntegrationId());
			System.out.println("Owner Alias :"+roleObj.get("CYDCOR/JCOONROD").getOwnerAlias());
			
			System.out.println("ICL Details:");
			for (Iterator iterator = roleObj.get("CYDCOR/JCOONROD").getMerlinICL().iterator(); iterator.hasNext();) {
				MerICLDetails type = (MerICLDetails) iterator.next();
				System.out.println("OFFICE SEQ : "+type.getOffice_seq());
				System.out.println("ICL CODE : "+type.getOffice_code());
				System.out.println("ICL NAME : "+type.getOffice_name());
			}
			
			System.out.println("Campaign Details:");
			for (Iterator iterator = roleObj.get("CYDCOR/JCOONROD").getMerlinCampaign().iterator(); iterator.hasNext();) {
				MerCampignDetails type = (MerCampignDetails ) iterator.next();
				System.out.println("Campaign SEQ : "+type.getCampaign_seq());
				System.out.println("Campaign Name : "+type.getCampaign_name());
				System.out.println("Campaign Type : "+type.getCampaign_type());
				System.out.println("Campaign Manager : "+type.getCampaign_manager());
				System.out.println("Active : "+type.getActive());
				System.out.println("End Date : "+type.getEnd_date());
			}
			
			System.out.println("ICL Size :"+roleObj.get("CYDCOR/JCOONROD").getMerlinICL().size());
			System.out.println("Campaign Size :"+roleObj.get("CYDCOR/JCOONROD").getMerlinCampaign().size());
			// wsInterface.call("AGEA-YOBJL"); "AGEA-WVNSJ"
		} catch (Exception e) {
			logger.debug(e);
		}*/
	}
}