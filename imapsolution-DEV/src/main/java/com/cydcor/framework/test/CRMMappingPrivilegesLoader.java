package com.cydcor.framework.test;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.model.MerCampignDetails;
import com.cydcor.framework.model.MerICLDetails;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.utils.ODSessionPool;
import com.cydcor.framework.utils.PropertyUtils;
import com.cydcor.framework.ws.GenericWS;
import com.cydcor.integration.privilege.RetrieveMappingPrivilegeFromCRM;
import com.cydcor.ws.stubs.privillege.CustomObject6Data;
import com.cydcor.ws.stubs.privillege.CustomObject6_ServiceLocator;

public class CRMMappingPrivilegesLoader extends GenericWS {
	private static final transient Log logger = LogFactory.getLog(CRMMappingPrivilegesLoader.class);

	public static void executeProcess() {
		RetrieveMappingPrivilegeFromCRM privilegeFromCRM = new RetrieveMappingPrivilegeFromCRM();
		List<CustomObject6Data> listCustomObjectData = new ArrayList<CustomObject6Data>();

		CustomObject6_ServiceLocator locator = new CustomObject6_ServiceLocator();
		String sessionId = null;
		try {
			sessionId = ODSessionPool.getInstance().getODSessionId();
		} catch (Exception e) {
			logger.error(e);
		}
		logger.debug(sessionId);
		try {
			locator.setCustomObject6EndpointAddress(PropertyUtils.getInstance().getProperty().get("wsLocation") + ";jsessionid=" + sessionId);
		} catch (IOException e) {
			logger.error(e);
		}
		logger.info("START TIME OF RetrieveMappingPrivillege ::" + new Date(System.currentTimeMillis()));

		try {
			listCustomObjectData = privilegeFromCRM.getAllPrivileges(locator, listCustomObjectData, 0, null);
		} catch (RemoteException e) {
			logger.error(e);
		} catch (ServiceException e) {
			logger.error(e);
		}
		RoleObject roleObject = null;
		Map<String, RoleObject> roleSpecificMap = null;
		for (CustomObject6Data object : listCustomObjectData) {
			if (roleSpecificMap == null) {
				roleSpecificMap = new HashMap<String, RoleObject>();
			}

			roleObject = roleSpecificMap.get(object.getName());
			if (roleObject == null) {
				roleObject = new RoleObject();
				roleSpecificMap.put(object.getName(), roleObject);
			}
			roleObject.setOwnerUserSignInId(object.getOwnerUserSignInId());
			roleObject.setCreated(object.getCreatedBy());
			roleObject.setModified(object.getModifiedBy());
			roleObject.setOwnerIntegrationId(object.getName());
			roleObject.setOwnerAlias(object.getOwnerAlias());
			roleObject.setMerlinICL(new ArrayList<MerICLDetails>());
			roleObject.setMerlinCampaign(new ArrayList<MerCampignDetails>());
			roleObject.setRoleName(object.getIndexedPick0());
			roleObject.setOwnerFirstName(object.getOwnerFirstName());
			roleObject.setOwnerLastName(object.getOwnerLastName());
			roleObject.setOwnerFullName(object.getOwnerFullName());
			roleObject.setOwnerEmailAddress(object.getOwnerEMailAddr());
			roleObject.setOwnerUserSignInId(object.getOwnerUserSignInId());
			if (roleObject != null)
				roleObject.addProfileNames(object.getIndexedPick1());
			MerICLDetails iclDetails = new MerICLDetails();
			iclDetails.setOffice_seq(object.getCustomObject4ExternalSystemId());
			roleObject.getMerlinICL().add(iclDetails);

			MerCampignDetails campaignObjs = new MerCampignDetails();
			campaignObjs.setCampaign_seq(object.getAccountExternalSystemId());
			roleObject.getMerlinCampaign().add(campaignObjs);
		}
		List<RoleObject> roleObjectList = new ArrayList<RoleObject>();
		roleObjectList.addAll(roleSpecificMap.values());
		getPlatformService().populateCRMPrivileges(roleObjectList);
	}

}
