package com.cydcor.framework.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringPage {
private static ApplicationContext context=null;
	
	static{
		context = new ClassPathXmlApplicationContext(new String[] {"applicationContext*.xml"});
	}
}
