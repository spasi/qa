package com.cydcor.framework.service;

public interface TestWS {
	public String getName();
	public void updateName(String name);
}
