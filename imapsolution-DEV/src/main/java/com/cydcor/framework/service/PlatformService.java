/**
 * 
 */
package com.cydcor.framework.service;

import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.GenericModel;
import com.cydcor.framework.model.LeadLifeCycle;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.MerOffice;
import com.cydcor.framework.model.MerOfficeInsert;
import com.cydcor.framework.model.ReportComponent;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.model.UserObject;
import com.cydcor.framework.ui.model.RequestObject;

/**
 * All Generic Service methods are Handled here
 * 
 * @author ashwin
 * 
 */
/**
 * @author suresh.chittala
 * 
 */
public interface PlatformService {

    /**
     * @param lwbCode
     * @return null if user not there/or not authenticated
     */
    public UserObject loadUser(String lwbCode);

    /**
     * For any Query Fired it creates MetadataObject along with data returned
     * frm database and wraps it in DatabaseResult
     * 
     * @param sql
     * @return
     */
    public DatabaseResult loadResult(String sql);

    /**
     * @param reportName
     * @return
     */
    public ReportComponent loadReport(String reportName);

    /**
     * ReportComponent Here acts as a Value Object for the data sent from
     * Request
     * 
     * Set Report ID and Filter Parameters in report Component Object and pass
     * to this method and this method processes further and returns result
     * 
     * @param reportComponent
     * @return
     */
    public DatabaseResult loadData(ReportComponent reportComponent, RequestObject requestObject);

    /**
     * @param sql
     * @param rowMapper
     * @return
     */
    public List<? extends GenericModel> loadRowMapper(String sql, RowMapper rowMapper);

    /**
     * @param query
     * @param values
     */
    public void save(String query, Object[] values);

    /**
     * @param insertSql
     * @param values
     */
    public void populateLeadInput(List<LeadLifeCycle> values);

    /**
     * @param merOfficeInserts
     */
    public void populateIclMaster(List<MerOfficeInsert> merOfficeInserts);

    /**
     * @param merOffice
     */
    public void deleteOfficeSeqs(List<MerOffice> merOffice);

    /**
     * @param sql
     */
    public void execute(String sql);

    /**
     * @param sql
     * @param args
     */
    public void update(String sql, Object[] args);

    /**
     * @param sql
     * @param values
     * @return
     */
    public DatabaseResult loadResult(String sql, List values);

    /**
     * 
     * @param values
     */
    public void updateLeadLifeCycle(final List<LeadLifeCycle> values);

    // public DatabaseResult query(String string, List<String> list);

    public Long saveAndGetKey(final String sql, final Object[] args);

    /**
     * @param campaignSeq
     * @param officeSeq
     * @param templateName
     *            TODO
     * @return return template params assigned to this guy
     */
    public DatabaseResult loadTemplateParams(int campaignSeq, int officeSeq, String templateName);

    public DatabaseResult loadSystemDefaultTemplateParams(int campaignSeq, int officeSeq, String templateName);

    /**
     * @param campaignSeq
     * @param templateName
     * @return return template params assigned to this Campaign
     */
    public DatabaseResult loadCampaignTemplateParams(int campaignSeq, String templateName);

    void insertWireCenters(String[] values);

    void insertDAs(String[] values);

    public List<LeadMaster> selectLeadsByLeadSheets(String leadSheetIds);

    String saveOrMergeLeadsheets(final String tempLeadSheetIds, final String savedLeadSheetId,
	    final boolean saveAllSheets, final boolean isAssigned, final String campaignSeq, final String officeSeq);

    public void updateDispositionAndNotes(final List<String> dis, final List<String> diDescs, final List<String> notes,
	    final List<String> rowIds, final String leadSheetId, String archived);

    public void populateCRMPrivileges(List<RoleObject> roleObjects);

    public void updateLeadsheetPrintStatus(List<String> values);

    void updateLeadSheets(List<String[]> values);
    
    public void upsertATTOwner(final String cyCode, final String merlinCode, final String sfOwnerId, final String region, final String user, final String active, final String action);
    
    public void upsertMergedOwners(final String fromOffice, final String toOffice, final String user, final String active, final String action);
}
