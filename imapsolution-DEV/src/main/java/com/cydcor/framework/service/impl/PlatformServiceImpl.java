/**
 * 
 */
package com.cydcor.framework.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.cydcor.framework.dao.PlatformDAO;
import com.cydcor.framework.model.DBEntityComponent;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.GenericModel;
import com.cydcor.framework.model.LeadLifeCycle;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.model.MerOffice;
import com.cydcor.framework.model.MerOfficeInsert;
import com.cydcor.framework.model.ReportComponent;
import com.cydcor.framework.model.RoleObject;
import com.cydcor.framework.model.UserObject;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.ui.model.RequestObject;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * @author ashwin
 * 
 */
@Service("platformService")
public class PlatformServiceImpl implements PlatformService {
    protected final Log logger = LogFactory.getLog(getClass());
    private PlatformDAO platformDAO;

    /**
     * @return the platformDAO
     */
    public PlatformDAO getPlatformDAO() {
	if (platformDAO == null) {
	    platformDAO = (PlatformDAO) ServiceLocator.getService("platformDAO");
	}
	return platformDAO;

    }

    /**
     * @param platformDAO
     *            the platformDAO to set
     */
    public void setPlatformDAO(PlatformDAO platformDAO) {
	this.platformDAO = platformDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.linkwithweb.framework.service.PlatformService#loadUser(java.lang.
     * String)
     */
    public UserObject loadUser(String lwbCode) {
	return getPlatformDAO().loadUser(lwbCode);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.linkwithweb.framework.service.PlatformService#loadResult(java.lang
     * .String)
     */
    public DatabaseResult loadResult(String sql) {
	return getPlatformDAO().loadResult(sql);
    }

    /**
     * Load Definition of report from Database
     * 
     * @param reportName
     * @return
     */
    public ReportComponent loadReport(String reportName) {
	return getPlatformDAO().loadReport(reportName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.linkwithweb.framework.service.PlatformService#loadData(com.linkwithweb
     * .framework.model.ReportComponent,
     * com.linkwithweb.framework.ui.model.RequestObject)
     */
    public DatabaseResult loadData(ReportComponent reportComponent, RequestObject requestObject) {
	// If GetTotal is true then return max rows for query
	int totalRows = -1;

	// Now Create Query Out of it and Load Data
	StringBuffer sb = new StringBuffer();

	DatabaseResult databaseResult = null;

	/**
	 * First find if the type of Report is table or sql. If it is table we
	 * have to construct Sql else we can execute the given sql Before
	 * executing SQL it has to pass through Freemarker Engine for Evaluation
	 * of any Dynamic Parameters
	 */
	if (StringUtils.equalsIgnoreCase(DBEntityComponent.EntityTypes.SQL, reportComponent.getEntityType())) {
	    StringBuffer queryToExecute = new StringBuffer();
	    queryToExecute.append(reportComponent.getEntityValue());

	    StringBuffer queryToCount = new StringBuffer();

	    appendFilters(reportComponent, queryToExecute);

	    queryToCount.append("SELECT count(*) FROM (").append(queryToExecute.toString()).append(") A");
	    if (requestObject.isQueryForTotal()) {
		totalRows = getPlatformDAO().countRows(queryToCount.toString());
		queryToCount = null;
	    }

	    queryToExecute.append(" LIMIT ").append(requestObject.getOffset()).append(",")
		    .append(requestObject.getPageSize());

	    String sqlAfterEvaluation = FreeMarkerEngine.getInstance().evaluateString(queryToExecute.toString());
	    databaseResult = loadResult(sqlAfterEvaluation);

	} else if (StringUtils.equalsIgnoreCase(DBEntityComponent.EntityTypes.TABLE, reportComponent.getEntityType())) {
	    StringBuffer queryToExecute = new StringBuffer();
	    StringBuffer queryToCount = new StringBuffer();
	    queryToExecute.append("SELECT * FROM ").append(reportComponent.getSchemaName()).append(".")
		    .append(reportComponent.getEntityValue()).append(" WHERE 1=1 ");

	    appendFilters(reportComponent, queryToExecute);

	    queryToCount.append("SELECT count(*) FROM (").append(queryToExecute.toString()).append(") A");
	    if (requestObject.isQueryForTotal()) {
		totalRows = getPlatformDAO().countRows(queryToCount.toString());
		queryToCount = null;
	    }

	    queryToExecute.append(" LIMIT ").append(requestObject.getOffset()).append(",")
		    .append(requestObject.getPageSize());

	    String sqlAfterEvaluation = FreeMarkerEngine.getInstance().evaluateString(queryToExecute.toString());
	    databaseResult = loadResult(sqlAfterEvaluation);
	}

	if (totalRows != -1) {
	    databaseResult.setTotalRows(String.valueOf(totalRows));
	}

	return databaseResult;
    }

    /**
     * @param reportComponent
     * @param queryToExecute
     */
    private void appendFilters(ReportComponent reportComponent, StringBuffer queryToExecute) {
	// Add Filters to the Query
	for (MapObject mapObject : reportComponent.getRequestFilters()) {
	    queryToExecute.append(" AND ").append(mapObject.getKey()).append(" LIKE '%").append(mapObject.getValue())
		    .append("%' ");
	}
    }

    public List<? extends GenericModel> loadRowMapper(String sql, RowMapper rowMapper) {
	return getPlatformDAO().loadRowMapper(sql, rowMapper);
    }

    public void save(String query, Object[] values) {
	getPlatformDAO().save(query, values);

    }

    public void populateLeadInput(List<LeadLifeCycle> values) {
	getPlatformDAO().populateLeadInput(values);

    }

    public void execute(String sql) {
	getPlatformDAO().execute(sql);

    }

    public void update(String sql, Object[] args) {
	getPlatformDAO().update(sql, args);
    }

    public DatabaseResult loadResult(String sql, List values) {
	return getPlatformDAO().loadResult(sql, values);
    }

    /*
     * public DatabaseResult query(String sql, List args) { return
     * getPlatformDAO().query(sql, args); }
     */

    public void populateIclMaster(List<MerOfficeInsert> merOfficeInserts) {
	getPlatformDAO().populateIclMaster(merOfficeInserts);
    }

    public void deleteOfficeSeqs(List<MerOffice> merOffice) {
	getPlatformDAO().deleteOfficeSeqs(merOffice);

    }

    public void updateLeadLifeCycle(final List<LeadLifeCycle> values) {
	getPlatformDAO().updateLeadLifeCycle(values);
    }

    public Long saveAndGetKey(final String sql, final Object[] args) {
	return getPlatformDAO().saveAndGetKey(sql, args);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cydcor.framework.service.PlatformService#loadTemplateParams(int,
     * int)
     */
    public DatabaseResult loadTemplateParams(int campaignSeq, int officeSeq, String templateName) {
	String sql = QueryUtils.getQuery().get("SELECT_TEMPLATE_PARAMS");
	List values = new ArrayList();

	values.add(templateName);
	values.add(campaignSeq);
	values.add(officeSeq);

	return loadResult(sql, values);

    }

    public DatabaseResult loadSystemDefaultTemplateParams(int campaignSeq, int officeSeq, String templateName) {
	String sql = QueryUtils.getQuery().get("SELECT_TEMPLATE_PARAMS_WITH_SYSTEM_DEFAULT");
	List values = new ArrayList();

	return loadResult(sql, values);
    }

    public DatabaseResult loadCampaignTemplateParams(int campaignSeq, String templateName) {
	String sql = QueryUtils.getQuery().get("SELECT_CAMPAIGN_TEMPLATE_PARAMS");
	List list = new ArrayList();
	list.add(templateName);
	list.add(campaignSeq);
	return loadResult(sql, list);

    }

    public void insertDAs(String[] values) {
	getPlatformDAO().insertDAs(values);

    }

    public void insertWireCenters(String[] values) {
	getPlatformDAO().insertWireCenters(values);
    }

    public List<LeadMaster> selectLeadsByLeadSheets(String leadSheetIds) {
	return getPlatformDAO().selectLeadsByLeadSheets(leadSheetIds);
    }

    public String saveOrMergeLeadsheets(String tempLeadSheetIds, String savedLeadSheetId, boolean saveAllSheets,
	    boolean isAssigned, String campaignSeq, String officeSeq) {
	return getPlatformDAO().saveOrMergeLeadsheets(tempLeadSheetIds, savedLeadSheetId, saveAllSheets, isAssigned,
		campaignSeq, officeSeq);
    }

    public void updateDispositionAndNotes(List<String> dis, List<String> diDescs, List<String> notes,
	    List<String> rowIds, String leadSheetId, String archived) {
	getPlatformDAO().updateDispositionAndNotes(dis, diDescs, notes, rowIds, leadSheetId, archived);

    }

    public void populateCRMPrivileges(List<RoleObject> roleObjects) {
	getPlatformDAO().populateCRMPrivileges(roleObjects);

    }

    public void updateLeadsheetPrintStatus(List<String> values) {
	getPlatformDAO().updateLeadsheetPrintStatus(values);
    }

    public void updateLeadSheets(List<String[]> values) {
	getPlatformDAO().updateLeadSheets(values);

    }
    
    public void upsertATTOwner(final String cyCode, final String merlinCode, final String sfOwnerId, final String region, final String user, final String active, final String action){
    	getPlatformDAO().upsertATTOwner(cyCode, merlinCode, sfOwnerId, region, user, active, action);
    }
    
    public void upsertMergedOwners(final String fromOffice, final String toOffice, final String user, final String active, final String action){
    	getPlatformDAO().upsertMergedOwners(fromOffice, toOffice, user, active, action);
    }
}
