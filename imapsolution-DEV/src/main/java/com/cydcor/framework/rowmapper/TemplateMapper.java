package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.Template;

/**
 * @author ashwin
 * 
 */
public class TemplateMapper implements RowMapper {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 * int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Template template = new Template();
		template.setTemplateName(rs.getString("TEMPLATE_NAME"));
		template.setTemplateType(rs.getString("TEMPLATE_TYPE"));
		template.setTemplateContent(rs.getString("TEMPLATE_TEXT"));
		return template;
	}
}