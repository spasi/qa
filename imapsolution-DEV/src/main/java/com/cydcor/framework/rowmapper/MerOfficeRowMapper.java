package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.MerOffice;

/**
 * 
 SELECT TOP 1000 [OFFICE_SEQ] ,[OFFICE_CODE] ,[OFFICE_NAME]
 * ,[OFFICE_LEGAL_NAME] ,[OFFICE_ADDRESS1] ,[OFFICE_ADDRESS2] ,[OFFICE_CITY]
 * ,[OFFICE_ZIP] ,[OFFICE_STATE] ,[OFFICE_COUNTRY] ,[PHONE] ,[CELL] ,[FAX]
 * ,[EMAIL] ,[OWNER] ,[PROMOTING_MANAGER] ,[CONSULTANT] ,[CONSULTANT_NAME]
 * ,[ACTIVE] ,[START_DATE] ,[CLOSE_DATE] ,[CLOSED] ,[CLOSURE_REASON] ,[COMMENTS]
 * ,[CAMPAIGN_SEQ] ,[CAMPAIGN_OFFICE_START_DATE] ,[CAMPAIGN_OFFICE_END_DATE]
 * FROM [Cydcor].[IMS].[IMS_ICL_DETAILS]
 * 
 * @author ashwin
 * 
 */
public class MerOfficeRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		MerOffice merOffice = new MerOffice();
		try {
			merOffice.setOffice_seq(rs.getInt("OFFICE_SEQ"));
			merOffice.setOffice_code(rs.getString("OFFICE_CODE"));
			merOffice.setOffice_state(rs.getString("OFFICE_STATE"));
			merOffice.setOffice_city(rs.getString("OFFICE_CITY"));
			merOffice.setOffice_address1(rs.getString("OFFICE_ADDRESS1"));
			merOffice.setOffice_address2(rs.getString("OFFICE_ADDRESS2"));
			merOffice.setOffice_zip(rs.getString("OFFICE_ZIP"));
			merOffice.setActive(rs.getString("ACTIVE"));
			// merOffice.setHub_manager(rs.getString("HUB_MANAGER"));
			merOffice.setOwner(rs.getString("OWNER"));
			merOffice.setPromoting_manager(rs.getString("PROMOTING_MANAGER"));
			merOffice.setComments(rs.getString("COMMENTS"));
			merOffice.setEmail(rs.getString("EMAIL"));
			merOffice.setPhone(rs.getString("PHONE"));
			merOffice.setOffice_name(rs.getString("OFFICE_NAME"));
			merOffice.setFax(rs.getString("FAX"));
			merOffice.setCell(rs.getString("CELL"));
			// merOffice.setFed_ex(rs.getString("FED_EX"));
			merOffice.setStart_date(rs.getDate("START_DATE"));
			merOffice.setClose_date(rs.getDate("CLOSE_DATE"));
			// merOffice.setFein(rs.getString("FEIN"));
			// merOffice.setHub_manager_seq(rs.getLong("HUB_MANAGER_SEQ"));
			// merOffice.setOffice_update(rs.getString("OFFICE_UPDATE"));
			// merOffice.setQuarterback(rs.getString("QUARTERBACK"));
			// merOffice.setHot_jobs(rs.getString("HOT_JOBS"));
			// merOffice.setClass_id(rs.getString("CLASS_ID"));
			// merOffice.setWeb_user(rs.getString("WEB_USER"));
			// merOffice.setLast_update(rs.getString("LAST_UPDATE"));
			// merOffice.setLast_update_by(rs.getString("LAST_UPDATE_BY"));
			merOffice.setOffice_country(rs.getString("OFFICE_COUNTRY"));
			// merOffice.setInsurance_exp_date(rs.getString("INSURANCE_EXP_DATE"));
			// merOffice.setWork_comp_exp_date(rs.getString("WORK_COMP_EXP_DATE"));
			// merOffice.setPayroll_processor(rs.getString("PAYROLL_PROCESSOR"));
			// merOffice.setDate_of_birth(rs.getString("DATE_OF_BIRTH"));
			// merOffice.setAddress_change(rs.getString("ADDRESS_CHANGE"));
			// merOffice.setConsult_seq(rs.getLong("CONSULT_SEQ"));
			// merOffice.setConsultant(rs.getString("CONSULTANT"));
			// merOffice.setClosure_reason_seq(rs.getLong("CLOSURE_REASON_SEQ"));
			merOffice.setClosed(rs.getString("CLOSED"));
			// merOffice.setOffice_legal_name(rs.getString("OFFICE_LEGAL_NAME"));
			// merOffice.setSecondary_email(rs.getString("SECONDARY_EMAIL"));
		} catch (Exception e) {

		}
		return merOffice;

	}
}