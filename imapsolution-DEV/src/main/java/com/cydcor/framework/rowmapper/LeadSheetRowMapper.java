package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.LeadSheet;

public class LeadSheetRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		LeadSheet leadSheet = new LeadSheet();
		leadSheet.setLeadsheet_id(rs.getString("LEADSHEET_ID"));
		leadSheet.setLead_id(rs.getString("LEAD_ID"));
		leadSheet.setPerson_id(rs.getLong("PERSON_ID"));
		leadSheet.setCampaign_seq(rs.getLong("CAMPAIGN_SEQ"));
		leadSheet.setOffice_seq(rs.getLong("OFFICE_SEQ"));
		leadSheet.setCust_name(rs.getString("CUST_NAME"));
		leadSheet.setFull_addr(rs.getString("FULL_ADDR"));
		leadSheet.setApt_num(rs.getString("APT_NUM"));
		leadSheet.setCity(rs.getString("CITY"));
		leadSheet.setZip(rs.getString("ZIP"));
		leadSheet.setDa(rs.getString("DA"));
		leadSheet.setDisposition(rs.getString("DISPOSITION"));
		leadSheet.setNotes(rs.getString("NOTES"));
		leadSheet.setIs_active(rs.getString("IS_ACTIVE"));
		leadSheet.setCreated_date(rs.getTimestamp("CREATED_DATE"));
		leadSheet.setCreated_by(rs.getString("CREATED_BY"));
		leadSheet.setModified_date(rs.getTimestamp("MODIFIED_DATE"));
		leadSheet.setModified_by(rs.getString("MODIFIED_BY"));
		return leadSheet;
	}
}