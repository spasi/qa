package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.ImsLead;

public class ImsLeadRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ImsLead imsLead = new ImsLead();
		imsLead.setLead_id(rs.getString("LEAD_ID"));
		imsLead.setCampaign_seq(rs.getLong("CAMPAIGN_SEQ"));
		imsLead.setOffice_seq(rs.getLong("OFFICE_SEQ"));
		imsLead.setIcl_rep_seq(rs.getString("ICL_REP_SEQ"));
		imsLead.setRegion(rs.getString("REGION"));
		imsLead.setClli(rs.getString("CLLI"));
		imsLead.setDa(rs.getString("DA"));
		imsLead.setIs_active(rs.getString("IS_ACTIVE"));
		imsLead.setCust_name(rs.getString("CUST_NAME"));
		imsLead.setHouse_num(rs.getString("HOUSE_NUM"));
		imsLead.setStr_pre(rs.getString("STR_PRE"));
		imsLead.setStr_name(rs.getString("STR_NAME"));
		imsLead.setStr_type(rs.getString("STR_TYPE"));
		imsLead.setStr_post(rs.getString("STR_POST"));
		imsLead.setApt_num(rs.getString("APT_NUM"));
		imsLead.setCity(rs.getString("CITY"));
		imsLead.setState(rs.getString("STATE"));
		imsLead.setZip(rs.getString("ZIP"));
		imsLead.setDisposition_code(rs.getString("DISPOSITION_CODE"));
		imsLead.setDisposition(rs.getString("DISPOSITION"));
		imsLead.setNotes(rs.getString("NOTES"));
		imsLead.setLead_dob(rs.getTimestamp("LEAD_DOB"));
		imsLead.setLead_assigned(rs.getTimestamp("LEAD_ASSIGNED"));
		imsLead.setLead_expires(rs.getTimestamp("LEAD_EXPIRES"));
		imsLead.setModified_date(rs.getTimestamp("MODIFIED_DATE"));
		imsLead.setProcess_status(rs.getString("PROCESS_STATUS"));
		imsLead.setProcess_date(rs.getTimestamp("PROCESS_DATE"));
		imsLead.setError_message(rs.getString("ERROR_MESSAGE"));
		imsLead.setRetry_count(rs.getLong("RETRY_COUNT"));
		return imsLead;
	}
}