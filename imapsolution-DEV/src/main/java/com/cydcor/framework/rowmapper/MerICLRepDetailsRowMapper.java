package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.MerICLRepDetails;

public class MerICLRepDetailsRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		MerICLRepDetails merICLRepDetails = new MerICLRepDetails();
		merICLRepDetails.setPerson_id(rs.getString("PERSON_ID"));
		merICLRepDetails.setDob(rs.getTimestamp("DOB"));
		merICLRepDetails.setActive(rs.getString("ACTIVE"));
		merICLRepDetails.setStatus_seq(rs.getString("STATUS_SEQ"));
		merICLRepDetails.setFirst_name(rs.getString("FIRST_NAME"));
		merICLRepDetails.setLast_name(rs.getString("LAST_NAME"));
		merICLRepDetails.setMiddle_initial(rs.getString("MIDDLE_INITIAL"));
		merICLRepDetails.setSs_number(rs.getString("SS_NUMBER"));
		merICLRepDetails.setAddress1(rs.getString("ADDRESS1"));
		merICLRepDetails.setAddress2(rs.getString("ADDRESS2"));
		merICLRepDetails.setCity(rs.getString("CITY"));
		merICLRepDetails.setState(rs.getString("STATE"));
		merICLRepDetails.setZip(rs.getString("ZIP"));
		merICLRepDetails.setPhone1(rs.getString("PHONE1"));
		merICLRepDetails.setMobile_phone(rs.getString("MOBILE_PHONE"));
		merICLRepDetails.setCountry(rs.getString("COUNTRY"));
		merICLRepDetails.setProvince(rs.getString("PROVINCE"));
		merICLRepDetails.setGender(rs.getString("GENDER"));
		merICLRepDetails.setPay_rate(rs.getString("PAY_RATE"));
		merICLRepDetails.setHire_status(rs.getString("HIRE_STATUS"));
		merICLRepDetails.setHire_date(rs.getTimestamp("HIRE_DATE"));
		merICLRepDetails.setEmail_address(rs.getString("EMAIL_ADDRESS"));
		merICLRepDetails.setFed_marital_status(rs
				.getString("FED_MARITAL_STATUS"));
		merICLRepDetails.setFed_exemptions(rs.getString("FED_EXEMPTIONS"));
		merICLRepDetails.setTermination_date(rs
				.getTimestamp("TERMINATION_DATE"));
		merICLRepDetails.setCreation_date(rs.getTimestamp("CREATION_DATE"));
		merICLRepDetails.setLast_update_date(rs
				.getTimestamp("LAST_UPDATE_DATE"));
		merICLRepDetails.setCreated_by_id(rs.getString("CREATED_BY_ID"));
		merICLRepDetails.setLast_updated_by_id(rs
				.getString("LAST_UPDATED_BY_ID"));
		merICLRepDetails.setLocked(rs.getString("LOCKED"));
		merICLRepDetails.setState_code_of_conduct_signed(rs
				.getTimestamp("STATE_CODE_OF_CONDUCT_SIGNED"));
		merICLRepDetails.setAtt_code_of_conduct_signed(rs
				.getTimestamp("ATT_CODE_OF_CONDUCT_SIGNED"));
		merICLRepDetails.setCorporate_training_date(rs
				.getTimestamp("CORPORATE_TRAINING_DATE"));
		merICLRepDetails.setUnlock_request(rs.getString("UNLOCK_REQUEST"));
		merICLRepDetails.setTermination_reason(rs
				.getString("TERMINATION_REASON"));
		merICLRepDetails.setClosure_reason_seq(rs
				.getString("CLOSURE_REASON_SEQ"));
		merICLRepDetails.setProcess_status(rs.getString("PROCESS_STATUS"));
		merICLRepDetails.setProcess_date(rs.getTimestamp("PROCESS_DATE"));
		merICLRepDetails.setError_message(rs.getString("ERROR_MESSAGE"));
		merICLRepDetails.setRetry_count(rs.getLong("RETRY_COUNT"));
		return merICLRepDetails;
	}
}