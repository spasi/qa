package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.sf.click.util.ClickUtils;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.context.TableMetaDataCache;
import com.cydcor.framework.model.ColumnMetadata;
import com.cydcor.framework.model.DBEntityComponent;
import com.cydcor.framework.model.MapObject;

/**
 * This Class Create Table MetaData in First Call and then returns rowMap
 * 
 * @author ashwin
 * 
 */
public class TableComponentRowMapper implements RowMapper {

	/**
	 * The constant indicating that a column does not allow <code>NULL</code>
	 * values.
	 */
	int columnNoNulls = 0;

	/**
	 * The constant indicating that a column allows <code>NULL</code> values.
	 */
	int columnNullable = 1;

	/**
	 * The constant indicating that the nullability of a column's values is
	 * unknown.
	 */
	int columnNullableUnknown = 2;

	private String entityName = null;

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		int columnCount = rs.getMetaData().getColumnCount();

		DBEntityComponent entityComponent = TableMetaDataCache.getInstance()
				.getMetaData(rs.getMetaData().getTableName(1));

		entityName = rs.getMetaData().getTableName(1);
		// For First Row Create Metadata
		if (entityComponent == null) {
			entityComponent = new DBEntityComponent();

			entityComponent.setEntityDescription(rs.getMetaData().getTableName(
					1));
			entityComponent.setEntityName(rs.getMetaData().getTableName(1));

			for (int column = 1; column <= columnCount; column++) {
				ColumnMetadata columnMetadata = new ColumnMetadata();

				int nullableStatus = rs.getMetaData().isNullable(column);
				if (nullableStatus == columnNoNulls) {
					columnMetadata.setNullable(false);
				} else if (nullableStatus == columnNullable) {
					columnMetadata.setNullable(true);
				} else if (nullableStatus == columnNoNulls) {
					columnMetadata.setNullable(false);
				}

				columnMetadata.setCatalogName(rs.getMetaData().getCatalogName(
						column));
				columnMetadata.setTableName(rs.getMetaData().getTableName(
						column));
				columnMetadata.setCatalogName(rs.getMetaData().getCatalogName(
						column));
				columnMetadata.setColumnClassName(rs.getMetaData()
						.getColumnClassName(column));
				columnMetadata.setColumnDisplaySize(rs.getMetaData()
						.getColumnDisplaySize(column));
				columnMetadata.setColumnLabel(rs.getMetaData().getColumnLabel(
						column));
				columnMetadata.setColumnName(rs.getMetaData().getColumnName(
						column));
				columnMetadata.setColumnType(rs.getMetaData().getColumnType(
						column));
				columnMetadata.setColumnTypeName(rs.getMetaData()
						.getColumnTypeName(column));
				columnMetadata.setPrecision(rs.getMetaData().getPrecision(
						column));
				columnMetadata.setScale(rs.getMetaData().getScale(column));
				columnMetadata.setSchemaName(rs.getMetaData().getSchemaName(
						column));
				columnMetadata.setSigned(rs.getMetaData().isSigned(column));
				columnMetadata.setTableName(rs.getMetaData().getTableName(
						column));

				entityComponent.addColumnMetadata(columnMetadata.getTableName()
						+ "." + columnMetadata.getColumnName(), columnMetadata);
			}
			TableMetaDataCache.getInstance().storeMetaData(
					entityComponent.getEntityName(), entityComponent);
		}

		List<MapObject> rowList = new ArrayList<MapObject>();
		for (int i = 1; i <= columnCount; i++) {
			MapObject mapObject = new MapObject();
			mapObject.setKey(rs.getMetaData().getColumnName(i));
			
			Object actualValue =rs.getObject(i);
			
			String value = rs.getString(i);
			try {
				value = ClickUtils.escapeHtml(value);
			} catch (Exception e) {
				value = "";
			}
			mapObject.setValue(value);
			mapObject.setActualValue(actualValue);
			rowList.add(mapObject);

		}

		return rowList;
	}

	/**
	 * @return the entityName
	 */
	public String getEntityName() {
		return entityName;
	}

	/**
	 * @param entityName
	 *            the entityName to set
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

}