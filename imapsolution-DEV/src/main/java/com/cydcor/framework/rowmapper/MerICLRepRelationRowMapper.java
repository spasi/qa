package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.MerICLRepRelation;

public class MerICLRepRelationRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		MerICLRepRelation merICLRepRelation = new MerICLRepRelation();
		merICLRepRelation.setCampaign_seq(rs.getLong("CAMPAIGN_SEQ"));
		merICLRepRelation.setOffice_seq(rs.getLong("OFFICE_SEQ"));
		merICLRepRelation.setPerson_id(rs.getLong("PERSON_ID"));
		merICLRepRelation.setCampaign_agent_code(rs
				.getLong("CAMPAIGN_AGENT_CODE"));
		merICLRepRelation.setCampaign_office_relationship_start_date(rs
				.getTimestamp("CAMPAIGN_OFFICE_RELATIONSHIP_START_DATE"));
		merICLRepRelation.setCampaign_office_relationship_end_date(rs
				.getTimestamp("CAMPAIGN_OFFICE_RELATIONSHIP_END_DATE"));
		merICLRepRelation.setActive(rs.getString("ACTIVE"));
		merICLRepRelation.setProcess_status(rs.getString("PROCESS_STATUS"));
		merICLRepRelation.setProcess_date(rs.getTimestamp("PROCESS_DATE"));
		merICLRepRelation.setError_msg(rs.getString("ERROR_MSG"));
		merICLRepRelation.setRetry_count(rs.getString("RETRY_COUNT"));
		return merICLRepRelation;
	}
}