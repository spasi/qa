package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.LeadLifeCycleDetails;

public class LeadLifeCycleRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		LeadLifeCycleDetails leadLifeCycleDetails = new LeadLifeCycleDetails();
		leadLifeCycleDetails.setRun_id(rs.getString("RUN_ID"));
		leadLifeCycleDetails.setLead_id(rs.getString("LEAD_ID"));
		leadLifeCycleDetails.setCust_name(rs.getString("CUST_NAME"));
		leadLifeCycleDetails.setFull_addr(rs.getString("FULL_ADDR"));
		leadLifeCycleDetails.setApt_num(rs.getString("APT_NUM"));
		leadLifeCycleDetails.setCity(rs.getString("CITY"));
		leadLifeCycleDetails.setState(rs.getString("STATE"));
		leadLifeCycleDetails.setZip(rs.getString("ZIP"));
		leadLifeCycleDetails.setLead_expires(rs.getTimestamp("LEAD_EXPIRES"));
		leadLifeCycleDetails.setNotes(rs.getString("NOTES"));
		leadLifeCycleDetails.setGeo_point(rs.getString("GEO_POINT"));
		leadLifeCycleDetails
				.setValidation_code(rs.getString("VALIDATION_CODE"));
		leadLifeCycleDetails.setResponse_text(rs.getString("RESPONSE_TEXT"));
		leadLifeCycleDetails.setCampaign_seq(rs.getLong("CAMPAIGN_SEQ"));
		leadLifeCycleDetails.setOffice_seq(rs.getLong("OFFICE_SEQ"));
		leadLifeCycleDetails.setClli(rs.getString("CLLI"));
		leadLifeCycleDetails.setDa(rs.getString("DA"));
		leadLifeCycleDetails.setPerson_id(rs.getLong("PERSON_ID"));
		leadLifeCycleDetails.setMarker_icon_image(rs
				.getString("MARKER_ICON_IMAGE"));
		leadLifeCycleDetails.setLead_status(rs.getString("LEAD_STATUS"));
		leadLifeCycleDetails.setCredit_class(rs.getString("CREDIT_CLASS"));
		leadLifeCycleDetails.setGreen_status(rs.getString("GREEN_STATUS"));
		leadLifeCycleDetails.setConn_tech(rs.getString("CONN_TECH"));
		leadLifeCycleDetails.setDisposition_code(rs
				.getString("DISPOSITION_CODE"));
		leadLifeCycleDetails.setDisposition(rs.getString("DISPOSITION"));
		leadLifeCycleDetails.setIs_active(rs.getString("IS_ACTIVE"));
		leadLifeCycleDetails.setCreated_date(rs.getTimestamp("CREATED_DATE"));
		leadLifeCycleDetails.setCreated_by(rs.getString("CREATED_BY"));
		leadLifeCycleDetails.setModified_date(rs.getTimestamp("MODIFIED_DATE"));
		leadLifeCycleDetails.setModified_by(rs.getString("MODIFIED_BY"));
		leadLifeCycleDetails.setIcl_rep_seq(rs.getLong("ICL_REP_SEQ"));
		leadLifeCycleDetails.setRegion(rs.getString("REGION"));
		leadLifeCycleDetails.setStatus(rs.getString("STATUS"));
		leadLifeCycleDetails.setHouse_num(rs.getString("HOUSE_NUM"));
		leadLifeCycleDetails.setStr_pre(rs.getString("STR_PRE"));
		leadLifeCycleDetails.setStr_name(rs.getString("STR_NAME"));
		leadLifeCycleDetails.setStr_type(rs.getString("STR_TYPE"));
		leadLifeCycleDetails.setStr_post(rs.getString("STR_POST"));
		leadLifeCycleDetails.setLead_dob(rs.getTimestamp("LEAD_DOB"));
		leadLifeCycleDetails.setLead_assigned(rs.getTimestamp("LEAD_ASSIGNED"));
		leadLifeCycleDetails.setLast_updated(rs.getTimestamp("LAST_UPDATED"));
		leadLifeCycleDetails.setUpdate_crm(rs.getString("UPDATE_CRM"));
		leadLifeCycleDetails.setProcess_status(rs.getString("PROCESS_STATUS"));
		leadLifeCycleDetails.setProcess_date(rs.getTimestamp("PROCESS_DATE"));
		leadLifeCycleDetails.setError_message(rs.getString("ERROR_MESSAGE"));
		leadLifeCycleDetails.setRetry_count(rs.getLong("RETRY_COUNT"));
		return leadLifeCycleDetails;
	}
}