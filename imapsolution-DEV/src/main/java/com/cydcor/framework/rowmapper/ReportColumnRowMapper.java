package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.ReportColumn;

/**
 * SELECT * FROM LWB_FRM_REPORT_COLUMNS. This Class Loads ReportColumns
 * 
 * CREATE TABLE `healthcare`.`lwb_frm_report_columns` ( `ID` int(10) unsigned
 * NOT NULL AUTO_INCREMENT, `COLUMN_NAME` varchar(60) NOT NULL, `LABEL`
 * varchar(100) NOT NULL, `IS_FILTER_ENABLED` tinyint(1) NOT NULL,
 * `IS_SORT_ENABLED` tinyint(1) NOT NULL, `PANEL_NAME` varchar(45) DEFAULT NULL,
 * `IS_DISPLAYABLE` tinyint(1) NOT NULL, `DISPLAY_ORDER` int(10) unsigned NOT
 * NULL, `DATE_FORMAT` varchar(45) DEFAULT NULL, `NUMBER_FORMAT` varchar(45)
 * DEFAULT NULL, `TOOLTIP` varchar(45) DEFAULT NULL, `IS_READONLY` varchar(45)
 * NOT NULL, `DISPLAY_WIDTH` INTEGER DEFAULT NULL, `IS_ACTIVE` tinyint(1) NOT
 * NULL, `CREATED_BY` int(10) unsigned NOT NULL, `CREATED_DATE` datetime NOT
 * NULL, `MODIFIED_BY` int(10) unsigned NOT NULL, `MODIFIED_DATE` datetime NOT
 * NULL, `REF_REPORT_NAME` varchar(45) NOT NULL, PRIMARY KEY (`ID`) )
 * ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
 * 
 * @author ashwin
 * 
 */
public class ReportColumnRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReportColumn reportColumn = new ReportColumn();

		reportColumn.setId(rs.getLong("ID"));
		reportColumn.setColumnName(rs.getString("COLUMN_NAME"));
		reportColumn.setColumnLabel(rs.getString("LABEL"));
		reportColumn.setFilterEnabled(rs.getBoolean("IS_FILTER_ENABLED"));
		reportColumn.setSortEnabled(rs.getBoolean("IS_SORT_ENABLED"));
		reportColumn.setDisplayable(rs.getBoolean("IS_DISPLAYABLE"));

		reportColumn.setPanelName(rs.getString("PANEL_NAME"));
		reportColumn.setDisplayOrder(rs.getInt("DISPLAY_ORDER"));
		reportColumn.setDateFormat(rs.getString("DATE_FORMAT"));
		reportColumn.setNumberFormat(rs.getString("NUMBER_FORMAT"));
		reportColumn.setTooltip(rs.getString("TOOLTIP"));

		reportColumn.setDisplayWidth(rs.getInt("DISPLAY_WIDTH"));
		reportColumn.setReadOnly(rs.getBoolean("IS_READONLY"));
		return reportColumn;
	}

}