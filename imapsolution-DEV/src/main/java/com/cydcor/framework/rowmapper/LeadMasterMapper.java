package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.LeadMaster;

public class LeadMasterMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		LeadMaster leadMaster = new LeadMaster();
		try {
			leadMaster.setLeadId(rs.getString("LEAD_ID"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setGeoPoint(rs.getString("GEO_POINT"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setValidationCode(rs.getString("VALIDATION_CODE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setResponseText(rs.getString("RESPONSE_TEXT"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCampaignSeq(rs.getLong("CAMPAIGN_SEQ"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setOfficeSeq(rs.getLong("OFFICE_SEQ"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setPersonId(rs.getLong("PERSON_ID"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setActive(rs.getString("IS_ACTIVE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCreatedBy(rs.getString("CREATED_BY"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setModifiedDate(rs.getTimestamp("MODIFIED_DATE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setModifiedBy(rs.getString("MODIFIED_BY"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setRunId(rs.getString("RUN_ID"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setDisposition(rs.getString("DISPOSITION"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setDa(rs.getString("DA"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setWireCenter(rs.getString("CLLI"));
		} catch (Exception e) {
		}
		try {
			// For all columns which are not standard and using this mapper
			String leadIcon = rs.getString("MARKER_ICON_IMAGE");
			leadMaster.setImageIcon(leadIcon);
		} catch (Exception e) {
		}
		try {
			leadMaster.setFullAddr(rs.getString("FULL_ADDR"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setCity(rs.getString("CITY"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setState(rs.getString("STATE"));
		} catch (Exception e) {
		}
		try {
			leadMaster.setZip(rs.getString("ZIP"));

		} catch (Exception e) {
		}
		try {
			leadMaster.setAptNum(rs.getString("APT_NUM"));

		} catch (Exception e) {
			// TODO: handle exception
		}

		// LEAD_SHEET_ID
		try {
			leadMaster.setLeadSheetId(rs.getString("LEAD_SHEET_ID"));

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setRowId(rs.getLong("ROW_ID"));

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setDispositionalImage(rs.getString("DISPOSITION_IMAGE"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			leadMaster.setDispositionCode(rs.getString("DISPOSITION_CODE"));
		} catch (Exception e) {
			// TODO: handle exception
		}

		return leadMaster;
	}
}
