package com.cydcor.framework.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cydcor.framework.model.ReportParameter;

/**
 * SELECT * FROM LWB_FRM_REPORT_COLUMNS. This Class Loads ReportColumns
 * 
 * CREATE TABLE `healthcare`.`lwb_frm_report_params` ( `ID` int(10) unsigned NOT
 * NULL AUTO_INCREMENT, `COLUMN_NAME` varchar(45) NOT NULL COMMENT 'SHLD MATCH
 * COLUMN OF TABLE COLUMN', `LABEL` varchar(100) NOT NULL, `PARAM_TYPE`
 * varchar(45) NOT NULL COMMENT
 * 'LOV/PICKLIST/TEXT/TEXTAREA/DATETIME/CHECKBOX/FILEUPLOAD', `MAX_LENGTH`
 * int(10) unsigned NOT NULL, `IS_EDITABLE` tinyint(1) NOT NULL, `DEFAULT_VALUE`
 * varchar(60) DEFAULT NULL, `REF_TABLE_ID` int(10) unsigned DEFAULT NULL
 * COMMENT 'MAPS TO LOV/PICKLIST/FILE TABLE', `REF_KEY_COLUMN` varchar(50)
 * DEFAULT NULL COMMENT 'KEY COLUMN WHERE PARAM ID IS STORED TO MAP',
 * `REF_VALUE_COLUMN` varchar(50) DEFAULT NULL COMMENT 'WHERE ACTUAL VALUE IS
 * STORED', `IS_HIDDEN` varchar(45) DEFAULT NULL COMMENT 'HIDDEN PARAMETERS',
 * `PARAM_DATA_URL` varchar(45) DEFAULT NULL COMMENT 'USED IN AJAX HITS',
 * `PARAM_DEPENDENTS` varchar(200) DEFAULT NULL COMMENT 'ONCHANGE OF VALUE CALLS
 * DEPENDENDS PARAMS TRIGGER FUNCTIONS', `IS_ACTIVE` tinyint(1) NOT NULL,
 * `CREATED_BY` int(10) unsigned NOT NULL, `CREATED_DATE` datetime NOT NULL,
 * `MODIFIED_BY` int(10) unsigned NOT NULL, `MODIFIED_DATE` datetime NOT NULL,
 * `REPORT_NAME` varchar(45) NOT NULL)
 * 
 * @author ashwin
 * 
 */
public class ReportParamRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReportParameter reportParameter = new ReportParameter();

		reportParameter.setId(rs.getLong("ID"));
		reportParameter.setParamName(rs.getString("COLUMN_NAME"));
		reportParameter.setParamLabel(rs.getString("LABEL"));

		reportParameter.setParamType(rs.getString("PARAM_TYPE"));
		reportParameter.setMaxLength(rs.getInt("MAX_LENGTH"));
		reportParameter.setEditable(rs.getBoolean("IS_EDITABLE"));
		reportParameter.setDefaultValue(rs.getString("DEFAULT_VALUE"));
		reportParameter.setRefTableName(rs.getString("REF_TABLE_NAME"));
		reportParameter.setRefKeyColumn(rs.getString("REF_KEY_COLUMN"));
		reportParameter.setRefValueColumn(rs.getString("REF_VALUE_COLUMN"));
		reportParameter.setHidden(rs.getBoolean("IS_HIDDEN"));
		reportParameter.setParamDataURL(rs.getString("PARAM_DATA_URL"));
		reportParameter.setParamDependents(rs.getString("PARAM_DEPENDENTS"));
		reportParameter.setActive(rs.getBoolean("IS_ACTIVE"));
		reportParameter.setReportName(rs.getString("REF_REPORT_NAME"));

		return reportParameter;
	}

}