package com.cydcor.framework.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.RequestContext;
import com.cydcor.framework.context.ResponseContext;
import com.cydcor.framework.model.UserObject;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;
import com.cydcor.framework.wrapper.CydcorResponseWrapper;

/**
 * Servlet Filter implementation class CydcorSecurityFilter
 */
public class CydcorSecurityFilter implements Filter {
    protected final Log logger = LogFactory.getLog(getClass());

    /**
     * Default constructor.
     */
    public CydcorSecurityFilter() {
	// TODO Auto-generated constructor stub
    }

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
	// TODO Auto-generated method stub
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
	    ServletException {

	propogate(request, response, chain);

    }

    /**
     * Add Db Connection and user Info in RequestContext and propogate
     * 
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    private void propogate(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
	    ServletException {
	RequestContext requestContext = null;
	try {
	    UserObject userObject = new UserObject();
	    // Wrap Request so as we can extend it's existing Functionality
	    CydcorRequestWrapper requestWrapper = new CydcorRequestWrapper((HttpServletRequest) request);
	    CydcorResponseWrapper responseWrapper = new CydcorResponseWrapper((HttpServletResponse) response);

	    requestContext = new RequestContext();
	    requestContext.setRequestWrapper(requestWrapper);
	    requestContext.setUser(userObject);
	    ResponseContext responseContext = new ResponseContext();
	    responseContext.setResponseWrapper(responseWrapper);
	    CydcorContext.getInstance().setResponseContext(responseContext);

	    CydcorContext.getInstance().setRequestContext(requestContext);
	    chain.doFilter(request, response);
	} catch (Throwable th) {
	    logger.fatal("Exception in Security Filter:", th);
	    th.printStackTrace();
	} finally {
	    response.flushBuffer();
	    CydcorContext.getInstance().setRequestContext(null);
	}
    }

    /**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
	// TODO Auto-generated method stub
    }

}
