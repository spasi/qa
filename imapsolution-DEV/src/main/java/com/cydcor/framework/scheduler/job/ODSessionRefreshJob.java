package com.cydcor.framework.scheduler.job;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.cydcor.framework.utils.ODSessionPool;

public class ODSessionRefreshJob implements StatefulJob {

	private static final transient Log logger = LogFactory.getLog(ODSessionRefreshJob.class);
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
    	logger.info("Quartz - Starting Scheduler for OD Session Refresh");
        ODSessionPool odSessionPool = ODSessionPool.getInstance();
        logger.info("Quartz - Calling the refreshSessions() method for evicting idle sessions");
        odSessionPool.refreshSessions();
    }
}
