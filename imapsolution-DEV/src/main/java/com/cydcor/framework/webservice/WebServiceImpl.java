package com.cydcor.framework.webservice;

import org.springframework.remoting.jaxrpc.ServletEndpointSupport;

import com.cydcor.framework.service.TestWS;
import com.cydcor.framework.service.impl.TestWSImpl;
import com.cydcor.framework.utils.ServiceLocator;

public class WebServiceImpl extends ServletEndpointSupport implements WebService {

	private TestWS testws;

	protected final void onInit() {
		this.testws = (TestWSImpl)ServiceLocator.getService("dbLogic");
	}

	public String getName(String s) throws java.rmi.RemoteException {
		return s+"---"+testws.getName();
	}
	public void updateName(String name) throws java.rmi.RemoteException {
		testws.updateName(name);
	}

}
