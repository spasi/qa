package com.cydcor.framework.webservice;

public interface WebService extends java.rmi.Remote {

	public String getName(String s) throws java.rmi.RemoteException;

	public void updateName(String s) throws java.rmi.RemoteException;

}