/**
 * 
 */
package com.cydcor.framework.task;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.model.LeadMaster;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * @author ashwin
 * 
 */
public class LeadAssignByTerritoryTask extends AbstractTask {
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.task.AbstractTask#execute(com.cydcor.framework.task
	 * .TaskContext)
	 */
	@Override
	public void execute(TaskContext context) {
		
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator.getService("jdbcTemplate");
		PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator
				.getService("txManager");
		DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);
		
		List<LeadMaster> unassignedLeads = dbUtil.selectUnAssingedLeadsUsingFiles((String) context.getProcessContext()
				.getParameters().get("campaignSeq"), "", "","");
		for (LeadMaster lead : unassignedLeads) {
			if (!StringUtils.isBlank(lead.getGeoPoint())) {
				List officeSeqs = dbUtil.findIclByLeadPoint(lead.getGeoPoint());
				if (officeSeqs != null && officeSeqs.size() == 1) {
					dbUtil.assignLeadToICL(lead.getLeadId(), new Integer(((Map) officeSeqs.get(0)).get("OFFICE_SEQ")
							.toString()));
				}
			}
		}
		
	}
	
}
