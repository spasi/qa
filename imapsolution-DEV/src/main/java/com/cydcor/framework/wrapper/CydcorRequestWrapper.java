/**
 * 
 */
package com.cydcor.framework.wrapper;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.cydcor.framework.utils.CydcorConstants;

/**
 * This Request wrapper extends Implements Map so that it can enable OGNL kind
 * of expression on Request Object
 * 
 * @author ashwin
 * 
 */
public class CydcorRequestWrapper extends HttpServletRequestWrapper
		implements Map<String, Object> {

	public CydcorRequestWrapper(HttpServletRequest request) {
		super(request);
		// TODO Auto-generated constructor stub
	}

	public void clear() {
		// TODO Auto-generated method stub

	}

	public boolean containsKey(Object key) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean containsValue(Object value) {
		// TODO Auto-generated method stub
		return false;
	}

	public Set<java.util.Map.Entry<String, Object>> entrySet() {
		// TODO Auto-generated method stub
		return null;
	}

	/* 
	 * If Request Dosent Have the Object then Return some Standard Value
	 * @see java.util.Map#get(java.lang.Object)
	 */
	public Object get(Object key) {
		Object value = getRequest().getParameter((String) key);
		if(value== null){
			return CydcorConstants.DataConstants.DEFAULT_EVALUATION_STRING;
		}
		return getRequest().getParameter((String) key);
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	public Set<String> keySet() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object put(String key, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	public void putAll(Map<? extends String, ? extends Object> t) {
		// TODO Auto-generated method stub

	}

	public Object remove(Object key) {
		// TODO Auto-generated method stub
		return null;
	}

	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Collection<Object> values() {
		// TODO Auto-generated method stub
		return null;
	}

}
