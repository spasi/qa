/**
 * 
 */
package com.cydcor.framework.encryption;

/**
 * @author ashwin
 *
 */
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RSACryptographer {
	private static final transient Log logger = LogFactory.getLog(RSACryptographer.class);
	public static void main(String[] args) throws Exception {
		Security
				.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		byte[] input = "abc".getBytes();
		Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
		SecureRandom random = new SecureRandom();
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");

		generator.initialize(256, random);

		KeyPair pair = generator.generateKeyPair();
		Key pubKey = pair.getPublic();
		Key privKey = pair.getPrivate();

		cipher.init(Cipher.ENCRYPT_MODE, pubKey, random);
		byte[] cipherText = cipher.doFinal(input);
		logger.debug("cipher: " + new String(cipherText));

		cipher.init(Cipher.DECRYPT_MODE, privKey);
		byte[] plainText = cipher.doFinal(cipherText);
		logger.debug("plain : " + new String(plainText));
	}

	/**
	 * @param data
	 * @return
	 */
	public static String bytesToHex(byte[] data) {
		if (data == null) {
			return null;
		} else {
			int len = data.length;
			String str = "";
			for (int i = 0; i < len; i++) {
				if ((data[i] & 0xFF) < 16)
					str = str + "0"
							+ java.lang.Integer.toHexString(data[i] & 0xFF);
				else
					str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
			}
			return str;
		}
	}

}
