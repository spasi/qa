/**
 * 
 */
package com.cydcor.framework.parser;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import com.cydcor.framework.model.glocation.Kml;
import com.cydcor.framework.test.SpringPage;
import com.cydcor.framework.utils.CydcorUtils;
import com.cydcor.framework.utils.URLSigner;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

/**
 * @author ashwin kumar
 * 
 */
public class GMapJSONParser {

	private static SpringPage page = new SpringPage();

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		HttpClient client = null;
		GetMethod method = null;
		Kml kml = null;
		String googleKey = CydcorUtils.getProperty("GoogleKey");
		String url = "http://maps.google.com/maps/api/geocode/json?address=314,East,Highland,Mall,Blvd,Suite,411&sensor=true&client="
				+ CydcorUtils.getProperty("ClientID");

		client = new HttpClient();
		try {

			// System.out.println(URLSigner.getInstance().getSignedURL(url));
			method = new GetMethod(URLSigner.getInstance().getSignedURL(url));
			Calendar startTime = new GregorianCalendar();
			client.executeMethod(method);
			Calendar endTime = new GregorianCalendar();
/*			System.out.println("TimeTaken :"
					+ (endTime.getTime().getTime() - startTime.getTime()
							.getTime()));*/

			//System.out.println("Checking for XML");

			url = "http://maps.google.com/maps/api/geocode/xml?address=314,East,Highland,Mall,Blvd,Suite,411&sensor=true&client="
					+ CydcorUtils.getProperty("ClientID");
			method = new GetMethod(URLSigner.getInstance().getSignedURL(url));
			startTime = new GregorianCalendar();
			client.executeMethod(method);
			endTime = new GregorianCalendar();
/*			System.out.println("TimeTaken :"
					+ (endTime.getTime().getTime() - startTime.getTime()
							.getTime()));*/

			// String response = method.getResponseBodyAsString();
			// System.out.println(response);
			// kml = (Kml) XStreamUtils.getObjectFromXml(Kml.class, response);
			// kml.setReponseFromGoogle(response);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		// return kml;

		// TODO Auto-generated method stub
		XStream xstream = new XStream(new JettisonMappedXmlDriver());
		// xstream.alias("product", Product.class);
	}

}
