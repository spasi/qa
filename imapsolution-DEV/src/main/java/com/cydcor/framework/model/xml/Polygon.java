package com.cydcor.framework.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Polygon")
public class Polygon {
	private String extrude;
	private String altitudeMode;

	private OuterBoundaryIs outerBoundaryIs;
	private InnerBoundaryIs innerBoundaryIs; 

	/**
	 * @return the extrude
	 */
	public String getExtrude() {
		return extrude;
	}

	/**
	 * @param extrude
	 *            the extrude to set
	 */
	public void setExtrude(String extrude) {
		this.extrude = extrude;
	}

	/**
	 * @return the altitudeMode
	 */
	public String getAltitudeMode() {
		return altitudeMode;
	}

	/**
	 * @param altitudeMode
	 *            the altitudeMode to set
	 */
	public void setAltitudeMode(String altitudeMode) {
		this.altitudeMode = altitudeMode;
	}

	/**
	 * @return the outerBoundaryIs
	 */
	public OuterBoundaryIs getOuterBoundaryIs() {
		return outerBoundaryIs;
	}

	/**
	 * @param outerBoundaryIs
	 *            the outerBoundaryIs to set
	 */
	public void setOuterBoundaryIs(OuterBoundaryIs outerBoundaryIs) {
		this.outerBoundaryIs = outerBoundaryIs;
	}

	/**
	 * @return the innerBoundaryIs
	 */
	public InnerBoundaryIs getInnerBoundaryIs() {
		return innerBoundaryIs;
	}

	/**
	 * @param innerBoundaryIs the innerBoundaryIs to set
	 */
	public void setInnerBoundaryIs(InnerBoundaryIs innerBoundaryIs) {
		this.innerBoundaryIs = innerBoundaryIs;
	}

}
