package com.cydcor.framework.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("Style")
public class Style {
	
	@XStreamAlias("id")
   	@XStreamAsAttribute
	private String id;
	private String name;
	
	private IconStyle iconStyle;
	private PolyStyle polyStyle;

	/**
	 * @return the iconStyle
	 */
	public IconStyle getIconStyle() {
		return iconStyle;
	}

	/**
	 * @param iconStyle
	 *            the iconStyle to set
	 */
	public void setIconStyle(IconStyle iconStyle) {
		this.iconStyle = iconStyle;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the polyStyle
	 */
	public PolyStyle getPolyStyle() {
		return polyStyle;
	}

	/**
	 * @param polyStyle the polyStyle to set
	 */
	public void setPolyStyle(PolyStyle polyStyle) {
		this.polyStyle = polyStyle;
	}

}
