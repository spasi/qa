package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("AdministrativeArea")
public class AdministrativeArea {
	private String AdministrativeAreaName;
	private SubAdministrativeArea SubAdministrativeArea;
	private Locality Locality;
	private Thoroughfare Thoroughfare;
	private DependentLocality DependentLocality;
    private PostalCode PostalCode;
	
	/**
	 * @return the thoroughfare
	 */
	public Thoroughfare getThoroughfare() {
		return Thoroughfare;
	}
	
	/**
	 * @param thoroughfare
	 *            the thoroughfare to set
	 */
	public void setThoroughfare(Thoroughfare thoroughfare) {
		Thoroughfare = thoroughfare;
	}
	
	/**
	 * @return the administrativeAreaName
	 */
	public String getAdministrativeAreaName() {
		return AdministrativeAreaName;
	}
	
	/**
	 * @param administrativeAreaName
	 *            the administrativeAreaName to set
	 */
	public void setAdministrativeAreaName(String administrativeAreaName) {
		this.AdministrativeAreaName = administrativeAreaName;
	}
	
	/**
	 * @return the subAdministrativeArea
	 */
	public SubAdministrativeArea getSubAdministrativeArea() {
		return SubAdministrativeArea;
	}
	
	/**
	 * @param subAdministrativeArea
	 *            the subAdministrativeArea to set
	 */
	public void setSubAdministrativeArea(SubAdministrativeArea subAdministrativeArea) {
		this.SubAdministrativeArea = subAdministrativeArea;
	}
	
	/**
	 * @return the locality
	 */
	public Locality getLocality() {
		return Locality;
	}
	
	/**
	 * @param locality
	 *            the locality to set
	 */
	public void setLocality(Locality locality) {
		this.Locality = locality;
	}
	
	public DependentLocality getDependentLocality() {
		return DependentLocality;
	}
	
	public void setDependentLocality(DependentLocality dependentLocality) {
		DependentLocality = dependentLocality;
	}
	
	@Override
	public String toString() {
		return "AdministrativeArea [AdministrativeAreaName=" + AdministrativeAreaName + ", SubAdministrativeArea="
				+ SubAdministrativeArea + ",Locality=" + Locality + "]";
	}

    /**
     * @return the postalCode
     */
    public PostalCode getPostalCode() {
	return PostalCode;
    }

    /**
     * @param postalCode
     *            the postalCode to set
     */
    public void setPostalCode(PostalCode postalCode) {
	PostalCode = postalCode;
    }
	
}
