package com.cydcor.framework.model;

import java.sql.Timestamp;

public class MerICLRepDetails extends GenericModel {
	private String person_id;
	private Timestamp dob;
	private String active;
	private String status_seq;
	private String first_name;
	private String last_name;
	private String middle_initial;
	private String ss_number;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String phone1;
	private String mobile_phone;
	private String country;
	private String province;
	private String gender;
	private String pay_rate;
	private String hire_status;
	private Timestamp hire_date;
	private String email_address;
	private String fed_marital_status;
	private String fed_exemptions;
	private Timestamp termination_date;
	private Timestamp creation_date;
	private Timestamp last_update_date;
	private String created_by_id;
	private String last_updated_by_id;
	private String locked;
	private Timestamp state_code_of_conduct_signed;
	private Timestamp att_code_of_conduct_signed;
	private Timestamp corporate_training_date;
	private String unlock_request;
	private String termination_reason;
	private String closure_reason_seq;
	private String process_status;
	private Timestamp process_date;
	private String error_message;
	private long retry_count;

	public String getPerson_id() {
		return person_id;
	}

	public void setPerson_id(String person_id) {
		this.person_id = person_id;
	}

	public Timestamp getDob() {
		return dob;
	}

	public void setDob(Timestamp dob) {
		this.dob = dob;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus_seq() {
		return status_seq;
	}

	public void setStatus_seq(String status_seq) {
		this.status_seq = status_seq;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getMiddle_initial() {
		return middle_initial;
	}

	public void setMiddle_initial(String middle_initial) {
		this.middle_initial = middle_initial;
	}

	public String getSs_number() {
		return ss_number;
	}

	public void setSs_number(String ss_number) {
		this.ss_number = ss_number;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getMobile_phone() {
		return mobile_phone;
	}

	public void setMobile_phone(String mobile_phone) {
		this.mobile_phone = mobile_phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPay_rate() {
		return pay_rate;
	}

	public void setPay_rate(String pay_rate) {
		this.pay_rate = pay_rate;
	}

	public String getHire_status() {
		return hire_status;
	}

	public void setHire_status(String hire_status) {
		this.hire_status = hire_status;
	}

	public Timestamp getHire_date() {
		return hire_date;
	}

	public void setHire_date(Timestamp hire_date) {
		this.hire_date = hire_date;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getFed_marital_status() {
		return fed_marital_status;
	}

	public void setFed_marital_status(String fed_marital_status) {
		this.fed_marital_status = fed_marital_status;
	}

	public String getFed_exemptions() {
		return fed_exemptions;
	}

	public void setFed_exemptions(String fed_exemptions) {
		this.fed_exemptions = fed_exemptions;
	}

	public Timestamp getTermination_date() {
		return termination_date;
	}

	public void setTermination_date(Timestamp termination_date) {
		this.termination_date = termination_date;
	}

	public Timestamp getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Timestamp creation_date) {
		this.creation_date = creation_date;
	}

	public Timestamp getLast_update_date() {
		return last_update_date;
	}

	public void setLast_update_date(Timestamp last_update_date) {
		this.last_update_date = last_update_date;
	}

	public String getCreated_by_id() {
		return created_by_id;
	}

	public void setCreated_by_id(String created_by_id) {
		this.created_by_id = created_by_id;
	}

	public String getLast_updated_by_id() {
		return last_updated_by_id;
	}

	public void setLast_updated_by_id(String last_updated_by_id) {
		this.last_updated_by_id = last_updated_by_id;
	}

	public String getLocked() {
		return locked;
	}

	public void setLocked(String locked) {
		this.locked = locked;
	}

	public Timestamp getState_code_of_conduct_signed() {
		return state_code_of_conduct_signed;
	}

	public void setState_code_of_conduct_signed(
			Timestamp state_code_of_conduct_signed) {
		this.state_code_of_conduct_signed = state_code_of_conduct_signed;
	}

	public Timestamp getAtt_code_of_conduct_signed() {
		return att_code_of_conduct_signed;
	}

	public void setAtt_code_of_conduct_signed(
			Timestamp att_code_of_conduct_signed) {
		this.att_code_of_conduct_signed = att_code_of_conduct_signed;
	}

	public Timestamp getCorporate_training_date() {
		return corporate_training_date;
	}

	public void setCorporate_training_date(Timestamp corporate_training_date) {
		this.corporate_training_date = corporate_training_date;
	}

	public String getUnlock_request() {
		return unlock_request;
	}

	public void setUnlock_request(String unlock_request) {
		this.unlock_request = unlock_request;
	}

	public String getTermination_reason() {
		return termination_reason;
	}

	public void setTermination_reason(String termination_reason) {
		this.termination_reason = termination_reason;
	}

	public String getClosure_reason_seq() {
		return closure_reason_seq;
	}

	public void setClosure_reason_seq(String closure_reason_seq) {
		this.closure_reason_seq = closure_reason_seq;
	}

	public String getProcess_status() {
		return process_status;
	}

	public void setProcess_status(String process_status) {
		this.process_status = process_status;
	}

	public Timestamp getProcess_date() {
		return process_date;
	}

	public void setProcess_date(Timestamp process_date) {
		this.process_date = process_date;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}

	public long getRetry_count() {
		return retry_count;
	}

	public void setRetry_count(long retry_count) {
		this.retry_count = retry_count;
	}
}