package com.cydcor.framework.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RoleObject implements Serializable {

    String roleName;
    List<String> profileNames = new ArrayList<String>();
    String ownerUserSignInId;
    String ownerFirstName;
    String ownerLastName;
    String ownerFullName;
    String ownerEmailAddress;
    String ownerIntegrationId;
    String ownerAlias;
    String created;
    String modified;

    List<MerICLDetails> merlinICL;
    List<MerCampignDetails> merlinCampaign;
    List urls;

    public String getOwnerUserSignInId() {
	return ownerUserSignInId;
    }

    public void setOwnerUserSignInId(String ownerUserSignInId) {
	this.ownerUserSignInId = ownerUserSignInId;
    }

    public String getRoleName() {
	return roleName;
    }

    public void setRoleName(String roleName) {
	this.roleName = roleName;
    }

    public List<MerICLDetails> getMerlinICL() {
	return merlinICL;
    }

    public void setMerlinICL(List<MerICLDetails> merlinICL) {
	this.merlinICL = merlinICL;
    }

    public List<MerCampignDetails> getMerlinCampaign() {
	return merlinCampaign;
    }

    public void setMerlinCampaign(List<MerCampignDetails> merlinCampaign) {
	this.merlinCampaign = merlinCampaign;
    }

    public List getUrls() {
	return urls;
    }

    public void setUrls(List urls) {
	this.urls = urls;
    }

    public String getOwnerFirstName() {
	return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
	this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerLastName() {
	return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
	this.ownerLastName = ownerLastName;
    }

    public String getOwnerFullName() {
	return ownerFullName;
    }

    public void setOwnerFullName(String ownerFullName) {
	this.ownerFullName = ownerFullName;
    }

    public String getOwnerEmailAddress() {
	return ownerEmailAddress;
    }

    public void setOwnerEmailAddress(String ownerEmailAddress) {
	this.ownerEmailAddress = ownerEmailAddress;
    }

    public String getOwnerIntegrationId() {
	return ownerIntegrationId;
    }

    public void setOwnerIntegrationId(String ownerIntegrationId) {
	this.ownerIntegrationId = ownerIntegrationId;
    }

    public String getOwnerAlias() {
	return ownerAlias;
    }

    public void setOwnerAlias(String ownerAlias) {
	this.ownerAlias = ownerAlias;
    }

    @Override
    public int hashCode() {
	// TODO Auto-generated method stub
	return ownerUserSignInId.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
	// TODO Auto-generated method stub
	if (obj instanceof RoleObject) {
	    RoleObject roleObj = (RoleObject) obj;
	    return (ownerUserSignInId == roleObj.ownerUserSignInId);
	}
	return false;

    }

    public List<String> getProfileNames() {
	return profileNames;
    }

    public void addProfileNames(String profileName) {
	this.profileNames.add(profileName);
    }

    public String getCreated() {
	return created;
    }

    public void setCreated(String created) {
	this.created = created;
    }

    public String getModified() {
	return modified;
    }

    public void setModified(String modified) {
	this.modified = modified;
    }

    public String getOfficesString() {
	String returnStr = "";
	for (MerICLDetails details : merlinICL) {
	    if (null != details.getOffice_seq() && !"".equals(details.getOffice_seq()))
		returnStr += "," + details.getOffice_seq();
	}
	return returnStr.length() > 1 ? returnStr.substring(1) : null;
    }

    public String getCampaignsString() {
	String returnStr = "";
	for (MerCampignDetails details : merlinCampaign) {
	    if (null != details.getCampaign_seq() && !"".equals(details.getCampaign_seq()))
		returnStr += "," + details.getCampaign_seq();
	}
	return returnStr.length() > 1 ? returnStr.substring(1) : null;
    }

    public String getProfilesString() {
	String returnStr = "";
	for (String profile : profileNames) {
	    if (null != profile && !"".equals(profile))
		returnStr += "," + profile;
	}
	return returnStr.length() > 1 ? returnStr.substring(1) : null;
    }
}
