package com.cydcor.framework.model;

import java.sql.Timestamp;

public class ImsLead extends GenericModel {
	private String lead_id;
	private long campaign_seq;
	private long office_seq;
	private String icl_rep_seq;
	private String region;
	private String clli;
	private String da;
	private String is_active;
	private String cust_name;
	private String house_num;
	private String str_pre;
	private String str_name;
	private String str_type;
	private String str_post;
	private String apt_num;
	private String city;
	private String state;
	private String zip;
	private String disposition_code;
	private String disposition;
	private String notes;
	private Timestamp lead_dob;
	private Timestamp lead_assigned;
	private Timestamp lead_expires;
	private Timestamp modified_date;
	private String process_status;
	private Timestamp process_date;
	private String error_message;
	private long retry_count;

	/**
	 * @return the lead_id
	 */
	public String getLead_id() {
		return lead_id;
	}

	/**
	 * @param leadId
	 *            the lead_id to set
	 */
	public void setLead_id(String leadId) {
		lead_id = leadId;
	}

	/**
	 * @return the campaign_seq
	 */
	public long getCampaign_seq() {
		return campaign_seq;
	}

	/**
	 * @param campaignSeq
	 *            the campaign_seq to set
	 */
	public void setCampaign_seq(long campaignSeq) {
		campaign_seq = campaignSeq;
	}

	/**
	 * @return the office_seq
	 */
	public long getOffice_seq() {
		return office_seq;
	}

	/**
	 * @param officeSeq
	 *            the office_seq to set
	 */
	public void setOffice_seq(long officeSeq) {
		office_seq = officeSeq;
	}

	/**
	 * @return the icl_rep_seq
	 */
	public String getIcl_rep_seq() {
		return icl_rep_seq;
	}

	/**
	 * @param iclRepSeq
	 *            the icl_rep_seq to set
	 */
	public void setIcl_rep_seq(String iclRepSeq) {
		icl_rep_seq = iclRepSeq;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the clli
	 */
	public String getClli() {
		return clli;
	}

	/**
	 * @param clli
	 *            the clli to set
	 */
	public void setClli(String clli) {
		this.clli = clli;
	}

	/**
	 * @return the da
	 */
	public String getDa() {
		return da;
	}

	/**
	 * @param da
	 *            the da to set
	 */
	public void setDa(String da) {
		this.da = da;
	}

	/**
	 * @return the is_active
	 */
	public String getIs_active() {
		return is_active;
	}

	/**
	 * @param isActive
	 *            the is_active to set
	 */
	public void setIs_active(String isActive) {
		is_active = isActive;
	}

	/**
	 * @return the cust_name
	 */
	public String getCust_name() {
		return cust_name;
	}

	/**
	 * @param custName
	 *            the cust_name to set
	 */
	public void setCust_name(String custName) {
		cust_name = custName;
	}

	/**
	 * @return the house_num
	 */
	public String getHouse_num() {
		return house_num;
	}

	/**
	 * @param houseNum
	 *            the house_num to set
	 */
	public void setHouse_num(String houseNum) {
		house_num = houseNum;
	}

	/**
	 * @return the str_pre
	 */
	public String getStr_pre() {
		return str_pre;
	}

	/**
	 * @param strPre
	 *            the str_pre to set
	 */
	public void setStr_pre(String strPre) {
		str_pre = strPre;
	}

	/**
	 * @return the str_name
	 */
	public String getStr_name() {
		return str_name;
	}

	/**
	 * @param strName
	 *            the str_name to set
	 */
	public void setStr_name(String strName) {
		str_name = strName;
	}

	/**
	 * @return the str_type
	 */
	public String getStr_type() {
		return str_type;
	}

	/**
	 * @param strType
	 *            the str_type to set
	 */
	public void setStr_type(String strType) {
		str_type = strType;
	}

	/**
	 * @return the str_post
	 */
	public String getStr_post() {
		return str_post;
	}

	/**
	 * @param strPost
	 *            the str_post to set
	 */
	public void setStr_post(String strPost) {
		str_post = strPost;
	}

	/**
	 * @return the apt_num
	 */
	public String getApt_num() {
		return apt_num;
	}

	/**
	 * @param aptNum
	 *            the apt_num to set
	 */
	public void setApt_num(String aptNum) {
		apt_num = aptNum;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the disposition_code
	 */
	public String getDisposition_code() {
		return disposition_code;
	}

	/**
	 * @param dispositionCode
	 *            the disposition_code to set
	 */
	public void setDisposition_code(String dispositionCode) {
		disposition_code = dispositionCode;
	}

	/**
	 * @return the disposition
	 */
	public String getDisposition() {
		return disposition;
	}

	/**
	 * @param disposition
	 *            the disposition to set
	 */
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the lead_dob
	 */
	public Timestamp getLead_dob() {
		return lead_dob;
	}

	/**
	 * @param leadDob
	 *            the lead_dob to set
	 */
	public void setLead_dob(Timestamp leadDob) {
		lead_dob = leadDob;
	}

	/**
	 * @return the lead_assigned
	 */
	public Timestamp getLead_assigned() {
		return lead_assigned;
	}

	/**
	 * @param leadAssigned
	 *            the lead_assigned to set
	 */
	public void setLead_assigned(Timestamp leadAssigned) {
		lead_assigned = leadAssigned;
	}

	/**
	 * @return the lead_expires
	 */
	public Timestamp getLead_expires() {
		return lead_expires;
	}

	/**
	 * @param leadExpires
	 *            the lead_expires to set
	 */
	public void setLead_expires(Timestamp leadExpires) {
		lead_expires = leadExpires;
	}

	/**
	 * @return the modified_date
	 */
	public Timestamp getModified_date() {
		return modified_date;
	}

	/**
	 * @param modifiedDate
	 *            the modified_date to set
	 */
	public void setModified_date(Timestamp modifiedDate) {
		modified_date = modifiedDate;
	}

	/**
	 * @return the process_status
	 */
	public String getProcess_status() {
		return process_status;
	}

	/**
	 * @param processStatus
	 *            the process_status to set
	 */
	public void setProcess_status(String processStatus) {
		process_status = processStatus;
	}

	/**
	 * @return the process_date
	 */
	public Timestamp getProcess_date() {
		return process_date;
	}

	/**
	 * @param processDate
	 *            the process_date to set
	 */
	public void setProcess_date(Timestamp processDate) {
		process_date = processDate;
	}

	/**
	 * @return the error_message
	 */
	public String getError_message() {
		return error_message;
	}

	/**
	 * @param errorMessage
	 *            the error_message to set
	 */
	public void setError_message(String errorMessage) {
		error_message = errorMessage;
	}

	/**
	 * @return the retry_count
	 */
	public long getRetry_count() {
		return retry_count;
	}

	/**
	 * @param retryCount
	 *            the retry_count to set
	 */
	public void setRetry_count(long retryCount) {
		retry_count = retryCount;
	}
}