package com.cydcor.framework.model.xml.v3.directions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("DirectionsResponse")
public class DirectionsResponse {

    private String status;
    private Route route;

    /**
     * @return the status
     */
    public String getStatus() {
	return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
	this.status = status;
    }

    /**
     * @return the route
     */
    public Route getRoute() {
	return route;
    }

    /**
     * @param route
     *            the route to set
     */
    public void setRoute(Route route) {
	this.route = route;
    }

}
