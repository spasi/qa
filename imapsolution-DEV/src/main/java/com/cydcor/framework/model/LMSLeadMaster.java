package com.cydcor.framework.model;

import java.sql.Timestamp;

public class LMSLeadMaster extends GenericModel {
    private Long rowid;
    private Long campaign_seq;
    private String region;
    private String market;
    private String leadid;
    private String clli;
    private String da;
    private String lead_type;
    private String lead_status;
    private String cust_name;
    private String full_addr;
    private String house_num;
    private String str_pre;
    private String str_name;
    private String str_type;
    private String str_post;
    private String apt_num;
    private String city;
    private String state;
    private String zip;
    private String zip4;
    private String green_status;
    private String credit_class;
    private String conn_tech;
    private String apt_ind;
    private String uverse_ind;
    private String dsl_ind;
    private String dish_ind;
    private String disposition_code;
    private Timestamp lead_dob;
    private Timestamp lead_assigned;
    private Timestamp lead_expires;
    private String disposition;
    private String notes;
    private String lead_status_notes;
    private String upload_filename;
    private String update_status;
    private Timestamp last_updated;
    private String updated_by;
    private String lead_owner;
    private Timestamp last_worked;
    public Long getRowid() {
        return rowid;
    }
    public void setRowid(Long rowid) {
        this.rowid = rowid;
    }
    public Long getCampaign_seq() {
        return campaign_seq;
    }
    public void setCampaign_seq(Long campaignSeq) {
        campaign_seq = campaignSeq;
    }
    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }
    public String getMarket() {
        return market;
    }
    public void setMarket(String market) {
        this.market = market;
    }
    public String getLeadid() {
        return leadid;
    }
    public void setLeadid(String leadid) {
        this.leadid = leadid;
    }
    public String getClli() {
        return clli;
    }
    public void setClli(String clli) {
        this.clli = clli;
    }
    public String getDa() {
        return da;
    }
    public void setDa(String da) {
        this.da = da;
    }
    public String getLead_type() {
        return lead_type;
    }
    public void setLead_type(String leadType) {
        lead_type = leadType;
    }
    public String getLead_status() {
        return lead_status;
    }
    public void setLead_status(String leadStatus) {
        lead_status = leadStatus;
    }
    public String getCust_name() {
        return cust_name;
    }
    public void setCust_name(String custName) {
        cust_name = custName;
    }
    public String getFull_addr() {
        return full_addr;
    }
    public void setFull_addr(String fullAddr) {
        full_addr = fullAddr;
    }
    public String getHouse_num() {
        return house_num;
    }
    public void setHouse_num(String houseNum) {
        house_num = houseNum;
    }
    public String getStr_pre() {
        return str_pre;
    }
    public void setStr_pre(String strPre) {
        str_pre = strPre;
    }
    public String getStr_name() {
        return str_name;
    }
    public void setStr_name(String strName) {
        str_name = strName;
    }
    public String getStr_type() {
        return str_type;
    }
    public void setStr_type(String strType) {
        str_type = strType;
    }
    public String getStr_post() {
        return str_post;
    }
    public void setStr_post(String strPost) {
        str_post = strPost;
    }
    public String getApt_num() {
        return apt_num;
    }
    public void setApt_num(String aptNum) {
        apt_num = aptNum;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getZip() {
        return zip;
    }
    public void setZip(String zip) {
        this.zip = zip;
    }
    public String getZip4() {
        return zip4;
    }
    public void setZip4(String zip4) {
        this.zip4 = zip4;
    }
    public String getGreen_status() {
        return green_status;
    }
    public void setGreen_status(String greenStatus) {
        green_status = greenStatus;
    }
    public String getCredit_class() {
        return credit_class;
    }
    public void setCredit_class(String creditClass) {
        credit_class = creditClass;
    }
    public String getConn_tech() {
        return conn_tech;
    }
    public void setConn_tech(String connTech) {
        conn_tech = connTech;
    }
    public String getApt_ind() {
        return apt_ind;
    }
    public void setApt_ind(String aptInd) {
        apt_ind = aptInd;
    }
    public String getUverse_ind() {
        return uverse_ind;
    }
    public void setUverse_ind(String uverseInd) {
        uverse_ind = uverseInd;
    }
    public String getDsl_ind() {
        return dsl_ind;
    }
    public void setDsl_ind(String dslInd) {
        dsl_ind = dslInd;
    }
    public String getDish_ind() {
        return dish_ind;
    }
    public void setDish_ind(String dishInd) {
        dish_ind = dishInd;
    }
    public String getDisposition_code() {
        return disposition_code;
    }
    public void setDisposition_code(String dispositionCode) {
        disposition_code = dispositionCode;
    }
    public Timestamp getLead_dob() {
        return lead_dob;
    }
    public void setLead_dob(Timestamp leadDob) {
        lead_dob = leadDob;
    }
    public Timestamp getLead_assigned() {
        return lead_assigned;
    }
    public void setLead_assigned(Timestamp leadAssigned) {
        lead_assigned = leadAssigned;
    }
    public Timestamp getLead_expires() {
        return lead_expires;
    }
    public void setLead_expires(Timestamp leadExpires) {
        lead_expires = leadExpires;
    }
    public String getDisposition() {
        return disposition;
    }
    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getLead_status_notes() {
        return lead_status_notes;
    }
    public void setLead_status_notes(String leadStatusNotes) {
        lead_status_notes = leadStatusNotes;
    }
    public String getUpload_filename() {
        return upload_filename;
    }
    public void setUpload_filename(String uploadFilename) {
        upload_filename = uploadFilename;
    }
    public String getUpdate_status() {
        return update_status;
    }
    public void setUpdate_status(String updateStatus) {
        update_status = updateStatus;
    }
    public Timestamp getLast_updated() {
        return last_updated;
    }
    public void setLast_updated(Timestamp lastUpdated) {
        last_updated = lastUpdated;
    }
    public String getUpdated_by() {
        return updated_by;
    }
    public void setUpdated_by(String updatedBy) {
        updated_by = updatedBy;
    }
    public String getLead_owner() {
        return lead_owner;
    }
    public void setLead_owner(String leadOwner) {
        lead_owner = leadOwner;
    }
    public Timestamp getLast_worked() {
        return last_worked;
    }
    public void setLast_worked(Timestamp lastWorked) {
        last_worked = lastWorked;
    }
    
    
}