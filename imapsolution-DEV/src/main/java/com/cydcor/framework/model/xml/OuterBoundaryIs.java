package com.cydcor.framework.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("outerBoundaryIs")
public class OuterBoundaryIs {
	private LinearRing linearRing;

	/**
	 * @return the linearRing
	 */
	public LinearRing getLinearRing() {
		return linearRing;
	}

	/**
	 * @param linearRing
	 *            the linearRing to set
	 */
	public void setLinearRing(LinearRing linearRing) {
		this.linearRing = linearRing;
	}
}
