package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Locality")
public class Locality {
	private String LocalityName;
	private Thoroughfare Thoroughfare;
	private PostalCode PostalCode;
	private String AddressLine;
	private DependentLocality DependentLocality;

	/**
	 * @return the localityName
	 */
	public String getLocalityName() {
		return LocalityName;
	}

	/**
	 * @param localityName
	 *            the localityName to set
	 */
	public void setLocalityName(String localityName) {
		this.LocalityName = localityName;
	}

	/**
	 * @return the thoroughfare
	 */
	public Thoroughfare getThoroughfare() {
		return Thoroughfare;
	}

	/**
	 * @param thoroughfare
	 *            the thoroughfare to set
	 */
	public void setThoroughfare(Thoroughfare thoroughfare) {
		Thoroughfare = thoroughfare;
	}

	/**
	 * @return the postalCode
	 */
	public PostalCode getPostalCode() {
		return PostalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(PostalCode postalCode) {
		PostalCode = postalCode;
	}

	/**
	 * @return the addressLine
	 */
	public String getAddressLine() {
		return AddressLine;
	}

	/**
	 * @param addressLine
	 *            the addressLine to set
	 */
	public void setAddressLine(String addressLine) {
		AddressLine = addressLine;
	}

	public DependentLocality getDependentLocality() {
		return DependentLocality;
	}

	public void setDependentLocality(DependentLocality DependentLocality) {
		this.DependentLocality = DependentLocality;
	}

	@Override
	public String toString() {
		return "Locality [LocalityName=" + LocalityName + ",Thoroughfare="
				+ Thoroughfare + ",PostalCode=" + PostalCode + ",PostalCode="
				+ PostalCode + "]";
	}

}
