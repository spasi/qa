package com.cydcor.framework.model.xml.v3.directions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("route")
public class Route {

    private String summary;
    private String copyrights;
    private OverviewPolyline overview_polyline;
    private Bounds bounds;
    private Leg leg;

    /**
     * @return the summary
     */
    public String getSummary() {
	return summary;
    }

    /**
     * @param summary
     *            the summary to set
     */
    public void setSummary(String summary) {
	this.summary = summary;
    }

    /**
     * @return the copyrights
     */
    public String getCopyrights() {
	return copyrights;
    }

    /**
     * @param copyrights
     *            the copyrights to set
     */
    public void setCopyrights(String copyrights) {
	this.copyrights = copyrights;
    }

    /**
     * @return the overview_polyline
     */
    public OverviewPolyline getOverview_polyline() {
	return overview_polyline;
    }

    /**
     * @param overview_polyline
     *            the overview_polyline to set
     */
    public void setOverview_polyline(OverviewPolyline overview_polyline) {
	this.overview_polyline = overview_polyline;
    }

    /**
     * @return the bounds
     */
    public Bounds getBounds() {
	return bounds;
    }

    /**
     * @param bounds
     *            the bounds to set
     */
    public void setBounds(Bounds bounds) {
	this.bounds = bounds;
    }

    /**
     * @return the leg
     */
    public Leg getLeg() {
	return leg;
    }

    /**
     * @param leg
     *            the leg to set
     */
    public void setLeg(Leg leg) {
	this.leg = leg;
    }

}
