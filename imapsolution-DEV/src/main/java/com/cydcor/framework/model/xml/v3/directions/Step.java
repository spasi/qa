package com.cydcor.framework.model.xml.v3.directions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("step")
public class Step {

    private String html_instructions;
    private String travel_mode;
    private StartLocation start_location;
    private EndLocation end_location;
    private Polyline polyline;
    private Duration duration;
    private Distance distance;

    /**
     * @return the html_instructions
     */
    public String getHtml_instructions() {
	return html_instructions;
    }

    /**
     * @param html_instructions
     *            the html_instructions to set
     */
    public void setHtml_instructions(String html_instructions) {
	this.html_instructions = html_instructions;
    }

    /**
     * @return the travel_mode
     */
    public String getTravel_mode() {
	return travel_mode;
    }

    /**
     * @param travel_mode
     *            the travel_mode to set
     */
    public void setTravel_mode(String travel_mode) {
	this.travel_mode = travel_mode;
    }

    /**
     * @return the start_location
     */
    public StartLocation getStart_location() {
	return start_location;
    }

    /**
     * @param start_location
     *            the start_location to set
     */
    public void setStart_location(StartLocation start_location) {
	this.start_location = start_location;
    }

    /**
     * @return the end_location
     */
    public EndLocation getEnd_location() {
	return end_location;
    }

    /**
     * @param end_location
     *            the end_location to set
     */
    public void setEnd_location(EndLocation end_location) {
	this.end_location = end_location;
    }

    /**
     * @return the polyline
     */
    public Polyline getPolyline() {
	return polyline;
    }

    /**
     * @param polyline
     *            the polyline to set
     */
    public void setPolyline(Polyline polyline) {
	this.polyline = polyline;
    }

    /**
     * @return the duration
     */
    public Duration getDuration() {
	return duration;
    }

    /**
     * @param duration
     *            the duration to set
     */
    public void setDuration(Duration duration) {
	this.duration = duration;
    }

    /**
     * @return the distance
     */
    public Distance getDistance() {
	return distance;
    }

    /**
     * @param distance
     *            the distance to set
     */
    public void setDistance(Distance distance) {
	this.distance = distance;
    }

}
