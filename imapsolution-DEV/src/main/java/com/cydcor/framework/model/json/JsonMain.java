package com.cydcor.framework.model.json;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JsonMain {
	private static final transient Log logger = LogFactory
			.getLog(JsonMain.class);

	public static void main(String[] args) {
		GmapJsonObject jsonObject = new GmapJsonObject();
		jsonObject.setStatus("success");
		jsonObject.setOperation("get");

		Result result = new Result();
		jsonObject.setResult(result);

		Geometries geometries = new Geometries();
		Type type = new Type();
		type.setTitle("Hi");
		type.setDescription("sup");
		type.setType("point");
		type.getCoordinates().add(new Coordinates("37.6881", " -122.4934"));
		Style style = new Style();
		Icon icon = new Icon();
		icon.setX("1");
		icon.setY("1");
		style.setIcon(icon);
		type.setStyle(style);
		geometries.getRecords().add(type);

		type = new Type();
		type.setTitle("cool");
		type.setDescription("I come from json");
		type.setType("point");
		type.getCoordinates().add(new Coordinates("37.8981", " -122.6034"));
		style = new Style();
		icon = new Icon();
		icon.setX("2");
		icon.setY("2");
		style.setIcon(icon);
		type.setStyle(style);
		geometries.getRecords().add(type);

		type = new Type();
		type.setTitle("nice");
		type.setDescription("json made me");
		type.setType("poly");
		type.getCoordinates().add(new Coordinates("37.8981", " -122.6034"));
		type.getCoordinates().add(new Coordinates("37.8981", " -122.0034"));
		type.getCoordinates().add(new Coordinates("37.4981", " -122.3034"));
		type.getCoordinates().add(new Coordinates("37.8981", " -122.6034"));
		style = new Style();
		style.setStrokeColor("#000000");
		style.setStrokeWeight(3);
		style.setStrokeOpacity(0.25);
		style.setFillColor("#ff0000");
		style.setFillOpacity(0.45);
		Opts opts = new Opts();
		opts.setClickable(true);
		style.setOpts(opts);
		type.setStyle(style);
		geometries.getRecords().add(type);

		type = new Type();
		type.setTitle("hello, i'm a line");
		type.setDescription("json made me");
		type.setType("line");
		type.getCoordinates().add(new Coordinates("37.991", " -122.0034"));
		type.getCoordinates().add(new Coordinates("37.991", " -121.1034"));
		type.getCoordinates().add(new Coordinates("37.3981", " -121.1034"));

		style = new Style();
		style.setStrokeColor("#0000FF");
		style.setStrokeWeight(3);
		style.setStrokeOpacity(0.45);
		opts = new Opts();
		opts.setClickable(true);
		style.setOpts(opts);
		type.setStyle(style);
		geometries.getRecords().add(type);

		result.setGeometries(geometries);

		JsonConfig config = new JsonConfig();

		logger.debug(JSONSerializer.toJSON(jsonObject));

	}

}
