package com.cydcor.framework.model.xml.v3.directions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("distance")
public class Distance {

    private String value;
    private String text;

    /**
     * @return the value
     */
    public String getValue() {
	return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(String value) {
	this.value = value;
    }

    /**
     * @return the text
     */
    public String getText() {
	return text;
    }

    /**
     * @param text
     *            the text to set
     */
    public void setText(String text) {
	this.text = text;
    }

}
