package com.cydcor.framework.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Icon")
public class Icon {
	private String href;

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href the href to set
	 */
	public void setHref(String href) {
		this.href = href;
	}
}
