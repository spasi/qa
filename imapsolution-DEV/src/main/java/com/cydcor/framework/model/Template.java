/**
 * 
 */
package com.cydcor.framework.model;

/**
 * @author ashwin
 * 
 */
public class Template extends BaseDataObject {
	private String templateName = "";
	private String templateType = "";
	private String templateContent = "";
	
	
	public static interface Types{
		public static String LEAD_SHEET = "LEAD_SHEET";
		public static String ICL = "ICL";
		public static String CAMPAIGN = "CAMPAIGN";
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName
	 *            the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return the templateType
	 */
	public String getTemplateType() {
		return templateType;
	}

	/**
	 * @param templateType
	 *            the templateType to set
	 */
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	/**
	 * @return the templateContent
	 */
	public String getTemplateContent() {
		return templateContent;
	}

	/**
	 * @param templateContent
	 *            the templateContent to set
	 */
	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

}
