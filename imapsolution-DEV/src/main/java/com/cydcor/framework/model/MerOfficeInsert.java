package com.cydcor.framework.model;

import java.sql.Timestamp;

public class MerOfficeInsert {
	private int officeSeq;
	private String geoLocation;
	private String isActive;
	private Timestamp createdDate;
	private int createdBy;
	private Timestamp modifiedDate;
	private int modifiedBy;
	private String fullAddr;

	/**
	 * @return the officeSeq
	 */
	public int getOfficeSeq() {
		return officeSeq;
	}

	/**
	 * @param officeSeq
	 *            the officeSeq to set
	 */
	public void setOfficeSeq(int officeSeq) {
		this.officeSeq = officeSeq;
	}

	/**
	 * @return the geoLocation
	 */
	public String getGeoLocation() {
		return geoLocation;
	}

	/**
	 * @param geoLocation
	 *            the geoLocation to set
	 */
	public void setGeoLocation(String geoLocation) {
		this.geoLocation = geoLocation;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdDate
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the createdBy
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the modifiedDate
	 */
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            the modifiedDate to set
	 */
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the modifiedBy
	 */
	public int getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(int modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getFullAddr() {
	    return fullAddr;
	}

	public void setFullAddr(String fullAddr) {
	    this.fullAddr = fullAddr;
	}

}
