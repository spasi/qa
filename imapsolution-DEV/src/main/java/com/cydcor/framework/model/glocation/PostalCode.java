package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PostalCode")
public class PostalCode {
	private String PostalCodeNumber;

	/**
	 * @return the postalCodeNumber
	 */
	public String getPostalCodeNumber() {
		return PostalCodeNumber;
	}

	/**
	 * @param postalCodeNumber
	 *            the postalCodeNumber to set
	 */
	public void setPostalCodeNumber(String postalCodeNumber) {
		this.PostalCodeNumber = postalCodeNumber;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostalCode[PostalCodeNumber="+PostalCodeNumber+"]";
	}
	
	
}
