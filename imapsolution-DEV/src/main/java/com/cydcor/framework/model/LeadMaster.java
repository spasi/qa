package com.cydcor.framework.model;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class LeadMaster {
	private String leadId;
	private String fullAddr = "";
	private String zip = "";
	private String city = "";
	private String state = "";
	private String county = "";
	private String disposition = "";
	private String dispositionCode = "";
	private String aptNum = "";

	private String geoPoint;
	private String validationCode;
	private String responseText;
	private Long campaignSeq;
	private Long officeSeq;
	private Long personId;
	private String active;
	private Date createdDate;
	private String createdBy;
	private Date modifiedDate;
	private String modifiedBy;
	private String da;
	private String wireCenter;
	private String leadSheetId;
	private String address;
	private String custName;
	private String imageIcon = null;
	private String runId;
	private Long rowId;
	private String dispositionalImage;
	

	public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	public String getGeoPoint() {
		return geoPoint;
	}

	public String getGeoPointForDB() {
		return "POINT(" + geoPoint.replaceAll(",", " ") + ")";
	}

	private String geoPointForKML = "";

	public String getGeoPointForKML() {
		if (StringUtils.isNotBlank(geoPoint))
			this.geoPointForKML = geoPoint.replaceAll("\\(|\\)|POINT ", "")
					.replaceAll(", ", "\n").replaceAll(" ", ",");
		return geoPointForKML;
	}

	/**
	 * @return the aptNum
	 */
	public String getAptNum() {
		return aptNum;
	}

	/**
	 * @param aptNum
	 *            the aptNum to set
	 */
	public void setAptNum(String aptNum) {
		this.aptNum = aptNum;
	}

	public void setGeoPoint(String geoPoint) {
		this.geoPoint = geoPoint;
	}

	/**
	 * @return the fullAddr
	 */
	public String getFullAddr() {
		return fullAddr;
	}

	/**
	 * @param fullAddr
	 *            the fullAddr to set
	 */
	public void setFullAddr(String fullAddr) {
		if(StringUtils.isNotBlank(fullAddr)){
			this.fullAddr = fullAddr.replaceAll("�", "i");
		}
		this.fullAddr = fullAddr;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the county
	 */
	public String getCounty() {
		return county;
	}

	/**
	 * @param county
	 *            the county to set
	 */
	public void setCounty(String county) {
		this.county = county;
	}

	/**
	 * @return the disposition
	 */
	public String getDisposition() {
		return disposition;
	}

	/**
	 * @param disposition
	 *            the disposition to set
	 */
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	/**
	 * @param geoPointForKML
	 *            the geoPointForKML to set
	 */
	public void setGeoPointForKML(String geoPointForKML) {
		this.geoPointForKML = geoPointForKML;
	}

	public String getValidationCode() {
		return validationCode;
	}

	public void setValidationCode(String validationCode) {
		this.validationCode = validationCode;
	}

	public String getResponseText() {
		return responseText;
	}

	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}

	public Long getCampaignSeq() {
		return campaignSeq;
	}

	public void setCampaignSeq(Long campaignSeq) {
		this.campaignSeq = campaignSeq;
	}

	public Long getOfficeSeq() {
		return officeSeq;
	}

	public void setOfficeSeq(Long officeSeq) {
		this.officeSeq = officeSeq;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	/**
	 * @return the imageIcon
	 */
	public String getImageIcon() {
		return imageIcon;
	}

	/**
	 * @param imageIcon
	 *            the imageIcon to set
	 */
	public void setImageIcon(String imageIcon) {
		this.imageIcon = imageIcon;
	}

	public String getRunId() {
		return runId;
	}

	public void setRunId(String runId) {
		this.runId = runId;
	}

	public String getDa() {
		if (StringUtils.isBlank(da)) {
			return "NULL";
		}
		return da;
	}

	public void setDa(String da) {
		this.da = da;
	}

	public String getWireCenter() {
		if (StringUtils.isBlank(wireCenter)) {
			return "NULL";
		}
		return wireCenter;
	}

	public void setWireCenter(String wirecenter) {
		this.wireCenter = wirecenter;
	}

	public boolean isDA() {
		return StringUtils.isNotBlank(this.da);
	}

	public boolean isThisWireCenter() {
		return StringUtils.isNotBlank(this.wireCenter);
	}

	/**
	 * @return the leadSheetId
	 */
	public String getLeadSheetId() {
		return leadSheetId;
	}

	/**
	 * @param leadSheetId
	 *            the leadSheetId to set
	 */
	public void setLeadSheetId(String leadSheetId) {
		this.leadSheetId = leadSheetId;
	}

	public Long getRowId() {
		return rowId;
	}

	public void setRowId(Long rowId) {
		this.rowId = rowId;
	}

	public String getDispositionalImage() {
		return dispositionalImage;
	}

	public void setDispositionalImage(String dispositionalImage) {
		this.dispositionalImage = dispositionalImage;
	}

	public String getDispositionCode() {
		return dispositionCode;
	}

	public void setDispositionCode(String dispositionCode) {
		this.dispositionCode = dispositionCode;
	}

}
