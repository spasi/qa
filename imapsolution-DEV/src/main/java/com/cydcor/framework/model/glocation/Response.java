package com.cydcor.framework.model.glocation;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Response")
public class Response {
	private String name;
	private Status Status;

	@XStreamImplicit
	private List<Placemark> Placemark= new ArrayList<Placemark>();

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	/**
	 * @return the placemark
	 */
	public List<Placemark> getPlacemark() {
		return Placemark;
	}

	/**
	 * @param placemark
	 *            the placemark to set
	 */
	public void setPlacemark(List<Placemark> placemark) {
		this.Placemark = placemark;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return Status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		Status = status;
	}

	@Override
	public String toString() {
		return "Response [Placemark=" + Placemark + ", Status=" + Status
				+ ", name=" + name + "]";
	}

	
}
