package com.cydcor.framework.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("LineString")
public class LineString {
	private String extrude;
	private String tessellate;
	private String altitudeMode;
	private String coordinates;

	/**
	 * @return the extrude
	 */
	public String getExtrude() {
		return extrude;
	}

	/**
	 * @param extrude
	 *            the extrude to set
	 */
	public void setExtrude(String extrude) {
		this.extrude = extrude;
	}

	/**
	 * @return the tessellate
	 */
	public String getTessellate() {
		return tessellate;
	}

	/**
	 * @param tessellate
	 *            the tessellate to set
	 */
	public void setTessellate(String tessellate) {
		this.tessellate = tessellate;
	}

	/**
	 * @return the altitudeMode
	 */
	public String getAltitudeMode() {
		return altitudeMode;
	}

	/**
	 * @param altitudeMode
	 *            the altitudeMode to set
	 */
	public void setAltitudeMode(String altitudeMode) {
		this.altitudeMode = altitudeMode;
	}

	/**
	 * @return the coordinates
	 */
	public String getCoordinates() {
		return coordinates;
	}

	/**
	 * @param coordinates
	 *            the coordinates to set
	 */
	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}
}
