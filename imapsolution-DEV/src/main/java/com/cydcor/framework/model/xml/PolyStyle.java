package com.cydcor.framework.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PolyStyle")
public class PolyStyle {
	private String color;
	private String colorMode;
	private int fill;
	private int outline;

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the colorMode
	 */
	public String getColorMode() {
		return colorMode;
	}

	/**
	 * @param colorMode
	 *            the colorMode to set
	 */
	public void setColorMode(String colorMode) {
		this.colorMode = colorMode;
	}

	/**
	 * @return the fill
	 */
	public int getFill() {
		return fill;
	}

	/**
	 * @param fill the fill to set
	 */
	public void setFill(int fill) {
		this.fill = fill;
	}

	/**
	 * @return the outline
	 */
	public int getOutline() {
		return outline;
	}

	/**
	 * @param outline the outline to set
	 */
	public void setOutline(int outline) {
		this.outline = outline;
	}

}
