/**
 * 
 */
package com.cydcor.framework.model.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author suresh.chittala
 *
 */
@XStreamAlias("IconStyle")
public class IconStyle {
private Icon icon;

/**
 * @return the icon
 */
public Icon getIcon() {
	return icon;
}

/**
 * @param icon the icon to set
 */
public void setIcon(Icon icon) {
	this.icon = icon;
}
}
