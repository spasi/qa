package com.cydcor.framework.model.json;

public class Result {
	
	private Geometries geometries;

	/**
	 * @return the geometries
	 */
	public Geometries getGeometries() {
		return geometries;
	}

	/**
	 * @param geometries
	 *            the geometries to set
	 */
	public void setGeometries(Geometries geometries) {
		this.geometries = geometries;
	}

}
