package com.cydcor.framework.model;

import java.sql.Timestamp;

public class LeadLifeCycle {
	private Long lmsRowID;
	private String runId;
	private String leadId;
	private Long campaignSeq;
	private String officeSeq;
	private String leadOwner;
	private String personId;
	private Timestamp lead_dob;
	private Timestamp leadAssigned;
	private Timestamp leadReleaseDate;
	private Timestamp leadExpires;
	private String custName;
	private String geoPoint;
	private String validationCode;
	private String reponseFromGoogle;
	private String fullAddr;
	private String streetAddress;
	private String aptNum;
	private String city;
	private String state;
	private String stateName;
	private String zip;
	private String wireCenter;
	private String da;
	private String markerIconImage;
	private String leadStatus;
	private String creditClass;
	private String greenStatus;
	private String connTech;
	private String aptInd;
	private String dispositionCode;
	private String disposition;
	private String notes;
	private String isActive;
	private Timestamp createdDate;
	private String createdBy;
	private Timestamp modifiedDate;
	private String modifiedBy;

	/**
	 * @return the leadId
	 */
	public Long getLmsRowID() {
		return lmsRowID;
	}

	/**
	 * @param leadId
	 *            the leadId to set
	 */
	public void setLmsRowID(Long lmsRowID) {
		this.lmsRowID = lmsRowID;
	}
	
	/**
	 * @return the leadId
	 */
	public String getLeadId() {
		return leadId;
	}

	/**
	 * @param leadId
	 *            the leadId to set
	 */
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	/**
	 * @return the geoPoint
	 */
	public String getGeoPoint() {
		return geoPoint;
	}

	/**
	 * @param geoPoint
	 *            the geoPoint to set
	 */
	public void setGeoPoint(String geoPoint) {
		this.geoPoint = geoPoint;
	}

	/**
	 * @return the validationCode
	 */
	public String getValidationCode() {
		return validationCode;
	}

	/**
	 * @param validationCode
	 *            the validationCode to set
	 */
	public void setValidationCode(String validationCode) {
		this.validationCode = validationCode;
	}

	/**
	 * @return the reponseFromGoogle
	 */
	public String getReponseFromGoogle() {
		return reponseFromGoogle;
	}

	/**
	 * @param reponseFromGoogle
	 *            the reponseFromGoogle to set
	 */
	public void setReponseFromGoogle(String reponseFromGoogle) {
		this.reponseFromGoogle = reponseFromGoogle;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdDate
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the modifiedDate
	 */
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            the modifiedDate to set
	 */
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public Long getCampaignSeq() {
		return campaignSeq;
	}

	public void setCampaignSeq(Long campaignSeq) {
		this.campaignSeq = campaignSeq;
	}

	public String getOfficeSeq() {
		return officeSeq;
	}

	public void setOfficeSeq(String officeSeq) {
		this.officeSeq = officeSeq;
	}

	public String getLeadOwner() {
		return leadOwner;
	}

	public void setLeadOwner(String leadOwner) {
		this.leadOwner = leadOwner;
	}

	public String getWireCenter() {
		return wireCenter;
	}

	public void setWireCenter(String wireCenter) {
		this.wireCenter = wireCenter;
	}

	public String getDa() {
		return da;
	}

	public void setDa(String da) {
		this.da = da;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getMarkerIconImage() {
		return markerIconImage;
	}

	public void setMarkerIconImage(String markerIconImage) {
		this.markerIconImage = markerIconImage;
	}

	public String getCreditClass() {
		return creditClass;
	}

	public void setCreditClass(String creditClass) {
		this.creditClass = creditClass;
	}

	public String getGreenStatus() {
		return greenStatus;
	}

	public void setGreenStatus(String greenStatus) {
		this.greenStatus = greenStatus;
	}

	public String getConnTech() {
		return connTech;
	}

	public void setConnTech(String connTech) {
		this.connTech = connTech;
	}

	public String getDispositionCode() {
		return dispositionCode;
	}

	public void setDispositionCode(String dispositionCode) {
		this.dispositionCode = dispositionCode;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getRunId() {
		return runId;
	}

	public void setRunId(String runId) {
		this.runId = runId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getFullAddr() {
		return fullAddr;
	}

	public void setFullAddr(String fullAddr) {
		this.fullAddr = fullAddr;
	}

	public String getAptNum() {
		return aptNum;
	}

	public void setAptNum(String aptNum) {
		this.aptNum = aptNum;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Timestamp getLeadExpires() {
		return leadExpires;
	}

	public void setLeadExpires(Timestamp leadExpires) {
		this.leadExpires = leadExpires;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Timestamp getLead_dob() {
		return lead_dob;
	}

	public void setLead_dob(Timestamp leadDob) {
		lead_dob = leadDob;
	}

	public Timestamp getLeadAssigned() {
		return leadAssigned;
	}

	public void setLeadAssigned(Timestamp leadAssigned) {
		this.leadAssigned = leadAssigned;
	}

	public Timestamp getLeadReleaseDate() {
		return leadReleaseDate;
	}

	public void setLeadReleaseDate(Timestamp leadReleaseDate) {
		this.leadReleaseDate = leadReleaseDate;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getAptInd() {
		return aptInd;
	}

	public void setAptInd(String aptInd) {
		this.aptInd = aptInd;
	}

}
