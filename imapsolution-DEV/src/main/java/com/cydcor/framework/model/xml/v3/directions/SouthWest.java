package com.cydcor.framework.model.xml.v3.directions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("south_west")
public class SouthWest {
    private String lat;
    private String lng;

    /**
     * @return the lat
     */
    public String getLat() {
	return lat;
    }

    /**
     * @param lat
     *            the lat to set
     */
    public void setLat(String lat) {
	this.lat = lat;
    }

    /**
     * @return the lan
     */
    public String getLng() {
	return lng;
    }

    /**
     * @param lan
     *            the lan to set
     */
    public void setLan(String lan) {
	this.lng = lng;
    }

}
