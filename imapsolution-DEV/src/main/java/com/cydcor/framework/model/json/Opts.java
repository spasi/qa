package com.cydcor.framework.model.json;

public class Opts {
	private boolean clickable;

	/**
	 * @return the clickable
	 */
	public boolean isClickable() {
		return clickable;
	}

	/**
	 * @param clickable
	 *            the clickable to set
	 */
	public void setClickable(boolean clickable) {
		this.clickable = clickable;
	}
}
