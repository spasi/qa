/**
 * 
 */
package com.cydcor.framework.model;

/**
 * This Class defines any Column that is dispayed on webpage either in Tabular
 * Format or in Form Based Format It Extends exiting ColumnComponent to add some
 * Framework Defined Differentiators
 * 
 * @author ashwin
 * 
 */
public class DisplayColumnComponent extends ColumnMetadata {
	/**
	 * Can be LOV/PICKList/DATE/File Chooser/NUMBER/STRING/CHECK BOX. Depending
	 * on type the value of this display Column Can be Either stored in present
	 * Table or in some External Table. Values of this display column can have a
	 * differnt source table from where data is populated
	 */
	private String displayColumnType = "";

	// This defines the loaction from which the displayed values are source
	private String sourceEntityName = "";
	private String sourceLabelColumnName = "";
	private String sourceValueColumnName = "";

	// This is Mapped to Present Table Primary Key
	private String foreignKeyColumnName = "";
	private String foreignEntityColumnNameToStore = "";

	// Now some Displayable and Editable Properties
	private boolean isDisplayable = true;
	private boolean isEditable = true;

	/**
	 * Defined for elements that are loaded by ajax after events triggerring
	 * This is made dynamic with Freemarker
	 */
	private String ajaxRequestString = "";

	/**
	 * @return the displayColumnType
	 */
	public String getDisplayColumnType() {
		return displayColumnType;
	}

	/**
	 * @param displayColumnType
	 *            the displayColumnType to set
	 */
	public void setDisplayColumnType(String displayColumnType) {
		this.displayColumnType = displayColumnType;
	}

	/**
	 * @return the sourceEntityName
	 */
	public String getSourceEntityName() {
		return sourceEntityName;
	}

	/**
	 * @param sourceEntityName
	 *            the sourceEntityName to set
	 */
	public void setSourceEntityName(String sourceEntityName) {
		this.sourceEntityName = sourceEntityName;
	}

	/**
	 * @return the sourceLabelColumnName
	 */
	public String getSourceLabelColumnName() {
		return sourceLabelColumnName;
	}

	/**
	 * @param sourceLabelColumnName
	 *            the sourceLabelColumnName to set
	 */
	public void setSourceLabelColumnName(String sourceLabelColumnName) {
		this.sourceLabelColumnName = sourceLabelColumnName;
	}

	/**
	 * @return the sourceValueColumnName
	 */
	public String getSourceValueColumnName() {
		return sourceValueColumnName;
	}

	/**
	 * @param sourceValueColumnName
	 *            the sourceValueColumnName to set
	 */
	public void setSourceValueColumnName(String sourceValueColumnName) {
		this.sourceValueColumnName = sourceValueColumnName;
	}

	/**
	 * @return the foreignKeyColumnName
	 */
	public String getForeignKeyColumnName() {
		return foreignKeyColumnName;
	}

	/**
	 * @param foreignKeyColumnName
	 *            the foreignKeyColumnName to set
	 */
	public void setForeignKeyColumnName(String foreignKeyColumnName) {
		this.foreignKeyColumnName = foreignKeyColumnName;
	}

	/**
	 * @return the foreignEntityColumnNameToStore
	 */
	public String getForeignEntityColumnNameToStore() {
		return foreignEntityColumnNameToStore;
	}

	/**
	 * @param foreignEntityColumnNameToStore
	 *            the foreignEntityColumnNameToStore to set
	 */
	public void setForeignEntityColumnNameToStore(
			String foreignEntityColumnNameToStore) {
		this.foreignEntityColumnNameToStore = foreignEntityColumnNameToStore;
	}

	/**
	 * @return the isDisplayable
	 */
	public boolean isDisplayable() {
		return isDisplayable;
	}

	/**
	 * @param isDisplayable
	 *            the isDisplayable to set
	 */
	public void setDisplayable(boolean isDisplayable) {
		this.isDisplayable = isDisplayable;
	}

	/**
	 * @return the isEditable
	 */
	public boolean isEditable() {
		return isEditable;
	}

	/**
	 * @param isEditable
	 *            the isEditable to set
	 */
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

}
