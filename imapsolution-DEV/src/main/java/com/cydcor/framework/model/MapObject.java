/**
 * 
 */
package com.cydcor.framework.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ashwin
 * 
 */
public class MapObject {
	private String key = "";
	private String value = null;
	private Object actualValue = null;

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the actualValue
	 */
	public Object getActualValue() {
		return actualValue;
	}

	/**
	 * @param actualValue
	 *            the actualValue to set
	 */
	public void setActualValue(Object actualValue) {
		this.actualValue = actualValue;
		if (actualValue != null && actualValue instanceof Date) {
			this.value = gettFormattedDate("MM/dd/yyyy", (Date) actualValue);
		}
	}

	public static String gettFormattedDate(String format, Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String newDateString = formatter.format(date);
		return newDateString;
	}

	public static void main(String[] args) {
		//System.out.println(gettFormattedDate("MM/dd/yyyy", new Date()));
	}
}
