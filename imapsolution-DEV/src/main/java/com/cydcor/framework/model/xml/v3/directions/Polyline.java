package com.cydcor.framework.model.xml.v3.directions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("poly_line")
public class Polyline {
    private String points;

    /**
     * @return the points
     */
    public String getPoints() {
	return points;
    }

    /**
     * @param points
     *            the points to set
     */
    public void setPoints(String points) {
	this.points = points;
    }

}
