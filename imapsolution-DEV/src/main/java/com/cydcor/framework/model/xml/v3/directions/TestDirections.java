package com.cydcor.framework.model.xml.v3.directions;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;

import com.cydcor.framework.model.glocation.Kml;
import com.cydcor.framework.utils.XStreamUtils;

public class TestDirections {

    /**
     * @param args
     */
    public static void main(String[] args) {
	String url = "http://maps.googleapis.com/maps/api/directions/xml?origin=Toronto&destination=Montreal&sensor=false";

	HttpClient client = null;
	GetMethod method = null;
	Kml kml = null;

	client = new HttpClient();
	method = new GetMethod(url);
	try {
	    client.executeMethod(method);
	} catch (HttpException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	DirectionsResponse directionsResponse = null;
	try {
	    String response = method.getResponseBodyAsString();
	    System.out.println(response);
	    directionsResponse = (DirectionsResponse) XStreamUtils.getObjectFromXml(
		    DirectionsResponse.class, response);
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	System.out.println(directionsResponse);

    }

}
