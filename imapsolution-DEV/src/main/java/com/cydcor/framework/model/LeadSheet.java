package com.cydcor.framework.model;

import java.sql.Timestamp;

public class LeadSheet extends GenericModel {
	private String leadsheet_id;
	private String lead_id;
	private long person_id;
	private long campaign_seq;
	private long office_seq;
	private String cust_name;
	private String full_addr;
	private String apt_num;
	private String city;
	private String zip;
	private String da;
	private String disposition;
	private String notes;
	private String is_active;
	private Timestamp created_date;
	private String created_by;
	private Timestamp modified_date;
	private String modified_by;

	/**
	 * @return the leadsheet_id
	 */
	public String getLeadsheet_id() {
		return leadsheet_id;
	}

	/**
	 * @param leadsheetId
	 *            the leadsheet_id to set
	 */
	public void setLeadsheet_id(String leadsheetId) {
		leadsheet_id = leadsheetId;
	}

	/**
	 * @return the lead_id
	 */
	public String getLead_id() {
		return lead_id;
	}

	/**
	 * @param leadId
	 *            the lead_id to set
	 */
	public void setLead_id(String leadId) {
		lead_id = leadId;
	}

	/**
	 * @return the person_id
	 */
	public long getPerson_id() {
		return person_id;
	}

	/**
	 * @param personId
	 *            the person_id to set
	 */
	public void setPerson_id(long personId) {
		person_id = personId;
	}

	/**
	 * @return the campaign_seq
	 */
	public long getCampaign_seq() {
		return campaign_seq;
	}

	/**
	 * @param campaignSeq
	 *            the campaign_seq to set
	 */
	public void setCampaign_seq(long campaignSeq) {
		campaign_seq = campaignSeq;
	}

	/**
	 * @return the office_seq
	 */
	public long getOffice_seq() {
		return office_seq;
	}

	/**
	 * @param officeSeq
	 *            the office_seq to set
	 */
	public void setOffice_seq(long officeSeq) {
		office_seq = officeSeq;
	}

	/**
	 * @return the cust_name
	 */
	public String getCust_name() {
		return cust_name;
	}

	/**
	 * @param custName
	 *            the cust_name to set
	 */
	public void setCust_name(String custName) {
		cust_name = custName;
	}

	/**
	 * @return the full_addr
	 */
	public String getFull_addr() {
		return full_addr;
	}

	/**
	 * @param fullAddr
	 *            the full_addr to set
	 */
	public void setFull_addr(String fullAddr) {
		full_addr = fullAddr;
	}

	/**
	 * @return the apt_num
	 */
	public String getApt_num() {
		return apt_num;
	}

	/**
	 * @param aptNum
	 *            the apt_num to set
	 */
	public void setApt_num(String aptNum) {
		apt_num = aptNum;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the da
	 */
	public String getDa() {
		return da;
	}

	/**
	 * @param da
	 *            the da to set
	 */
	public void setDa(String da) {
		this.da = da;
	}

	/**
	 * @return the disposition
	 */
	public String getDisposition() {
		return disposition;
	}

	/**
	 * @param disposition
	 *            the disposition to set
	 */
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the is_active
	 */
	public String getIs_active() {
		return is_active;
	}

	/**
	 * @param isActive
	 *            the is_active to set
	 */
	public void setIs_active(String isActive) {
		is_active = isActive;
	}

	/**
	 * @return the created_date
	 */
	public Timestamp getCreated_date() {
		return created_date;
	}

	/**
	 * @param createdDate
	 *            the created_date to set
	 */
	public void setCreated_date(Timestamp createdDate) {
		created_date = createdDate;
	}

	/**
	 * @return the created_by
	 */
	public String getCreated_by() {
		return created_by;
	}

	/**
	 * @param createdBy
	 *            the created_by to set
	 */
	public void setCreated_by(String createdBy) {
		created_by = createdBy;
	}

	/**
	 * @return the modified_date
	 */
	public Timestamp getModified_date() {
		return modified_date;
	}

	/**
	 * @param modifiedDate
	 *            the modified_date to set
	 */
	public void setModified_date(Timestamp modifiedDate) {
		modified_date = modifiedDate;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() {
		return modified_by;
	}

	/**
	 * @param modifiedBy
	 *            the modified_by to set
	 */
	public void setModified_by(String modifiedBy) {
		modified_by = modifiedBy;
	}
}