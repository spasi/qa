package com.cydcor.framework.model.xml.v3.directions;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("leg")
public class Leg {

    @XStreamImplicit
    private List<Step> step = new ArrayList<Step>();
    private Duration duration;
    private Distance distance;
    private StartLocation start_location;
    private EndLocation end_location;
    private String start_address;
    private String end_address;

    /**
     * @return the step
     */
    public List<Step> getStep() {
	return step;
    }

    /**
     * @param step
     *            the step to set
     */
    public void setStep(List<Step> step) {
	this.step = step;
    }

    /**
     * @return the duration
     */
    public Duration getDuration() {
	return duration;
    }

    /**
     * @param duration
     *            the duration to set
     */
    public void setDuration(Duration duration) {
	this.duration = duration;
    }

    /**
     * @return the distance
     */
    public Distance getDistance() {
	return distance;
    }

    /**
     * @param distance
     *            the distance to set
     */
    public void setDistance(Distance distance) {
	this.distance = distance;
    }

    /**
     * @return the start_location
     */
    public StartLocation getStart_location() {
	return start_location;
    }

    /**
     * @param start_location
     *            the start_location to set
     */
    public void setStart_location(StartLocation start_location) {
	this.start_location = start_location;
    }

    /**
     * @return the end_location
     */
    public EndLocation getEnd_location() {
	return end_location;
    }

    /**
     * @param end_location
     *            the end_location to set
     */
    public void setEnd_location(EndLocation end_location) {
	this.end_location = end_location;
    }

    /**
     * @return the start_address
     */
    public String getStart_address() {
	return start_address;
    }

    /**
     * @param start_address
     *            the start_address to set
     */
    public void setStart_address(String start_address) {
	this.start_address = start_address;
    }

    /**
     * @return the end_address
     */
    public String getEnd_address() {
	return end_address;
    }

    /**
     * @param end_address
     *            the end_address to set
     */
    public void setEnd_address(String end_address) {
	this.end_address = end_address;
    }

}
