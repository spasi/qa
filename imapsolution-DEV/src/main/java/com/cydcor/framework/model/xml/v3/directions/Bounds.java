package com.cydcor.framework.model.xml.v3.directions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("bounds")
public class Bounds {
    private SouthWest southwest;
    private NorthEast northeast;

    /**
     * @return the southwest
     */
    public SouthWest getSouthwest() {
	return southwest;
    }

    /**
     * @param southwest
     *            the southwest to set
     */
    public void setSouthwest(SouthWest southwest) {
	this.southwest = southwest;
    }

    /**
     * @return the northEast
     */
    public NorthEast getNortheast() {
	return northeast;
    }

    /**
     * @param northEast
     *            the northEast to set
     */
    public void setNortheast(NorthEast northEast) {
	this.northeast = northEast;
    }

}
