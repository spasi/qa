/**
 * 
 */
package com.cydcor.framework.model.glocation;

/**
 * @author ashwin kumar
 * 
 */
public class GeocodeResponse extends Kml {
	private String formattedAddress = null;
	private String latitude = null;
	private String logitude = null;
	private String responseCode = null;

	/**
	 * @return the formattedAddress
	 */
	public String getFormattedAddress() {
		return formattedAddress;
	}

	/**
	 * @param formattedAddress
	 *            the formattedAddress to set
	 */
	public void setFormattedAddress(String formattedAddress) {
		this.formattedAddress = formattedAddress;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the logitude
	 */
	public String getLogitude() {
		return logitude;
	}

	/**
	 * @param logitude
	 *            the logitude to set
	 */
	public void setLogitude(String logitude) {
		this.logitude = logitude;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

}
