package com.cydcor.framework.model;

import java.sql.Timestamp;

public class MerICLDetails extends GenericModel implements java.io.Serializable{
	private String office_seq;
	private String office_code;
	private String office_state;
	private String office_city;
	private String office_address1;
	private String office_address2;
	private String office_zip;
	private String active;
	private String hub_manager;
	private String owner;
	private String promoting_manager;
	private String comments;
	private String email;
	private String phone;
	private String office_name;
	private String fax;
	private String cell;
	private String fed_ex;
	private Timestamp start_date;
	private Timestamp close_date;
	private String fein;
	private String hub_manager_seq;
	private String office_update;
	private String quarterback;
	private String hot_jobs;
	private String class_id;
	private String web_user;
	private Timestamp last_update;
	private String last_update_by;
	private String office_country;
	private Timestamp insurance_exp_date;
	private Timestamp work_comp_exp_date;
	private String payroll_processor;
	private Timestamp date_of_birth;
	private String address_change;
	private String consult_seq;
	private String consultant;
	private String closure_reason_seq;
	private String closed;
	private String office_legal_name;
	private String secondary_email;
	private String consultant_function_call;
	private String promoting_mgr_function_call;
	private String process_status;
	private Timestamp process_date;
	private String error_message;
	private long retry_count;

	public String getOffice_seq() {
		return office_seq;
	}

	public void setOffice_seq(String office_seq) {
		this.office_seq = office_seq;
	}

	public String getOffice_code() {
		return office_code;
	}

	public void setOffice_code(String office_code) {
		this.office_code = office_code;
	}

	public String getOffice_state() {
		return office_state;
	}

	public void setOffice_state(String office_state) {
		this.office_state = office_state;
	}

	public String getOffice_city() {
		return office_city;
	}

	public void setOffice_city(String office_city) {
		this.office_city = office_city;
	}

	public String getOffice_address1() {
		return office_address1;
	}

	public void setOffice_address1(String office_address1) {
		this.office_address1 = office_address1;
	}

	public String getOffice_address2() {
		return office_address2;
	}

	public void setOffice_address2(String office_address2) {
		this.office_address2 = office_address2;
	}

	public String getOffice_zip() {
		return office_zip;
	}

	public void setOffice_zip(String office_zip) {
		this.office_zip = office_zip;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getHub_manager() {
		return hub_manager;
	}

	public void setHub_manager(String hub_manager) {
		this.hub_manager = hub_manager;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getPromoting_manager() {
		return promoting_manager;
	}

	public void setPromoting_manager(String promoting_manager) {
		this.promoting_manager = promoting_manager;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOffice_name() {
		return office_name;
	}

	public void setOffice_name(String office_name) {
		this.office_name = office_name;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getFed_ex() {
		return fed_ex;
	}

	public void setFed_ex(String fed_ex) {
		this.fed_ex = fed_ex;
	}

	public Timestamp getStart_date() {
		return start_date;
	}

	public void setStart_date(Timestamp start_date) {
		this.start_date = start_date;
	}

	public Timestamp getClose_date() {
		return close_date;
	}

	public void setClose_date(Timestamp close_date) {
		this.close_date = close_date;
	}

	public String getFein() {
		return fein;
	}

	public void setFein(String fein) {
		this.fein = fein;
	}

	public String getHub_manager_seq() {
		return hub_manager_seq;
	}

	public void setHub_manager_seq(String hub_manager_seq) {
		this.hub_manager_seq = hub_manager_seq;
	}

	public String getOffice_update() {
		return office_update;
	}

	public void setOffice_update(String office_update) {
		this.office_update = office_update;
	}

	public String getQuarterback() {
		return quarterback;
	}

	public void setQuarterback(String quarterback) {
		this.quarterback = quarterback;
	}

	public String getHot_jobs() {
		return hot_jobs;
	}

	public void setHot_jobs(String hot_jobs) {
		this.hot_jobs = hot_jobs;
	}

	public String getClass_id() {
		return class_id;
	}

	public void setClass_id(String class_id) {
		this.class_id = class_id;
	}

	public String getWeb_user() {
		return web_user;
	}

	public void setWeb_user(String web_user) {
		this.web_user = web_user;
	}

	public Timestamp getLast_update() {
		return last_update;
	}

	public void setLast_update(Timestamp last_update) {
		this.last_update = last_update;
	}

	public String getLast_update_by() {
		return last_update_by;
	}

	public void setLast_update_by(String last_update_by) {
		this.last_update_by = last_update_by;
	}

	public String getOffice_country() {
		return office_country;
	}

	public void setOffice_country(String office_country) {
		this.office_country = office_country;
	}

	public Timestamp getInsurance_exp_date() {
		return insurance_exp_date;
	}

	public void setInsurance_exp_date(Timestamp insurance_exp_date) {
		this.insurance_exp_date = insurance_exp_date;
	}

	public Timestamp getWork_comp_exp_date() {
		return work_comp_exp_date;
	}

	public void setWork_comp_exp_date(Timestamp work_comp_exp_date) {
		this.work_comp_exp_date = work_comp_exp_date;
	}

	public String getPayroll_processor() {
		return payroll_processor;
	}

	public void setPayroll_processor(String payroll_processor) {
		this.payroll_processor = payroll_processor;
	}

	public Timestamp getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(Timestamp date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public String getAddress_change() {
		return address_change;
	}

	public void setAddress_change(String address_change) {
		this.address_change = address_change;
	}

	public String getConsult_seq() {
		return consult_seq;
	}

	public void setConsult_seq(String consult_seq) {
		this.consult_seq = consult_seq;
	}

	public String getConsultant() {
		return consultant;
	}

	public void setConsultant(String consultant) {
		this.consultant = consultant;
	}

	public String getClosure_reason_seq() {
		return closure_reason_seq;
	}

	public void setClosure_reason_seq(String closure_reason_seq) {
		this.closure_reason_seq = closure_reason_seq;
	}

	public String getClosed() {
		return closed;
	}

	public void setClosed(String closed) {
		this.closed = closed;
	}

	public String getOffice_legal_name() {
		return office_legal_name;
	}

	public void setOffice_legal_name(String office_legal_name) {
		this.office_legal_name = office_legal_name;
	}

	public String getSecondary_email() {
		return secondary_email;
	}

	public void setSecondary_email(String secondary_email) {
		this.secondary_email = secondary_email;
	}

	public String getConsultant_function_call() {
		return consultant_function_call;
	}

	public void setConsultant_function_call(String consultant_function_call) {
		this.consultant_function_call = consultant_function_call;
	}

	public String getPromoting_mgr_function_call() {
		return promoting_mgr_function_call;
	}

	public void setPromoting_mgr_function_call(
			String promoting_mgr_function_call) {
		this.promoting_mgr_function_call = promoting_mgr_function_call;
	}

	public String getProcess_status() {
		return process_status;
	}

	public void setProcess_status(String process_status) {
		this.process_status = process_status;
	}

	public Timestamp getProcess_date() {
		return process_date;
	}

	public void setProcess_date(Timestamp process_date) {
		this.process_date = process_date;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}

	public long getRetry_count() {
		return retry_count;
	}

	public void setRetry_count(long retry_count) {
		this.retry_count = retry_count;
	}
}