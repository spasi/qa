package com.cydcor.framework.model;

import java.sql.Timestamp;

public class MerCampignDetails extends GenericModel implements java.io.Serializable{
	private String campaign_seq;
	private String campaign_name;
	private String campaign_abbrev;
	private String holdback_percent;
	private String holdback_weeks;
	private String reject_days;
	private String reject_date;
	private String campaign_manager;
	private Timestamp start_date;
	private Timestamp end_date;
	private String active;
	private String info1;
	private String info2;
	private String info3;
	private String info4;
	private String info5;
	private String info1_unique;
	private String info2_unique;
	private String info3_unique;
	private String info4_unique;
	private String info5_unique;
	private String admin_fee;
	private String week_end_day;
	private String pay_based;
	private String bill_based;
	private String match_type;
	private String pay_format_type;
	private String apply_admin_fee;
	private String surcharge_percentage;
	private String apply_surcharge;
	private String surchare_percentage;
	private String gp_abbrev;
	private String gp_customer_id;
	private String duplicate;
	private String info1_dup;
	private String info2_dup;
	private String info3_dup;
	private String info4_dup;
	private String info5_dup;
	private String gp_pro_forma;
	private String org_override_type;
	private String org_override_based_on;
	private String camp_override_type;
	private String camp_override_based_on;
	private String info6;
	private String info7;
	private String info8;
	private String info9;
	private String info10;
	private String info11;
	private String info12;
	private String info13;
	private String info14;
	private String info15;
	private String fots_office;
	private String fots_agent;
	private String fots_sales_date;
	private String fots_info1;
	private String fots_info2;
	private String fots_info3;
	private String fots_info4;
	private String fots_info5;
	private String fots_info6;
	private String fots_info7;
	private String fots_info8;
	private String fots_info9;
	private String fots_info10;
	private String fots_info11;
	private String fots_info12;
	private String fots_info13;
	private String fots_info14;
	private String fots_info15;
	private String web_info1;
	private String web_info2;
	private String web_info3;
	private String web_info4;
	private String web_info5;
	private String web_info6;
	private String web_info7;
	private String web_info8;
	private String web_info9;
	private String web_info10;
	private String web_info11;
	private String web_info12;
	private String web_info13;
	private String web_info14;
	private String web_info15;
	private String lock_fots;
	private String fots_new_format;
	private String info1_bill_display;
	private String info2_bill_display;
	private String info3_bill_display;
	private String info4_bill_display;
	private String info5_bill_display;
	private String info6_bill_display;
	private String info7_bill_display;
	private String info8_bill_display;
	private String info9_bill_display;
	private String info10_bill_display;
	private String info11_bill_display;
	private String info12_bill_display;
	private String info13_bill_display;
	private String info14_bill_display;
	private String info15_bill_display;
	private String info1_pay_display;
	private String info2_pay_display;
	private String info3_pay_display;
	private String info4_pay_display;
	private String info5_pay_display;
	private String info6_pay_display;
	private String info7_pay_display;
	private String info8_pay_display;
	private String info9_pay_display;
	private String info10_pay_display;
	private String info11_pay_display;
	private String info12_pay_display;
	private String info13_pay_display;
	private String info14_pay_display;
	private String info15_pay_display;
	private String appeal_type;
	private Timestamp appeal_start_date;
	private String appeal_pay_periods;
	private String fots_validation;
	private String compliance_req;
	private String allow_tpv;
	private String secondary_match;
	private String info1_secondary;
	private String info2_secondary;
	private String info3_secondary;
	private String info4_secondary;
	private String info5_secondary;
	private String automatach_code;
	private String automatch_code;
	private String attribute1_title;
	private String tpv_message1;
	private String bill_ignored_rej;
	private String auto_submit;
	private String auto_submit_email;
	private String auto_submit_ftp;
	private String auto_submit_email_destination;
	private String auto_submit_ftp_destination;
	private String auto_submit_time;
	private Timestamp auto_submit_last_run;
	private String edit_staging;
	private String cydcor_times_cutoff;
	private String person_worked_info_req;
	private String use_person_worked_info;
	private String attribute1_description;
	private String campaign_info_required;
	private String mask_test;
	private String attribute1_description_2;
	private String attribute1_description_3;
	private String attribute1_description_4;
	private String attribute1_description_5;
	private String attribute1_description_6;
	private String attribute1_description_7;
	private String attribute1_description_8;
	private String attribute1_description_9;
	private String attribute1_description_10;
	private String auto_upload_staging;
	private String staging_ftp_path;
	private String fots_grand_total;
	private String fots_entry_type;
	private String display_absent;
	private String reject_threshold_perc;
	private String new_upload_window_yn;
	private String use_reject_logic_yn;
	private String date_to_use_new_data;
	private String date_to_use_existing_data;
	private String calculate_days_to_use;
	private String number_of_days_reject_valid;
	private Timestamp new_data_process_date;
	private String last_worked_backfill;
	private String product_group_match_flag;
	private String by_row;
	private String match_dup_by_sales_date;
	private String office_info1_unique;
	private String office_info2_unique;
	private String office_info3_unique;
	private String office_info4_unique;
	private String office_info5_unique;
	private String apply_sysdate_on_null_dt_yn;
	private String do_not_update_camp_override;
	private String staging_add_prod_to_app;
	private String info6_dup;
	private String info7_dup;
	private String info8_dup;
	private String info9_dup;
	private String info10_dup;
	private String info11_dup;
	private String info12_dup;
	private String info13_dup;
	private String info14_dup;
	private String info15_dup;
	private String use_reject_logic_yn_b;
	private String pay_bill_display_rb;
	private String date_to_use_new_data_b;
	private String date_to_use_existing_data_b;
	private String calculate_days_to_use_b;
	private String number_of_days_reject_valid_b;
	private String apply_sysdate_on_null_dt_yn_b;
	private String campaign_country;
	private String web_appeal_all;
	private String new_bill_pay_manager_yn;
	private String backfill_days;
	private String backfill_less_equal_1_display;
	private String backfill_less_equal_2_display;
	private String backfill_less_equal_3_display;
	private String backfill_less_equal_4_display;
	private String backfill_less_equal_5_display;
	private String backfill_greater_1_display;
	private String backfill_greater_2_display;
	private String backfill_greater_3_display;
	private String backfill_greater_4_display;
	private String backfill_greater_5_display;
	private String backfill_inactive_reps;
	private String backfill_recent_assigned_rep;
	private String backfill_inactive_offices;
	private String backfill_existing_office_rep;
	private String campaign_type;
	private String product_match_flag;
	private String mon;
	private String tue;
	private String wed;
	private String thu;
	private String fri;
	private String sat;
	private String sun;
	private String do_not_report_ir_rop;
	private String upload_dw_display_type;
	private String process_status;
	private Timestamp process_date;
	private String error_message;
	private long retry_count;

	public String getCampaign_seq() {
		return campaign_seq;
	}

	public void setCampaign_seq(String campaign_seq) {
		this.campaign_seq = campaign_seq;
	}

	public String getCampaign_name() {
		return campaign_name;
	}

	public void setCampaign_name(String campaign_name) {
		this.campaign_name = campaign_name;
	}

	public String getCampaign_abbrev() {
		return campaign_abbrev;
	}

	public void setCampaign_abbrev(String campaign_abbrev) {
		this.campaign_abbrev = campaign_abbrev;
	}

	public String getHoldback_percent() {
		return holdback_percent;
	}

	public void setHoldback_percent(String holdback_percent) {
		this.holdback_percent = holdback_percent;
	}

	public String getHoldback_weeks() {
		return holdback_weeks;
	}

	public void setHoldback_weeks(String holdback_weeks) {
		this.holdback_weeks = holdback_weeks;
	}

	public String getReject_days() {
		return reject_days;
	}

	public void setReject_days(String reject_days) {
		this.reject_days = reject_days;
	}

	public String getReject_date() {
		return reject_date;
	}

	public void setReject_date(String reject_date) {
		this.reject_date = reject_date;
	}

	public String getCampaign_manager() {
		return campaign_manager;
	}

	public void setCampaign_manager(String campaign_manager) {
		this.campaign_manager = campaign_manager;
	}

	public Timestamp getStart_date() {
		return start_date;
	}

	public void setStart_date(Timestamp start_date) {
		this.start_date = start_date;
	}

	public Timestamp getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Timestamp end_date) {
		this.end_date = end_date;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getInfo1() {
		return info1;
	}

	public void setInfo1(String info1) {
		this.info1 = info1;
	}

	public String getInfo2() {
		return info2;
	}

	public void setInfo2(String info2) {
		this.info2 = info2;
	}

	public String getInfo3() {
		return info3;
	}

	public void setInfo3(String info3) {
		this.info3 = info3;
	}

	public String getInfo4() {
		return info4;
	}

	public void setInfo4(String info4) {
		this.info4 = info4;
	}

	public String getInfo5() {
		return info5;
	}

	public void setInfo5(String info5) {
		this.info5 = info5;
	}

	public String getInfo1_unique() {
		return info1_unique;
	}

	public void setInfo1_unique(String info1_unique) {
		this.info1_unique = info1_unique;
	}

	public String getInfo2_unique() {
		return info2_unique;
	}

	public void setInfo2_unique(String info2_unique) {
		this.info2_unique = info2_unique;
	}

	public String getInfo3_unique() {
		return info3_unique;
	}

	public void setInfo3_unique(String info3_unique) {
		this.info3_unique = info3_unique;
	}

	public String getInfo4_unique() {
		return info4_unique;
	}

	public void setInfo4_unique(String info4_unique) {
		this.info4_unique = info4_unique;
	}

	public String getInfo5_unique() {
		return info5_unique;
	}

	public void setInfo5_unique(String info5_unique) {
		this.info5_unique = info5_unique;
	}

	public String getAdmin_fee() {
		return admin_fee;
	}

	public void setAdmin_fee(String admin_fee) {
		this.admin_fee = admin_fee;
	}

	public String getWeek_end_day() {
		return week_end_day;
	}

	public void setWeek_end_day(String week_end_day) {
		this.week_end_day = week_end_day;
	}

	public String getPay_based() {
		return pay_based;
	}

	public void setPay_based(String pay_based) {
		this.pay_based = pay_based;
	}

	public String getBill_based() {
		return bill_based;
	}

	public void setBill_based(String bill_based) {
		this.bill_based = bill_based;
	}

	public String getMatch_type() {
		return match_type;
	}

	public void setMatch_type(String match_type) {
		this.match_type = match_type;
	}

	public String getPay_format_type() {
		return pay_format_type;
	}

	public void setPay_format_type(String pay_format_type) {
		this.pay_format_type = pay_format_type;
	}

	public String getApply_admin_fee() {
		return apply_admin_fee;
	}

	public void setApply_admin_fee(String apply_admin_fee) {
		this.apply_admin_fee = apply_admin_fee;
	}

	public String getSurcharge_percentage() {
		return surcharge_percentage;
	}

	public void setSurcharge_percentage(String surcharge_percentage) {
		this.surcharge_percentage = surcharge_percentage;
	}

	public String getApply_surcharge() {
		return apply_surcharge;
	}

	public void setApply_surcharge(String apply_surcharge) {
		this.apply_surcharge = apply_surcharge;
	}

	public String getSurchare_percentage() {
		return surchare_percentage;
	}

	public void setSurchare_percentage(String surchare_percentage) {
		this.surchare_percentage = surchare_percentage;
	}

	public String getGp_abbrev() {
		return gp_abbrev;
	}

	public void setGp_abbrev(String gp_abbrev) {
		this.gp_abbrev = gp_abbrev;
	}

	public String getGp_customer_id() {
		return gp_customer_id;
	}

	public void setGp_customer_id(String gp_customer_id) {
		this.gp_customer_id = gp_customer_id;
	}

	public String getDuplicate() {
		return duplicate;
	}

	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}

	public String getInfo1_dup() {
		return info1_dup;
	}

	public void setInfo1_dup(String info1_dup) {
		this.info1_dup = info1_dup;
	}

	public String getInfo2_dup() {
		return info2_dup;
	}

	public void setInfo2_dup(String info2_dup) {
		this.info2_dup = info2_dup;
	}

	public String getInfo3_dup() {
		return info3_dup;
	}

	public void setInfo3_dup(String info3_dup) {
		this.info3_dup = info3_dup;
	}

	public String getInfo4_dup() {
		return info4_dup;
	}

	public void setInfo4_dup(String info4_dup) {
		this.info4_dup = info4_dup;
	}

	public String getInfo5_dup() {
		return info5_dup;
	}

	public void setInfo5_dup(String info5_dup) {
		this.info5_dup = info5_dup;
	}

	public String getGp_pro_forma() {
		return gp_pro_forma;
	}

	public void setGp_pro_forma(String gp_pro_forma) {
		this.gp_pro_forma = gp_pro_forma;
	}

	public String getOrg_override_type() {
		return org_override_type;
	}

	public void setOrg_override_type(String org_override_type) {
		this.org_override_type = org_override_type;
	}

	public String getOrg_override_based_on() {
		return org_override_based_on;
	}

	public void setOrg_override_based_on(String org_override_based_on) {
		this.org_override_based_on = org_override_based_on;
	}

	public String getCamp_override_type() {
		return camp_override_type;
	}

	public void setCamp_override_type(String camp_override_type) {
		this.camp_override_type = camp_override_type;
	}

	public String getCamp_override_based_on() {
		return camp_override_based_on;
	}

	public void setCamp_override_based_on(String camp_override_based_on) {
		this.camp_override_based_on = camp_override_based_on;
	}

	public String getInfo6() {
		return info6;
	}

	public void setInfo6(String info6) {
		this.info6 = info6;
	}

	public String getInfo7() {
		return info7;
	}

	public void setInfo7(String info7) {
		this.info7 = info7;
	}

	public String getInfo8() {
		return info8;
	}

	public void setInfo8(String info8) {
		this.info8 = info8;
	}

	public String getInfo9() {
		return info9;
	}

	public void setInfo9(String info9) {
		this.info9 = info9;
	}

	public String getInfo10() {
		return info10;
	}

	public void setInfo10(String info10) {
		this.info10 = info10;
	}

	public String getInfo11() {
		return info11;
	}

	public void setInfo11(String info11) {
		this.info11 = info11;
	}

	public String getInfo12() {
		return info12;
	}

	public void setInfo12(String info12) {
		this.info12 = info12;
	}

	public String getInfo13() {
		return info13;
	}

	public void setInfo13(String info13) {
		this.info13 = info13;
	}

	public String getInfo14() {
		return info14;
	}

	public void setInfo14(String info14) {
		this.info14 = info14;
	}

	public String getInfo15() {
		return info15;
	}

	public void setInfo15(String info15) {
		this.info15 = info15;
	}

	public String getFots_office() {
		return fots_office;
	}

	public void setFots_office(String fots_office) {
		this.fots_office = fots_office;
	}

	public String getFots_agent() {
		return fots_agent;
	}

	public void setFots_agent(String fots_agent) {
		this.fots_agent = fots_agent;
	}

	public String getFots_sales_date() {
		return fots_sales_date;
	}

	public void setFots_sales_date(String fots_sales_date) {
		this.fots_sales_date = fots_sales_date;
	}

	public String getFots_info1() {
		return fots_info1;
	}

	public void setFots_info1(String fots_info1) {
		this.fots_info1 = fots_info1;
	}

	public String getFots_info2() {
		return fots_info2;
	}

	public void setFots_info2(String fots_info2) {
		this.fots_info2 = fots_info2;
	}

	public String getFots_info3() {
		return fots_info3;
	}

	public void setFots_info3(String fots_info3) {
		this.fots_info3 = fots_info3;
	}

	public String getFots_info4() {
		return fots_info4;
	}

	public void setFots_info4(String fots_info4) {
		this.fots_info4 = fots_info4;
	}

	public String getFots_info5() {
		return fots_info5;
	}

	public void setFots_info5(String fots_info5) {
		this.fots_info5 = fots_info5;
	}

	public String getFots_info6() {
		return fots_info6;
	}

	public void setFots_info6(String fots_info6) {
		this.fots_info6 = fots_info6;
	}

	public String getFots_info7() {
		return fots_info7;
	}

	public void setFots_info7(String fots_info7) {
		this.fots_info7 = fots_info7;
	}

	public String getFots_info8() {
		return fots_info8;
	}

	public void setFots_info8(String fots_info8) {
		this.fots_info8 = fots_info8;
	}

	public String getFots_info9() {
		return fots_info9;
	}

	public void setFots_info9(String fots_info9) {
		this.fots_info9 = fots_info9;
	}

	public String getFots_info10() {
		return fots_info10;
	}

	public void setFots_info10(String fots_info10) {
		this.fots_info10 = fots_info10;
	}

	public String getFots_info11() {
		return fots_info11;
	}

	public void setFots_info11(String fots_info11) {
		this.fots_info11 = fots_info11;
	}

	public String getFots_info12() {
		return fots_info12;
	}

	public void setFots_info12(String fots_info12) {
		this.fots_info12 = fots_info12;
	}

	public String getFots_info13() {
		return fots_info13;
	}

	public void setFots_info13(String fots_info13) {
		this.fots_info13 = fots_info13;
	}

	public String getFots_info14() {
		return fots_info14;
	}

	public void setFots_info14(String fots_info14) {
		this.fots_info14 = fots_info14;
	}

	public String getFots_info15() {
		return fots_info15;
	}

	public void setFots_info15(String fots_info15) {
		this.fots_info15 = fots_info15;
	}

	public String getWeb_info1() {
		return web_info1;
	}

	public void setWeb_info1(String web_info1) {
		this.web_info1 = web_info1;
	}

	public String getWeb_info2() {
		return web_info2;
	}

	public void setWeb_info2(String web_info2) {
		this.web_info2 = web_info2;
	}

	public String getWeb_info3() {
		return web_info3;
	}

	public void setWeb_info3(String web_info3) {
		this.web_info3 = web_info3;
	}

	public String getWeb_info4() {
		return web_info4;
	}

	public void setWeb_info4(String web_info4) {
		this.web_info4 = web_info4;
	}

	public String getWeb_info5() {
		return web_info5;
	}

	public void setWeb_info5(String web_info5) {
		this.web_info5 = web_info5;
	}

	public String getWeb_info6() {
		return web_info6;
	}

	public void setWeb_info6(String web_info6) {
		this.web_info6 = web_info6;
	}

	public String getWeb_info7() {
		return web_info7;
	}

	public void setWeb_info7(String web_info7) {
		this.web_info7 = web_info7;
	}

	public String getWeb_info8() {
		return web_info8;
	}

	public void setWeb_info8(String web_info8) {
		this.web_info8 = web_info8;
	}

	public String getWeb_info9() {
		return web_info9;
	}

	public void setWeb_info9(String web_info9) {
		this.web_info9 = web_info9;
	}

	public String getWeb_info10() {
		return web_info10;
	}

	public void setWeb_info10(String web_info10) {
		this.web_info10 = web_info10;
	}

	public String getWeb_info11() {
		return web_info11;
	}

	public void setWeb_info11(String web_info11) {
		this.web_info11 = web_info11;
	}

	public String getWeb_info12() {
		return web_info12;
	}

	public void setWeb_info12(String web_info12) {
		this.web_info12 = web_info12;
	}

	public String getWeb_info13() {
		return web_info13;
	}

	public void setWeb_info13(String web_info13) {
		this.web_info13 = web_info13;
	}

	public String getWeb_info14() {
		return web_info14;
	}

	public void setWeb_info14(String web_info14) {
		this.web_info14 = web_info14;
	}

	public String getWeb_info15() {
		return web_info15;
	}

	public void setWeb_info15(String web_info15) {
		this.web_info15 = web_info15;
	}

	public String getLock_fots() {
		return lock_fots;
	}

	public void setLock_fots(String lock_fots) {
		this.lock_fots = lock_fots;
	}

	public String getFots_new_format() {
		return fots_new_format;
	}

	public void setFots_new_format(String fots_new_format) {
		this.fots_new_format = fots_new_format;
	}

	public String getInfo1_bill_display() {
		return info1_bill_display;
	}

	public void setInfo1_bill_display(String info1_bill_display) {
		this.info1_bill_display = info1_bill_display;
	}

	public String getInfo2_bill_display() {
		return info2_bill_display;
	}

	public void setInfo2_bill_display(String info2_bill_display) {
		this.info2_bill_display = info2_bill_display;
	}

	public String getInfo3_bill_display() {
		return info3_bill_display;
	}

	public void setInfo3_bill_display(String info3_bill_display) {
		this.info3_bill_display = info3_bill_display;
	}

	public String getInfo4_bill_display() {
		return info4_bill_display;
	}

	public void setInfo4_bill_display(String info4_bill_display) {
		this.info4_bill_display = info4_bill_display;
	}

	public String getInfo5_bill_display() {
		return info5_bill_display;
	}

	public void setInfo5_bill_display(String info5_bill_display) {
		this.info5_bill_display = info5_bill_display;
	}

	public String getInfo6_bill_display() {
		return info6_bill_display;
	}

	public void setInfo6_bill_display(String info6_bill_display) {
		this.info6_bill_display = info6_bill_display;
	}

	public String getInfo7_bill_display() {
		return info7_bill_display;
	}

	public void setInfo7_bill_display(String info7_bill_display) {
		this.info7_bill_display = info7_bill_display;
	}

	public String getInfo8_bill_display() {
		return info8_bill_display;
	}

	public void setInfo8_bill_display(String info8_bill_display) {
		this.info8_bill_display = info8_bill_display;
	}

	public String getInfo9_bill_display() {
		return info9_bill_display;
	}

	public void setInfo9_bill_display(String info9_bill_display) {
		this.info9_bill_display = info9_bill_display;
	}

	public String getInfo10_bill_display() {
		return info10_bill_display;
	}

	public void setInfo10_bill_display(String info10_bill_display) {
		this.info10_bill_display = info10_bill_display;
	}

	public String getInfo11_bill_display() {
		return info11_bill_display;
	}

	public void setInfo11_bill_display(String info11_bill_display) {
		this.info11_bill_display = info11_bill_display;
	}

	public String getInfo12_bill_display() {
		return info12_bill_display;
	}

	public void setInfo12_bill_display(String info12_bill_display) {
		this.info12_bill_display = info12_bill_display;
	}

	public String getInfo13_bill_display() {
		return info13_bill_display;
	}

	public void setInfo13_bill_display(String info13_bill_display) {
		this.info13_bill_display = info13_bill_display;
	}

	public String getInfo14_bill_display() {
		return info14_bill_display;
	}

	public void setInfo14_bill_display(String info14_bill_display) {
		this.info14_bill_display = info14_bill_display;
	}

	public String getInfo15_bill_display() {
		return info15_bill_display;
	}

	public void setInfo15_bill_display(String info15_bill_display) {
		this.info15_bill_display = info15_bill_display;
	}

	public String getInfo1_pay_display() {
		return info1_pay_display;
	}

	public void setInfo1_pay_display(String info1_pay_display) {
		this.info1_pay_display = info1_pay_display;
	}

	public String getInfo2_pay_display() {
		return info2_pay_display;
	}

	public void setInfo2_pay_display(String info2_pay_display) {
		this.info2_pay_display = info2_pay_display;
	}

	public String getInfo3_pay_display() {
		return info3_pay_display;
	}

	public void setInfo3_pay_display(String info3_pay_display) {
		this.info3_pay_display = info3_pay_display;
	}

	public String getInfo4_pay_display() {
		return info4_pay_display;
	}

	public void setInfo4_pay_display(String info4_pay_display) {
		this.info4_pay_display = info4_pay_display;
	}

	public String getInfo5_pay_display() {
		return info5_pay_display;
	}

	public void setInfo5_pay_display(String info5_pay_display) {
		this.info5_pay_display = info5_pay_display;
	}

	public String getInfo6_pay_display() {
		return info6_pay_display;
	}

	public void setInfo6_pay_display(String info6_pay_display) {
		this.info6_pay_display = info6_pay_display;
	}

	public String getInfo7_pay_display() {
		return info7_pay_display;
	}

	public void setInfo7_pay_display(String info7_pay_display) {
		this.info7_pay_display = info7_pay_display;
	}

	public String getInfo8_pay_display() {
		return info8_pay_display;
	}

	public void setInfo8_pay_display(String info8_pay_display) {
		this.info8_pay_display = info8_pay_display;
	}

	public String getInfo9_pay_display() {
		return info9_pay_display;
	}

	public void setInfo9_pay_display(String info9_pay_display) {
		this.info9_pay_display = info9_pay_display;
	}

	public String getInfo10_pay_display() {
		return info10_pay_display;
	}

	public void setInfo10_pay_display(String info10_pay_display) {
		this.info10_pay_display = info10_pay_display;
	}

	public String getInfo11_pay_display() {
		return info11_pay_display;
	}

	public void setInfo11_pay_display(String info11_pay_display) {
		this.info11_pay_display = info11_pay_display;
	}

	public String getInfo12_pay_display() {
		return info12_pay_display;
	}

	public void setInfo12_pay_display(String info12_pay_display) {
		this.info12_pay_display = info12_pay_display;
	}

	public String getInfo13_pay_display() {
		return info13_pay_display;
	}

	public void setInfo13_pay_display(String info13_pay_display) {
		this.info13_pay_display = info13_pay_display;
	}

	public String getInfo14_pay_display() {
		return info14_pay_display;
	}

	public void setInfo14_pay_display(String info14_pay_display) {
		this.info14_pay_display = info14_pay_display;
	}

	public String getInfo15_pay_display() {
		return info15_pay_display;
	}

	public void setInfo15_pay_display(String info15_pay_display) {
		this.info15_pay_display = info15_pay_display;
	}

	public String getAppeal_type() {
		return appeal_type;
	}

	public void setAppeal_type(String appeal_type) {
		this.appeal_type = appeal_type;
	}

	public Timestamp getAppeal_start_date() {
		return appeal_start_date;
	}

	public void setAppeal_start_date(Timestamp appeal_start_date) {
		this.appeal_start_date = appeal_start_date;
	}

	public String getAppeal_pay_periods() {
		return appeal_pay_periods;
	}

	public void setAppeal_pay_periods(String appeal_pay_periods) {
		this.appeal_pay_periods = appeal_pay_periods;
	}

	public String getFots_validation() {
		return fots_validation;
	}

	public void setFots_validation(String fots_validation) {
		this.fots_validation = fots_validation;
	}

	public String getCompliance_req() {
		return compliance_req;
	}

	public void setCompliance_req(String compliance_req) {
		this.compliance_req = compliance_req;
	}

	public String getAllow_tpv() {
		return allow_tpv;
	}

	public void setAllow_tpv(String allow_tpv) {
		this.allow_tpv = allow_tpv;
	}

	public String getSecondary_match() {
		return secondary_match;
	}

	public void setSecondary_match(String secondary_match) {
		this.secondary_match = secondary_match;
	}

	public String getInfo1_secondary() {
		return info1_secondary;
	}

	public void setInfo1_secondary(String info1_secondary) {
		this.info1_secondary = info1_secondary;
	}

	public String getInfo2_secondary() {
		return info2_secondary;
	}

	public void setInfo2_secondary(String info2_secondary) {
		this.info2_secondary = info2_secondary;
	}

	public String getInfo3_secondary() {
		return info3_secondary;
	}

	public void setInfo3_secondary(String info3_secondary) {
		this.info3_secondary = info3_secondary;
	}

	public String getInfo4_secondary() {
		return info4_secondary;
	}

	public void setInfo4_secondary(String info4_secondary) {
		this.info4_secondary = info4_secondary;
	}

	public String getInfo5_secondary() {
		return info5_secondary;
	}

	public void setInfo5_secondary(String info5_secondary) {
		this.info5_secondary = info5_secondary;
	}

	public String getAutomatach_code() {
		return automatach_code;
	}

	public void setAutomatach_code(String automatach_code) {
		this.automatach_code = automatach_code;
	}

	public String getAutomatch_code() {
		return automatch_code;
	}

	public void setAutomatch_code(String automatch_code) {
		this.automatch_code = automatch_code;
	}

	public String getAttribute1_title() {
		return attribute1_title;
	}

	public void setAttribute1_title(String attribute1_title) {
		this.attribute1_title = attribute1_title;
	}

	public String getTpv_message1() {
		return tpv_message1;
	}

	public void setTpv_message1(String tpv_message1) {
		this.tpv_message1 = tpv_message1;
	}

	public String getBill_ignored_rej() {
		return bill_ignored_rej;
	}

	public void setBill_ignored_rej(String bill_ignored_rej) {
		this.bill_ignored_rej = bill_ignored_rej;
	}

	public String getAuto_submit() {
		return auto_submit;
	}

	public void setAuto_submit(String auto_submit) {
		this.auto_submit = auto_submit;
	}

	public String getAuto_submit_email() {
		return auto_submit_email;
	}

	public void setAuto_submit_email(String auto_submit_email) {
		this.auto_submit_email = auto_submit_email;
	}

	public String getAuto_submit_ftp() {
		return auto_submit_ftp;
	}

	public void setAuto_submit_ftp(String auto_submit_ftp) {
		this.auto_submit_ftp = auto_submit_ftp;
	}

	public String getAuto_submit_email_destination() {
		return auto_submit_email_destination;
	}

	public void setAuto_submit_email_destination(
			String auto_submit_email_destination) {
		this.auto_submit_email_destination = auto_submit_email_destination;
	}

	public String getAuto_submit_ftp_destination() {
		return auto_submit_ftp_destination;
	}

	public void setAuto_submit_ftp_destination(
			String auto_submit_ftp_destination) {
		this.auto_submit_ftp_destination = auto_submit_ftp_destination;
	}

	public String getAuto_submit_time() {
		return auto_submit_time;
	}

	public void setAuto_submit_time(String auto_submit_time) {
		this.auto_submit_time = auto_submit_time;
	}

	public Timestamp getAuto_submit_last_run() {
		return auto_submit_last_run;
	}

	public void setAuto_submit_last_run(Timestamp auto_submit_last_run) {
		this.auto_submit_last_run = auto_submit_last_run;
	}

	public String getEdit_staging() {
		return edit_staging;
	}

	public void setEdit_staging(String edit_staging) {
		this.edit_staging = edit_staging;
	}

	public String getCydcor_times_cutoff() {
		return cydcor_times_cutoff;
	}

	public void setCydcor_times_cutoff(String cydcor_times_cutoff) {
		this.cydcor_times_cutoff = cydcor_times_cutoff;
	}

	public String getPerson_worked_info_req() {
		return person_worked_info_req;
	}

	public void setPerson_worked_info_req(String person_worked_info_req) {
		this.person_worked_info_req = person_worked_info_req;
	}

	public String getUse_person_worked_info() {
		return use_person_worked_info;
	}

	public void setUse_person_worked_info(String use_person_worked_info) {
		this.use_person_worked_info = use_person_worked_info;
	}

	public String getAttribute1_description() {
		return attribute1_description;
	}

	public void setAttribute1_description(String attribute1_description) {
		this.attribute1_description = attribute1_description;
	}

	public String getCampaign_info_required() {
		return campaign_info_required;
	}

	public void setCampaign_info_required(String campaign_info_required) {
		this.campaign_info_required = campaign_info_required;
	}

	public String getMask_test() {
		return mask_test;
	}

	public void setMask_test(String mask_test) {
		this.mask_test = mask_test;
	}

	public String getAttribute1_description_2() {
		return attribute1_description_2;
	}

	public void setAttribute1_description_2(String attribute1_description_2) {
		this.attribute1_description_2 = attribute1_description_2;
	}

	public String getAttribute1_description_3() {
		return attribute1_description_3;
	}

	public void setAttribute1_description_3(String attribute1_description_3) {
		this.attribute1_description_3 = attribute1_description_3;
	}

	public String getAttribute1_description_4() {
		return attribute1_description_4;
	}

	public void setAttribute1_description_4(String attribute1_description_4) {
		this.attribute1_description_4 = attribute1_description_4;
	}

	public String getAttribute1_description_5() {
		return attribute1_description_5;
	}

	public void setAttribute1_description_5(String attribute1_description_5) {
		this.attribute1_description_5 = attribute1_description_5;
	}

	public String getAttribute1_description_6() {
		return attribute1_description_6;
	}

	public void setAttribute1_description_6(String attribute1_description_6) {
		this.attribute1_description_6 = attribute1_description_6;
	}

	public String getAttribute1_description_7() {
		return attribute1_description_7;
	}

	public void setAttribute1_description_7(String attribute1_description_7) {
		this.attribute1_description_7 = attribute1_description_7;
	}

	public String getAttribute1_description_8() {
		return attribute1_description_8;
	}

	public void setAttribute1_description_8(String attribute1_description_8) {
		this.attribute1_description_8 = attribute1_description_8;
	}

	public String getAttribute1_description_9() {
		return attribute1_description_9;
	}

	public void setAttribute1_description_9(String attribute1_description_9) {
		this.attribute1_description_9 = attribute1_description_9;
	}

	public String getAttribute1_description_10() {
		return attribute1_description_10;
	}

	public void setAttribute1_description_10(String attribute1_description_10) {
		this.attribute1_description_10 = attribute1_description_10;
	}

	public String getAuto_upload_staging() {
		return auto_upload_staging;
	}

	public void setAuto_upload_staging(String auto_upload_staging) {
		this.auto_upload_staging = auto_upload_staging;
	}

	public String getStaging_ftp_path() {
		return staging_ftp_path;
	}

	public void setStaging_ftp_path(String staging_ftp_path) {
		this.staging_ftp_path = staging_ftp_path;
	}

	public String getFots_grand_total() {
		return fots_grand_total;
	}

	public void setFots_grand_total(String fots_grand_total) {
		this.fots_grand_total = fots_grand_total;
	}

	public String getFots_entry_type() {
		return fots_entry_type;
	}

	public void setFots_entry_type(String fots_entry_type) {
		this.fots_entry_type = fots_entry_type;
	}

	public String getDisplay_absent() {
		return display_absent;
	}

	public void setDisplay_absent(String display_absent) {
		this.display_absent = display_absent;
	}

	public String getReject_threshold_perc() {
		return reject_threshold_perc;
	}

	public void setReject_threshold_perc(String reject_threshold_perc) {
		this.reject_threshold_perc = reject_threshold_perc;
	}

	public String getNew_upload_window_yn() {
		return new_upload_window_yn;
	}

	public void setNew_upload_window_yn(String new_upload_window_yn) {
		this.new_upload_window_yn = new_upload_window_yn;
	}

	public String getUse_reject_logic_yn() {
		return use_reject_logic_yn;
	}

	public void setUse_reject_logic_yn(String use_reject_logic_yn) {
		this.use_reject_logic_yn = use_reject_logic_yn;
	}

	public String getDate_to_use_new_data() {
		return date_to_use_new_data;
	}

	public void setDate_to_use_new_data(String date_to_use_new_data) {
		this.date_to_use_new_data = date_to_use_new_data;
	}

	public String getDate_to_use_existing_data() {
		return date_to_use_existing_data;
	}

	public void setDate_to_use_existing_data(String date_to_use_existing_data) {
		this.date_to_use_existing_data = date_to_use_existing_data;
	}

	public String getCalculate_days_to_use() {
		return calculate_days_to_use;
	}

	public void setCalculate_days_to_use(String calculate_days_to_use) {
		this.calculate_days_to_use = calculate_days_to_use;
	}

	public String getNumber_of_days_reject_valid() {
		return number_of_days_reject_valid;
	}

	public void setNumber_of_days_reject_valid(
			String number_of_days_reject_valid) {
		this.number_of_days_reject_valid = number_of_days_reject_valid;
	}

	public Timestamp getNew_data_process_date() {
		return new_data_process_date;
	}

	public void setNew_data_process_date(Timestamp new_data_process_date) {
		this.new_data_process_date = new_data_process_date;
	}

	public String getLast_worked_backfill() {
		return last_worked_backfill;
	}

	public void setLast_worked_backfill(String last_worked_backfill) {
		this.last_worked_backfill = last_worked_backfill;
	}

	public String getProduct_group_match_flag() {
		return product_group_match_flag;
	}

	public void setProduct_group_match_flag(String product_group_match_flag) {
		this.product_group_match_flag = product_group_match_flag;
	}

	public String getBy_row() {
		return by_row;
	}

	public void setBy_row(String by_row) {
		this.by_row = by_row;
	}

	public String getMatch_dup_by_sales_date() {
		return match_dup_by_sales_date;
	}

	public void setMatch_dup_by_sales_date(String match_dup_by_sales_date) {
		this.match_dup_by_sales_date = match_dup_by_sales_date;
	}

	public String getOffice_info1_unique() {
		return office_info1_unique;
	}

	public void setOffice_info1_unique(String office_info1_unique) {
		this.office_info1_unique = office_info1_unique;
	}

	public String getOffice_info2_unique() {
		return office_info2_unique;
	}

	public void setOffice_info2_unique(String office_info2_unique) {
		this.office_info2_unique = office_info2_unique;
	}

	public String getOffice_info3_unique() {
		return office_info3_unique;
	}

	public void setOffice_info3_unique(String office_info3_unique) {
		this.office_info3_unique = office_info3_unique;
	}

	public String getOffice_info4_unique() {
		return office_info4_unique;
	}

	public void setOffice_info4_unique(String office_info4_unique) {
		this.office_info4_unique = office_info4_unique;
	}

	public String getOffice_info5_unique() {
		return office_info5_unique;
	}

	public void setOffice_info5_unique(String office_info5_unique) {
		this.office_info5_unique = office_info5_unique;
	}

	public String getApply_sysdate_on_null_dt_yn() {
		return apply_sysdate_on_null_dt_yn;
	}

	public void setApply_sysdate_on_null_dt_yn(
			String apply_sysdate_on_null_dt_yn) {
		this.apply_sysdate_on_null_dt_yn = apply_sysdate_on_null_dt_yn;
	}

	public String getDo_not_update_camp_override() {
		return do_not_update_camp_override;
	}

	public void setDo_not_update_camp_override(
			String do_not_update_camp_override) {
		this.do_not_update_camp_override = do_not_update_camp_override;
	}

	public String getStaging_add_prod_to_app() {
		return staging_add_prod_to_app;
	}

	public void setStaging_add_prod_to_app(String staging_add_prod_to_app) {
		this.staging_add_prod_to_app = staging_add_prod_to_app;
	}

	public String getInfo6_dup() {
		return info6_dup;
	}

	public void setInfo6_dup(String info6_dup) {
		this.info6_dup = info6_dup;
	}

	public String getInfo7_dup() {
		return info7_dup;
	}

	public void setInfo7_dup(String info7_dup) {
		this.info7_dup = info7_dup;
	}

	public String getInfo8_dup() {
		return info8_dup;
	}

	public void setInfo8_dup(String info8_dup) {
		this.info8_dup = info8_dup;
	}

	public String getInfo9_dup() {
		return info9_dup;
	}

	public void setInfo9_dup(String info9_dup) {
		this.info9_dup = info9_dup;
	}

	public String getInfo10_dup() {
		return info10_dup;
	}

	public void setInfo10_dup(String info10_dup) {
		this.info10_dup = info10_dup;
	}

	public String getInfo11_dup() {
		return info11_dup;
	}

	public void setInfo11_dup(String info11_dup) {
		this.info11_dup = info11_dup;
	}

	public String getInfo12_dup() {
		return info12_dup;
	}

	public void setInfo12_dup(String info12_dup) {
		this.info12_dup = info12_dup;
	}

	public String getInfo13_dup() {
		return info13_dup;
	}

	public void setInfo13_dup(String info13_dup) {
		this.info13_dup = info13_dup;
	}

	public String getInfo14_dup() {
		return info14_dup;
	}

	public void setInfo14_dup(String info14_dup) {
		this.info14_dup = info14_dup;
	}

	public String getInfo15_dup() {
		return info15_dup;
	}

	public void setInfo15_dup(String info15_dup) {
		this.info15_dup = info15_dup;
	}

	public String getUse_reject_logic_yn_b() {
		return use_reject_logic_yn_b;
	}

	public void setUse_reject_logic_yn_b(String use_reject_logic_yn_b) {
		this.use_reject_logic_yn_b = use_reject_logic_yn_b;
	}

	public String getPay_bill_display_rb() {
		return pay_bill_display_rb;
	}

	public void setPay_bill_display_rb(String pay_bill_display_rb) {
		this.pay_bill_display_rb = pay_bill_display_rb;
	}

	public String getDate_to_use_new_data_b() {
		return date_to_use_new_data_b;
	}

	public void setDate_to_use_new_data_b(String date_to_use_new_data_b) {
		this.date_to_use_new_data_b = date_to_use_new_data_b;
	}

	public String getDate_to_use_existing_data_b() {
		return date_to_use_existing_data_b;
	}

	public void setDate_to_use_existing_data_b(
			String date_to_use_existing_data_b) {
		this.date_to_use_existing_data_b = date_to_use_existing_data_b;
	}

	public String getCalculate_days_to_use_b() {
		return calculate_days_to_use_b;
	}

	public void setCalculate_days_to_use_b(String calculate_days_to_use_b) {
		this.calculate_days_to_use_b = calculate_days_to_use_b;
	}

	public String getNumber_of_days_reject_valid_b() {
		return number_of_days_reject_valid_b;
	}

	public void setNumber_of_days_reject_valid_b(
			String number_of_days_reject_valid_b) {
		this.number_of_days_reject_valid_b = number_of_days_reject_valid_b;
	}

	public String getApply_sysdate_on_null_dt_yn_b() {
		return apply_sysdate_on_null_dt_yn_b;
	}

	public void setApply_sysdate_on_null_dt_yn_b(
			String apply_sysdate_on_null_dt_yn_b) {
		this.apply_sysdate_on_null_dt_yn_b = apply_sysdate_on_null_dt_yn_b;
	}

	public String getCampaign_country() {
		return campaign_country;
	}

	public void setCampaign_country(String campaign_country) {
		this.campaign_country = campaign_country;
	}

	public String getWeb_appeal_all() {
		return web_appeal_all;
	}

	public void setWeb_appeal_all(String web_appeal_all) {
		this.web_appeal_all = web_appeal_all;
	}

	public String getNew_bill_pay_manager_yn() {
		return new_bill_pay_manager_yn;
	}

	public void setNew_bill_pay_manager_yn(String new_bill_pay_manager_yn) {
		this.new_bill_pay_manager_yn = new_bill_pay_manager_yn;
	}

	public String getBackfill_days() {
		return backfill_days;
	}

	public void setBackfill_days(String backfill_days) {
		this.backfill_days = backfill_days;
	}

	public String getBackfill_less_equal_1_display() {
		return backfill_less_equal_1_display;
	}

	public void setBackfill_less_equal_1_display(
			String backfill_less_equal_1_display) {
		this.backfill_less_equal_1_display = backfill_less_equal_1_display;
	}

	public String getBackfill_less_equal_2_display() {
		return backfill_less_equal_2_display;
	}

	public void setBackfill_less_equal_2_display(
			String backfill_less_equal_2_display) {
		this.backfill_less_equal_2_display = backfill_less_equal_2_display;
	}

	public String getBackfill_less_equal_3_display() {
		return backfill_less_equal_3_display;
	}

	public void setBackfill_less_equal_3_display(
			String backfill_less_equal_3_display) {
		this.backfill_less_equal_3_display = backfill_less_equal_3_display;
	}

	public String getBackfill_less_equal_4_display() {
		return backfill_less_equal_4_display;
	}

	public void setBackfill_less_equal_4_display(
			String backfill_less_equal_4_display) {
		this.backfill_less_equal_4_display = backfill_less_equal_4_display;
	}

	public String getBackfill_less_equal_5_display() {
		return backfill_less_equal_5_display;
	}

	public void setBackfill_less_equal_5_display(
			String backfill_less_equal_5_display) {
		this.backfill_less_equal_5_display = backfill_less_equal_5_display;
	}

	public String getBackfill_greater_1_display() {
		return backfill_greater_1_display;
	}

	public void setBackfill_greater_1_display(String backfill_greater_1_display) {
		this.backfill_greater_1_display = backfill_greater_1_display;
	}

	public String getBackfill_greater_2_display() {
		return backfill_greater_2_display;
	}

	public void setBackfill_greater_2_display(String backfill_greater_2_display) {
		this.backfill_greater_2_display = backfill_greater_2_display;
	}

	public String getBackfill_greater_3_display() {
		return backfill_greater_3_display;
	}

	public void setBackfill_greater_3_display(String backfill_greater_3_display) {
		this.backfill_greater_3_display = backfill_greater_3_display;
	}

	public String getBackfill_greater_4_display() {
		return backfill_greater_4_display;
	}

	public void setBackfill_greater_4_display(String backfill_greater_4_display) {
		this.backfill_greater_4_display = backfill_greater_4_display;
	}

	public String getBackfill_greater_5_display() {
		return backfill_greater_5_display;
	}

	public void setBackfill_greater_5_display(String backfill_greater_5_display) {
		this.backfill_greater_5_display = backfill_greater_5_display;
	}

	public String getBackfill_inactive_reps() {
		return backfill_inactive_reps;
	}

	public void setBackfill_inactive_reps(String backfill_inactive_reps) {
		this.backfill_inactive_reps = backfill_inactive_reps;
	}

	public String getBackfill_recent_assigned_rep() {
		return backfill_recent_assigned_rep;
	}

	public void setBackfill_recent_assigned_rep(
			String backfill_recent_assigned_rep) {
		this.backfill_recent_assigned_rep = backfill_recent_assigned_rep;
	}

	public String getBackfill_inactive_offices() {
		return backfill_inactive_offices;
	}

	public void setBackfill_inactive_offices(String backfill_inactive_offices) {
		this.backfill_inactive_offices = backfill_inactive_offices;
	}

	public String getBackfill_existing_office_rep() {
		return backfill_existing_office_rep;
	}

	public void setBackfill_existing_office_rep(
			String backfill_existing_office_rep) {
		this.backfill_existing_office_rep = backfill_existing_office_rep;
	}

	public String getCampaign_type() {
		return campaign_type;
	}

	public void setCampaign_type(String campaign_type) {
		this.campaign_type = campaign_type;
	}

	public String getProduct_match_flag() {
		return product_match_flag;
	}

	public void setProduct_match_flag(String product_match_flag) {
		this.product_match_flag = product_match_flag;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getTue() {
		return tue;
	}

	public void setTue(String tue) {
		this.tue = tue;
	}

	public String getWed() {
		return wed;
	}

	public void setWed(String wed) {
		this.wed = wed;
	}

	public String getThu() {
		return thu;
	}

	public void setThu(String thu) {
		this.thu = thu;
	}

	public String getFri() {
		return fri;
	}

	public void setFri(String fri) {
		this.fri = fri;
	}

	public String getSat() {
		return sat;
	}

	public void setSat(String sat) {
		this.sat = sat;
	}

	public String getSun() {
		return sun;
	}

	public void setSun(String sun) {
		this.sun = sun;
	}

	public String getDo_not_report_ir_rop() {
		return do_not_report_ir_rop;
	}

	public void setDo_not_report_ir_rop(String do_not_report_ir_rop) {
		this.do_not_report_ir_rop = do_not_report_ir_rop;
	}

	public String getUpload_dw_display_type() {
		return upload_dw_display_type;
	}

	public void setUpload_dw_display_type(String upload_dw_display_type) {
		this.upload_dw_display_type = upload_dw_display_type;
	}

	public String getProcess_status() {
		return process_status;
	}

	public void setProcess_status(String process_status) {
		this.process_status = process_status;
	}

	public Timestamp getProcess_date() {
		return process_date;
	}

	public void setProcess_date(Timestamp process_date) {
		this.process_date = process_date;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}

	public long getRetry_count() {
		return retry_count;
	}

	public void setRetry_count(long retry_count) {
		this.retry_count = retry_count;
	}
}