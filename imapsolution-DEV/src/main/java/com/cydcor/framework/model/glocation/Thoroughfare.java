package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Thoroughfare")
public class Thoroughfare {
	private String ThoroughfareName;

	/**
	 * @return the thoroughfareName
	 */
	public String getThoroughfareName() {
		return ThoroughfareName;
	}

	/**
	 * @param thoroughfareName
	 *            the thoroughfareName to set
	 */
	public void setThoroughfareName(String thoroughfareName) {
		ThoroughfareName = thoroughfareName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Thoroughfare [ThoroughfareName=" + ThoroughfareName + "]";
	}

}
