/**
 * 
 */
package com.cydcor.framework.model.glocation;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author ashwin kumar
 * 
 */
@XStreamAlias("DependentLocality")
public class DependentLocality {

    private String DependentLocalityName = "";
    private Thoroughfare Thoroughfare;
    private PostalCode PostalCode;
    private String AddressLine;

    public String getDependentLocalityName() {
	return DependentLocalityName;
    }

    public void setDependentLocalityName(String dependentLocalityName) {
	DependentLocalityName = dependentLocalityName;
    }

    public Thoroughfare getThoroughfare() {
	return Thoroughfare;
    }

    public void setThoroughfare(Thoroughfare thoroughfare) {
	Thoroughfare = thoroughfare;
    }

    public PostalCode getPostalCode() {
	return PostalCode;
    }

    public void setPostalCode(PostalCode postalCode) {
	PostalCode = postalCode;
    }

    public void setAddressLine(String addressLine) {
	AddressLine = addressLine;
    }

    public String getAddressLine() {
	return AddressLine;
    }
}
