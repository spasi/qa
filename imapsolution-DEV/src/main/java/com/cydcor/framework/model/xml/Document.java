package com.cydcor.framework.model.xml;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Document")
public class Document {
	
	private String name;
	private String description;
	
	@XStreamImplicit
	private List<Style> styleList = new ArrayList<Style>();
	
	@XStreamImplicit
	private List<Placemark> placeMarkList = new ArrayList<Placemark>();
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the styleList
	 */
	public List<Style> getStyleList() {
		return styleList;
	}

	/**
	 * @param styleList the styleList to set
	 */
	public void setStyleList(List<Style> styleList) {
		this.styleList = styleList;
	}

	/**
	 * @return the placeMarkList
	 */
	public List<Placemark> getPlaceMarkList() {
		return placeMarkList;
	}

	/**
	 * @param placeMarkList the placeMarkList to set
	 */
	public void setPlaceMarkList(List<Placemark> placeMarkList) {
		this.placeMarkList = placeMarkList;
	}

	
	
}
