/**
 * 
 */
package com.cydcor.framework.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This Object Represents Any Data Object That is Loaded from database. Note:
 * This represents Logical Table Name Registered in database if loaded from
 * dynamic sql
 * 
 * @author ashwin
 * 
 */
public class DBEntityComponent extends BaseDataObject {

	private Long id = 0L;
	private String entityName = "";
	private String entityDescription = "";
	private String entityType = "";

	public static interface EntityTypes {
		public static String TABLE = "TABLE";
		public static String SQL = "SQL";
	}

	private String entityValue = "";
	private String schemaName = "";

	// We are not supporting composite Keys for now
	private String primaryKey = "";

	// To Store Column Types Derived From Database Queries
	private Map<String, ColumnMetadata> columnTypeMetadata = new HashMap<String, ColumnMetadata>();

	private List<Relation> relations = new ArrayList<Relation>();

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the entityName
	 */
	public String getEntityName() {
		return entityName;
	}

	/**
	 * @param entityName
	 *            the entityName to set
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @return the entityDescription
	 */
	public String getEntityDescription() {
		return entityDescription;
	}

	/**
	 * @param entityDescription
	 *            the entityDescription to set
	 */
	public void setEntityDescription(String entityDescription) {
		this.entityDescription = entityDescription;
	}

	/**
	 * @return the columnTypeMetadata
	 */
	public Map<String, ColumnMetadata> getColumnTypeMetadata() {
		return columnTypeMetadata;
	}

	/**
	 * @param columnTypeMetadata
	 *            the columnTypeMetadata to set
	 */
	public void setColumnTypeMetadata(
			Map<String, ColumnMetadata> columnTypeMetadata) {
		this.columnTypeMetadata = columnTypeMetadata;
	}

	/**
	 * @param columnName
	 * @param metadata
	 */
	public void addColumnMetadata(String columnName, ColumnMetadata metadata) {
		this.columnTypeMetadata.put(columnName, metadata);
	}

	/**
	 * @return the relations
	 */
	public List<Relation> getRelations() {
		return relations;
	}

	/**
	 * @param relations
	 *            the relations to set
	 */
	public void setRelations(List<Relation> relations) {
		this.relations = relations;
	}

	/**
	 * @param relation
	 */
	public void addRelation(Relation relation) {
		this.relations.add(relation);
	}

	/**
	 * @return the primaryKey
	 */
	public String getPrimaryKey() {
		return primaryKey;
	}

	/**
	 * @param primaryKey
	 *            the primaryKey to set
	 */
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * @param entityType
	 *            the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	/**
	 * @return the entityValue
	 */
	public String getEntityValue() {
		return entityValue;
	}

	/**
	 * @param entityValue
	 *            the entityValue to set
	 */
	public void setEntityValue(String entityValue) {
		this.entityValue = entityValue;
	}

	/**
	 * @return the schemaName
	 */
	public String getSchemaName() {
		return schemaName;
	}

	/**
	 * @param schemaName
	 *            the schemaName to set
	 */
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

}
