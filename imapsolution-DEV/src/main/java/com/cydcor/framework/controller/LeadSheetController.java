package com.cydcor.framework.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

public class LeadSheetController extends GenericController {

	@SuppressWarnings("unchecked")
	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

		String pageDefnintion = "leadsheet/LeadSheetDef.html";
		String controlDefnintion = "leadsheet/LeadSheet.html";
		String print = requestWrapper.getParameter("print");
		String disableBackLinks = requestWrapper.getParameter("disableBackLinks");
		String readOnly = requestWrapper.getParameter("readOnlyUser");
		if (StringUtils.isBlank(print)) {
			print = "false";
		} else {
			if (StringUtils.isBlank(disableBackLinks))
				disableBackLinks = "true";
		}
		if (StringUtils.isBlank(uiContext.getControlName())) {
			if (StringUtils.equalsIgnoreCase("false", print)) {
				Map<String, String> myParamMap = new HashMap<String, String>();
				myParamMap.put("noPrint", "true");
				clearOldPage(uiContext, pageDefnintion, myParamMap);
			} else {
				clearOldPage(uiContext, pageDefnintion);
			}

			GenericUIControl iclRepSelector = new GenericUIControl();
			iclRepSelector.setTemplateName(pageDefnintion);
			iclRepSelector.addParameter("iclRepSelector", true);
			iclRepSelector.addParameter("leadSheetId", requestWrapper.getParameter("leadSheetId"));
			iclRepSelector.addParameter("type", requestWrapper.getParameter("type"));
			iclRepSelector.addParameter("print", print);

			if (StringUtils.isBlank(disableBackLinks)) {
				iclRepSelector.addParameter("disableBackLinks", "false");
			}

			if (StringUtils.equalsIgnoreCase("false", print)) {
				iclRepSelector.addParameter("noPrint", "true");
			}

			uiContext.addControl(iclRepSelector);

			GenericUIControl iclRepInfo = new GenericUIControl();
			iclRepInfo.setTemplateName(pageDefnintion);
			iclRepInfo.addParameter("iclRepInfo", true);
			// uiContext.addControl(iclRepInfo);

			// Add Map Control
			GenericUIControl mapControl = new GenericUIControl();
			mapControl.setTemplateName(pageDefnintion);
			mapControl.addParameter("showLeadsMap", true);
			uiContext.addControl(mapControl);

			GenericUIControl iclLeadListTable = new GenericUIControl();
			if (requestWrapper.getParameter("type").equalsIgnoreCase("Saved") && StringUtils.equalsIgnoreCase(print, "false")) {
				iclLeadListTable.addParameter("showPrintButton", true);
				iclLeadListTable.addParameter("readOnlyUser", readOnly);
				List<Object> values = new ArrayList<Object>();
				values.add(requestWrapper.getParameter("leadSheetId"));
				DatabaseResult result = getPlatformService().loadResult(
						"SELECT COUNT(*) FROM IMS.IMS_LEADSHEET WHERE  GETDATE()-CREATED_DATE < 60  and LEADSHEET_ID =?", values);
				if (new Integer(result.getData().get(0).get(0).getValue()) > 0) {
					iclLeadListTable.addParameter("isExpired", false);
				} else {
					iclLeadListTable.addParameter("isExpired", true);
				}

			}

			iclLeadListTable.setTemplateName(pageDefnintion);
			iclLeadListTable.addParameter("iclLeadListTable", true);
			iclLeadListTable.addParameter("type", requestWrapper.getParameter("type"));
			uiContext.addControl(iclLeadListTable);

		} else {
			DatabaseResult dbResult = null;
			String queryString = null;
			if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclRepSelector")) {
				List irInputs = new ArrayList();
				irInputs.add(requestWrapper.getParameter("leadSheetId"));
				irInputs.add(requestWrapper.getParameter("leadSheetId"));
		irInputs.add(requestWrapper.getParameter("leadSheetId"));
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REPS"), irInputs);

				// Add Second Control the Campaign Selector
				GenericUIControl iclRepSelector = new GenericUIControl();
				iclRepSelector.setTemplateName(controlDefnintion);
				iclRepSelector.addParameter("iclRepSelector", true);
				iclRepSelector.addParameter("reps", dbResult);

				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_IMS_DISPOSITION_LIST"));
				List<MapObject> defaultDisp = new ArrayList<MapObject>();
				MapObject object = new MapObject();
				object.setValue("Default");
				defaultDisp.add(object);
				object = new MapObject();
				object.setValue("Not Dispositioned");
				defaultDisp.add(object);
				object = new MapObject();
				object.setValue("assets/images/mapicons/red-dot.png");
				defaultDisp.add(object);

				dbResult.getData().add(0, defaultDisp);
				List returnList = new ArrayList();
				int size = (dbResult.getData().size() / 3) + (dbResult.getData().size() % 3 > 0 ? 1 : 0);
				boolean hasReminder = dbResult.getData().size() % 3 > 0 ? true : false;
				int j = 1;
				for (int i = 1; i <= size; i++) {
					int startIdx = j - 1;
					int lastIdx = i * 3;
					if (i == size && hasReminder)
						lastIdx = dbResult.getData().size();
					returnList.add(dbResult.getData().subList(startIdx, lastIdx));
					j = j + 3;
				}
				iclRepSelector.addParameter("dispositionResult", returnList);

				uiContext.addControl(iclRepSelector);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclRepInfo")) {
				if (StringUtils.isNotBlank(requestWrapper.getParameter("repId"))) {
					List iriInputs = new ArrayList();
					// params.add(requestWrapper.getParameter("campaignSeq"));
					// params.add(requestWrapper.getParameter("officeSeq"));
					iriInputs.add(requestWrapper.getParameter("repId"));
					dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_INFO"), iriInputs);
				} else {
					dbResult = new DatabaseResult();
				}

				// Add Second Control the Campaign Selector
				GenericUIControl iclRepInfo = new GenericUIControl();
				iclRepInfo.setTemplateName(controlDefnintion);
				iclRepInfo.addParameter("iclRepInfo", true);
				iclRepInfo.addParameter("repDetails", dbResult);

				uiContext.addControl(iclRepInfo);
			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "leadListTable")) {

				List irlsInputs = new ArrayList();
				// irlsInputs.add(requestWrapper.getParameter("repId"));
				irlsInputs.add(requestWrapper.getParameter("leadSheetId"));
				irlsInputs.add(requestWrapper.getParameter("leadSheetId"));
		irlsInputs.add(requestWrapper.getParameter("leadSheetId"));
				GenericUIControl tableUIControl = new GenericUIControl();
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_LEAD_SHEET"), irlsInputs);
				tableUIControl.addParameter("result", dbResult);
				irlsInputs.remove(0);
				DatabaseResult countResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_LEAD_SHEET_COUNT"), irlsInputs);
				tableUIControl.addParameter("availLeads", countResult.getData().get(0).get(0).getValue());
				tableUIControl.addParameter("disabledLeads", countResult.getData().get(0).get(1).getValue());
				tableUIControl.addParameter("dispLeads", countResult.getData().get(0).get(2).getValue());
				tableUIControl.addParameter("leadListTable", true);
				tableUIControl.addParameter("type", requestWrapper.getParameter("type"));
				tableUIControl.setTemplateName(controlDefnintion);

				uiContext.addControl(tableUIControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "loadLeadsInMap")) {
				List inputs = new ArrayList();
				// inputs.add(requestWrapper.getParameter("repId"));
				inputs.add(requestWrapper.getParameter("leadSheetId"));
				inputs.add(requestWrapper.getParameter("leadSheetId"));
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_REP_LEAD_ADDRESSES"));

				GenericUIControl iclRepLeadsAddress = new GenericUIControl();
				iclRepLeadsAddress.setTemplateName(controlDefnintion);
				iclRepLeadsAddress.addParameter("result", dbResult);
				uiContext.addControl(iclRepLeadsAddress);

			} else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "moveLeads")) {
				String unDispositionedLeads = requestWrapper.getParameter("unDispositionedLeads");
				getPlatformService().update(
						"UPDATE IMS.IMS_LEAD_LIFECYCLE SET PERSON_ID=?,LEAD_SHEET_ID=? WHERE CAST(ROW_ID as VARCHAR) +'-'+ LEAD_ID in (" + unDispositionedLeads
								+ ")", new Object[]{null, null});
				getPlatformService().update("UPDATE IMS.IMS_LEADSHEET SET NUMBER_OF_LEADS=NUMBER_OF_LEADS-? WHERE LEADSHEET_ID =? ",
						new Object[]{new Long(unDispositionedLeads.split(",").length), requestWrapper.getParameter("leadSheetId")});
				List inputs = new ArrayList();
				inputs.add(requestWrapper.getParameter("leadSheetId"));
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_LEADSHEET_COUNT"), inputs);
				if (((Integer) dbResult.getData().get(0).get(0).getActualValue()) <= 0) {
					getPlatformService().update("DELETE IMS.IMS_LEADSHEET WHERE LEADSHEET_ID =? ", new Object[]{requestWrapper.getParameter("leadSheetId")});

				}

			}
		}

	}
	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
