/**
 * 
 */
package com.cydcor.framework.controller;

import java.util.Iterator;
import java.util.Map;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.service.PlatformService;
import com.cydcor.framework.utils.ServiceLocator;

/**
 * @author ashwin
 * 
 */
public abstract class GenericController implements Controller {

	private PlatformService platformService = null;

	/**
	 * @return
	 */
	public String toString() {
		return this.getClass().getName();
	}

	/**
	 * @return
	 */
	protected PlatformService getPlatformService() {
		return ServiceLocator.getService(PlatformService.class);
	}

	/**
	 * @param uiContext
	 * @param pageDefnintion
	 */
	protected void clearOldPage(DynamicUIContext uiContext,
			String pageDefnintion) {
		GenericUIControl clearPageControl = new GenericUIControl();
		clearPageControl.setTemplateName(pageDefnintion);
		clearPageControl.addParameter("clearPage", true);

		uiContext.addControl(clearPageControl);
	}

	protected void clearOldPage(DynamicUIContext uiContext,
			String pageDefnintion, Map paramMap) {
		GenericUIControl clearPageControl = new GenericUIControl();
		clearPageControl.setTemplateName(pageDefnintion);
		clearPageControl.addParameter("clearPage", true);

		for (Iterator it = paramMap.entrySet().iterator(); it.hasNext();) {
			Map.Entry e = (Map.Entry) it.next();
			String key = (String)e.getKey();
			String value = (String)e.getValue();
			clearPageControl.addParameter(key, value);
		}

		uiContext.addControl(clearPageControl);
	}

	/**
	 * @param uiContext
	 */
	protected void disableMap(DynamicUIContext uiContext) {
		uiContext.setShowMap(false);
	}

}
