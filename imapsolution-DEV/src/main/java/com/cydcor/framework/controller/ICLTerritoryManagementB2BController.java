package com.cydcor.framework.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

public class ICLTerritoryManagementB2BController extends GenericController {

    public void execute(DynamicUIContext uiContext) {

	CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
	String officeSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("officeSeq");

	String campaignSeq = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("campaignSeq");

	String territoryID = CydcorContext.getInstance().getRequestContext().getRequestWrapper().getParameter("territoryID");

	String pageDefnintion = "icl/ICLTerritoryManagementB2BDef.html";
	String controlDefnintion = "icl/ICLTerritoryManagementB2B.html";
	if (StringUtils.isBlank(uiContext.getControlName())) {
	    clearOldPage(uiContext, pageDefnintion);

	    // Add Map Control Noe
	    GenericUIControl mapControl = new GenericUIControl();
	    mapControl.setTemplateName(pageDefnintion);
	    mapControl.addParameter("officeSeq", officeSeq);
	    mapControl.addParameter("campaignSeq", campaignSeq);
	    mapControl.addParameter("territoryID", territoryID);
	    mapControl.addParameter("showCampaignMap", true);

	    uiContext.addControl(mapControl);

	    // Add Control the territoryDef Selector
	    GenericUIControl territoryDefControl = new GenericUIControl();
	    territoryDefControl.setTemplateName(pageDefnintion);
	    territoryDefControl.addParameter("territoryDefSelector", true);

	    uiContext.addControl(territoryDefControl);

	    // Add Control the territoryDef Selector
	    GenericUIControl territoryListTableControl = new GenericUIControl();
	    territoryListTableControl.setTemplateName(pageDefnintion);

	    territoryListTableControl.addParameter("territoryListTable", true);

	    uiContext.addControl(territoryListTableControl);
	} else {
	    DatabaseResult dbResult = null;
	    if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "territoryDefSelector")) {

		/*
		 * queryString =
		 * "SELECT CAMPAIGN_SEQ,CAMPAIGN_NAME FROM IMS.MERLIN_CAMPAIGN_DETAILS"
		 * ; dbResult = getPlatformService().loadResult(queryString);
		 */
		dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

		// Add Second Control the Campaign Selector
		GenericUIControl territoryDefControl = new GenericUIControl();
		territoryDefControl.setTemplateName(controlDefnintion);
		territoryDefControl.addParameter("territoryDefSelector", true);
		territoryDefControl.addParameter("selectBox", dbResult);

		List<Object> list = new ArrayList<Object>();
		list.add(new Integer(requestWrapper.getParameter("personID")));
		dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TERRITORY_ICL_DETAILS"), list);
		territoryDefControl.addParameter("iclInfo", dbResult);

		uiContext.addControl(territoryDefControl);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "territoryListTable")) {

		List<Object> list = new ArrayList<Object>();
		list.add(new Integer(campaignSeq));
		list.add(new Integer(officeSeq));
		// Add Second Control the Campaign Selector
		TableUIControl territaryListTableControl = new TableUIControl();
		territaryListTableControl.setFilterParamList(list);
		territaryListTableControl.setQueryKeyName("SELECT_ICLREP_TERRITORY_LIST");
		territaryListTableControl.setColumnName("TERRITORY_TYPE");
		territaryListTableControl.setJavaScriptMethodName("loadTerritoryTable");
		territaryListTableControl.addParameter("territoryListTable", true);
		territaryListTableControl.setTemplateName("../" + controlDefnintion);

		uiContext.addControl(territaryListTableControl);
	    }
	}

    }

    public Object getResult(Object imput, Map<String, Object> paramMap) {
	// TODO Auto-generated method stub
	return null;
    }

    public void init(Object params) {
	// TODO Auto-generated method stub

    }

}
