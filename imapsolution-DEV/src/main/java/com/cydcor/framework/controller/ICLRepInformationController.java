package com.cydcor.framework.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

public class ICLRepInformationController extends GenericController {

	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
		
		String pageDefnintion = "icl/ICLRepInformationDef.html";
		String controlDefnintion = "icl/ICLRepInformation.html";
		if (StringUtils.isBlank(uiContext.getControlName())) {
		    clearOldPage(uiContext, pageDefnintion);
		   
		    // Add Control the iclRepInformation Selector
		    GenericUIControl iclRepInformationSelectorControl = new GenericUIControl();
		    iclRepInformationSelectorControl.setTemplateName(pageDefnintion);
		    iclRepInformationSelectorControl.addParameter("iclRepInformationSelector", true);
	
		    uiContext.addControl(iclRepInformationSelectorControl);
		    
		    // Add Control the leadListTable  Selector
		    GenericUIControl leadListTableControl = new GenericUIControl();
		    leadListTableControl.setTemplateName(pageDefnintion);
		    leadListTableControl.addParameter("leadListTable", true);
	
		    uiContext.addControl(leadListTableControl);
		    
		 // Add Map Control Noe
			GenericUIControl mapControl = new GenericUIControl();
			mapControl.setTemplateName(pageDefnintion);
			mapControl.addParameter("showCampaignMap", true);

			uiContext.addControl(mapControl);

		   
		}else {
		    DatabaseResult dbResult = new DatabaseResult();
		    String queryString = null;
		    if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
			    "iclRepInformationSelector")) {
			/*queryString = "SELECT CAMPAIGN_SEQ,CAMPAIGN_NAME FROM IMS.MERLIN_CAMPAIGN_DETAILS";
			//dbResult = getPlatformService().loadResult(queryString);
			dbResult=new DatabaseResult();*/
		    dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));
			// Add Second Control the Campaign Selector
			GenericUIControl iclRepInformationSelectorControl = new GenericUIControl();
			iclRepInformationSelectorControl.setTemplateName(controlDefnintion);
			iclRepInformationSelectorControl.addParameter("iclRepInformationSelector", true);
			iclRepInformationSelectorControl.addParameter("result", dbResult);

			uiContext.addControl(iclRepInformationSelectorControl);

		}else if (StringUtils.endsWithIgnoreCase(uiContext
				.getControlName(), "leadListTable")) {
			List list = new ArrayList();
			list.add(new Integer(requestWrapper.getParameter("personID")));
			list.toArray(new Object[0]);
			dbResult =getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_TEMPLATE_NAMES"),list);
			
			//dbResult = getPlatformService().loadResult(queryString);
			dbResult=new DatabaseResult();
			// Add Second Control the Campaign Selector
			GenericUIControl leadListTableControl = new GenericUIControl();
			leadListTableControl.setTemplateName(controlDefnintion);
			leadListTableControl.addParameter("leadListTable", true);
			leadListTableControl.addParameter("result", dbResult);

			uiContext.addControl(leadListTableControl);
		}
		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
