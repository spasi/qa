package com.cydcor.framework.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

public class RepManagementController extends GenericController {

	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance()
				.getRequestContext().getRequestWrapper();

		String pageDefnintion = "icl/RepManagementDef.html";
		String controlDefnintion = "icl/RepManagement.html";
		if (StringUtils.isBlank(uiContext.getControlName())) {
			clearOldPage(uiContext, pageDefnintion);

			// Add Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);

			uiContext.addControl(campaignSelectorControl);

			// Add Control the ICL Selector
			GenericUIControl iclSelectorControl = new GenericUIControl();
			iclSelectorControl.setTemplateName(pageDefnintion);
			iclSelectorControl.addParameter("iclSelector", true);

			uiContext.addControl(iclSelectorControl);

			// Add Map Control Noe
			GenericUIControl mapControl = new GenericUIControl();
			mapControl.setTemplateName(pageDefnintion);
			mapControl.addParameter("showCampaignMap", true);
			uiContext.addControl(mapControl);

			// Add iclTerritaryTable Control
			GenericUIControl iclTerritaryTableControl = new GenericUIControl();
			iclTerritaryTableControl.setTemplateName(pageDefnintion);
			iclTerritaryTableControl.addParameter("iclTerritaryTable", true);
			// iclTableControl.addParameter("officeSeq", officeSeq);

			uiContext.addControl(iclTerritaryTableControl);

			// Add iclRepTable Control
			GenericUIControl iclRepTableControl = new GenericUIControl();
			iclRepTableControl.setTemplateName(pageDefnintion);
			iclRepTableControl.addParameter("iclRepTable", true);

			uiContext.addControl(iclRepTableControl);

		} else {
			DatabaseResult dbResult = null;
			String queryString = null;
			if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
					"campaignSelector")) {
				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(campaignSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext
					.getControlName(), "iclSelector")) {

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.toArray(new Object[0]);

				dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get(
								"SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

				// Add Second Control the Campaign Selector
				GenericUIControl iclSelectorControl = new GenericUIControl();
				iclSelectorControl.setTemplateName(controlDefnintion);
				iclSelectorControl.addParameter("iclSelector", true);
				iclSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(iclSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext
					.getControlName(), "iclTerritaryTable")) {

				List list = new ArrayList();
				list.add(requestWrapper.getParameter("campaignSeq"));
				list.add(requestWrapper.getParameter("officeSeq"));
				list.toArray(new Object[0]);
				// Add Second Control the Campaign Selector
				TableUIControl iclTerritaryTableControl = new TableUIControl();
				iclTerritaryTableControl.setFilterParamList(list);
				iclTerritaryTableControl
						.setQueryKeyName("SELECT_ICLLEAD_TERRITORY_DEFINITION");
				iclTerritaryTableControl
						.setColumnName("LEAD_TERRITORY_DEF_SEQ");
				iclTerritaryTableControl
						.setJavaScriptMethodName("loadICLTerritaryTable");
				iclTerritaryTableControl
						.addParameter("iclTerritaryTable", true);
				iclTerritaryTableControl.setTemplateName("../"
						+ controlDefnintion);

				uiContext.addControl(iclTerritaryTableControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext
					.getControlName(), "iclRepTable")) {
				List list = new ArrayList();
				list.add(requestWrapper.getParameter("officeSeq"));
				list.toArray(new Object[0]);

				TableUIControl iclRepTableControl = new TableUIControl();
				iclRepTableControl.setFilterParamList(list);
				iclRepTableControl
						.setQueryKeyName("SELECT_LEADSHEET_ICL_REPDETAILS");
				iclRepTableControl.setColumnName("REP_NAME");
				iclRepTableControl
						.setJavaScriptMethodName("refreshIclRepTable");
				iclRepTableControl.addParameter("iclRepTable", true);
				iclRepTableControl.setTemplateName("../" + controlDefnintion);

				uiContext.addControl(iclRepTableControl);
			}
		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
