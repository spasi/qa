/**
 * 
 */
package com.cydcor.framework.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author ashwin
 * 
 */
public class ControllerFactory {
	protected final Log logger = LogFactory.getLog(getClass());
	private static ControllerFactory controllerFactory = new ControllerFactory();

	private ControllerFactory() {

	}

	/**
	 * @return
	 */
	public static ControllerFactory getInstance() {
		return controllerFactory;
	}

	/**
	 * Every Controller Class Is PageName appended with Controller
	 * 
	 * @param pageName
	 * @return
	 */
	public Controller getController(String pageName) {
		Controller controller = null;
		String className = "com.cydcor.framework.controller." + pageName
				+ "Controller";
		try {
			controller = (Controller) Class.forName(className).newInstance();
		} catch (InstantiationException e) {
			logger.debug("unsupporter Class " + className + ": "
					+ e.getMessage());
		} catch (IllegalAccessException e) {
			logger.debug("unsupporter Class " + className + ": "
					+ e.getMessage());

		} catch (ClassNotFoundException e) {
			logger.debug("error instantiating Class " + className + ": "
					+ e.getMessage());
		}
		// h.init(params);
		return controller;
	}

}
