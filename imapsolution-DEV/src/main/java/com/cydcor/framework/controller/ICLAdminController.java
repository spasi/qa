package com.cydcor.framework.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

/**
 * @author ashwin
 * 
 */
public class ICLAdminController extends GenericController {

	public void execute(DynamicUIContext uiContext) {
		
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance()
		.getRequestContext().getRequestWrapper();


		String officeSeq = CydcorContext.getInstance().getRequestContext()
				.getRequestWrapper().getParameter("officeSeq");
		String campaignSeq = CydcorContext.getInstance().getRequestContext()
		.getRequestWrapper().getParameter("campaignSeq");
		String pageDefnintion = "account/ICLAdminDef.html";
		String controlDefnintion = "account/ICLAdmin.html";
		if (StringUtils.isBlank(uiContext.getControlName())) {
			// clearOldPage(uiContext, pageDefnintion);

			// Add Map Control
			GenericUIControl pageControl = new GenericUIControl();
			pageControl.setTemplateName(pageDefnintion);
			pageControl.addParameter("showTerritoryMap", true);
			pageControl.addParameter("officeSeq", officeSeq);
			pageControl.addParameter("campaignSeq", campaignSeq);
			pageControl.addParameter("campaignIclDef", true);			

			uiContext.addControl(pageControl);

		} else if ("campaignIclDef"
				.equalsIgnoreCase(uiContext.getControlName())) {

			List list = new ArrayList();
			list.add(officeSeq.replaceAll("'", ""));
			list.toArray(new Object[0]);
			DatabaseResult dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery()
							.get("SELECT_CAMPAIGN_ICL_DEFINITIONS"), list);

			// Add Second Control the Campaign Selector
			GenericUIControl iclTableControl = new GenericUIControl();
			iclTableControl.setTemplateName(controlDefnintion);
			iclTableControl.addParameter("campaignIclDef", true);
			iclTableControl.addParameter("result", dbResult);
			
			List list1 = new ArrayList();
			list1.add(requestWrapper.getParameter("officeSeq"));
			list1.add(requestWrapper.getParameter("campaignSeq"));
			list1.toArray(new Object[0]);
			
			DatabaseResult result1 = getPlatformService()
					.loadResult(
							QueryUtils.getQuery()
									.get("SELECT_LEADS_NUMBER"), list1);
			String defleads="";
			String maxleads="";
			if (result1.getData().size() > 0) {
				for (List<MapObject> row : result1.getData()) {
					if(row.get(0).getValue()!=null)
					defleads = row.get(0).getValue();
					
					if(row.get(1).getValue()!=null)
					maxleads = row.get(1).getValue();
					
					break;
				}
			}
			iclTableControl.addParameter("defleads",
					defleads);
			iclTableControl.addParameter("maxleads",
					maxleads);

			uiContext.addControl(iclTableControl);

		} else if ("territoryTable"
				.equalsIgnoreCase(uiContext.getControlName())) {

			List list = new ArrayList();
			list.add(officeSeq.replaceAll("'", ""));
			list.add(campaignSeq.replaceAll("'", ""));
			list.toArray(new Object[0]);

			TableUIControl territoryTableControl = new TableUIControl();
			territoryTableControl.setFilterParamList(list);
			territoryTableControl.setQueryKeyName("SELECT_ICL_TERRITORIES");
			territoryTableControl.setColumnName("TERRITORY_NAME");
			territoryTableControl.setJavaScriptMethodName("refreshTerritory");
			territoryTableControl.addParameter("territoryTable", true);
			territoryTableControl.setTemplateName("../" + controlDefnintion);

			uiContext.addControl(territoryTableControl);

		} else if ("deleteTerritory".equalsIgnoreCase(uiContext
				.getControlName())) {

			String territorySeq = CydcorContext.getInstance()
					.getRequestContext().getRequestWrapper().getParameter(
							"territorySeq");
			/*
			 * String sql =
			 * "select ter.TERRITORY_TYPE , def.TERRITORY_KEY ,pol.POLYGON_KEY,def.ICL_TERRITORY_SEQ   from  IMS.IMS_ICL_TERRITORY_DEF def , IMS.IMS_Territory ter , IMS.IMS_POLYGON pol where def.TERRITORY_KEY=ter.TERRITORY_KEY and ter.TERRITORY_KEY=pol.TERRITORY_KEY and ter.TERRITORY_KEY= "
			 * + territorySeq;
			 */
			List list1 = new ArrayList();
			list1.add(territorySeq);
			list1.toArray(new Object[0]);

			DatabaseResult result = getPlatformService().loadResult(
					QueryUtils.getQuery().get(
							"SELECT_TERRITORY_DEF_AND_TERRITORY_DETAILS"),
					list1);
			// DatabaseResult result = getPlatformService().loadResult(sql);
			if (result.getData().size() > 0) {
				String polygonSqlParam = result.getData().get(0).get(2)
						.getValue();
				/*
				 * String polygonSql =
				 * "delete from IMS.IMS_Polygon where TERRITORY_KEY=" +
				 * result.getData().get(0).get(2).getValue() + ";";
				 */

				String territorySqlParam = result.getData().get(0).get(1)
						.getValue();
				/*
				 * String territorySql =
				 * "delete from IMS.IMS_Territory where TERRITORY_KEY=" +
				 * result.getData().get(0).get(1).getValue() + ";";
				 */

				String iclAssnSqlParam = result.getData().get(0).get(3)
						.getValue();
				/*
				 * String iclAssnSql =
				 * "delete from IMS.IMS_ICL_TERRITORY_DEF where ICL_TERRITORY_SEQ="
				 * + result.getData().get(0).get(3).getValue() + ";";
				 */

				// logger.debug(sql);
				// getPlatformService().execute(sql);
				getPlatformService().loadResult(
						QueryUtils.getQuery().get(
								"SELECT_TERRITORY_DEF_AND_TERRITORY_DETAILS"),
						list1);
				getPlatformService().update(
						QueryUtils.getQuery().get(
								"DELETE_ASSIGN_ICL_DETAILS_BY_SEQ"),
						new Object[] { iclAssnSqlParam });
				if (StringUtils.equalsIgnoreCase("CUSTOM", result.getData()
						.get(0).get(0).getValue())) {
					// getPlatformService().execute(polygonSql);
					getPlatformService()
							.update(
									QueryUtils.getQuery().get(
											"DELETE_POLYGON_DETAILS"),
									new Object[] { polygonSqlParam });

					getPlatformService().update(
							QueryUtils.getQuery().get(
									"DELETE_TERRITORY_DETAILS_BY_KEY"),
							new Object[] { territorySqlParam });
				}
			}
		}
	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
