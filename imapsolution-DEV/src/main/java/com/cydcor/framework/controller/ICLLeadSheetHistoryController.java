package com.cydcor.framework.controller;

import groovyjarjarbackport.java.util.Arrays;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.model.MapObject;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

public class ICLLeadSheetHistoryController extends GenericController {
    private static final transient Log logger = LogFactory.getLog(ICLLeadSheetHistoryController.class);

    public void execute(DynamicUIContext uiContext) {
	CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();

	String pageDefnintion = "icl/ICLLeadSheetHistoryDef.html";
	String controlDefnintion = "icl/ICLLeadSheetHistory.html";
	if (StringUtils.isBlank(uiContext.getControlName())) {
	    clearOldPage(uiContext, pageDefnintion);

	    // Add Control the Campaign Selector
	    GenericUIControl campaignSelectorControl = new GenericUIControl();
	    campaignSelectorControl.setTemplateName(pageDefnintion);
	    campaignSelectorControl.addParameter("campaignSelector", true);

	    uiContext.addControl(campaignSelectorControl);

	    // Add Control the ICL Selector
	    GenericUIControl iclSelectorControl = new GenericUIControl();
	    iclSelectorControl.setTemplateName(pageDefnintion);
	    iclSelectorControl.addParameter("iclSelector", true);

	    uiContext.addControl(iclSelectorControl);

	    GenericUIControl leadSheetCriteriaControl = new GenericUIControl();
	    leadSheetCriteriaControl.setTemplateName(pageDefnintion);
	    leadSheetCriteriaControl.addParameter("leadSheetCriteriaComponents", true);
	    leadSheetCriteriaControl.addParameter("tableName", "leadSheetCriteriaComponents");

	    uiContext.addControl(leadSheetCriteriaControl);

	    // Add leadSheetAssignmentHistoryTable Control
	    GenericUIControl leadSheetAssignmentHistoryTableControl = new GenericUIControl();
	    leadSheetAssignmentHistoryTableControl.setTemplateName(pageDefnintion);
	    leadSheetAssignmentHistoryTableControl.addParameter("leadSheetAssignmentHistoryTable", false);
	    // iclTableControl.addParameter("officeSeq", officeSeq);

	    uiContext.addControl(leadSheetAssignmentHistoryTableControl);
	} else {
	    DatabaseResult dbResult = null;
	    String queryString = null;
	    if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
		dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));

		// Add Second Control the Campaign Selector
		GenericUIControl campaignSelectorControl = new GenericUIControl();
		campaignSelectorControl.setTemplateName(controlDefnintion);
		campaignSelectorControl.addParameter("campaignSelector", true);
		campaignSelectorControl.addParameter("result", dbResult);

		uiContext.addControl(campaignSelectorControl);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "iclSelector")) {

		List<Object> list = new ArrayList<Object>();
		list.add(requestWrapper.getParameter("campaignSeq"));

		dbResult = getPlatformService().loadResult(
			QueryUtils.getQuery().get("SELECT_ICL_DETAILS_CAMPAIGN_SEQ"), list);

		System.out.println("Show the DbResult........." + dbResult);
		// Add Second Control the Campaign Selector
		GenericUIControl iclSelectorControl = new GenericUIControl();
		iclSelectorControl.setTemplateName(controlDefnintion);
		iclSelectorControl.addParameter("iclSelector", true);
		iclSelectorControl.addParameter("result", dbResult);

		uiContext.addControl(iclSelectorControl);

	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "leadSheetCriteriaComponents")) {
		List<Object> inputs = new ArrayList<Object>();
		inputs.add(new Long(requestWrapper.getParameter("officeSeq")));
		DatabaseResult repResult;
		repResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_MERGED_OFFICES"), inputs);
		String sql = QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
		for (List<MapObject> objects : repResult.getData()) {
		    sql = sql + " UNION ALL " + QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
		    inputs.add(new Integer(objects.get(0).getValue()));
		}

		sql = "SELECT * FROM ( " + sql + " ) AS TEMP ORDER BY REP_NAME";
		repResult = getPlatformService().loadResult(sql, inputs);

		GenericUIControl criteriaControl = new GenericUIControl();
		criteriaControl.setTemplateName(controlDefnintion);
		criteriaControl.addParameter("leadSheetCriteriaComponents", true);
		criteriaControl.addParameter("result", repResult);

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar startDate = new GregorianCalendar();
		startDate.add(Calendar.DAY_OF_YEAR, -30);
		Calendar endDate = new GregorianCalendar();

		criteriaControl.addParameter("startDate", dateFormat.format(startDate.getTime()));
		criteriaControl.addParameter("endDate", dateFormat.format(endDate.getTime()));

		uiContext.addControl(criteriaControl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "leadSheetAssignmentHistoryTable")) {

		queryString = QueryUtils.getQuery().get("SELECT_LEADSHEETS");
		String closedSheetsQueryString = QueryUtils.getQuery().get("SELECT_CLOSED_LEADSHEETS");

		List<Object> inputs = new ArrayList<Object>();
		inputs.add(requestWrapper.getParameter("startDate"));
		inputs.add(requestWrapper.getParameter("endDate"));

		List<Object> closedSheetsInputs = new ArrayList<Object>();
		closedSheetsInputs.add(requestWrapper.getParameter("startDate"));
		closedSheetsInputs.add(requestWrapper.getParameter("endDate"));

		if (StringUtils.isNotBlank(requestWrapper.getParameter("campaignSeq"))) {
		    queryString = queryString + " " + QueryUtils.getQuery().get("ADD_CAMPAIGN_SEQ");
		    inputs.add(new Long(requestWrapper.getParameter("campaignSeq")));
		    closedSheetsQueryString = closedSheetsQueryString + " "
			    + QueryUtils.getQuery().get("ADD_CAMPAIGN_SEQ");
		    closedSheetsInputs.add(new Long(requestWrapper.getParameter("campaignSeq")));
		}

		if (StringUtils.isNotBlank(requestWrapper.getParameter("officeSeq"))) {
		    queryString = queryString + " " + QueryUtils.getQuery().get("ADD_OFFICE_SEQ");
		    inputs.add(new Long(requestWrapper.getParameter("officeSeq")));
		    closedSheetsQueryString = closedSheetsQueryString + " "
			    + QueryUtils.getQuery().get("ADD_OFFICE_SEQ");
		    closedSheetsInputs.add(new Long(requestWrapper.getParameter("officeSeq")));

		}

		if (StringUtils.isNotBlank(requestWrapper.getParameter("repId"))) {
		    if (!requestWrapper.getParameter("repId").equalsIgnoreCase("UA")) {
			queryString = queryString + " " + QueryUtils.getQuery().get("ADD_PERSON_ID");
			inputs.add(new Long(requestWrapper.getParameter("repId")));
			closedSheetsQueryString = closedSheetsQueryString + " "
				+ QueryUtils.getQuery().get("ADD_PERSON_ID");
			closedSheetsInputs.add(new Long(requestWrapper.getParameter("repId")));
		    } else {
			queryString = queryString + " " + QueryUtils.getQuery().get("ADD_UN_ASSGN_PERSON_ID");
		    }
		}

		if (StringUtils.isNotBlank(requestWrapper.getParameter("status"))) {
		    queryString = queryString + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_STATUS");
		    inputs.add(requestWrapper.getParameter("status"));
		    closedSheetsQueryString = closedSheetsQueryString + " "
			    + QueryUtils.getQuery().get("ADD_LEAD_SHEET_STATUS");
		    closedSheetsInputs.add(requestWrapper.getParameter("status"));
		}

		TableUIControl leadSheetSavedControl = new TableUIControl();

		String leadSheetID = requestWrapper.getParameter("leadSheetID");
		if ("%".equalsIgnoreCase(leadSheetID.trim()))
		    leadSheetID = null;
		if (StringUtils.isNotBlank(leadSheetID)) {
		    queryString = queryString + " " + QueryUtils.getQuery().get("ADD_LEAD_SHEET_NAME");
		    inputs.add(leadSheetID.trim());
		    closedSheetsQueryString = closedSheetsQueryString + " "
			    + QueryUtils.getQuery().get("ADD_LEAD_SHEET_NAME");
		    closedSheetsInputs.add(leadSheetID.trim());
		}

		List repInputs = new ArrayList();
		repInputs.add(new Integer(requestWrapper.getParameter("officeSeq")));
		DatabaseResult repResult;
		repResult = getPlatformService().loadResult(QueryUtils.getQuery().get("ICL_MERGED_OFFICES"), repInputs);
		String sql = QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
		for (List<MapObject> objects : repResult.getData()) {
		    sql = sql + " UNION ALL " + QueryUtils.getQuery().get("GET_ALL_REP_FOR_CAMPAIGN_AND_ICL");
		    repInputs.add(new Integer(objects.get(0).getValue()));
		}

		sql = "SELECT * FROM ( " + sql + " ) AS TEMP ORDER BY REP_NAME";
		repResult = getPlatformService().loadResult(sql, repInputs);
		leadSheetSavedControl.addParameter("repResult", repResult);
		queryString = queryString + " UNION ALL " + closedSheetsQueryString;
		inputs.addAll(closedSheetsInputs);
		leadSheetSavedControl.setFilterParamList(inputs);
		leadSheetSavedControl.setTableName(uiContext.getControlName());
		leadSheetSavedControl.setQueryString(queryString);
		leadSheetSavedControl.setColumnName("RED_LEAD_FLAG DESC, CREATED_DATE DESC");
		leadSheetSavedControl.setJavaScriptMethodName("showLeadSheets");
		leadSheetSavedControl.addParameter("leadSheetAssignmentHistoryTable", true);
		leadSheetSavedControl.setTemplateName("../" + controlDefnintion);
		leadSheetSavedControl.setRowsPerPage(30);
		uiContext.addControl(leadSheetSavedControl);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "assnReturnDatesToLeadSheets")) {
		List<String> values = Arrays.asList(requestWrapper.getParameter("sheetIdsAndDates").split(","));
		String[] idDts = null;
		for (String value : values) {
		    idDts = value.split("~");
		    getPlatformService().update(
			    "UPDATE IMS.IMS_LEADSHEET SET REP_RETURNED_DATE=? WHERE LEADSHEET_ID=?",
			    new Object[] { idDts[1], idDts[0] });
		}
		System.out.println(values);
	    } else if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(), "editHistoryLeadSheet")) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String repId = requestWrapper.getParameter("repAssigned").trim();
		String status = requestWrapper.getParameter("status").trim();
		String assnDate = requestWrapper.getParameter("repAssignedDate").trim();
		String sheetStatus = "INP";
		boolean alreadyAssigned = true;
		boolean updateRepInfo = false;
		if (StringUtils.equalsIgnoreCase(repId, "-1") || StringUtils.equalsIgnoreCase(repId, "")) {
		    repId = null;
		    assnDate = null;
		    if (status.equalsIgnoreCase("In Progress"))
			updateRepInfo = true;
		}
		if (repId != null && assnDate.equalsIgnoreCase("")) {
		    assnDate = dateFormat.format(System.currentTimeMillis());
		    alreadyAssigned = false;
		    updateRepInfo = true;
		}

		String returnedDate = requestWrapper.getParameter("returnedDate").trim();
		if (returnedDate.equalsIgnoreCase("")) {
		    returnedDate = null;
		} else if (repId != null && status.equalsIgnoreCase("In Progress")) {
		    sheetStatus = "X";
		    updateRepInfo = true;
		}

		String sales = requestWrapper.getParameter("sales").trim();
		String dm = requestWrapper.getParameter("decisionMaker").trim();
		String dk = requestWrapper.getParameter("doorsKnocked").trim();
		String notes = requestWrapper.getParameter("notes").trim();
		String sheetId = requestWrapper.getParameter("leadSheetId");

		GenericUIControl updateLSCtrl = new GenericUIControl();
		try {
		    if (updateRepInfo) {
			getPlatformService().update(
				QueryUtils.getQuery().get("UPDATE_LEADSHEET"),
				new Object[] { repId, assnDate, returnedDate, sales, dm, dk, notes, sheetStatus,
					sheetId });
			if (assnDate != null && !alreadyAssigned) {
			    getPlatformService().update(QueryUtils.getQuery().get("ASSIGN_LEADS_REP"),
				    new Object[] { repId, sheetId });
			}

		    } else {
			getPlatformService().update(QueryUtils.getQuery().get("UPDATE_HISTORY_LEADSHEET"),
				new Object[] { returnedDate, sales, dm, notes, sheetId });
		    }
		    updateLSCtrl.setResponseText("SUCCESS");
		} catch (Exception e) {
		    logger.error(e);
		    updateLSCtrl.setResponseText("ERROR");
		}
		uiContext.addControl(updateLSCtrl);
	    }
	}
    }

    public Object getResult(Object imput, Map<String, Object> paramMap) {
	return null;
    }

    public void init(Object params) {

    }
}
