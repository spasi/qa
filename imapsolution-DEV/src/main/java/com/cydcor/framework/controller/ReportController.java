package com.cydcor.framework.controller;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

public class ReportController extends GenericController {
	private CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
	private static final transient Log logger = LogFactory.getLog(ReportController.class);

	public void execute(DynamicUIContext uiContext) {
		String pageDefnintion = "reports/" + requestWrapper.getParameter("pageDefination");

		if (StringUtils.isBlank(uiContext.getControlName())) {
			// Add Second Control the Campaign Selector
			GenericUIControl leadOperationsControl = new GenericUIControl();
			leadOperationsControl.setTemplateName(pageDefnintion);
			leadOperationsControl.addParameter("report", true);
			uiContext.addControl(leadOperationsControl);
		}
	}
	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
