package com.cydcor.framework.controller;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

public class ICLRepLeadManagementController extends GenericController {

	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
		
		String pageDefnintion = "icl/ICLRepLeadManagementDef.html";
		String controlDefnintion = "icl/ICLRepLeadManagement.html";
		if (StringUtils.isBlank(uiContext.getControlName())) {
		    clearOldPage(uiContext, pageDefnintion);
		   
	
		 // Add Map Control Noe
			GenericUIControl mapControl = new GenericUIControl();
			mapControl.setTemplateName(pageDefnintion);
			mapControl.addParameter("showCampaignMap", true);

			uiContext.addControl(mapControl);

		    // Add Control the iclRepDetails  Selector
		    GenericUIControl iclRepDetailsSelectorControl = new GenericUIControl();
		    iclRepDetailsSelectorControl.setTemplateName(pageDefnintion);
		    iclRepDetailsSelectorControl.addParameter("iclRepDetailsSelector", true);
	
		    uiContext.addControl(iclRepDetailsSelectorControl);
		    
		 // Add iclRepLeadSheetHistoryTable Control
			GenericUIControl iclRepLeadSheetHistoryTableControl = new GenericUIControl();
			iclRepLeadSheetHistoryTableControl.setTemplateName(pageDefnintion);
			iclRepLeadSheetHistoryTableControl.addParameter("iclRepLeadSheetHistoryTable", true);
			// iclTableControl.addParameter("officeSeq", officeSeq);

			uiContext.addControl(iclRepLeadSheetHistoryTableControl);
			
		}else {
		    DatabaseResult dbResult = new DatabaseResult();
		    String queryString = null;
		    if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
			    "iclRepDetailsSelector")) {
			//queryString = "SELECT CAMPAIGN_SEQ,CAMPAIGN_NAME FROM IMS.MERLIN_CAMPAIGN_DETAILS";
			
			 dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));
			//dbResult = getPlatformService().loadResult(queryString);
			//dbResult = new DatabaseResult();
			// Add Second Control the Campaign Selector
			GenericUIControl iclRepDetailsSelectorControl = new GenericUIControl();
			iclRepDetailsSelectorControl.setTemplateName(controlDefnintion);
			iclRepDetailsSelectorControl.addParameter("iclRepDetailsSelector", true);
			iclRepDetailsSelectorControl.addParameter("result", dbResult);

			uiContext.addControl(iclRepDetailsSelectorControl);

		    }else if (StringUtils.endsWithIgnoreCase(uiContext
					.getControlName(), "iclRepLeadSheetHistoryTable")) {
				//queryString = "SELECT TERRITORY_KEY,TERRITORY_NAME,TERRITORY_TYPE,CREATED_DATE,IS_ACTIVE  FROM IMS.IMS_TERRITORY";
				//dbResult = getPlatformService().loadResult(queryString);
				//dbResult = new DatabaseResult();
		    	dbResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_TERRITORY"));
				// Add Second Control the Campaign Selector
				GenericUIControl iclRepLeadSheetHistoryTableControl = new GenericUIControl();
				iclRepLeadSheetHistoryTableControl.setTemplateName(controlDefnintion);
				iclRepLeadSheetHistoryTableControl
						.addParameter("iclRepLeadSheetHistoryTable", true);
				iclRepLeadSheetHistoryTableControl.addParameter("result", dbResult);

				uiContext.addControl(iclRepLeadSheetHistoryTableControl);

			}
		}


	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
