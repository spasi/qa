package com.cydcor.framework.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

public class IMSDashboardController extends GenericController {
	
	public void execute(DynamicUIContext uiContext) {
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance().getRequestContext().getRequestWrapper();
		
		String pageDefnintion = "icl/IMSDashboardDef.html";
		String controlDefnintion = "icl/IMSDashboard.html";
		if (StringUtils.isBlank(uiContext.getControlName())) {
			clearOldPage(uiContext, pageDefnintion);
			
			GenericUIControl searchCriteriaControl = new GenericUIControl();
			searchCriteriaControl.setTemplateName(pageDefnintion);
			searchCriteriaControl.addParameter("searchCriteriaTable", true);
			uiContext.addControl(searchCriteriaControl);
			
					
			// Add Control the leadListTable Selector
			GenericUIControl leadUploadHistoryTableControl = new GenericUIControl();
			leadUploadHistoryTableControl.setTemplateName(pageDefnintion);
			leadUploadHistoryTableControl.addParameter("leadUploadHistoryTable", true);
			leadUploadHistoryTableControl.addParameter("tableName", "leadUploadHistoryTable");
			uiContext.addControl(leadUploadHistoryTableControl);
			
			GenericUIControl leadUploadHistoryControl = new GenericUIControl();
			leadUploadHistoryControl = new GenericUIControl();
			leadUploadHistoryControl.setTemplateName(pageDefnintion);
			leadUploadHistoryControl.addParameter("leadUploadHistory", true);
			leadUploadHistoryControl.addParameter("tableName", "leadUploadHistory");
			uiContext.addControl(leadUploadHistoryControl);
			
			
		} else {
			String loggedInUserRole = FreeMarkerEngine.getInstance().evaluateString(
					"${GlobalContext.crmContext.roleNames}");
			
			DatabaseResult dbResult = new DatabaseResult();
			if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "campaignSelector")) {
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));
				
				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				if (loggedInUserRole != null
						&& (loggedInUserRole.equals("Application Administrator") || loggedInUserRole
								.equals("Cydcor Executive"))) {
					campaignSelectorControl.addParameter("campaignSelector", true);
				} else {
					campaignSelectorControl.addParameter("iclCampaignSelector", true);
				}
				campaignSelectorControl.addParameter("result", dbResult);
				
				uiContext.addControl(campaignSelectorControl);
			} 
			
			else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "leadUploadHistoryTable")) {
				String mainString = "";
				
				int grpByIdx = 0;
				String queryString = "";
				List<Object> inputs = new ArrayList<Object>();
				TableUIControl leadUploadHistoryTableControl = new TableUIControl();
				// leadUploadHistoryTableControl.setDataSource("LMS");
				
				boolean whereAdded = false;
				
				if (loggedInUserRole != null
						&& (loggedInUserRole.equals("Application Administrator") || loggedInUserRole
								.equals("Cydcor Executive"))) 
				{
					mainString = QueryUtils.getQuery().get("SELECT_IMS_DASHBOARD_ADMIN");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					if (StringUtils.isNotBlank(requestWrapper.getParameter("ownerName"))) {
						queryString = queryString + " AND LEAD_OWNER=? ";
						inputs.add(requestWrapper.getParameter("ownerName"));
					}
					
					if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))
							&& StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + " AND CREATED_DATE BETWEEN ? AND ? ";
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))) {
						queryString = queryString + " AND CREATED_DATE = ? ";
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + " AND CREATED_DATE = ? ";
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					}
					if (StringUtils.isNotBlank(requestWrapper.getParameter("campainId"))) {
						queryString = queryString + " AND " + " CAMPAIGN_SEQ = ?";
						inputs.add(new Long(requestWrapper.getParameter("campainId")));
					}
					
					leadUploadHistoryTableControl.setColumnName("UPLOAD_DATE DESC,UPLOAD_FILENAME");
				} else {
					mainString = QueryUtils.getQuery().get("SELECT_IMS_DASHBOARD_OTHERS");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					leadUploadHistoryTableControl
							.setColumnName("MODIFIED_DATE DESC, CAMPAIGN_NAME,ICL_CODE,LEAD_SHEET_ID");
					if (StringUtils.isNotBlank(requestWrapper.getParameter("ownerName"))) {
						queryString = queryString + " WHERE ICL_CODE=? ";
						inputs.add(requestWrapper.getParameter("ownerName"));
						whereAdded = true;
					}
					
					if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))
							&& StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + (whereAdded ? " AND " : " WHERE ")
								+ " MODIFIED_DATE BETWEEN ? AND ? ";
						
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))) {
						queryString = queryString + (whereAdded ? " AND " : " WHERE ")
								+ " MODIFIED_DATE = ? ";
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + (whereAdded ? " AND " : " WHERE ")
								+ "  MODIFIED_DATE = ? ";
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					}
					if (StringUtils.isNotBlank(requestWrapper.getParameter("campainId"))) {
						queryString = queryString + (whereAdded ? " AND " : " WHERE ") + "  CAMPAIGN_NAME = ?";
						inputs.add((requestWrapper.getParameter("campainId")));
					}
					
				}
				
				queryString = queryString + mainString.substring(grpByIdx);
				leadUploadHistoryTableControl.setQueryString(queryString);
				leadUploadHistoryTableControl.setFilterParamList(inputs);
				leadUploadHistoryTableControl.setTableName(uiContext.getControlName());
				leadUploadHistoryTableControl.setRowsPerPage(20);
				leadUploadHistoryTableControl.setJavaScriptMethodName("refreshleadUploadHistoryTable");
				leadUploadHistoryTableControl.addParameter("leadUploadHistoryTable", true);
				leadUploadHistoryTableControl.setTemplateName("../" + controlDefnintion);
				uiContext.addControl(leadUploadHistoryTableControl);
			} 
			
			else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "leadUploadHistory")) 
			{
				String mainString = "";
				int grpByIdx = 0;
				String queryString = "";
				List<Object> inputs = new ArrayList<Object>();
				TableUIControl leadUploadHistoryControl = new TableUIControl();
				// leadUploadHistoryTableControl.setDataSource("LMS");
				
				boolean whereAdded = false;
				
				if (loggedInUserRole != null
						&& (loggedInUserRole.equals("Application Administrator") || loggedInUserRole
								.equals("Cydcor Executive"))) {
					mainString = QueryUtils.getQuery().get("SELECT_IMS_DASHBOARD_ADMIN_TABLE");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					if (StringUtils.isNotBlank(requestWrapper.getParameter("ownerName"))) {
						queryString = queryString + " AND LEAD_OWNER=? ";
						inputs.add(requestWrapper.getParameter("ownerName"));
					}
					
					if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))
							&& StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + " AND CREATED_DATE BETWEEN ? AND ? ";
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					} 
					else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))) {
						queryString = queryString + " AND CREATED_DATE = ? ";
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + " AND CREATED_DATE = ? ";
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					}
					if (StringUtils.isNotBlank(requestWrapper.getParameter("campainId"))) {
						queryString = queryString + " AND " + " CAMPAIGN_SEQ = ?";
						inputs.add(new Long(requestWrapper.getParameter("campainId")));
					}
					
					leadUploadHistoryControl.setColumnName("UPLOAD_DATE DESC,CAMPAIGN_NAME");
				} else {
					mainString = QueryUtils.getQuery().get("SELECT_IMS_DASHBOARD_OTHERS");
					grpByIdx = mainString.lastIndexOf(" GROUP BY ");
					queryString = mainString.substring(0, grpByIdx);
					leadUploadHistoryControl
							.setColumnName("MODIFIED_DATE DESC, CAMPAIGN_NAME,ICL_CODE,LEAD_SHEET_ID");
					if (StringUtils.isNotBlank(requestWrapper.getParameter("ownerName"))) {
						queryString = queryString + " WHERE ICL_CODE=? ";
						inputs.add(requestWrapper.getParameter("ownerName"));
						whereAdded = true;
					}
					
					if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))
							&& StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + (whereAdded ? " AND " : " WHERE ")
								+ " MODIFIED_DATE BETWEEN ? AND ? ";
						
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedStartDate"))) {
						queryString = queryString + (whereAdded ? " AND " : " WHERE ")
								+ " MODIFIED_DATE = ? ";
						inputs.add((requestWrapper.getParameter("uploadedStartDate")));
					} else if (StringUtils.isNotBlank(requestWrapper.getParameter("uploadedEndDate"))) {
						queryString = queryString + (whereAdded ? " AND " : " WHERE ")
								+ "  MODIFIED_DATE = ? ";
						inputs.add((requestWrapper.getParameter("uploadedEndDate")));
					}
					if (StringUtils.isNotBlank(requestWrapper.getParameter("campainId"))) {
						queryString = queryString + (whereAdded ? " AND " : " WHERE ") + "  CAMPAIGN_NAME = ?";
						inputs.add((requestWrapper.getParameter("campainId")));
					}
					
				}
				
				queryString = queryString + mainString.substring(grpByIdx);
				leadUploadHistoryControl.setQueryString(queryString);
				leadUploadHistoryControl.setFilterParamList(inputs);
				leadUploadHistoryControl.setTableName(uiContext.getControlName());
				leadUploadHistoryControl.setRowsPerPage(20);
				leadUploadHistoryControl.setJavaScriptMethodName("refreshleadUploadHistoryTableNEW");
				leadUploadHistoryControl.addParameter("leadUploadHistory", true);
				leadUploadHistoryControl.setTemplateName("../" + controlDefnintion);
				uiContext.addControl(leadUploadHistoryControl);
			} 
			else if (StringUtils.equalsIgnoreCase(uiContext.getControlName(), "showAlerts")) 
			{
				
				DatabaseResult newLeadsResult = null;
				DatabaseResult expiringLeadsResult = null;
				String queryString = null;
				newLeadsResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_ALERT_FOR_NEW_LEADS"));
				
				expiringLeadsResult = getPlatformService().loadResult(
						QueryUtils.getQuery().get("SELECT_ALERT_FOR_EXPIRING_LEADS"));
				
				// Add Second Control the Campaign Selector
				GenericUIControl alertsControl = new GenericUIControl();
				alertsControl.setTemplateName(controlDefnintion);
				alertsControl.addParameter("showAlerts", true);
				alertsControl.addParameter("newLeadsResult", newLeadsResult);
				alertsControl.addParameter("expiringLeadsResult", expiringLeadsResult);
				
				uiContext.addControl(alertsControl);
				
				// SELECT_ALERT_FOR_EXPIRING_LEADS
				
			}
		}
	}
	
	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void init(Object params) {
		// TODO Auto-generated method stub
		
	}
	
}
