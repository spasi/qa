package com.cydcor.framework.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import com.cydcor.framework.census.DBUtil;
import com.cydcor.framework.census.Territory;
import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;
import com.cydcor.framework.wrapper.CydcorRequestWrapper;

public class PopupWindowController extends GenericController {
	private static final transient Log logger = LogFactory.getLog(PopupWindowController.class);

	public void execute(DynamicUIContext uiContext) {
		String pageDefnintion = "template/PopupWindowDef.html";
		String controlDefnintion = "template/PopupWindow.html";
		CydcorRequestWrapper requestWrapper = CydcorContext.getInstance()
				.getRequestContext().getRequestWrapper();
		String usState = null, usCounty = null;
		;
		if (!StringUtils.isBlank(requestWrapper.getParameter("territorySeq"))) {
			/* String sql = "SELECT state_name,TERRITORY_NAME    FROM IMS.IMS_ICL_TERRITORY_DEF def , IMS.IMS_TERRITORY t where def.TERRITORY_KEY=t.TERRITORY_KEY and def.ICL_TERRITORY_SEQ="
					+ requestWrapper.getParameter("territorySeq");
			DatabaseResult databaseResult = getPlatformService()
					.loadResult(sql); */
			List list = new ArrayList();
			list.add(requestWrapper.getParameter("territorySeq"));
			list.toArray(new Object[0]);

			DatabaseResult databaseResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get(
							"SELECT_STATENAME_TERRITORYNAME"), list);
			
			usState = databaseResult.getData().get(0).get(0).getValue();
			usCounty = databaseResult.getData().get(0).get(1).getValue();
		}
		if (StringUtils.isBlank(uiContext.getControlName())) {
			clearOldPage(uiContext, pageDefnintion);

			disableMap(uiContext);

			if (StringUtils.isNotBlank(requestWrapper
					.getParameter("displayStates"))
					&& StringUtils.equalsIgnoreCase(requestWrapper
							.getParameter("displayStates"), "true")) {
				GenericUIControl statesCtrl = new GenericUIControl();
				statesCtrl.setTemplateName(pageDefnintion);
				statesCtrl.addParameter("statesSelector", true);
				statesCtrl.addParameter("usState", usState);
				statesCtrl.addParameter("territorySeq", requestWrapper
						.getParameter("territorySeq"));
				String officeSeq = requestWrapper.getParameter("officeSeq");
				statesCtrl.addParameter("officeSeq", officeSeq);

				uiContext.addControl(statesCtrl);

			}
			if (StringUtils.isNotBlank(requestWrapper
					.getParameter("displayCounties"))
					&& StringUtils.equalsIgnoreCase(requestWrapper
							.getParameter("displayCounties"), "true")) {
				GenericUIControl countiesCtrl = new GenericUIControl();
				countiesCtrl.setTemplateName(pageDefnintion);
				countiesCtrl.addParameter("countiesSelector", true);
				countiesCtrl.addParameter("usCounty", usCounty);

				uiContext.addControl(countiesCtrl);

			}
			if (StringUtils.isNotBlank(requestWrapper
					.getParameter("iclSubmitActions"))
					&& StringUtils.equalsIgnoreCase(requestWrapper
							.getParameter("iclSubmitActions"), "true")) {
				GenericUIControl submitActions = new GenericUIControl();
				submitActions.setTemplateName(pageDefnintion);
				submitActions.addParameter("iclSubmitActions", true);

				uiContext.addControl(submitActions);

			}

			if (StringUtils.isNotBlank(requestWrapper
					.getParameter("wireCenterStatesSelector"))
					&& StringUtils.equalsIgnoreCase(requestWrapper
							.getParameter("wireCenterStatesSelector"), "true")) {
				GenericUIControl wireCenterStatesSelector = new GenericUIControl();
				wireCenterStatesSelector.setTemplateName(pageDefnintion);
				wireCenterStatesSelector.addParameter(
						"wireCenterStatesSelector", true);
				wireCenterStatesSelector.addParameter("repId", requestWrapper
						.getParameter("repId"));
				uiContext.addControl(wireCenterStatesSelector);

			}
			if (StringUtils.isNotBlank(requestWrapper
					.getParameter("wireCenterSelector"))
					&& StringUtils.equalsIgnoreCase(requestWrapper
							.getParameter("wireCenterSelector"), "true")) {
				GenericUIControl wireCenterSelector = new GenericUIControl();
				wireCenterSelector.setTemplateName(pageDefnintion);
				wireCenterSelector.addParameter("wireCenterSelector", true);

				uiContext.addControl(wireCenterSelector);

			}
			if (StringUtils.isNotBlank(requestWrapper
					.getParameter("daSelector"))
					&& StringUtils.equalsIgnoreCase(requestWrapper
							.getParameter("daSelector"), "true")) {
				GenericUIControl daSelector = new GenericUIControl();
				daSelector.setTemplateName(pageDefnintion);
				daSelector.addParameter("daSelector", true);

				uiContext.addControl(daSelector);

			}

		} else if ("statesSelector"
				.equalsIgnoreCase(uiContext.getControlName())) {
			/* String queryString = "SELECT DISTINCT STATE_NAME FROM IMS.IMS_TERRITORY WHERE STATE_NAME IS NOT NULL ORDER BY STATE_NAME";
			DatabaseResult dbResult = getPlatformService().loadResult(
					queryString); */
			DatabaseResult dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get(
							"SELECT_STATES"));
			GenericUIControl statesControl = new GenericUIControl();
			statesControl.setTemplateName(controlDefnintion);
			statesControl.addParameter("statesSelector", true);
			statesControl.addParameter("result", dbResult);

			logger.debug("usState=========" + usState);
			statesControl.addParameter("usState", usState);

			uiContext.addControl(statesControl);

		} else if ("countiesSelector".equalsIgnoreCase(uiContext
				.getControlName())) {

			/* String queryString = "SELECT DISTINCT TERRITORY_NAME,TERRITORY_KEY FROM IMS.IMS_TERRITORY WHERE STATE_NAME IS NOT NULL AND STATE_NAME LIKE '"
					+ requestWrapper.getParameter("stateName")
					+ "' ORDER BY TERRITORY_NAME";
			DatabaseResult dbResult = getPlatformService().loadResult(
					queryString);*/
			List list = new ArrayList();
			list.add(requestWrapper.getParameter("stateName"));
			list.toArray(new Object[0]);

			DatabaseResult dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get(
							"SELECT_COUNTIES"), list);
			GenericUIControl countiesControl = new GenericUIControl();
			countiesControl.setTemplateName(controlDefnintion);
			countiesControl.addParameter("countiesSelector", true);
			countiesControl.addParameter("result", dbResult);
			countiesControl.addParameter("usCounty", usCounty);
			countiesControl.addParameter("territorySeq", requestWrapper
					.getParameter("territorySeq"));

			uiContext.addControl(countiesControl);
		} else if ("iclSubmitActions".equalsIgnoreCase(uiContext
				.getControlName())) {

			GenericUIControl iclSubmitActions = new GenericUIControl();
			iclSubmitActions.setTemplateName(controlDefnintion);
			iclSubmitActions.addParameter("iclSubmitActions", true);

			uiContext.addControl(iclSubmitActions);
		} else if ("wireCenterStatesSelector".equalsIgnoreCase(uiContext
				.getControlName())) {
			/* String queryString = "SELECT DISTINCT STATE_NAME FROM IMS.IMS_WIRECENTERS WHERE STATE_NAME IS NOT NULL ORDER BY STATE_NAME";
			DatabaseResult dbResult = getPlatformService().loadResult(
					queryString); */
			DatabaseResult dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get(
							"SELECT_WIRECENTER_STATES"));
			GenericUIControl statesControl = new GenericUIControl();
			statesControl.setTemplateName(controlDefnintion);
			statesControl.addParameter("wireCenterStatesSelector", true);
			statesControl.addParameter("result", dbResult);

			uiContext.addControl(statesControl);

		} else if ("wireCenterSelector".equalsIgnoreCase(uiContext
				.getControlName())) {
			/* String queryString = "SELECT DISTINCT WIRECENTER_NAME FROM IMS.IMS_WIRECENTERS WHERE WIRECENTER_NAME IS NOT NULL AND STATE_NAME like '"
					+ requestWrapper.getParameter("stateId")
					+ "' ORDER BY WIRECENTER_NAME ";
			DatabaseResult dbResult = getPlatformService().loadResult(
					queryString); */
			List list = new ArrayList();
			list.add(requestWrapper.getParameter("stateId"));
			list.toArray(new Object[0]);

			DatabaseResult dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get(
							"SELECT_WIRECENTER_BY_STATEID"), list);
			GenericUIControl statesControl = new GenericUIControl();
			statesControl.setTemplateName(controlDefnintion);
			statesControl.addParameter("wireCenterSelector", true);
			statesControl.addParameter("result", dbResult);

			uiContext.addControl(statesControl);

		} else if ("daSelector".equalsIgnoreCase(uiContext.getControlName())) {
			/* String queryString = "SELECT DISTINCT DA_NAME FROM IMS.IMS_DA WHERE DA_NAME IS NOT NULL AND WIRECENTER_NAME like '"
					+ requestWrapper.getParameter("wcId")
					+ "' ORDER BY DA_NAME";
			DatabaseResult dbResult = getPlatformService().loadResult(
					queryString); */
			List list = new ArrayList();
			list.add(requestWrapper.getParameter("wcId"));
			list.toArray(new Object[0]);

			DatabaseResult dbResult = getPlatformService().loadResult(
					QueryUtils.getQuery().get(
							"SELECT_DA"), list);
			GenericUIControl statesControl = new GenericUIControl();
			statesControl.setTemplateName(controlDefnintion);
			statesControl.addParameter("daSelector", true);
			statesControl.addParameter("result", dbResult);

			uiContext.addControl(statesControl);

		}

		if (StringUtils.isNotBlank(requestWrapper.getParameter("action"))) {
			if (StringUtils.equalsIgnoreCase(requestWrapper
					.getParameter("action"), "saveIclTerritory")) {
				if (StringUtils.isBlank(requestWrapper
						.getParameter("territorySeq"))) {
					//String sql = "insert into IMS.IMS_ICL_TERRITORY_DEF(OFFICE_SEQ, TERRITORY_KEY, IS_ACTIVE, CREATED_DATE, CREATED_BY, MODIFIED_DATE, MODIFIED_BY) values (?,?,?,?,?,?,?);";
					getPlatformService().update(
							QueryUtils.getQuery().get("INSERT_IMS_ICL_TERRITORY_DEF"),
							new Object[] {
									new Integer(requestWrapper
											.getParameter("officeSeq")),
									new Integer(requestWrapper
											.getParameter("countyKey")),
									Boolean.TRUE.booleanValue(),
									new Timestamp(System.currentTimeMillis()),
									"admin",
									new Timestamp(System.currentTimeMillis()),
									"admin" });
				} else if (!StringUtils.isBlank(requestWrapper
						.getParameter("territorySeq"))) {
					//String sql = "update IMS.IMS_ICL_TERRITORY_DEF set  TERRITORY_KEY=?, MODIFIED_DATE=?, MODIFIED_BY=? WHERE ICL_TERRITORY_SEQ=?;";
					getPlatformService().update(
							QueryUtils.getQuery().get("UPDATE_IMS_ICL_TERRITORY_DEF"),
							new Object[] {

									new Integer(requestWrapper
											.getParameter("countyKey")),
									new Timestamp(System.currentTimeMillis()),
									"admin",
									new Integer(requestWrapper
											.getParameter("territorySeq")) });
				}
			}
		}
		if (StringUtils.isNotBlank(requestWrapper.getParameter("action"))) {
			if (StringUtils.equalsIgnoreCase(requestWrapper
					.getParameter("action"), "saveIclRepTerritory")) {
				JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator
						.getService("jdbcTemplate");
				PlatformTransactionManager transactionManager = (PlatformTransactionManager) ServiceLocator
						.getService("txManager");
				DBUtil dbUtil = new DBUtil(jdbcTemplate, transactionManager);

				String wc = requestWrapper.getParameter("wireCenter");
				String da = requestWrapper.getParameter("da");
				String stateName = requestWrapper.getParameter("stateName");
				String repId = requestWrapper.getParameter("repId");
				String type = "WIRECENTER";
				String tName = wc;
				Territory territory = new Territory();
				if (!StringUtils.isBlank(da)) {
					type = "DA";
					tName = da;
					territory.setGroup(wc);
				}
				territory.setName(tName);
				territory.setCountyTypeId(tName);
				territory.setType(type);
				territory.setDescription("TerritoryType:"+territory.getType());
				territory.setStateName(stateName);
				territory.setStateId(stateName);
				territory.setActive(Boolean.TRUE);
				territory.setCreatedUser("admin");
				territory.setCreatedDate(new Timestamp(System
						.currentTimeMillis()));
				territory.setModifiedDate(new Timestamp(System
						.currentTimeMillis()));
				territory.setModifiedUser("admin");

				List<Territory> list = new ArrayList<Territory>();
				list.add(territory);
				int tKey = dbUtil.insertCounties(list, stateName).get(tName);
				getPlatformService()
						.update(
								QueryUtils.getQuery().get("INSERT_IMS_ICLREP_TERRITORY_DEF"),
								new Object[] {
										repId,
										tKey,
										Boolean.TRUE,
										new Timestamp(System
												.currentTimeMillis()),
										"admin",
										new Timestamp(System
												.currentTimeMillis()), "admin",requestWrapper
												.getParameter("officeSeq"),requestWrapper
												.getParameter("campaignSeq") });
			}
		}

	}

	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
