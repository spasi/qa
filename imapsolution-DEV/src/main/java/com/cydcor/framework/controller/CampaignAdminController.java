/**
 * 
 */
package com.cydcor.framework.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cydcor.framework.clickframework.GenericUIControl;
import com.cydcor.framework.clickframework.TableUIControl;
import com.cydcor.framework.context.CydcorContext;
import com.cydcor.framework.context.DynamicUIContext;
import com.cydcor.framework.model.DatabaseResult;
import com.cydcor.framework.utils.QueryUtils;

/**
 * This handles all Operations For CampaignAdmin Page
 * 
 * @author ashwin
 * 
 */
public class CampaignAdminController extends GenericController {
	


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.controller.Controller#execute(com.cydcor.framework
	 * .context.DynamicUIContext)
	 */
	
	public void execute(DynamicUIContext uiContext) {
		String pageDefnintion = "account/CampaignAdminDef.html";
		String controlDefnintion = "account/CampaignAdmin.html";
	
		/**
		 * If controlName is null we assume it's request for new Page and so Add
		 * Control to clear all Existing Components in page
		 */
		if (StringUtils.isBlank(uiContext.getControlName())) {
			// Add Second Control the Campaign Selector
			GenericUIControl campaignSelectorControl = new GenericUIControl();
			campaignSelectorControl.setTemplateName(pageDefnintion);
			campaignSelectorControl.addParameter("campaignSelector", true);

			uiContext.addControl(campaignSelectorControl);

			// Add Map Control Noe
			GenericUIControl mapControl = new GenericUIControl();
			mapControl.setTemplateName(pageDefnintion);
			mapControl.addParameter("showCampaignMap", true);

			uiContext.addControl(mapControl);

			// Add Map Control Noe
			GenericUIControl iclTableControl = new GenericUIControl();
			iclTableControl.setTemplateName(pageDefnintion);
			iclTableControl.addParameter("iclTable", true);

			uiContext.addControl(iclTableControl);

		} else {
			DatabaseResult dbResult = null;
			String queryString = null;
			if (StringUtils.endsWithIgnoreCase(uiContext.getControlName(),
					"campaignSelector")) {
				
				dbResult = getPlatformService().loadResult(QueryUtils.getQuery().get("SELECT_CAMPAIGN_DETAILS"));
			  
				// Add Second Control the Campaign Selector
				GenericUIControl campaignSelectorControl = new GenericUIControl();
				campaignSelectorControl.setTemplateName(controlDefnintion);
				campaignSelectorControl.addParameter("campaignSelector", true);
				campaignSelectorControl.addParameter("result", dbResult);

				uiContext.addControl(campaignSelectorControl);

			} else if (StringUtils.endsWithIgnoreCase(uiContext
					.getControlName(), "iclTable")) {

				String campaignSeq = CydcorContext.getInstance()
						.getRequestContext().getRequestWrapper().getParameter(
								"campaignSeq");
				if (StringUtils.isNotBlank(campaignSeq)) {
					
					List list = new ArrayList();
					list.add(campaignSeq);					
					list.toArray(new Object[0]);

					TableUIControl iclTableControl = new TableUIControl();
					iclTableControl.setFilterParamList(list);
					iclTableControl.setQueryKeyName("SELECT_ICL_CAMPAIGN_DETAILS");
					iclTableControl.setColumnName("OFFICE_SEQ");
					iclTableControl.setJavaScriptMethodName("loadCampiagnICLS");
					iclTableControl.addParameter("iclTable", true);
					iclTableControl.setTemplateName("../" + controlDefnintion);

					uiContext.addControl(iclTableControl);
				}
			}

		}// Control Rendering Ends Here

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cydcor.framework.controller.Controller#getResult(java.lang.Object,
	 * java.util.Map)
	 */
	public Object getResult(Object imput, Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cydcor.framework.controller.Controller#init(java.lang.Object)
	 */
	public void init(Object params) {
		// TODO Auto-generated method stub

	}

}
