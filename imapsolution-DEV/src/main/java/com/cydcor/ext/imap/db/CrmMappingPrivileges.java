package com.cydcor.ext.imap.db;

import java.io.Serializable;

public class CrmMappingPrivileges implements Serializable {

	private String privilegeId;
	private String role;
	private String userAlias;
	private String applicationProfile;
	private String campaigns;
	private String offices;
	private String created;
	private String modified;
	private String firstName;
	private String lastName;
	private String fullName;
	private String email;
	private String loginId;
	private String isActive;

	public CrmMappingPrivileges() {

	}

	public CrmMappingPrivileges(String privilegeId, String role,
			String userAlias, String applicationProfile, String campaigns,
			String offices, String created, String modified, String firstName,
			String lastName, String fullName, String email, String loginId,
			String isActive) {
		super(); 
		this.privilegeId = privilegeId;
		this.role = role;
		this.userAlias = userAlias;
		this.applicationProfile = applicationProfile;
		this.campaigns = campaigns;
		this.offices = offices;
		this.created = created;
		this.modified = modified;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.email = email;
		this.loginId = loginId;
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((applicationProfile == null) ? 0 : applicationProfile
						.hashCode());
		result = prime * result
				+ ((campaigns == null) ? 0 : campaigns.hashCode());
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((fullName == null) ? 0 : fullName.hashCode());
		result = prime * result
				+ ((isActive == null) ? 0 : isActive.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((loginId == null) ? 0 : loginId.hashCode());
		result = prime * result
				+ ((modified == null) ? 0 : modified.hashCode());
		result = prime * result + ((offices == null) ? 0 : offices.hashCode());
		result = prime * result
				+ ((privilegeId == null) ? 0 : privilegeId.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result
				+ ((userAlias == null) ? 0 : userAlias.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrmMappingPrivileges other = (CrmMappingPrivileges) obj;
		if (applicationProfile == null) {
			if (other.applicationProfile != null)
				return false;
		} else if (!applicationProfile.equals(other.applicationProfile))
			return false;
		if (campaigns == null) {
			if (other.campaigns != null)
				return false;
		} else if (!campaigns.equals(other.campaigns))
			return false;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (fullName == null) {
			if (other.fullName != null)
				return false;
		} else if (!fullName.equals(other.fullName))
			return false;
		if (isActive == null) {
			if (other.isActive != null)
				return false;
		} else if (!isActive.equals(other.isActive))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (loginId == null) {
			if (other.loginId != null)
				return false;
		} else if (!loginId.equals(other.loginId))
			return false;
		if (modified == null) {
			if (other.modified != null)
				return false;
		} else if (!modified.equals(other.modified))
			return false;
		if (offices == null) {
			if (other.offices != null)
				return false;
		} else if (!offices.equals(other.offices))
			return false;
		if (privilegeId == null) {
			if (other.privilegeId != null)
				return false;
		} else if (!privilegeId.equals(other.privilegeId))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (userAlias == null) {
			if (other.userAlias != null)
				return false;
		} else if (!userAlias.equals(other.userAlias))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "\n\t\tCrmMappingPrivileges [privilegeId=" + privilegeId + ", role="
				+ role + ", userAlias=" + userAlias + ", applicationProfile="
				+ applicationProfile + ", campaigns=" + campaigns
				+ ", offices=" + offices + ", created=" + created
				+ ", modified=" + modified + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", fullName=" + fullName
				+ ", email=" + email + ", loginId=" + loginId + ", isActive="
				+ isActive + "]";
	}

	public String getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(String privilegeId) {
		this.privilegeId = privilegeId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUserAlias() {
		return userAlias;
	}

	public void setUserAlias(String userAlias) {
		this.userAlias = userAlias;
	}

	public String getApplicationProfile() {
		return applicationProfile;
	}

	public void setApplicationProfile(String applicationProfile) {
		this.applicationProfile = applicationProfile;
	}

	public String getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(String campaigns) {
		this.campaigns = campaigns;
	}

	public String getOffices() {
		return offices;
	}

	public void setOffices(String offices) {
		this.offices = offices;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

}
