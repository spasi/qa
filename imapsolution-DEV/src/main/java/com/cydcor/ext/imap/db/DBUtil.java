package com.cydcor.ext.imap.db;


import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;

import com.cydcor.framework.utils.FreeMarkerEngine;
import com.cydcor.framework.utils.QueryUtils;
import com.cydcor.framework.utils.ServiceLocator;
//import com.sun.script.javascript.JSAdapter;

/**
 * DB Operations
 * 
 * @author Aswin
 */

public class DBUtil {
	protected static Log logger = LogFactory.getLog(DBUtil.class);
	
	public static JdbcTemplate getJdbcTemplate(String clientKey) {
		//System.out.println("Not CydcorPortal::: + " + clientKey);
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ServiceLocator //.getService("jdbcTemplate");
				.getService(clientKey+"JdbcTemplate");
		if (jdbcTemplate == null) {
			//System.out.println("CydcorPortal::: + " + clientKey);
			jdbcTemplate = (JdbcTemplate) ServiceLocator.getService("CydcorPortalJdbcTemplate");
		}
		return jdbcTemplate;
	}


	public static String getCydcorDBName() {
		if (strCydcorDBName == null) {
			Properties customProps = new Properties();
			
			 try {
				customProps = loadProperties("/CustomProps.properties");
				strCydcorDBName = customProps.getProperty("cydcor.db.name");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return strCydcorDBName;
	}
	
	public static String getMapsProdDBName(String strProvider) {
		//if (strMapsDBName == null) 
		{
			Properties customProps = new Properties();
			String strDBKey = strProvider + ".db.name";
			 try {
				customProps = loadProperties("/CustomProps.properties");
				strMapsDBName = customProps.getProperty(strDBKey);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return strMapsDBName;
	}
	
	public static Properties loadProperties(String filename) throws IOException {
		// create properties object
		Properties properties = new Properties();
		// Load properties from class loader
		properties.load(DBUtil.class.getResourceAsStream(filename));
		// Return
		return properties;
	}

	
	private static JdbcTemplate jdbcTemplate;
	private static String strCydcorDBName;
	private static String strMapsDBName;
	
	public Connection dbConnect(String db_connect_string, String db_userid, String db_password) {

		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			Connection conn = DriverManager.getConnection(db_connect_string, db_userid, db_password);
			return conn;
		} catch (Exception e) {
			logger.debug(e);
		}
		return null;
	}
	
	public static List<ListOrderedMap> getAccessLinksByRoleName() throws Exception {
		String sql = QueryUtils.getQuery().get("SELECT_ACCESS_LINKS_BY_ROLE_NAME");
		try {
			sql = FreeMarkerEngine.getInstance().evaluateString(sql);
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}

		return getJdbcTemplate("").queryForList(sql);
	}
	
	@SuppressWarnings("unchecked")
	public static List<CrmMappingPrivileges> getPrivilegeMappings(String topMaxRowCount, String whereClause, String clientKey) throws Exception {
		String topRowsText = "all".equals(topMaxRowCount)?"" : "TOP " + topMaxRowCount;
		
		
		String sql = "SELECT "+topRowsText+" [PRIVILEGE_ID]" + 
				"      ,[ROLE]" + 
				"      ,[USER_ALIAS]" + 
				"      ,[APPLICATION_PROFILE]" + 
				",(select top 1 CAMPAIGN_NAME  from " + getMapsProdDBName(clientKey) + ".merlin.CAMPAIGNS where CAMPAIGN_SEQ = ISNUMERIC(NULLIF([CAMPAIGNS],''))) as [CAMPAIGNS]" +
				",(select top 1 OFFICE_NAME  from " + getMapsProdDBName(clientKey) + ".merlin.OFFICES where OFFICE_SEQ = ISNUMERIC(NULLIF([OFFICES],''))) as [OFFICES]" +				
				//"      ,[CAMPAIGNS]" + 
				//"      ,[OFFICES]" + 
				"      ,[CREATED]" + 
				"      ,[MODIFIED]" + 
				"      ,[FIRST_NAME]" + 
				"      ,[LAST_NAME]" + 
				"      ,[FULL_NAME]" + 
				"      ,[EMAIL]" + 
				"      ,[LOGIN_ID]" + 
				"      ,[IS_ACTIVE]" + 
				"  FROM " + getMapsProdDBName(clientKey) + ".[IMS].[IMS_CRM_MAPPING_PRIVILEGES]" +
					whereClause +
				"    order by PRIVILEGE_ID asc";
		try {
			sql = FreeMarkerEngine.getInstance().evaluateString(sql);
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
		return getJdbcTemplate(clientKey).query(sql, new CrmMappingPrivilegesRowMapper());
	}
	
	public static CrmMappingPrivileges getCrmMappingPrivilegesForId(String privilegeId, String clientKey) throws Exception {
		String sql = "SELECT [PRIVILEGE_ID]" + 
				"      ,[ROLE]" + 
				"      ,[USER_ALIAS]" + 
				"      ,[APPLICATION_PROFILE]" + 
				"      ,[CAMPAIGNS]" + 
				"      ,[OFFICES]" + 
				"      ,[CREATED]" + 
				"      ,[MODIFIED]" + 
				"      ,[FIRST_NAME]" + 
				"      ,[LAST_NAME]" + 
				"      ,[FULL_NAME]" + 
				"      ,[EMAIL]" + 
				"      ,[LOGIN_ID]" + 
				"      ,[IS_ACTIVE]" + 
				"  FROM " + getMapsProdDBName(clientKey) + ".[IMS].[IMS_CRM_MAPPING_PRIVILEGES]" +
				"   WHERE PRIVILEGE_ID = '" + privilegeId + "' " +
				"    order by PRIVILEGE_ID asc";
		try {
			sql = FreeMarkerEngine.getInstance().evaluateString(sql);
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
		List<CrmMappingPrivileges> crmMappingsList = getJdbcTemplate(clientKey).query(sql, new CrmMappingPrivilegesRowMapper()); 
		if (crmMappingsList.size() == 1) {
			return crmMappingsList.get(0);
		}
		return null;
	}
	
	public static boolean updateUserDetailsFor(final String userName, final CrmMappingPrivileges cmp) {
		final boolean[] foundUserName = new boolean[1];
		String sql = "SELECT [UserID]" + 
				"      ,[UserName]" + 
				"      ,[EmailAddress]" + 
				"      ,[FirstName]" + 
				"      ,[LastName]" + 
				"      ,[Active]" + 
				"      ,[UpdatedByID]" + 
				"      ,[LastUpdate]" + 
				"      ,[ISSuperUser]" + 
				"      ,[ISADUser]" + 
				"      ,[UserPassword]" + 
				"      ,[LastPasswordChangedDate]" + 
				"  FROM " + getCydcorDBName() + ".[dbo].[Users] " +
				"	WHERE UserName = '" + userName + "'";
		
		getJdbcTemplate("CydcorPortal").query(sql, new RowMapper() {
			@Override
			public Object mapRow(ResultSet rs, int rowNumber) throws SQLException {
					cmp.setEmail(rs.getString("EmailAddress"));
					cmp.setFirstName(rs.getString("FirstName"));
					cmp.setLastName(rs.getString("LastName"));
					cmp.setFullName(rs.getString("FirstName") + " " + rs.getString("LastName"));
					//cmp.setLoginId("CYDCOR/"+ userName.toUpperCase());
					cmp.setLoginId(userName.toUpperCase());
					foundUserName[0] = true;
				return null;
			}			
		}); 		
		return foundUserName[0];
	}
	
	public static boolean insertOrUpdateCrmMappingPrivileges(final CrmMappingPrivileges cmp, final String loggedInUserName, String clientKey) {
		boolean retVal = true;
		
		if (cmp.getPrivilegeId() == null) {
			
			if( clientKey != null )
			{
				retVal = insertCrmMappingPrivileges(cmp,loggedInUserName,clientKey);
				if( ! clientKey.matches("att"))
				{
					retVal = insertCrmMappingPrivileges(cmp,loggedInUserName,"att");
				}
				if( ! clientKey.matches("verizonb2b"))
				{
					retVal = insertCrmMappingPrivileges(cmp,loggedInUserName,"verizonb2b");
				}
				if( ! clientKey.matches("verizonfios"))
				{
					retVal = insertCrmMappingPrivileges(cmp,loggedInUserName,"verizonfios");
				}
				if( ! clientKey.matches("centurylink"))
				{
					retVal = insertCrmMappingPrivileges(cmp,loggedInUserName,"centurylink");
				}
				retVal = true;
			}
			else
			{
				boolean retVal2 = insertCrmMappingPrivileges(cmp,loggedInUserName,"att");
				boolean retVal3 = insertCrmMappingPrivileges(cmp,loggedInUserName,"verizonb2b");
				boolean retVal4 = insertCrmMappingPrivileges(cmp,loggedInUserName,"verizonfios");
				boolean retVal5 = insertCrmMappingPrivileges(cmp,loggedInUserName,"centurylink");
				if ( retVal2 || retVal3 || retVal4 || retVal5 )
				{
					retVal = true;
				}
			}

		}
		else
		{
			boolean retVal2 = insertOrUpdateCrmMappingPrivileges2(cmp,loggedInUserName,"att");
			boolean retVal3 = insertOrUpdateCrmMappingPrivileges2(cmp,loggedInUserName,"verizonb2b");
			boolean retVal4 = insertOrUpdateCrmMappingPrivileges2(cmp,loggedInUserName,"verizonfios");
			boolean retVal5 = insertOrUpdateCrmMappingPrivileges2(cmp,loggedInUserName,"centurylink");
			if ( retVal2 || retVal3 || retVal4 || retVal5 )
			{
				retVal = true;
			}
		}
		
		
		return retVal;
	}
	

	public static boolean insertCrmMappingPrivileges(final CrmMappingPrivileges cmp, final String loggedInUserName, String clientKey) {
		try {
		//if (!isUserAdminForProfile(loggedInUserName, cmp.getApplicationProfile())) {
			//return false;
		//}
		{		
			if (cmp.getPrivilegeId() == null) {	
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlOutParameter("@newSeqText", Types.VARCHAR));
			//Map map = jdbcTemplate.call(new CallableStatementCreator() {
			Map map = getJdbcTemplate(clientKey).call(new CallableStatementCreator() {

				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.GetPrivMappingSeqId(?)}");
					statement.registerOutParameter(1, Types.VARCHAR);
					return statement;
				}
			}, parameters );
			
			String privilegeId = (String) map.get("@newSeqText");
			
			
			cmp.setPrivilegeId(privilegeId);
			}
			
			String insertSql = "INSERT INTO " + getMapsProdDBName(clientKey) + ".[IMS].[IMS_CRM_MAPPING_PRIVILEGES] ([PRIVILEGE_ID]" + 
					"      ,[ROLE]" + 
					"      ,[USER_ALIAS]" + 
					"      ,[APPLICATION_PROFILE]" + 
					"      ,[CAMPAIGNS]" + 
					"      ,[OFFICES]" + 
					"      ,[CREATED]" + 
					"      ,[MODIFIED]" + 
					"      ,[FIRST_NAME]" + 
					"      ,[LAST_NAME]" + 
					"      ,[FULL_NAME]" + 
					"      ,[EMAIL]" + 
					"      ,[LOGIN_ID]" + 
					"      ,[IS_ACTIVE]) VALUES " + 
					"      (?,  ?,  ?,  ?,  ?, ?, " +
					"(SELECT FirstName + space(1) + LastName + ', ' +  rtrim(convert(char, getdate(), 101)) + ' ' + ltrim(convert(char, getdate(), 108))  from " + getCydcorDBName() + ".[dbo].[Users] where UserName = ?) , " +
					"(SELECT FirstName + space(1) + LastName + ', ' + rtrim(convert(char, getdate(), 101)) + ' ' + ltrim(convert(char, getdate(), 108))  from " + getCydcorDBName() + ".[dbo].[Users] where UserName = ?) , " +
					"?,  ?,  ?, ?, ?,  ?  )";
			
			return getJdbcTemplate(clientKey).update(insertSql, new PreparedStatementSetter() {
				
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(1, cmp.getPrivilegeId());
					ps.setString(2, cmp.getRole());
					ps.setString(3, cmp.getUserAlias());
					ps.setString(4, cmp.getApplicationProfile());
					if("-1".equals(cmp.getCampaigns()))
					{
						ps.setNull(5, java.sql.Types.VARCHAR);
					}
					else
					{
						ps.setString(5,cmp.getCampaigns());
					}
					if("-1".equals(cmp.getOffices()))
					{
						ps.setNull(6, java.sql.Types.VARCHAR);
					}
					else
					{
						ps.setString(6,cmp.getOffices());
					}					//ps.setString(6, cmp.getOffices());
					//ps.setString(6, "-1".equals(cmp.getOffices())?"NULL":cmp.getOffices());
					ps.setString(7, loggedInUserName);
					ps.setString(8, loggedInUserName);
					ps.setString(9, cmp.getFirstName());
					ps.setString(10, cmp.getLastName());
					ps.setString(11, cmp.getFullName());
					ps.setString(12, cmp.getEmail());
					ps.setString(13, cmp.getLoginId());
					ps.setString(14, cmp.getIsActive());
					
				}
			}) == 1;
		} 
		}
		catch(Exception ex)
		{
			System.out.println("Error in inserting data : " + ex.getMessage());
			return false;
		}
		
	}

	
	public static boolean insertOrUpdateCrmMappingPrivileges2(final CrmMappingPrivileges cmp, final String loggedInUserName, String clientKey) 
	{
		try {
		if (!isUserAdminForProfile(loggedInUserName, cmp.getApplicationProfile())) {
			return false;
		}
		if (cmp.getPrivilegeId() == null) {			
			List<SqlParameter> parameters = new ArrayList<SqlParameter>();
			parameters.add(new SqlOutParameter("@newSeqText", Types.VARCHAR));
			//Map map = jdbcTemplate.call(new CallableStatementCreator() {
			Map map = getJdbcTemplate(clientKey).call(new CallableStatementCreator() {

				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement statement = con.prepareCall("{call dbo.GetPrivMappingSeqId(?)}");
					statement.registerOutParameter(1, Types.VARCHAR);
					return statement;
				}
			}, parameters );
			String privilegeId = (String) map.get("@newSeqText");
			
			cmp.setPrivilegeId(privilegeId);
			
			String insertSql = "INSERT INTO " + getMapsProdDBName(clientKey) + ".[IMS].[IMS_CRM_MAPPING_PRIVILEGES] ([PRIVILEGE_ID]" + 
					"      ,[ROLE]" + 
					"      ,[USER_ALIAS]" + 
					"      ,[APPLICATION_PROFILE]" + 
					"      ,[CAMPAIGNS]" + 
					"      ,[OFFICES]" + 
					"      ,[CREATED]" + 
					"      ,[MODIFIED]" + 
					"      ,[FIRST_NAME]" + 
					"      ,[LAST_NAME]" + 
					"      ,[FULL_NAME]" + 
					"      ,[EMAIL]" + 
					"      ,[LOGIN_ID]" + 
					"      ,[IS_ACTIVE]) VALUES " + 
					"      (?,  ?,  ?,  ?,  ?, ?, " +
					"(SELECT FirstName + space(1) + LastName + ', ' +  rtrim(convert(char, getdate(), 101)) + ' ' + ltrim(convert(char, getdate(), 108))  from " + getCydcorDBName() + ".[dbo].[Users] where UserName = ?) , " +
					"(SELECT FirstName + space(1) + LastName + ', ' + rtrim(convert(char, getdate(), 101)) + ' ' + ltrim(convert(char, getdate(), 108))  from " + getCydcorDBName() + ".[dbo].[Users] where UserName = ?) , " +
					"?,  ?,  ?, ?, ?,  ?  )";
			
			return getJdbcTemplate(clientKey).update(insertSql, new PreparedStatementSetter() {
				
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(1, cmp.getPrivilegeId());
					ps.setString(2, cmp.getRole());
					ps.setString(3, cmp.getUserAlias());
					ps.setString(4, cmp.getApplicationProfile());
					if("-1".equals(cmp.getCampaigns()))
					{
						ps.setNull(5, java.sql.Types.VARCHAR);
					}
					else
					{
						ps.setString(5,cmp.getCampaigns());
					}
					if("-1".equals(cmp.getOffices()))
					{
						ps.setNull(6, java.sql.Types.VARCHAR);
					}
					else
					{
						ps.setString(6,cmp.getOffices());
					}					//ps.setString(6, cmp.getOffices());
					//ps.setString(6, "-1".equals(cmp.getOffices())?"NULL":cmp.getOffices());
					ps.setString(7, loggedInUserName);
					ps.setString(8, loggedInUserName);
					ps.setString(9, cmp.getFirstName());
					ps.setString(10, cmp.getLastName());
					ps.setString(11, cmp.getFullName());
					ps.setString(12, cmp.getEmail());
					ps.setString(13, cmp.getLoginId());
					ps.setString(14, cmp.getIsActive());
					
				}
			}) == 1;
		} else {
			//update script
			
		
		
		String updateSql =  "UPDATE " + getMapsProdDBName(clientKey) + ".[IMS].[IMS_CRM_MAPPING_PRIVILEGES] " + 
				"SET " + 
				"      [ROLE] = ?, " +
				"      [USER_ALIAS] = ?, " +
				"      [APPLICATION_PROFILE] = ?," +
				"      [CAMPAIGNS] = ?, " +
				"      [OFFICES] = ?, " +
				"      [MODIFIED] = " + " (SELECT FirstName + space(1) + LastName + ', ' +  rtrim(convert(char, getdate(), 101)) + ' ' + ltrim(convert(char, getdate(), 108))  from " + getCydcorDBName() + ".[dbo].[Users] where UserName = ?) , " +
				"      [FIRST_NAME] = ?, " +
				"      [LAST_NAME] = ?, " +
				"      [FULL_NAME] = ?, " +
				"      [EMAIL] = ?, " +
				"      [LOGIN_ID] = ?, " +
				"      [IS_ACTIVE] = ? " +
				"   WHERE " + 
				"   	[PRIVILEGE_ID] = ? " + 
				"   	";
			return getJdbcTemplate(clientKey).update(updateSql, new PreparedStatementSetter() {
				
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(1, cmp.getRole());
					ps.setString(2, cmp.getUserAlias());
					ps.setString(3, cmp.getApplicationProfile());
					//ps.setString(4, cmp.getCampaigns());
					//ps.setString(5, cmp.getOffices());
					//ps.setString(4, "-1".equals(cmp.getCampaigns())?"NULL":cmp.getCampaigns());
					//ps.setString(5, "-1".equals(cmp.getOffices())?"NULL":cmp.getOffices());					
					if("-1".equals(cmp.getCampaigns()))
					{
						ps.setNull(4, java.sql.Types.VARCHAR);
					}
					else
					{
						ps.setString(4,cmp.getCampaigns());
					}
					if("-1".equals(cmp.getOffices()))
					{
						ps.setNull(5, java.sql.Types.VARCHAR);
					}
					else
					{
						ps.setString(5,cmp.getOffices());
					}
					ps.setString(6, loggedInUserName);
					ps.setString(7, cmp.getFirstName());
					ps.setString(8, cmp.getLastName());
					ps.setString(9, cmp.getFullName());
					ps.setString(10, cmp.getEmail());
					ps.setString(11, cmp.getLoginId());
					ps.setString(12, cmp.getIsActive());
					ps.setString(13, cmp.getPrivilegeId());
				}
			}) == 1;
		}
		}
		catch(Exception ex)
		{
			System.out.println("Error in inserting data : " + ex.getMessage());
			return false;
		}
	}
	
	public static List<String> getDistinctApplicationProfiles(final String userName, String providerKey) {
		String sql = "SELECT distinct(APPLICATION_PROFILE) " + 
				"  FROM " + getMapsProdDBName(providerKey) + ".[IMS].[IMS_CRM_MAPPING_PRIVILEGES] " + 
				"  where USER_ALIAS = ? AND IS_Active = 'Y' order by APPLICATION_PROFILE DESC";
		return getJdbcTemplate(providerKey).query(sql, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, userName);
			}
		} , new RowMapper(){

			@Override
			public String mapRow(ResultSet rs, int rowNumber) throws SQLException {
				return rs.getString("APPLICATION_PROFILE");
			}
		});
	}
	
	
	public static UserDetails getUserDetailsFor(String userName) {
		String sql = "Select UserID, UserName, FirstName, LastName, EmailAddress, Active from " +
				getCydcorDBName() +  ".[dbo].[Users] where UserName = '"+ userName+"'";
		System.out.println("sql @@@ : " + sql);
		return (UserDetails)getJdbcTemplate("CydcorPortal").queryForObject(sql, new UserDetailsRowMapper());
	}
	
	@SuppressWarnings("unchecked")
	public static boolean isUserAdminForProfile(final String userName, final String profileName) {
		
		String shortProfileLabel = profileName.endsWith(" Leads Profile")? profileName.replace(" Leads Profile", "") : profileName;
		shortProfileLabel = shortProfileLabel.trim();
		String clientKey = "verizonfios";
		
		if( shortProfileLabel.matches("Verizon B2B")) //== "Verizon B2B" )
		{
			clientKey = "verizonb2b";
		}
		if( shortProfileLabel.matches("Verizon FIOS")) // == "Verizon FIOS" )
		{
			clientKey = "verizonfios";
		}	
		if( shortProfileLabel.matches("Century Link")) // == "Verizon FIOS" )
		{
			System.out.println("inside verizon ::::" + shortProfileLabel);
			clientKey = "centurylink";
		}	
		System.out.println("##########clientKey######## : " + clientKey);
		String sql = "SELECT distinct(ROLE) " + 
				"  FROM " +  getMapsProdDBName(clientKey) + ".[IMS].[IMS_CRM_MAPPING_PRIVILEGES] " + 
				"  where IS_ACTIVE = 'Y' and USER_ALIAS = ? and APPLICATION_PROFILE = ? ";
		List<String> rolesList = getJdbcTemplate(clientKey).query(sql, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, userName);
				ps.setString(2, profileName);
			}
		} , new RowMapper(){

			@Override
			public String mapRow(ResultSet rs, int rowNumber) throws SQLException {
				return rs.getString("ROLE");
			}
		});
		for (String role : rolesList) {
			if ("Application Administrator".equals(role)) {
				return true;
			}
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public static List<UserDetails> getUserDetails(final String firstName, final String lastName, final String emailAddress, final String workPhone) {
		String sql = "Select UserID, UserName, FirstName, LastName, EmailAddress, Active from " 
				+  getCydcorDBName() + ".[dbo].[Users] " +
				"where ";
		String whereClause = "";
		if (firstName.indexOf("%") != -1) {
			whereClause += " FirstName like ? AND ";
		} else {
			whereClause += " FirstName = ? AND ";
		}
		if (lastName.indexOf("%") != -1) {
			whereClause += " LastName like ? AND ";
		} else {
			whereClause += " LastName = ? AND ";
		}
		if (lastName.indexOf("%") != -1) {
			whereClause += " EmailAddress like ? ";
		} else {
			whereClause += " EmailAddress = ? ";
		}
		/*
		 TODO: Lakshmipathi: Please enable this once the Users table structure is altered 
		if (lastName.indexOf("%") != -1) {
			whereClause += " AND  WorkPhone like ? ";
		} else {
			whereClause += " AND  WorkPhone = ? ";
		}
		*/		
		sql += whereClause;
		//System.out.println("sql2 @@@ : " + sql);
		return getJdbcTemplate("CydcorPortal").query(sql, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, firstName);
				ps.setString(2, lastName);
				ps.setString(3, emailAddress);
				/*
				 TODO: Lakshmipathi: Please enable this once the Users table structure is altered 
				ps.setString(4, workPhone);
				*/
			}
		} ,new UserDetailsRowMapper());
	}

	private static class UserDetailsRowMapper implements RowMapper {
		@Override
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {			
			UserDetails ud = new UserDetails();
			ud.setUserId(rs.getInt("userID"));
			ud.setUserName(rs.getString("UserName"));
			ud.setFirstName(rs.getString("FirstName"));
			ud.setLastName(rs.getString("LastName"));
			ud.setEmailAddress(rs.getString("EmailAddress"));
			/*
			 TODO: Lakshmipathi: Please enable this once the Users table structure is altered 
			ud.setEmailAddress(rs.getString("WorkPhone"));
			*/
			ud.setIsActive(rs.getInt("Active"));
			return ud;
		}
		
	}
	
	private static class CrmMappingPrivilegesRowMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			CrmMappingPrivileges crmMappingPrivileges = new CrmMappingPrivileges();
			try {
				crmMappingPrivileges.setPrivilegeId(rs.getString("PRIVILEGE_ID"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setRole(rs.getString("ROLE"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setUserAlias(rs.getString("USER_ALIAS"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setApplicationProfile(rs
						.getString("APPLICATION_PROFILE"));
			} catch (Exception e) {
			}
			try {
				crmMappingPrivileges.setCampaigns(rs.getString("CAMPAIGNS"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setOffices(rs.getString("OFFICES"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setCreated(rs.getString("CREATED"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setModified(rs.getString("MODIFIED"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setFirstName(rs.getString("FIRST_NAME"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setLastName(rs.getString("LAST_NAME"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setFullName(rs.getString("FULL_NAME"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setEmail(rs.getString("EMAIL"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setLoginId(rs.getString("LOGIN_ID"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				crmMappingPrivileges.setIsActive(rs.getString("IS_ACTIVE"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return crmMappingPrivileges;
		}
	}

}
