package com.cydcor.ws;

import java.util.Map;

import com.cydcor.framework.model.RoleObject;

/**
 * @author ashwin
 * 
 */
public interface WSMappingInterface {
	public Map<String, Map<String, RoleObject>> call(String userId)
			throws Exception;
}
