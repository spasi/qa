package com.cydcor.ws.stubs.privillege;
/**
 * ListOfCustomObject6Query.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class ListOfCustomObject6Query  implements java.io.Serializable {
    private CustomObject6Query customObject6;

    private java.math.BigInteger pagesize;  // attribute

    private java.math.BigInteger startrownum;  // attribute

    private boolean recordcountneeded;  // attribute

    public ListOfCustomObject6Query() {
    }

    public ListOfCustomObject6Query(
           CustomObject6Query customObject6,
           java.math.BigInteger pagesize,
           java.math.BigInteger startrownum,
           boolean recordcountneeded) {
           this.customObject6 = customObject6;
           this.pagesize = pagesize;
           this.startrownum = startrownum;
           this.recordcountneeded = recordcountneeded;
    }


    /**
     * Gets the customObject6 value for this ListOfCustomObject6Query.
     * 
     * @return customObject6
     */
    public CustomObject6Query getCustomObject6() {
        return customObject6;
    }


    /**
     * Sets the customObject6 value for this ListOfCustomObject6Query.
     * 
     * @param customObject6
     */
    public void setCustomObject6(CustomObject6Query customObject6) {
        this.customObject6 = customObject6;
    }


    /**
     * Gets the pagesize value for this ListOfCustomObject6Query.
     * 
     * @return pagesize
     */
    public java.math.BigInteger getPagesize() {
        return pagesize;
    }


    /**
     * Sets the pagesize value for this ListOfCustomObject6Query.
     * 
     * @param pagesize
     */
    public void setPagesize(java.math.BigInteger pagesize) {
        this.pagesize = pagesize;
    }


    /**
     * Gets the startrownum value for this ListOfCustomObject6Query.
     * 
     * @return startrownum
     */
    public java.math.BigInteger getStartrownum() {
        return startrownum;
    }


    /**
     * Sets the startrownum value for this ListOfCustomObject6Query.
     * 
     * @param startrownum
     */
    public void setStartrownum(java.math.BigInteger startrownum) {
        this.startrownum = startrownum;
    }


    /**
     * Gets the recordcountneeded value for this ListOfCustomObject6Query.
     * 
     * @return recordcountneeded
     */
    public boolean isRecordcountneeded() {
        return recordcountneeded;
    }


    /**
     * Sets the recordcountneeded value for this ListOfCustomObject6Query.
     * 
     * @param recordcountneeded
     */
    public void setRecordcountneeded(boolean recordcountneeded) {
        this.recordcountneeded = recordcountneeded;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListOfCustomObject6Query)) return false;
        ListOfCustomObject6Query other = (ListOfCustomObject6Query) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.customObject6==null && other.getCustomObject6()==null) || 
             (this.customObject6!=null &&
              this.customObject6.equals(other.getCustomObject6()))) &&
            ((this.pagesize==null && other.getPagesize()==null) || 
             (this.pagesize!=null &&
              this.pagesize.equals(other.getPagesize()))) &&
            ((this.startrownum==null && other.getStartrownum()==null) || 
             (this.startrownum!=null &&
              this.startrownum.equals(other.getStartrownum()))) &&
            this.recordcountneeded == other.isRecordcountneeded();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomObject6() != null) {
            _hashCode += getCustomObject6().hashCode();
        }
        if (getPagesize() != null) {
            _hashCode += getPagesize().hashCode();
        }
        if (getStartrownum() != null) {
            _hashCode += getStartrownum().hashCode();
        }
        _hashCode += (isRecordcountneeded() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListOfCustomObject6Query.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ListOfCustomObject6Query"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("pagesize");
        attrField.setXmlName(new javax.xml.namespace.QName("", "pagesize"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("startrownum");
        attrField.setXmlName(new javax.xml.namespace.QName("", "startrownum"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("recordcountneeded");
        attrField.setXmlName(new javax.xml.namespace.QName("", "recordcountneeded"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customObject6");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject6"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject6Query"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
