/**
 * CustomObject6_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cydcor.ws.stubs.privillege;

public class CustomObject6_BindingStub extends org.apache.axis.client.Stub implements com.cydcor.ws.stubs.privillege.CustomObject6_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[5];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CustomObject6Update");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Update_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Update_Input"), com.cydcor.ws.stubs.privillege.CustomObject6Update_Input.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Update_Output"));
        oper.setReturnClass(com.cydcor.ws.stubs.privillege.CustomObject6Update_Output.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Update_Output"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CustomObject6Insert");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Insert_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Insert_Input"), com.cydcor.ws.stubs.privillege.CustomObject6Insert_Input.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Insert_Output"));
        oper.setReturnClass(com.cydcor.ws.stubs.privillege.CustomObject6Insert_Output.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Insert_Output"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CustomObject6QueryPage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6QueryPage_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6QueryPage_Input"), com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Input.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6QueryPage_Output"));
        oper.setReturnClass(com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6QueryPage_Output"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CustomObject6Delete");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Delete_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Delete_Input"), com.cydcor.ws.stubs.privillege.CustomObject6Delete_Input.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Delete_Output"));
        oper.setReturnClass(com.cydcor.ws.stubs.privillege.CustomObject6Delete_Output.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Delete_Output"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CustomObject6Execute");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Execute_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Execute_Input"), com.cydcor.ws.stubs.privillege.CustomObject6Execute_Input.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Execute_Output"));
        oper.setReturnClass(com.cydcor.ws.stubs.privillege.CustomObject6Execute_Output.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Execute_Output"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

    }

    public CustomObject6_BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public CustomObject6_BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public CustomObject6_BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject6Data");
            cachedSerQNames.add(qName);
            cls = CustomObject6Data.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "ListOfCustomObject6Data");
            cachedSerQNames.add(qName);
            cls = CustomObject6Data[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject6Data");
            qName2 = new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Data", "CustomObject6");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "CustomObject6Query");
            cachedSerQNames.add(qName);
            cls = CustomObject6Query.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "ListOfCustomObject6Query");
            cachedSerQNames.add(qName);
            cls = ListOfCustomObject6Query.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:/crmondemand/xml/CustomObject6/Query", "queryType");
            cachedSerQNames.add(qName);
            cls = QueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Delete_Input");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6Delete_Input.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Delete_Output");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6Delete_Output.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Execute_Input");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6Execute_Input.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Execute_Output");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6Execute_Output.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Insert_Input");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6Insert_Input.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Insert_Output");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6Insert_Output.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6QueryPage_Input");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Input.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6QueryPage_Output");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Update_Input");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6Update_Input.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Update_Output");
            cachedSerQNames.add(qName);
            cls = com.cydcor.ws.stubs.privillege.CustomObject6Update_Output.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Update_Output customObject6Update(com.cydcor.ws.stubs.privillege.CustomObject6Update_Input customObject6Update_Input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6Update");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CustomObject6Update"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {customObject6Update_Input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.cydcor.ws.stubs.privillege.CustomObject6Update_Output) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.cydcor.ws.stubs.privillege.CustomObject6Update_Output) org.apache.axis.utils.JavaUtils.convert(_resp, com.cydcor.ws.stubs.privillege.CustomObject6Update_Output.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Insert_Output customObject6Insert(com.cydcor.ws.stubs.privillege.CustomObject6Insert_Input customObject6Insert_Input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6Insert");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CustomObject6Insert"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {customObject6Insert_Input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.cydcor.ws.stubs.privillege.CustomObject6Insert_Output) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.cydcor.ws.stubs.privillege.CustomObject6Insert_Output) org.apache.axis.utils.JavaUtils.convert(_resp, com.cydcor.ws.stubs.privillege.CustomObject6Insert_Output.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output customObject6QueryPage(com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Input customObject6QueryPage_Input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6QueryPage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CustomObject6QueryPage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {customObject6QueryPage_Input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output) org.apache.axis.utils.JavaUtils.convert(_resp, com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Delete_Output customObject6Delete(com.cydcor.ws.stubs.privillege.CustomObject6Delete_Input customObject6Delete_Input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6Delete");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CustomObject6Delete"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {customObject6Delete_Input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.cydcor.ws.stubs.privillege.CustomObject6Delete_Output) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.cydcor.ws.stubs.privillege.CustomObject6Delete_Output) org.apache.axis.utils.JavaUtils.convert(_resp, com.cydcor.ws.stubs.privillege.CustomObject6Delete_Output.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Execute_Output customObject6Execute(com.cydcor.ws.stubs.privillege.CustomObject6Execute_Input customObject6Execute_Input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6Execute");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CustomObject6Execute"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {customObject6Execute_Input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.cydcor.ws.stubs.privillege.CustomObject6Execute_Output) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.cydcor.ws.stubs.privillege.CustomObject6Execute_Output) org.apache.axis.utils.JavaUtils.convert(_resp, com.cydcor.ws.stubs.privillege.CustomObject6Execute_Output.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
