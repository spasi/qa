/**
 * CustomObject6_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cydcor.ws.stubs.privillege;

public interface CustomObject6_Service extends javax.xml.rpc.Service {
    public java.lang.String getCustomObject6Address();

    public com.cydcor.ws.stubs.privillege.CustomObject6_PortType getCustomObject6() throws javax.xml.rpc.ServiceException;

    public com.cydcor.ws.stubs.privillege.CustomObject6_PortType getCustomObject6(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
