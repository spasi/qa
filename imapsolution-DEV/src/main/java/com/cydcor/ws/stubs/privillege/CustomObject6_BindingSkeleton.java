/**
 * CustomObject6_BindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cydcor.ws.stubs.privillege;

public class CustomObject6_BindingSkeleton implements com.cydcor.ws.stubs.privillege.CustomObject6_PortType, org.apache.axis.wsdl.Skeleton {
    private com.cydcor.ws.stubs.privillege.CustomObject6_PortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Update_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Update_Input"), com.cydcor.ws.stubs.privillege.CustomObject6Update_Input.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("customObject6Update", _params, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Update_Output"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Update_Output"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CustomObject6Update"));
        _oper.setSoapAction("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6Update");
        _myOperationsList.add(_oper);
        if (_myOperations.get("customObject6Update") == null) {
            _myOperations.put("customObject6Update", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("customObject6Update")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Insert_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Insert_Input"), com.cydcor.ws.stubs.privillege.CustomObject6Insert_Input.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("customObject6Insert", _params, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Insert_Output"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Insert_Output"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CustomObject6Insert"));
        _oper.setSoapAction("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6Insert");
        _myOperationsList.add(_oper);
        if (_myOperations.get("customObject6Insert") == null) {
            _myOperations.put("customObject6Insert", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("customObject6Insert")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6QueryPage_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6QueryPage_Input"), com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Input.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("customObject6QueryPage", _params, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6QueryPage_Output"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6QueryPage_Output"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CustomObject6QueryPage"));
        _oper.setSoapAction("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6QueryPage");
        _myOperationsList.add(_oper);
        if (_myOperations.get("customObject6QueryPage") == null) {
            _myOperations.put("customObject6QueryPage", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("customObject6QueryPage")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Delete_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Delete_Input"), com.cydcor.ws.stubs.privillege.CustomObject6Delete_Input.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("customObject6Delete", _params, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Delete_Output"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Delete_Output"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CustomObject6Delete"));
        _oper.setSoapAction("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6Delete");
        _myOperationsList.add(_oper);
        if (_myOperations.get("customObject6Delete") == null) {
            _myOperations.put("customObject6Delete", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("customObject6Delete")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Execute_Input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Execute_Input"), com.cydcor.ws.stubs.privillege.CustomObject6Execute_Input.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("customObject6Execute", _params, new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", "CustomObject6Execute_Output"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:crmondemand/ws/ecbs/customobject6/10/2004", ">CustomObject6Execute_Output"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "CustomObject6Execute"));
        _oper.setSoapAction("document/urn:crmondemand/ws/ecbs/customobject6/10/2004:CustomObject6Execute");
        _myOperationsList.add(_oper);
        if (_myOperations.get("customObject6Execute") == null) {
            _myOperations.put("customObject6Execute", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("customObject6Execute")).add(_oper);
    }

    public CustomObject6_BindingSkeleton() {
        this.impl = new com.cydcor.ws.stubs.privillege.CustomObject6_BindingImpl();
    }

    public CustomObject6_BindingSkeleton(com.cydcor.ws.stubs.privillege.CustomObject6_PortType impl) {
        this.impl = impl;
    }
    public com.cydcor.ws.stubs.privillege.CustomObject6Update_Output customObject6Update(com.cydcor.ws.stubs.privillege.CustomObject6Update_Input customObject6Update_Input) throws java.rmi.RemoteException
    {
        com.cydcor.ws.stubs.privillege.CustomObject6Update_Output ret = impl.customObject6Update(customObject6Update_Input);
        return ret;
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Insert_Output customObject6Insert(com.cydcor.ws.stubs.privillege.CustomObject6Insert_Input customObject6Insert_Input) throws java.rmi.RemoteException
    {
        com.cydcor.ws.stubs.privillege.CustomObject6Insert_Output ret = impl.customObject6Insert(customObject6Insert_Input);
        return ret;
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output customObject6QueryPage(com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Input customObject6QueryPage_Input) throws java.rmi.RemoteException
    {
        com.cydcor.ws.stubs.privillege.CustomObject6QueryPage_Output ret = impl.customObject6QueryPage(customObject6QueryPage_Input);
        return ret;
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Delete_Output customObject6Delete(com.cydcor.ws.stubs.privillege.CustomObject6Delete_Input customObject6Delete_Input) throws java.rmi.RemoteException
    {
        com.cydcor.ws.stubs.privillege.CustomObject6Delete_Output ret = impl.customObject6Delete(customObject6Delete_Input);
        return ret;
    }

    public com.cydcor.ws.stubs.privillege.CustomObject6Execute_Output customObject6Execute(com.cydcor.ws.stubs.privillege.CustomObject6Execute_Input customObject6Execute_Input) throws java.rmi.RemoteException
    {
        com.cydcor.ws.stubs.privillege.CustomObject6Execute_Output ret = impl.customObject6Execute(customObject6Execute_Input);
        return ret;
    }

}
